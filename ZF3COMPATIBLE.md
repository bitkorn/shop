
- Frontend\Basket\BasketController
  - Setter for forms
- UserRegistrationService()->registerUser()
  - wegen Frontend\Basket\BasketController line 721
- use UserService from module bitkorn/user
  - \BitkornUser\Entity\User
  - bitkorn/user HTML Login for user
- migrate module BitkornImages to ZF3

## db
Converting from MySQL to PostgreSQL
- [wiki.postgresql.org](https://wiki.postgresql.org/wiki/Converting_from_other_Databases_to_PostgreSQL#MySQL)
  - [pgloader.readthedocs.mysql](https://pgloader.readthedocs.io/en/latest/ref/mysql.html)
  - [pgloader.readthedocs.quickstart](https://pgloader.readthedocs.io/en/latest/quickstart.html#migrating-from-mysql)

```bash
pgloader mysql://pgloader_allapow:sqlkomuni@localhost/systemgurt postgresql://postgres:sqlkomuni@localhost:5432/lmnshop
```


### last output
allapow@sharkoon1912:/var/www/html/gas/godata2$ pgloader mysql://pgloader_allapow:sqlkomuni@localhost/systemgurt postgresql://postgres:sqlkomuni@localhost:5432/lmnshop
2019-12-19T13:10:01.035000Z LOG pgloader version "3.6.1"
2019-12-19T13:10:01.097000Z LOG Migrating from #<MYSQL-CONNECTION mysql://pgloader_allapow@localhost:3306/systemgurt {1005543D73}>
2019-12-19T13:10:01.097000Z LOG Migrating into #<PGSQL-CONNECTION pgsql://postgres@localhost:5432/lmnshop {10056CE323}>
KABOOM!
FATAL error: 76 fell through ECASE expression.
             Wanted one of (2 3 4 5 6 8 9 10 11 14 15 17 20 21 23 27 28 30 31
                            32 33 35 41 42 45 46 47 48 49 50 51 52 54 55 56 60
                            61 62 63 64 65 69 72 77 78 79 82 83 87 90 92 93 94
                            95 96 97 98 101 102 103 104 105 106 107 108 109 110
                            111 112 113 114 115 116 117 118 119 120 121 122 123
                            124 128 129 130 131 132 133 134 135 136 137 138 139
                            140 141 142 143 144 145 146 147 148 149 150 151 159
                            160 161 162 163 164 165 166 167 168 169 170 171 172
                            173 174 175 176 177 178 179 180 181 182 183 192 193
                            194 195 196 197 198 199 200 201 202 203 204 205 206
                            207 208 209 210 211 212 213 214 215 223 224 225 226
                            227 228 229 230 231 232 233 234 235 236 237 238 239
                            240 241 242 243 244 245 246 247 254).
An unhandled error condition has been signalled:
   76 fell through ECASE expression.
   Wanted one of (2 3 4 5 6 8 9 10 11 14 15 17 20 21 23 27 28 30 31 32 33 35 41
                  42 45 46 47 48 49 50 51 52 54 55 56 60 61 62 63 64 65 69 72
                  77 78 79 82 83 87 90 92 93 94 95 96 97 98 101 102 103 104 105
                  106 107 108 109 110 111 112 113 114 115 116 117 118 119 120
                  121 122 123 124 128 129 130 131 132 133 134 135 136 137 138
                  139 140 141 142 143 144 145 146 147 148 149 150 151 159 160
                  161 162 163 164 165 166 167 168 169 170 171 172 173 174 175
                  176 177 178 179 180 181 182 183 192 193 194 195 196 197 198
                  199 200 201 202 203 204 205 206 207 208 209 210 211 212 213
                  214 215 223 224 225 226 227 228 229 230 231 232 233 234 235
                  236 237 238 239 240 241 242 243 244 245 246 247 254).




What I am doing here?

76 fell through ECASE expression.
Wanted one of (2 3 4 5 6 8 9 10 11 14 15 17 20 21 23 27 28 30 31 32 33 35 41 42
               45 46 47 48 49 50 51 52 54 55 56 60 61 62 63 64 65 69 72 77 78
               79 82 83 87 90 92 93 94 95 96 97 98 101 102 103 104 105 106 107
               108 109 110 111 112 113 114 115 116 117 118 119 120 121 122 123
               124 128 129 130 131 132 133 134 135 136 137 138 139 140 141 142
               143 144 145 146 147 148 149 150 151 159 160 161 162 163 164 165
               166 167 168 169 170 171 172 173 174 175 176 177 178 179 180 181
               182 183 192 193 194 195 196 197 198 199 200 201 202 203 204 205
               206 207 208 209 210 211 212 213 214 215 223 224 225 226 227 228
               229 230 231 232 233 234 235 236 237 238 239 240 241 242 243 244
               245 246 247 254).
