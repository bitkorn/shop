
CREATE TABLE `shop_article_class` (
    `shop_article_class_id` int(10) unsigned NOT NULL,
    `shop_article_class_name` varchar(100) NOT NULL,
    PRIMARY KEY (`shop_article_class_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='die Kategorie für das WaWi'


CREATE TABLE `shop_article_size_group` (
    `shop_article_size_group_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `shop_article_size_group_name` varchar(100) NOT NULL,
    `shop_article_size_group_desc` text,
    `shop_article_size_group_heading` varchar(100) DEFAULT NULL,
    `shop_article_size_group_text` text,
    PRIMARY KEY (`shop_article_size_group_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='z.B. Hundegeschirr komplett oder Hundejacke'


CREATE TABLE `shop_article_size_def` (
    `shop_article_size_def_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `shop_article_size_group_id` int(10) unsigned NOT NULL,
    `shop_article_size_def_key` varchar(45) NOT NULL,
    `shop_article_size_def_name` tinytext,
    `shop_article_size_def_desc` text,
    `shop_article_size_def_priority` int(11) NOT NULL DEFAULT '0',
    `shop_article_size_def_order_priority` int(11) NOT NULL DEFAULT '0',
    PRIMARY KEY (`shop_article_size_def_id`),
    KEY `shop_article_size_def_shop_article_size_group_FK` (`shop_article_size_group_id`),
    CONSTRAINT `shop_article_size_def_shop_article_size_group_FK` FOREIGN KEY (`shop_article_size_group_id`) REFERENCES `shop_article_size_group` (`shop_article_size_group_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COMMENT='XXL, XL, L, S ...etc'

CREATE TABLE `shop_article` (
    `shop_article_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `shop_article_sku` varchar(100) DEFAULT NULL,
    `shop_article_class_id` int(10) unsigned NOT NULL DEFAULT '130',
    `shop_article_type` enum('standard','config','group','sizegroup','download') NOT NULL DEFAULT 'standard',
    `shop_article_groupable` int(1) unsigned NOT NULL DEFAULT '0',
    `shop_article_size_def_id` int(10) unsigned NOT NULL DEFAULT '0',
    `shop_article_weight` float(10,2) NOT NULL DEFAULT '0.00' COMMENT 'die gewichtseinheit (shopweit) steht in db.shop_configuration',
    `shop_article_shipping_costs_single` float(10,2) NOT NULL DEFAULT '0.00' COMMENT 'Gilt für Einzelversand. Bei mehreren Artikeln berechnet sich Versand über Gewicht (Höchstmaße für Pakete müssen bedacht werden ...keine langen Stangen :)',
    `shop_article_price` float(10,2) NOT NULL DEFAULT '0.00',
    `shop_article_tax` float(5,2) NOT NULL DEFAULT '19.00' COMMENT 'im Preis inklusive',
    `shop_article_price_net` float(10,2) NOT NULL DEFAULT '0.00',
    `shop_article_name` varchar(200) NOT NULL,
    `shop_article_sefurl` varchar(200) NOT NULL,
    `shop_article_desc` text,
    `shop_article_hexcolor` varchar(7) NOT NULL DEFAULT '' COMMENT 'RGB color',
    `shop_article_meta_title` tinytext NOT NULL,
    `shop_article_meta_desc` text NOT NULL,
    `shop_article_meta_keywords` text NOT NULL,
    `shop_article_active` int(1) unsigned NOT NULL DEFAULT '1',
    `shop_article_active_in_categories` int(1) unsigned NOT NULL DEFAULT '1',
    `shop_article_active_in_search` int(1) unsigned NOT NULL DEFAULT '1',
    `shop_article_stock_using` int(1) unsigned NOT NULL DEFAULT '0',
    `shop_article_feeable` int(1) unsigned NOT NULL DEFAULT '0' COMMENT 'ob es beim Verkauf eine Provision (fee) geben kann',
    PRIMARY KEY (`shop_article_id`),
    UNIQUE KEY `shop_article_sefurl_UNIQUE` (`shop_article_sefurl`)
    ) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8

CREATE TABLE `shop_article_category` (
    `shop_article_category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `shop_article_category_alias` varchar(200) NOT NULL,
    `shop_article_category_name` tinytext NOT NULL,
    `shop_article_category_desc` text NOT NULL,
    `shop_article_category_img` text COMMENT 'relative path with file name, thumbs: imagename_thumb.ext',
    `shop_article_category_hexcolor` varchar(7) DEFAULT NULL,
    `shop_article_category_id_parent` int(10) unsigned NOT NULL DEFAULT '0',
    `shop_article_category_meta_title` tinytext NOT NULL,
    `shop_article_category_meta_desc` text NOT NULL,
    `shop_article_category_meta_keywords` text NOT NULL,
    PRIMARY KEY (`shop_article_category_id`),
    UNIQUE KEY `shop_article_category_alias_UNIQUE` (`shop_article_category_alias`)
    ) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8

CREATE TABLE `shop_article_comment` (
    `shop_article_comment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `shop_article_id` int(10) unsigned NOT NULL,
    `shop_article_comment_id_parent` int(10) unsigned NOT NULL DEFAULT '0',
    `shop_article_comment_text` text NOT NULL,
    `shop_article_comment_image_filename` tinytext,
    `shop_article_comment_active` int(1) NOT NULL DEFAULT '0',
    PRIMARY KEY (`shop_article_comment_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8

CREATE TABLE `shop_article_group_relation` (
    `shop_article_group_relation_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `shop_article_id_root` int(10) unsigned NOT NULL,
    `shop_article_id_slave` int(10) unsigned NOT NULL,
    `shop_article_group_relation_amount` float NOT NULL DEFAULT '1',
    PRIMARY KEY (`shop_article_group_relation_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='zu einem Gruppen-Artikel die gruppierten Artikel'

CREATE TABLE `shop_article_image` (
    `shop_article_image_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `shop_article_id` int(10) unsigned DEFAULT NULL,
    `bk_images_image_id` int(10) unsigned NOT NULL,
    `shop_article_image_priority` int(10) unsigned NOT NULL DEFAULT '0',
    PRIMARY KEY (`shop_article_image_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8

CREATE TABLE `shop_article_option_def` (
    `shop_article_option_def_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `shop_article_option_def_type` enum('input','select','sizedef','multiselect','checkbox','radio','file') NOT NULL DEFAULT 'select',
    `shop_article_option_def_name` varchar(200) NOT NULL,
    `shop_article_option_def_desc` text,
    `shop_article_option_def_view_name` varchar(200) NOT NULL,
    `shop_article_option_def_view_desc` text,
    `shop_article_option_def_priority` int(11) NOT NULL DEFAULT '0' COMMENT 'um die Optionen im Frontend zu sortieren. Z.B. erst die Größe, dann erscheint FarbeSelect mit Bildern hinter jedem OptionItem',
    PRIMARY KEY (`shop_article_option_def_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8

CREATE TABLE `shop_article_option_item` (
    `shop_article_option_item_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `shop_article_option_def_id` int(10) unsigned NOT NULL,
    `shop_article_option_item_name` varchar(200) NOT NULL COMMENT 'bei shop_article_option_def_type = sizedef, ist name = shop_article_size_def_key',
    `shop_article_option_item_value` varchar(200) NOT NULL COMMENT 'bei shop_article_option_def_type = sizedef, ist value = shop_article_size_def_id',
    `shop_article_option_item_priority` int(11) NOT NULL DEFAULT '0',
    `shop_article_option_item_view_name` varchar(200) NOT NULL,
    `shop_article_option_item_view_value` varchar(200) NOT NULL,
    PRIMARY KEY (`shop_article_option_item_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8


CREATE TABLE `shop_basket` (
    `shop_basket_unique` varchar(100) NOT NULL COMMENT 'sha2 for cookie relation',
    `user_id` int(11) DEFAULT NULL COMMENT 'after login; 0 if buy without resgistration',
    `vendor_user_id` int(11) NOT NULL DEFAULT '0' COMMENT 'falls vom POS verkauft wird, hier die user_id des eingeloggten User',
    `shop_basket_time_create` int(10) unsigned NOT NULL,
    `shop_basket_time_order` int(10) unsigned DEFAULT NULL,
    `shop_basket_time_payed` int(10) unsigned DEFAULT NULL COMMENT 'used to build folders; the same as in shop_document_xyz',
    `shop_basket_status` enum('basket','ordered') DEFAULT 'basket' COMMENT 'NUR basket oder ordered, danach ists n shop_basket_entity',
    `payment_method` varchar(20) DEFAULT NULL COMMENT 'OPTIONAL; z.B. PayPal hat n token wenns die redirect_url aufruft',
    `payment_number` varchar(200) DEFAULT NULL COMMENT 'PayPal ist die paymentId',
    `shipping_provider_unique_descriptor` varchar(40) NOT NULL DEFAULT '',
    `shop_basket_discount_hash` varchar(20) DEFAULT NULL,
    `shop_basket_discount_value` float(8,2) DEFAULT NULL,
    `computed_value_article_id_count` int(10) unsigned DEFAULT NULL,
    `computed_value_article_amount_sum` float DEFAULT NULL,
    `computed_value_article_weight_sum` float(10,2) DEFAULT NULL,
    `computed_value_article_shipping_costs_single_sum` float(10,2) DEFAULT NULL,
    `computed_value_article_price_total_sum` float(10,2) DEFAULT NULL,
    `computed_value_article_tax_total_sum` float(10,2) DEFAULT NULL,
    `computed_value_article_price_total_sum_end` float(10,2) DEFAULT NULL COMMENT 'after discount',
    `computed_value_article_tax_total_sum_end` float(10,2) DEFAULT NULL COMMENT 'after discount',
    `computed_value_shop_basket_discount_tax` float(10,2) DEFAULT NULL,
    `computed_value_article_shipping_costs_computed` float(10,2) DEFAULT NULL,
    `computed_value_article_shipping_costs_tax_computed` float(10,2) DEFAULT NULL,
    `computed_value_shop_basket_tax_total_sum_end` float(10,2) DEFAULT NULL,
    PRIMARY KEY (`shop_basket_unique`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8

CREATE TABLE `shop_basket_item` (
    `shop_basket_item_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `shop_basket_unique` varchar(100) NOT NULL,
    `shop_article_id` int(10) unsigned NOT NULL,
    `shop_article_amount` float NOT NULL DEFAULT '1',
    `shop_basket_item_article_options` text NOT NULL COMMENT 'JSON',
    PRIMARY KEY (`shop_basket_item_id`),
    KEY `shop_basket_item_shop_basket_FK` (`shop_basket_unique`),
    CONSTRAINT `shop_basket_item_shop_basket_FK` FOREIGN KEY (`shop_basket_unique`) REFERENCES `shop_basket` (`shop_basket_unique`)
    ) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8

CREATE TABLE `shop_article_rating` (
    `shop_article_rating_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `shop_article_id` int(10) unsigned NOT NULL,
    `shop_basket_item_id` int(10) unsigned DEFAULT NULL COMMENT 'not null if rating for a basket item',
    `shop_article_rating_time_create` int(10) unsigned NOT NULL,
    `shop_article_rating_value` int(1) unsigned NOT NULL DEFAULT '0',
    `shop_article_rating_text` text NOT NULL,
    `shop_article_rating_published` int(1) unsigned NOT NULL DEFAULT '0',
    PRIMARY KEY (`shop_article_rating_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8

CREATE TABLE `shop_article_relation` (
    `shop_article_relation_id` int(10) unsigned NOT NULL,
    `shop_article_id_root` int(10) unsigned NOT NULL,
    `shop_article_id_slave` int(10) unsigned NOT NULL,
    PRIMARY KEY (`shop_article_relation_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='für welche Artikel zusammen passen würden'


CREATE TABLE `shop_article_size_archive` (
    `shop_article_size_archive_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `shop_article_size_archive_uniquehash` varchar(100) NOT NULL COMMENT 'sha256(timestamp + ip)',
    `shop_article_size_archive_ip` varchar(50) NOT NULL COMMENT 'IP (15 chars) IPv6 (39 chars) oder nur IP',
    `shop_article_size_archive_time` int(10) unsigned NOT NULL,
    `shop_article_size_group_id` int(10) unsigned NOT NULL,
    `shop_article_size_position_id` int(10) unsigned NOT NULL,
    `shop_article_size_def_id` int(10) unsigned NOT NULL,
    `shop_article_size_item_value` float NOT NULL DEFAULT '0',
    `user_id` int(11) DEFAULT NULL COMMENT 'if user click on <save in my backend>',
    `shop_article_size_archive_title` varchar(100) DEFAULT NULL,
    PRIMARY KEY (`shop_article_size_archive_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=328 DEFAULT CHARSET=utf8

CREATE TABLE `shop_article_sizegroup_ralation` (
    `shop_article_sizegroup_ralation_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `shop_article_id_root` int(10) unsigned NOT NULL,
    `shop_article_id_slave` int(10) unsigned NOT NULL,
    `shop_article_sizegroup_ralation_amount` float NOT NULL DEFAULT '1',
    PRIMARY KEY (`shop_article_sizegroup_ralation_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Einem Sizegroup-Artikel seine Slave Artikel. Slave-Artikel eines Root-Article müssen die gleiche shop_article_size_def_id haben!'

CREATE TABLE `shop_vendor_group` (
    `shop_vendor_group_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `shop_vendor_group_no` int(10) unsigned NOT NULL,
    `shop_vendor_group_name` varchar(100) NOT NULL,
    PRIMARY KEY (`shop_vendor_group_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8

CREATE TABLE `shop_vendor` (
    `shop_vendor_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `shop_vendor_group_id` int(10) unsigned NOT NULL,
    `shop_vendor_number_int` int(10) unsigned NOT NULL,
    `shop_vendor_number_varchar` varchar(60) NOT NULL,
    `shop_vendor_name` varchar(100) NOT NULL,
    `shop_vendor_street` varchar(100) NOT NULL,
    `shop_vendor_street_no` varchar(10) NOT NULL,
    `shop_vendor_zip` varchar(10) NOT NULL,
    `shop_vendor_pobox` varchar(10) NOT NULL,
    `shop_vendor_city` varchar(100) NOT NULL,
    `iso_country_id` int(10) unsigned NOT NULL,
    `shop_vendor_tel` varchar(45) NOT NULL,
    `shop_vendor_fax` varchar(45) NOT NULL,
    `shop_vendor_email` varchar(100) NOT NULL,
    `shop_vendor_web` varchar(100) NOT NULL,
    `shop_vendor_min_order_value` int(10) unsigned NOT NULL DEFAULT '0',
    `shop_vendor_active` int(1) NOT NULL DEFAULT '1',
    PRIMARY KEY (`shop_vendor_id`),
    UNIQUE KEY `shop_vendor_number_int_UNIQUE` (`shop_vendor_number_int`),
    KEY `fk_shop_vendor_group_idx` (`shop_vendor_group_id`),
    CONSTRAINT `fk_shop_vendor_group` FOREIGN KEY (`shop_vendor_group_id`) REFERENCES `shop_vendor_group` (`shop_vendor_group_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8

CREATE TABLE `shop_article_stock` (
    `shop_article_stock_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `shop_article_id` int(10) unsigned NOT NULL,
    `shop_article_options` text NOT NULL COMMENT 'JSON',
    `shop_article_stock_amount` float NOT NULL DEFAULT '0' COMMENT 'minus oder plus',
    `shop_article_stock_time` int(10) unsigned NOT NULL,
    `shop_vendor_id` int(10) unsigned NOT NULL DEFAULT '0',
    `shop_article_stock_batch` varchar(100) DEFAULT NULL COMMENT 'die ChargenBezeichnung',
    `shop_article_stock_reason_type` enum('basket','retail','production','purchase') DEFAULT NULL,
    `shop_article_stock_reason` tinytext COMMENT 'IF shop_article_stock_reason_type=basket THEN shop_basket_unique & shop_basket_item_id || Text wie z.B. Produktionsstätte 04 oder Messeverkauf',
    PRIMARY KEY (`shop_article_stock_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=168 DEFAULT CHARSET=utf8

CREATE TABLE `shop_basket_claim` (
    `shop_basket_claim_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `shop_basket_unique` varchar(100) NOT NULL COMMENT 'ums leichter gruppiert abzurufen',
    `shop_basket_item_id` int(10) unsigned NOT NULL,
    `shop_basket_claim_reason` text NOT NULL,
    `shop_basket_claim_time_create` int(10) unsigned NOT NULL,
    `shop_basket_claim_user_create` int(11) NOT NULL,
    `shop_basket_claim_finished` int(1) NOT NULL DEFAULT '0',
    PRIMARY KEY (`shop_basket_claim_id`),
    UNIQUE KEY `shop_basket_item_id_UNIQUE` (`shop_basket_item_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8

CREATE TABLE `shop_basket_entity` (
    `shop_basket_entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `shop_basket_unique` varchar(100) NOT NULL,
    `user_id` int(11) NOT NULL,
    `vendor_user_id` int(11) NOT NULL DEFAULT '0' COMMENT 'falls vom POS verkauft wird, hier die user_id des eingeloggten User',
    `shop_basket_time_create` int(10) unsigned DEFAULT NULL,
    `shop_basket_time_order` int(10) unsigned DEFAULT NULL,
    `shop_basket_time_payed` int(10) unsigned DEFAULT NULL,
    `shop_basket_entity_time_shipped` int(10) unsigned DEFAULT NULL,
    `shop_basket_status` enum('ordered','payed','failed','shipped') DEFAULT NULL,
    `payment_method` varchar(20) DEFAULT NULL COMMENT 'OPTIONAL; z.B. PayPal hat n token wenns die redirect_url aufruft',
    `payment_number` varchar(200) DEFAULT NULL,
    `shipping_pickup_customer` int(1) DEFAULT NULL,
    `shipping_provider_unique_descriptor` varchar(40) DEFAULT NULL,
    `shipping_method` varchar(100) NOT NULL DEFAULT 'dhl',
    `shipping_number` varchar(200) DEFAULT NULL COMMENT 'die kann dann wohl aus shop_basket raus',
    `shop_basket_item_id` int(10) unsigned DEFAULT NULL,
    `shop_article_id` int(10) unsigned DEFAULT NULL,
    `shop_article_amount` float DEFAULT NULL,
    `shop_basket_item_article_options` text,
    `shop_article_sku` varchar(100) DEFAULT NULL,
    `shop_article_type` varchar(45) DEFAULT NULL,
    `shop_article_size_def_id` int(10) unsigned DEFAULT NULL,
    `shop_article_weight` float(10,2) DEFAULT NULL,
    `shop_article_shipping_costs_single` float(10,2) DEFAULT NULL,
    `shop_article_price` float(10,2) DEFAULT NULL,
    `shop_article_price_total` float(10,2) DEFAULT NULL,
    `shop_article_tax` float(5,2) DEFAULT NULL,
    `shop_article_price_net` float(10,2) DEFAULT NULL,
    `shop_article_name` tinytext,
    `shop_article_desc` text,
    `shop_article_active` int(1) unsigned DEFAULT NULL,
    `shop_article_stock_using` int(1) unsigned DEFAULT NULL,
    `shop_basket_discount_hash` varchar(20) DEFAULT NULL,
    `shop_basket_discount_value` float(8,2) DEFAULT NULL,
    `computed_value_article_id_count` int(10) unsigned DEFAULT NULL,
    `computed_value_article_amount_sum` float DEFAULT NULL,
    `computed_value_article_weight_sum` float(10,2) DEFAULT NULL,
    `computed_value_article_shipping_costs_single_sum` float(10,2) DEFAULT NULL,
    `computed_value_article_price_total_sum` float(10,2) DEFAULT NULL,
    `computed_value_article_tax_total_sum` float(10,2) DEFAULT NULL,
    `computed_value_article_price_total_sum_end` float(10,2) DEFAULT NULL,
    `computed_value_article_tax_total_sum_end` float(10,2) DEFAULT NULL,
    `computed_value_shop_basket_discount_tax` float(10,2) DEFAULT NULL,
    `computed_value_article_shipping_costs_computed` float(10,2) DEFAULT NULL,
    `computed_value_article_shipping_costs_tax_computed` float(10,2) DEFAULT NULL,
    `computed_value_shop_basket_tax_total_sum_end` float(10,2) DEFAULT NULL,
    `shop_user_address_shipment_id` int(10) unsigned DEFAULT NULL,
    `shop_user_address_shipment_name1` varchar(100) DEFAULT NULL,
    `shop_user_address_shipment_name2` varchar(100) DEFAULT NULL,
    `shop_user_address_shipment_street` varchar(100) DEFAULT NULL,
    `shop_user_address_shipment_street_no` varchar(45) DEFAULT NULL,
    `shop_user_address_shipment_additional` varchar(100) DEFAULT NULL,
    `shop_user_address_shipment_zip` varchar(10) DEFAULT NULL,
    `shop_user_address_shipment_city` varchar(100) DEFAULT NULL,
    `shop_user_address_shipment_iso_country_id` int(10) unsigned DEFAULT NULL,
    `shop_user_address_shipment_tel` varchar(45) DEFAULT NULL,
    `shop_user_address_shipment_company_name` varchar(100) DEFAULT NULL,
    `shop_user_address_shipment_company_department` varchar(100) DEFAULT NULL,
    `shop_user_address_invoice_id` int(10) unsigned DEFAULT NULL,
    `shop_user_address_invoice_salut` varchar(20) DEFAULT NULL,
    `shop_user_address_invoice_degree` varchar(20) DEFAULT NULL,
    `shop_user_address_invoice_name1` varchar(100) DEFAULT NULL,
    `shop_user_address_invoice_name2` varchar(100) DEFAULT NULL,
    `shop_user_address_invoice_street` varchar(100) DEFAULT NULL,
    `shop_user_address_invoice_street_no` varchar(45) DEFAULT NULL,
    `shop_user_address_invoice_zip` varchar(10) DEFAULT NULL,
    `shop_user_address_invoice_city` varchar(100) DEFAULT NULL,
    `shop_user_address_invoice_additional` varchar(100) DEFAULT NULL,
    `shop_user_address_invoice_iso_country_id` int(10) unsigned DEFAULT NULL,
    `shop_user_address_invoice_tel` varchar(45) DEFAULT NULL,
    `shop_user_address_invoice_company_name` varchar(100) DEFAULT NULL,
    `shop_user_address_invoice_company_department` varchar(100) DEFAULT NULL,
    `shop_user_address_tax_id` varchar(30) DEFAULT NULL,
    `shop_basket_address_shipment_id` int(10) unsigned DEFAULT NULL,
    `shop_basket_address_shipment_name1` varchar(100) DEFAULT NULL,
    `shop_basket_address_shipment_name2` varchar(100) DEFAULT NULL,
    `shop_basket_address_shipment_street` varchar(100) DEFAULT NULL,
    `shop_basket_address_shipment_street_no` varchar(45) DEFAULT NULL,
    `shop_basket_address_shipment_zip` varchar(10) DEFAULT NULL,
    `shop_basket_address_shipment_city` varchar(100) DEFAULT NULL,
    `shop_basket_address_shipment_additional` varchar(100) DEFAULT NULL,
    `shop_basket_address_shipment_iso_country_id` int(10) unsigned DEFAULT NULL,
    `shop_basket_address_shipment_email` varchar(100) DEFAULT NULL,
    `shop_basket_address_shipment_tel` varchar(45) DEFAULT NULL,
    `shop_basket_address_shipment_company_name` varchar(100) DEFAULT NULL,
    `shop_basket_address_shipment_company_department` varchar(100) DEFAULT NULL,
    `shop_basket_address_invoice_id` int(10) unsigned DEFAULT NULL,
    `shop_basket_address_invoice_salut` varchar(20) DEFAULT NULL,
    `shop_basket_address_invoice_degree` varchar(20) DEFAULT NULL,
    `shop_basket_address_invoice_name1` varchar(100) DEFAULT NULL,
    `shop_basket_address_invoice_name2` varchar(100) DEFAULT NULL,
    `shop_basket_address_invoice_street` varchar(100) DEFAULT NULL,
    `shop_basket_address_invoice_street_no` varchar(45) DEFAULT NULL,
    `shop_basket_address_invoice_zip` varchar(10) DEFAULT NULL,
    `shop_basket_address_invoice_city` varchar(100) DEFAULT NULL,
    `shop_basket_address_invoice_additional` varchar(100) DEFAULT NULL,
    `shop_basket_address_invoice_iso_country_id` int(10) unsigned DEFAULT NULL,
    `shop_basket_address_invoice_email` varchar(100) DEFAULT NULL,
    `shop_basket_address_invoice_tel` varchar(45) DEFAULT NULL,
    `shop_basket_address_tax_id` varchar(30) DEFAULT NULL,
    `shop_basket_address_invoice_company_name` varchar(100) DEFAULT NULL,
    `shop_basket_address_invoice_company_department` varchar(100) DEFAULT NULL,
    `shop_basket_entity_comments` varchar(10000) NOT NULL DEFAULT '',
    `shop_basket_entity_rating_reminder_send` int(10) unsigned DEFAULT NULL,
    PRIMARY KEY (`shop_basket_entity_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COMMENT='table has the same names like in class XshopBasketEntity.php plus class members'

CREATE TABLE `shop_configuration` (
    `shop_configuration_id` int(10) unsigned NOT NULL,
    `shop_configuration_group` varchar(45) NOT NULL DEFAULT 'default' COMMENT 'ums gruppiert anzuzeigen',
    `shop_configuration_orderweight` int(11) NOT NULL DEFAULT '0' COMMENT 'big becomes first',
    `shop_configuration_key` varchar(100) NOT NULL,
    `shop_configuration_name` varchar(100) NOT NULL COMMENT 'display in backend',
    `shop_configuration_desc` text,
    `shop_configuration_inputtype` enum('text','textarea','radio') NOT NULL DEFAULT 'text',
    `shop_configuration_value` text NOT NULL,
    `shop_configuration_valueoptions` text COMMENT 'for inputtype= radio | select',
    PRIMARY KEY (`shop_configuration_id`),
    UNIQUE KEY `shop_configuration_name_UNIQUE` (`shop_configuration_key`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8

CREATE TABLE `shop_document_delivery` (
    `shop_document_delivery_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `shop_basket_unique` varchar(100) NOT NULL,
    `shop_document_delivery_number` varchar(100) NOT NULL COMMENT 'Die Bestellnummer',
    `shop_document_delivery_filename` varchar(200) DEFAULT NULL,
    `shop_document_delivery_path_absolute` text COMMENT 'einfach nur zur Sicherheit auch den absoluten Pfad',
    `shop_document_delivery_time` int(10) unsigned NOT NULL,
    PRIMARY KEY (`shop_document_delivery_id`),
    UNIQUE KEY `shop_document_delivery_number_UNIQUE` (`shop_document_delivery_number`),
    UNIQUE KEY `shop_basket_unique_UNIQUE` (`shop_basket_unique`)
    ) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8
CREATE TABLE `shop_document_invoice` (
    `shop_document_invoice_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `shop_basket_unique` varchar(100) NOT NULL,
    `shop_document_invoice_number` varchar(100) NOT NULL COMMENT 'Die Bestellnummer',
    `shop_document_invoice_filename` varchar(200) DEFAULT NULL,
    `shop_document_invoice_path_absolute` text COMMENT 'einfach nur zur Sicherheit auch den absoluten Pfad',
    `shop_document_invoice_time` int(10) unsigned NOT NULL,
    PRIMARY KEY (`shop_document_invoice_id`),
    UNIQUE KEY `shop_document_invoice_number_UNIQUE` (`shop_document_invoice_number`),
    UNIQUE KEY `shop_basket_unique_UNIQUE` (`shop_basket_unique`)
    ) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8
CREATE TABLE `shop_document_order` (
    `shop_document_order_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `shop_basket_unique` varchar(100) NOT NULL,
    `shop_document_order_number` varchar(100) NOT NULL COMMENT 'Die Bestellnummer',
    `shop_document_order_filename` varchar(200) DEFAULT NULL,
    `shop_document_order_path_absolute` text COMMENT 'einfach nur zur Sicherheit auch den absoluten Pfad',
    `shop_document_order_time` int(10) unsigned NOT NULL,
    PRIMARY KEY (`shop_document_order_id`),
    UNIQUE KEY `shop_basket_unique_UNIQUE` (`shop_basket_unique`),
    UNIQUE KEY `shop_document_order_number_UNIQUE` (`shop_document_order_number`)
    ) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8

CREATE TABLE `shop_prepay_number` (
    `shop_prepay_number_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `shop_prepay_number_number` varchar(100) DEFAULT NULL COMMENT 'the payment number for this service',
    PRIMARY KEY (`shop_prepay_number_id`),
    UNIQUE KEY `shop_prepay_number_number_UNIQUE` (`shop_prepay_number_number`)
    ) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8

CREATE TABLE `shop_user_group` (
    `shop_user_group_id` int(10) unsigned NOT NULL,
    `shop_user_group_name` varchar(80) NOT NULL,
    `shop_user_group_name_short` varchar(100) NOT NULL,
    PRIMARY KEY (`shop_user_group_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8

CREATE TABLE `shop_article_category_relation` (
    `idshop_article_category_relation_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `shop_article_id` int(10) unsigned NOT NULL,
    `shop_article_category_id` int(10) unsigned NOT NULL,
    PRIMARY KEY (`idshop_article_category_relation_id`),
    KEY `shop_article_category_relation_shop_article_FK` (`shop_article_id`),
    KEY `shop_article_category_relation_shop_article_category_FK` (`shop_article_category_id`),
    CONSTRAINT `shop_article_category_relation_shop_article_FK` FOREIGN KEY (`shop_article_id`) REFERENCES `shop_article` (`shop_article_id`),
    CONSTRAINT `shop_article_category_relation_shop_article_category_FK` FOREIGN KEY (`shop_article_category_id`) REFERENCES `shop_article_category` (`shop_article_category_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8

CREATE TABLE `shop_article_option_article_relation` (
    `shop_article_option_article_relation_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `shop_article_id` int(10) unsigned NOT NULL,
    `shop_article_option_def_id` int(10) unsigned NOT NULL,
    PRIMARY KEY (`shop_article_option_article_relation_id`),
    KEY `shop_article_option_article_relation_shop_article_FK` (`shop_article_id`),
    KEY `shop_article_option_article_relation_shop_article_option_def_FK` (`shop_article_option_def_id`),
    CONSTRAINT `shop_article_option_article_relation_shop_article_FK` FOREIGN KEY (`shop_article_id`) REFERENCES `shop_article` (`shop_article_id`),
    CONSTRAINT `shop_article_option_article_relation_shop_article_option_def_FK` FOREIGN KEY (`shop_article_option_def_id`) REFERENCES `shop_article_option_def` (`shop_article_option_def_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8

CREATE TABLE `shop_article_option_item_article_pricediff` (
    `shop_article_option_item_article_pricediff_id` int(11) NOT NULL AUTO_INCREMENT,
    `shop_article_id` int(10) unsigned NOT NULL,
    `shop_article_option_item_id` int(10) unsigned NOT NULL,
    `shop_article_option_item_article_pricediff_value` float NOT NULL DEFAULT '0',
    PRIMARY KEY (`shop_article_option_item_article_pricediff_id`),
    KEY `fk_shop_article_option_item_article_pricediff_optionitem_idx` (`shop_article_option_item_id`),
    KEY `fk_shop_article_option_item_article_pricediff_article_idx` (`shop_article_id`),
    CONSTRAINT `fk_shop_article_option_item_article_pricediff_article` FOREIGN KEY (`shop_article_id`) REFERENCES `shop_article` (`shop_article_id`),
    CONSTRAINT `fk_shop_article_option_item_article_pricediff_optionitem` FOREIGN KEY (`shop_article_option_item_id`) REFERENCES `shop_article_option_item` (`shop_article_option_item_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8

CREATE TABLE `shop_article_size_position` (
    `shop_article_size_position_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `shop_article_size_group_id` int(10) unsigned NOT NULL,
    `shop_article_size_position_name` varchar(100) NOT NULL,
    `shop_article_size_position_desc` text,
    `shop_article_size_position_priority` int(11) NOT NULL DEFAULT '0',
    `shop_article_size_position_order_priority` int(11) NOT NULL DEFAULT '0',
    PRIMARY KEY (`shop_article_size_position_id`),
    KEY `shop_article_size_position_shop_article_size_group_FK` (`shop_article_size_group_id`),
    CONSTRAINT `shop_article_size_position_shop_article_size_group_FK` FOREIGN KEY (`shop_article_size_group_id`) REFERENCES `shop_article_size_group` (`shop_article_size_group_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='B=Halsumfang, C=Brustlänge ...etc'

CREATE TABLE `shop_basket_address` (
    `shop_basket_address_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `shop_basket_unique` varchar(100) NOT NULL,
    `shop_basket_address_customer_type` enum('private','enterprise') NOT NULL DEFAULT 'private',
    `shop_basket_address_type` enum('invoice','shipment') NOT NULL DEFAULT 'invoice',
    `shop_basket_address_salut` varchar(20) NOT NULL DEFAULT '' COMMENT 'only invoice',
    `shop_basket_address_degree` varchar(20) NOT NULL DEFAULT '' COMMENT 'only invoice',
    `shop_basket_address_name1` varchar(100) NOT NULL,
    `shop_basket_address_name2` varchar(100) NOT NULL,
    `shop_basket_address_street` varchar(100) NOT NULL,
    `shop_basket_address_street_no` varchar(45) NOT NULL,
    `shop_basket_address_zip` varchar(10) NOT NULL,
    `shop_basket_address_city` varchar(100) NOT NULL,
    `shop_basket_address_additional` varchar(100) NOT NULL DEFAULT '',
    `iso_country_id` int(10) unsigned NOT NULL DEFAULT '48',
    `shop_basket_address_email` varchar(100) NOT NULL,
    `shop_basket_address_tel` varchar(45) NOT NULL,
    `shop_basket_address_birthday` int(11) NOT NULL DEFAULT '0' COMMENT 'only invoice & private',
    `shop_basket_address_tax_id` varchar(30) NOT NULL DEFAULT '' COMMENT 'only invoice & enterprise',
    `shop_basket_address_company_name` varchar(100) NOT NULL DEFAULT '' COMMENT 'only enterprise',
    `shop_basket_address_company_department` varchar(100) NOT NULL DEFAULT '' COMMENT 'only enterprise',
    PRIMARY KEY (`shop_basket_address_id`),
    KEY `shop_basket_address_shop_basket_FK` (`shop_basket_unique`),
    CONSTRAINT `shop_basket_address_shop_basket_FK` FOREIGN KEY (`shop_basket_unique`) REFERENCES `shop_basket` (`shop_basket_unique`)
    ) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COMMENT='buy without registration, here are the addresses'

CREATE TABLE `shop_user_address` (
    `shop_user_address_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `user_id` int(11) NOT NULL,
    `shop_user_address_customer_type` enum('private','enterprise') NOT NULL DEFAULT 'private',
    `shop_user_address_type` enum('invoice','shipment') NOT NULL DEFAULT 'invoice',
    `shop_user_address_salut` varchar(20) NOT NULL DEFAULT '' COMMENT 'only invoice',
    `shop_user_address_degree` varchar(20) NOT NULL DEFAULT '' COMMENT 'only invoice',
    `shop_user_address_name1` varchar(100) NOT NULL,
    `shop_user_address_name2` varchar(100) NOT NULL,
    `shop_user_address_street` varchar(100) NOT NULL,
    `shop_user_address_street_no` varchar(45) NOT NULL,
    `shop_user_address_zip` varchar(10) NOT NULL,
    `shop_user_address_city` varchar(100) NOT NULL,
    `shop_user_address_additional` varchar(100) NOT NULL DEFAULT '',
    `iso_country_id` int(10) unsigned NOT NULL DEFAULT '48',
    `shop_user_address_tel` varchar(45) NOT NULL,
    `shop_user_address_birthday` int(11) NOT NULL DEFAULT '0' COMMENT 'only invoice & private',
    `shop_user_address_tax_id` varchar(30) NOT NULL DEFAULT '' COMMENT 'only invoice & enterprise',
    `shop_user_address_company_name` varchar(100) NOT NULL DEFAULT '' COMMENT 'only enterprise',
    `shop_user_address_company_department` varchar(100) NOT NULL DEFAULT '' COMMENT 'only enterprise',
    PRIMARY KEY (`shop_user_address_id`),
    KEY `shop_user_address_user_FK` (`user_id`),
    CONSTRAINT `shop_user_address_user_FK` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8

CREATE TABLE `shop_article_size_item` (
    `shop_article_size_item_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `shop_article_size_def_id` int(10) unsigned NOT NULL,
    `shop_article_size_position_id` int(10) unsigned NOT NULL,
    `shop_article_size_item_from` float DEFAULT NULL,
    `shop_article_size_item_to` float DEFAULT NULL,
    PRIMARY KEY (`shop_article_size_item_id`),
    KEY `shop_article_size_item_shop_article_size_position_FK` (`shop_article_size_position_id`),
    KEY `fk_shop_article_size_item_shop_article_size_def_FK_idx` (`shop_article_size_def_id`),
    CONSTRAINT `fk_shop_article_size_item_shop_article_size_def_FK` FOREIGN KEY (`shop_article_size_def_id`) REFERENCES `shop_article_size_def` (`shop_article_size_def_id`),
    CONSTRAINT `shop_article_size_item_shop_article_size_position_FK` FOREIGN KEY (`shop_article_size_position_id`) REFERENCES `shop_article_size_position` (`shop_article_size_position_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8 COMMENT='Maßbereich (von bis)'

CREATE TABLE `shop_article_option_item_article_image_relation` (
    `shop_article_option_item_article_image_relation_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `shop_article_option_item_id` int(10) unsigned NOT NULL,
    `shop_article_id` int(10) unsigned NOT NULL,
    `bk_images_image_id` int(10) unsigned NOT NULL,
    `shop_article_option_item_article_image_relation_priority` int(11) NOT NULL DEFAULT '0',
    PRIMARY KEY (`shop_article_option_item_article_image_relation_id`),
    KEY `shop_article_option_item_article_image_relation__option_item_FK` (`shop_article_option_item_id`),
    KEY `shop_article_option_item_article_image_relation_shop_article_FK` (`shop_article_id`),
    KEY `shop_article_option_item_article_image_relation__images_image_FK` (`bk_images_image_id`),
    CONSTRAINT `shop_article_option_item_article_image_relation__images_image_FK` FOREIGN KEY (`bk_images_image_id`) REFERENCES `bk_images_image` (`bk_images_image_id`),
    CONSTRAINT `shop_article_option_item_article_image_relation__option_item_FK` FOREIGN KEY (`shop_article_option_item_id`) REFERENCES `shop_article_option_item` (`shop_article_option_item_id`),
    CONSTRAINT `shop_article_option_item_article_image_relation_shop_article_FK` FOREIGN KEY (`shop_article_id`) REFERENCES `shop_article` (`shop_article_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8

CREATE TABLE `shop_basket_discount` (
    `shop_basket_discount_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `shop_basket_discount_hash` varchar(20) NOT NULL,
    `shop_basket_discount_type` enum('unique','quantity','time','date') NOT NULL DEFAULT 'unique' COMMENT 'unique=einmalig gültig; quantity=bestimmte Anzahl gültig; time=Anzahl Tage gültig; date=bis zu Datum gültig',
    `shop_basket_discount_typevalue` varchar(100) NOT NULL COMMENT 'JSON',
    `shop_basket_discount_amount_type` enum('static','percent') NOT NULL DEFAULT 'percent',
    `shop_basket_discount_amount_typevalue` float(8,2) NOT NULL DEFAULT '1.00',
    `shop_basket_discount_min_order_amount` float(8,2) NOT NULL DEFAULT '0.00',
    `shop_basket_discount_active` int(1) NOT NULL DEFAULT '1',
    `shop_basket_discount_desc` varchar(200) NOT NULL,
    `shop_basket_discount_time` int(10) unsigned NOT NULL COMMENT 'create time',
    `user_id` int(11) DEFAULT NULL,
    `shop_basket_discount_fee_percent` float(5,2) unsigned NOT NULL DEFAULT '0.00' COMMENT 'nur wenn user_id != 0 sinnvoll',
    `shop_basket_discount_fee_static` float(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT 'nur wenn user_id != 0 sinnvoll',
    PRIMARY KEY (`shop_basket_discount_id`),
    UNIQUE KEY `shop_basket_discount_hash_UNIQUE` (`shop_basket_discount_hash`),
    KEY `shop_basket_discount_user_FK` (`user_id`),
    CONSTRAINT `shop_basket_discount_user_FK` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=422 DEFAULT CHARSET=utf8

CREATE TABLE `shop_user_data` (
    `shop_user_data_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `user_id` int(11) NOT NULL,
    `shop_user_group_id` int(10) unsigned NOT NULL DEFAULT '1',
    `shop_user_data_number_id` int(10) unsigned NOT NULL COMMENT 'die Kundennummer',
    `iso_country_id` int(10) unsigned DEFAULT NULL,
    `shop_user_data_paypal_email` varchar(100) NOT NULL DEFAULT '',
    `shop_user_data_active` int(1) unsigned NOT NULL DEFAULT '1',
    `shop_user_data_create_by_user_id` int(11) NOT NULL DEFAULT '0',
    `shop_user_data_bank_owner_name` varchar(100) DEFAULT NULL,
    `shop_user_data_bank_bank_name` varchar(100) DEFAULT NULL,
    `shop_user_data_bank_iban` varchar(45) DEFAULT NULL,
    `shop_user_data_bank_bic` varchar(45) DEFAULT NULL,
    PRIMARY KEY (`shop_user_data_id`),
    UNIQUE KEY `shop_user_data_user_id_IDX` (`user_id`) USING BTREE,
    KEY `fk_shop_user_data_user_group_id_idx` (`shop_user_group_id`),
    CONSTRAINT `fk_shop_user_data_user_group_id` FOREIGN KEY (`shop_user_group_id`) REFERENCES `shop_user_group` (`shop_user_group_id`),
    CONSTRAINT `shop_user_data_user_FK` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COMMENT='User aus db.user hat EINEN Datensatz shop_user_data'

CREATE TABLE `shop_user_data_number` (
    `shop_user_data_number_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `user_id` int(11) NOT NULL,
    `shop_user_group_id` int(10) unsigned NOT NULL,
    `shop_user_data_number_int` int(10) unsigned NOT NULL,
    `shop_user_data_number_varchar` varchar(60) NOT NULL,
    PRIMARY KEY (`shop_user_data_number_id`),
    KEY `shop_user_data_number_user_FK` (`user_id`),
    CONSTRAINT `shop_user_data_number_user_FK` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8

CREATE TABLE `shop_user_fee` (
    `shop_user_fee_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `user_id` int(11) NOT NULL,
    `shop_user_fee_percent` float(5,2) unsigned NOT NULL DEFAULT '0.00',
    `shop_user_fee_coupon_percent` float(5,2) unsigned NOT NULL DEFAULT '0.00',
    `shop_user_fee_static` float(10,2) unsigned NOT NULL DEFAULT '0.00',
    `shop_user_fee_mlm_parent_user_id` int(11) DEFAULT NULL COMMENT 'Multi Level Marketing: provisionsberechtigter User',
    `shop_user_fee_mlm_child_fee_percent` float(5,2) unsigned NOT NULL DEFAULT '0.00',
    `shop_user_fee_mlm_child_fee_percent_first` float(5,2) unsigned NOT NULL DEFAULT '0.00',
    `shop_user_fee_mlm_child_fee_coupon_percent` float(5,2) unsigned NOT NULL DEFAULT '0.00',
    PRIMARY KEY (`shop_user_fee_id`),
    UNIQUE KEY `user_id_UNIQUE` (`user_id`),
    CONSTRAINT `shop_user_fee_user_FK` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8

CREATE TABLE `shop_user_fee_paid` (
    `shop_user_fee_paid_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `user_id` int(11) NOT NULL,
    `shop_basket_entity_id` int(10) unsigned NOT NULL,
    `shop_user_fee_paid_type` enum('sale','discount','mlm') NOT NULL DEFAULT 'sale',
    `shop_user_fee_paid_sum` float(10,2) NOT NULL DEFAULT '0.00',
    `shop_user_fee_paid_time` int(10) unsigned NOT NULL,
    `shop_user_fee_paid_payment_id` varchar(100) NOT NULL DEFAULT '',
    `shop_user_fee_paid_payment_method` enum('other','paypal') NOT NULL DEFAULT 'other',
    PRIMARY KEY (`shop_user_fee_paid_id`),
    UNIQUE KEY `shop_basket_entity_id_UNIQUE` (`shop_basket_entity_id`),
    KEY `shop_user_fee_paid_user_FK` (`user_id`),
    CONSTRAINT `shop_user_fee_paid_user_FK` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8
