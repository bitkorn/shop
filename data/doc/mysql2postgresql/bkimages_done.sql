
CREATE TABLE `bk_images_image` (
    `bk_images_image_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `bk_images_imagegroup_id` int(10) unsigned NOT NULL,
    `bk_images_image_title` varchar(200) NOT NULL,
    `bk_images_image_desc` text,
    `bk_images_image_desc_intern` varchar(200) NOT NULL DEFAULT '',
    `bk_images_image_filename` tinytext NOT NULL,
    `bk_images_image_extension` varchar(10) NOT NULL,
    `bk_images_image_priority` int(11) NOT NULL DEFAULT '1',
    `bk_images_image_scaling` text NOT NULL COMMENT 'JSON {"magicTwoSquare":[1024,32,16],"fullHd":1,"sixteenToNine":[60,50,40,30,20,10,8,6],"custom":["42x27"]}',
    `bk_images_image_time_create` int(10) unsigned NOT NULL,
    PRIMARY KEY (`bk_images_image_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8
CREATE TABLE `bk_images_imagegroup` (
    `bk_images_imagegroup_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `bk_images_imagegroup_name` varchar(100) NOT NULL,
    `bk_images_imagegroup_priority` int(11) NOT NULL DEFAULT '1',
    PRIMARY KEY (`bk_images_imagegroup_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8

CREATE TABLE `bk_images_image_noscale` (
    `bk_images_image_noscale_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `bk_images_imagegroup_id` int(10) unsigned NOT NULL,
    `bk_images_image_noscale_title` varchar(200) NOT NULL,
    `bk_images_image_noscale_desc` text,
    `bk_images_image_noscale_desc_intern` varchar(200) NOT NULL DEFAULT '',
    `bk_images_image_noscale_filename` tinytext NOT NULL,
    `bk_images_image_noscale_extension` varchar(10) NOT NULL,
    `bk_images_image_noscale_priority` int(11) NOT NULL DEFAULT '1',
    `bk_images_image_noscale_scaling` varchar(100) NOT NULL COMMENT 'width and height separated with x',
    `bk_images_image_noscale_time_create` int(10) unsigned NOT NULL,
    PRIMARY KEY (`bk_images_image_noscale_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8


CREATE TABLE `bk_images_slider` (
    `bk_images_slider_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `bk_images_slider_title` tinytext NOT NULL,
    `bk_images_slider_desc` text NOT NULL,
    `bk_images_slider_filename` tinytext NOT NULL,
    `bk_images_slider_filename_thumb` tinytext NOT NULL,
    `bk_images_slider_extension` varchar(10) NOT NULL,
    `bk_images_slider_time_create` int(10) unsigned NOT NULL,
    PRIMARY KEY (`bk_images_slider_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8
