# Provisionen
Rabatt-Codes können **personalisiert** werden. Dann sind sie einem bestimmten User zugeordnet.

## Provisionsrechnugsarten
- **%** Provision vom Netto Warenkorb-Wert
- **fester Wert** auf bestimmte Produkte

###fester Wert
Bei jedem User in den Provisionseinstellungen (% €) rechts von **bestimmte Produkte** wird der feste Wert angegeben.

###bestimmte Produkte
Ob ein **Produkt für Provisionen aktiv** ist kann in den Stammdaten jedes einzelnen Produkts geändert werden.

## Provisionsberechtigte (Ranking = salesstaff)
- Vertriebler
- Veterinär

Provisionsberechtigter wird man wenn in der Userkonfiguration das Ranking `101 salesstaff` angehakt ist.

## zu provisionieren
Verkauf = Kauf, POS, Rabatt-Code

- Verkauf bestimmter Produkte
- Verkauf Warenkorb
- MLM-Kunden: Kauf
	- erster Kauf
	- weitere Käufe
- MLM-Kunden: Rabatt-Codes

**Anmerkung**: Der erste Kauf eines MLM-Kunden bezieht sich nur auf Kauf, nicht auf Kauf mit Rabatt-Code?

**Anmerkung**: Besteht der erste Kauf eines MLM-Kunden aus mehreren gleichen Artikeln (auch die gleichen Optionen (Größe & Farbe), so wird die Provision auf die Gesamtsumme der Artikel gerechnet.

### Hirarchie (Priorität absteigend sortiert)
1. Verkauf bestimmte Produkte
2. Verkauf Warenkorb
3. Verkauf Rabatt-Code

**Verkauf bestimmte Produkte** funktioniert nur wenn bei dem User in den Provisionseinstellungen unter **bestimmte Produkte** ein Wert angegeben und das **Produkt für Provisionen aktiv** ist. Ist beides gegeben werden die anderen Provisionen -für diesen Artikel- nicht berechnet.
  Die anderen Artikel im Warenkorb werden nach den anderen Provisionen (**Verkauf Warenkorb** & **Verkauf Rabatt-Code**) provisioniert.

Auch wenn ein personalisierter Rabatt-Code angewandt wurde, wird die Provision auf **Verkauf Warenkorb** (nicht **Verkauf Rabatt-Code**) verrechnet falls der Käufer selber für **Verkauf Warenkorb** eingestellt ist.

### Provisionsfälle Systemgurt

MLM-Kunden sind z.B. Veterinär und haben keinen Zugang zum **POS**. Somit können sie nur selber kaufen (**Kauf**) oder über **Rabatt-Code** verkaufen.

MLM-Kunden muß in der Provisions-Adminsicht ein **Provisions Berechtigter (MLM parent)** zugeordnet werden. Erst damit wird er zum MLM-Kunden! Wenn ein Vertriebler einen neuen Kunden anlegt geschieht das automatisch (Provisions Berechtigter ist Vertriebler).

#### Fall: Kunde mit Rabatt-Code eines MLM-Kunden von einem Vertriebler
- Kunde -> Rabatt durch Rabatt-Code (personalisiert für MLM-Kunden)
- MLM Kunde -> fester Wert Provision (falls eines der bestimmten Produkte gekauft wurde), sonst Rabatt-Code Provision
- Vertrieb -> Provision auf Warenkorb

## Screenshots
### Provisionseinstellungen eines Users
![Shop Provisionen Screenshot][shop-provision]

[shop-provision]:shop-provisionen.jpg