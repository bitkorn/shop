<?php

namespace Bitkorn\ShopTest\Controller\Frontend\Basket;

use Laminas\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

class BasketControllerTest extends AbstractHttpControllerTestCase
{
    public function setUp()
    {
        $this->setApplicationConfig(
            include __DIR__ . '/../../../../../../../config/application.config.php'
        );
        parent::setUp();
    }

}
