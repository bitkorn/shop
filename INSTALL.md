## System Requirements
- PHP ^7.3
- Bitkorn/User
- Bitkorn/Trinket
- Bitkorn/ShopPrepay
    - wegen POS Bestellungen
- Bitkorn/Images

## install

- `mkdir /data/files/documents`
- `chmod 775 /data/files/documents`
- put agb.pdf into `/data/files/documents`

- apt install php-imagick
- `composer require html2text/html2text`

- chmod $config['bitkorn_shop']['image_path_absolute']
