# Laminas module bitkorn/shop

- [ ] Versandkosten UPS
  - [ ] UPS ShipAccept bei BasketOrdered (in basketOrdered() ein Event triggern)
  - [ ] UPS Kosten == 0 THEN nicht im Basket anzeigen (also, wenn es einen Fehler beim UPS request gab)

- [ ] article_stock_batch (Charge) in das BasketEntity bei Lieferschein Erstellung
 - [ ] also im BasketFinalizeService

- [ ] Artikel READ_ONLY wegen Stock und Parametern/Optionen/etc

- [ ] remove usages of db.shop_article.shop_article_shipping_costs_single!?!?

- POS
  - param2d & lengthcut
  - /public/js/app/article-option-pricediff.js & /public/js/app/admin/basket-pos.js
    - vertragen Preisunterschiede nur in einem Select
        - Preisunterschiede in mehreren Selects müssten sich miteinander verrechnen

## Größen Zeug

- add Größen-Gruppen Form
    - delete Größen-Gruppen (mit Warnung)

- Hundegrößen-Liste im User-Backend
    - so viele wie will mit Titel speichern
    - den letzten ohne Titel (wenn eingeloggt)

## tec Zeug

- datenbankname global setzen/holen in Tablex's

- JS aus Forms (z.B. Adresse) eliminieren

- DoS Angriffe abwehren
    - bzw. massig Aufrufe pro Zeiteinheit
        - z.B. bei der Rabatt-Code Valid-Prüfung (im BasketController)

### später

- Artikel-Größen GUI
    - DELETE Trigger/Observer Produkt-Optionen

- Internationalisierung
    - Währung
    - Zahlenformat
    - Umsatzsteuer

- Artikel-Attribute
    - CPU MHz, HDD Größe
    - Filter für frontend
