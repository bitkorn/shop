<?php

namespace Bitkorn\Shop;

use Bitkorn\Shop\Controller\Admin\Article\ArticleAdminController;
use Bitkorn\Shop\Controller\Admin\Article\ArticleImageAdminController;
use Bitkorn\Shop\Controller\Admin\Article\Category\ArticleCategoryAdminController;
use Bitkorn\Shop\Controller\Admin\Article\Option\ArticleOptionAdminController;
use Bitkorn\Shop\Controller\Admin\Article\Size\ArticleSizeAdminController;
use Bitkorn\Shop\Controller\Admin\Article\TypeDelivery\ArticleLengthcutController;
use Bitkorn\Shop\Controller\Admin\Backup\BackupController;
use Bitkorn\Shop\Controller\Admin\Basket\BasketAdminController;
use Bitkorn\Shop\Controller\Admin\Basket\BasketDiscountAdminController;
use Bitkorn\Shop\Controller\Admin\Basket\Claim\ClaimAdminController;
use Bitkorn\Shop\Controller\Admin\Basket\Rating\RatingAdminController;
use Bitkorn\Shop\Controller\Admin\Config\ShopConfigAdminController;
use Bitkorn\Shop\Controller\Admin\DashboardAdminController;
use Bitkorn\Shop\Controller\Admin\User\Fee\ShopUserFeeAdminController;
use Bitkorn\Shop\Controller\Admin\User\ShopUserAdminController;
use Bitkorn\Shop\Controller\Admin\Vendor\VendorAdminController;
use Bitkorn\Shop\Controller\Ajax\Article\TypeDeliveryAjaxController;
use Bitkorn\Shop\Controller\Ajaxhelper\Admin\AjaxArticleImageAdminController;
use Bitkorn\Shop\Controller\Ajaxhelper\Admin\AjaxArticleOptionArticleImageAdminController;
use Bitkorn\Shop\Controller\Ajaxhelper\Admin\AjaxArticleSizeAdminController;
use Bitkorn\Shop\Controller\Ajaxhelper\Admin\AjaxBasketAdminController;
use Bitkorn\Shop\Controller\Ajaxhelper\Admin\AjaxShopConfigAdminController;
use Bitkorn\Shop\Controller\Ajaxhelper\Admin\AjaxUserAdminController;
use Bitkorn\Shop\Controller\Ajaxhelper\AjaxAnonymusController;
use Bitkorn\Shop\Controller\Ajaxhelper\AjaxArticleController;
use Bitkorn\Shop\Controller\Ajaxhelper\AjaxArticleOptionController;
use Bitkorn\Shop\Controller\Ajaxhelper\AjaxBasketController;
use Bitkorn\Shop\Controller\Ajaxhelper\AjaxUserController;
use Bitkorn\Shop\Controller\Ajaxhelper\Statistic\AjaxStatisticSalesController;
use Bitkorn\Shop\Controller\Ajaxhelper\Statistic\AjaxStatisticSizeArchiveController;
use Bitkorn\Shop\Controller\Common\BasketuniquePdfController;
use Bitkorn\Shop\Controller\Common\PdfController;
use Bitkorn\Shop\Controller\Console\CronController;
use Bitkorn\Shop\Controller\Frontend\Article\ArticleFrontendController;
use Bitkorn\Shop\Controller\Frontend\Article\Size\ArticleSizeFrontendController;
use Bitkorn\Shop\Controller\Frontend\Basket\BasketController;
use Bitkorn\Shop\Controller\Frontend\Content\ShopContentController;
use Bitkorn\Shop\Controller\Frontend\User\UserBackendController;
use Bitkorn\Shop\Controller\Pos\SellController;
use Bitkorn\Shop\Entity\Article\ShopArticleOptionsEntity;
use Bitkorn\Shop\Factory\Controller\Admin\Article\ArticleAdminControllerFactory;
use Bitkorn\Shop\Factory\Controller\Admin\Article\ArticleImageAdminControllerFactory;
use Bitkorn\Shop\Factory\Controller\Admin\Article\Category\ArticleCategoryAdminControllerFactory;
use Bitkorn\Shop\Factory\Controller\Admin\Article\Option\ArticleOptionAdminControllerFactory;
use Bitkorn\Shop\Factory\Controller\Admin\Article\Size\ArticleSizeAdminControllerFactory;
use Bitkorn\Shop\Factory\Controller\Admin\Article\TypeDelivery\ArticleLengthcutControllerFactory;
use Bitkorn\Shop\Factory\Controller\Admin\Backup\BackupControllerFactory;
use Bitkorn\Shop\Factory\Controller\Admin\Basket\BasketAdminControllerFactory;
use Bitkorn\Shop\Factory\Controller\Admin\Basket\BasketDiscountAdminControllerFactory;
use Bitkorn\Shop\Factory\Controller\Admin\Basket\Claim\ClaimAdminControllerFactory;
use Bitkorn\Shop\Factory\Controller\Admin\Basket\Rating\RatingAdminControllerFactory;
use Bitkorn\Shop\Factory\Controller\Admin\Config\ShopConfigAdminControllerFactory;
use Bitkorn\Shop\Factory\Controller\Admin\DashboardAdminControllerFactory;
use Bitkorn\Shop\Factory\Controller\Admin\User\Fee\ShopUserFeeAdminControllerFactory;
use Bitkorn\Shop\Factory\Controller\Admin\User\ShopUserAdminControllerFactory;
use Bitkorn\Shop\Factory\Controller\Admin\Vendor\VendorAdminControllerFactory;
use Bitkorn\Shop\Factory\Controller\Ajax\Article\TypeDeliveryAjaxControllerFactory;
use Bitkorn\Shop\Factory\Controller\Ajaxhelper\Admin\AjaxArticleImageAdminControllerFactory;
use Bitkorn\Shop\Factory\Controller\Ajaxhelper\Admin\AjaxArticleOptionArticleImageAdminControllerFactory;
use Bitkorn\Shop\Factory\Controller\Ajaxhelper\Admin\AjaxArticleSizeAdminControllerFactory;
use Bitkorn\Shop\Factory\Controller\Ajaxhelper\Admin\AjaxBasketAdminControllerFactory;
use Bitkorn\Shop\Factory\Controller\Ajaxhelper\Admin\AjaxShopConfigAdminControllerFactory;
use Bitkorn\Shop\Factory\Controller\Ajaxhelper\Admin\AjaxUserAdminControllerFactory;
use Bitkorn\Shop\Factory\Controller\Ajaxhelper\AjaxAnonymusControllerFactory;
use Bitkorn\Shop\Factory\Controller\Ajaxhelper\AjaxArticleControllerFactory;
use Bitkorn\Shop\Factory\Controller\Ajaxhelper\AjaxArticleOptionControllerFactory;
use Bitkorn\Shop\Factory\Controller\Ajaxhelper\AjaxBasketControllerFactory;
use Bitkorn\Shop\Factory\Controller\Ajaxhelper\AjaxUserControllerFactory;
use Bitkorn\Shop\Factory\Controller\Ajaxhelper\Statistic\AjaxStatisticSalesControllerFactory;
use Bitkorn\Shop\Factory\Controller\Ajaxhelper\Statistic\AjaxStatisticSizeArchiveControllerFactory;
use Bitkorn\Shop\Factory\Controller\Common\BasketuniquePdfControllerFactory;
use Bitkorn\Shop\Factory\Controller\Common\PdfControllerFactory;
use Bitkorn\Shop\Factory\Controller\Frontend\Article\ArticleFrontendControllerFactory;
use Bitkorn\Shop\Factory\Controller\Frontend\Article\Size\ArticleSizeFrontendControllerFactory;
use Bitkorn\Shop\Factory\Controller\Frontend\Basket\BasketControllerFactory;
use Bitkorn\Shop\Factory\Controller\Frontend\Content\ShopContentControllerFactory;
use Bitkorn\Shop\Factory\Controller\Frontend\User\UserBackendControllerFactory;
use Bitkorn\Shop\Factory\Controller\Pos\SellControllerFactory;
use Bitkorn\Shop\Factory\Form\Article\Category\ShopArticleCategoryFormFactory;
use Bitkorn\Shop\Factory\Form\Article\ShopArticleFormFactory;
use Bitkorn\Shop\Factory\Form\Article\ShopArticleImageEditFormFactory;
use Bitkorn\Shop\Factory\Form\Article\ShopArticleImageFormFactory;
use Bitkorn\Shop\Factory\Form\Article\ShopArticleSizeGroupFormFactory;
use Bitkorn\Shop\Factory\Form\Article\ShopArticleStockFormFactory;
use Bitkorn\Shop\Factory\Form\Article\TypeDelivery\ShopArticleLengthcutDefFormFactory;
use Bitkorn\Shop\Factory\Form\Basket\Claim\ShopBasketClaimFileFormFactory;
use Bitkorn\Shop\Factory\Form\Basket\Claim\ShopBasketClaimFormFactory;
use Bitkorn\Shop\Factory\Form\Basket\ShopBasketAddressFormFactory;
use Bitkorn\Shop\Factory\Form\Basket\ShopBasketDiscountFormFactory;
use Bitkorn\Shop\Factory\Form\User\Bank\ShopUserBankFormFactory;
use Bitkorn\Shop\Factory\Form\User\Fee\ShopUserFeeFormFactory;
use Bitkorn\Shop\Factory\Form\User\ShopUserAddressFormFactory;
use Bitkorn\Shop\Factory\Form\User\ShopUserCreateFormFactory;
use Bitkorn\Shop\Factory\Form\User\ShopUserFormFactory;
use Bitkorn\Shop\Factory\Form\Vendor\ShopVendorFormFactory;
use Bitkorn\Shop\Factory\Listener\UserDeleteListenerFactory;
use Bitkorn\Shop\Factory\Observer\ImageDeleteObserverFactory;
use Bitkorn\Shop\Factory\Listener\UserRegisterListenerFactory;
use Bitkorn\Shop\Factory\Service\Address\ShopAddressServiceFactory;
use Bitkorn\Shop\Factory\Service\Article\ShopArticleImageServiceFactory;
use Bitkorn\Shop\Factory\Service\Article\ShopArticleOptionServiceFactory;
use Bitkorn\Shop\Factory\Service\Article\ShopArticleParam2dServiceFactory;
use Bitkorn\Shop\Factory\Service\Article\ShopArticleServiceFactory;
use Bitkorn\Shop\Factory\Service\Article\ShopArticleSizeServiceFactory;
use Bitkorn\Shop\Factory\Service\Article\ShopArticleStockServiceFactory;
use Bitkorn\Shop\Factory\Service\Article\ShopArticleTypeDeliveryServiceFactory;
use Bitkorn\Shop\Factory\Service\Article\TypeDelivery\ShopArticleLengthcutDefServiceFactory;
use Bitkorn\Shop\Factory\Service\Basket\BasketAdminServiceFactory;
use Bitkorn\Shop\Factory\Service\Basket\BasketFinalizeServiceFactory;
use Bitkorn\Shop\Factory\Service\Basket\BasketServiceFactory;
use Bitkorn\Shop\Factory\Service\Claim\ShopClaimServiceFactory;
use Bitkorn\Shop\Factory\Service\Content\ShopContentServiceFactory;
use Bitkorn\Shop\Factory\Service\Document\DocumentDeliveryServiceFactory;
use Bitkorn\Shop\Factory\Service\Document\DocumentInvoiceServiceFactory;
use Bitkorn\Shop\Factory\Service\Document\DocumentOrderServiceFactory;
use Bitkorn\Shop\Factory\Service\Quantityunit\QuantityunitServiceFactory;
use Bitkorn\Shop\Factory\Service\Routes\ConfigRoutesServiceFactory;
use Bitkorn\Shop\Factory\Service\Shipping\ShippingServiceFactory;
use Bitkorn\Shop\Factory\Service\ShopServiceFactory;
use Bitkorn\Shop\Factory\Service\User\ShopUserFeeServiceFactory;
use Bitkorn\Shop\Factory\Service\User\ShopUserNumberServiceFactory;
use Bitkorn\Shop\Factory\Service\User\ShopUserServiceFactory;
use Bitkorn\Shop\Factory\Service\Vendor\ShopVendorServiceFactory;
use Bitkorn\Shop\Factory\Service\Vendor\VendorNumberServiceFactory;
use Bitkorn\Shop\Factory\Table\Article\Option\ShopArticleOptionArticleRelationTableFactory;
use Bitkorn\Shop\Factory\Table\Article\Option\ShopArticleOptionDefTableFactory;
use Bitkorn\Shop\Factory\Table\Article\Option\ShopArticleOptionItemArticleImageTableFactory;
use Bitkorn\Shop\Factory\Table\Article\Option\ShopArticleOptionItemArticlePricediffTableFactory;
use Bitkorn\Shop\Factory\Table\Article\Option\ShopArticleOptionItemTableFactory;
use Bitkorn\Shop\Factory\Table\Article\Param2d\ShopArticleParam2dItemTableFactory;
use Bitkorn\Shop\Factory\Table\Article\Param2d\ShopArticleParam2dTableFactory;
use Bitkorn\Shop\Factory\Table\Article\ShopArticleCategoryRelationTableFactory;
use Bitkorn\Shop\Factory\Table\Article\ShopArticleCategoryTableFactory;
use Bitkorn\Shop\Factory\Table\Article\ShopArticleClassTableFactory;
use Bitkorn\Shop\Factory\Table\Article\ShopArticleGroupRelationTableFactory;
use Bitkorn\Shop\Factory\Table\Article\ShopArticleImageTableFactory;
use Bitkorn\Shop\Factory\Table\Article\ShopArticleRatingTableFactory;
use Bitkorn\Shop\Factory\Table\Article\ShopArticleSizeGroupRelationTableFactory;
use Bitkorn\Shop\Factory\Table\Article\ShopArticleStockTableFactory;
use Bitkorn\Shop\Factory\Table\Article\ShopArticleTableFactory;
use Bitkorn\Shop\Factory\Table\Article\Size\ShopArticleSizeArchiveTableFactory;
use Bitkorn\Shop\Factory\Table\Article\Size\ShopArticleSizeDefTableFactory;
use Bitkorn\Shop\Factory\Table\Article\Size\ShopArticleSizeGroupTableFactory;
use Bitkorn\Shop\Factory\Table\Article\Size\ShopArticleSizeItemTableFactory;
use Bitkorn\Shop\Factory\Table\Article\Size\ShopArticleSizePositionTableFactory;
use Bitkorn\Shop\Factory\Table\Article\TypeDelivery\ShopArticleLengthcutDefTableFactory;
use Bitkorn\Shop\Factory\Table\Basket\Claim\ShopBasketClaimTableFactory;
use Bitkorn\Shop\Factory\Table\Basket\Discount\ShopBasketDiscountTableFactory;
use Bitkorn\Shop\Factory\Table\Basket\ShopBasketAddressTableFactory;
use Bitkorn\Shop\Factory\Table\Basket\ShopBasketEntityTableFactory;
use Bitkorn\Shop\Factory\Table\Basket\ShopBasketItemTableFactory;
use Bitkorn\Shop\Factory\Table\Basket\ShopBasketTableFactory;
use Bitkorn\Shop\Factory\Table\Common\ShopConfigurationTableFactory;
use Bitkorn\Shop\Factory\Table\Content\ShopContentTableFactory;
use Bitkorn\Shop\Factory\Table\Document\ShopDocumentDeliveryTableFactory;
use Bitkorn\Shop\Factory\Table\Document\ShopDocumentInvoiceTableFactory;
use Bitkorn\Shop\Factory\Table\Document\ShopDocumentOrderTableFactory;
use Bitkorn\Shop\Factory\Table\User\ShopUserAddressTableFactory;
use Bitkorn\Shop\Factory\Table\User\ShopUserDataNumberTableFactory;
use Bitkorn\Shop\Factory\Table\User\ShopUserDataTableFactory;
use Bitkorn\Shop\Factory\Table\User\ShopUserFeePaidTableFactory;
use Bitkorn\Shop\Factory\Table\User\ShopUserFeeTableFactory;
use Bitkorn\Shop\Factory\Table\User\ShopUserGroupTableFactory;
use Bitkorn\Shop\Factory\Table\Vendor\ShopVendorGroupTableFactory;
use Bitkorn\Shop\Factory\Table\Vendor\ShopVendorTableFactory;
use Bitkorn\Shop\Factory\Tablex\Article\Option\ShopArticleOptionTablexFactory;
use Bitkorn\Shop\Factory\Tablex\Article\ShopArticleImageTablexFactory;
use Bitkorn\Shop\Factory\Tablex\Article\ShopArticleStockTablexFactory;
use Bitkorn\Shop\Factory\Tablex\Article\ShopArticleTablexFactory;
use Bitkorn\Shop\Factory\Tablex\Article\Size\ShopArticleSizeTablexFactory;
use Bitkorn\Shop\Factory\Tablex\Basket\ShopBasketEntityTablexFactory;
use Bitkorn\Shop\Factory\Tablex\Basket\ShopBasketTablexFactory;
use Bitkorn\Shop\Factory\Tablex\User\ShopUserTablexFactory;
use Bitkorn\Shop\Factory\Tablex\Vendor\ShopVendorTablexFactory;
use Bitkorn\Shop\Factory\View\Helper\Address\AddressDisplayFactory;
use Bitkorn\Shop\Factory\View\Helper\Article\ArticleDescVariableFactory;
use Bitkorn\Shop\Factory\View\Helper\Article\Category\ShopArticleCategorySelectFactory;
use Bitkorn\Shop\Factory\View\Helper\Article\Category\ShopArticleCategoryUnorderedListFactory;
use Bitkorn\Shop\Factory\View\Helper\Article\Frontend\ShopArticleCategoryUrlFactory;
use Bitkorn\Shop\Factory\View\Helper\Article\Frontend\ShopArticlePreviewFactory;
use Bitkorn\Shop\Factory\View\Helper\Article\Frontend\ShopArticlePreviewImageFactory;
use Bitkorn\Shop\Factory\View\Helper\Article\Frontend\ShopArticleTypeUrlFactory;
use Bitkorn\Shop\Factory\View\Helper\Article\Option\ShopArticleOptionFactory;
use Bitkorn\Shop\Factory\View\Helper\Article\Option\ShopArticleOptionsByArticleIdFactory;
use Bitkorn\Shop\Factory\View\Helper\Article\Option\ShopArticleOptionsFromJsonFactory;
use Bitkorn\Shop\Factory\View\Helper\Article\Param2d\ArticleParam2dItemDescFactory;
use Bitkorn\Shop\Factory\View\Helper\Article\Param2d\ArticleParam2dsSelectFactory;
use Bitkorn\Shop\Factory\View\Helper\Article\Rating\ArticleRatingsFactory;
use Bitkorn\Shop\Factory\View\Helper\Article\ShopArticleImageRelativeFilenameFactory;
use Bitkorn\Shop\Factory\View\Helper\Article\ShopArticleSefurlFactory;
use Bitkorn\Shop\Factory\View\Helper\Article\Stock\ShopArticleStockWidgetFactory;
use Bitkorn\Shop\Factory\View\Helper\Article\TypeDelivery\ArticleLengthcutOptionsDescFactory;
use Bitkorn\Shop\Factory\View\Helper\Basket\Admin\BasketStatusSelectFactory;
use Bitkorn\Shop\Factory\View\Helper\Basket\Email\Confirmation\ShopBasketOrderedFactory;
use Bitkorn\Shop\Factory\View\Helper\Basket\Email\Confirmation\ShopBasketPayedFactory;
use Bitkorn\Shop\Factory\View\Helper\Basket\Email\Rating\RatingReminderFactory;
use Bitkorn\Shop\Factory\View\Helper\Basket\Email\ShopBasketShippedFactory;
use Bitkorn\Shop\Factory\View\Helper\Basket\Email\ShopBasketSummaryFactory;
use Bitkorn\Shop\Factory\View\Helper\Basket\Frontend\ShopBasketOverviewFactory;
use Bitkorn\Shop\Factory\View\Helper\Document\ShopDocumentIdByBasketUniqueFactory;
use Bitkorn\Shop\Factory\View\Helper\Form\DirectEditInputFactory;
use Bitkorn\Shop\Factory\View\Helper\IdAssoc\Article\ShopArticleSizeDefIdAssocFactory;
use Bitkorn\Shop\Factory\View\Helper\IdAssoc\Vendor\ShopVendorIdAssocFactory;
use Bitkorn\Shop\Factory\View\Helper\Quantityunit\QuantityunitToOriginFactory;
use Bitkorn\Shop\Form\Article\Category\ShopArticleCategoryForm;
use Bitkorn\Shop\Form\Article\ShopArticleForm;
use Bitkorn\Shop\Form\Article\ShopArticleImageEditForm;
use Bitkorn\Shop\Form\Article\ShopArticleImageForm;
use Bitkorn\Shop\Form\Article\ShopArticleSizeGroupForm;
use Bitkorn\Shop\Form\Article\ShopArticleStockForm;
use Bitkorn\Shop\Form\Article\TypeDelivery\ShopArticleLengthcutDefForm;
use Bitkorn\Shop\Form\Basket\Claim\ShopBasketClaimFileForm;
use Bitkorn\Shop\Form\Basket\Claim\ShopBasketClaimForm;
use Bitkorn\Shop\Form\Basket\ShopBasketAddressForm;
use Bitkorn\Shop\Form\Basket\ShopBasketDiscountForm;
use Bitkorn\Shop\Form\Element\Password;
use Bitkorn\Shop\Form\User\Bank\ShopUserBankForm;
use Bitkorn\Shop\Form\User\Fee\ShopUserFeeForm;
use Bitkorn\Shop\Form\User\RegisterForm;
use Bitkorn\Shop\Form\User\ShopUserAddressForm;
use Bitkorn\Shop\Form\User\ShopUserCreateForm;
use Bitkorn\Shop\Form\User\ShopUserForm;
use Bitkorn\Shop\Form\Vendor\ShopVendorForm;
use Bitkorn\Shop\Listener\UserDeleteListener;
use Bitkorn\Shop\Observer\ImageDeleteObserver;
use Bitkorn\Shop\Listener\UserRegisterListener;
use Bitkorn\Shop\Service\Address\ShopAddressService;
use Bitkorn\Shop\Service\Article\ShopArticleImageService;
use Bitkorn\Shop\Service\Article\ShopArticleOptionService;
use Bitkorn\Shop\Service\Article\ShopArticleParam2dService;
use Bitkorn\Shop\Service\Article\ShopArticleService;
use Bitkorn\Shop\Service\Article\ShopArticleSizeService;
use Bitkorn\Shop\Service\Article\ShopArticleStockService;
use Bitkorn\Shop\Service\Article\ShopArticleTypeDeliveryService;
use Bitkorn\Shop\Service\Article\TypeDelivery\ShopArticleLengthcutDefService;
use Bitkorn\Shop\Service\Basket\BasketAdminService;
use Bitkorn\Shop\Service\Basket\BasketFinalizeService;
use Bitkorn\Shop\Service\Basket\BasketService;
use Bitkorn\Shop\Service\Claim\ShopClaimService;
use Bitkorn\Shop\Service\Content\ShopContentService;
use Bitkorn\Shop\Service\Document\DocumentDeliveryService;
use Bitkorn\Shop\Service\Document\DocumentInvoiceService;
use Bitkorn\Shop\Service\Document\DocumentOrderService;
use Bitkorn\Shop\Service\Quantityunit\QuantityunitService;
use Bitkorn\Shop\Service\Routes\ConfigRoutesService;
use Bitkorn\Shop\Service\Shipping\ShippingProvider\ShippingProviderCustomerPickup;
use Bitkorn\Shop\Service\Shipping\ShippingService\ShippingService;
use Bitkorn\Shop\Service\ShopService;
use Bitkorn\Shop\Service\User\ShopUserFeeService;
use Bitkorn\Shop\Service\User\ShopUserNumberService;
use Bitkorn\Shop\Service\User\ShopUserService;
use Bitkorn\Shop\Service\Vendor\ShopVendorService;
use Bitkorn\Shop\Service\Vendor\VendorNumberService;
use Bitkorn\Shop\Table\Article\Option\ShopArticleOptionArticleRelationTable;
use Bitkorn\Shop\Table\Article\Option\ShopArticleOptionDefTable;
use Bitkorn\Shop\Table\Article\Option\ShopArticleOptionItemArticleImageTable;
use Bitkorn\Shop\Table\Article\Option\ShopArticleOptionItemArticlePricediffTable;
use Bitkorn\Shop\Table\Article\Option\ShopArticleOptionItemTable;
use Bitkorn\Shop\Table\Article\Param2d\ShopArticleParam2dItemTable;
use Bitkorn\Shop\Table\Article\Param2d\ShopArticleParam2dTable;
use Bitkorn\Shop\Table\Article\ShopArticleCategoryRelationTable;
use Bitkorn\Shop\Table\Article\ShopArticleCategoryTable;
use Bitkorn\Shop\Table\Article\ShopArticleClassTable;
use Bitkorn\Shop\Table\Article\ShopArticleGroupRelationTable;
use Bitkorn\Shop\Table\Article\ShopArticleImageTable;
use Bitkorn\Shop\Table\Article\ShopArticleRatingTable;
use Bitkorn\Shop\Table\Article\ShopArticleSizeGroupRelationTable;
use Bitkorn\Shop\Table\Article\ShopArticleStockTable;
use Bitkorn\Shop\Table\Article\ShopArticleTable;
use Bitkorn\Shop\Table\Article\Size\ShopArticleSizeArchiveTable;
use Bitkorn\Shop\Table\Article\Size\ShopArticleSizeDefTable;
use Bitkorn\Shop\Table\Article\Size\ShopArticleSizeGroupTable;
use Bitkorn\Shop\Table\Article\Size\ShopArticleSizeItemTable;
use Bitkorn\Shop\Table\Article\Size\ShopArticleSizePositionTable;
use Bitkorn\Shop\Table\Article\TypeDelivery\ShopArticleLengthcutDefTable;
use Bitkorn\Shop\Table\Basket\Claim\ShopBasketClaimTable;
use Bitkorn\Shop\Table\Basket\Discount\ShopBasketDiscountTable;
use Bitkorn\Shop\Table\Basket\ShopBasketAddressTable;
use Bitkorn\Shop\Table\Basket\ShopBasketEntityTable;
use Bitkorn\Shop\Table\Basket\ShopBasketItemTable;
use Bitkorn\Shop\Table\Basket\ShopBasketTable;
use Bitkorn\Shop\Table\Common\ShopConfigurationTable;
use Bitkorn\Shop\Table\Content\ShopContentTable;
use Bitkorn\Shop\Table\Document\ShopDocumentDeliveryTable;
use Bitkorn\Shop\Table\Document\ShopDocumentInvoiceTable;
use Bitkorn\Shop\Table\Document\ShopDocumentOrderTable;
use Bitkorn\Shop\Table\User\ShopUserAddressTable;
use Bitkorn\Shop\Table\User\ShopUserDataNumberTable;
use Bitkorn\Shop\Table\User\ShopUserDataTable;
use Bitkorn\Shop\Table\User\ShopUserFeePaidTable;
use Bitkorn\Shop\Table\User\ShopUserFeeTable;
use Bitkorn\Shop\Table\User\ShopUserGroupTable;
use Bitkorn\Shop\Table\Vendor\ShopVendorGroupTable;
use Bitkorn\Shop\Table\Vendor\ShopVendorTable;
use Bitkorn\Shop\Tablex\Article\Option\ShopArticleOptionTablex;
use Bitkorn\Shop\Tablex\Article\ShopArticleImageTablex;
use Bitkorn\Shop\Tablex\Article\ShopArticleStockTablex;
use Bitkorn\Shop\Tablex\Article\ShopArticleTablex;
use Bitkorn\Shop\Tablex\Article\Size\ShopArticleSizeTablex;
use Bitkorn\Shop\Tablex\Basket\ShopBasketEntityTablex;
use Bitkorn\Shop\Tablex\Basket\ShopBasketTablex;
use Bitkorn\Shop\Tablex\User\ShopUserTablex;
use Bitkorn\Shop\Tablex\Vendor\ShopVendorTablex;
use Bitkorn\Shop\View\Helper\Address\AddressDisplay;
use Bitkorn\Shop\View\Helper\Address\AddressTypeDisplay;
use Bitkorn\Shop\View\Helper\Article\ArticleDescVariable;
use Bitkorn\Shop\View\Helper\Article\Category\ShopArticleCategorySelect;
use Bitkorn\Shop\View\Helper\Article\Category\ShopArticleCategoryUnorderedList;
use Bitkorn\Shop\View\Helper\Article\Frontend\ShopArticleCategoryUrl;
use Bitkorn\Shop\View\Helper\Article\Frontend\ShopArticlePreview;
use Bitkorn\Shop\View\Helper\Article\Frontend\ShopArticlePreviewImage;
use Bitkorn\Shop\View\Helper\Article\Frontend\ShopArticlesPreview;
use Bitkorn\Shop\View\Helper\Article\Frontend\ShopArticleTypeUrl;
use Bitkorn\Shop\View\Helper\Article\Option\ShopArticleOption;
use Bitkorn\Shop\View\Helper\Article\Option\ShopArticleOptionsSelect;
use Bitkorn\Shop\View\Helper\Article\Option\ShopArticleOptionsByArticleId;
use Bitkorn\Shop\View\Helper\Article\Option\ShopArticleOptionsFromJson;
use Bitkorn\Shop\View\Helper\Article\Param2d\ArticleParam2dItemDesc;
use Bitkorn\Shop\View\Helper\Article\Param2d\ArticleParam2dsSelect;
use Bitkorn\Shop\View\Helper\Article\Rating\ArticleRatings;
use Bitkorn\Shop\View\Helper\Article\Rating\BarRating;
use Bitkorn\Shop\View\Helper\Article\ShopArticleImageRelativeFilename;
use Bitkorn\Shop\View\Helper\Article\ShopArticleSefurl;
use Bitkorn\Shop\View\Helper\Article\Stock\ShopArticleStockWidget;
use Bitkorn\Shop\View\Helper\Article\TypeDelivery\ArticleLengthcutOptionsDesc;
use Bitkorn\Shop\View\Helper\Basket\Admin\BasketDiscountValid;
use Bitkorn\Shop\View\Helper\Basket\Admin\BasketStatusSelect;
use Bitkorn\Shop\View\Helper\Basket\Email\Confirmation\ShopBasketOrdered;
use Bitkorn\Shop\View\Helper\Basket\Email\Confirmation\ShopBasketPayed;
use Bitkorn\Shop\View\Helper\Basket\Email\Rating\RatingReminder;
use Bitkorn\Shop\View\Helper\Basket\Email\ShopBasketShipped;
use Bitkorn\Shop\View\Helper\Basket\Email\ShopBasketSummary;
use Bitkorn\Shop\View\Helper\Basket\Frontend\ShopBasketOverview;
use Bitkorn\Shop\View\Helper\Common\ShopFooter;
use Bitkorn\Shop\View\Helper\Document\ShopDocumentIdByBasketUnique;
use Bitkorn\Shop\View\Helper\Form\DirectEditInput;
use Bitkorn\Shop\View\Helper\IdAssoc\Article\ShopArticleSizeDefIdAssoc;
use Bitkorn\Shop\View\Helper\IdAssoc\Vendor\ShopVendorIdAssoc;
use Bitkorn\Shop\View\Helper\Quantityunit\QuantityunitToOrigin;
use Bitkorn\ShopShippingUps\Provider\ShippingProviderUps;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use Laminas\ServiceManager\Factory\InvokableFactory;

return [
    'router' => [
        'routes' => [
            /*
             * Frontend Article
             */
            'shop_frontend_article_articles' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/articles[/:article_category_alias]',
                    'constraints' => [
                        'article_category_alias' => '[0-9a-zA-Z_-]*'
                    ],
                    'defaults' => [
                        'controller' => ArticleFrontendController::class,
                        'action' => 'articles',
                    ],
                ],
                'route_def' => [
                    'name' => 'Shop Article Category: ', // can used as prefix
                    'constraints' => [
                        'article_category_alias' => [
                            'db_table' => 'shop_article_category',
                            'db_table_value' => 'shop_article_category_alias',
                            'db_table_display' => 'shop_article_category_name',
                            'db_where' => 'shop_article_category_id > 1'
                        ]
                    ]
                ]
            ],
            'shop_frontend_article_article' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/article[/:article_sefurl]',
                    'constraints' => [
                        'article_sefurl' => '[a-z0-9-]*'
                    ],
                    'defaults' => [
                        'controller' => ArticleFrontendController::class,
                        'action' => 'article',
                    ],
                ],
                'route_def' => [
                    'name' => 'Shop Article: ', // can used as prefix
                    'constraints' => [
                        'article_sefurl' => [
                            'db_table' => 'shop_article',
                            'db_table_value' => 'shop_article_sefurl',
                            'db_table_display' => 'shop_article_name',
                            'db_where' => 'shop_article_type = \'standard\''
                        ]
                    ]
                ]
            ],
            'shop_frontend_article_configarticle' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/c-article[/:article_sefurl]',
                    'constraints' => [
                        'article_sefurl' => '[a-z0-9-]*'
                    ],
                    'defaults' => [
                        'controller' => ArticleFrontendController::class,
                        'action' => 'configArticle',
                    ],
                ],
                'route_def' => [
                    'name' => 'Shop Config-Article: ', // can used as prefix
                    'constraints' => [
                        'article_sefurl' => [
                            'db_table' => 'shop_article',
                            'db_table_value' => 'shop_article_sefurl',
                            'db_table_display' => 'shop_article_name',
                            'db_where' => 'shop_article_type = \'config\''
                        ]
                    ]
                ]
            ],
            'shop_frontend_article_param2dlengthcutarticle' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/p2dlc-article[/:article_sefurl]',
                    'constraints' => [
                        'article_sefurl' => '[a-z0-9-]*'
                    ],
                    'defaults' => [
                        'controller' => ArticleFrontendController::class,
                        'action' => 'param2dLengthcutArticle',
                    ],
                ],
                'route_def' => [
                    'name' => 'Shop Config-Article: ', // can used as prefix
                    'constraints' => [
                        'article_sefurl' => [
                            'db_table' => 'shop_article',
                            'db_table_value' => 'shop_article_sefurl',
                            'db_table_display' => 'shop_article_name',
                            'db_where' => 'shop_article_type = \'config\' AND shop_article_type_delivery \'lengthcut\''
                        ]
                    ]
                ]
            ],
            /*
             * Frontend article size
             */
            'shop_frontend_articlesize_articlesizegroupproductconfig' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/article-size-group-product-config[/:size_group_id]',
                    'constraints' => [
                        'size_group_id' => '[0-9]*'
                    ],
                    'defaults' => [
                        'controller' => ArticleSizeFrontendController::class,
                        'action' => 'articleSizeGroupProductConfig',
                    ],
                ],
            ],
            /*
             * Frontend Basket
             */
            'shop_frontend_basket_basket' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/shop-basket',
                    'defaults' => [
                        'controller' => BasketController::class,
                        'action' => 'basket',
                    ],
                ],
                'route_def' => [
                    'name' => 'Basket', // can used as prefix
                    'constraints' => [
                        'article_category_alias' => [
                            'db_table' => '',
                            'db_table_value' => '',
                            'db_table_display' => '',
                        ]
                    ]
                ]
            ],
            'shop_frontend_basket_chooseaddress' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/shop-basket-chooseaddress',
                    'defaults' => [
                        'controller' => BasketController::class,
                        'action' => 'chooseAddress',
                    ],
                ],
            ],
            'shop_frontend_basket_chooseshipping' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/shop-basket-chooseshipping',
                    'defaults' => [
                        'controller' => BasketController::class,
                        'action' => 'chooseShipping',
                    ],
                ],
            ],
            'shop_frontend_basket_choosepayment' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/shop-basket-choosepayment',
                    'defaults' => [
                        'controller' => BasketController::class,
                        'action' => 'choosePayment',
                    ],
                ],
            ],
            'shop_frontend_basket_afterpayment' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/einkauf-status[/:basket_unique]',
                    'constraints' => [
                        'basket_unique' => '[0-9a-zA-Z_-]*'
                    ],
                    'defaults' => [
                        'controller' => BasketController::class,
                        'action' => 'basketStatus',
                    ],
                ],
            ],
            'shop_frontend_basket_basketentityrating' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/basket-rating[/:basket_unique]',
                    'constraints' => [
                        'basket_unique' => '[0-9a-zA-Z_-]*'
                    ],
                    'defaults' => [
                        'controller' => BasketController::class,
                        'action' => 'basketEntityRating',
                    ],
                ],
            ],
            /*
             * Frontend content
             */
            'shop_frontend_content_shopcontent_staticcontent' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/shop-content[/:content_alias]',
                    'constraints' => [
                        'content_alias' => '[0-9a-zA-Z_-]*'
                    ],
                    'defaults' => [
                        'controller' => ShopContentController::class,
                        'action' => 'staticContent',
                    ],
                ],
            ],
            /*
             * Frontend UserBackend
             */
            'shop_frontend_userbackend_userbaskets' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/einkaeufe',
                    'defaults' => [
                        'controller' => UserBackendController::class,
                        'action' => 'userBaskets',
                    ],
                ],
            ],
            'shop_frontend_userbackend_userprofile' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/me-profile',
                    'defaults' => [
                        'controller' => UserBackendController::class,
                        'action' => 'userProfile',
                    ],
                ],
            ],
            /*
             * Admin
             */
            'shop_admin_dashboardadmin_dashboard' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/shopadmin-dashboard',
                    'defaults' => [
                        'controller' => DashboardAdminController::class,
                        'action' => 'dashboard',
                    ],
                ],
            ],
            /*
             * Admin Article
             */
            'shop_admin_article_articleadmin_articles' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/shopadmin-articles',
                    'defaults' => [
                        'controller' => ArticleAdminController::class,
                        'action' => 'articles',
                    ],
                ],
            ],
            'shop_admin_article_articleadmin_addarticle' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/shopadmin-addarticle',
                    'defaults' => [
                        'controller' => ArticleAdminController::class,
                        'action' => 'addArticle',
                    ],
                ],
            ],
            'shop_admin_article_articleadmin_editarticle' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/shopadmin-editarticle[/:article_id[/:copy]]',
                    'constraints' => [
                        'article_id' => '[0-9]*',
                        'copy' => '[0-1]*'
                    ],
                    'defaults' => [
                        'controller' => ArticleAdminController::class,
                        'action' => 'editArticle',
                    ],
                ],
            ],
            'shop_admin_article_articleadmin_articlecategories' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/shopadmin-articlecategories',
                    'defaults' => [
                        'controller' => ArticleCategoryAdminController::class,
                        'action' => 'articleCategories',
                    ],
                ],
            ],
            'shop_admin_article_articleadmin_articlecategory' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/shopadmin-articlecategory[/:category_id]',
                    'constraints' => [
                        'category_id' => '[0-9]*'
                    ],
                    'defaults' => [
                        'controller' => ArticleCategoryAdminController::class,
                        'action' => 'articleCategory',
                    ],
                ],
            ],
            /*
             * Admin Article group
             */
            'shop_admin_article_articleadmin_editarticlegrouprelation' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/shopadmin-editarticlegrouprelation[/:article_id]',
                    'constraints' => [
                        'article_id' => '[0-9]*'
                    ],
                    'defaults' => [
                        'controller' => ArticleAdminController::class,
                        'action' => 'editArticleGroupRelation',
                    ],
                ],
            ],
            /*
             * Admin Article size group
             */
            'shop_admin_article_articleadmin_editarticlesizegrouprelation' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/shopadmin-editarticlesizegrouprelation[/:article_id]',
                    'constraints' => [
                        'article_id' => '[0-9]*'
                    ],
                    'defaults' => [
                        'controller' => ArticleAdminController::class,
                        'action' => 'editArticleSizeGroupRelation',
                    ],
                ],
            ],
            /*
             * Admin Article stock
             */
            'shop_admin_article_articleadmin_articlestock' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/shopadmin-articlestock[/:article_id]',
                    'constraints' => [
                        'article_id' => '[0-9]*'
                    ],
                    'defaults' => [
                        'controller' => ArticleAdminController::class,
                        'action' => 'articleStock',
                    ],
                ],
            ],
            /*
             * Admin Article Option
             */
            'shop_admin_article_articleoptionadmin_articleoptiondefs_articlerelation' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/shopadmin-articleoptiondefs-articlerelation[/:article_id]',
                    'constraints' => [
                        'article_id' => '[0-9]*'
                    ],
                    'defaults' => [
                        'controller' => ArticleOptionAdminController::class,
                        'action' => 'articleOptionDefsArticleRelation',
                    ],
                ],
            ],
            'shop_admin_article_articleoptionadmin_articleoptionitemsarticleimage' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/shopadmin-articleoptionitemsarticleimage[/:def_id[/:article_id]]',
                    'constraints' => [
                        'def_id' => '[0-9]*',
                        'article_id' => '[0-9]*'
                    ],
                    'defaults' => [
                        'controller' => ArticleOptionAdminController::class,
                        'action' => 'articleOptionItemsArticleImage',
                    ],
                ],
            ],
            'shop_admin_article_articleoptionadmin_articleoptionitemsarticlepricediff' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/shopadmin-articleoptionitemsarticlepricediff[/:def_id[/:article_id]]',
                    'constraints' => [
                        'def_id' => '[0-9]*',
                        'article_id' => '[0-9]*'
                    ],
                    'defaults' => [
                        'controller' => ArticleOptionAdminController::class,
                        'action' => 'articleOptionItemsArticlePricediff',
                    ],
                ],
            ],
            'shop_admin_article_articleoptionadmin_articleoptiondefs' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/shopadmin-articleoptiondefs',
                    'defaults' => [
                        'controller' => ArticleOptionAdminController::class,
                        'action' => 'articleOptionDefs',
                    ],
                ],
            ],
            'shop_admin_article_articleoptionadmin_articleoptiondef' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/shopadmin-articleoptiondef[/:def_id]',
                    'constraints' => [
                        'def_id' => '[0-9]*'
                    ],
                    'defaults' => [
                        'controller' => ArticleOptionAdminController::class,
                        'action' => 'articleOptionDef',
                    ],
                ],
            ],
            /*
             * Admin Article Images
             */
            'shop_admin_article_articleimageadmin_editarticleimages' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/shopadmin-editarticleimages[/:article_id]',
                    'constraints' => [
                        'article_id' => '[0-9]*'
                    ],
                    'defaults' => [
                        'controller' => ArticleImageAdminController::class,
                        'action' => 'editArticleImages',
                    ],
                ],
            ],
            'shop_admin_article_articleimageadmin_uploadimage' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/shopadmin-uploadimage[/:article_id]',
                    'constraints' => [
                        'article_id' => '[0-9]*'
                    ],
                    'defaults' => [
                        'controller' => ArticleImageAdminController::class,
                        'action' => 'uploadImage',
                    ],
                ],
            ],
            /*
             * Admin Shopuser
             */
            'shop_admin_user_shopuseradmin_shopusers' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/shopadmin-shopusers',
                    'defaults' => [
                        'controller' => ShopUserAdminController::class,
                        'action' => 'shopUsers',
                    ],
                ],
            ],
            'shop_admin_user_shopuseradmin_addshopuser' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/shopadmin-addshopuser',
                    'defaults' => [
                        'controller' => ShopUserAdminController::class,
                        'action' => 'addShopUser',
                    ],
                ],
            ],
            'shop_admin_user_shopuseradmin_editshopuseraddress' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/shopadmin-editshopuseraddress[/:user_uuid]',
                    'constraints' => [
                        'user_uuid' => '[0-9a-z-]*'
                    ],
                    'defaults' => [
                        'controller' => ShopUserAdminController::class,
                        'action' => 'editShopUserAddress',
                    ],
                ],
            ],
            'shop_admin_user_shopuseradmin_editshopuser' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/shopadmin-editshopuser[/:user_uuid]',
                    'constraints' => [
                        'user_uuid' => '[0-9a-z-]*'
                    ],
                    'defaults' => [
                        'controller' => ShopUserAdminController::class,
                        'action' => 'editShopUser',
                    ],
                ],
            ],
            'shop_admin_user_shopuseradmin_shopuser' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/shopadmin-shopuser[/:user_uuid]',
                    'constraints' => [
                        'user_uuid' => '[0-9a-zA-Z-]*'
                    ],
                    'defaults' => [
                        'controller' => ShopUserAdminController::class,
                        'action' => 'shopUser',
                    ],
                ],
            ],
            /**
             * Admin ShopUserFee
             */
            'shop_admin_user_fee_shopuserfeeadmin_shopuser' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/shopadmin-shopuserfee[/:user_uuid]',
                    'constraints' => [
                        'user_uuid' => '[0-9a-zA-Z-]*'
                    ],
                    'defaults' => [
                        'controller' => ShopUserFeeAdminController::class,
                        'action' => 'shopUserFee',
                    ],
                ],
            ],
            /*
             * Admin Basket
             */
            'shop_admin_basket_basketadmin_baskets' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/shopadmin-baskets[/:user_uuid]',
                    'constraints' => [
                        'user_uuid' => '[0-9a-zA-Z-]*'
                    ],
                    'defaults' => [
                        'controller' => BasketAdminController::class,
                        'action' => 'baskets',
                    ],
                ],
            ],
            'shop_admin_basket_basketadmin_basket' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/shopadmin-basket[/:basket_unique]',
                    'constraints' => [
                        'basket_unique' => '[0-9a-zA-Z_-]*'
                    ],
                    'defaults' => [
                        'controller' => BasketAdminController::class,
                        'action' => 'basket',
                    ],
                ],
            ],
            'shop_admin_basket_basketdiscountadmin_discounts' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/shopadmin-basketdiscounts[/:user_uuid]',
                    'constraints' => [
                        'user_uuid' => '[0-9a-z-]*'
                    ],
                    'defaults' => [
                        'controller' => BasketDiscountAdminController::class,
                        'action' => 'basketDiscounts',
                    ],
                ],
            ],
            'shop_admin_basket_basketadmin_basketentityrating' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/shopadmin-basketentityrating[/:basket_entity_id]',
                    'constraints' => [
                        'basket_entity_id' => '[0-9]*'
                    ],
                    'defaults' => [
                        'controller' => BasketAdminController::class,
                        'action' => 'basketEntityRating',
                    ],
                ],
            ],
            'shop_admin_basket_basketadmin_basketsclearing' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/shopadmin-basketsclearing',
                    'defaults' => [
                        'controller' => BasketAdminController::class,
                        'action' => 'basketsClearing',
                    ],
                ],
            ],
            /*
             * Admin Basket Claim
             */
            'shop_admin_basket_claimadmin_claims' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/shopadmin-claims',
                    'defaults' => [
                        'controller' => ClaimAdminController::class,
                        'action' => 'claims',
                    ],
                ],
            ],
            'shop_admin_basket_claimadmin_claimsfrombasket' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/shopadmin-claimsfrombasket[/:basket_unique]',
                    'constraints' => [
                        'basket_unique' => '[0-9a-zA-Z_-]*'
                    ],
                    'defaults' => [
                        'controller' => ClaimAdminController::class,
                        'action' => 'claimsFromBasket',
                    ],
                ],
            ],
            'shop_admin_basket_claimadmin_claim' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/shopadmin-claim[/:claim_id]',
                    'constraints' => [
                        'claim_id' => '[0-9]*'
                    ],
                    'defaults' => [
                        'controller' => ClaimAdminController::class,
                        'action' => 'claim',
                    ],
                ],
            ],
            'shop_admin_basket_claimadmin_manageclaim' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/shopadmin-manageclaim[/:claim_id][/:basket_item_id][/:basket_unique][/]',
                    'constraints' => [
                        'claim_id' => '[0-9]*',
                        'basket_item_id' => '[0-9]*',
                        'basket_unique' => '[0-9a-zA-Z_-]*'
                    ],
                    'defaults' => [
                        'controller' => ClaimAdminController::class,
                        'action' => 'manageClaim',
                    ],
                ],
            ],
            'shop_admin_basket_claimadmin_streamclaimfile' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/shopadmin-streamclaimfile[/:claim_id[/:filename]]',
                    'constraints' => [
                        'claim_id' => '[0-9]*',
                        'filename' => '[0-9a-z_.]*'
                    ],
                    'defaults' => [
                        'controller' => ClaimAdminController::class,
                        'action' => 'streamClaimFile',
                    ],
                ],
            ],
            'shop_admin_basket_claimadmin_deleteclaimfile' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/shopadmin-deleteclaimfile[/:claim_id[/:filename]]',
                    'constraints' => [
                        'claim_id' => '[0-9]*',
                        'filename' => '[0-9a-z_.]*'
                    ],
                    'defaults' => [
                        'controller' => ClaimAdminController::class,
                        'action' => 'deleteClaimFile',
                    ],
                ],
            ],
            /*
             * Admin Rating
             */
            'shop_admin_basket_ratingadmin_ratings' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/shopadmin-ratings',
                    'defaults' => [
                        'controller' => RatingAdminController::class,
                        'action' => 'ratings',
                    ],
                ],
            ],
            'shop_admin_basket_ratingadmin_rating' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/shopadmin-rating[/:rating_id]',
                    'constraints' => [
                        'rating_id' => '[0-9a-zA-Z_-]*'
                    ],
                    'defaults' => [
                        'controller' => RatingAdminController::class,
                        'action' => 'rating',
                    ],
                ],
            ],
            /*
             * Admin Article Size
             */
            'shop_admin_article_articlesize_sizes' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/shopadmin-articlesizes',
                    'defaults' => [
                        'controller' => ArticleSizeAdminController::class,
                        'action' => 'articleSizes',
                    ],
                ],
            ],
            'shop_admin_article_articlesize_sizegroups' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/shopadmin-articlesizegroups',
                    'defaults' => [
                        'controller' => ArticleSizeAdminController::class,
                        'action' => 'articleSizeGroups',
                    ],
                ],
            ],
            'shop_admin_article_articlesize_sizegroup' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/shopadmin-articlesizegroup[/:size_group_id]',
                    'constraints' => [
                        'size_group_id' => '[0-9]*'
                    ],
                    'defaults' => [
                        'controller' => ArticleSizeAdminController::class,
                        'action' => 'articleSizeGroup',
                    ],
                ],
            ],
            /*
             * Admin Article lengthcut
             */
            'shop_admin_article_articlelengthcut_lengthcutdefs' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/shopadmin-lengthcutdefs',
                    'defaults' => [
                        'controller' => ArticleLengthcutController::class,
                        'action' => 'lengthcutDefs',
                    ],
                ],
            ],
            'shop_admin_article_articlelengthcut_lengthcutdef' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/shopadmin-lengthcutdef[/:lengthcut_def_id]',
                    'constraints' => [
                        'lengthcut_def_id' => '[0-9]*'
                    ],
                    'defaults' => [
                        'controller' => ArticleLengthcutController::class,
                        'action' => 'lengthcutDef',
                    ],
                ],
            ],
            /*
             * Admin Vendor
             */
            'shop_admin_vendor_vendors' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/shopadmin-vendors',
                    'defaults' => [
                        'controller' => VendorAdminController::class,
                        'action' => 'vendors',
                    ],
                ],
            ],
            'shop_admin_vendor_vendor' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/shopadmin-vendor[/:vendor_id]',
                    'constraints' => [
                        'vendor_id' => '[0-9]*'
                    ],
                    'defaults' => [
                        'controller' => VendorAdminController::class,
                        'action' => 'vendor',
                    ],
                ],
            ],
            'shop_admin_vendor_addvendor' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/shopadmin-addvendor',
                    'defaults' => [
                        'controller' => VendorAdminController::class,
                        'action' => 'addVendor',
                    ],
                ],
            ],
            /*
             * Admin Config
             */
            'shop_admin_config_configs' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/shopadmin-configs',
                    'defaults' => [
                        'controller' => ShopConfigAdminController::class,
                        'action' => 'shopConfigs',
                    ],
                ],
            ],
            /*
             * Admin Backup
             */
            'shop_admin_backup_backup_backupcomplete' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/shopadmin-backup-complete',
                    'defaults' => [
                        'controller' => BackupController::class,
                        'action' => 'backupComplete',
                    ],
                ],
            ],
            /*
             * Common without view-file
             */
            'shop_common_pdf_streaminvoice' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/streaminvoice[/:document_id]',
                    'constraints' => [
                        'document_id' => '[0-9]*'
                    ],
                    'defaults' => [
                        'controller' => PdfController::class,
                        'action' => 'streamInvoicePdf',
                    ],
                ],
            ],
            'shop_common_pdf_streamorder' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/streamorder[/:document_id]',
                    'constraints' => [
                        'document_id' => '[0-9]*'
                    ],
                    'defaults' => [
                        'controller' => PdfController::class,
                        'action' => 'streamOrderPdf',
                    ],
                ],
            ],
            'shop_common_pdf_streamdelivery' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/streamdelivery[/:document_id]',
                    'constraints' => [
                        'document_id' => '[0-9]*'
                    ],
                    'defaults' => [
                        'controller' => PdfController::class,
                        'action' => 'streamDeliveryPdf',
                    ],
                ],
            ],
            'shop_common_basketuniquepdf_streaminvoice' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/bu-streaminvoice[/:basket_unique]',
                    'constraints' => [
                        'basket_unique' => '[a-zA-Z0-9_-]*'
                    ],
                    'defaults' => [
                        'controller' => BasketuniquePdfController::class,
                        'action' => 'streamInvoicePdf',
                    ],
                ],
            ],
            'shop_common_basketuniquepdf_streamorder' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/bu-streamorder[/:basket_unique]',
                    'constraints' => [
                        'basket_unique' => '[a-zA-Z0-9_-]*'
                    ],
                    'defaults' => [
                        'controller' => BasketuniquePdfController::class,
                        'action' => 'streamOrderPdf',
                    ],
                ],
            ],
            'shop_common_basketuniquepdf_streamdelivery' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/bu-streamdelivery[/:basket_unique]',
                    'constraints' => [
                        'basket_unique' => '[a-zA-Z0-9_-]*'
                    ],
                    'defaults' => [
                        'controller' => BasketuniquePdfController::class,
                        'action' => 'streamDeliveryPdf',
                    ],
                ],
            ],
            /*
             * POS
             */
            'shop_pos_sellforuser' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/pos-sellforuser[/:user_uuid]',
                    'constraints' => [
                        'user_uuid' => '[0-9a-z-]*'
                    ],
                    'defaults' => [
                        'controller' => SellController::class,
                        'action' => 'sellForUser',
                    ],
                ],
            ],
            'shop_pos_sellforguest' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/pos-sellforguest',
                    'defaults' => [
                        'controller' => SellController::class,
                        'action' => 'sellForGuest',
                    ],
                ],
            ],
            'shop_pos_enterbasketaddress' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/pos-enterbasketaddress',
                    'defaults' => [
                        'controller' => SellController::class,
                        'action' => 'enterBasketAddress',
                    ],
                ],
            ],
            'shop_ajax_basket_admin_posbasket' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/basket-widget-compact',
                    'defaults' => [
                        'controller' => AjaxBasketAdminController::class,
                        'action' => 'basketWidgetCompact',
                    ],
                ],
            ],
            /*
             * Ajax
             */
            'shop_ajax_article_typedelivery_switchlengthdef' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/article-typedelivery-switchlengthcutdef/:article_id/:lengthcut_def_id',
                    'constraints' => [
                        'article_id' => '[0-9]*',
                        'lengthcut_def_id' => '[0-9]*'
                    ],
                    'defaults' => [
                        'controller' => TypeDeliveryAjaxController::class,
                        'action' => 'switchLengthcutDef',
                    ],
                ],
            ],
            /*
             * AjaxHelper Basket
             */
            'shop_ajax_basket_basketwidget' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/shop-ajax-basket-basketwidget',
                    'defaults' => [
                        'controller' => AjaxBasketController::class,
                        'action' => 'basketWidgetLine',
                    ],
                ],
            ],
            'shop_ajax_basket_addtobasket' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/shop-ajax-basket-addtobasket[/:article_id[/:article_amount]]',
                    'constraints' => [
                        'article_id' => '[0-9]*',
                        'article_amount' => '[0-9.]*' // muss in der Action auf echte Dezimalzahl getestet werden
                    ],
                    'defaults' => [
                        'controller' => AjaxBasketController::class,
                        'action' => 'addToBasket',
                    ],
                ],
            ],
            'shop_ajax_basket_basketdeleteitem' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/shop-ajax-basket-basketitem-delete[/:basket_item_id]',
                    'constraints' => [
                        'basket_item_id' => '[0-9]*'
                    ],
                    'defaults' => [
                        'controller' => AjaxBasketController::class,
                        'action' => 'deleteBasketItem',
                    ],
                ],
            ],
            'shop_ajax_basket_basketdelete' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/shop-ajax-basket-deletebasket',
                    'defaults' => [
                        'controller' => AjaxBasketController::class,
                        'action' => 'deleteBasket',
                    ],
                ],
            ],
            'shop_ajax_basket_articlestockwidget' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/shop-ajax-basket-articlestockwidget[/:article_id]', // article-options in POST
                    'constraints' => [
                        'article_id' => '[0-9]*'
                    ],
                    'defaults' => [
                        'controller' => AjaxBasketController::class,
                        'action' => 'articleStockWidget',
                    ],
                ],
            ],
            /*
             * AjaxHelper Address
             */
            'shop_ajax_user_addressdisplay' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/shop-ajax-user-addressdisplay',
                    'defaults' => [
                        'controller' => AjaxUserController::class,
                        'action' => 'addressDisplay',
                    ],
                ],
            ],
            'shop_ajax_user_addressform' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/shop-ajax-user-addressform[/:address_id]',
                    'constraints' => [
                        'address_id' => '[0-9]*',
                    ],
                    'defaults' => [
                        'controller' => AjaxUserController::class,
                        'action' => 'addressForm',
                    ],
                ],
            ],
            'shop_ajax_user_addressdelete' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/shop-ajax-user-addressdelete[/:address_id]',
                    'constraints' => [
                        'address_id' => '[0-9]*',
                    ],
                    'defaults' => [
                        'controller' => AjaxUserController::class,
                        'action' => 'addressDelete',
                    ],
                ],
            ],
            /*
             * AjaxHelper Anonymus
             */
            'shop_ajax_anonymus_addressdisplay' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/shop-ajax-anonymus-addressdisplay',
                    'defaults' => [
                        'controller' => AjaxAnonymusController::class,
                        'action' => 'addressDisplay',
                    ],
                ],
            ],
            'shop_ajax_anonymus_addressform' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/shop-ajax-anonymus-addressform[/:address_id]',
                    'constraints' => [
                        'address_id' => '[0-9]*'
                    ],
                    'defaults' => [
                        'controller' => AjaxAnonymusController::class,
                        'action' => 'addressForm',
                    ],
                ],
            ],
            'shop_ajax_anonymus_addressdelete' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/shop-ajax-anonymus-addressdelete[/:address_id]',
                    'constraints' => [
                        'address_id' => '[0-9]*',
                    ],
                    'defaults' => [
                        'controller' => AjaxAnonymusController::class,
                        'action' => 'addressDelete',
                    ],
                ],
            ],
            /*
             * AjaxHelper Article
             */
            'shop_ajax_article_selectgroupablearticlesform' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/shop-ajax-article-selectgroupablearticlesform[/:article_id]',
                    'constraints' => [
                        'article_id' => '[0-9]*',
                    ],
                    'defaults' => [
                        'controller' => AjaxArticleController::class,
                        'action' => 'selectGroupableArticlesForm',
                    ],
                ],
            ],
            'shop_ajax_article_selectgroupablearticlesforsizegroupform' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/shop-ajax-article-selectgroupablearticlesforsizegroupform[/:article_id[/:size_group_id]][/]',
                    'constraints' => [
                        'article_id' => '[0-9]*',
                        'size_group_id' => '[0-9]*',
                    ],
                    'defaults' => [
                        'controller' => AjaxArticleController::class,
                        'action' => 'selectGroupableArticlesForSizeGroupForm',
                    ],
                ],
            ],
            /*
             * AjaxHelper Article Option
             */
            'shop_ajax_articleoption_optionimagecontainer' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/shop-ajax-articleoption-optionimagecontainer[/:article_id[/:option_item_id]]',
                    'constraints' => [
                        'article_id' => '[0-9]*',
                        'option_item_id' => '[0-9]*',
                    ],
                    'defaults' => [
                        'controller' => AjaxArticleOptionController::class,
                        'action' => 'optionImageContainer',
                    ],
                ],
            ],
            /*
             * AjaxHelper Admin
             */
            'shop_ajax_basket_admin_basketitems' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/shop-ajax-basket-admin-basketitems[/:basket_unique]',
                    'constraints' => [
                        'basket_unique' => '[0-9a-zA-Z_-]*'
                    ],
                    'defaults' => [
                        'controller' => AjaxBasketAdminController::class,
                        'action' => 'basketItems',
                    ],
                ],
            ],
            'shop_ajax_basket_admin_basketdiscountform' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/shop-ajax-basket-admin-basketdiscountform',
                    'defaults' => [
                        'controller' => AjaxBasketAdminController::class,
                        'action' => 'basketDiscountForm',
                    ],
                ],
            ],
            'shop_ajax_basket_admin_basketdiscountsalesstaffform' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/shop-ajax-basket-admin-basketdiscountsalesstaffform',
                    'defaults' => [
                        'controller' => AjaxBasketAdminController::class,
                        'action' => 'basketDiscountSalesstaffForm',
                    ],
                ],
            ],
            'shop_ajax_basket_admin_basketdiscountc2cform' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/shop-ajax-basket-admin-basketdiscountc2cform',
                    'defaults' => [
                        'controller' => AjaxBasketAdminController::class,
                        'action' => 'basketDiscountC2cForm',
                    ],
                ],
            ],
            'shop_ajax_basket_admin_basketdiscountswitchactive' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/shop-ajax-basket-admin-basketdiscountswitchactive[/:shop_basket_discount_id][/]',
                    'constraints' => [
                        'shop_basket_discount_id' => '[0-9]*',
                    ],
                    'defaults' => [
                        'controller' => AjaxBasketAdminController::class,
                        'action' => 'basketDiscountSwitchActive', // without view-file (JsonView)
                    ],
                ],
            ],
            'shop_ajax_article_admin_articlesize_updatearticlesizeitem' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/shop-ajax-article-size-item-update[/:shop_article_size_item_id/:shop_article_size_item_type/:shop_article_size_item_value]',
                    'constraints' => [
                        'shop_article_size_item_id' => '[0-9]*',
                        'shop_article_size_item_type' => '[a-z]*', // from | to
                        'shop_article_size_item_value' => '[0-9]*'
                    ],
                    'defaults' => [
                        'controller' => AjaxArticleSizeAdminController::class,
                        'action' => 'updateArticleSizeItem', // without view-file (JsonView)
                    ],
                ],
            ],
            'shop_ajax_admin_form_editarticleimagerelations' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/shop-ajax-admin-form-addarticleimagerelations[/:article_id]',
                    'constraints' => [
                        'article_id' => '[0-9]*',
                    ],
                    'defaults' => [
                        'controller' => AjaxArticleImageAdminController::class,
                        'action' => 'formAddArticleImageRelations',
                    ],
                ],
            ],
            'shop_ajax_user_admin_addressdisplay' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/shop-ajax-user-admin-addressdisplay[/:user_uuid]',
                    'constraints' => [
                        'user_uuid' => '[0-9a-z-]*',
                    ],
                    'defaults' => [
                        'controller' => AjaxUserAdminController::class,
                        'action' => 'addressDisplay',
                    ],
                ],
            ],
            'shop_ajax_user_admin_addressform' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/shop-ajax-user-admin-addressform[/:address_id[/:user_uuid]]',
                    'constraints' => [
                        'address_id' => '[0-9]*',
                        'user_uuid' => '[0-9a-z-]*',
                    ],
                    'defaults' => [
                        'controller' => AjaxUserAdminController::class,
                        'action' => 'addressForm',
                    ],
                ],
            ],
            'shop_ajax_user_admin_addressdelete' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/shop-ajax-user-admin-addressdelete[/:address_id[/:user_uuid]]',
                    'constraints' => [
                        'address_id' => '[0-9]*',
                        'user_uuid' => '[0-9a-z-]*',
                    ],
                    'defaults' => [
                        'controller' => AjaxUserAdminController::class,
                        'action' => 'addressDelete', // without view-file (JsonView)
                    ],
                ],
            ],
            'shop_ajax_user_admin_autocomplete' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/shop-ajax-user-admin-autocomplete',
                    'defaults' => [
                        'controller' => AjaxUserAdminController::class,
                        'action' => 'shopUserAutocomplete',
                    ],
                ],
            ],
            'shop_ajax_admin_shopconfig_edit' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/shop-ajax-admin-shopconfig-edit[/:shop_configuration_id[/:shop_configuration_value]]',
                    'constraints' => [
                        'shop_configuration_id' => '[0-9]*',
//                        'shop_configuration_value' => '*',
                    ],
                    'defaults' => [
                        'controller' => AjaxShopConfigAdminController::class,
                        'action' => 'updateShopConfigValue', // without view-file (JsonView)
                    ],
                ],
            ],
            'shop_ajax_admin_articleoptionarticleimage_formaddarticleimagerelations' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/shop-ajax-admin-articleoption-formaddarticleimagerelations[/:article_id[/:option_item_id]]',
                    'constraints' => [
                        'article_id' => '[0-9]*',
                        'option_item_id' => '[0-9]*'
                    ],
                    'defaults' => [
                        'controller' => AjaxArticleOptionArticleImageAdminController::class,
                        'action' => 'formAddArticleImageRelations',
                    ],
                ],
            ],
            /*
             * AjaxHelper Statistic
             */
            'shop_ajax_statistic_sales_sales' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/shop-ajax-statistic-sales[/:date_from[/[:date_to]]]',
                    'constraints' => [
                        'date_from' => '[0-9.-]*',
                        'date_to' => '[0-9.-]*'
                    ],
                    'defaults' => [
                        'controller' => AjaxStatisticSalesController::class,
                        'action' => 'sales',
                    ],
                ],
            ],
            'shop_ajax_statistic_sizearchive_sizearchive' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/shop-ajax-statistic-sizearchive[/:date_from[/[:date_to]]]',
                    'constraints' => [
                        'date_from' => '[0-9.-]*',
                        'date_to' => '[0-9.-]*'
                    ],
                    'defaults' => [
                        'controller' => AjaxStatisticSizeArchiveController::class,
                        'action' => 'sizeArchive',
                    ],
                ],
            ],
        ],
    ],
    'console' => [
        'router' => [
            'routes' => [
                'shop_console_cron_sendratingreminder' => [
//                    'type' => \Laminas\Mvc\Router\Console\Simple::class,
                    'options' => [
                        'route' => 'sendratingreminder (all|one) [<basket_entity_id>]',
                        'defaults' => [
                            'controller' => CronController::class,
                            'action' => 'sendRatingReminder'
                        ]
                    ]
                ]
            ]
        ]
    ],
    'controllers' => [
        'factories' => [
            /*
             * Admin
             */
            DashboardAdminController::class => DashboardAdminControllerFactory::class,
            ArticleImageAdminController::class => ArticleImageAdminControllerFactory::class,
            ArticleCategoryAdminController::class => ArticleCategoryAdminControllerFactory::class,
            BasketDiscountAdminController::class => BasketDiscountAdminControllerFactory::class,
            ClaimAdminController::class => ClaimAdminControllerFactory::class,
            ArticleSizeAdminController::class => ArticleSizeAdminControllerFactory::class,
            VendorAdminController::class => VendorAdminControllerFactory::class,
            ShopConfigAdminController::class => ShopConfigAdminControllerFactory::class,
            BackupController::class => BackupControllerFactory::class,
            BasketAdminController::class => BasketAdminControllerFactory::class,
            RatingAdminController::class => RatingAdminControllerFactory::class,
            ShopUserAdminController::class => ShopUserAdminControllerFactory::class,
            ShopUserFeeAdminController::class => ShopUserFeeAdminControllerFactory::class,
            ArticleOptionAdminController::class => ArticleOptionAdminControllerFactory::class,
            ArticleLengthcutController::class => ArticleLengthcutControllerFactory::class,
            /*
             * Frontend
             */
            ArticleSizeFrontendController::class => ArticleSizeFrontendControllerFactory::class,
            BasketController::class => BasketControllerFactory::class,
            ArticleFrontendController::class => ArticleFrontendControllerFactory::class,
            ShopContentController::class => ShopContentControllerFactory::class,
            /*
             * Frontend UserBackend
             */
            UserBackendController::class => UserBackendControllerFactory::class,
            /*
             * Common
             */
            PdfController::class => PdfControllerFactory::class,
            BasketuniquePdfController::class => BasketuniquePdfControllerFactory::class,
            /*
             * POS
             */
            SellController::class => SellControllerFactory::class,
            /*
             * Ajax
             */
            TypeDeliveryAjaxController::class => TypeDeliveryAjaxControllerFactory::class,
            /*
             * AjaxHelper
             */
            AjaxBasketController::class => AjaxBasketControllerFactory::class,
            AjaxUserController::class => AjaxUserControllerFactory::class,
            AjaxArticleController::class => AjaxArticleControllerFactory::class,
            AjaxAnonymusController::class => AjaxAnonymusControllerFactory::class,
            AjaxArticleOptionController::class => AjaxArticleOptionControllerFactory::class,
            AjaxArticleOptionArticleImageAdminController::class => AjaxArticleOptionArticleImageAdminControllerFactory::class,
            /*
             * Ajax Admin
             */
            AjaxBasketAdminController::class => AjaxBasketAdminControllerFactory::class,
            AjaxArticleSizeAdminController::class => AjaxArticleSizeAdminControllerFactory::class,
            AjaxArticleImageAdminController::class => AjaxArticleImageAdminControllerFactory::class,
            AjaxUserAdminController::class => AjaxUserAdminControllerFactory::class,
            AjaxShopConfigAdminController::class => AjaxShopConfigAdminControllerFactory::class,
            ArticleAdminController::class => ArticleAdminControllerFactory::class,
            AjaxStatisticSalesController::class => AjaxStatisticSalesControllerFactory::class,
            AjaxStatisticSizeArchiveController::class => AjaxStatisticSizeArchiveControllerFactory::class,
            /*
             * Console
             */
            CronController::class => Factory\Controller\Console\CronControllerFactory::class
        ]
    ],
    'service_manager' => [
        'abstract_factories' => [
        ],
        'factories' => [
            ShopService::class => ShopServiceFactory::class,
            ShopArticleService::class => ShopArticleServiceFactory::class,
            ShopArticleStockService::class => ShopArticleStockServiceFactory::class,
            ShopArticleOptionService::class => ShopArticleOptionServiceFactory::class,
            ShopArticleParam2dService::class => ShopArticleParam2dServiceFactory::class,
            ShopArticleSizeService::class => ShopArticleSizeServiceFactory::class,
            ShopArticleImageService::class => ShopArticleImageServiceFactory::class,
            ShopArticleTypeDeliveryService::class => ShopArticleTypeDeliveryServiceFactory::class,
            ShopArticleLengthcutDefService::class => ShopArticleLengthcutDefServiceFactory::class,
            BasketService::class => BasketServiceFactory::class,
            BasketFinalizeService::class => BasketFinalizeServiceFactory::class,
            BasketAdminService::class => BasketAdminServiceFactory::class,
            ShopClaimService::class => ShopClaimServiceFactory::class,
            DocumentOrderService::class => DocumentOrderServiceFactory::class,
            DocumentInvoiceService::class => DocumentInvoiceServiceFactory::class,
            DocumentDeliveryService::class => DocumentDeliveryServiceFactory::class,
            ShopUserNumberService::class => ShopUserNumberServiceFactory::class,
            ShopUserService::class => ShopUserServiceFactory::class,
            ShopUserFeeService::class => ShopUserFeeServiceFactory::class,
            ShopVendorService::class => ShopVendorServiceFactory::class,
            VendorNumberService::class => VendorNumberServiceFactory::class,
            ShippingService::class => ShippingServiceFactory::class,
            ConfigRoutesService::class => ConfigRoutesServiceFactory::class,
            ShopContentService::class => ShopContentServiceFactory::class,
            ShippingProviderCustomerPickup::class => InvokableFactory::class,
            QuantityunitService::class => QuantityunitServiceFactory::class,
            ShopAddressService::class => ShopAddressServiceFactory::class,
            // table - common
            ShopConfigurationTable::class => ShopConfigurationTableFactory::class,
            // table - article
            ShopArticleCategoryTable::class => ShopArticleCategoryTableFactory::class,
            ShopArticleTable::class => ShopArticleTableFactory::class,
            ShopArticleCategoryRelationTable::class => ShopArticleCategoryRelationTableFactory::class,
            ShopArticleImageTable::class => ShopArticleImageTableFactory::class,
            ShopArticleStockTable::class => ShopArticleStockTableFactory::class,
            ShopArticleGroupRelationTable::class => ShopArticleGroupRelationTableFactory::class,
            ShopArticleSizeGroupRelationTable::class => ShopArticleSizeGroupRelationTableFactory::class,
            ShopArticleClassTable::class => ShopArticleClassTableFactory::class,
            ShopArticleRatingTable::class => ShopArticleRatingTableFactory::class,
            // table - article size
            ShopArticleSizeGroupTable::class => ShopArticleSizeGroupTableFactory::class,
            ShopArticleSizePositionTable::class => ShopArticleSizePositionTableFactory::class,
            ShopArticleSizeDefTable::class => ShopArticleSizeDefTableFactory::class,
            ShopArticleSizeItemTable::class => ShopArticleSizeItemTableFactory::class,
            ShopArticleSizeArchiveTable::class => ShopArticleSizeArchiveTableFactory::class,
            // table - article option
            ShopArticleOptionDefTable::class => ShopArticleOptionDefTableFactory::class,
            ShopArticleOptionItemTable::class => ShopArticleOptionItemTableFactory::class,
            ShopArticleOptionArticleRelationTable::class => ShopArticleOptionArticleRelationTableFactory::class,
            ShopArticleOptionItemArticleImageTable::class => ShopArticleOptionItemArticleImageTableFactory::class,
            ShopArticleOptionItemArticlePricediffTable::class => ShopArticleOptionItemArticlePricediffTableFactory::class,
            // table - article param2d
            ShopArticleParam2dTable::class => ShopArticleParam2dTableFactory::class,
            ShopArticleParam2dItemTable::class => ShopArticleParam2dItemTableFactory::class,
            // table - article type-delivery
            ShopArticleLengthcutDefTable::class => ShopArticleLengthcutDefTableFactory::class,
            // table - basket
            ShopBasketTable::class => ShopBasketTableFactory::class,
            ShopBasketItemTable::class => ShopBasketItemTableFactory::class,
            ShopBasketEntityTable::class => ShopBasketEntityTableFactory::class,
            ShopBasketAddressTable::class => ShopBasketAddressTableFactory::class,
            ShopBasketDiscountTable::class => ShopBasketDiscountTableFactory::class,
            ShopBasketClaimTable::class => ShopBasketClaimTableFactory::class,
            // table - shopUser
            ShopUserAddressTable::class => ShopUserAddressTableFactory::class,
            ShopUserDataTable::class => ShopUserDataTableFactory::class,
            ShopUserDataNumberTable::class => ShopUserDataNumberTableFactory::class,
            ShopUserGroupTable::class => ShopUserGroupTableFactory::class,
            ShopUserFeeTable::class => ShopUserFeeTableFactory::class,
            ShopUserFeePaidTable::class => ShopUserFeePaidTableFactory::class,
            // table - document
            ShopDocumentOrderTable::class => ShopDocumentOrderTableFactory::class,
            ShopDocumentInvoiceTable::class => ShopDocumentInvoiceTableFactory::class,
            ShopDocumentDeliveryTable::class => ShopDocumentDeliveryTableFactory::class,
            /*
             * table vendor
             */
            ShopVendorGroupTable::class => ShopVendorGroupTableFactory::class,
            ShopVendorTable::class => ShopVendorTableFactory::class,
            /*
             *
             */
            ShopContentTable::class => ShopContentTableFactory::class,
            /*
             * tablex article
             */
            ShopArticleTablex::class => ShopArticleTablexFactory::class,
            ShopArticleStockTablex::class => ShopArticleStockTablexFactory::class,
            ShopArticleImageTablex::class => ShopArticleImageTablexFactory::class,
            ShopArticleSizeTablex::class => ShopArticleSizeTablexFactory::class,
            ShopArticleOptionTablex::class => ShopArticleOptionTablexFactory::class,
            /*
             * tablex basket
             */
            ShopBasketTablex::class => ShopBasketTablexFactory::class,
            ShopBasketEntityTablex::class => ShopBasketEntityTablexFactory::class,
            /*
             * tablex shopUser
             */
            ShopUserTablex::class => ShopUserTablexFactory::class,
            /*
             * tablex vendor
             */
            ShopVendorTablex::class => ShopVendorTablexFactory::class,
            /*
             * form
             */
            ShopArticleForm::class => ShopArticleFormFactory::class,
            ShopArticleImageForm::class => ShopArticleImageFormFactory::class,
            ShopArticleImageEditForm::class => ShopArticleImageEditFormFactory::class,
            ShopArticleStockForm::class => ShopArticleStockFormFactory::class,
            ShopArticleCategoryForm::class => ShopArticleCategoryFormFactory::class,
            ShopUserAddressForm::class => ShopUserAddressFormFactory::class,
            'Bitkorn\Shop\Form\User\ShopUserAddressFormInvoicePrivate' => Factory\Form\User\ShopUserAddressFormInvoicePrivateFactory::class,
            'Bitkorn\Shop\Form\User\ShopUserAddressFormInvoiceEnterprise' => Factory\Form\User\ShopUserAddressFormInvoiceEnterpriseFactory::class,
            'Bitkorn\Shop\Form\User\ShopUserAddressFormShipmentPrivate' => Factory\Form\User\ShopUserAddressFormShipmentPrivateFactory::class,
            'Bitkorn\Shop\Form\User\ShopUserAddressFormShipmentEnterprise' => Factory\Form\User\ShopUserAddressFormShipmentEnterpriseFactory::class,
            ShopUserCreateForm::class => ShopUserCreateFormFactory::class,
            ShopUserForm::class => ShopUserFormFactory::class,
            ShopUserFeeForm::class => ShopUserFeeFormFactory::class,
            ShopUserBankForm::class => ShopUserBankFormFactory::class,
            ShopArticleSizeGroupForm::class => ShopArticleSizeGroupFormFactory::class,
            ShopBasketAddressForm::class => ShopBasketAddressFormFactory::class,
            'Bitkorn\Shop\Form\Basket\ShopBasketAddressFormInvoicePrivate' => Factory\Form\Basket\ShopBasketAddressFormInvoicePrivateFactory::class,
            'Bitkorn\Shop\Form\Basket\ShopBasketAddressFormInvoiceEnterprise' => Factory\Form\Basket\ShopBasketAddressFormInvoiceEnterpriseFactory::class,
            'Bitkorn\Shop\Form\Basket\ShopBasketAddressFormShipmentPrivate' => Factory\Form\Basket\ShopBasketAddressFormShipmentPrivateFactory::class,
            'Bitkorn\Shop\Form\Basket\ShopBasketAddressFormShipmentEnterprise' => Factory\Form\Basket\ShopBasketAddressFormShipmentEnterpriseFactory::class,
            ShopBasketDiscountForm::class => ShopBasketDiscountFormFactory::class,
            ShopBasketClaimForm::class => ShopBasketClaimFormFactory::class,
            ShopBasketClaimFileForm::class => ShopBasketClaimFileFormFactory::class,
            ShopVendorForm::class => ShopVendorFormFactory::class,
            ShopArticleLengthcutDefForm::class => ShopArticleLengthcutDefFormFactory::class,
            /*
             * observer
             */
            ImageDeleteObserver::class => ImageDeleteObserverFactory::class,
            /*
             * Listener
             */
            UserRegisterListener::class => UserRegisterListenerFactory::class,
            UserDeleteListener::class => UserDeleteListenerFactory::class,
            /*
             * entity
             */
            ShopArticleOptionsEntity::class => Factory\Entity\Article\ShopArticleOptionsEntityFactory::class,
        ],
        'invokables' => [
            // Form
            RegisterForm::class => RegisterForm::class,
        ],
    ],
    'listeners' => [
        UserRegisterListener::class,
        UserDeleteListener::class,
    ],
    'view_helpers' => [
        'aliases' => [
            'shopFooter' => ShopFooter::class,
            'barRating' => BarRating::class,
            // article
            'shopArticlesPreview' => ShopArticlesPreview::class,
            'shopArticleOptionsSelect' => ShopArticleOptionsSelect::class,
            'articleParam2dItemDesc' => ArticleParam2dItemDesc::class,
            'articleRatings' => ArticleRatings::class,
            'shopArticleImageRelativeFilename' => ShopArticleImageRelativeFilename::class,
            'shopArticlePreview' => ShopArticlePreview::class,
            'shopArticlePreviewImage' => ShopArticlePreviewImage::class,
            'shopArticleCategoryUnorderedList' => ShopArticleCategoryUnorderedList::class,
            'shopArticleCategorySelect' => ShopArticleCategorySelect::class,
            'shopArticleSefurl' => ShopArticleSefurl::class,
            'shopArticleTypeUrl' => ShopArticleTypeUrl::class,
            'shopArticleCategoryUrl' => ShopArticleCategoryUrl::class,
            'shopArticleOption' => ShopArticleOption::class,
            'shopArticleOptionsFromJson' => ShopArticleOptionsFromJson::class,
            'shopArticleOptionsByArticleId' => ShopArticleOptionsByArticleId::class,
            'articleParam2dsSelect' => ArticleParam2dsSelect::class,
            'articleLengthcutOptionsDesc' => ArticleLengthcutOptionsDesc::class,
            'articleDescVariable' => ArticleDescVariable::class,
            'shopArticleStockWidget' => ShopArticleStockWidget::class,
            // basket
            'shopBasketOverview' => ShopBasketOverview::class,
            'shopDocumentIdByBasketUnique' => ShopDocumentIdByBasketUnique::class,
            'basketStatusSelect' => BasketStatusSelect::class,
            'isBasketDiscountValid' => BasketDiscountValid::class,
            // address
            'addressTypeDisplay' => AddressTypeDisplay::class,
            'addressDisplay' => AddressDisplay::class,
            // email draft
            'emailShopBasketSummary' => ShopBasketSummary::class,
            'emailShopConfirmationOrdered' => ShopBasketOrdered::class,
            'emailShopConfirmationPayed' => ShopBasketPayed::class,
            'emailShopBasketShipped' => ShopBasketShipped::class,
            'emailShopConfirmationShipped' => View\Helper\Basket\Email\Confirmation\ShopBasketShipped::class,
            // idAssoc
            'shopArticleSizeDefIdAssocHelper' => ShopArticleSizeDefIdAssoc::class,
            'shopVendorIdAssocHelper' => ShopVendorIdAssoc::class,
            //
            'directEditInput' => DirectEditInput::class,
            'quantityunitToOrigin' => QuantityunitToOrigin::class,
        ],
        'invokables' => [
            ShopFooter::class => ShopFooter::class,
            BarRating::class => BarRating::class,
            ShopArticlesPreview::class => ShopArticlesPreview::class,
            ShopArticleOptionsSelect::class => ShopArticleOptionsSelect::class,
            BasketDiscountValid::class => BasketDiscountValid::class,
            AddressTypeDisplay::class => AddressTypeDisplay::class,
        ],
        'factories' => [
            /*
             * article
             */
            ArticleRatings::class => ArticleRatingsFactory::class,
            ShopArticleImageRelativeFilename::class => ShopArticleImageRelativeFilenameFactory::class,
            ShopArticlePreview::class => ShopArticlePreviewFactory::class,
            ShopArticlePreviewImage::class => ShopArticlePreviewImageFactory::class,
            ShopArticleCategoryUnorderedList::class => ShopArticleCategoryUnorderedListFactory::class,
            ShopArticleCategorySelect::class => ShopArticleCategorySelectFactory::class,
            ShopArticleSefurl::class => ShopArticleSefurlFactory::class,
            ShopArticleTypeUrl::class => ShopArticleTypeUrlFactory::class,
            ShopArticleCategoryUrl::class => ShopArticleCategoryUrlFactory::class,
            ShopArticleOption::class => ShopArticleOptionFactory::class,
            ShopArticleOptionsFromJson::class => ShopArticleOptionsFromJsonFactory::class,
            ShopArticleOptionsByArticleId::class => ShopArticleOptionsByArticleIdFactory::class,
            ShopArticleStockWidget::class => ShopArticleStockWidgetFactory::class,
            ArticleParam2dsSelect::class => ArticleParam2dsSelectFactory::class,
            ArticleParam2dItemDesc::class => ArticleParam2dItemDescFactory::class,
            ArticleLengthcutOptionsDesc::class => ArticleLengthcutOptionsDescFactory::class,
            ArticleDescVariable::class => ArticleDescVariableFactory::class,
            /*
             * basket
             */
            ShopBasketOverview::class => ShopBasketOverviewFactory::class,
            ShopDocumentIdByBasketUnique::class => ShopDocumentIdByBasketUniqueFactory::class,
            BasketStatusSelect::class => BasketStatusSelectFactory::class,
            /*
             * address
             */
            AddressDisplay::class => AddressDisplayFactory::class,
            /*
             * email drafts
             */
            ShopBasketSummary::class => ShopBasketSummaryFactory::class,
            ShopBasketShipped::class => ShopBasketShippedFactory::class,
            ShopBasketOrdered::class => ShopBasketOrderedFactory::class,
            ShopBasketPayed::class => ShopBasketPayedFactory::class,
            View\Helper\Basket\Email\Confirmation\ShopBasketShipped::class => Factory\View\Helper\Basket\Email\Confirmation\ShopBasketShippedFactory::class,
            RatingReminder::class => RatingReminderFactory::class,
            /*
             * idAssocs
             */
            ShopArticleSizeDefIdAssoc::class => ShopArticleSizeDefIdAssocFactory::class,
            ShopVendorIdAssoc::class => ShopVendorIdAssocFactory::class,
            /*
             * form
             */
            DirectEditInput::class => DirectEditInputFactory::class,
            /*
             *
             */
            QuantityunitToOrigin::class => QuantityunitToOriginFactory::class,
        ],
    ],
    'form_elements' => [
        'invokables' => [
            Password::class => Password::class,
        ],
    ],
    'translator' => [
//        'locale' => 'de_DE', // BitkornListener->setupLocalization() kann das hier nicht überschreiben
        'translation_file_patterns' => [
            /*
             * Das hier kann auch in der Module.php->onBootstrap()
             * $this->translator->addTranslationFilePattern($type, $baseDir, $pattern)
             * 
             * zum kategorisieren kann man z.B.:
             * 'pattern'  => 'bitkorn_%s.mo'
             * ...%s ist der language code
             */
            [
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
                'text_domain' => 'bitkornshop'
            ],
        ],
    ],
    'view_manager' => [
        'template_map' => [
            // Common
            'template/common/shop-footer' => __DIR__ . '/../view/template/common/shop-footer.phtml',
            'template/article/rating/bar-rating' => __DIR__ . '/../view/template/article/rating/bar-rating.phtml',
            'template/article/rating/article-ratings' => __DIR__ . '/../view/template/article/rating/article-ratings.phtml',
            // Article
            'template/article/frontend/articlesPreview' => __DIR__ . '/../view/template/article/frontend/articlesPreview.phtml',
            'template/article/frontend/articlePreview' => __DIR__ . '/../view/template/article/frontend/articlePreview.phtml',
            'template/article/category/shopArticleCategoryUnorderedList' => __DIR__ . '/../view/template/article/category/shopArticleCategoryUnorderedList.phtml',
            'template/article/category/shopArticleCategorySelect' => __DIR__ . '/../view/template/article/category/shopArticleCategorySelect.phtml',
            'template/article/stock/articleStockWidget' => __DIR__ . '/../view/template/article/stock/articleStockWidget.phtml',
            // Article Options
            'template/article/option/optionSelect' => __DIR__ . '/../view/template/article/option/optionSelect.phtml',
            'template/article/option/optionSizedef' => __DIR__ . '/../view/template/article/option/optionSizedef.phtml',
            'template/article/option/shopArticleOptionsFromJson' => __DIR__ . '/../view/template/article/option/shopArticleOptionsFromJson.phtml',
            'template/article/option/shopArticleOptionsByArticleId' => __DIR__ . '/../view/template/article/option/shopArticleOptionsByArticleId.phtml',
            // article param2d
            'template/article/param2d/param2dSelect' => __DIR__ . '/../view/template/article/param2d/param2dSelect.phtml',
            // Basket
            'template/basket/frontend/shopBasketOverview' => __DIR__ . '/../view/template/basket/frontend/shopBasketOverview.phtml',
            'template/basket/admin/basketStatusSelect' => __DIR__ . '/../view/template/basket/admin/basketStatusSelect.phtml',
            // Email Drafts
            'template/basket/email/shopBasketSummary' => __DIR__ . '/../view/template/basket/email/shopBasketSummary.phtml',
            'template/basket/email/shopBasketShipped' => __DIR__ . '/../view/template/basket/email/shopBasketShipped.phtml',
            'template/basket/email/confirmation/shopBasketOrdered' => __DIR__ . '/../view/template/basket/email/confirmation/shopBasketOrdered.phtml',
            'template/basket/email/confirmation/shopBasketPayed' => __DIR__ . '/../view/template/basket/email/confirmation/shopBasketPayed.phtml',
            'template/basket/email/confirmation/shopBasketShipped' => __DIR__ . '/../view/template/basket/email/confirmation/shopBasketShipped.phtml',
            'template/basket/email/rating/ratingReminder' => __DIR__ . '/../view/template/basket/email/rating/ratingReminder.phtml',
            // view replacements
            'frontend/article/article-frontend/template/articleTypeGroup' => __DIR__ . '/../view/bitkorn/frontend/article/article-frontend/template/articleTypeGroup.phtml',
            // DirectEditInput
            'template/form/directEditInput/text' => __DIR__ . '/../view/template/form/directEditInput/text.phtml',
            'template/form/directEditInput/textarea' => __DIR__ . '/../view/template/form/directEditInput/textarea.phtml',
            'template/form/directEditInput/radio' => __DIR__ . '/../view/template/form/directEditInput/radio.phtml',
            // Address
            'template/address/address-display' => __DIR__ . '/../view/template/address/address-display.phtml',
            // address form basket
            'template/form/basket/address/invoice-private' => __DIR__ . '/../view/template/form/basket/address/invoice-private.phtml',
            'template/form/basket/address/invoice-enterprise' => __DIR__ . '/../view/template/form/basket/address/invoice-enterprise.phtml',
            'template/form/basket/address/shipment-private' => __DIR__ . '/../view/template/form/basket/address/shipment-private.phtml',
            'template/form/basket/address/shipment-enterprise' => __DIR__ . '/../view/template/form/basket/address/shipment-enterprise.phtml',
            // address form user
            'template/form/user/address/invoice-private' => __DIR__ . '/../view/template/form/user/address/invoice-private.phtml',
            'template/form/user/address/invoice-enterprise' => __DIR__ . '/../view/template/form/user/address/invoice-enterprise.phtml',
            'template/form/user/address/shipment-private' => __DIR__ . '/../view/template/form/user/address/shipment-private.phtml',
            'template/form/user/address/shipment-enterprise' => __DIR__ . '/../view/template/form/user/address/shipment-enterprise.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
    'view_helper_config' => [
    ],
    'bitkorn_shop' => [
        'document_path_absolute' => __DIR__ . '/../../../../data/files/documents', // + computeBrandDateFolder()
        'image_path_absolute' => __DIR__ . '/../../../public/img/shop',
        'image_path_relative' => '/img/shop',
        'image_thumb_width' => 256,
        'image_thumb_heigth' => 256,
        'image_thumb_width_small' => 64,
        'image_thumb_heigth_small' => 64,
        'image_thumb_width_medium' => 200, // 200 is preffered e.g. from facebook
        'image_thumb_heigth_medium' => 200, // 200 is preffered e.g from facebook
        'after_payment_redirect_route' => 'shop_frontend_basket_afterpayment',
        'after_payment_redirect_route_pos' => 'shop_admin_basket_basketadmin_basket',
        'after_payment_redirect_route_param_name' => 'basket_unique',
        'shipping_service_provider' => [
            'ups' => ShippingProviderUps::class,
            'cp' => Service\Shipping\ShippingProvider\ShippingProviderCustomerPickup::class,
        ],
        'dbdumps_path_absolute' => __DIR__ . '/../data/dbDumps',
        'tax_default' => 19,
    ]
];
