<?php

namespace Bitkorn\Shop\Controller\Admin\User;

use Bitkorn\Shop\Entity\User\ShopUserAddressEntity;
use Bitkorn\Shop\Form\User\ShopUserAddressForm;
use Bitkorn\Shop\Form\User\ShopUserCreateForm;
use Bitkorn\Shop\Form\User\ShopUserForm;
use Bitkorn\Shop\Service\User\ShopUserNumberService;
use Bitkorn\Shop\Service\User\ShopUserService;
use Bitkorn\Shop\Table\User\ShopUserAddressTable;
use Bitkorn\Shop\Table\User\ShopUserDataNumberTable;
use Bitkorn\Shop\Table\User\ShopUserFeeTable;
use Bitkorn\Shop\Table\User\ShopUserGroupTable;
use Bitkorn\Trinket\Service\I18n\NumberFormatService;
use Bitkorn\User\Service\Registration\UserRegistrationService;
use Bitkorn\User\Table\User\Role\UserRoleRelationTable;
use Bitkorn\User\Table\User\Role\UserRoleTable;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\Response;
use Laminas\View\Model\ViewModel;
use Laminas\View\Model\JsonModel;
use Bitkorn\Shop\Controller\AbstractShopController;

/**
 *
 * @author allapow
 */
class ShopUserAdminController extends AbstractShopController
{

    /**
     * @var ShopUserCreateForm
     */
    protected $shopUserCreateForm;

    /**
     * @var ShopUserForm
     */
    protected $shopUserForm;

    /**
     * @var ShopUserAddressForm
     */
    protected $shopUserAddressForm;

    /**
     * @var ShopUserService
     */
    protected $shopUserService;

    /**
     * @var UserRegistrationService
     */
    protected $userRegistrationService;

    /**
     * @var ShopUserNumberService
     */
    protected $shopUserNumberService;

    /**
     * @var ShopUserDataNumberTable
     */
    protected $shopUserDataNumberTable;

    /**
     * @var ShopUserGroupTable
     */
    protected $shopUserGroupTable;

    /**
     * @var UserRoleRelationTable
     */
    protected $userRoleRelationTable;

    /**
     * @var UserRoleTable
     */
    protected $userRoleTable;

    /**
     * @var ShopUserFeeTable
     */
    protected $shopUserFeeTable;

    /**
     * @var NumberFormatService
     */
    protected $numberFormatService;

    /**
     * @var ShopUserAddressTable
     */
    protected $shopUserAddressTable;

    /**
     * @param ShopUserCreateForm $shopUserCreateForm
     */
    public function setShopUserCreateForm(ShopUserCreateForm $shopUserCreateForm): void
    {
        $this->shopUserCreateForm = $shopUserCreateForm;
    }

    /**
     * @param ShopUserForm $shopUserForm
     */
    public function setShopUserForm(ShopUserForm $shopUserForm): void
    {
        $this->shopUserForm = $shopUserForm;
    }

    /**
     * @param ShopUserAddressForm $shopUserAddressForm
     */
    public function setShopUserAddressForm(ShopUserAddressForm $shopUserAddressForm): void
    {
        $this->shopUserAddressForm = $shopUserAddressForm;
    }

    /**
     * @param ShopUserService $shopUserService
     */
    public function setShopUserService(ShopUserService $shopUserService): void
    {
        $this->shopUserService = $shopUserService;
    }

    /**
     * @param UserRegistrationService $userRegistrationService
     */
    public function setUserRegistrationService(UserRegistrationService $userRegistrationService): void
    {
        $this->userRegistrationService = $userRegistrationService;
    }

    /**
     * @param ShopUserNumberService $shopUserNumberService
     */
    public function setShopUserNumberService(ShopUserNumberService $shopUserNumberService): void
    {
        $this->shopUserNumberService = $shopUserNumberService;
    }

    /**
     * @param ShopUserDataNumberTable $shopUserDataNumberTable
     */
    public function setShopUserDataNumberTable(ShopUserDataNumberTable $shopUserDataNumberTable): void
    {
        $this->shopUserDataNumberTable = $shopUserDataNumberTable;
    }

    /**
     * @param ShopUserGroupTable $shopUserGroupTable
     */
    public function setShopUserGroupTable(ShopUserGroupTable $shopUserGroupTable): void
    {
        $this->shopUserGroupTable = $shopUserGroupTable;
    }

    /**
     * @param UserRoleRelationTable $userRoleRelationTable
     */
    public function setUserRoleRelationTable(UserRoleRelationTable $userRoleRelationTable)
    {
        $this->userRoleRelationTable = $userRoleRelationTable;
    }

    public function setUserRoleTable(UserRoleTable $userRoleTable)
    {
        $this->userRoleTable = $userRoleTable;
    }

    public function setShopUserFeeTable(ShopUserFeeTable $shopUserFeeTable)
    {
        $this->shopUserFeeTable = $shopUserFeeTable;
    }

    public function setNumberFormatService(NumberFormatService $numberFormatService): void
    {
        $this->numberFormatService = $numberFormatService;
    }

    /**
     * @param ShopUserAddressTable $shopUserAddressTable
     */
    public function setShopUserAddressTable(ShopUserAddressTable $shopUserAddressTable): void
    {
        $this->shopUserAddressTable = $shopUserAddressTable;
    }

    /**
     * @return ViewModel|Response
     */
    public function shopUsersAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(2) && !$this->userService->checkUserRoleAccess(101)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $viewModel = new ViewModel();
        $viewModel->setVariable('isManager', $this->userService->checkUserRoleAccessMin(2));
        $this->layout('layout/admin');

        /*
         * search parameter
         */
        $shopUserGroupId = 0;
        $onlyWithRole = false;

        $request = $this->getRequest();
        if ($request->isPost() && $request instanceof Request) {
            $postData = $request->getPost()->toArray();
            if (isset($postData['shop_user_group_id'])) {
                $shopUserGroupId = filter_var($postData['shop_user_group_id'], FILTER_SANITIZE_NUMBER_INT);
            }
            if (!empty($postData['only_with_role'])) {
                $onlyWithRole = true;
            }
        }
        $viewModel->setVariable('shopUserGroupId', $shopUserGroupId);
        $viewModel->setVariable('onlyWithRole', $onlyWithRole);

        $createByUserUuid = '';
        if (!$this->userService->checkUserRoleAccessMin(5) && $this->userService->checkUserRoleAccess(101)) {
            $createByUserUuid = $this->userService->getUserUuid();
        }

        $shopUsersSearch = $this->shopUserService->searchShopUsers([
            'shop_user_group_id' => $shopUserGroupId,
            'only_with_role' => $onlyWithRole,
            'order_by' => 'user_time_create',
            'order_direc' => 'DESC',
            'create_by_user_uuid' => $createByUserUuid
        ]);
        $viewModel->setVariable('shopUsersSearch', $shopUsersSearch);

        $shopUserGroupsIdAssoc = $this->shopUserService->getShopUserGroupIdAssoc();
        $viewModel->setVariable('shopUserGroupsIdAssoc', $shopUserGroupsIdAssoc);

        return $viewModel;
    }

    /**
     *
     * @return ViewModel|Response
     */
    public function addShopUserAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(2) && !$this->userService->checkUserRoleAccess(101)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $viewModel = new ViewModel();
        $this->layout('layout/admin');
        $this->shopUserCreateForm->init();
        $viewModel->setVariable('shopUserCreateForm', $this->shopUserCreateForm);

        $request = $this->getRequest();
        if ($request->isPost() && $request instanceof Request) {
            $postData = $request->getPost()->toArray();
            $this->shopUserCreateForm->setData($postData);
            if ($this->shopUserCreateForm->isValid()) {
                $formData = $this->shopUserCreateForm->getData();
                /**
                 * @todo die drei Dinger in die Form packen
                 */
                $formData['user_active'] = 1;
                $formData['user_lang_iso'] = 'de';
                $formData['user_role_id'] = 5;
                if (empty($userUuid = $this->userService->createNewUser($formData)) || !$this->userService->updateUserActive($userUuid, 1)) {
                    $this->layout()->message = [
                        'level' => 'error',
                        'text' => 'Fehler bei der Speicherung der Userdaten.'
                    ];
                    return $viewModel;
                }
                $shopUserDataNumberId = $this->shopUserService->createNewShopUserDataNumber($userUuid, $formData['shop_user_group_id']);
                if (empty($shopUserDataNumberId)) {
                    $this->layout()->message = [
                        'level' => 'error',
                        'text' => 'Fehler bei der Speicherung von shop_user_data_number.'
                    ];
                    return $viewModel;
                }
                if ($this->shopUserService->saveNewShopUserData([
                        'user_uuid' => $userUuid,
                        'shop_user_group_id' => $formData['shop_user_group_id'],
                        'shop_user_data_number_id' => $shopUserDataNumberId,
                        'shop_user_data_user_uuid_create' => $this->userService->getUserUuid()
                    ]) < 1) {
                    $this->layout()->message = [
                        'level' => 'error',
                        'text' => 'Fehler bei der Speicherung von shop_user_data. Bitte dem Administrator mit folgender UserID Bescheid geben: ' . $userUuid
                    ];
                    return $viewModel;
                }
                if ($this->userService->checkUserRoleAccess(101)) {
                    if ($this->shopUserFeeTable->saveNewShopUserFeeMinimal($userUuid, $this->userService->getUserUuid()) < 1) {
                        $this->layout()->message = [
                            'level' => 'error',
                            'text' => 'Fehler bei der Speicherung von shop_user_fee. Bitte dem Administrator mit folgender UserID Bescheid geben: ' . $userUuid
                        ];
                        return $viewModel;
                    } else {
                        return $this->redirect()->toRoute('shop_admin_user_shopuseradmin_shopusers');
                    }
                }
                return $this->redirect()->toRoute('shop_admin_user_shopuseradmin_editshopuser', ['user_uuid' => $userUuid]);
            }
        }

        return $viewModel;
    }

    /**
     * UNUSED !!eigentlich!!
     * Wird eigentlich nicht benoetigt. Der User selbst kann diese Daten aendern!!!
     *
     * @return ViewModel|Response
     */
    public function editShopUserAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(2)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $userUuid = $this->params('user_uuid');
        if (empty($userUuid)) {
            return $this->redirect()->toRoute('shop_admin_user_shopuseradmin_shopusers');
        }
        $viewModel = new ViewModel();
        $viewModel->setVariable('isAdmin', $this->userService->checkUserRoleAccessMin(2));
        $this->layout('layout/admin');

        $userData = $this->shopUserService->getShopUserData($userUuid);
        $viewModel->setVariable('userData', $userData);
        $this->shopUserForm->init();
        $this->shopUserForm->setData($userData);
        $viewModel->setVariable('shopUserForm', $this->shopUserForm);

        $viewModel->setVariable('userRoleRelationsRoleIds', $this->shopUserService->getUserRoleRelationsRoleIds($userUuid));
        $viewModel->setVariable('roles', $this->userService->getRoles());

        $request = $this->getRequest();
        if ($request->isPost() && $request instanceof Request) {
            $postData = $request->getPost()->toArray();
            if (isset($postData['submit'])) {
                $this->shopUserForm->setData($postData);
                if ($this->shopUserForm->isValid()) {
                    $formData = $this->shopUserForm->getData();
                    if ($userData['shop_user_group_id'] != $formData['shop_user_group_id']) {
                        if ($this->shopUserService->updateShopUserDataByUserUuid(['shop_user_group_id' => $formData['shop_user_group_id']],
                            $userUuid)) {
                            $this->layout()->message = ['level' => 'info', 'text' => 'Die Daten wurden geändert!'];
                        }
                    }
                    if ($userData['email'] != $formData['email']) {
                        if ($this->userService->updateUserEmail($userUuid, $formData['email'])) {
                            $this->layout()->message = ['level' => 'info', 'text' => 'Die Daten wurden geändert!'];
                        }
                    }
                }
            }
            if (isset($postData['roleRankingCheck'])) {
                $jsonModel = new JsonModel();
                $userUuid = filter_var($postData['roleRankingCheck']['userUuid'], FILTER_SANITIZE_STRING);
                $roleId = (int)$postData['roleRankingCheck']['roleId'];
                $roleIdChecked = filter_var($postData['roleRankingCheck']['roleIdChecked'], FILTER_VALIDATE_BOOLEAN);
                if ($roleIdChecked) {
                    $jsonModel->setVariable('status', $this->userService->setUserRoleRelation($userUuid, $roleId));
                } else {
                    $jsonModel->setVariable('status', $this->userService->deleteUserRoleRelation($userUuid, $roleId));
                }
                return $jsonModel;
            }
        }

        return $viewModel;
    }

    /**
     *
     * @return ViewModel|Response
     * @throws \Exception
     */
    public function editShopUserAddressAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(2) && !$this->userService->checkUserRoleAccess(101)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $userUuid = $this->params('user_uuid');
        if (empty($userUuid)) {
            return $this->redirect()->toRoute('shop_admin_user_shopuseradmin_shopusers');
        }
        $viewModel = new ViewModel();
        $viewModel->setVariable('userId', $userUuid);
        $this->layout('layout/admin');

        $userAddressCount = $this->shopUserService->countShopUserAddress($userUuid);

        $request = $this->getRequest();
        if ($request->isPost() && $request instanceof Request) {
            $postData = $request->getPost()->toArray();
            if ($postData['shop_user_address_id'] == 0) {
                $this->shopUserAddressForm->setShopUserAddressIdRequired(false);
                $postData['shop_user_address_id'] = null;
            }
            if ($postData['shop_user_address_customer_type'] == 'private') {
                $this->shopUserAddressForm->setTaxIdEnabled(false);
                $this->shopUserAddressForm->setCompanyNameEnabled(false);
                $this->shopUserAddressForm->setCompanyDepartmentEnabled(false);
            } else if ($postData['shop_user_address_customer_type'] == 'enterprise') {
                $this->shopUserAddressForm->setTaxIdEnabled(true);
                $this->shopUserAddressForm->setCompanyNameEnabled(true);
                $this->shopUserAddressForm->setCompanyDepartmentEnabled(true);
            }
            if ($userAddressCount < 2 && $postData['shop_user_address_customer_type'] == 'private') {
                $this->shopUserAddressForm->setBirthdayEnabled(true);
            } else {
                $this->shopUserAddressForm->setBirthdayEnabled(false);
            }

            if (!empty($postData['shop_user_address_birthday'])) {
                $birthdayDateTime = new \DateTime($postData['shop_user_address_birthday']);
                if ($birthdayDateTime) {
                    $postData['shop_user_address_birthday'] = $birthdayDateTime->getTimestamp();
                }
            }

            $this->shopUserAddressForm->init();
            $this->shopUserAddressForm->setData($postData);
            $showUserAddressForm = true;
            if ($this->shopUserAddressForm->isValid()) {
                $userAddressEntity = new ShopUserAddressEntity();
                $formData = $this->shopUserAddressForm->getData();
                $userAddressEntity->exchangeArray($formData);
                if (!$userAddressEntity->isUserOwner($userUuid)) {
                    $this->getResponse()->setStatusCode(\Laminas\Http\PhpEnvironment\Response::STATUS_CODE_403);
                    return $viewModel;
                }
                $result = $userAddressEntity->saveOrUpdate($this->shopUserAddressTable, $userAddressCount);
                if ($result) {
                    $this->layout()->message = [
                        'level' => 'success',
                        'text' => 'Die Adresse wurde gespeichert'
                    ];
                    $userAddressCount = $this->shopUserService->countShopUserAddress($userUuid);
                    $showUserAddressForm = false;
                } else {
                    $this->layout()->message = [
                        'level' => 'warn',
                        'text' => 'Es ist ein Fehler aufgetreten. Bitte versuchen sie es erneut!'
                    ];
                }
            }
            if ($showUserAddressForm) {
                if (!empty($postData['shop_user_address_birthday'])) {
                    $this->shopUserAddressForm->get('shop_user_address_birthday')->setValue(strftime('%d.%m.%Y',
                        $postData['shop_user_address_birthday']));
                }
                $viewModel->setVariable('shopUserAddressForm', $this->shopUserAddressForm);
            }
        }

        $viewModel->setVariable('userAddressCount', $userAddressCount);
        return $viewModel;
    }

    /**
     *
     * @return ViewModel|Response
     */
    public function shopUserAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(5) && !$this->userService->checkUserRoleAccess(101)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $userUuid = $this->params('user_uuid');
        if (empty($userUuid)) {
            return $this->redirect()->toRoute('shop_admin_user_shopuseradmin_shopusers');
        }
        $viewModel = new ViewModel();
        $this->layout('layout/admin');

        $userData = $this->shopUserService->getShopUserData($userUuid);
        $viewModel->setVariable('userData', $userData);

        return $viewModel;
    }

}
