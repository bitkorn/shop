<?php

namespace Bitkorn\Shop\Controller\Admin\User\Fee;

use Bitkorn\Shop\Entity\User\Fee\ShopBasketFeeEntity;
use Bitkorn\Shop\Entity\User\Fee\ShopUserFeeEntity;
use Bitkorn\Shop\Form\User\Bank\ShopUserBankForm;
use Bitkorn\Shop\Form\User\Fee\ShopUserFeeForm;
use Bitkorn\Shop\Service\Basket\BasketAdminService;
use Bitkorn\Shop\Service\User\ShopUserFeeService;
use Bitkorn\Shop\Service\User\ShopUserService;
use Bitkorn\Trinket\Service\I18n\NumberFormatService;
use PayPal\Api\Currency;
use PayPal\Api\Payout;
use PayPal\Api\PayoutItem;
use PayPal\Api\PayoutSenderBatchHeader;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\Response;
use Laminas\View\Model\ViewModel;
use Bitkorn\Shop\Controller\AbstractShopController;

/**
 * Description of ShopUserFeeAdminController
 *
 * @author allapow
 */
class ShopUserFeeAdminController extends AbstractShopController
{

    /**
     * @var array
     */
    protected $shopPaypalConfig;

    /**
     * @var ShopUserFeeService
     */
    protected $shopUserFeeService;

    /**
     * @var BasketAdminService
     */
    protected $basketAdminService;

    /**
     *
     * @var NumberFormatService
     */
    private $numberFormatService;

    /**
     *
     * @var ShopUserFeeForm
     */
    private $shopUserFeeForm;

    /**
     *
     * @var ShopUserBankForm
     */
    private $shopUserBankForm;

    /**
     * @var ShopUserService
     */
    protected $shopUserService;

    /**
     * @param array $shopPaypalConfig
     */
    public function setShopPaypalConfig(array $shopPaypalConfig): void
    {
        $this->shopPaypalConfig = $shopPaypalConfig;
    }

    /**
     * @param ShopUserFeeService $shopUserFeeService
     */
    public function setShopUserFeeService(ShopUserFeeService $shopUserFeeService): void
    {
        $this->shopUserFeeService = $shopUserFeeService;
    }

    /**
     * @param BasketAdminService $basketAdminService
     */
    public function setBasketAdminService(BasketAdminService $basketAdminService): void
    {
        $this->basketAdminService = $basketAdminService;
    }

    /**
     *
     * @param NumberFormatService $numberFormatService
     */
    public function setNumberFormatService(NumberFormatService $numberFormatService)
    {
        $this->numberFormatService = $numberFormatService;
    }

    /**
     *
     * @param ShopUserFeeForm $shopUserFeeForm
     */
    public function setShopUserFeeForm(ShopUserFeeForm $shopUserFeeForm)
    {
        $this->shopUserFeeForm = $shopUserFeeForm;
    }

    /**
     *
     * @param ShopUserBankForm $shopUserBankForm
     */
    public function setShopUserBankForm(ShopUserBankForm $shopUserBankForm)
    {
        $this->shopUserBankForm = $shopUserBankForm;
    }

    /**
     * @param ShopUserService $shopUserService
     */
    public function setShopUserService(ShopUserService $shopUserService): void
    {
        $this->shopUserService = $shopUserService;
    }

    /**
     *
     * @return ViewModel|Response
     */
    public function shopUserFeeAction()
    {
        $userUuid = $this->params('user_uuid');
        if (empty($userUuid) || $userUuid != $this->userService->getUserUuid()) {
            return $this->redirect()->toRoute('shop_admin_user_shopuseradmin_shopusers');
        }
        if (!$this->userService->checkUserRoleAccessMin(2) && (!$this->userService->checkUserRoleAccess(101))) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $onlySalesStaff = false;
        if (!$this->userService->checkUserRoleAccessMin(5) && $this->userService->checkUserRoleAccess(101)) {
            $onlySalesStaff = true;
        }
        $viewModel = new ViewModel();
        $viewModel->setVariable('isManager', $this->userService->checkUserRoleAccessMin(2));
        $this->layout('layout/admin');
        $userData = $this->shopUserService->getShopUserData($userUuid);
        $userFeeData = $this->shopUserFeeService->getShopUserFeeByUserId($userUuid);
        $shopUserFeeEntity = new ShopUserFeeEntity();
        $shopUserFeeEntity->flipMapping();
        $shopUserFeeEntity->exchangeArray($userFeeData);

        $this->shopUserFeeForm->setUserId($userUuid);
        $readonly = false;
        if (!$this->userService->checkUserRoleAccessMin(2)) {
            $this->shopUserFeeForm->setReadOnly(true);
            $readonly = true;
        } else {
            $this->layout()->helpId = 6;
        }
        $viewModel->setVariable('readonly', $readonly);
        $this->shopUserFeeForm->init();
        $this->shopUserFeeForm->setData($shopUserFeeEntity->storage);
        $this->shopUserFeeForm->get('user_uuid')->setValue($userUuid);

        $shopBasketFeeEntity = new ShopBasketFeeEntity();
        $shopBasketFeeEntity->exchangeArrayBasketFee($this->basketAdminService->getShopBasketByVendorUserId($userUuid));
        $shopBasketFeeEntity->exchangeArrayBasketFee($this->basketAdminService->getShopBasketByVendorUserIdMlm($userUuid));
        $shopBasketFeeEntity->computeFee();

        $this->shopUserBankForm->init();

        $request = $this->getRequest();
        if ($request->isPost() && $request instanceof Request) {
            $postData = $request->getPost()->toArray();
            if (!$readonly && isset($postData['shop_user_fee_submit'])) {
                if ($postData['user_uuid'] != $userUuid) {
                    exit('was soll das?');
                }
                $shopUserFeeEntity->flipMappingBack();
                $shopUserFeeEntity->exchangeArrayNumberFormat($postData, $this->numberFormatService);
                $this->shopUserFeeForm->setData($shopUserFeeEntity->storage);
                if ($this->shopUserFeeForm->isValid()) {
                    $shopUserFeeEntity->exchangeArray($this->shopUserFeeForm->getData());
                    $shopUserFeeEntity->saveOrUpdate($this->shopUserFeeService->getShopUserFeeTable());
                } else {
                    $this->layout()->message = ['level' => 'warn', 'text' => 'Nicht alle Eingaben sind OK!'];
                }
            }
            if (!empty($postData['shop_user_data_paypal_email']) && empty($postData['shop_user_data_paypal_email_delete'])) {
                $payPalEmail = filter_input(INPUT_POST, 'shop_user_data_paypal_email', FILTER_VALIDATE_EMAIL);
                $emailValidator = new \Laminas\Validator\EmailAddress();
                if ($emailValidator->isValid($payPalEmail)) {
                    $this->shopUserFeeService->updateShopUserFeePayPalEmail($userUuid, $payPalEmail);
                    $userData = $this->shopUserService->getShopUserData($userUuid);
                } else {
                    $payPalEmailMessages = $emailValidator->getMessages();
                    $viewModel->setVariable('payPalEmailMessage', array_shift($payPalEmailMessages));
                }
            } elseif (!empty($postData['shop_user_data_paypal_email_delete'])) {
                $this->shopUserFeeService->updateShopUserFeePayPalEmail($userUuid, '');
                $userData = $this->shopUserService->getShopUserData($userUuid);
            }
            if (!empty($postData['submit_shop_user_data_bank'])) {
                str_replace(' ', '', $postData['shop_user_data_bank_iban']);
                $this->shopUserBankForm->setData($postData);
                if ($this->shopUserBankForm->isValid()) {
                    $formData = $this->shopUserBankForm->getData();
                    if ($this->shopUserService->updateShopUserDataBank($userUuid, $formData['shop_user_data_bank_owner_name'],
                            $formData['shop_user_data_bank_bank_name'], $formData['shop_user_data_bank_iban'], $formData['shop_user_data_bank_bic']) >= 0) {
                        $userData = $this->shopUserService->getShopUserData($userUuid);
                    } else {
                        $this->layout()->message = ['level' => 'error', 'text' => 'Es gab einen Fehler beim Speichern der Daten.'];
                    }
                }
            }
            /*
             * do fee-pay-action
             */
            $csvAll = (isset($postData['shop_user_fee_paid_action']) && $postData['shop_user_fee_paid_action'] == 'csv_all') ? true : false;
            if ((!empty($postData['shop_user_fee_paid_action']) && !empty($postData['shop_user_fee_paid_basket_id']) && is_array($postData['shop_user_fee_paid_basket_id']) && !empty($userFeeData)) || $csvAll) {
                $feeBasketIdsChecked = [];
                if (!$csvAll) {
                    foreach ($postData['shop_user_fee_paid_basket_id'] as $feeBasketId) {
                        $feeBasketIdsChecked[] = filter_var($feeBasketId, FILTER_SANITIZE_NUMBER_INT);
                    }
                }
                $timeCurrent = time();
                switch ($postData['shop_user_fee_paid_action']) {
                    case 'csv':
                    case 'csv_all':
                        $csv = '"Rechn.-Nr.";"Kunden Name";"Datum Order";"Artikel Netto";"Provision";"Provision Typ";"Provision ausgezahlt";"am";"PayID";"PayMethod"';
                        $csv .= "\r\n";
                        foreach ($shopBasketFeeEntity as $basketFeeEntityItem) {
                            if (in_array($basketFeeEntityItem['shop_basket_entity_id'], $feeBasketIdsChecked) || $postData['shop_user_fee_paid_action'] == 'csv_all') {
                                $csv .= '"' . ($basketFeeEntityItem['shop_document_invoice_number'] ?: '') . '"';
                                $csv .= ';"' . ($basketFeeEntityItem['shop_basket_address_name'] ?: $basketFeeEntityItem['shop_user_address_name']) . '"';
                                $csv .= ';"' . date('d.m.Y', $basketFeeEntityItem['shop_basket_time_order']) . '"';
                                $csv .= ';"' . $this->numberFormatService->format($basketFeeEntityItem['computet_basketfee_shopping_value_net']) . '"';
                                $csv .= ';"' . $this->numberFormatService->format($basketFeeEntityItem['computet_basketfee_fee']) . '"';
                                $csv .= ';"' . $basketFeeEntityItem['computet_basketfee_type'] . '"';
                                $csv .= ';"' . $this->numberFormatService->format($basketFeeEntityItem['shop_basket_fee_paid_sum']) . '"';
                                $csv .= ';"' . (!empty($basketFeeEntityItem['shop_user_fee_paid_time']) ? date('d.m.Y',
                                        $basketFeeEntityItem['shop_user_fee_paid_time']) : '') . '"';
                                $csv .= ';"' . (!empty($basketFeeEntityItem['shop_user_fee_paid_payment_id']) ? $basketFeeEntityItem['shop_user_fee_paid_payment_id'] : '') . '"';
                                $csv .= ';"' . (!empty($basketFeeEntityItem['shop_user_fee_paid_payment_method']) ? $basketFeeEntityItem['shop_user_fee_paid_payment_method'] : '') . '"';
                                $csv .= "\r\n";
                            }
                        }
                        header('Content-Type: text/csv; charset=utf-8');
                        header('Content-Disposition: attachment; filename=VerkaufProvisionUebersicht.csv');
                        header("Pragma: no-cache");
                        header("Expires: 0");
                        echo $csv;
                        exit();
                        break;
                    case 'check':
                        if ($readonly) {
                            // only hacker can be here
                            return $viewModel;
                        }
                        foreach ($shopBasketFeeEntity as $basketFeeEntityItem) {
                            if (in_array($basketFeeEntityItem['shop_basket_entity_id'], $feeBasketIdsChecked)) {
                                if (!$this->shopUserFeeService->existShopUserFeePaidByBasketEntityId($basketFeeEntityItem['shop_basket_entity_id'])) {
                                    $this->shopUserFeeService->saveNewShopUserFeePaid($userUuid, $basketFeeEntityItem['shop_basket_entity_id'],
                                        $basketFeeEntityItem['computet_basketfee_fee'], $timeCurrent);
                                }
                            }
                        }
                        break;
                    case 'paypal':
                        if ($readonly) {
                            // only hacker can be here
                            return $viewModel;
                        }
                        if (!empty($userData['shop_user_data_paypal_email'])) {
                            $paySum = 0;
                            $basketEntityIds = [];
                            foreach ($shopBasketFeeEntity as $basketFeeEntityItem) {
                                if (in_array($basketFeeEntityItem['shop_basket_entity_id'], $feeBasketIdsChecked)) {
                                    if (!$this->shopUserFeeService->existShopUserFeePaidByBasketEntityId($basketFeeEntityItem['shop_basket_entity_id'])) {
                                        $basketEntityIds[$basketFeeEntityItem['shop_basket_entity_id']] = $basketFeeEntityItem['computet_basketfee_fee'];
                                        $paySum += $basketFeeEntityItem['computet_basketfee_fee'];
                                    }
                                }
                            }
                            if ($paySum > 0) {
                                $payouts = new Payout();
                                $senderBatchHeader = new PayoutSenderBatchHeader();
                                $senderBatchHeader->setEmailSubject('Provisionsauszahlung');
                                $senderItem1 = new PayoutItem();
                                $currency = new Currency();
                                $currency->setCurrency('EUR');
                                $currency->setValue($paySum);
                                $senderItem1->setRecipientType('Email')
                                    ->setNote('Thanks you.')
                                    ->setReceiver($userData['shop_user_data_paypal_email'])
                                    /**
                                     * @todo Eine eigene UniqueID (Quittungsnummer oder so) benutzen
                                     */
                                    ->setSenderItemId('item_1' . uniqid())
                                    ->setAmount($currency);
                                $payouts->setSenderBatchHeader($senderBatchHeader)->addItem($senderItem1);
                                try {
                                    $output = $payouts->create(null, $this->getApiContect());
                                } catch (\Exception $ex) {
                                    $this->logger->err('Created Batch Payout with PayPal. $payouts: ' . $payouts->toJSON());
                                    $this->logger->err('Created Batch Payout with PayPal. Message: ' . $ex->getMessage());
                                    $this->layout()->message = ['level' => 'error', 'text' => $ex->getMessage()];
                                }
                                if (isset($output) && $output instanceof \PayPal\Api\PayoutBatch && empty($output->getBatchHeader()->getErrors())) {
                                    foreach ($basketEntityIds as $basketEntityId => $fee) {
                                        $this->shopUserFeeService->saveNewShopUserFeePaid($userUuid, $basketEntityId, $fee, $timeCurrent, $output->getBatchHeader()->getPayoutBatchId(), 'paypal');
                                    }
                                }
                                $viewModel->setVariable('payoutBatchId', $output->getBatchHeader()->getPayoutBatchId());
                                $viewModel->setVariable('paypalOutput', $output->toJSON());
                                break;
                            } else {
                                $this->layout()->message = ['level' => 'info', 'text' => 'Die Summe der auszuzahlenden Provisionen ergab Null!'];
                            }
                        } else {
                            $this->layout()->message = ['level' => 'warn', 'text' => 'Es ist keine PayPal Emailadresse angegeben!'];
                        }
                }
            }
            $shopBasketFeeEntity->exchangeArrayBasketFee($this->basketAdminService->getShopBasketByVendorUserId($userUuid), true);
            $shopBasketFeeEntity->exchangeArrayBasketFee($this->basketAdminService->getShopBasketByVendorUserIdMlm($userUuid));
            $shopBasketFeeEntity->computeFee();
        }

        $viewModel->setVariable('userData', $userData);

        $viewModel->setVariable('numberFormatService', $this->numberFormatService);

        $viewModel->setVariable('shopUserFeeForm', $this->shopUserFeeForm);

        $viewModel->setVariable('shopBasketFeeEntity', $shopBasketFeeEntity);

        $viewModel->setVariable('mlmChilds', $this->shopUserFeeService->getShopUserMlmChildsByUserId($userUuid));

        $this->shopUserBankForm->setData($userData);
        $viewModel->setVariable('shopUserBankForm', $this->shopUserBankForm);

        return $viewModel;
    }

    /**
     *
     * @return ApiContext
     */
    private function getApiContect()
    {
        $apiContext = new ApiContext(
            new OAuthTokenCredential(
                $this->shopPaypalConfig['client_id'], // ClientID
                $this->shopPaypalConfig['secret']      // ClientSecret
            )
        );
        $apiContext->setConfig(
            [
                'log.LogEnabled' => true,
                'log.FileName' => $this->shopPaypalConfig['logfile'],
                'log.LogLevel' => $this->shopPaypalConfig['loglevel'],
                'mode' => $this->shopPaypalConfig['mode']
            ]
        );
        return $apiContext;
    }
}
