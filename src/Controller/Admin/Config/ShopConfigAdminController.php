<?php

namespace Bitkorn\Shop\Controller\Admin\Config;

use Bitkorn\Shop\Service\ShopService;
use Laminas\Http\Response;
use Laminas\View\Model\ViewModel;
use Bitkorn\Shop\Controller\AbstractShopController;

/**
 *
 * @author allapow
 */
class ShopConfigAdminController extends AbstractShopController
{

    /**
     * @var ShopService
     */
    protected $shopService;

    /**
     * @param ShopService $shopService
     */
    public function setShopService(ShopService $shopService): void
    {
        $this->shopService = $shopService;
    }

    /**
     *
     * @return ViewModel|Response
     */
    public function shopConfigsAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(3)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $viewModel = new ViewModel();
        $this->layout('layout/admin');
        $this->layout()->helpId = 1;

        $configs = $this->shopService->getShopConfigurationsGrouped();
        $viewModel->setVariable('configs', $configs);

        return $viewModel;
    }

}
