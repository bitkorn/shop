<?php

namespace Bitkorn\Shop\Controller\Admin\Basket\Claim;

use Bitkorn\Shop\Form\Basket\Claim\ShopBasketClaimFileForm;
use Bitkorn\Shop\Form\Basket\Claim\ShopBasketClaimForm;
use Bitkorn\Shop\Service\Claim\ShopClaimService;
use Bitkorn\Trinket\Service\Filesystem\FolderTool;
use Bitkorn\Trinket\Tools\Filesystem\FilenameTool;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\Response;
use Laminas\View\Model\ViewModel;
use Laminas\View\Model\JsonModel;
use Bitkorn\Shop\Controller\AbstractShopController;

class ClaimAdminController extends AbstractShopController
{

    protected ShopClaimService $shopClaimService;
    protected ShopBasketClaimForm $shopBasketClaimForm;
    protected ShopBasketClaimFileForm $shopBasketClaimFileForm;
    protected FolderTool $folderTool;

    public function setShopClaimService(ShopClaimService $shopClaimService): void
    {
        $this->shopClaimService = $shopClaimService;
    }

    public function setShopBasketClaimForm(ShopBasketClaimForm $shopBasketClaimForm): void
    {
        $this->shopBasketClaimForm = $shopBasketClaimForm;
    }

    public function setShopBasketClaimFileForm(ShopBasketClaimFileForm $shopBasketClaimFileForm): void
    {
        $this->shopBasketClaimFileForm = $shopBasketClaimFileForm;
    }

    public function setFolderTool(FolderTool $folderTool): void
    {
        $this->folderTool = $folderTool;
    }

    /**
     * @return ViewModel|Response
     */
    public function claimsAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            return $this->redirect()->toRoute('shop_admin_basket_basketadmin_baskets');
        }
        $viewModel = new ViewModel();

        $onlyUnfinished = true;
        $showall = $this->params()->fromQuery('showall');
        $viewModel->setVariable('showall', $showall);
        if ($showall == 1) {
            $onlyUnfinished = false;
        }

        $claims = $this->shopClaimService->getShopBasketClaims($onlyUnfinished);
        $viewModel->setVariable('claims', $claims);

        $request = $this->getRequest();
        if ($request->isPost() && $request instanceof Request) {
            $postData = $request->getPost()->toArray();
        }

        $this->layout('layout/admin');

        return $viewModel;
    }

    /**
     * @return ViewModel|Response
     */
    public function claimsFromBasketAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            return $this->redirect()->toRoute('shop_admin_basket_basketadmin_baskets');
        }
        $viewModel = new ViewModel();

        $basketUnique = $this->params('basket_unique');


        $request = $this->getRequest();
        if ($request->isPost() && $request instanceof Request) {
            $postData = $request->getPost()->toArray();
        }

        $this->layout('layout/admin');

        return $viewModel;
    }

    /**
     * @return ViewModel|Response
     */
    public function claimAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            return $this->redirect()->toRoute('shop_admin_basket_basketadmin_baskets');
        }
        $claimId = $this->params('claim_id');
        if (empty($claimId)) {
            return $this->redirect()->toRoute('shop_admin_basket_claimadmin_claims');
        }
        $viewModel = new ViewModel();
        $this->layout('layout/admin');

        $claim = $this->shopClaimService->getShopBasketClaim($claimId);
        $viewModel->setVariable('claim', $claim);

        $request = $this->getRequest();
        if ($request->isPost() && $request instanceof Request) {
            $postData = $request->getPost()->toArray();
        }

        return $viewModel;
    }

    /**
     * Called to create through POST ...then without shop_basket_claim_id.
     *
     * @return ViewModel|Response
     */
    public function manageClaimAction()
    {
        if ($this->getRequest()->isPost()) {
            $claimId = (int)$this->params()->fromPost('shop_basket_claim_id');
            $basketItemId = (int)$this->params()->fromPost('shop_basket_item_id');
            $basketUnique = filter_var($this->params()->fromPost('shop_basket_unique'), FILTER_SANITIZE_STRING);
        } else {
            $claimId = (int)$this->params('claim_id');
            $basketItemId = (int)$this->params('basket_item_id');
            $basketUnique = filter_var($this->params('basket_unique'), FILTER_SANITIZE_STRING);
        }
        if (!$this->userService->checkUserRoleAccessMin(2) || (empty($basketItemId) && empty($basketUnique))) {
            return $this->redirect()->toRoute('shop_admin_basket_basketadmin_baskets');
        }
        $viewModel = new ViewModel();

        $this->shopBasketClaimForm->init();
        $this->shopBasketClaimFileForm->init();
        $create = false;
        if (!$this->params()->fromPost('create') && !empty($claimId) && !empty($basketItemId) && !empty($basketUnique)) {
            // claim exist
            $this->shopBasketClaimForm->setShopBasketClaimIdRequired(true);
            $selfUrl = $this->url()->fromRoute(
                'shop_admin_basket_claimadmin_manageclaim', ['claim_id' => $claimId, 'basket_item_id' => $basketItemId, 'basket_unique' => $basketUnique]);
            $this->shopBasketClaimForm->setAttribute('action', $selfUrl);
            $this->shopBasketClaimFileForm->setAttribute('action', $selfUrl);
            $shopBasketClaimData = $this->shopClaimService->getShopBasketClaim($claimId);
            $this->shopBasketClaimForm->setData($shopBasketClaimData);
            $this->shopBasketClaimFileForm->setData($shopBasketClaimData);
            $climeData = $this->shopClaimService->getShopBasketClaim($claimId);
            $folder = $this->folderTool->computeBrandDateFolder(
                $this->config['bitkorn_shop']['document_path_absolute']
                , 'claim'
                , date('Y', $climeData['shop_basket_claim_time_create'])
                , date('m', $climeData['shop_basket_claim_time_create'])
            );
            if (isset($folder) && file_exists($folder)) {
                $folderArr = scandir($folder);
                $viewModel->setVariable('folderArr', $folderArr);
            }
        } else if ($this->params()->fromPost('create')) {
            $create = true; // kleines Hindernis fuer Skripkiddies (im Backend ja nich zu erwarten)
        }

        $request = $this->getRequest();
        if ($request->isPost() && $request instanceof Request) {
            $postData = $request->getPost()->toArray();
            $this->shopBasketClaimForm->setData($postData);
            $this->shopBasketClaimFileForm->setData($postData);
            if (!isset($postData['submit_upload'])) {
                if ($this->shopBasketClaimForm->isValid()) {
                    $formData = $this->shopBasketClaimForm->getData();
                    unset($formData['submit']);
                    if ($create && empty($claimId)) {
                        $claimId = $this->shopClaimService->saveNewShopBasketClaim($formData, $this->userService->getUserUuid());
                        if ($claimId < 1) {
                            $this->layout()->message = ['level' => 'warn', 'text' => 'Fehler beim Speichern.'];
                        } else {
                            $this->shopBasketClaimForm->get('shop_basket_claim_id')->setValue($claimId);
                            $this->shopBasketClaimFileForm->get('shop_basket_claim_id')->setValue($claimId);
                        }
                    } else {
                        if (!$this->shopClaimService->updateShopBasketClaim($claimId, $formData)) {
                            $this->layout()->message = ['level' => 'warn', 'text' => 'Fehler beim Speichern.'];
                        }
                    }
                }
            } else {
                $postData = array_merge_recursive($postData, $request->getFiles()->toArray());
                $this->shopBasketClaimFileForm->setData($postData);
                if ($this->shopBasketClaimFileForm->isValid()) {
                    $formData = $this->shopBasketClaimFileForm->getData();
                    $fileinfo = new \finfo(FILEINFO_MIME);
                    $extension01 = explode(';', $fileinfo->file($formData['shop_basket_claim_file']['tmp_name']));
                    $extension02 = explode('/', $extension01[0]);
                    $extension = $extension02[1];
                    if (empty($extension)) {
                        $this->layout()->message = [
                            'level' => 'warn',
                            'text' => 'Die Datei ist defekt. Bitte benutze eine andere oder repariere diese.'
                        ];
                    } else {
                        $filename = FilenameTool::computeSeoFriendlyFilename($formData['shop_basket_claim_filename_tokens'] . ' ' . time());
                        $filenameCompl = $filename . '.' . $extension;
                        if (!move_uploaded_file($formData['shop_basket_claim_file']['tmp_name'], $folder . '/' . $filenameCompl)) {
                            $this->layout()->message = [
                                'level' => 'warn',
                                'text' => 'Fehler beim Upload der Datei.'
                            ];
                        } else {
                            return $this->redirect()->toRoute('shop_admin_basket_claimadmin_manageclaim'
                                , ['claim_id' => $claimId, 'basket_item_id' => $basketItemId, 'basket_unique' => $basketUnique]
                            );
                        }
                    }
                }
            }
        }

        $viewModel->setVariable('shopBasketClaimId', $claimId);
        $viewModel->setVariable('shopBasketItemId', $basketItemId);
        $viewModel->setVariable('shopBasketUnique', $basketUnique);

        $viewModel->setVariable('shopBasketClaimForm', $this->shopBasketClaimForm);
        $viewModel->setVariable('shopBasketClaimFileForm', $this->shopBasketClaimFileForm);

        $shopBasketEntity = $this->basketService->getShopBasketEntityByBasketItemId($basketUnique, $basketItemId);
        $viewModel->setVariable('shopBasketEntity', $shopBasketEntity);

        $this->layout('layout/admin');

        return $viewModel;
    }

    /**
     *
     * @return JsonModel|Response
     */
    public function streamClaimFileAction()
    {
        $shopBasketClaimId = $this->params('claim_id');
        $filename = $this->params('filename');
        if (!$this->userService->checkUserRoleAccessMin(5) || empty($shopBasketClaimId) || empty($filename)) {
            return $this->redirect()->toRoute('shop_admin_basket_basketadmin_baskets');
        }
        $climeData = $this->shopClaimService->getShopBasketClaim($shopBasketClaimId);
        $folder = $this->folderTool->computeBrandDateFolder(
            $this->config['bitkorn_shop']['document_path_absolute']
            , 'claim'
            , date('Y', $climeData['shop_basket_claim_time_create'])
            , date('m', $climeData['shop_basket_claim_time_create'])
        );

        if (file_exists($folder . '/' . $filename)) {
            $fileinfo = new \finfo(FILEINFO_MIME);
            $extension01 = explode(';', $fileinfo->file($folder . '/' . $filename));
            if (!empty($extension01[0])) {
                header('Content-type: ' . $extension01[0]);
                header('Content-Disposition: inline; filename="' . $filename . '"'); // inline || attachment
                header('Content-length: ' . filesize($folder . '/' . $filename));
                header('Content-Transfer-Encoding: binary');
                header('Accept-Ranges: bytes');
                $resource = fopen('php://output', 'w');
                $rawPdf = file_get_contents($folder . '/' . $filename);
                fwrite($resource, $rawPdf);
                exit();
            }
        }

        $jsonModel = new JsonModel();
        $jsonModel->setVariable('shopBasketClaimId', $shopBasketClaimId);
        $jsonModel->setVariable('filename', $filename);
        return $jsonModel;
    }

    public function deleteClaimFileAction()
    {
        $shopBasketClaimId = $this->params('claim_id');
        $filename = $this->params('filename');
        if (!$this->userService->checkUserRoleAccessMin(5) || empty($shopBasketClaimId) || empty($filename)) {
            return $this->redirect()->toRoute('shop_admin_basket_basketadmin_baskets');
        }
        $climeData = $this->shopClaimService->getShopBasketClaim($shopBasketClaimId);
        $folder = $this->folderTool->computeBrandDateFolder(
            $this->config['bitkorn_shop']['document_path_absolute']
            , 'claim'
            , date('Y', $climeData['shop_basket_claim_time_create'])
            , date('m', $climeData['shop_basket_claim_time_create'])
        );

        $deleteSuccess = false;
        if (file_exists($folder . '/' . $filename)) {
            $deleteSuccess = unlink($folder . '/' . $filename);
        }

        $jsonModel = new JsonModel();
        $jsonModel->setVariable('shopBasketClaimId', $shopBasketClaimId);
        $jsonModel->setVariable('filename', $filename);
        $jsonModel->setVariable('deleteSuccess', $deleteSuccess);
        return $jsonModel;
    }
}
