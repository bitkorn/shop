<?php

namespace Bitkorn\Shop\Controller\Admin\Basket;

use Bitkorn\Shop\Form\Basket\ShopBasketDiscountForm;
use Bitkorn\Shop\Service\Basket\BasketAdminService;
use Bitkorn\Shop\Service\ShopService;
use Bitkorn\Shop\Service\User\ShopUserService;
use Bitkorn\Shop\Table\Basket\Discount\ShopBasketDiscountTable;
use Bitkorn\Shop\Table\User\ShopUserDataTable;
use Bitkorn\Trinket\Service\LangService;
use Bitkorn\Trinket\View\Helper\Messages;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\Response;
use Laminas\View\Model\ViewModel;
use Bitkorn\Shop\Controller\AbstractShopController;
use Bitkorn\Shop\Entity\Basket\ShopBasketDiscountEntity;

/**
 *
 * @author allapow
 */
class BasketDiscountAdminController extends AbstractShopController
{

    /**
     * @var LangService
     */
    protected $langService;

    /**
     * @var ShopService
     */
    protected $shopService;

    /**
     *
     * @var ShopBasketDiscountForm
     */
    protected $shopBasketDiscountForm;

    /**
     *
     * @var ShopBasketDiscountForm
     */
    protected $shopBasketDiscountSalesstaffForm;

    /**
     *
     * @var ShopBasketDiscountForm
     */
    protected $shopBasketDiscountC2cForm;

    /**
     * @var BasketAdminService
     */
    protected $basketAdminService;

    /**
     * @var ShopUserService
     */
    protected $shopUserService;

    /**
     * @var ShopBasketDiscountTable
     */
    protected $shopBasketDiscountTable;

    /**
     * @var ShopUserDataTable
     */
    protected $shopUserDataTable;

    /**
     * @param LangService $langService
     */
    public function setLangService(LangService $langService): void
    {
        $this->langService = $langService;
    }

    /**
     * @param ShopService $shopService
     */
    public function setShopService(ShopService $shopService): void
    {
        $this->shopService = $shopService;
    }

    /**
     * @param ShopBasketDiscountForm $shopBasketDiscountForm
     */
    public function setShopBasketDiscountForm(ShopBasketDiscountForm $shopBasketDiscountForm): void
    {
        $this->shopBasketDiscountForm = $shopBasketDiscountForm;
    }

    /**
     * @param ShopBasketDiscountForm $shopBasketDiscountSalesstaffForm
     */
    public function setShopBasketDiscountSalesstaffForm(ShopBasketDiscountForm $shopBasketDiscountSalesstaffForm): void
    {
        $this->shopBasketDiscountSalesstaffForm = $shopBasketDiscountSalesstaffForm;
    }

    /**
     * @param ShopBasketDiscountForm $shopBasketDiscountC2cForm
     */
    public function setShopBasketDiscountC2cForm(ShopBasketDiscountForm $shopBasketDiscountC2cForm): void
    {
        $this->shopBasketDiscountC2cForm = $shopBasketDiscountC2cForm;
    }

    /**
     * @param BasketAdminService $basketAdminService
     */
    public function setBasketAdminService(BasketAdminService $basketAdminService): void
    {
        $this->basketAdminService = $basketAdminService;
    }

    /**
     * @param ShopUserService $shopUserService
     */
    public function setShopUserService(ShopUserService $shopUserService): void
    {
        $this->shopUserService = $shopUserService;
    }

    /**
     * @param ShopBasketDiscountTable $shopBasketDiscountTable
     */
    public function setShopBasketDiscountTable(ShopBasketDiscountTable $shopBasketDiscountTable): void
    {
        $this->shopBasketDiscountTable = $shopBasketDiscountTable;
    }

    /**
     * @param ShopUserDataTable $shopUserDataTable
     */
    public function setShopUserDataTable(ShopUserDataTable $shopUserDataTable): void
    {
        $this->shopUserDataTable = $shopUserDataTable;
    }

    /**
     *
     * @return ViewModel|Response
     * @throws \Exception
     */
    public function basketDiscountsAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(3)) {
            return $this->redirect()->toRoute('shop_admin_dashboardadmin_dashboard');
        }
        $viewModel = new ViewModel();
        $this->layout()->helpId = 5;

        $paramUserUuid = $this->params('user_uuid', '');
        $viewModel->setVariable('userUuid', $paramUserUuid);

        $shopBasketDiscountHash = '';
        $shopBasketDiscountOnlyActive = 1;
        $shopBasketDiscountTimeFrom = 0;
        $shopBasketDiscountTimeTo = 9999999999;

        $request = $this->getRequest();
        $postData = [];
        if ($request->isPost() && $request instanceof Request) {
            $postData = $request->getPost()->toArray();
            if (!empty($postData['shop_basket_discount_hash_search'])) {
                $shopBasketDiscountHash = filter_var($postData['shop_basket_discount_hash_search'], FILTER_SANITIZE_STRING);
                $viewModel->setVariable('shopBasketDiscountHash', $shopBasketDiscountHash);
            }
            if (isset($postData['shop_basket_discount_only_active'])) {
                $shopBasketDiscountOnlyActive = filter_var($postData['shop_basket_discount_only_active'], FILTER_SANITIZE_NUMBER_INT);
            }
            if (!empty($postData['shop_basket_discount_time_from'])) {
                $shopBasketDiscountTimeFromDatetime = new \DateTime(filter_var($postData['shop_basket_discount_time_from'], FILTER_SANITIZE_STRING));
                if ($shopBasketDiscountTimeFromDatetime) {
                    $shopBasketDiscountTimeFrom = $shopBasketDiscountTimeFromDatetime->getTimestamp();
                    $viewModel->setVariable('shopBasketDiscountTimeFrom', date('Y-m-d', $shopBasketDiscountTimeFrom));
                }
            }
            if (!empty($postData['shop_basket_discount_time_to'])) {
                $shopBasketDiscountTimeToDatetime = new \DateTime(filter_var($postData['shop_basket_discount_time_to'], FILTER_SANITIZE_STRING));
                if ($shopBasketDiscountTimeToDatetime) {
                    $shopBasketDiscountTimeTo = $shopBasketDiscountTimeToDatetime->getTimestamp();
                    $viewModel->setVariable('shopBasketDiscountTimeTo', date('Y-m-d', $shopBasketDiscountTimeTo));
                }
            }
            if (!empty($postData['shop_basket_discount_shop_user_uuid']) && !empty($postData['shop_basket_discount_shop_user_name'])) {
                $postUserUuid = filter_var($postData['shop_basket_discount_shop_user_uuid'], FILTER_SANITIZE_STRING);
                $postuserName = filter_var($postData['shop_basket_discount_shop_user_name'], FILTER_SANITIZE_STRING);
                $viewModel->setVariable('userUuid', $postUserUuid);
                $viewModel->setVariable('postuserName', $postuserName);
            }
            if (!empty($postData['shop_basket_discount_shop_user'])) {
                $postUserUuid = (int)$postData['shop_basket_discount_shop_user'];
                $viewModel->setVariable('userUuid', $postUserUuid);
            }

            /**
             * Form
             */
            if (!empty($postData['form_name']) && $postData['form_name'] == 'shop_basket_discount') {
                $this->shopBasketDiscountForm->init();
                $this->shopBasketDiscountForm->setData($postData);
                if ($postData['shop_basket_discount_type'] != 'unique') {
                    $this->shopBasketDiscountForm->getInputFilter()->get('shop_basket_discount_typevalue')->setRequired(true);
                }
                if ($this->shopBasketDiscountForm->isValid()) {
                    $shopBasketDiscountEntity = new ShopBasketDiscountEntity();
                    $shopBasketDiscountEntity->exchangeArray($this->shopBasketDiscountForm->getData());
                    if ($shopBasketDiscountEntity->saveNewShopBasketDiscount($this->shopBasketDiscountTable) > 0) {
                        return $this->redirect()->toRoute('shop_admin_basket_basketdiscountadmin_discounts');
                    } else {
                        $this->layout()->message = Messages::$messageErrorSaveData;
                    }
                }
                $viewModel->setVariable('shopBasketDiscountForm', $this->shopBasketDiscountForm);
            }
            if (!empty($postData['form_name']) && $postData['form_name'] == 'shop_basket_discount_salesstaff') {
                $this->shopBasketDiscountSalesstaffForm->setFormName('shop_basket_discount_salesstaff');
                $this->shopBasketDiscountSalesstaffForm->setWithUserId(true);
                /*
                 * die gleichen Parameter müssen in AjaxBasketAdminController()->basketDiscountSalesstaffFormAction()
                 */
                $this->shopBasketDiscountSalesstaffForm->setShopUserIdAssoc($this->shopUserService->getShopUserByRoleIdAssoc(101));
                $this->shopBasketDiscountSalesstaffForm->setIsMassAction(true);
                $this->shopBasketDiscountSalesstaffForm->init();
                $this->shopBasketDiscountSalesstaffForm->setData($postData);
                if ($postData['shop_basket_discount_type'] != 'unique') {
                    $this->shopBasketDiscountSalesstaffForm->getInputFilter()->get('shop_basket_discount_typevalue')->setRequired(true);
                }
                if ($this->shopBasketDiscountSalesstaffForm->isValid()) {
                    $shopBasketDiscountEntity = new ShopBasketDiscountEntity();
                    $amountSalesstaffForm = (int)$postData['amount'];
                    $formData = $this->shopBasketDiscountSalesstaffForm->getData();
                    $formUserId = (int)$formData['user_uuid'];
                    $userData = $this->shopUserService->getShopUserDataXByUserId($formUserId);
                    $success = true;
                    for ($i = 0; $i < $amountSalesstaffForm; $i++) {
                        $formData['shop_basket_discount_hash'] = ShopBasketDiscountEntity::generateDiscountHash($this->shopBasketDiscountTable, $userData['shop_user_data_number_varchar'] . '_');
                        $shopBasketDiscountEntity->exchangeArray($formData);
                        if ($shopBasketDiscountEntity->saveNewShopBasketDiscount($this->shopBasketDiscountTable) < 1) {
                            $success = false;
                        }
                    }
                    if (!$success) {
                        $this->layout()->message = Messages::$messageErrorSaveData;
                    } else {
                        return $this->redirect()->toRoute('shop_admin_basket_basketdiscountadmin_discounts', ['user_uuid' => $formUserId]);
                    }
                } else {
                    $this->logger->debug(print_r($this->shopBasketDiscountSalesstaffForm->getMessages(), true));
                }
                $viewModel->setVariable('shopBasketDiscountSalesstaffForm', $this->shopBasketDiscountSalesstaffForm);
            }
            if (!empty($postData['form_name']) && $postData['form_name'] == 'shop_basket_discount_c2c') {
                $this->shopBasketDiscountC2cForm->setFormName($postData['form_name']);
                $postDataUserId = (int)$postData['user_uuid'];
                if (!empty($postDataUserId) && strpos($postData['shop_basket_discount_hash'], 'KDNR_') === 0) {
                    $userData = $this->shopUserService->getShopUserDataXByUserId($postDataUserId);
                    if (!empty($userData)) {
                        $postData['shop_basket_discount_hash'] = ShopBasketDiscountEntity::generateDiscountHash($this->shopBasketDiscountTable, $userData['shop_user_data_number_varchar'] . '_');
                    }
                }
                $this->shopBasketDiscountC2cForm->setWithUserId(true);
                $this->shopBasketDiscountC2cForm->setShopUserIdAssoc($this->shopUserService->getShopUserByGroupIdAssocX(1));
                $this->shopBasketDiscountC2cForm->init();
                $this->shopBasketDiscountC2cForm->setData($postData);
                if ($postData['shop_basket_discount_type'] != 'unique') {
                    $this->shopBasketDiscountC2cForm->getInputFilter()->get('shop_basket_discount_typevalue')->setRequired(true);
                }
                if ($this->shopBasketDiscountC2cForm->isValid()) {
                    $formData = $this->shopBasketDiscountC2cForm->getData();
                    $shopBasketDiscountEntity = new ShopBasketDiscountEntity();
                    $shopBasketDiscountEntity->exchangeArray($formData);
                    if ($shopBasketDiscountEntity->saveNewShopBasketDiscount($this->shopBasketDiscountTable) < 1) {
                        $this->layout()->message = Messages::$messageErrorSaveData;
                    } else {
                        return $this->redirect()->toRoute('shop_admin_basket_basketdiscountadmin_discounts', ['user_uuid' => $formData['user_uuid']]);
                    }
                }
                $viewModel->setVariable('shopBasketDiscountC2cForm', $this->shopBasketDiscountC2cForm);
            }
        }

        $shopBasketDiscounts = $this->basketAdminService->getShopBasketDiscounts($shopBasketDiscountHash, $shopBasketDiscountOnlyActive, $shopBasketDiscountTimeFrom, $shopBasketDiscountTimeTo, $paramUserUuid);
        $viewModel->setVariable('shopBasketDiscounts', $shopBasketDiscounts);

        /**
         * export
         */
        if (!empty($postData['export_basket_discount']) && !empty($shopBasketDiscounts)) {
            $csv = '"Hash";"Typ";"Typ-Wert";"Betrag-Typ";"Betrag-Typ-Wert";"Mindestbestellwert";"aktiv";"Beschreibung"' . "\r\n";
            $numberFormatter = new \NumberFormatter($this->langService->getLocale(), \NumberFormatter::DECIMAL); // 'de_DE.utf8'

            foreach ($shopBasketDiscounts as $shopBasketDiscount) {
                $csv .= '"' . $shopBasketDiscount['shop_basket_discount_hash'] . '"';
                $csv .= ';"' . $shopBasketDiscount['shop_basket_discount_type'] . '"';
                switch ($shopBasketDiscount['shop_basket_discount_type']) {
                    case 'quantity':
                        $csv .= ';"' . $numberFormatter->format($shopBasketDiscount['shop_basket_discount_typevalue']) . '"';
                        break;
                    case 'time':
                        $csv .= ';"' . $shopBasketDiscount['shop_basket_discount_typevalue'] . '"';
                        break;
                    case 'date':
                        $csv .= ';"' . date('d.m.Y', $shopBasketDiscount['shop_basket_discount_typevalue']) . '"';
                        break;
                    default :
                        $csv .= ';"' . $shopBasketDiscount['shop_basket_discount_typevalue'] . '"';
                }
                $csv .= ';"' . $shopBasketDiscount['shop_basket_discount_amount_type'] . '"';
                $csv .= ';"' . $numberFormatter->format($shopBasketDiscount['shop_basket_discount_amount_typevalue']) . '"';
                $csv .= ';"' . $numberFormatter->format($shopBasketDiscount['shop_basket_discount_min_order_amount']) . '"';
                $csv .= ';"' . ($shopBasketDiscount['shop_basket_discount_active'] == 1 ? 'ja' : 'nein') . '"';
                $csv .= ';"' . html_entity_decode($shopBasketDiscount['shop_basket_discount_desc']) . '"';
                $csv .= "\r\n";
            }
            header('Content-type: text/csv');
            header('Content-Disposition: attachment; filename="discounts.csv"');
            header('Content-Transfer-Encoding: binary');
            header('Accept-Ranges: bytes');
            $resource = fopen('php://output', 'w');
            fwrite($resource, $csv);
            exit();
        }

        $this->layout('layout/admin');

        $viewModel->setVariable('shopBasketDiscountActive', $this->shopService->getShopConfigurationValue('basket_discount_enable'));
        $viewModel->setVariable('shopBasketDiscountOnlyActive', $shopBasketDiscountOnlyActive);
        $viewModel->setVariable('salesstaffIdAssoc', $this->shopUserService->getShopUserByRoleIdAssoc(101));
        return $viewModel;
    }

}
