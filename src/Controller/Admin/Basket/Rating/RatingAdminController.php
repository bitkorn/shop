<?php

namespace Bitkorn\Shop\Controller\Admin\Basket\Rating;

use Bitkorn\Shop\Table\Article\ShopArticleRatingTable;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\Response;
use Laminas\View\Model\ViewModel;
use Laminas\View\Model\JsonModel;
use Bitkorn\Shop\Controller\AbstractShopController;

/**
 *
 * @author allapow
 */
class RatingAdminController extends AbstractShopController
{

    /**
     *
     * @var ShopArticleRatingTable
     */
    private $shopArticleRatingTable;

    /**
     *
     * @return ViewModel|Response
     */
    public function ratingsAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            return $this->redirect()->toRoute('shop_admin_dashboardadmin_dashboard');
        }
        $viewModel = new ViewModel();

        $onlyUnpublished = true;
        $showall = $this->params()->fromQuery('showall');
        $viewModel->setVariable('showall', $showall);
        if ($showall == 1) {
            $onlyUnpublished = false;
        }

        $ratings = $this->shopArticleRatingTable->searchShopArticleRatings();
        $viewModel->setVariable('ratings', $ratings);

        $request = $this->getRequest();
        if ($request->isPost() && $request instanceof Request) {
            $ratingId = filter_input(INPUT_POST, 'ratingId', FILTER_SANITIZE_NUMBER_INT);
            $ratingValue = filter_input(INPUT_POST, 'ratingValue', FILTER_SANITIZE_NUMBER_INT);
            if ($ratingId && isset($ratingValue)) {
                $jsonModel = new JsonModel();
                if ($this->shopArticleRatingTable->updateRatingItemPublished($ratingId, $ratingValue) > 0) {
                    $jsonModel->setVariable('success', 1);
                } else {
                    $jsonModel->setVariable('success', 0);
                }
                return $jsonModel;
            }
        }

        $this->layout('layout/admin');

        return $viewModel;
    }

    /**
     *
     * @return ViewModel|Response
     */
    public function ratingAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            return $this->redirect()->toRoute('shop_admin_dashboardadmin_dashboard');
        }
        $ratingId = $this->params('rating_id');
        if (empty($ratingId)) {
            return $this->redirect()->toRoute('shop_admin_basket_ratingadmin_ratings');
        }
        $viewModel = new ViewModel();
        $this->layout('layout/admin');

        $request = $this->getRequest();
        if ($request->isPost() && $request instanceof Request) {
            $postData = $request->getPost()->toArray();
        }

        return $viewModel;
    }

    /**
     *
     * @param ShopArticleRatingTable $shopArticleRatingTable
     */
    public function setShopArticleRatingTable(ShopArticleRatingTable $shopArticleRatingTable)
    {
        $this->shopArticleRatingTable = $shopArticleRatingTable;
    }

}
