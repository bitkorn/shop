<?php

namespace Bitkorn\Shop\Controller\Admin\Basket;

use Bitkorn\Shop\Entity\Basket\XshopBasketEntity;
use Bitkorn\Shop\Service\Basket\BasketAdminService;
use Bitkorn\Shop\Service\Basket\BasketFinalizeService;
use Bitkorn\Shop\Service\Basket\BasketService;
use Bitkorn\Shop\Service\Document\DocumentDeliveryService;
use Bitkorn\Shop\Service\Document\DocumentInvoiceService;
use Bitkorn\Shop\Service\User\ShopUserService;
use Bitkorn\Shop\Table\Article\Option\ShopArticleOptionItemArticlePricediffTable;
use Bitkorn\Shop\Table\Article\Param2d\ShopArticleParam2dItemTable;
use Bitkorn\Shop\Table\Article\ShopArticleRatingTable;
use Bitkorn\Shop\Table\Article\TypeDelivery\ShopArticleLengthcutDefTable;
use Bitkorn\Trinket\Service\Filesystem\FolderTool;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\Response;
use Laminas\View\Model\ViewModel;
use Laminas\View\Model\JsonModel;
use Bitkorn\Shop\Controller\AbstractShopController;

/**
 *
 * @author allapow
 */
class BasketAdminController extends AbstractShopController
{

    /**
     * @var array
     */
    protected $shopConfig;

    /**
     *
     * @var DocumentDeliveryService
     */
    protected $documentDeliveryService;

    /**
     *
     * @var DocumentInvoiceService
     */
    protected $documentInvoiceService;

    /**
     *
     * @var BasketFinalizeService
     */
    protected $basketFinalizeService;

    /**
     * @var BasketAdminService
     */
    protected $basketAdminService;

    /**
     * @var ShopUserService
     */
    protected $shopUserService;

    /**
     * @var ShopArticleRatingTable
     */
    protected $shopArticleRatingTable;

    /**
     * @var ShopArticleOptionItemArticlePricediffTable
     */
    protected $shopArticleOptionItemArticlePricediffTable;

    /**
     * @var ShopArticleParam2dItemTable
     */
    protected $shopArticleParam2dItemTable;

    /**
     * @var ShopArticleLengthcutDefTable
     */
    protected $shopArticleLengthcutDefTable;

    /**
     * @var FolderTool
     */
    protected $folderTool;

    public function setShopConfig(array $shopConfig): void
    {
        $this->shopConfig = $shopConfig;
    }

    public function setDocumentDeliveryService(DocumentDeliveryService $documentDeliveryService): void
    {
        $this->documentDeliveryService = $documentDeliveryService;
    }

    public function setDocumentInvoiceService(DocumentInvoiceService $documentInvoiceService): void
    {
        $this->documentInvoiceService = $documentInvoiceService;
    }

    public function setBasketFinalizeService(BasketFinalizeService $basketFinalizeService): void
    {
        $this->basketFinalizeService = $basketFinalizeService;
    }

    public function setBasketAdminService(BasketAdminService $basketAdminService): void
    {
        $this->basketAdminService = $basketAdminService;
    }

    public function setShopUserService(ShopUserService $shopUserService): void
    {
        $this->shopUserService = $shopUserService;
    }

    public function setShopArticleRatingTable(ShopArticleRatingTable $shopArticleRatingTable)
    {
        $this->shopArticleRatingTable = $shopArticleRatingTable;
    }

    public function setShopArticleOptionItemArticlePricediffTable(ShopArticleOptionItemArticlePricediffTable $shopArticleOptionItemArticlePricediffTable): void
    {
        $this->shopArticleOptionItemArticlePricediffTable = $shopArticleOptionItemArticlePricediffTable;
    }

    public function setShopArticleParam2dItemTable(ShopArticleParam2dItemTable $shopArticleParam2dItemTable): void
    {
        $this->shopArticleParam2dItemTable = $shopArticleParam2dItemTable;
    }

    public function setShopArticleLengthcutDefTable(ShopArticleLengthcutDefTable $shopArticleLengthcutDefTable): void
    {
        $this->shopArticleLengthcutDefTable = $shopArticleLengthcutDefTable;
    }

    public function setFolderTool(FolderTool $folderTool): void
    {
        $this->folderTool = $folderTool;
    }

    /**
     *
     * @return ViewModel|Response
     * @throws \Exception
     */
    public function basketsAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(3)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $viewModel = new ViewModel();

        // perhaps only baskets for one user
        $userUuid = $this->params('user_uuid', '');
        $viewModel->setVariable('userUuid', $userUuid);
        $viewModel->setVariable('isAdmin', $this->userService->checkUserRoleAccessMin(2));

        $paymentNumber = '';
        $shopDocumentOrderNumber = '';
        $shopBasketTimeCreateFrom = 0;
        $shopBasketTimeCreateTo = 9999999999;
        $shopBasketStatus = '';

        $request = $this->getRequest();
        if ($request->isPost() && $request instanceof Request) {
            $postData = $request->getPost()->toArray();
            if (!empty($postData['payment_number'])) {
                $paymentNumber = filter_var($postData['payment_number'], FILTER_SANITIZE_STRING);
            }
            if (!empty($postData['shop_document_order_number'])) {
                $shopDocumentOrderNumber = filter_var($postData['shop_document_order_number'], FILTER_SANITIZE_STRING);
            }
            if (!empty($postData['shop_basket_time_create_from'])) {
                $shopBasketTimeCreateFromDatetime = new \DateTime(filter_var($postData['shop_basket_time_create_from'], FILTER_SANITIZE_STRING));
                $shopBasketTimeCreateFrom = $shopBasketTimeCreateFromDatetime->getTimestamp();
            }
            if (!empty($postData['shop_basket_time_create_to'])) {
                $shopBasketTimeCreateToDatetime = new \DateTime(filter_var($postData['shop_basket_time_create_to'], FILTER_SANITIZE_STRING));
                $shopBasketTimeCreateTo = $shopBasketTimeCreateToDatetime->getTimestamp() + (60 * 60 * 24);
            }
            if (!empty($postData['shop_basket_status'])) {
                $shopBasketStatus = filter_var($postData['shop_basket_status'], FILTER_SANITIZE_STRING);
            }
            if ($postData['deleteBasketUnique']) {
                $deleteBasketUnique = filter_input(INPUT_POST, 'deleteBasketUnique', FILTER_SANITIZE_STRING);
                return new JsonModel($this->basketAdminService->deleteCompleteBasketEntityBasketGroup($deleteBasketUnique));
            }
        }

        $shopBaskets = $this->basketAdminService->getShopBasketEntitiesGrouped($paymentNumber, $shopDocumentOrderNumber,
            $shopBasketTimeCreateFrom, $shopBasketTimeCreateTo, $shopBasketStatus, $userUuid, 'shop_basket_time_order', 'DESC');
        if (!empty($userUuid)) {
            $userData = $this->shopUserService->getShopUserData($userUuid);
            $viewModel->setVariable('userData', $userData);
        }

        $viewModel->setVariable('shopBaskets', $shopBaskets);

        $viewModel->setVariable('paymentNumber', $paymentNumber);
        $viewModel->setVariable('shopDocumentOrderNumber', $shopDocumentOrderNumber);
        $viewModel->setVariable('shopBasketStatus', $shopBasketStatus);
        if (!empty($shopBasketTimeCreateFrom)) {
            $viewModel->setVariable('shopBasketTimeCreateFrom', date('Y-m-d', $shopBasketTimeCreateFrom));
        }
        if ($shopBasketTimeCreateTo != 9999999999) {
            $viewModel->setVariable('shopBasketTimeCreateTo', date('Y-m-d', $shopBasketTimeCreateTo));
        }

        $this->layout('layout/admin');

        return $viewModel;
    }

    /**
     *
     * @return ViewModel|Response
     */
    public function basketAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(2)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $basketUnique = filter_var($this->params('basket_unique'), FILTER_SANITIZE_STRING);
        if (empty($basketUnique)) {
            return $this->redirect()->toRoute('shop_admin_basket_basketadmin_baskets');
        }
        $viewModel = new ViewModel();
        $this->layout('layout/admin');
        $this->layout()->helpId = 4;

        $shopBasket = $this->basketAdminService->getShopBasketEntitiesByBasketUnique($basketUnique);

        $request = $this->getRequest();
        if ($request->isPost() && $request instanceof Request) {
            $postData = $request->getPost()->toArray();
            /*
             * Status payed
             */
            if (!empty($postData['shop_basket_status']) && ($postData['shop_basket_status'] == 'payed' && $shopBasket[0]['shop_basket_status'] == 'ordered')) {
                if (empty($postData['shop_basket_time_payed'])) {
                    $this->layout()->message = [
                        'level' => 'warn',
                        'text' => 'Status "bezahlt" erfordert ein Datum für den Zeitpunkt der Bezahlung.'
                    ];
                } else {
                    $shopBasketTimePayed = \DateTime::createFromFormat('d.m.Y',
                        filter_var($postData['shop_basket_time_payed'], FILTER_SANITIZE_STRING));
                    if ($shopBasketTimePayed && $shopBasketTimePayed instanceof \DateTime) {
                        $this->basketFinalizeService->basketPayed($basketUnique, $shopBasketTimePayed->getTimestamp());
                        $shopBasket = $this->basketAdminService->getShopBasketEntitiesByBasketUnique($basketUnique);
                    }
                }
            }
            /*
             * Status shipped
             */
            if (!empty($postData['shipping_number']) || (!empty($postData['shipping_number']) && !empty($postData['shop_basket_status']) && $postData['shop_basket_status'] == 'shipped')) {
                if ($shopBasket[0]['shop_basket_status'] != 'payed') {
                    $this->layout()->message = [
                        'level' => 'warn',
                        'text' => 'NUR bezahlte Artikel können versand worden sein!'
                    ];
                } else if (!$this->basketAdminService->existShopDocumentInvoiceByBasketUnique($basketUnique)) {
                    $this->layout()->message = [
                        'level' => 'warn',
                        'text' => 'Status "shipped" erfordert ein Rechnungs PDF'
                    ];
                } else if (!$this->basketAdminService->existShopDocumentDeliveryByBasketUnique($basketUnique)) {
                    $this->layout()->message = [
                        'level' => 'warn',
                        'text' => 'Status "shipped" erfordert ein Lieferschein PDF'
                    ];
                } else {
                    $shippingNumber = filter_var($postData['shipping_number'], FILTER_SANITIZE_STRING);
                    $shippingMethod = filter_var($postData['shipping_method'], FILTER_SANITIZE_STRING);
                    $this->basketAdminService->updateShipping($shippingNumber, $shippingMethod, $basketUnique);
                    $this->basketFinalizeService->basketShipped($basketUnique);
                    $shopBasket = $this->basketAdminService->getShopBasketEntitiesByBasketUnique($basketUnique);
                }
            }
            if (!empty($postData['shop_document_invoice_create'])) {
                if (!$this->basketAdminService->existShopDocumentInvoiceByBasketUnique($basketUnique)) {
                    $xshopBasketEntity = new XshopBasketEntity($basketUnique);
                    $xshopBasketEntity->exchangeArrayBasket($shopBasket);
                    $xshopBasketEntity = $this->basketService->computeValuesXshopBasketEntity($xshopBasketEntity);
                    $this->documentInvoiceService->createDocument($xshopBasketEntity, time());
                }
            }
            if (!empty($postData['shop_document_delivery_create'])) {
                if (!$this->basketAdminService->existShopDocumentDeliveryByBasketUnique($basketUnique)) {
                    $xshopBasketEntity = new XshopBasketEntity($basketUnique);
                    $xshopBasketEntity->exchangeArrayBasket($shopBasket);
                    $xshopBasketEntity = $this->basketService->computeValuesXshopBasketEntity($xshopBasketEntity);
                    $this->documentDeliveryService->createDocument($xshopBasketEntity, time());
                }
            }
            /*
             * comments
             */
            if (!empty($postData['shop_basket_entity_comments_submit'])) {
                $this->basketAdminService->updateShopBasketEntityComments($basketUnique, filter_var($postData['shop_basket_entity_comments'], FILTER_SANITIZE_STRING));
                $shopBasket = $this->basketAdminService->getShopBasketEntitiesByBasketUnique($basketUnique);
            }
            /*
             * send email again
             */
            if (!empty($postData['shop_basket_email_type']) && isset($shopBasket[0])) {
                switch (filter_input(INPUT_POST, 'shop_basket_email_type', FILTER_SANITIZE_STRING)) {
                    case 'ordered':
                        $document = $this->basketAdminService->getShopDocumentOrderByBasketUnique($basketUnique);
                        $documentFolder = $this->folderTool->computeBrandDateFolder(realpath($this->shopConfig['document_path_absolute']), 'order',
                            date('Y', $document['shop_document_order_time']), date('m', $document['shop_document_order_time']));
                        if ($this->basketFinalizeService->sendEmailOrder($basketUnique, $shopBasket,
                                $documentFolder . '/' . $document['shop_document_order_filename']) >= 0) {
                            $this->layout()->message = ['level' => 'success', 'text' => 'Die Email wurde gesendet.'];
                        }
                        break;
                    case 'payed':
                        if ($shopBasket[0]['shop_basket_status'] == 'ordered' || $shopBasket[0]['shop_basket_status'] == 'payed') {
                            if ($this->basketFinalizeService->sendEmailPayed($basketUnique, $shopBasket) >= 0) {
                                $this->layout()->message = ['level' => 'success', 'text' => 'Die Email wurde gesendet.'];
                            }
                        }
                        break;
                    case 'shipped':
                        $document = $this->basketAdminService->getShopDocumentDeliveryByBasketUnique($basketUnique);
                        if (!empty($document)) {
                            $documentFolder = $this->folderTool->computeBrandDateFolder(realpath($this->shopConfig['document_path_absolute']), 'delivery',
                                date('Y', $document['shop_document_delivery_time']),
                                date('m', $document['shop_document_delivery_time']));
                            if ($this->basketFinalizeService->sendEmailShipped($basketUnique, $shopBasket,
                                    $documentFolder . '/' . $document['shop_document_delivery_filename']) >= 0) {
                                $this->layout()->message = ['level' => 'success', 'text' => 'Die Email wurde gesendet.'];
                            }
                        } else {
                            $this->layout()->message = ['level' => 'warn', 'text' => 'Es existiert kein Lieferschein'];
                        }
                        break;
                }
            }
            /*
             * send basketRating reminder email
             */
            if (!empty($postData['shop_basket_email_type']) && $postData['shop_basket_email_type'] == 'basket_entity_rating' && isset($shopBasket[0])) {
                if ($this->basketFinalizeService->sendRatingReminder($shopBasket) >= 0) {
                    $this->basketAdminService->updateBasketItemRatingReminderSend($basketUnique);
                    $shopBasket = $this->basketAdminService->getShopBasketEntitiesByBasketUnique($basketUnique);
                    $this->layout()->message = ['level' => 'success', 'text' => 'Die Email wurde gesendet.'];
                } else {
                    $this->layout()->message = ['level' => 'warn', 'text' => 'Es gab einen fehler beim Emailversand.'];
                }
            }
        }
        $viewModel->setVariable('shopBasket', $shopBasket);
        $viewModel->setVariable('shopDocumentDelivery', $this->basketAdminService->getShopDocumentDeliveryByBasketUnique($basketUnique));
        $viewModel->setVariable('shopDocumentInvoice', $this->basketAdminService->getShopDocumentInvoiceByBasketUnique($basketUnique));
        $viewModel->setVariable('shopDocumentOrder', $this->basketAdminService->getShopDocumentOrderByBasketUnique($basketUnique));
        $viewModel->setVariable('claimsIdAssoc', $this->basketAdminService->getShopBasketClaimsFromBasketIdAssoc($basketUnique));
        $viewModel->setVariable('basketEntityRating', $this->basketAdminService->getShopBasketEntityRating($shopBasket[0]['shop_basket_item_id']));
        return $viewModel;
    }

    /**
     * List all basketEntityRatings
     * @return ViewModel|Response
     */
    public function basketEntityRatingAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $basketEntityId = intval($this->params('basket_entity_id'));
        $basketEntity = $this->basketAdminService->getShopBasketEntity($basketEntityId);
        if (empty($basketEntity)) {
            return $this->redirect()->toRoute('shop_admin_basket_basketadmin_baskets');
        }
        $viewModel = new ViewModel();
        $this->layout('layout/admin');
//        $this->layout()->helpId = 4;

        $request = $this->getRequest();
        if ($request->isPost() && $request instanceof Request) {
            $postData = $request->getPost()->toArray();
            if (isset($postData['published'])) {
                if (!$this->basketAdminService->updateBasketItemRatingPublished($basketEntity['shop_basket_item_id'], (int)$postData['published'])) {
                    $this->layout()->message = ['level' => 'error', 'text' => 'Es gab einen Fehler beim Speichern der Daten'];
                }
            }
        }

        $viewModel->setVariable('basketEntity', $basketEntity);
        $shopBasketItemRating = $this->basketAdminService->getShopBasketEntityRating($basketEntity['shop_basket_item_id']);
        $viewModel->setVariable('shopBasketEntityRating', $shopBasketItemRating);
        return $viewModel;
    }

    /**
     * Clearing database from dead baskets
     * @return ViewModel|Response
     */
    public function basketsClearingAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(1)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $viewModel = new ViewModel();
        $this->layout('layout/admin');
//        $this->layout()->helpId = 4;

        $basketsWithoutBasketEntity = $this->basketAdminService->getShopBasketsWithoutBasketEntity();

        $request = $this->getRequest();
        if ($request->isPost() && $request instanceof Request) {
            $postData = $request->getPost()->toArray();
            if (!empty($postData['clear_basket'])) {
                $basketWithoutBasketEntityDeleteCount = 0;
                foreach ($basketsWithoutBasketEntity as $basketWithoutBasketEntity) {
                    $basketWithoutBasketEntityDeleteCount += $this->basketService->deleteShopBasket($basketWithoutBasketEntity['shop_basket_unique']);
                }
                $viewModel->setVariable('basketWithoutBasketEntityDeleteCount', $basketWithoutBasketEntityDeleteCount);
                if ($basketWithoutBasketEntityDeleteCount > 0) {
                    $basketsWithoutBasketEntity = $this->basketAdminService->getShopBasketsWithoutBasketEntity();
                    $this->layout()->message = ['level' => 'info', 'text' => sprintf('Es wurden %d Datensätze gelöscht.',
                        $basketWithoutBasketEntityDeleteCount)];
                } else {
                    $this->layout()->message = ['level' => 'info', 'text' => 'Es wurden keine Datensätze gelöscht.'];
                }
            }
        }

        $viewModel->setVariable('basketsWithoutBasketEntity', $basketsWithoutBasketEntity);
        $viewModel->setVariable('basketsWithoutBasketEntityCount', count($basketsWithoutBasketEntity));

        return $viewModel;
    }

}
