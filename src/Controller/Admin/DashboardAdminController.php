<?php

namespace Bitkorn\Shop\Controller\Admin;

use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\Response;
use Laminas\View\Model\ViewModel;
use Bitkorn\Shop\Controller\AbstractShopController;

/**
 *
 * @author allapow
 */
class DashboardAdminController extends AbstractShopController
{

    /**
     * 
     * @return ViewModel|Response
     */
    public function dashboardAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(2)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $viewModel = new ViewModel();
        $this->layout('layout/admin');

        $request = $this->getRequest();
        if ($request->isPost() && $request instanceof Request) {
            $postData = $request->getPost()->toArray();
        }

        $viewModel->setVariable('dateFrom', '01.01.' . (date('Y') - 1));
        $viewModel->setVariable('dateTo', date('d.m.Y'));
        return $viewModel;
    }

}
