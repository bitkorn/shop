<?php

namespace Bitkorn\Shop\Controller\Admin\Article\Size;

use Bitkorn\Shop\Form\Article\ShopArticleSizeGroupForm;
use Bitkorn\Trinket\Tools\Filesystem\FolderTool;
use Bitkorn\Trinket\Tools\Image\ImageScale;
use Laminas\Http\Response;
use Laminas\View\Model\ViewModel;
use Bitkorn\Shop\Controller\AbstractShopController;

/**
 *
 * @author allapow
 */
class ArticleSizeAdminController extends AbstractShopController
{

    /**
     *
     * @var ShopArticleSizeGroupForm
     */
    protected $shopArticleSizeGroupForm;

    /**
     * @param ShopArticleSizeGroupForm $shopArticleSizeGroupForm
     */
    public function setShopArticleSizeGroupForm(ShopArticleSizeGroupForm $shopArticleSizeGroupForm): void
    {
        $this->shopArticleSizeGroupForm = $shopArticleSizeGroupForm;
    }

    /**
     * Listet die einzelnen Maßangaben zur Ansicht UND zum Editieren.
     * @return ViewModel|Response
     */
    public function articleSizesAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $this->layout('layout/admin');
        $this->layout()->helpId = 2;
        $viewModel = new ViewModel();

        $shopArticleSizeDefs = $this->getShopArticleSizeTablex()->getShopArticleSizeDefs();
        $viewModel->setVariable('shopArticleSizeDefs', $shopArticleSizeDefs);
        $shopArticleSizeItems = $this->getShopArticleSizeTablex()->getShopArticleSizeItemsGroupedAssoc();
        $viewModel->setVariable('shopArticleSizeItems', $shopArticleSizeItems);

        return $viewModel;
    }

    /**
     * Listet die Gruppen um eine auszuwählen.
     * @return ViewModel|Response
     */
    public function articleSizeGroupsAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $this->layout('layout/admin');
        $viewModel = new ViewModel();

        $shopArticleSizeGroups = $this->getShopArticleSizeGroupTable()->getShopArticleSizeGroups();
        $viewModel->setVariable('shopArticleSizeGroups', $shopArticleSizeGroups);

        return $viewModel;
    }

    /**
     * Beschreibt die Gruppe für das Frontend mit Text und Bild.
     * ...auch Text für das Backend.
     * @return ViewModel|Response
     * @throws \ImagickException
     */
    public function articleSizeGroupAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $sizeGroupId = $this->params('size_group_id');
        if (empty($sizeGroupId)) {
            return $this->redirect()->toRoute('shop_admin_article_articlesize_sizegroups');
        }
        $this->layout('layout/admin');
        $viewModel = new ViewModel();
        if (file_exists($this->config['bitkorn_shop']['image_path_absolute'] . '/sizegroup/sizegroupimage_' . $sizeGroupId . '_50.png')) {
            $viewModel->setVariable('sizegroupimage', $this->config['bitkorn_shop']['image_path_relative'] . '/sizegroup/sizegroupimage_' . $sizeGroupId . '_50.png');
        }

        $shopArticleSizeGroup = $this->getShopArticleSizeGroupTable()->getShopArticleSizeGroupById($sizeGroupId);

        $this->shopArticleSizeGroupForm->setShopArticleSizeGroupId($sizeGroupId);
        $this->shopArticleSizeGroupForm->init();
        $this->shopArticleSizeGroupForm->setData($shopArticleSizeGroup);

        $request = $this->getRequest();
        if ($request->isPost() && $request instanceof \Laminas\Http\PhpEnvironment\Request) {
            $postData = array_merge_recursive($request->getPost()->toArray(), $request->getFiles()->toArray());
            $this->shopArticleSizeGroupForm->setData($postData);
            if ($this->shopArticleSizeGroupForm->isValid()) {
                $formData = $this->shopArticleSizeGroupForm->getData();
                if (!empty($formData['shop_article_size_group_image_image']['tmp_name'])) {
                    /*
                     * Bild hoch laden
                     */
                    $fileinfo = new \finfo(FILEINFO_MIME);
                    $extension01 = explode(';', $fileinfo->file($formData['shop_article_size_group_image_image']['tmp_name']));
                    $extension02 = explode('/', $extension01[0]);
                    $extension = $extension02[1];
                    if (empty($extension)) {
                        $this->layout()->message = [
                            'level' => 'warn',
                            'text' => 'Die Datei ist defekt. Bitte benutze eine andere oder repariere diese.'
                        ];
                    } else {
                        $folder = FolderTool::computeIdFolder($this->config['bitkorn_shop']['image_path_absolute'], 'sizegroup');
                        $filename = 'sizegroupimage_' . $sizeGroupId;
                        $filenameCompl = $filename . '.' . $extension;
                        if (move_uploaded_file($formData['shop_article_size_group_image_image']['tmp_name'], $folder . '/' . $filenameCompl)) {
                            $images = ImageScale::makeSixteenToNines($folder, $filename, $extension, [30, 40, 50, 60, 80, 120]);
                            $thumbs = ImageScale::makeThumb($folder, $filename, $extension, 600, 600, 400, 400);
                        }
                    }
                }
                /*
                 * TextDaten speichern
                 */
                $this->getShopArticleSizeGroupTable()->updataShopArticleSizeGroup($sizeGroupId, [
                    'shop_article_size_group_name' => $formData['shop_article_size_group_name'],
                    'shop_article_size_group_desc' => $formData['shop_article_size_group_desc'],
                    'shop_article_size_group_heading' => $formData['shop_article_size_group_heading'],
                    'shop_article_size_group_text' => $formData['shop_article_size_group_text']
                ]);
                return $this->redirect()->toRoute('shop_admin_article_articlesize_sizegroup', ['size_group_id' => $sizeGroupId]);
            }
        }

        $viewModel->setVariable('shopArticleSizeGroup', $shopArticleSizeGroup);
        $viewModel->setVariable('shopArticleSizeGroupForm', $this->shopArticleSizeGroupForm);

        return $viewModel;
    }

}
