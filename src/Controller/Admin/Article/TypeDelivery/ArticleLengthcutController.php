<?php

namespace Bitkorn\Shop\Controller\Admin\Article\TypeDelivery;

use Bitkorn\Shop\Controller\AbstractShopController;
use Bitkorn\Shop\Entity\Article\TypeDelivery\ShopArticleLengthcutDefEntity;
use Bitkorn\Shop\Form\Article\TypeDelivery\ShopArticleLengthcutDefForm;
use Bitkorn\Shop\Service\Article\ShopArticleTypeDeliveryService;
use Bitkorn\Trinket\View\Helper\Messages;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\PhpEnvironment\Response;
use Laminas\View\Model\ViewModel;

class ArticleLengthcutController extends AbstractShopController
{
    /**
     * @var ShopArticleTypeDeliveryService
     */
    protected $shopArticleTypeDeliveryService;

    /**
     * @var ShopArticleLengthcutDefForm
     */
    protected $shopArticleLengthcutDefForm;

    /**
     * @param ShopArticleTypeDeliveryService $shopArticleTypeDeliveryService
     */
    public function setShopArticleTypeDeliveryService(ShopArticleTypeDeliveryService $shopArticleTypeDeliveryService): void
    {
        $this->shopArticleTypeDeliveryService = $shopArticleTypeDeliveryService;
    }

    /**
     * @param ShopArticleLengthcutDefForm $shopArticleLengthcutDefForm
     */
    public function setShopArticleLengthcutDefForm(ShopArticleLengthcutDefForm $shopArticleLengthcutDefForm): void
    {
        $this->shopArticleLengthcutDefForm = $shopArticleLengthcutDefForm;
    }

    /**
     * @return ViewModel|\Laminas\Http\Response
     */
    public function lengthcutDefsAction()
    {
        $viewModel = new ViewModel();
        if (!$this->userService->checkUserRoleAccessMin(3)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }

        $this->shopArticleLengthcutDefForm->init();
        $this->shopArticleLengthcutDefForm->setIsNew(true);

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $post = $request->getPost();
            $this->shopArticleLengthcutDefForm->setData($post);
            $this->shopArticleLengthcutDefForm->parseStringNumbers($this->numberFormatService);
            if($this->shopArticleLengthcutDefForm->isValid()) {
                $entity = new ShopArticleLengthcutDefEntity();
                if(!$entity->exchangeArrayFromRequest($this->shopArticleLengthcutDefForm->getData())) {
                    $this->layout()->message = Messages::$messageErrorUndefined;
                    return $viewModel;
                }
                $entity->unsetPrimaryKey();
                if($this->shopArticleTypeDeliveryService->insertLengthcutDef($entity->getStorage()) < 1) {
                    $this->layout()->message = Messages::$messageErrorSaveData;
                }
            } else {
                $viewModel->setVariable('isModalAddLengthcutDef', true);
                $this->shopArticleLengthcutDefForm->formatStringNumbers($this->numberFormatService);
            }
        }

        $lengthcutDefs = $this->shopArticleTypeDeliveryService->getLengthcutDefs();
        $viewModel->setVariable('lengthcutDefs', $lengthcutDefs);
        $viewModel->setVariable('shopArticleLengthcutDefForm', $this->shopArticleLengthcutDefForm);
        $this->layout('layout/admin');
        return $viewModel;
    }

    /**
     * @return ViewModel|\Laminas\Http\Response
     */
    public function lengthcutDefAction()
    {
        $viewModel = new ViewModel();
        if (!$this->userService->checkUserRoleAccessMin(3)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $lengthdefId = $this->params('lengthcut_def_id');
        if (empty($lengthdefId)) {
            return $viewModel;
        }
        $viewModel->setVariable('lengthdefId', $lengthdefId);


        $this->layout('layout/admin');
        return $viewModel;
    }
}
