<?php

namespace Bitkorn\Shop\Controller\Admin\Article;

use Bitkorn\Shop\Entity\Article\ShopArticleEntity;
use Bitkorn\Shop\Entity\Article\ShopArticleOptionsEntity;
use Bitkorn\Shop\Entity\Database\QueryParamsArticleSearch;
use Bitkorn\Shop\Form\Article\ShopArticleForm;
use Bitkorn\Shop\Form\Article\ShopArticleStockForm;
use Bitkorn\Shop\Service\Article\ShopArticleParam2dService;
use Bitkorn\Shop\Service\Article\ShopArticleService;
use Bitkorn\Shop\Service\Article\ShopArticleStockService;
use Bitkorn\Shop\Service\Article\ShopArticleTypeDeliveryService;
use Bitkorn\Shop\Service\Vendor\ShopVendorService;
use Bitkorn\Shop\Table\Article\ShopArticleCategoryRelationTable;
use Bitkorn\Shop\Table\Article\ShopArticleTable;
use Bitkorn\Shop\Tablex\Article\Option\ShopArticleOptionTablex;
use Bitkorn\Shop\Tools\Article\ShopArticleSefurl;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\Response;
use Laminas\View\Model\ViewModel;
use Bitkorn\Shop\Controller\AbstractShopController;
use Bitkorn\Trinket\View\Helper\Messages;

/**
 * Description of ArticleAdminController
 *
 * @author allapow
 */
class ArticleAdminController extends AbstractShopController
{
    /**
     * @var ShopArticleService
     */
    protected $shopArticleService;

    /**
     * @var ShopVendorService
     */
    protected $shopVendorService;

    /**
     * @var ShopArticleStockService
     */
    protected $shopArticleStockService;

    /**
     * @var ShopArticleParam2dService
     */
    protected $shopArticleParam2dService;

    /**
     * @var ShopArticleTypeDeliveryService
     */
    protected $shopArticleTypeDeliveryService;

    /**
     * @var ShopArticleTable
     */
    protected $shopArticleTable;

    /**
     *
     * @var ShopArticleForm
     */
    protected $shopArticleForm;

    /**
     *
     * @var ShopArticleStockForm
     */
    protected $shopArticleStockForm;

    /**
     *
     * @var ShopArticleCategoryRelationTable
     */
    protected $shopArticleCategoryRelationTable;

    /**
     *
     * @var ShopArticleOptionTablex
     */
    protected $shopArticleOptionTablex;

    /**
     *
     * @var ShopArticleOptionsEntity
     */
    protected $shopArticleOptionsEntity;

    /**
     * @param ShopArticleService $shopArticleService
     */
    public function setShopArticleService(ShopArticleService $shopArticleService): void
    {
        $this->shopArticleService = $shopArticleService;
    }

    /**
     * @param ShopVendorService $shopVendorService
     */
    public function setShopVendorService(ShopVendorService $shopVendorService): void
    {
        $this->shopVendorService = $shopVendorService;
    }

    /**
     * @param ShopArticleStockService $shopArticleStockService
     */
    public function setShopArticleStockService(ShopArticleStockService $shopArticleStockService): void
    {
        $this->shopArticleStockService = $shopArticleStockService;
    }

    /**
     * @param ShopArticleParam2dService $shopArticleParam2dService
     */
    public function setShopArticleParam2dService(ShopArticleParam2dService $shopArticleParam2dService): void
    {
        $this->shopArticleParam2dService = $shopArticleParam2dService;
    }

    /**
     * @param ShopArticleTypeDeliveryService $shopArticleTypeDeliveryService
     */
    public function setShopArticleTypeDeliveryService(ShopArticleTypeDeliveryService $shopArticleTypeDeliveryService): void
    {
        $this->shopArticleTypeDeliveryService = $shopArticleTypeDeliveryService;
    }

    /**
     * @param ShopArticleTable $shopArticleTable
     */
    public function setShopArticleTable(ShopArticleTable $shopArticleTable): void
    {
        $this->shopArticleTable = $shopArticleTable;
    }

    /**
     * @param ShopArticleForm $shopArticleForm
     */
    public function setShopArticleForm(ShopArticleForm $shopArticleForm): void
    {
        $this->shopArticleForm = $shopArticleForm;
    }

    /**
     * @param ShopArticleStockForm $shopArticleStockForm
     */
    public function setShopArticleStockForm(ShopArticleStockForm $shopArticleStockForm): void
    {
        $this->shopArticleStockForm = $shopArticleStockForm;
    }

    /**
     *
     * @param ShopArticleCategoryRelationTable $shopArticleCategoryRelationTable
     */
    public function setShopArticleCategoryRelationTable(ShopArticleCategoryRelationTable $shopArticleCategoryRelationTable)
    {
        $this->shopArticleCategoryRelationTable = $shopArticleCategoryRelationTable;
    }

    /**
     * @param ShopArticleOptionTablex $shopArticleOptionTablex
     */
    public function setShopArticleOptionTablex(ShopArticleOptionTablex $shopArticleOptionTablex)
    {
        $this->shopArticleOptionTablex = $shopArticleOptionTablex;
    }

    /**
     *
     * @param ShopArticleOptionsEntity $shopArticleOptionsEntity
     */
    public function setShopArticleOptionsEntity(ShopArticleOptionsEntity $shopArticleOptionsEntity)
    {
        $this->shopArticleOptionsEntity = $shopArticleOptionsEntity;
    }

    /**
     *
     * @return ViewModel|Response
     */
    public function articlesAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(5) && !$this->userService->checkUserRoleAccess(101)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $viewModel = new ViewModel();

        $articleType = $this->params()->fromPost('article_type', '');
        $articlesData = $this->shopArticleService->searchShopArticles('', '', 0, $articleType, '');
        $viewModel->setVariable('articles', $articlesData);

        $viewModel->setVariable('articleType', $articleType);
        $viewModel->setVariable('articleTypes', $this->toolsTable->getEnumValuesPostgreSQL('enum_shop_article_type'));
        $viewModel->setVariable('param2dIdAssoc', $this->shopArticleParam2dService->getShopArticleParam2dIdAssoc());
        $viewModel->setVariable('lengthcutDefIdAssoc', $this->shopArticleTypeDeliveryService->getLengthcutDefIdAssoc());

        $this->layout('layout/admin');

        return $viewModel;
    }

    /**
     *
     * @return ViewModel|Response
     */
    public function addArticleAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(2)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }

        $this->shopArticleForm->setShopArticleIdRequired(false);
        $this->shopArticleForm->setIsSubmitBackward(false);
        $this->shopArticleForm->setIsSubmitForward(false);
        $this->shopArticleForm->init();
        $this->shopArticleForm->get('shop_article_category_relation')->setAttribute('disabled', 'disabled');
        $this->shopArticleForm->getInputFilter()->get('shop_article_category_relation')->setRequired(false);

        $request = $this->getRequest();
        if ($request->isPost() && $request instanceof Request) {
            $postData = $request->getPost()->toArray();
            if (!empty($postData['shop_article_sefurl'])) {
                $postData['shop_article_sefurl'] = ShopArticleSefurl::computeSefurl($postData['shop_article_sefurl']);
            } else {
                $postData['shop_article_sefurl'] = ShopArticleSefurl::computeSefurl($postData['shop_article_name']);
            }
            $this->shopArticleForm->setData($postData);
            $this->shopArticleForm->parseStringNumbers($this->numberFormatService);
            if ($this->shopArticleForm->isValid()) {
                $formData = $this->shopArticleForm->getData();
                $articleEntity = new ShopArticleEntity();
                $articleEntity->exchangeArray($formData);
                if ($articleEntity->checkGroupable()) {
                    $articleEntity->calculatePriceNet();
                    $newArticleId = $articleEntity->save($this->shopArticleTable, $this->shopArticleCategoryRelationTable);
                    if ($newArticleId > 0) {
                        return $this->redirect()->toRoute('shop_admin_article_articleadmin_editarticle', ['article_id' => $newArticleId]);
                    } else {
                        $this->layout()->message = Messages::$messageErrorSaveData;
                    }
                } else {
                    $this->layout()->message = [
                        'level' => 'warn',
                        'text' => 'Gruppenartikel können nicht gruppierbar sein oder Lagerverwaltung nutzen.'
                    ];
                }
            } else {
                $this->layout()->message = Messages::$messageErrorSaveData;
                $this->logger->debug(print_r($this->shopArticleForm->getMessages(), true));
            }
        }

        $this->layout('layout/admin');
        return new ViewModel([
            'shopArticleForm' => $this->shopArticleForm
        ]);
    }

    /**
     *
     * @return ViewModel|Response
     */
    public function editArticleAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $articleId = $this->params('article_id', 0);
        if ($articleId == 0) {
            return $this->redirect()->toRoute('shop_admin_article_articleadmin_articles');
        }
        $articleData = $this->shopArticleService->getShopArticleById($articleId);
        if (empty($articleData)) {
            return $this->redirect()->toRoute('shop_admin_article_articleadmin_articles');
        }
        $viewModel = new ViewModel();
        $viewModel->setVariable('articleData', $articleData);

        if ($this->params('copy', 0) == 1) {
            $this->shopArticleForm->setAttribute('action', $this->url()->fromRoute('shop_admin_article_articleadmin_editarticle', ['article_id' => $articleId]));
            $this->layout()->message = [
                'level' => 'info',
                'text' => 'Das Produkt wurde kopiert.'
            ];
        }

        $this->shopArticleForm->setIsSubmitForward(false);
        $this->shopArticleForm->init();
        $this->shopArticleForm->setData($articleData);
        $this->shopArticleForm->get('shop_article_category_relation')->setValue($this->shopArticleService->getShopArticleCategoriesForShopArticle($articleId));

        $request = $this->getRequest();
        if ($request->isPost() && $request instanceof Request) {
            $postData = $request->getPost()->toArray();
            if (!empty($postData['copy_article'])) {
                unset($articleData['shop_article_id']);
                $articleData['shop_article_sefurl'] = $articleData['shop_article_sefurl'] . '-' . time();
                $articleIdNew = $this->shopArticleService->saveShopArticle($articleData);
                if (!empty($articleIdNew)) {
                    return $this->redirect()->toRoute('shop_admin_article_articleadmin_editarticle', ['article_id' => $articleIdNew, 'copy' => 1]);
                } else {
                    $this->layout()->message = [
                        'level' => 'warn',
                        'text' => 'Fehler beim Kopieren.'
                    ];
                }
            } else {
                if (!empty($postData['shop_article_sefurl'])) {
                    $postData['shop_article_sefurl'] = ShopArticleSefurl::computeSefurl($postData['shop_article_sefurl']);
                } else {
                    $postData['shop_article_sefurl'] = ShopArticleSefurl::computeSefurl($postData['shop_article_name']);
                }
                $this->shopArticleForm->setData($postData);
                $this->shopArticleForm->parseStringNumbers($this->numberFormatService);
                if ($this->shopArticleForm->isValid()) {
                    $formData = $this->shopArticleForm->getData();
                    $articleEntity = new ShopArticleEntity();
                    $articleEntity->exchangeArray($formData);
                    if ($articleEntity->checkGroupable()) {
                        $articleEntity->calculatePriceNet();
                        $resultInt = $articleEntity->update($this->shopArticleTable, $this->shopArticleCategoryRelationTable);
                        if ($resultInt >= 0) {
                            $this->layout()->message = [
                                'level' => 'success',
                                'text' => ($resultInt > 0) ? 'Der Artikel wurde gespeichert' : 'Es gab keine Daten zum Speichern.'
                            ];
                            if (isset($formData['submit_backward'])) {
                                return $this->redirect()->toRoute('shop_admin_article_articleadmin_articles');
                            }
                            $this->shopArticleForm->setData($this->shopArticleService->getShopArticleById($articleId));
                            $this->shopArticleForm->formatStringNumbers($this->numberFormatService);
                        }
                    } else {
                        $this->layout()->message = [
                            'level' => 'warn',
                            'text' => 'Gruppenartikel können nicht gruppierbar sein oder Lagerverwaltung nutzen.'
                        ];
                    }
                } else {
                    $this->layout()->message = [
                        'level' => 'warn',
                        'text' => implode('<br>', $this->shopArticleForm->getMessages())
                    ];
                }
            }
        }
        $this->shopArticleForm->formatStringNumbers($this->numberFormatService);
        $viewModel->setVariable('shopArticleForm', $this->shopArticleForm);
        $this->layout('layout/admin');

        return $viewModel;
    }

    /**
     *
     * @return ViewModel|Response
     */
    public function editArticleGroupRelationAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $articleId = $this->params('article_id', 0);
        if ($articleId == 0) {
            return $this->redirect()->toRoute('shop_admin_article_articleadmin_articles');
        }
        $articleData = $this->shopArticleService->getShopArticleById($articleId);
        if (empty($articleData)) {
            return $this->redirect()->toRoute('shop_admin_article_articleadmin_articles');
        }
        $viewModel = new ViewModel(['articleData' => $articleData]);

        $articleGroupRelationArticles = $this->shopArticleService->getShopArticleGroupRelationArticles($articleId);

        $request = $this->getRequest();
        if ($request->isPost() && $request instanceof Request) {
            $postData = $request->getPost()->toArray();

            if (isset($postData['select_relation_article'])) {
                /*
                 * select_relation_article from AJAX Form
                 */
                $relationArticleId = (int)$postData['select_relation_article'];
                if ($relationArticleId == $articleId) {
                    // HACKER
                } else {
                    if (!$this->shopArticleService->addOrUpdateShopArticleGroupRelation($articleId, $relationArticleId)) {
                        $this->layout()->message = [
                            'level' => 'warn',
                            'text' => 'Es ist ein Fehler passiert :('
                        ];
                    } else {
                        $articleGroupRelationArticles = $this->shopArticleService->getShopArticleGroupRelationArticles($articleId);
                    }
                }
            } else if (isset($postData['update_article_group_relation']) && isset($postData['article_group_relation_amount_' . $postData['update_article_group_relation']])) {
                $shopArticleGroupRelationId = (int)$postData['update_article_group_relation'];
                $shopArticleGroupRelationAmount = (float)$postData['article_group_relation_amount_' . $postData['update_article_group_relation']];
                if ($this->shopArticleService->updateShopArticleGroupRelationAmount($shopArticleGroupRelationId, $shopArticleGroupRelationAmount)) {
                    $articleGroupRelationArticles = $this->shopArticleService->getShopArticleGroupRelationArticles($articleId);
                }
            } else if (isset($postData['delete_article_group_relation'])) {
                $shopArticleGroupRelationId = (int)$postData['delete_article_group_relation'];
                if ($this->shopArticleService->deleteShopArticleGroupRelation($shopArticleGroupRelationId)) {
                    $articleGroupRelationArticles = $this->shopArticleService->getShopArticleGroupRelationArticles($articleId);
                }
            }
        }
        $viewModel->setVariable('articleGroupRelationArticles', $articleGroupRelationArticles);

        $this->layout('layout/admin');
        return $viewModel;
    }

    /**
     *
     * @return ViewModel|Response
     */
    public function editArticleSizeGroupRelationAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $articleId = $this->params('article_id', 0);
        if ($articleId == 0) {
            return $this->redirect()->toRoute('shop_admin_article_articleadmin_articles');
        }
        $articleData = $this->shopArticleService->getShopArticleById($articleId);
        if (empty($articleData)) {
            return $this->redirect()->toRoute('shop_admin_article_articleadmin_articles');
        }
        $viewModel = new ViewModel();
        $viewModel->setVariable('articleData', $articleData);

        $articleSizeGroupRelationArticles = $this->shopArticleService->getShopArticleSizeGroupRelationArticles($articleId);

        $request = $this->getRequest();
        if ($request->isPost() && $request instanceof Request) {
            $postData = $request->getPost()->toArray();
            if (isset($postData['select_relation_article'])) {
                /*
                 * select_relation_article from AJAX Form
                 */
                $relationArticleId = (int)$postData['select_relation_article'];
                if ($relationArticleId == $articleId) {
                    // HACKER
                } else {
                    if (!$this->shopArticleService->addOrUpdateShopArticleSizeGroupRelation($articleId, $relationArticleId)) {
                        $this->layout()->message = [
                            'level' => 'warn',
                            'text' => 'Es ist ein Fehler passiert :('
                        ];
                    } else {
                        $articleSizeGroupRelationArticles = $this->shopArticleService->getShopArticleSizeGroupRelationArticles($articleId);
                    }
                }
            } else if (isset($postData['update_article_sizegroup_relation']) && isset($postData['shop_article_sizegroup_ralation_amount_' . $postData['update_article_sizegroup_relation']])) {
                $shopArticleSizeGroupRelationId = (int)$postData['update_article_sizegroup_relation'];
                $shopArticleGroupRelationAmount = (float)$postData['shop_article_sizegroup_ralation_amount_' . $postData['update_article_sizegroup_relation']];
                if ($this->shopArticleService->updateShopArticleSizeGroupRelationAmount($shopArticleSizeGroupRelationId, $shopArticleGroupRelationAmount)) {
                    $articleSizeGroupRelationArticles = $this->shopArticleService->getShopArticleSizeGroupRelationArticles($articleId);
                }
            } else if (isset($postData['delete_article_sizegroup_relation'])) {
                $shopArticleSizeGroupRelationId = (int)$postData['delete_article_sizegroup_relation'];
                if ($this->shopArticleService->deleteShopArticleSizeGroupRelation($shopArticleSizeGroupRelationId)) {
                    $articleSizeGroupRelationArticles = $this->shopArticleService->getShopArticleSizeGroupRelationArticles($articleId);
                }
            }
        }
        $viewModel->setVariable('articleSizeGroupRelationArticles', $articleSizeGroupRelationArticles);
        $articleSizeGroupId = 0;
        if (!empty($articleSizeGroupRelationArticles)) {
            $articleSizeDefData = $this->shopArticleService->getShopArticleSizeDefById($articleSizeGroupRelationArticles[0]['shop_article_size_def_id']);
            $articleSizeGroupId = $articleSizeDefData['shop_article_size_group_id'];
        }
        $viewModel->setVariable('articleSizeGroupId', $articleSizeGroupId);

        $this->layout('layout/admin');
        return $viewModel;
    }

    /**
     *
     * @return ViewModel|Response
     */
    public function articleStockAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(2)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $articleId = $this->params('article_id', 0);
        if ($articleId == 0) {
            return $this->redirect()->toRoute('shop_admin_article_articleadmin_articles');
        }
        $viewModel = new ViewModel();
        $viewModel->setVariable('articleId', $articleId);

        $shopVendorIdAssoc = $this->shopVendorService->getShopVendorNumberVarcharIdAssoc();
        $shopVendorIdAssoc[0] = '';
        $viewModel->setVariable('shopVendorIdAssoc', $shopVendorIdAssoc);

        $this->shopArticleStockForm->init();
        $this->shopArticleStockForm->get('shop_article_id')->setValue($articleId);
        $this->shopArticleStockForm->get('shop_article_stock_reason_type')->setValue('production');
        $viewModel->setVariable('formValid', true);

        $shopArticleOptions = $this->shopArticleOptionTablex->getShopArticleOptionItemArticleRelationsIdAssoc($articleId, false);
        $shopArticleParam2dItemsIdAssoc = $this->shopArticleParam2dService->getShopArticleParam2dItemsForArticleIdAssoc($articleId);

        $articleOptionsJson = '[]';
        /** @var Request $request */
        $request = $this->getRequest();
        if (!$request instanceof Request) {
            return $viewModel;
        }
        if ($request->isPost() && !empty($request->getPost('article_stock'))) {
            $postData = $request->getPost()->toArray();
            $this->shopArticleStockForm->setData($postData);
            if ($this->shopArticleStockForm->isValid()) {
                $formData = $this->shopArticleStockForm->getData();
                $articleStockValid = true;
                $articleOptionsJson = '[]';
                if (!empty($shopArticleOptions)) {
                    $shopArticleOptionsEntityData = [];
                    foreach ($shopArticleOptions as $shopArticleOptionDefId => $shopArticleOption) {
                        if (!array_key_exists($shopArticleOptionDefId, $postData) || empty($postData[$shopArticleOptionDefId])) {
                            $articleStockValid = false;
                        }
                        if ($articleStockValid) {
                            $tmpOptionItemIds = [];
                            foreach ($shopArticleOption as $shopArticleOptionItem) {
                                $tmpOptionItemIds[] = $shopArticleOptionItem['shop_article_option_item_id'];
                            }
                            if (!in_array($postData[$shopArticleOptionDefId], $tmpOptionItemIds)) {
                                throw new \RuntimeException('You are skriptkiddie X(');
                            }
                            $shopArticleOptionsEntityData[] = ['optiondefid' => intval($shopArticleOptionDefId), 'optionitemid' => intval($postData[$shopArticleOptionDefId])];
                        }
                    }
                    if ($articleStockValid) {
                        $this->shopArticleOptionsEntity->exchangeArrayOptions($shopArticleOptionsEntityData);
                        $articleOptionsJson = $this->shopArticleOptionsEntity->getShopArticleOptionsJson(true);
                    } else {
                        $viewModel->setVariable('articleStockMessage', ['level' => 'warn', 'text' => 'Nicht alle Produkt-Optionen sind OK!']);
                        $viewModel->setVariable('formValid', false);
                    }
                }
                $param2dItemId = null;
                if(!empty($shopArticleParam2dItemsIdAssoc)) {
                    $param2dItemId = $request->getPost('shop_article_param2d_item_id');
                    if(empty($param2dItemId)) {
                        $articleStockValid = false;
                        $viewModel->setVariable('articleStockMessage', ['level' => 'warn', 'text' => 'Es muss ein Param2D gewählt werden!']);
                        $viewModel->setVariable('formValid', false);
                    }
                }
                if ($articleStockValid) {
                    if ($this->shopArticleStockService->saveShopArticleStockData([
                            'shop_article_id' => $formData['shop_article_id'],
                            'shop_article_options' => $articleOptionsJson,
                            'shop_article_param2d_item_id' => $param2dItemId,
                            'shop_article_stock_amount' => $formData['shop_article_stock_amount'],
                            'shop_vendor_id' => $formData['shop_vendor_id'],
                            'shop_article_stock_batch' => $formData['shop_article_stock_batch'],
                            'shop_article_stock_reason_type' => $formData['shop_article_stock_reason_type'],
                            'shop_article_stock_reason' => $formData['shop_article_stock_reason']]) > 0) {
                        return $this->redirect()->toRoute('shop_admin_article_articleadmin_articlestock', ['article_id' => $articleId]);
                    } else {
                        $this->layout()->message = Messages::$messageErrorSaveData;
                    }
                }
            } else {
                $viewModel->setVariable('formValid', false);
            }
        }

        $optionItemIdsSelected = [];
        $allOptionsSelected = true; // some article has more than one option
        if ($request->isPost() && $request->getPost('article_option_select', 0) == 1) {
            $count = 0;
            $articleOptionsJson = '[';
            $postData = $request->getPost()->toArray();
            foreach ($postData as $optionDefId => $optionItemId) {
                if (!is_int($optionDefId) || !array_key_exists($optionDefId, $shopArticleOptions) || empty($optionItemId)) {
                    continue;
                }
                if ($count > 0) {
                    $articleOptionsJson .= ',';
                }
                $optionItemIdsSelected[] = $optionItemId;
                $articleOptionsJson .= '{"optiondefid":' . $optionDefId . ',"optionitemid":' . $optionItemId . '}';
                $count++;
            }
            $articleOptionsJson .= ']';
            if ($articleOptionsJson != '[]') {
                $articleOptionsJson = $this->shopArticleOptionsEntity->cleanOptionsJson($articleOptionsJson);
            }
            if (count($optionItemIdsSelected) > 0 && count($optionItemIdsSelected) != count($shopArticleOptions)) {
                $allOptionsSelected = false;
            }
        }

        $param2dItemIdSelected = 0;
        if ($request->isPost() && $request->getPost('article_param2d_select') == 1) {
            $param2dItemIdSelected = $request->getPost('shop_article_param2d_item_id', 0);
        }

        $viewModel->setVariable('shopArticle', $this->shopArticleService->getShopArticleById($articleId));
        $viewModel->setVariable('sumOrderedAndPayed', $this->basketService->sumOrderedAndPayedShopArticle($articleId, $articleOptionsJson));
        $viewModel->setVariable('shopArticleStock', $this->shopArticleStockService->searchShopArticleStock($articleId, $articleOptionsJson, $param2dItemIdSelected));
        $viewModel->setVariable('shopArticleStockList', $this->shopArticleStockService->searchShopArticleStockList($articleId, $articleOptionsJson, $param2dItemIdSelected));
        $viewModel->setVariable('shopArticleStockForm', $this->shopArticleStockForm);
        $viewModel->setVariable('shopArticleOptions', $shopArticleOptions);
        $viewModel->setVariable('allOptionsSelected', $allOptionsSelected);
        $viewModel->setVariable('optionItemIdsSelected', $optionItemIdsSelected);
        $viewModel->setVariable('shopArticleParam2dItemsIdAssoc', $shopArticleParam2dItemsIdAssoc);
        $viewModel->setVariable('param2dItemIdSelected', $param2dItemIdSelected);

        $this->layout('layout/admin');
        return $viewModel;
    }

}
