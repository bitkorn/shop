<?php

namespace Bitkorn\Shop\Controller\Admin\Article\Option;

use Bitkorn\Images\Entity\Image\ImagesEntity;
use Bitkorn\Shop\Service\Article\ShopArticleOptionService;
use Bitkorn\Shop\Service\Article\ShopArticleService;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\Response;
use Laminas\View\Model\ViewModel;
use Laminas\View\Model\JsonModel;
use Bitkorn\Shop\Controller\AbstractShopController;

/**
 *
 * @author allapow
 */
class ArticleOptionAdminController extends AbstractShopController
{

    /**
     * @var ShopArticleService
     */
    protected $shopArticleService;

    /**
     * @var ShopArticleOptionService
     */
    protected $shopArticleOptionService;

    /**
     * @param ShopArticleService $shopArticleService
     */
    public function setShopArticleService(ShopArticleService $shopArticleService): void
    {
        $this->shopArticleService = $shopArticleService;
    }

    /**
     * @param ShopArticleOptionService $shopArticleOptionService
     */
    public function setShopArticleOptionService(ShopArticleOptionService $shopArticleOptionService): void
    {
        $this->shopArticleOptionService = $shopArticleOptionService;
    }

    /**
     * Zeigt alle OptionDefs
     * mit Beziehungen zu einem Artikel (Checkbox & Button(images))
     *
     * @return ViewModel|Response
     */
    public function articleOptionDefsArticleRelationAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $articleId = $this->params('article_id');
        $this->layout('layout/admin');
//        $this->layout()->helpId = 2;
        $viewModel = new ViewModel();

        $request = $this->getRequest();
        if ($request->isPost() && $request instanceof Request) {
            $postData = $request->getPost()->toArray();
            $articleIdPost = (int)$postData['shop_article_id'];
            if ($articleIdPost != $articleId) {
                exit();
            }
            $articleOptionDefIdPost = (int)$postData['shop_article_option_def_id'];
            if (!empty($postData['shop_article_option_article_relation'])) {
                $result = $this->shopArticleOptionService->addShopArticleOptionArticleRelation($articleOptionDefIdPost, $articleId);
            } else {
                $result = $this->shopArticleOptionService->deleteShopArticleOptionArticleRelation($articleOptionDefIdPost, $articleId);
            }
            if ($result > 0) {
                $this->layout()->message = ['level' => 'info', 'text' => 'Die Änderungen wurden gespeichert'];
            }
        }

        $viewModel->setVariable('shopArticleOptionDefs', $this->shopArticleOptionService->getShopArticleOptionDefs());
        $articleData = [];
        $shopArticleOptionArticleRelations = [];
        if (!empty($articleId)) {
            $articleData = $this->shopArticleService->getShopArticleById($articleId);
            $viewModel->setVariable('articleDisplay', $articleData['shop_article_sku'] . ' | ' . $articleData['shop_article_name']);
            $shopArticleOptionArticleRelations = $this->shopArticleOptionService->getShopArticleOptionArticleRelations($articleId);
        }
        $viewModel->setVariable('articleData', $articleData);
        $viewModel->setVariable('shopArticleOptionArticleRelations', $shopArticleOptionArticleRelations);

        return $viewModel;
    }

    /**
     *
     * @return ViewModel|Response
     */
    public function articleOptionItemsArticleImageAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $defId = $this->params('def_id');
        $articleId = $this->params('article_id');
        $this->layout('layout/admin');
        $viewModel = new ViewModel();

        $request = $this->getRequest();
        if ($request->isPost() && $request instanceof Request) {
            $postData = $request->getPost()->toArray();
            if (!empty($postData['add_relation_ids']) && is_array($postData['add_relation_ids'])) {
                foreach ($postData['add_relation_ids'] as $bkImagesImageId => $optionItemId) {
                    $this->shopArticleOptionService->addShopArticleOptionItemArticleImageRelation($optionItemId, $articleId, $bkImagesImageId);
                }
                return $this->redirect()->toRoute('shop_admin_article_articleoptionadmin_articleoptionitemsarticleimage',
                    ['def_id' => $defId, 'article_id' => $articleId]);
            }
            if (isset($postData['imageDelete']) && !empty($postData['imageDelete']['optionItemArticleImageRelationId'])) {
                $optionItemArticleImageRelationId = (int)$postData['imageDelete']['optionItemArticleImageRelationId'];
                if ($this->shopArticleOptionService->deleteShopArticleOptionItemArticleImageRelation($optionItemArticleImageRelationId)) {
                    return new JsonModel(['status' => 1]);
                } else {
                    return new JsonModel(['status' => 0]);
                }
            }
            if (isset($postData['imagePriority']) && !empty($postData['imagePriority']['optionItemArticleImageRelationId'])) {
                $optionItemArticleImageRelationId = (int)$postData['imagePriority']['optionItemArticleImageRelationId'];
                $optionItemArticleImageRelationPriority = (int)$postData['imagePriority']['optionItemArticleImageRelationPriority'];
                if ($this->shopArticleOptionService->updateShopArticleOptionItemArticleImageRelationPriority($optionItemArticleImageRelationId, $optionItemArticleImageRelationPriority)) {
                    return new JsonModel(['status' => 1]);
                } else {
                    return new JsonModel(['status' => 0]);
                }
            }
        }

        $forwardUrl = $this->url()->fromRoute('shop_admin_article_articleoptionadmin_articleoptionitemsarticleimage',
            ['def_id' => $defId, 'article_id' => $articleId]);
        $viewModel->setVariable('forwardurl', urlencode($forwardUrl));

        $viewModel->setVariable('shopArticleOptionDef', $this->shopArticleOptionService->getShopArticleOptionDefById($defId));

        $articleData = $this->shopArticleService->getShopArticleById($articleId);
        $viewModel->setVariable('articleData', $articleData);
        $viewModel->setVariable('articleDisplay', $articleData['shop_article_sku'] . ' | ' . $articleData['shop_article_name']);

        $viewModel->setVariable('shopArticleOptionItems', $this->shopArticleOptionService->getShopArticleOptionItemsIdAssoc($defId));

        $shopArticleOptionItemArticleImagesBoth = $this->shopArticleOptionService->getShopArticleOptionDefArticleImagesBoth($defId, $articleId);
        $viewModel->setVariable('shopArticleOptionItemArticleImages', $shopArticleOptionItemArticleImagesBoth['idAssoc']);

        $imageEntity = new ImagesEntity();
        $imageEntity->exchangeArrayImages($shopArticleOptionItemArticleImagesBoth['stock']);
        $viewModel->setVariable('imageEntity', $imageEntity);

        return $viewModel;
    }

    /**
     * Option-Items Pricediff
     * @return ViewModel|Response
     */
    public function articleOptionItemsArticlePricediffAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $defId = $this->params('def_id');
        $articleId = $this->params('article_id');
        $this->layout('layout/admin');
//        $this->layout()->helpId = 2;
        $viewModel = new ViewModel();

        $request = $this->getRequest();
        if ($request->isPost() && $request instanceof Request) {
            $postData = $request->getPost()->toArray();
            $jsonModel = new JsonModel();
            $status = 0;
            if (isset($postData['editPriceDiff'])) {
                $itemId = (int)$postData['editPriceDiff']['itemId'];
                $newVal = (float)$postData['editPriceDiff']['newVal'];
//                $articleIdOptionItemId = explode('_', $itemId);
//                if(count($articleIdOptionItemId) != 2) {
//                    $this->getResponse()->setStatusCode(\Laminas\Http\PhpEnvironment\Response::STATUS_CODE_400);
//                    return $jsonModel;
//                }
                if ($this->shopArticleOptionService->updateShopArticleOptionItemArticlePricediff($articleId, $itemId, $newVal) >= 0) {
                    $status = 1;
                    $jsonModel->setVariable('value', $newVal);
                }
            }
            $jsonModel->setVariable('status', $status);
            return $jsonModel;
        }

        $viewModel->setVariable('shopArticleOptionDef', $this->shopArticleOptionService->getShopArticleOptionDefById($defId));

        $articleData = $this->shopArticleService->getShopArticleById($articleId);
        $viewModel->setVariable('articleData', $articleData);
        $viewModel->setVariable('articleDisplay', $articleData['shop_article_sku'] . ' | ' . $articleData['shop_article_name']);

        $shopArticleOptionItems = $this->shopArticleOptionService->getShopArticleOptionItemsIdAssoc($defId);
        $viewModel->setVariable('shopArticleOptionItems', $shopArticleOptionItems);

        $shopArticleOptionItemIds = [];
        foreach ($shopArticleOptionItems as $shopArticleOptionItem) {
            $shopArticleOptionItemIds[] = $shopArticleOptionItem['shop_article_option_item_id'];
        }

        $viewModel->setVariable('shopArticleOptionItemArticlePricediffs', $this->shopArticleOptionService->getShopArticleOptionItemArticlePricediffsIdsAssoc($shopArticleOptionItemIds, $articleId));

        return $viewModel;
    }

    /**
     * Listet alle ArticleOptionDefs
     * mit Link zu einer ArticleOptionDef,
     * die dann Links zu den ArticleOptionItems hat
     * @return ViewModel|Response
     */
    public function articleOptionDefsAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $this->layout('layout/admin');
//        $this->layout()->helpId = 2;
        $viewModel = new ViewModel();

        $viewModel->setVariable('optionDefs', $this->shopArticleOptionService->getShopArticleOptionDefs());

        return $viewModel;
    }

    /**
     * Zeigt eine ArticleOptionDef mit ihren ArticleOptionItems
     * @return ViewModel|Response
     */
    public function articleOptionDefAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $defId = $this->params('def_id');
        if (!isset($defId)) {
            return $this->redirect()->toRoute('shop_admin_article_articleoptionadmin_articleoptiondefs');
        }
        $this->layout('layout/admin');
//        $this->layout()->helpId = 2;
        $viewModel = new ViewModel();

        $request = $this->getRequest();
        if ($request->isPost() && $request instanceof Request) {
//            $postData = $request->getPost()->toArray();
//            $this->getLogger()->debug(print_r($postData, true));
//            $jsonModel = new JsonModel();
//            $status = 0;
//            if(isset($postData['editPriceDiff'])) {
//                $itemId = (int)$postData['editPriceDiff']['itemId'];
//                $newVal = (float)$postData['editPriceDiff']['newVal'];
//                if($this->shopArticleOptionItemTable->updateShopArticleOptionItemPricediff($itemId, $newVal) >= 0) {
//                    $status = 1;
//                    $jsonModel->setVariable('value', $newVal);
//                }
//            }
//            $jsonModel->setVariable('status', $status);
//            return $jsonModel;
        }

        $optionDef = $this->shopArticleOptionService->getShopArticleOptionDefById($defId);
        $viewModel->setVariable('optionDef', $optionDef);

        if (!empty($optionDef)) {
            $viewModel->setVariable('optionItems', $this->shopArticleOptionService->getShopArticleOptionItemsByDefId($defId));
        }

        $this->layout()->message = ['level' => 'info', 'text' => 'coming soon'];

        return $viewModel;
    }

}
