<?php

namespace Bitkorn\Shop\Controller\Admin\Article\Category;

use Bitkorn\Shop\Entity\Article\ShopArticleCategoryEntity;
use Bitkorn\Shop\Form\Article\Category\ShopArticleCategoryForm;
use Bitkorn\Shop\Service\Article\ShopArticleService;
use Bitkorn\Shop\Table\Article\ShopArticleCategoryTable;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\Response;
use Laminas\View\Model\ViewModel;
use Bitkorn\Shop\Controller\AbstractShopController;

/**
 * Description of ArticleCategoryAdminController
 *
 * @author allapow
 */
class ArticleCategoryAdminController extends AbstractShopController
{
    protected ShopArticleService $shopArticleService;
    protected ShopArticleCategoryForm $shopArticleCategoryForm;
    protected ShopArticleCategoryTable $shopArticleCategoryTable;

    public function setShopArticleService(ShopArticleService $shopArticleService): void
    {
        $this->shopArticleService = $shopArticleService;
    }

    public function setShopArticleCategoryForm(ShopArticleCategoryForm $shopArticleCategoryForm): void
    {
        $this->shopArticleCategoryForm = $shopArticleCategoryForm;
    }

    public function setShopArticleCategoryTable(ShopArticleCategoryTable $shopArticleCategoryTable): void
    {
        $this->shopArticleCategoryTable = $shopArticleCategoryTable;
    }

    /**
     *
     * @return ViewModel|Response
     */
    public function articleCategoriesAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $viewModel = new ViewModel();
        $this->layout('layout/admin');
//        $this->layout()->helpId = 1;

        $categories = $this->shopArticleService->getShopArticleCategories();
        $viewModel->setVariable('categories', $categories);
        $viewModel->setVariable('textarea', $categories);
        $categoryIdAssoc = $this->shopArticleService->getShopArticleCategoryIdAssoc();
        $viewModel->setVariable('categoryIdAssoc', $categoryIdAssoc);

        return $viewModel;
    }

    /**
     *
     * @return ViewModel|Response
     */
    public function articleCategoryAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $categoryId = $this->params('category_id');
        if (empty($categoryId)) {
            return $this->redirect()->toRoute('shop_admin_article_articleadmin_articlecategories');
        }
        $viewModel = new ViewModel();
        $this->layout('layout/admin');
//        $this->layout()->helpId = 1;

        $category = $this->shopArticleService->getShopArticleCategoryById($categoryId);
        $viewModel->setVariable('category', $category);
        $viewModel->setVariable('textarea', $category);

        $this->shopArticleCategoryForm->init();
        $this->shopArticleCategoryForm->setData($category);


        $request = $this->getRequest();
        if ($request->isPost() && $request instanceof Request) {
            $postData = $request->getPost()->toArray();
            $this->shopArticleCategoryForm->setData($postData);
            if ($this->shopArticleCategoryForm->isValid()) {
                $formData = $this->shopArticleCategoryForm->getData();
                $shopArticleCategoryEntity = new ShopArticleCategoryEntity();
                $shopArticleCategoryEntity->exchangeArray($formData);
                if ($shopArticleCategoryEntity->update($this->shopArticleCategoryTable) < 0) {
                    $this->layout()->message = ['level' => 'error', 'text' => 'Es gab einen Fehler beim Speichern der Daten.'];
                }
            }
        }

        $viewModel->setVariable('categoryForm', $this->shopArticleCategoryForm);
        return $viewModel;
    }

}
