<?php

namespace Bitkorn\Shop\Controller\Admin\Article;

use Bitkorn\Images\Entity\Image\ImagesEntity;
use Bitkorn\Images\Form\Image\ImageUploadForm;
use Bitkorn\Images\Service\Image\ImageService;
use Bitkorn\Images\Tools\Image\ImageScale;
use Bitkorn\Images\Tools\Image\ImageWatermark;
use Bitkorn\Shop\Service\Article\ShopArticleImageService;
use Bitkorn\Shop\Service\Article\ShopArticleService;
use Bitkorn\Trinket\Tools\Filesystem\FilenameTool;
use Bitkorn\Trinket\Tools\Filesystem\FolderTool;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\Response;
use Laminas\View\Model\JsonModel;
use Laminas\View\Model\ViewModel;
use Bitkorn\Shop\Controller\AbstractShopController;

/**
 *
 * @author allapow
 */
class ArticleImageAdminController extends AbstractShopController
{

    /**
     * @var array
     */
    protected $imagesConfig;

    /**
     *
     * @var ImageUploadForm
     */
    protected $imageUploadForm;

    /**
     * @var ImageService
     */
    protected $imageService;

    /**
     * @var ShopArticleImageService
     */
    protected $shopArticleImageService;

    /**
     * @var ShopArticleService
     */
    protected $shopArticleService;

    /**
     * @param array $imagesConfig
     */
    public function setImagesConfig(array $imagesConfig): void
    {
        $this->imagesConfig = $imagesConfig;
    }

    /**
     * @param ImageUploadForm $imageUploadForm
     */
    public function setImageUploadForm(ImageUploadForm $imageUploadForm): void
    {
        $this->imageUploadForm = $imageUploadForm;
    }

    /**
     * @param ImageService $imageService
     */
    public function setImageService(ImageService $imageService): void
    {
        $this->imageService = $imageService;
    }

    /**
     * @param ShopArticleImageService $shopArticleImageService
     */
    public function setShopArticleImageService(ShopArticleImageService $shopArticleImageService): void
    {
        $this->shopArticleImageService = $shopArticleImageService;
    }

    /**
     * @param ShopArticleService $shopArticleService
     */
    public function setShopArticleService(ShopArticleService $shopArticleService): void
    {
        $this->shopArticleService = $shopArticleService;
    }

    /**
     *
     * @return ViewModel|Response
     */
    public function editArticleImagesAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(3)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $articleId = $this->params('article_id', 0);
        if ($articleId == 0) {
            return $this->redirect()->toRoute('shop_admin_article_articleadmin_articles');
        }
        $articleData = $this->shopArticleService->getShopArticleById($articleId);
        if (empty($articleData)) {
            return $this->redirect()->toRoute('shop_admin_article_articleadmin_articles');
        }
        $this->layout()->helpId = 3;

        $viewModel = new ViewModel();
        $viewModel->setVariable('articleData', $articleData);
        $viewModel->setVariable('forwardurl', urlencode($this->url()->fromRoute('shop_admin_article_articleimageadmin_editarticleimages', ['article_id' => $articleId])));

        $shopArticleImages = $this->shopArticleImageService->getShopArticleImages($articleId);

        $request = $this->getRequest();
        if ($request->isPost() && $request instanceof Request) {
            $postData = $request->getPost()->toArray();
            if (isset($postData['imageDelete']['imageId'])) {
                $jsonModel = new JsonModel();
                $imageIdDelete = (int)$postData['imageDelete']['imageId'];
                $imageData = $this->shopArticleImageService->getShopArticleImageById($imageIdDelete);
                if (empty($imageData) || $imageData['shop_article_id'] != $articleId) {
                    return $jsonModel;
                }
                if ($this->shopArticleImageService->deleteShopArticleImageById($imageIdDelete) == 1) {
                    $jsonModel->setVariable('status', 1);
                    return $jsonModel;
                }
            }
            if (!empty($postData['add_relation_imageid']) && is_array($postData['add_relation_imageid'])) {
                foreach ($postData['add_relation_imageid'] as $ralationImageId) {
                    $ralationImageId = (int)$ralationImageId;
                    if (!$this->shopArticleImageService->existImagesImageForArticle($articleId, $ralationImageId)) {
                        $this->shopArticleImageService->saveShopArticleImage(['shop_article_id' => $articleId, 'bk_images_image_id' => $ralationImageId]);
                    }
                }
                $shopArticleImages = $this->shopArticleImageService->getShopArticleImages($articleId);
            }
            if (isset($postData['imagePriority']) && !empty($postData['imagePriority']['articleImageId'])) {
                $articleImageId = (int)$postData['imagePriority']['articleImageId'];
                $articleImagePriority = (int)$postData['imagePriority']['articleImagePriority'];
                if ($this->shopArticleImageService->updateShopArticleImagePriority($articleImageId, $articleImagePriority)) {
                    return new JsonModel(['status' => 1]);
                } else {
                    return new JsonModel(['status' => 0]);
                }
            }
        }

        $viewModel->setVariable('shopArticleImages', $shopArticleImages);
        $imageEntity = new ImagesEntity();
        $imageEntity->exchangeArrayImages($shopArticleImages);
        $viewModel->setVariable('imageEntity', $imageEntity);

        $this->layout('layout/admin');

        return $viewModel;
    }

    /**
     * Special Upload for ShopArticles:
     * - force bk_images_imagegroup_id = 2
     * @return ViewModel|Response
     */
    public function uploadImageAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $articleId = $this->params('article_id', 0);
        if ($articleId == 0) {
            return $this->redirect()->toRoute('shop_admin_article_articleadmin_articles');
        }
        $articleData = $this->shopArticleService->getShopArticleById($articleId);
        if (empty($articleData)) {
            return $this->redirect()->toRoute('shop_admin_article_articleadmin_articles');
        }

        $viewModel = new ViewModel();
        $viewModel->setVariable('articleId', $articleId);
        $viewModel->setVariable('articleData', $articleData);
        $redirecturl = $this->url()->fromRoute('shop_admin_article_articleimageadmin_editarticleimages', ['article_id' => $articleId]);

        $this->imageUploadForm->init();

        $request = $this->getRequest();
        if ($request->isPost() && $request instanceof Request) {
            $postData = array_merge_recursive($request->getPost()->toArray(), $request->getFiles()->toArray());
            $this->imageUploadForm->setData($postData);
            if ($this->imageUploadForm->isValid()) {
                $formData = $this->imageUploadForm->getData();
                $fileinfo = new \finfo(FILEINFO_MIME);
                $extension01 = explode(';', $fileinfo->file($formData['bk_images_image_image']['tmp_name']));
                $extension02 = explode('/', $extension01[0]);
                $extension = $extension02[1];
                if (empty($extension)) {
                    $this->layout()->message = [
                        'level' => 'warn',
                        'text' => 'Die Datei ist defekt. Bitte benutze eine andere oder repariere diese.'
                    ];
                } else {
                    $imageIdNew = $this->imageService->insertAndUploadImageWithStandardScalings($formData, $extension);
                    if ($imageIdNew > 0 && $this->shopArticleImageService->saveShopArticleImage(['shop_article_id' => $articleId, 'bk_images_image_id' => $imageIdNew])) {
                        return $this->redirect()->toUrl($redirecturl);
                    } else {
                        if ($imageIdNew > 0) {
                            $this->imageService->deleteImageComplete($imageIdNew);
                        }
                        $this->layout()->message = [
                            'level' => 'success',
                            'text' => 'Bild-Upload und Datenspeicherung OK.'
                        ];
                    }

                    $this->imageUploadForm->init();
                }
            }
        }

        $viewModel->setVariable('imageForm', $this->imageUploadForm);

        $this->layout('layout/admin');
        return $viewModel;
    }

}
