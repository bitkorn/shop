<?php

namespace Bitkorn\Shop\Controller\Admin\Vendor;

use Bitkorn\Shop\Controller\AbstractShopController;
use Bitkorn\Shop\Form\Vendor\ShopVendorForm;
use Bitkorn\Shop\Service\Vendor\ShopVendorService;
use Bitkorn\Shop\Service\Vendor\VendorNumberService;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\Response;
use Laminas\View\Model\ViewModel;

/**
 *
 * @author allapow
 */
class VendorAdminController extends AbstractShopController
{

    /**
     *
     * @var ShopVendorForm
     */
    protected $shopVendorForm;

    /**
     * @var ShopVendorService
     */
    protected $shopVendorService;

    /**
     *
     * @var VendorNumberService
     */
    protected $vendorNumberService;

    /**
     * @param ShopVendorForm $shopVendorForm
     */
    public function setShopVendorForm(ShopVendorForm $shopVendorForm): void
    {
        $this->shopVendorForm = $shopVendorForm;
    }

    /**
     * @param ShopVendorService $shopVendorService
     */
    public function setShopVendorService(ShopVendorService $shopVendorService): void
    {
        $this->shopVendorService = $shopVendorService;
    }

    /**
     * @param VendorNumberService $vendorNumberService
     */
    public function setVendorNumberService(VendorNumberService $vendorNumberService): void
    {
        $this->vendorNumberService = $vendorNumberService;
    }

    /**
     *
     * @return ViewModel|Response
     */
    public function vendorsAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $viewModel = new ViewModel();
        $viewModel->setVariable('vendors', $this->shopVendorService->getShopVendors());

//        $viewModel->setVariable('vendorNoMax', $this->getShopVendorTable()->getShopVendorNoMax());

        $this->layout('layout/admin');

        return $viewModel;
    }

    /**
     *
     * @return ViewModel|Response
     */
    public function vendorAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $vendorId = $this->params('vendor_id');
        $viewModel = new ViewModel();
        $vendor = $this->shopVendorService->getShopVendorById($vendorId);
        $viewModel->setVariable('vendor', $vendor);
        $this->shopVendorForm->init();
        $this->shopVendorForm->setData($vendor);
        $viewModel->setVariable('shopVendorForm', $this->shopVendorForm);

        $request = $this->getRequest();
        if ($request->isPost() && $request instanceof Request) {
            $postData = $request->getPost()->toArray();
            $this->shopVendorForm->setData($postData);
            if ($this->shopVendorForm->isValid()) {
                $formData = $this->shopVendorForm->getData();
                unset($formData['submit']);
                $updateResult = $this->shopVendorService->updateShopVendor($vendorId, $formData);
                if ($updateResult > 0) {
                    $this->layout()->message = ['level' => 'info', 'text' => 'Die Daten wurden aktualisiert.'];
                }
                if ($updateResult < 0) {
                    $this->layout()->message = ['level' => 'warn', 'text' => 'Bei der Aktualisierung der Daten ist ein Fehler aufgetreten.'];
                }
            }
        }

        $this->layout('layout/admin');

        return $viewModel;
    }

    /**
     *
     * @return ViewModel|Response
     */
    public function addVendorAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $viewModel = new ViewModel();
        $this->shopVendorForm->init();
        $viewModel->setVariable('shopVendorForm', $this->shopVendorForm);

        $request = $this->getRequest();
        if ($request->isPost() && $request instanceof Request) {
            $postData = $request->getPost()->toArray();
            if (empty($postData['shop_vendor_min_order_value'])) {
                $postData['shop_vendor_min_order_value'] = 0;
            }
            $this->shopVendorForm->setData($postData);
            if ($this->shopVendorForm->isValid()) {
                $formData = $this->shopVendorForm->getData();
                unset($formData['submit']);
                $vendorGroup = $this->shopVendorService->getShopVendorGroupById($formData['shop_vendor_group_id']);
                $newVendorNumber = $this->vendorNumberService->getNewVendorNumber($vendorGroup['shop_vendor_group_no']);
                $formData['shop_vendor_number_int'] = $newVendorNumber['int'];
                $formData['shop_vendor_number_varchar'] = $newVendorNumber['string'];
                $updateResult = $this->shopVendorService->saveShopVendor($formData);
                if ($updateResult > 0) {
                    return $this->redirect()->toRoute('shop_admin_vendor_vendor', ['vendor_id' => $updateResult]);
                }
                if ($updateResult < 0) {
                    $this->layout()->message = ['level' => 'warn', 'text' => 'Beim Speichern der Daten ist ein Fehler aufgetreten.'];
                }
            }
        }

        $this->layout('layout/admin');

        return $viewModel;
    }

}
