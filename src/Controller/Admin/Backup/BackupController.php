<?php
/**
 * User: Torsten Brieskorn
 * Date: 13.12.17
 */

namespace Bitkorn\Shop\Controller\Admin\Backup;

use Bitkorn\Shop\Controller\AbstractShopController;
use Laminas\Db\Adapter\Adapter;
use Laminas\View\Model\JsonModel;

class BackupController extends AbstractShopController
{

    /**
     *
     * @var Adapter
     */
    protected $adapter;

    /**
     * @param Adapter $adapter
     */
    public function setAdapter($adapter)
    {
        $this->adapter = $adapter;
    }

    /**
     * http://systemgurt.local/shopadmin-backup-complete
     *
     * @return JsonModel
     */
    public function backupCompleteAction()
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(2)) {
            $jsonModel->setVariable('error', 'Only Manager or higher can perform this action.');
            return $jsonModel;
        }
        $success = true;
        $messages = [];
        $cmdOut = [];
        $timestamp = time();
        exec('mysqldump -V', $cmdOut);
        if (!isset($cmdOut[0]) || strpos($cmdOut[0], 'mysqldump') != 0) {
            $success = false;
            $messages[] = 'mysqldump are not installed.';
        }
        $tmpFolder = sys_get_temp_dir() . '/';
        if (!file_exists($tmpFolder) || !is_writable($tmpFolder)) {
            $success = false;
            $messages[] = 'Temporary folder does not exist or is not writable.';
        }
        $dbdumpsFolder = $this->config['bitkorn_shop']['dbdumps_path_absolute'] . '/';
        if (!file_exists($dbdumpsFolder) || !is_writable($dbdumpsFolder)) {
            $success = false;
            $messages[] = 'Database dumps folder does not exist or is not writable.';
        }
        $currentShema = $this->adapter->getCurrentSchema();
        if ($success) {
            $filename = $currentShema . date('YmdHi', $timestamp) . '_autoBackup.sql';
            $fqfn = $dbdumpsFolder . $filename;
            $dbUser = $this->config['db']['adapters']['dbDefault']['username'];
            $dbPasswd = $this->config['db']['adapters']['dbDefault']['password'];
            $cmdOut = [];
            exec("mysqldump --opt --host=localhost --user=$dbUser --password=$dbPasswd $currentShema > $fqfn", $cmdOut);
            if (!empty($cmdOut)) {
                $success = false;
                $messages[] = 'Error while dump database.';
                $messages = array_merge($messages, $cmdOut);
            }
        }
        $fqfnOut = '';
        if ($success) {
            ini_set('memory_limit', '512M');
            ini_set('max_execution_time', 1800); // 1800 = 30 min.
            $fqfnOut = $tmpFolder . date('YmdHi', $timestamp) . '_autoBackup_' . hash('sha256', $timestamp . 'zumHwrt3762vfsd') . '.tar.gz';
            $cmd = 'tar -czf "' . $fqfnOut . '" --exclude="vendor" --exclude="module" --exclude="data/tmp" --exclude="data/cache" --exclude=".idea" --exclude="nbproject" ' . getcwd();
            $cmdOut = [];
            exec($cmd, $cmdOut);
            if (!file_exists($fqfnOut)) {
                $success = false;
                $messages[] = 'Error while compress and save app folder.';
            }
        }
        if (!empty($fqfnOut) && $success) {
            header('Content-type: application/octet-stream'); // octet-stream | tar+gzip
            header('Content-Disposition: attachment; filename="completeBackup' . date('Y-m-d_H:i', $timestamp) . '.tar.gz"'); // inline || attachment
            header('Content-length: ' . filesize($fqfnOut));
            header('Content-Transfer-Encoding: binary');
            header('Accept-Ranges: bytes');
            readfile($fqfnOut);
            exit();
        }

        $jsonModel->setVariable('success', $success);
        $jsonModel->setVariable('messages', $messages);
        return $jsonModel;
    }

}
