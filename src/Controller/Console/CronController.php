<?php

namespace Bitkorn\Shop\Controller\Console;

use Bitkorn\Mail\Mail\MailWrapper;
use Bitkorn\Shop\Table\Basket\ShopBasketEntityTable;
use Bitkorn\Shop\Table\Common\ShopConfigurationTable;
use Bitkorn\Shop\View\Helper\Basket\Email\Rating\RatingReminder;
use Laminas\Log\Logger;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\HelperPluginManager;
use Laminas\Console\Request as ConsoleRequest;

/**
 *
 * @author allapow
 */
class CronController extends AbstractActionController
{

    /**
     *
     * @var Logger
     */
    private $logger;

    /**
     *
     * @var ShopConfigurationTable
     */
    private $shopConfigurationTable;

    /**
     *
     * @var ShopBasketEntityTable
     */
    private $shopBasketEntityTable;

    /**
     *
     * @var HelperPluginManager
     */
    private $viewHelperManager;

    /**
     *
     * @var MailWrapper
     */
    private $mailWrapper;

    /**
     * run this action with (e.g.):
     * cd /var/www/systemgurt/public/
     * php index.php sendratingreminder all
     * @throws \RuntimeException
     */
    public function sendRatingReminderAction()
    {
        $request = $this->getRequest();
        if (!$request instanceof ConsoleRequest) {
            throw new \RuntimeException('You can only use this action from a console!');
        }

        $basketEntitiesBasketuniqueAssoc = [];

        if ($request->getParam('all', false)) {
            $basketEntitiesBasketuniqueAssoc = $this->shopBasketEntityTable->getShopBasketEntitiesWithoutRatingReminderBasketuniqueAssoc(0);
        } elseif ($request->getParam('one', false)) {
            $basketEntityId = $request->getParam('basket_entity_id');
            $basketEntitiesBasketuniqueAssoc = $this->shopBasketEntityTable->getShopBasketEntitiesWithoutRatingReminderBasketuniqueAssoc(-1, $basketEntityId);
        }

        $this->mailWrapper->setFromEmail($this->shopConfigurationTable->getShopConfigurationValue('email_basket_from_email'));
        $this->mailWrapper->setFromName($this->shopConfigurationTable->getShopConfigurationValue('email_basket_from_name'));
        $this->mailWrapper->setSubject($this->shopConfigurationTable->getShopConfigurationValue('email_basketrating_subject'));

        $currentTime = time();

        $ratingReminderViewHelper = $this->viewHelperManager->get(RatingReminder::class);
        foreach ($basketEntitiesBasketuniqueAssoc as $basketEntities) {
            $htmlEmail = call_user_func($ratingReminderViewHelper, $basketEntities);
            if (!empty($basketEntities[0]['email'])) {
                $this->mailWrapper->setToName($basketEntities[0]['shop_user_address_invoice_name1'] . ' ' . $basketEntities[0]['shop_user_address_invoice_name2']);
                $this->mailWrapper->setToEmail($basketEntities[0]['email']);
                $this->mailWrapper->setTextHtml($htmlEmail);
            } elseif (!empty($basketEntities[0]['shop_basket_address_invoice_email'])) {
                $this->mailWrapper->setToName($basketEntities[0]['shop_basket_address_invoice_name1'] . ' ' . $basketEntities[0]['shop_basket_address_invoice_name2']);
                $this->mailWrapper->setToEmail($basketEntities[0]['shop_basket_address_invoice_email']);
                $this->mailWrapper->setTextHtml($htmlEmail);
            } else {
                // kakke
            }
            if ($this->mailWrapper->sendMail() != 1) {
                // kakke
            } else {
                $this->shopBasketEntityTable->update(['shop_basket_entity_rating_reminder_send' => $currentTime], ['shop_basket_unique' => $basketEntities[0]['shop_basket_unique']]);
            }
            $this->mailWrapper->resetMailRecipient();
        }
    }

    /**
     *
     * @param Logger $logger
     */
    public function setLogger(Logger $logger)
    {
        $this->logger = $logger;
    }

    /**
     *
     * @param ShopConfigurationTable $shopConfigurationTable
     */
    public function setShopConfigurationTable(ShopConfigurationTable $shopConfigurationTable)
    {
        $this->shopConfigurationTable = $shopConfigurationTable;
    }

    /**
     *
     * @param ShopBasketEntityTable $shopBasketEntityTable
     */
    public function setShopBasketEntityTable(ShopBasketEntityTable $shopBasketEntityTable)
    {
        $this->shopBasketEntityTable = $shopBasketEntityTable;
    }

    /**
     *
     * @param HelperPluginManager $viewHelperManager
     */
    public function setViewHelperManager(HelperPluginManager $viewHelperManager)
    {
        $this->viewHelperManager = $viewHelperManager;
    }

    /**
     *
     * @param MailWrapper $mailWrapper
     */
    public function setMailWrapper(MailWrapper $mailWrapper)
    {
        $this->mailWrapper = $mailWrapper;
    }

}
