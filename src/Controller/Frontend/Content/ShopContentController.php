<?php

namespace Bitkorn\Shop\Controller\Frontend\Content;

use Bitkorn\Shop\Controller\AbstractShopController;
use Bitkorn\Shop\Service\Content\ShopContentService;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\PhpEnvironment\Response;
use Laminas\View\Model\ViewModel;

class ShopContentController extends AbstractShopController
{
    /**
     * @var ShopContentService
     */
    protected $shopContentService;

    /**
     * @param ShopContentService $shopContentService
     */
    public function setShopContentService(ShopContentService $shopContentService): void
    {
        $this->shopContentService = $shopContentService;
    }

    /**
     * @return ViewModel
     */
    public function staticContentAction()
    {
        $viewModel = new ViewModel();
        $content = $this->shopContentService->getContentByAlias($this->params('content_alias'));
        if (empty($content)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_404);
        } else {
            $viewModel->setVariable('content', $content['shop_content_content']);
        }
        return $viewModel;
    }
}
