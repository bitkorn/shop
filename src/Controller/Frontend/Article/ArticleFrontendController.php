<?php

namespace Bitkorn\Shop\Controller\Frontend\Article;

use Bitkorn\Images\Entity\Image\ImagesEntity;
use Bitkorn\Shop\Service\Article\ShopArticleImageService;
use Bitkorn\Shop\Service\Article\ShopArticleOptionService;
use Bitkorn\Shop\Service\Article\ShopArticleParam2dService;
use Bitkorn\Shop\Service\Article\ShopArticleService;
use Bitkorn\Shop\Service\Article\ShopArticleStockService;
use Bitkorn\Shop\Service\ShopService;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\Response;
use Laminas\View\Model\ViewModel;
use Bitkorn\Shop\Controller\AbstractShopController;
use Laminas\View\Renderer\RendererInterface;

class ArticleFrontendController extends AbstractShopController
{
    /**
     * @var ShopArticleService
     */
    protected $shopArticleService;

    /**
     * @var ShopArticleImageService
     */
    protected $shopArticleImageService;

    /**
     * @var ShopService
     */
    protected $shopService;

    /**
     * @var ShopArticleOptionService
     */
    protected $shopArticleOptionService;

    /**
     * @var ShopArticleParam2dService
     */
    protected $shopArticleParam2dService;
    /**
     * @var ShopArticleStockService
     */
    protected $shopArticleStockService;

    /**
     *
     * @var RendererInterface
     */
    protected $viewRender;

    /**
     * @param ShopArticleService $shopArticleService
     */
    public function setShopArticleService(ShopArticleService $shopArticleService): void
    {
        $this->shopArticleService = $shopArticleService;
    }

    /**
     * @param ShopArticleImageService $shopArticleImageService
     */
    public function setShopArticleImageService(ShopArticleImageService $shopArticleImageService): void
    {
        $this->shopArticleImageService = $shopArticleImageService;
    }

    /**
     * @param ShopService $shopService
     */
    public function setShopService(ShopService $shopService): void
    {
        $this->shopService = $shopService;
    }

    /**
     * @param ShopArticleOptionService $shopArticleOptionService
     */
    public function setShopArticleOptionService(ShopArticleOptionService $shopArticleOptionService): void
    {
        $this->shopArticleOptionService = $shopArticleOptionService;
    }

    /**
     * @param ShopArticleParam2dService $shopArticleParam2dService
     */
    public function setShopArticleParam2dService(ShopArticleParam2dService $shopArticleParam2dService): void
    {
        $this->shopArticleParam2dService = $shopArticleParam2dService;
    }

    /**
     * @param ShopArticleStockService $shopArticleStockService
     */
    public function setShopArticleStockService(ShopArticleStockService $shopArticleStockService): void
    {
        $this->shopArticleStockService = $shopArticleStockService;
    }

    public function setViewRender(RendererInterface $viewRender)
    {
        $this->viewRender = $viewRender;
    }

    /**
     *
     * @return ViewModel
     */
    public function articlesAction()
    {
        $viewModel = new ViewModel();
        $articleCategoryAlias = filter_var($this->params('article_category_alias', ''), FILTER_SANITIZE_STRING);
        $articlesData = $this->shopArticleService->searchShopArticles('', $articleCategoryAlias, 0, '', '');

        $viewModel->setVariable('articles', $articlesData);
        $categoryData = $this->shopArticleService->getShopArticleCategoryByAlias($articleCategoryAlias ?? '');
        $viewModel->setVariable('category', $categoryData);

        $categoryDescColLarge = 12;
        $categoryDescColMedium = 12;
        if (!empty($categoryData['shop_article_category_img'])) {
            $categoryDescColLarge = 8;
            $categoryDescColMedium = 6;
        }
        $viewModel->setVariable('categoryDescColLarge', $categoryDescColLarge);
        $viewModel->setVariable('categoryDescColMedium', $categoryDescColMedium);

        $sidebarViewModel = new ViewModel();
        $sidebarViewModel->setTemplate('template/sidebar/article-harness');
        $sidebarViewModel->setVariable('shopArticleCategoryIdAssoc', $this->shopArticleService->getShopArticleCategoryIdAssoc(1));
        $sidebarViewModel->setVariable('routematch', $this->layout()->routematch);
        $sidebarViewModel->setVariable('routematchParams', $this->layout()->routematchParams);
        $this->layout()->sitebarLeft = $this->viewRender->render($sidebarViewModel);
        return $viewModel;
    }

    /**
     *
     * @return ViewModel|Response
     */
    public function articleAction()
    {
        $articleSefurl = $this->params('article_sefurl');
        if (empty($articleSefurl)) {
            return $this->redirect()->toRoute('shop_frontend_article_articles');
        }
        $articleData = $this->shopArticleService->getShopArticleBySefurl($articleSefurl, true);
        if (empty($articleData)) {
            return $this->redirect()->toRoute('shop_frontend_article_articles');
        }
        $viewModel = new ViewModel();
        $viewModel->setVariable('articleData', $articleData);

        $articleId = $articleData['shop_article_id'];

        if ($articleData['shop_article_stock_using'] == 1) {
            $viewModel->setVariable('stockAmountSum', $this->shopArticleStockService->getShopArticleStockAmount($articleId, '[]'));
        }

        $shopArticleImagesData = $this->shopArticleImageService->getShopArticleImages($articleId);
        $viewModel->setVariable('shopArticleImagesData', $shopArticleImagesData);
        $imageEntity = new ImagesEntity();
        $imageEntity->exchangeArrayImages($shopArticleImagesData);
        $viewModel->setVariable('imageEntity', $imageEntity);
        $viewModel->setVariable('shopName', $this->shopService->getShopConfigurationValue('shop_name'));

        $sidebarViewModel = new ViewModel();
        $sidebarViewModel->setTemplate('template/sidebar/article-harness');
        $sidebarViewModel->setVariable('shopArticleCategoryIdAssoc', $this->shopArticleService->getShopArticleCategoryIdAssoc(1));
        $sidebarViewModel->setVariable('routematch', $this->layout()->routematch);
        $sidebarViewModel->setVariable('routematchParams', $this->layout()->routematchParams);
        $this->layout()->sitebarLeft = $this->viewRender->render($sidebarViewModel);

        $request = $this->getRequest();
        if ($request->isPost() && $request instanceof Request) {
            $ratingValue = filter_input(INPUT_POST, 'shop_article_rating_value', FILTER_SANITIZE_NUMBER_INT);
            $ratingText = filter_input(INPUT_POST, 'shop_article_rating_text', FILTER_SANITIZE_STRING);
            if ($ratingValue) {
                if ($this->shopArticleService->saveShopArticleRating($articleId, $ratingValue, $ratingText)) {
                    $this->layout()->message = ['level' => 'success', 'text' => 'Danke für Ihre Meinung. Nach redaktioneller Prüfung ist Ihre Bewertung sichtbar.'];
                }
            }
        }

        return $viewModel;
    }

    /**
     *
     * @return ViewModel|Response
     */
    public function configArticleAction()
    {
        $articleSefurl = $this->params('article_sefurl');
        if (empty($articleSefurl)) {
            return $this->redirect()->toRoute('shop_frontend_article_articles');
        }
        $articleData = $this->shopArticleService->getShopArticleBySefurl($articleSefurl, true);
        if (empty($articleData)) {
            return $this->redirect()->toRoute('shop_frontend_article_articles');
        }
        $viewModel = new ViewModel();
        $viewModel->setVariable('articleData', $articleData);

        $articleId = $articleData['shop_article_id'];

        $shopArticleImagesData = $this->shopArticleImageService->getShopArticleImages($articleId);
        $viewModel->setVariable('shopArticleImagesData', $shopArticleImagesData);
        $imageEntity = new ImagesEntity();
        $imageEntity->exchangeArrayImages($shopArticleImagesData);
        $viewModel->setVariable('imageEntity', $imageEntity);
        $viewModel->setVariable('shopName', $this->shopService->getShopConfigurationValue('shop_name'));

        $sidebarViewModel = new ViewModel();
        $sidebarViewModel->setTemplate('template/sidebar/article-harness');
        $sidebarViewModel->setVariable('shopArticleCategoryIdAssoc', $this->shopArticleService->getShopArticleCategoryIdAssoc(1));
        $sidebarViewModel->setVariable('routematch', $this->layout()->routematch);
        $sidebarViewModel->setVariable('routematchParams', $this->layout()->routematchParams);
        $this->layout()->sitebarLeft = $this->viewRender->render($sidebarViewModel);

        $shopArticleOptionsIdAssoc = $this->shopArticleOptionService->getShopArticleOptionItemArticleRelationsIdAssoc($articleId, true);
        if (!empty($shopArticleOptionsIdAssoc['pricediff'])) {
            $viewModel->setVariable('isPricediff', true);
        }
        unset($shopArticleOptionsIdAssoc['pricediff']);
        $viewModel->setVariable('shopArticleOptionsIdAssoc', $shopArticleOptionsIdAssoc);

        $optionItemId = (int)$this->params()->fromQuery('optionitemid');
        if (!empty($optionItemId)) {
            $viewModel->setVariable('optionItemIdPreselection', $optionItemId);
        }

        $request = $this->getRequest();
        if ($request->isPost() && $request instanceof Request) {
            $ratingValue = filter_input(INPUT_POST, 'shop_article_rating_value', FILTER_SANITIZE_NUMBER_INT);
            $ratingText = filter_input(INPUT_POST, 'shop_article_rating_text', FILTER_SANITIZE_STRING);
            if ($ratingValue) {
                if ($this->shopArticleService->saveShopArticleRating($articleId, $ratingValue, $ratingText) > 0) {
                    $this->layout()->message = ['level' => 'success', 'text' => 'Danke für Ihre Meinung. Nach redaktioneller Prüfung ist Ihre Bewertung sichtbar.'];
                }
            }
        }

        return $viewModel;
    }

    /**
     *
     * @return ViewModel|Response
     */
    public function param2dLengthcutArticleAction()
    {
        $articleSefurl = $this->params('article_sefurl');
        if (empty($articleSefurl)) {
            return $this->redirect()->toRoute('shop_frontend_article_articles');
        }
        $articleData = $this->shopArticleService->getShopArticleBySefurl($articleSefurl, true);
        if (empty($articleData)) {
            return $this->redirect()->toRoute('shop_frontend_article_articles');
        }
        $viewModel = new ViewModel();
        $viewModel->setVariable('articleData', $articleData);

        $articleId = $articleData['shop_article_id'];

        $shopArticleParam2dItemsIdAssoc = $this->shopArticleParam2dService->getShopArticleParam2dItemsForArticleIdAssoc($articleId);
        $viewModel->setVariable('shopArticleParam2dItemsIdAssoc', $shopArticleParam2dItemsIdAssoc);

        $shopArticleImagesData = $this->shopArticleImageService->getShopArticleImages($articleId);
        $viewModel->setVariable('shopArticleImagesData', $shopArticleImagesData);
        $imageEntity = new ImagesEntity();
        $imageEntity->exchangeArrayImages($shopArticleImagesData);
        $viewModel->setVariable('imageEntity', $imageEntity);
        $viewModel->setVariable('shopName', $this->shopService->getShopConfigurationValue('shop_name'));

        $sidebarViewModel = new ViewModel();
        $sidebarViewModel->setTemplate('template/sidebar/article-harness');
        $sidebarViewModel->setVariable('shopArticleCategoryIdAssoc', $this->shopArticleService->getShopArticleCategoryIdAssoc(1));
        $sidebarViewModel->setVariable('routematch', $this->layout()->routematch);
        $sidebarViewModel->setVariable('routematchParams', $this->layout()->routematchParams);
        $this->layout()->sitebarLeft = $this->viewRender->render($sidebarViewModel);

        $shopArticleOptionsIdAssoc = $this->shopArticleOptionService->getShopArticleOptionItemArticleRelationsIdAssoc($articleId, true);
        if (!empty($shopArticleOptionsIdAssoc['pricediff'])) {
            $viewModel->setVariable('isPricediff', true);
        }
        unset($shopArticleOptionsIdAssoc['pricediff']);
        $viewModel->setVariable('shopArticleOptionsIdAssoc', $shopArticleOptionsIdAssoc);

        $optionItemId = (int)$this->params()->fromQuery('optionitemid');
        if (!empty($optionItemId)) {
            $viewModel->setVariable('optionItemIdPreselection', $optionItemId);
        }

        $request = $this->getRequest();
        if ($request->isPost() && $request instanceof Request) {
            $ratingValue = filter_input(INPUT_POST, 'shop_article_rating_value', FILTER_SANITIZE_NUMBER_INT);
            $ratingText = filter_input(INPUT_POST, 'shop_article_rating_text', FILTER_SANITIZE_STRING);
            if ($ratingValue) {
                if ($this->shopArticleService->saveShopArticleRating($articleId, $ratingValue, $ratingText) > 0) {
                    $this->layout()->message = ['level' => 'success', 'text' => 'Danke für Ihre Meinung. Nach redaktioneller Prüfung ist Ihre Bewertung sichtbar.'];
                }
            }
        }

        return $viewModel;
    }

}
