<?php

namespace Bitkorn\Shop\Controller\Frontend\Article\Size;

use Bitkorn\Shop\Service\Article\ShopArticleSizeService;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\Response;
use Laminas\View\Model\ViewModel;
use Bitkorn\Shop\Controller\AbstractShopController;

/**
 * @author allapow
 */
class ArticleSizeFrontendController extends AbstractShopController
{

    /**
     * @var ShopArticleSizeService
     */
    protected $shopArticleSizeService;

    /**
     * @param ShopArticleSizeService $shopArticleSizeService
     */
    public function setShopArticleSizeService(ShopArticleSizeService $shopArticleSizeService): void
    {
        $this->shopArticleSizeService = $shopArticleSizeService;
    }

    /**
     * Example implementation.
     * Please implement your own product-configurator.
     *
     * Shows one size-group with input-fields for user-sizes.
     * After submit the form-inputs, it shows acceptable items.
     * @return ViewModel|Response
     */
    public function articleSizeGroupProductConfigAction()
    {
        $viewModel = new ViewModel();
        $sizeGroupId = intval($this->params('size_group_id'));
        $sizeGroupData = $this->shopArticleSizeService->getShopArticleSizeGroupById($sizeGroupId);
        if (empty($sizeGroupId) || empty($sizeGroupData)) {
            return $this->redirect()->toRoute('shop_frontend_article_articles');
        }
        $viewModel->setVariable('sizeGroupData', $sizeGroupData);

        $sizePositionIdAssoc = $this->shopArticleSizeService->getShopArticleSizePositionIdAssocByArticleSizeGroupId($sizeGroupId);
        $viewModel->setVariable('sizePositionIdAssoc', $sizePositionIdAssoc);

        $request = $this->getRequest();
        if ($request->isPost() && $request instanceof Request) {
            $postData = $request->getPost()->toArray();
            if (!empty($postData['sizeposition']) && count($postData['sizeposition']) == count($sizePositionIdAssoc)) {
                $validSizePositions = true;
                $sizeDefs = [];
                foreach ($postData['sizeposition'] as $sizePositionId => $sizepositionValue) {
                    if (!array_key_exists($sizePositionId, $sizePositionIdAssoc) || empty($sizepositionValue)) {
                        $validSizePositions = false;
                        continue;
                    }
                    $viewModel->setVariable('spid_' . $sizePositionId, $sizepositionValue);
                    $sizeDefs[$sizePositionId] = $this->shopArticleSizeService->getShopArticleSizeDefBySizePositionIdAndSizeItemFromTo($sizePositionId,
                        $sizepositionValue, $sizepositionValue);
                }
                if (true || $validSizePositions) {
                    $viewModel->setVariable('sizeDefs', $sizeDefs);
                } else {
                    $this->layout()->message = ['level' => 'warn', 'text' => 'Es müssen alle Maße angegeben werden!'];
                }
            }
        }

        if (file_exists($this->config['bitkorn_shop']['image_path_absolute'] . '/sizegroup/sizegroupimage_' . $sizeGroupId . '_thumb.png')) {
            $viewModel->setVariable('sizegroupimage',
                $this->config['bitkorn_shop']['image_path_relative'] . '/sizegroup/sizegroupimage_' . $sizeGroupId . '_thumb.png');
        }

        return $viewModel;
    }

}
