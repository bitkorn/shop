<?php

namespace Bitkorn\Shop\Controller\Frontend\Basket;

use Bitkorn\Images\Entity\Image\ImagesEntity;
use Bitkorn\Mail\Mail\SimpleMailer;
use Bitkorn\Shop\Controller\AbstractShopController;
use Bitkorn\Shop\Entity\Basket\ShopBasketAddressEntity;
use Bitkorn\Shop\Entity\User\ShopUserAddressEntity;
use Bitkorn\Shop\Form\Basket\ShopBasketAddressForm;
use Bitkorn\Shop\Form\User\RegisterForm;
use Bitkorn\Shop\Form\User\ShopUserAddressForm;
use Bitkorn\Shop\Service\Article\ShopArticleService;
use Bitkorn\Shop\Service\Document\DocumentDeliveryService;
use Bitkorn\Shop\Service\Document\DocumentInvoiceService;
use Bitkorn\Shop\Service\Document\DocumentOrderService;
use Bitkorn\Shop\Service\Shipping\ShippingServiceInterface;
use Bitkorn\Shop\Service\ShopService;
use Bitkorn\Shop\Service\User\ShopUserService;
use Bitkorn\Shop\Table\Article\ShopArticleRatingTable;
use Bitkorn\Shop\Table\Basket\ShopBasketAddressTable;
use Bitkorn\Shop\Table\User\ShopUserAddressTable;
use Bitkorn\Trinket\View\Helper\Messages;
use Bitkorn\User\Service\Registration\UserRegistrationService;
use Bitkorn\User\Service\UserService;
use Laminas\EventManager\EventManagerAwareInterface;
use Laminas\Http\Header\SetCookie;
use Laminas\Http\PhpEnvironment\RemoteAddress;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\Response;
use Laminas\View\Model\ViewModel;

/**
 *
 * @author allapow
 */
class BasketController extends AbstractShopController implements EventManagerAwareInterface
{

    protected $messageWarn = ['level' => 'warn', 'text' => 'Es gab einen Fehler beim Speichern der Daten. Bitte überprüfen Sie Ihre Eingaben.'];
    protected $messageEmailUsernameExist = ['level' => 'warn', 'text' => 'Die Emailadresse oder der Username existieren bereits. Es kann kein neues Kundenkonto angelegt werden.'];

    /**
     * @var SimpleMailer
     */
    protected $simpleMailer;

    /**
     * @var ShopService
     */
    protected $shopService;

    /**
     * @var ShopUserService
     */
    protected $shopUserService;

    /**
     *
     * @var ShopBasketAddressForm
     */
    protected $shopBasketAddressFormInvoicePrivate;

    /**
     *
     * @var ShopBasketAddressForm
     */
    protected $shopBasketAddressFormInvoiceEnterprise;

    /**
     *
     * @var ShopBasketAddressForm
     */
    protected $shopBasketAddressFormShipmentPrivate;

    /**
     *
     * @var ShopBasketAddressForm
     */
    protected $shopBasketAddressFormShipmentEnterprise;

    /**
     *
     * @var ShopUserAddressForm
     */
    protected $shopUserAddressFormInvoicePrivate;

    /**
     *
     * @var ShopUserAddressForm
     */
    protected $shopUserAddressFormInvoiceEnterprise;

    /**
     *
     * @var ShopUserAddressForm
     */
    protected $shopUserAddressFormShipmentPrivate;

    /**
     *
     * @var ShopUserAddressForm
     */
    protected $shopUserAddressFormShipmentEnterprise;

    /**
     *
     * @var RegisterForm
     */
    protected $userRegisterForm;

    /**
     *
     * @var UserRegistrationService
     */
    protected $userRegistrationService;

    /**
     *
     * @var ShopArticleRatingTable
     */
    protected $shopArticleRatingTable;

    /**
     *
     * @var ShippingServiceInterface
     */
    protected $shippingService;

    /**
     * @var ShopUserAddressTable
     */
    protected $shopUserAddressTable;

    /**
     * @var ShopBasketAddressTable
     */
    protected $shopBasketAddressTable;

    /**
     * @var DocumentInvoiceService
     */
    protected $documentInvoiceService;

    /**
     * @var DocumentDeliveryService
     */
    protected $documentDeliveryService;

    /**
     * @var DocumentOrderService
     */
    protected $documentOrderService;

    /**
     * @var ShopArticleService
     */
    protected $shopArticleService;

    /**
     * @param SimpleMailer $simpleMailer
     */
    public function setSimpleMailer(SimpleMailer $simpleMailer): void
    {
        $this->simpleMailer = $simpleMailer;
    }

    /**
     * @param ShopService $shopService
     */
    public function setShopService(ShopService $shopService): void
    {
        $this->shopService = $shopService;
    }

    /**
     * @param ShopUserService $shopUserService
     */
    public function setShopUserService(ShopUserService $shopUserService): void
    {
        $this->shopUserService = $shopUserService;
    }

    /**
     * @param ShopBasketAddressForm $shopBasketAddressFormInvoicePrivate
     */
    public function setShopBasketAddressFormInvoicePrivate(ShopBasketAddressForm $shopBasketAddressFormInvoicePrivate): void
    {
        $this->shopBasketAddressFormInvoicePrivate = $shopBasketAddressFormInvoicePrivate;
    }

    /**
     * @param ShopBasketAddressForm $shopBasketAddressFormInvoiceEnterprise
     */
    public function setShopBasketAddressFormInvoiceEnterprise(ShopBasketAddressForm $shopBasketAddressFormInvoiceEnterprise): void
    {
        $this->shopBasketAddressFormInvoiceEnterprise = $shopBasketAddressFormInvoiceEnterprise;
    }

    /**
     * @param ShopBasketAddressForm $shopBasketAddressFormShipmentPrivate
     */
    public function setShopBasketAddressFormShipmentPrivate(ShopBasketAddressForm $shopBasketAddressFormShipmentPrivate): void
    {
        $this->shopBasketAddressFormShipmentPrivate = $shopBasketAddressFormShipmentPrivate;
    }

    /**
     * @param ShopBasketAddressForm $shopBasketAddressFormShipmentEnterprise
     */
    public function setShopBasketAddressFormShipmentEnterprise(ShopBasketAddressForm $shopBasketAddressFormShipmentEnterprise): void
    {
        $this->shopBasketAddressFormShipmentEnterprise = $shopBasketAddressFormShipmentEnterprise;
    }

    /**
     * @param ShopUserAddressForm $shopUserAddressFormInvoicePrivate
     */
    public function setShopUserAddressFormInvoicePrivate(ShopUserAddressForm $shopUserAddressFormInvoicePrivate): void
    {
        $this->shopUserAddressFormInvoicePrivate = $shopUserAddressFormInvoicePrivate;
    }

    /**
     * @param ShopUserAddressForm $shopUserAddressFormInvoiceEnterprise
     */
    public function setShopUserAddressFormInvoiceEnterprise(ShopUserAddressForm $shopUserAddressFormInvoiceEnterprise): void
    {
        $this->shopUserAddressFormInvoiceEnterprise = $shopUserAddressFormInvoiceEnterprise;
    }

    /**
     * @param ShopUserAddressForm $shopUserAddressFormShipmentPrivate
     */
    public function setShopUserAddressFormShipmentPrivate(ShopUserAddressForm $shopUserAddressFormShipmentPrivate): void
    {
        $this->shopUserAddressFormShipmentPrivate = $shopUserAddressFormShipmentPrivate;
    }

    /**
     * @param ShopUserAddressForm $shopUserAddressFormShipmentEnterprise
     */
    public function setShopUserAddressFormShipmentEnterprise(ShopUserAddressForm $shopUserAddressFormShipmentEnterprise): void
    {
        $this->shopUserAddressFormShipmentEnterprise = $shopUserAddressFormShipmentEnterprise;
    }

    /**
     * @param RegisterForm $userRegisterForm
     */
    public function setUserRegisterForm(RegisterForm $userRegisterForm): void
    {
        $this->userRegisterForm = $userRegisterForm;
    }

    /**
     * @param UserRegistrationService $userRegistrationService
     */
    public function setUserRegistrationService(UserRegistrationService $userRegistrationService): void
    {
        $this->userRegistrationService = $userRegistrationService;
    }

    /**
     *
     * @param ShopArticleRatingTable $shopArticleRatingTable
     */
    public function setShopArticleRatingTable(ShopArticleRatingTable $shopArticleRatingTable)
    {
        $this->shopArticleRatingTable = $shopArticleRatingTable;
    }

    /**
     *
     * @param ShippingServiceInterface $shippingService
     */
    public function setShippingService(ShippingServiceInterface $shippingService)
    {
        $this->shippingService = $shippingService;
    }

    /**
     * @param ShopUserAddressTable $shopUserAddressTable
     */
    public function setShopUserAddressTable(ShopUserAddressTable $shopUserAddressTable): void
    {
        $this->shopUserAddressTable = $shopUserAddressTable;
    }

    /**
     * @param ShopBasketAddressTable $shopBasketAddressTable
     */
    public function setShopBasketAddressTable(ShopBasketAddressTable $shopBasketAddressTable): void
    {
        $this->shopBasketAddressTable = $shopBasketAddressTable;
    }

    /**
     * @param DocumentInvoiceService $documentInvoiceService
     */
    public function setDocumentInvoiceService(DocumentInvoiceService $documentInvoiceService): void
    {
        $this->documentInvoiceService = $documentInvoiceService;
    }

    /**
     * @param DocumentDeliveryService $documentDeliveryService
     */
    public function setDocumentDeliveryService(DocumentDeliveryService $documentDeliveryService): void
    {
        $this->documentDeliveryService = $documentDeliveryService;
    }

    /**
     * @param DocumentOrderService $documentOrderService
     */
    public function setDocumentOrderService(DocumentOrderService $documentOrderService): void
    {
        $this->documentOrderService = $documentOrderService;
    }

    /**
     * @param ShopArticleService $shopArticleService
     */
    public function setShopArticleService(ShopArticleService $shopArticleService): void
    {
        $this->shopArticleService = $shopArticleService;
    }

    /**
     * Zeigt Warenkorb (editierbar)
     * und Button "Zur Kasse"
     *
     * @return ViewModel|Response
     */
    public function basketAction()
    {
        $request = $this->getRequest();
        if ($request->isGet()) {
            $this->basketService->refreshBasket();
            $this->basketService->deleteGuestPasswd();
        }
        $viewModel = new ViewModel();
        $basketDataItemIds = [];
        $basketDiscountEnabled = false;
        if (($xshopBasketEntity = $this->basketService->checkBasket()) !== null && !empty($xshopBasketEntity->getStorageItems())) {
            $basketDiscountEnabled = $this->basketService->isBasketDiscountEnabled();

            $basketDataItemIds = $xshopBasketEntity->getStorageItemsItemIds();
            $imageEntity = new ImagesEntity();
            $imageEntity->exchangeArrayImages($xshopBasketEntity->getImagesData());
            $viewModel->setVariables([
                'xshopBasketEntity' => $xshopBasketEntity,
                'shopBasketData' => $xshopBasketEntity->getStorageItems(),
                'imagesArticleIdAssoc' => $xshopBasketEntity->getImagesArticleIdAssoc(),
                'imageEntity' => $imageEntity,
                'articleAmountSum' => $xshopBasketEntity->getArticleAmountSum(),
                'articlePriceTotalSum' => $xshopBasketEntity->getArticlePriceTotalSum(),
                'articlePriceTotalSumEnd' => $xshopBasketEntity->getArticlePriceTotalSumEnd(),
                'basketDiscountHash' => $xshopBasketEntity->getBasketDiscountHash(),
                'basketDiscountValue' => $xshopBasketEntity->getBasketDiscountValue(),
                'articleShippingCostsComputed' => $xshopBasketEntity->getArticleShippingCostsComputed(),
                'basketTaxTotalSumEnd' => $xshopBasketEntity->getBasketTaxTotalSumEnd(),
            ]);
        }

        if ($request->isPost() && $request instanceof Request) {
            $postData = $request->getPost()->toArray();
            $refreshBasket = false;
            if (isset($postData['basket_item']) && is_array($postData['basket_item'])) {
                /*
                 * edit amount
                 */
                foreach ($postData['basket_item'] as $basketItemId => $basketItemAmount) {
                    if (!in_array($basketItemId, $basketDataItemIds)) {
                        $remote = new RemoteAddress();
                        $this->logger->err('HACKER; !in_array($basketItemId, $basketDataItemIds); ' . $remote->getIpAddress());
                        continue;
                    }
                    $this->basketService->updateShopBasketItemAmount($basketItemId, floatval($basketItemAmount));
                    $refreshBasket = true;
                }
                if ($refreshBasket) {
                    $this->basketService->refreshBasket();
                }
                return $this->redirect()->toRoute('shop_frontend_basket_basket');
            }
            if (!empty($postData['shop_basket_discount_hash'])) {
                if (!$this->basketService->computeBasketDiscount(filter_input(INPUT_POST, 'shop_basket_discount_hash', FILTER_SANITIZE_STRING))) {
                    $this->layout()->message = ['level' => 'info', 'text' => $this->basketService->getMessage()];
                } else {
                    return $this->redirect()->toRoute('shop_frontend_basket_basket');
                }
            }
        }

        $viewModel->setVariable('basketDiscountEnabled', $basketDiscountEnabled);

        return $viewModel;
    }

    /**
     * Hier wird alles abgehandelt (registriert oder nicht, private oder Firma, invoice & shipment)
     *
     * @return ViewModel|Response
     */
    public function chooseAddressAction()
    {
        $basketData = $this->basketService->getShopBasketByStatus(); // $basketStatus == 'basket'
        if (empty($basketData)) {
            return $this->redirect()->toRoute('shop_frontend_article_articles');
        }
        $viewModel = new ViewModel();

        $isUser = $this->userService->checkUserRoleAccessMin(5);
        $viewModel->setVariable('isUser', $isUser);
        $buyWithoutAccount = false;
        /*
         * Cookie for: buy Without Account
         * and set $buyWithoutAccount to TRUE
         */
        if (!$isUser) {
            if ($this->shopService->getShopConfigurationValue('enable_buy_without_account') == 1) {
                $viewModel->setVariable('buyWithoutAccountEnabled', 1);
                $asGuestParam = $this->params()->fromQuery('asg', '');
                if ($asGuestParam == 1 || filter_input(INPUT_COOKIE, 'buyWithoutAccount') == '1') {
                    $buyWithoutAccount = true;
                    setcookie('buyWithoutAccount', 1, time() + 60 * 60 * 24, '/');
                } else {
                    setcookie('buyWithoutAccount', 0, time() + 60 * 60 * 24, '/');
                }
                $viewModel->setVariable('buyWithoutAccount', $buyWithoutAccount);
            }
        }

        /*
         * Customer Type Invoice
         * initialze & set $isPrivateInvoice
         * Cookie for: customer type invoice
         */
        $customerTypeInvoiceParam = $this->params()->fromQuery('cti', '');
        if ($customerTypeInvoiceParam == 'private') {
            $isPrivateInvoice = true;
            setcookie('customerTypeInvoice', 'private', time() + 60 * 60 * 24, '/');
        } else if ($customerTypeInvoiceParam == 'enterprise') {
            $isPrivateInvoice = false;
            setcookie('customerTypeInvoice', 'enterprise', time() + 60 * 60 * 24, '/');
        } else {
            $customerTypeInvoiceCookie = filter_input(INPUT_COOKIE, 'customerTypeInvoice');
            if (isset($customerTypeInvoiceCookie) && $customerTypeInvoiceCookie == 'private') {
                $isPrivateInvoice = true;
            } else if (isset($customerTypeInvoiceCookie) && $customerTypeInvoiceCookie == 'enterprise') {
                $isPrivateInvoice = false;
            } else {
                $isPrivateInvoice = true;
            }
        }

        /*
         * view variable: show or hide variant shipping address
         */
        $showVariantShippingAddress = false;

        /*
         * Customer Type Shipment
         * initialze & set $isPrivateShipment
         * Cookie for: customer type shipment
         */
        $customerTypeShipmentParam = $this->params()->fromQuery('cts');
        if (!empty($customerTypeShipmentParam)) {
            $showVariantShippingAddress = true;
        }
        if (isset($customerTypeShipmentParam) && $customerTypeShipmentParam == 'private') {
            $isPrivateShipment = true;
            setcookie('customerTypeShipment', 'private', time() + 60 * 60 * 24, '/');
        } else if (isset($customerTypeShipmentParam) && $customerTypeShipmentParam == 'enterprise') {
            $isPrivateShipment = false;
            setcookie('customerTypeShipment', 'enterprise', time() + 60 * 60 * 24, '/');
        } else {
            $customerTypeShipmentCookie = filter_input(INPUT_COOKIE, 'customerTypeShipment');
            if (isset($customerTypeShipmentCookie) && $customerTypeShipmentCookie == 'private') {
                $isPrivateShipment = true;
            } else if (isset($customerTypeShipmentCookie) && $customerTypeShipmentCookie == 'enterprise') {
                $isPrivateShipment = false;
            } else {
                $isPrivateShipment = true;
            }
        }

        $userAddressCount = 0;
        $basketAddressCount = 0;
        if ($isUser) {
            $this->basketService->setBasketUser($this->userService->getUserUuid());
            $this->basketService->deleteShopBasketAddresses();
            $userAddresses = $this->shopUserService->getShopUserAddressesByUserId($this->userService->getUserUuid(), true);
            $viewModel->setVariable('userEmail', $this->userService->getUserEntity()->getUserEmail());
            $userAddressCount = count($userAddresses);
            if ($userAddressCount >= 1) {
                $userAddresses['invoice']['shop_user_address_birthday'] = $this->toDatestringDe($userAddresses['invoice'], 'shop_user_address_birthday');
            }
            if ($userAddressCount == 2) {
                $userAddresses['shipment']['shop_user_address_birthday'] = $this->toDatestringDe($userAddresses['shipment'], 'shop_user_address_birthday');
            }
            if ($isPrivateInvoice) {
                $viewModel->setVariable('shopUserAddressFormInvoicePrivate', $this->shopUserAddressFormInvoicePrivate);
                if ($userAddressCount == 0) {
                    $this->shopUserAddressFormInvoicePrivate->setShopUserAddressIdRequired(false);
                    $this->shopUserAddressFormInvoicePrivate->setUserId($this->userService->getUserUuid());
                    $this->shopUserAddressFormInvoicePrivate->init();
                }
                if ($userAddressCount >= 1) {
                    unset($userAddresses['invoice']['shop_user_address_customer_type']);
                    $this->shopUserAddressFormInvoicePrivate->setData($userAddresses['invoice']);
                }
            } else {
                if ($userAddressCount == 0) {
                    $this->shopUserAddressFormInvoiceEnterprise->setShopUserAddressIdRequired(false);
                    $this->shopUserAddressFormInvoiceEnterprise->setUserId($this->userService->getUserUuid());
                    $this->shopUserAddressFormInvoiceEnterprise->init();
                }
                if ($userAddressCount >= 1) {
                    unset($userAddresses['invoice']['shop_user_address_customer_type']);
                    $this->shopUserAddressFormInvoiceEnterprise->setData($userAddresses['invoice']);
                }
                $viewModel->setVariable('shopUserAddressFormInvoiceEnterprise', $this->shopUserAddressFormInvoiceEnterprise);
            }
            if ($isPrivateShipment) {
                $viewModel->setVariable('shopUserAddressFormShipmentPrivate', $this->shopUserAddressFormShipmentPrivate);
                if ($userAddressCount == 1) {
                    $this->shopUserAddressFormShipmentPrivate->setShopUserAddressIdRequired(false);
                    $this->shopUserAddressFormShipmentPrivate->setUserId($this->userService->getUserUuid());
                    $this->shopUserAddressFormShipmentPrivate->init();
                }
                if ($userAddressCount == 2) {
                    $this->shopUserAddressFormShipmentPrivate->setData($userAddresses['shipment']);
                }
            } else {
                if ($userAddressCount == 1) {
                    $this->shopUserAddressFormShipmentEnterprise->setShopUserAddressIdRequired(false);
                    $this->shopUserAddressFormShipmentEnterprise->setUserId($this->userService->getUserUuid());
                    $this->shopUserAddressFormShipmentEnterprise->init();
                }
                if ($userAddressCount == 2) {
                    $this->shopUserAddressFormShipmentEnterprise->setData($userAddresses['shipment']);
                }
                $viewModel->setVariable('shopUserAddressFormShipmentEnterprise', $this->shopUserAddressFormShipmentEnterprise);
            }
        } else { // anonymous
            $this->basketService->setBasketUser();
            $basketAddresses = $this->basketService->getShopBasketAddressesByBasketUnique();
            $basketAddressCount = count($basketAddresses);
            if ($basketAddressCount > 0) {
                unset($basketAddresses['invoice']['shop_basket_address_customer_type']); // wird bei Erstellung der Form (Factory) gesetzt
                $basketAddresses['invoice']['shop_basket_address_birthday'] = $this->toDatestringDe($basketAddresses['invoice'], 'shop_basket_address_birthday');
            }
            if ($basketAddressCount == 2) {
                $basketAddresses['shipment']['shop_basket_address_birthday'] = $this->toDatestringDe($basketAddresses['shipment'], 'shop_basket_address_birthday');
            }
            if ($isPrivateInvoice) {
                $viewModel->setVariable('shopBasketAddressFormInvoicePrivate', $this->shopBasketAddressFormInvoicePrivate);
                if ($basketAddressCount == 0) {
                    $this->shopBasketAddressFormInvoicePrivate->setShopBasketAddressIdRequired(false);
                    $this->shopBasketAddressFormInvoicePrivate->init();
                    $this->shopBasketAddressFormInvoicePrivate->setData(['shop_basket_unique' => $this->basketService->getBasketUnique()]);
                }
                if ($basketAddressCount >= 1) {
                    $this->shopBasketAddressFormInvoicePrivate->setData($basketAddresses['invoice']);
                }
            } else {
                $viewModel->setVariable('shopBasketAddressFormInvoiceEnterprise', $this->shopBasketAddressFormInvoiceEnterprise);
                if ($basketAddressCount == 0) {
                    $this->shopBasketAddressFormInvoiceEnterprise->setShopBasketAddressIdRequired(false);
                    $this->shopBasketAddressFormInvoiceEnterprise->init();
                    $this->shopBasketAddressFormInvoiceEnterprise->setData(['shop_basket_unique' => $this->basketService->getBasketUnique()]);
                }
                if ($basketAddressCount >= 1) {
                    $this->shopBasketAddressFormInvoiceEnterprise->setData($basketAddresses['invoice']);
                }
            }
            if ($isPrivateShipment) {
                $viewModel->setVariable('shopBasketAddressFormShipmentPrivate', $this->shopBasketAddressFormShipmentPrivate);
                if ($basketAddressCount == 1) {
                    $this->shopBasketAddressFormShipmentPrivate->setShopBasketAddressIdRequired(false);
                    $this->shopBasketAddressFormShipmentPrivate->init();
                    $this->shopBasketAddressFormShipmentPrivate->setData(['shop_basket_unique' => $this->basketService->getBasketUnique()]);
                }
                if ($basketAddressCount == 2) {
                    $this->shopBasketAddressFormShipmentPrivate->setData($basketAddresses['shipment']);
                }
            } else {
                $viewModel->setVariable('shopBasketAddressFormShipmentEnterprise', $this->shopBasketAddressFormShipmentEnterprise);
                if ($basketAddressCount == 1) {
                    $this->shopBasketAddressFormShipmentEnterprise->setShopBasketAddressIdRequired(false);
                    $this->shopBasketAddressFormShipmentEnterprise->init();
                    $this->shopBasketAddressFormShipmentEnterprise->setData(['shop_basket_unique' => $this->basketService->getBasketUnique()]);
                }
                if ($basketAddressCount == 2) {
                    $this->shopBasketAddressFormShipmentEnterprise->setData($basketAddresses['shipment']);
                }
            }

            if (!$buyWithoutAccount) {
                $viewModel->setVariable('userRegisterForm', $this->userRegisterForm);
            }
        }

        $request = $this->getRequest();
        if ($request->isPost() && $request instanceof Request && $request->getPost('further_to_shippingmethod', 0) == 0) {
            $postData = $request->getPost()->toArray();
            if (!isset($postData['form_name'])) {
                throw new \RuntimeException('You are script_kiddie');
            }
            if ($isUser) {
                $this->basketService->setGuestPasswd('');
                $postData['shop_user_address_email'] = $this->userService->getUserEntity()->getUserEmail();
                if ($postData['form_name'] == 'shop_user_address_invoice_private') {
                    $this->shopUserAddressFormInvoicePrivate->setData($postData);
                    $this->shopUserAddressFormInvoicePrivate->switchDate('integer');
                    if ($this->shopUserAddressFormInvoicePrivate->isValid()) {
                        $shopUserAdressEntity = new ShopUserAddressEntity();
                        $shopUserAdressEntity->exchangeArray($this->shopUserAddressFormInvoicePrivate->getData());
                        if ($shopUserAdressEntity->isUserOwner($this->userService->getUserUuid())) {
                            if ($shopUserAdressEntity->saveOrUpdate($this->shopUserAddressTable, $userAddressCount) >= 0) {
                                $userAddresses = $this->shopUserService->getShopUserAddressesByUserId($this->userService->getUserUuid(), true);
                                $this->shopUserAddressFormInvoicePrivate->setData($userAddresses['invoice']);
                                $userAddressCount = count($userAddresses);
                                $this->layout()->message = Messages::$messageSuccessSaveData;
                            } else {
                                $this->layout()->message = $this->messageWarn;
                            }
                        }
                    }
                    $this->shopUserAddressFormInvoicePrivate->switchDate('string');
                } else if ($postData['form_name'] == 'shop_user_address_invoice_enterprise') {
                    $this->shopUserAddressFormInvoiceEnterprise->setData($postData);
                    if ($this->shopUserAddressFormInvoiceEnterprise->isValid()) {
                        $shopUserAdressEntity = new ShopUserAddressEntity();
                        $shopUserAdressEntity->exchangeArray($this->shopUserAddressFormInvoiceEnterprise->getData());
                        if ($shopUserAdressEntity->isUserOwner($this->userService->getUserUuid())) {
                            if ($shopUserAdressEntity->saveOrUpdate($this->shopUserAddressTable, $userAddressCount) >= 0) {
                                $userAddresses = $this->shopUserService->getShopUserAddressesByUserId($this->userService->getUserUuid(), true);
                                $this->shopUserAddressFormInvoiceEnterprise->setData($userAddresses['invoice']);
                                $userAddressCount = count($userAddresses);
                                $this->layout()->message = Messages::$messageSuccessSaveData;
                            } else {
                                $this->layout()->message = $this->messageWarn;
                            }
                        }
                    }
                } else if ($postData['form_name'] == 'shop_user_address_shipment_private') {
                    if (!empty($postData['submit_delete']) && !empty($postData['shop_user_address_id']) && $postData['user_uuid'] == $this->userService->getUserUuid()) {
                        if ($this->shopUserService->deleteShopUserAddress(['shop_user_address_id' => $postData['shop_user_address_id']]) > 0) {
                            $this->shopUserAddressFormShipmentPrivate->setData([]);
                            $this->shopUserAddressFormShipmentPrivate->init();
                            $userAddressCount = 1;
                            $showVariantShippingAddress = false;
                            $this->layout()->message = Messages::$messageSuccessSaveData;
                        } else {
                            $this->layout()->message = $this->messageWarn;
                        }
                    } else if (!empty($postData['submit_delete'])) {
                        $showVariantShippingAddress = false;
                    } else if (empty($postData['submit_delete'])) {
                        $this->shopUserAddressFormShipmentPrivate->setData($postData);
                        $showVariantShippingAddress = true;
                        if ($this->shopUserAddressFormShipmentPrivate->isValid()) {
                            $shopUserAdressEntity = new ShopUserAddressEntity();
                            $shopUserAdressEntity->exchangeArray($this->shopUserAddressFormShipmentPrivate->getData());
                            if ($shopUserAdressEntity->isUserOwner($this->userService->getUserUuid())) {
                                if ($shopUserAdressEntity->saveOrUpdate($this->shopUserAddressTable, $userAddressCount) >= 0) {
                                    $userAddresses = $this->shopUserService->getShopUserAddressesByUserId($this->userService->getUserUuid(), true);
                                    $this->shopUserAddressFormShipmentPrivate->setData($userAddresses['shipment']);
                                    $userAddressCount = count($userAddresses);
                                    $this->layout()->message = Messages::$messageSuccessSaveData;
                                } else {
                                    $this->layout()->message = $this->messageWarn;
                                }
                            }
                        }
                    }
                } else if ($postData['form_name'] == 'shop_user_address_shipment_enterprise') {
                    if (!empty($postData['submit_delete']) && !empty($postData['shop_user_address_id']) && $postData['user_uuid'] == $this->userService->getUserUuid()) {
                        if ($this->shopUserService->deleteShopUserAddress(['shop_user_address_id' => $postData['shop_user_address_id']]) > 0) {
                            $this->shopUserAddressFormShipmentEnterprise->setData([]);
                            $this->shopUserAddressFormShipmentEnterprise->init();
                            $userAddressCount = 1;
                            $showVariantShippingAddress = false;
                            $this->layout()->message = Messages::$messageSuccessSaveData;
                        } else {
                            $this->layout()->message = $this->messageWarn;
                        }
                    } else if (!empty($postData['submit_delete'])) {
                        $showVariantShippingAddress = false;
                    } else if (empty($postData['submit_delete'])) {
                        $this->shopUserAddressFormShipmentEnterprise->setData($postData);
                        $showVariantShippingAddress = true;
                        if ($this->shopUserAddressFormShipmentEnterprise->isValid()) {
                            $shopUserAdressEntity = new ShopUserAddressEntity();
                            $shopUserAdressEntity->exchangeArray($this->shopUserAddressFormShipmentEnterprise->getData());
                            if ($shopUserAdressEntity->isUserOwner($this->userService->getUserUuid())) {
                                if ($shopUserAdressEntity->saveOrUpdate($this->shopUserAddressTable, $userAddressCount) >= 0) {
                                    $userAddresses = $this->shopUserService->getShopUserAddressesByUserId($this->userService->getUserUuid(), true);
                                    $this->shopUserAddressFormShipmentEnterprise->setData($userAddresses['shipment']);
                                    $userAddressCount = count($userAddresses);
                                    $this->layout()->message = Messages::$messageSuccessSaveData;
                                } else {
                                    $this->layout()->message = $this->messageWarn;
                                }
                            }
                        }
                    }
                }
            }

            /*
             * OHNE USER
             */
            if (!$isUser) {
                $userRegisterFormValid = true;
                if (!$buyWithoutAccount && empty($this->basketService->getGuestPasswd())) {
                    $this->userRegisterForm->setData($postData);
                    if (!$this->userRegisterForm->isValid()) {
                        $userRegisterFormValid = false;
                        $this->basketService->setGuestPasswd('');
                        $this->layout()->message = $this->messageWarn;
                    } else {
                        $this->basketService->setGuestPasswd($postData['passwd']);
                    }
                }
                if ($postData['form_name'] == 'shop_basket_address_invoice_private') {
                    $this->shopBasketAddressFormInvoicePrivate->setData($postData);
                    $this->shopBasketAddressFormInvoicePrivate->switchDate('integer');
                    if ($this->shopBasketAddressFormInvoicePrivate->isValid() && $userRegisterFormValid) {
                        $shopBasketAdressEntity = new ShopBasketAddressEntity();
                        $shopBasketAdressEntity->exchangeArray($this->shopBasketAddressFormInvoicePrivate->getData());
                        if ($shopBasketAdressEntity->isBasketUniqueValid($this->basketService->getBasketUnique())) {
                            if ($shopBasketAdressEntity->saveOrUpdate($this->shopBasketAddressTable, $basketAddressCount) >= 0) {
                                $basketAddresses = $this->basketService->getShopBasketAddressesByBasketUnique();
                                $this->shopBasketAddressFormInvoicePrivate->setData($basketAddresses['invoice']);
                                $basketAddressCount = count($basketAddresses);
                                $this->layout()->message = Messages::$messageSuccessSaveData;
                            } else {
                                $this->layout()->message = $this->messageWarn;
                            }
                        }
                    }
                    $this->shopBasketAddressFormInvoicePrivate->switchDate('string');
                } else if ($postData['form_name'] == 'shop_basket_address_invoice_enterprise') {
                    $this->shopBasketAddressFormInvoiceEnterprise->setData($postData);
                    if ($this->shopBasketAddressFormInvoiceEnterprise->isValid() && $userRegisterFormValid) {
                        $shopBasketAdressEntity = new ShopBasketAddressEntity();
                        $shopBasketAdressEntity->exchangeArray($this->shopBasketAddressFormInvoiceEnterprise->getData());
                        if ($shopBasketAdressEntity->isBasketUniqueValid($this->basketService->getBasketUnique())) {
                            if ($shopBasketAdressEntity->saveOrUpdate($this->shopBasketAddressTable, $basketAddressCount) >= 0) {
                                $basketAddresses = $this->basketService->getShopBasketAddressesByBasketUnique();
                                $this->shopBasketAddressFormInvoiceEnterprise->setData($basketAddresses['invoice']);
                                $basketAddressCount = count($basketAddresses);
                                $this->layout()->message = Messages::$messageSuccessSaveData;
                            } else {
                                $this->layout()->message = $this->messageWarn;
                            }
                        }
                    }
                } else if ($postData['form_name'] == 'shop_basket_address_shipment_private') {
                    if (
                        !empty($postData['submit_delete'])
                        && !empty($postData['shop_basket_address_id'])
                        && !empty($postData['shop_basket_unique'])
                        && $postData['shop_basket_unique'] == $this->basketService->getBasketUnique()
                    ) {
                        if ($this->basketService->deleteShopBasketAddress([
                                'shop_basket_address_id' => (int)$postData['shop_basket_address_id'],
                                'shop_basket_unique' => $postData['shop_basket_unique']
                            ]) > 0) {
                            $this->shopBasketAddressFormShipmentPrivate->setData([]);
                            $this->shopBasketAddressFormShipmentPrivate->init();
                            $basketAddressCount = 1;
                            $showVariantShippingAddress = false;
                            $this->layout()->message = Messages::$messageSuccessSaveData;
                        } else {
                            $this->layout()->message = $this->messageWarn;
                        }
                    } else if (!empty($postData['submit_delete'])) {
                        $showVariantShippingAddress = false;
                    } else if (empty($postData['submit_delete'])) {
                        $this->shopBasketAddressFormShipmentPrivate->setData($postData);
                        $showVariantShippingAddress = true;
                        if ($this->shopBasketAddressFormShipmentPrivate->isValid()) {
                            $shopBasketAdressEntity = new ShopBasketAddressEntity();
                            $shopBasketAdressEntity->exchangeArray($this->shopBasketAddressFormShipmentPrivate->getData());
                            if ($shopBasketAdressEntity->isBasketUniqueValid($this->basketService->getBasketUnique())) {
                                if ($shopBasketAdressEntity->saveOrUpdate($this->shopBasketAddressTable, $basketAddressCount) >= 0) {
                                    $basketAddresses = $this->basketService->getShopBasketAddressesByBasketUnique();
                                    $this->shopBasketAddressFormShipmentPrivate->setData($basketAddresses['shipment']);
                                    $basketAddressCount = count($basketAddresses);
                                    $this->layout()->message = Messages::$messageSuccessSaveData;
                                } else {
                                    $this->layout()->message = $this->messageWarn;
                                }
                            }
                        }
                    }
                } else if ($postData['form_name'] == 'shop_basket_address_shipment_enterprise') {
                    if (
                        !empty($postData['submit_delete'])
                        && !empty($postData['shop_basket_address_id'])
                        && !empty($postData['shop_basket_unique'])
                        && $postData['shop_basket_unique'] == $this->basketService->getBasketUnique()
                    ) {
                        if ($this->basketService->deleteShopBasketAddress(['shop_basket_address_id' => (int)$postData['shop_basket_address_id'],
                                'shop_basket_unique' => $postData['shop_basket_unique']]) > 0) {
                            $this->shopBasketAddressFormShipmentEnterprise->setData([]);
                            $this->shopBasketAddressFormShipmentEnterprise->init();
                            $basketAddressCount = 1;
                            $showVariantShippingAddress = false;
                            $this->layout()->message = Messages::$messageSuccessSaveData;
                        } else {
                            $this->layout()->message = $this->messageWarn;
                        }
                    } else if (!empty($postData['submit_delete'])) {
                        $showVariantShippingAddress = false;
                    } else if (empty($postData['submit_delete'])) {
                        $this->shopBasketAddressFormShipmentEnterprise->setData($postData);
                        $showVariantShippingAddress = true;
                        if ($this->shopBasketAddressFormShipmentEnterprise->isValid()) {
                            $shopBasketAdressEntity = new ShopBasketAddressEntity();
                            $shopBasketAdressEntity->exchangeArray($this->shopBasketAddressFormShipmentEnterprise->getData());
                            if ($shopBasketAdressEntity->isBasketUniqueValid($this->basketService->getBasketUnique())) {
                                if ($shopBasketAdressEntity->saveOrUpdate($this->shopBasketAddressTable, $basketAddressCount) >= 0) {
                                    $basketAddresses = $this->basketService->getShopBasketAddressesByBasketUnique();
                                    $this->shopBasketAddressFormShipmentEnterprise->setData($basketAddresses['invoice']);
                                    $basketAddressCount = count($basketAddresses);
                                    $this->layout()->message = Messages::$messageSuccessSaveData;
                                } else {
                                    $this->layout()->message = $this->messageWarn;
                                }
                            }
                        }
                    }
                }
            }
        }

        if (!empty($this->basketService->getGuestPasswd())) {
            $viewModel->setVariable('existPasswd', true);
        } else {
            $viewModel->setVariable('existPasswd', false);
        }

        $viewModel->setVariable('isPrivateInvoice', $isPrivateInvoice);
        $viewModel->setVariable('isPrivateShipment', $isPrivateShipment);

        $canFurther = false;
        if ($userAddressCount >= 1 || ($basketAddressCount >= 1 && ($buyWithoutAccount || !empty($this->basketService->getGuestPasswd())))) {
            $canFurther = true;
        }

        if ($canFurther && $request->isPost() && $request instanceof Request && $request->getPost('further_to_shippingmethod') == 1) {
            if ($request->getPost('data_protection_regulations') != 1) {
                // Datenschutzbestimmungen
                $this->layout()->message = Messages::$messageWarnPrivacyProtec;
            } else {
                if (!$buyWithoutAccount && !empty($this->basketService->getGuestPasswd()) &&
                    ($this->userService->existUseremail($basketAddresses['invoice']['shop_basket_address_email']) || $this->userService->existUserLogin($basketAddresses['invoice']['shop_basket_address_email']))) {
                    // email exist
                    $this->layout()->message = $this->messageEmailUsernameExist;
                } else if (!$buyWithoutAccount && !empty($this->basketService->getGuestPasswd())) {
                    // Account soll erstellt werden & es gibt schon das Passwort
                    $userData = [
                        'user_role_id' => 5,
                        'user_login' => $basketAddresses['invoice']['shop_basket_address_email'],
                        'user_passwd' => $this->basketService->getGuestPasswd(),
                        'user_passwd_confirm' => $this->basketService->getGuestPasswd(),
                        'user_active' => 0,
                        'user_email' => $basketAddresses['invoice']['shop_basket_address_email'],
                        'user_lang_iso' => 'de'
                    ];

                    if (!$this->userRegistrationService->registerUser($userData)) {
                        return $viewModel;
                    }
                    $userDataNew = $this->userService->getUserByLogin($userData['user_login']);
                    $this->basketService->setBasketUser($userDataNew['user_uuid']);
                    if (empty($userDataNew['user_uuid']) || !$this->basketService->moveShopBasketAddressesToShopUserAddresses($userDataNew['user_uuid'])) {
                        $errorMessage = 'Fehler bei moveShopBasketAddressesToShopUserAddresses(); $basketUnique: ' . $this->basketService->getBasketUnique() . '; $newUserUuid: ' . $userDataNew['user_uuid'];
                        $this->logger->debug($errorMessage);
                        $this->simpleMailer->sendAdminEmail($errorMessage);
                        $this->layout()->message = $this->messageWarn;
                    } else {
                        /**
                         * EVENT_AFTER_CREATE_USER
                         */
                        $this->userService->getEvents()->trigger(UserService::EVENT_AFTER_CREATE_USER, UserService::class, $userDataNew);
                        /**
                         * UserEntity erstellen und in die Session schreiben
                         */
                        if ($this->userService->loadWithLoginAndPassword($userDataNew['user_login'], $this->basketService->getGuestPasswd(), false)) {
                            $cookie = new SetCookie(UserService::SESSION_KEY, $this->userService->getUserSessionHash(), time() + $this->userService->getSessionLifetime(), '/');
                            $this->getResponse()->getHeaders()->addHeader($cookie);
                        }
                        $this->basketService->deleteGuestPasswd();
                        return $this->redirect()->toRoute('shop_frontend_basket_chooseshipping');
                    }
                } else if ($isUser || $buyWithoutAccount) {
                    return $this->redirect()->toRoute('shop_frontend_basket_chooseshipping');
                }
            }
        }

        $viewModel->setVariable('userAddressCount', $userAddressCount);
        $viewModel->setVariable('basketAddressCount', $basketAddressCount);
        $viewModel->setVariable('showVariantShippingAddress', $showVariantShippingAddress);
        $viewModel->setVariable('canFurther', $canFurther);

        return $viewModel;
    }

    /**
     * Choose Shipping
     * route: shop_frontend_basket_chooseshipping
     *
     * @return ViewModel|Response
     */
    public function chooseShippingAction()
    {
        if (empty($xshopBasketEntity = $this->basketService->checkBasket()) || empty($xshopBasketEntity->getStorageItems())) {
            return $this->redirect()->toRoute('shop_frontend_article_articles');
        }
        $viewModel = new ViewModel();
        $viewModel->setVariable('userUuid', '');
        if ($this->userService->checkUserRoleAccessMin(5)) {
            $viewModel->setVariable('userUuid', $this->userService->getUserUuid());
            $userAddressCount = $this->shopUserService->countShopUserAddress($this->userService->getUserUuid());
            if ($userAddressCount < 1) {
                return $this->redirect()->toRoute('shop_frontend_basket_chooseaddress');
            }
        } else {
            $shopBasketAddressCount = $this->basketService->countShopBasketAddresses();
            if ($shopBasketAddressCount < 1) {
                return $this->redirect()->toRoute('shop_frontend_basket_chooseaddress');
            }
        }

        $request = $this->getRequest();
        if ($request->isPost() && $request instanceof Request) {
            $postData = $request->getPost()->toArray();
            if (!empty($postData['shipping_provider'])) {
                if ($this->basketService->setShippingProviderUniqueDescriptor(filter_input(INPUT_POST, 'shipping_provider', FILTER_SANITIZE_STRING)) < 0) {
                    // scriptkiddie
                    return $viewModel;
                }
                return $this->redirect()->toRoute('shop_frontend_basket_choosepayment');
            }
            if (!empty($postData['shipping_provider_delete'])) {
                $this->basketService->deleteShippingProviderUniqueDescriptor();
            }
            if (!empty($postData['further_to_payment'])) {
                /**
                 * @todo irgend etwas persistieren?!?!?!
                 */
                return $this->redirect()->toRoute('shop_frontend_basket_choosepayment');
            }
        }
        $this->basketService->refreshBasket();
        $viewModel->setVariable('basketunique', $this->basketService->getBasketUnique());
        if (empty($shippingProvider = $this->shippingService->getShippingProvider())) {
            $viewModel->setVariable('shippingProviders', $this->shippingService->getShippingProvidersInstances());
        } else {
            $viewModel->setVariable('shippingProvider', $shippingProvider);
        }
        $viewModel->setVariable('numberFormatService', $this->numberFormatService);

        return $viewModel;
    }

    /**
     * Choose Payment
     * route: shop_frontend_basket_choosepayment
     *
     * @return ViewModel|Response
     */
    public function choosePaymentAction()
    {
        if (empty($xshopBasketEntity = $this->basketService->checkBasket()) || empty($xshopBasketEntity->getStorageItems())) {
            return $this->redirect()->toRoute('shop_frontend_article_articles');
        }
        $viewModel = new ViewModel();
        $viewModel->setVariable('userUuid', 0);

        if ($this->userService->checkUserRoleAccessMin(5)) {
            $viewModel->setVariable('userUuid', $this->userService->getUserUuid());
        }

        $basketData = $this->basketService->getShopBasketByStatus(); // $basketStatus == 'basket'
        if (empty($basketData)) {
            return $this->redirect()->toRoute('shop_frontend_article_articles');
        }
        if (empty($basketData[0]['shipping_provider_unique_descriptor'])) {
            return $this->redirect()->toRoute('shop_frontend_basket_chooseshipping');
        }

        $this->basketService->refreshBasket();
        $viewModel->setVariable('basketunique', $this->basketService->getBasketUnique());
        $viewModel->setVariable('shippingProvider', $this->shippingService->getShippingProvider());
        return $viewModel;
    }

    /**
     * Zeigt den Einkaufswagen mit Bezahlstatus.
     * Dazu muss an die URL der basketUnique angehangen sein.
     * http://example.com/einkauf-status/3078b1f6d5d42d65142e0b9a80178eeecfb549276f746c67248682dfaa421504
     *
     * @return ViewModel|Response
     */
    public function basketStatusAction()
    {
        $basketUnique = $this->params('basket_unique');
        if (empty($basketUnique)) {
            return $this->redirect()->toRoute('shop_frontend_article_articles');
        }
        $viewModel = new ViewModel();

        if (!$this->basketService->existShopBasket($basketUnique)) {
            $this->layout()->message = [
                'level' => 'warn',
                'text' => 'Es ist ein Fehler aufgetreten. Bitte wenden sie sich mit folgender Number an den Support: ' . $basketUnique
            ];
        } else {
            $this->basketService->setBasketUnique($basketUnique);
            $this->basketService->checkBasket(true);
            $viewModel->setVariable('shopBasketUnique', $basketUnique);
            $shopBasket = $this->basketService->getShopBasket($basketUnique);
            $viewModel->setVariable('userUuid', $shopBasket[0]['user_uuid'] ?: '');

            $shopDocumentOrder = $this->documentOrderService->getShopDocumentOrderByBasketUnique($basketUnique);
            $viewModel->setVariable('shopDocumentOrder', $shopDocumentOrder);

            $shopDocumentInvoice = $this->documentInvoiceService->getShopDocumentInvoiceByBasketUnique($basketUnique);
            $viewModel->setVariable('shopDocumentInvoice', $shopDocumentInvoice);

            $viewModel->setVariable('toOld', $this->basketService->isBasketTooOld($basketUnique));
        }

        return $viewModel;
    }

    /**
     *
     * @return ViewModel|Response
     */
    public function basketEntityRatingAction()
    {
        $basketUnique = filter_var($this->params('basket_unique'), FILTER_SANITIZE_STRING);
        if (empty($basketUnique)) {
            return $this->redirect()->toRoute('shop_frontend_article_articles');
        }
        $viewModel = new ViewModel();

        if (!$this->basketService->existShopBasket($basketUnique)) {
            $this->layout()->message = [
                'level' => 'warn',
                'text' => 'Es ist ein Fehler aufgetreten. Bitte wenden sie sich mit folgender Number an den Support: ' . $this->basketService->getBasketUnique()
            ];
        } else {
            $request = $this->getRequest();
            if ($request->isPost() && $request instanceof Request) {
                $basketItemId = filter_input(INPUT_POST, 'basket_item_id', FILTER_SANITIZE_NUMBER_INT);
                if ($basketItemId && !$this->shopArticleService->existShopBasketEntityRating($basketItemId)) {
                    $basketEntityData = $this->basketService->getShopBasketEntityByBasketItemId($basketUnique, $basketItemId);
                    if (!empty($basketEntityData)) {
                        if ($this->shopArticleRatingTable->saveShopArticleRating($basketEntityData['shop_article_id'],
                                filter_input(INPUT_POST, 'shop_article_rating_value', FILTER_SANITIZE_NUMBER_INT),
                                htmlentities(trim(filter_input(INPUT_POST, 'shop_article_rating_text', FILTER_SANITIZE_STRING))), $basketItemId) < 1) {
                            $this->layout()->message = [
                                'level' => 'error',
                                'text' => 'Es ist ein Fehler aufgetreten. Bitte wenden sie sich mit folgender Number an den Support: ' . $basketUnique
                            ];
                        }
                    }
                }
            }
            $viewModel->setVariable('shopBasket', $this->basketService->getShopBasketEntitiesWithRatings());
        }

        return $viewModel;
    }

}
