<?php

namespace Bitkorn\Shop\Controller\Frontend\User;

use Bitkorn\Shop\Service\User\ShopUserService;
use Bitkorn\Shop\Table\Basket\ShopBasketEntityTable;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\Response;
use Laminas\View\Model\ViewModel;
use Bitkorn\Shop\Controller\AbstractShopController;

/**
 *
 * @author allapow
 */
class UserBackendController extends AbstractShopController
{

    /**
     * @var ShopUserService
     */
    protected $shopUserService;

    /**
     * @param ShopUserService $shopUserService
     */
    public function setShopUserService(ShopUserService $shopUserService): void
    {
        $this->shopUserService = $shopUserService;
    }

    /**
     * Zeigt Uebersicht der Kaeufe
     * @return ViewModel|Response
     */
    public function userBasketsAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $viewModel = new ViewModel();

        $shopBaskets = $this->basketService->getShopBasketEntitiesWithRatingsByUserId($this->userService->getUserUuid());
        $viewModel->setVariable('shopBaskets', $shopBaskets);

        $viewModel->setVariable('invoicesBasketuniqueAssoc', $this->shopUserService->getShopDocumentInvoicesBasketuniqueAssocByUserid($this->userService->getUserUuid()));
        $viewModel->setVariable('ordersBasketuniqueAssoc', $this->shopUserService->getShopDocumentOrdersBasketuniqueAssocByUserid($this->userService->getUserUuid()));

        return $viewModel;
    }

    /**
     * @return ViewModel|Response
     */
    public function userProfileAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $viewModel = new ViewModel();
        $user = $this->shopUserService->getShopUserDataXByUserId($this->userService->getUserUuid());
        $viewModel->setVariable('user', $user);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $post = $request->getPost()->toArray();
            if (isset($post['passwd'])) {
                $passwdOld = filter_input(INPUT_POST, 'passwd_old', FILTER_SANITIZE_STRING);
                $passwdNew = filter_input(INPUT_POST, 'passwd_new', FILTER_SANITIZE_STRING);
                $passwdNewx = filter_input(INPUT_POST, 'passwd_new_x', FILTER_SANITIZE_STRING);
                if (
                    !$this->userService->canLogin($this->userService->getUserEntity()->getUserLogin(), $passwdOld)
                    || $passwdNew != $passwdNewx
                ) {
                    return $viewModel;
                }
                if($this->userService->updateUserPasswd($this->userService->getUserUuid(), $passwdNew)) {
                    $viewModel->setVariable('passwdOk', true);
                }
            }
        }
        return $viewModel;
    }
}
