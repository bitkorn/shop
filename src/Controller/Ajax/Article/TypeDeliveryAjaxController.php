<?php

namespace Bitkorn\Shop\Controller\Ajax\Article;

use Bitkorn\Shop\Controller\AbstractShopController;
use Bitkorn\Shop\Service\Article\ShopArticleTypeDeliveryService;
use Bitkorn\Trinket\View\Model\JsonModel;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\PhpEnvironment\Response;

class TypeDeliveryAjaxController extends AbstractShopController
{
    /**
     * @var ShopArticleTypeDeliveryService
     */
    protected $shopArticleTypeDeliveryService;

    /**
     * @param ShopArticleTypeDeliveryService $shopArticleTypeDeliveryService
     */
    public function setShopArticleTypeDeliveryService(ShopArticleTypeDeliveryService $shopArticleTypeDeliveryService): void
    {
        $this->shopArticleTypeDeliveryService = $shopArticleTypeDeliveryService;
    }

    /**
     * @return JsonModel
     */
    public function switchLengthcutDefAction()
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(3)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }

        $articleId = filter_var($this->params('article_id', 0), FILTER_SANITIZE_NUMBER_INT);
        $articleLengthcutDefId = filter_var($this->params('lengthcut_def_id', 0), FILTER_SANITIZE_NUMBER_INT);
        if (!$this->shopArticleTypeDeliveryService->isArticleLengthCutDefIdSwitchValid($articleId, $articleLengthcutDefId)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->shopArticleTypeDeliveryService->updateArticleLengthCutDefId($articleId, $articleLengthcutDefId)) {
            $jsonModel->setSuccess(1);
        }

        return $jsonModel;
    }
}
