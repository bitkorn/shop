<?php

namespace Bitkorn\Shop\Controller\Pos;

use Bitkorn\Shop\Entity\Basket\ShopBasketAddressEntity;
use Bitkorn\Shop\Entity\Basket\ShopBasketDiscountEntity;
use Bitkorn\Shop\Entity\Basket\XshopBasketEntity;
use Bitkorn\Shop\Form\Basket\ShopBasketAddressForm;
use Bitkorn\Shop\Service\Article\ShopArticleService;
use Bitkorn\Shop\Service\Basket\BasketFinalizeService;
use Bitkorn\Shop\Service\ShopService;
use Bitkorn\Shop\Service\User\ShopUserService;
use Bitkorn\Shop\Table\Basket\Discount\ShopBasketDiscountTable;
use Bitkorn\Shop\Table\Basket\ShopBasketAddressTable;
use Bitkorn\Shop\Tablex\Basket\ShopBasketTablex;
use Bitkorn\ShopPrepay\Service\PaymentNumberService;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\Response;
use Laminas\View\Model\JsonModel;
use Laminas\View\Model\ViewModel;
use Bitkorn\Shop\Controller\AbstractShopController;
use Bitkorn\Trinket\View\Helper\Messages;

/**
 *
 * @author allapow
 */
class SellController extends AbstractShopController
{
    /**
     * @var ShopService
     */
    protected $shopService;

    /**
     * @var ShopUserService
     */
    protected $shopUserService;

    /**
     *
     * @var PaymentNumberService
     */
    protected $paymentNumberService;

    /**
     *
     * @var BasketFinalizeService
     */
    protected $basketFinalizeService;

    /**
     * @var ShopArticleService
     */
    protected $shopArticleService;

    /**
     *
     * @var ShopBasketAddressForm
     */
    protected $shopBasketAddressFormInvoicePrivate;

    /**
     *
     * @var ShopBasketAddressForm
     */
    protected $shopBasketAddressFormInvoiceEnterprise;

    /**
     *
     * @var ShopBasketAddressForm
     */
    protected $shopBasketAddressFormShipmentPrivate;

    /**
     *
     * @var ShopBasketAddressForm
     */
    protected $shopBasketAddressFormShipmentEnterprise;

    /**
     * @var ShopBasketDiscountTable
     */
    protected $shopBasketDiscountTable;

    /**
     * @var ShopBasketTablex
     */
    protected $shopBasketTablex;

    /**
     * @var ShopBasketAddressTable
     */
    protected $shopBasketAddressTable;

    /**
     * @param ShopService $shopService
     */
    public function setShopService(ShopService $shopService): void
    {
        $this->shopService = $shopService;
    }

    /**
     * @param ShopUserService $shopUserService
     */
    public function setShopUserService(ShopUserService $shopUserService): void
    {
        $this->shopUserService = $shopUserService;
    }

    /**
     * @param PaymentNumberService $paymentNumberService
     */
    public function setPaymentNumberService(PaymentNumberService $paymentNumberService): void
    {
        $this->paymentNumberService = $paymentNumberService;
    }

    /**
     * @param BasketFinalizeService $basketFinalizeService
     */
    public function setBasketFinalizeService(BasketFinalizeService $basketFinalizeService): void
    {
        $this->basketFinalizeService = $basketFinalizeService;
    }

    /**
     * @param ShopArticleService $shopArticleService
     */
    public function setShopArticleService(ShopArticleService $shopArticleService): void
    {
        $this->shopArticleService = $shopArticleService;
    }

    /**
     * @param ShopBasketAddressForm $shopBasketAddressFormInvoicePrivate
     */
    public function setShopBasketAddressFormInvoicePrivate(ShopBasketAddressForm $shopBasketAddressFormInvoicePrivate): void
    {
        $this->shopBasketAddressFormInvoicePrivate = $shopBasketAddressFormInvoicePrivate;
    }

    /**
     * @param ShopBasketAddressForm $shopBasketAddressFormInvoiceEnterprise
     */
    public function setShopBasketAddressFormInvoiceEnterprise(ShopBasketAddressForm $shopBasketAddressFormInvoiceEnterprise): void
    {
        $this->shopBasketAddressFormInvoiceEnterprise = $shopBasketAddressFormInvoiceEnterprise;
    }

    /**
     * @param ShopBasketAddressForm $shopBasketAddressFormShipmentPrivate
     */
    public function setShopBasketAddressFormShipmentPrivate(ShopBasketAddressForm $shopBasketAddressFormShipmentPrivate): void
    {
        $this->shopBasketAddressFormShipmentPrivate = $shopBasketAddressFormShipmentPrivate;
    }

    /**
     * @param ShopBasketAddressForm $shopBasketAddressFormShipmentEnterprise
     */
    public function setShopBasketAddressFormShipmentEnterprise(ShopBasketAddressForm $shopBasketAddressFormShipmentEnterprise): void
    {
        $this->shopBasketAddressFormShipmentEnterprise = $shopBasketAddressFormShipmentEnterprise;
    }

    /**
     * @param ShopBasketDiscountTable $shopBasketDiscountTable
     */
    public function setShopBasketDiscountTable(ShopBasketDiscountTable $shopBasketDiscountTable): void
    {
        $this->shopBasketDiscountTable = $shopBasketDiscountTable;
    }

    /**
     * @param ShopBasketTablex $shopBasketTablex
     */
    public function setShopBasketTablex(ShopBasketTablex $shopBasketTablex): void
    {
        $this->shopBasketTablex = $shopBasketTablex;
    }

    /**
     * @param ShopBasketAddressTable $shopBasketAddressTable
     */
    public function setShopBasketAddressTable(ShopBasketAddressTable $shopBasketAddressTable): void
    {
        $this->shopBasketAddressTable = $shopBasketAddressTable;
    }

    /**
     *
     * @return ViewModel|Response
     */
    public function sellForUserAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(3) && !$this->userService->checkUserRoleAccess(101)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $userUuid = $this->params('user_uuid');
        if (!$this->userService->isValidUserUuid($userUuid)) {
            return $this->redirect()->toRoute('shop_admin_user_shopuseradmin_shopusers');
        }
        $viewModel = new ViewModel();
        $this->layout('layout/admin');

        $userWithAddresses = $this->shopUserService->getShopUserData($userUuid);
        if (empty($userWithAddresses)) {
            $this->layout()->message = [
                'level' => 'warn',
                'text' => 'Für den gewählten user existieren keine Daten.'
            ];
        } else {
            $viewModel->setVariable('userWithAddresses', $userWithAddresses);
        }

        if (empty($xshopBasketEntity = $this->basketService->checkBasket())) {
            $this->basketService->createEmptyBasket($userUuid, $this->userService->getUserUuid());
            $xshopBasketEntity = $this->basketService->getXshopBasketEntity();
        }

        $basketDiscountEnabled = false;
        $articlePriceTotalSum = $xshopBasketEntity->getArticlePriceTotalSum();
        if ($xshopBasketEntity instanceof XshopBasketEntity && !empty($xshopBasketEntity->getStorageItems())) {
            if (empty($xshopBasketEntity->getBasketDiscountHash()) && $this->shopService->getShopConfigurationValue('basket_discount_enable') == 1) {
                $basketDiscountEnabled = true;
            }
        }
        $addressCount = $this->shopUserService->countShopUserAddress($userUuid);
        $buyEnabled = false;
        if (!empty($xshopBasketEntity->getStorageItems()) && $addressCount > 0) {
            $buyEnabled = true;
        }

        $shopArticleCategoryId = 0;
        $shopArticleClassId = 0;
        $request = $this->getRequest();
        if ($request->isPost() && $request instanceof Request) {
            $postData = $request->getPost()->toArray();
            /*
             * discount
             */
            if(isset($postData['shop_basket_discount_hash'])) {
                if (!$this->basketService->computeBasketDiscount($postData['shop_basket_discount_hash'])) {
                    $this->layout()->message = $this->basketService->getMessage();
                }
            }
            /*
             * delete one basketItem
             */
            if (!empty($postData['shop_basket_item_id_delete'])) {
                $shopBasketItemIdDelete = (int)$postData['shop_basket_item_id_delete'];
                if ($this->basketService->updateShopBasketItemAmount($shopBasketItemIdDelete, 0) < 1) {
                    $this->layout()->message = ['level' => 'error', 'text' => 'Fehler beim Löschen'];
                } else {
                    $this->basketService->refreshBasket();
                    if ($xshopBasketEntity->getArticleAmountSum() < 1) {
                        $basketDiscountEnabled = false;
                        $buyEnabled = false;
                    }
                }
            }
            /*
             * shipping method
             */
            if (isset($postData['shipping_provider_unique_descriptor'])) {
                if ($this->basketService->setShippingProviderUniqueDescriptor(filter_input(INPUT_POST, 'shipping_provider_unique_descriptor', FILTER_SANITIZE_STRING)) < 0) {
                    $this->layout()->message = ['level' => 'error', 'text' => 'Fehler beim speichern der Versandart'];
                } else {
                    $this->basketService->refreshBasket();
                }
            }
            /*
             * article-list filter
             */
            if (!empty($postData['shop_article_category_id'])) {
                $shopArticleCategoryId = (int)$postData['shop_article_category_id'];
                $viewModel->setVariable('shopArticleCategoryCurrent', $shopArticleCategoryId);
            }
            if (!empty($postData['shop_article_class_id'])) {
                $shopArticleClassId = (int)$postData['shop_article_class_id'];
                $viewModel->setVariable('shopArticleClassCurrent', $shopArticleClassId);
            }
            $articlesData = $this->shopArticleService->getShopArticlesActiveByCategoryIdAndClassId($shopArticleCategoryId, $shopArticleClassId);
            $viewModel->setVariable('articlesData', $articlesData);
            /*
             * submit basket
             */
            if ($buyEnabled && !empty($postData['submit_basket_pos'])) {
                $this->basketService->setBasketUser($userUuid);
                $this->basketService->setPaymentMethod('prepay');
                $paymentNumber = $this->paymentNumberService->getNewPrepayNumber();
                if ($this->basketService->setPaymentNumber($paymentNumber) > 0) {
                    if ($this->basketService->setShopBasketStatusByPaymentNumber($paymentNumber, 'ordered') >= 0) {
                        if ($this->basketFinalizeService->basketOrdered($this->basketService->getBasketUnique()) >= 0) {
                            $this->layout()->message = [
                                'level' => 'info',
                                'text' => 'Der Warenkorb wurde bestellt.'
                            ];
                            return $this->redirect()->toRoute('shop_admin_basket_basketadmin_basket', ['basket_unique' => $this->basketService->getBasketUnique()]);
                        } else {
                            $this->layout()->message = [
                                'level' => 'warn',
                                'text' => 'Fehler beim Bestellen des Warenkorbs.'
                            ];
                        }
                    } else {
                        $this->layout()->message = [
                            'level' => 'warn',
                            'text' => 'Fehler beim Speichern des Bestell-Status.'
                        ];
                    }
                } else {
                    $this->layout()->message = [
                        'level' => 'warn',
                        'text' => 'Fehler beim Erstellen der Payment-ID.'
                    ];
                }
            }
        }

        $viewModel->setVariable('basketDiscountEnabled', $basketDiscountEnabled);
        $viewModel->setVariable('basketDiscountEnabledSys', $this->shopService->getShopConfigurationValue('basket_discount_enable'));
        $viewModel->setVariable('buyEnabled', $buyEnabled);
        $viewModel->setVariable('deleteBasketEnabled', !empty($xshopBasketEntity->getStorageItems()));
        $viewModel->setVariable('addressCount', $addressCount);
        $viewModel->setVariable('shippingPickupCustomer', !empty($xshopBasketEntity->isShippingPickupCustomer()));
        $viewModel->setVariable('shopArticleClassIdAssoc', $this->shopArticleService->getShopArticleClasssIdAssoc());
        return $viewModel;
    }

    /**
     *
     * @return ViewModel|Response
     */
    public function sellForGuestAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(2) && !$this->userService->checkUserRoleAccess(101)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $viewModel = new ViewModel();
        $this->layout('layout/admin');

        if (!$this->basketService->existShopBasket()) {
            $this->basketService->createEmptyBasket('', $this->userService->getUserUuid());
        }

        $basketDiscountEnabled = false;
        $articlePriceTotalSum = 0;
        $xshopBasketEntity = $this->basketService->checkBasket();
        if ($xshopBasketEntity instanceof XshopBasketEntity && !empty($xshopBasketEntity->getStorageItems())) {
            if (empty($xshopBasketEntity->getBasketDiscountHash()) && $this->shopService->getShopConfigurationValue('basket_discount_enable') == 1) {
                $basketDiscountEnabled = true;
            }
            $articlePriceTotalSum = $xshopBasketEntity->getArticlePriceTotalSum();
        }

        $addressCount = $this->basketService->countShopBasketAddresses();
        $buyEnabled = false;
        if (!empty($xshopBasketEntity->getStorageItems()) && $addressCount > 0) {
            $buyEnabled = true;
        }

        $shopArticleCategoryId = 0;
        $shopArticleClassId = 0;
        $request = $this->getRequest();
        if ($request->isPost() && $request instanceof Request) {
            $postData = $request->getPost()->toArray();
            /*
             * discount
             */
            if ($basketDiscountEnabled && !empty($postData['shop_basket_discount_hash']) && $articlePriceTotalSum > 0) {
                $shopBasketDiscountHash = filter_var($postData['shop_basket_discount_hash'], FILTER_SANITIZE_STRING);
                $shopBasketDiscountEntity = new ShopBasketDiscountEntity();
                $shopBasketDiscountEntity->loadShopBasketDiscount($shopBasketDiscountHash, $this->shopBasketDiscountTable, $this->shopBasketTablex);
                if ($shopBasketDiscountEntity->isHashValid()) {
                    if ($shopBasketDiscountEntity->getMinOrderAmount() <= $articlePriceTotalSum) {
                        if ($this->basketService->updateShopBasketValues([
                            'shop_basket_discount_hash' => $shopBasketDiscountHash,
                            'shop_basket_discount_value' => $shopBasketDiscountEntity->computeDiscountValue($articlePriceTotalSum)])) {
                            $shopBasketDiscountEntity->loadShopBasketDiscount($shopBasketDiscountHash, $this->shopBasketDiscountTable, $this->shopBasketTablex);
                            $this->basketService->refreshBasket();
                            $basketDiscountEnabled = false;
                        }
                    } else {
                        $this->layout()->message = ['level' => 'info', 'text' => 'Der Mindestbestellwert für diesen Rabatt-Code ist nicht erreicht.'];
                    }
                } else {
                    $this->layout()->message = ['level' => 'info', 'text' => 'Der Rabatt-Code ist nicht gültig.'];
                }
            }
            /*
             * delete one basketItem
             */
            if (!empty($postData['shop_basket_item_id_delete'])) {
                $shopBasketItemIdDelete = (int)$postData['shop_basket_item_id_delete'];
                if ($this->basketService->updateShopBasketItemAmount($shopBasketItemIdDelete, 0) < 1) {
                    $this->layout()->message = ['level' => 'error', 'text' => 'Fehler beim Löschen'];
                } else {
                    $this->basketService->refreshBasket();
                    $xshopBasketEntity = $this->basketService->getXshopBasketEntity();
                    if ($xshopBasketEntity->getArticleAmountSum() < 1) {
                        $basketDiscountEnabled = false;
                        $buyEnabled = false;
                    }
                }
            }
            /*
             * shipping method
             */
            if (isset($postData['shipping_provider_unique_descriptor'])) {
                if (!$this->basketService->setShippingProviderUniqueDescriptor(filter_input(INPUT_POST, 'shipping_provider_unique_descriptor', FILTER_SANITIZE_STRING))) {
                    $this->layout()->message = ['level' => 'error', 'text' => 'Fehler beim speichern der Versandart'];
                } else {
                    $this->basketService->refreshBasket();
                }
            }
            /*
             * article-list filter
             */
            if (!empty($postData['shop_article_category_id'])) {
                $shopArticleCategoryId = (int)$postData['shop_article_category_id'];
                $viewModel->setVariable('shopArticleCategoryCurrent', $shopArticleCategoryId);
            }
            if (!empty($postData['shop_article_class_id'])) {
                $shopArticleClassId = (int)$postData['shop_article_class_id'];
                $viewModel->setVariable('shopArticleClassCurrent', $shopArticleClassId);
            }
            $viewModel->setVariable('articlesData', $this->shopArticleService->getShopArticlesActiveByCategoryIdAndClassId($shopArticleCategoryId, $shopArticleClassId));
            /*
             * submit basket
             */
            if ($buyEnabled && !empty($postData['submit_basket_pos'])) {
                $this->basketService->setBasketUser('');
                $this->basketService->setPaymentMethod('prepay');
                $paymentNumber = $this->paymentNumberService->getNewPrepayNumber();
                if ($this->basketService->setPaymentNumber($paymentNumber) > 0) {
                    if ($this->basketService->setShopBasketStatusByPaymentNumber($paymentNumber, 'ordered') >= 0) {
                        if ($this->basketFinalizeService->basketOrdered($this->basketService->getBasketUnique()) >= 0) {
                            $this->layout()->message = [
                                'level' => 'info',
                                'text' => 'Der Warenkorb wurde bestellt.'
                            ];
                            return $this->redirect()->toRoute('shop_admin_basket_basketadmin_basket', ['basket_unique' => $this->basketService->getBasketUnique()]);
                        } else {
                            $this->layout()->message = [
                                'level' => 'warn',
                                'text' => 'Fehler beim Bestellen des Warenkorbs.'
                            ];
                        }
                    } else {
                        $this->layout()->message = [
                            'level' => 'warn',
                            'text' => 'Fehler beim Speichern des Bestell-Status.'
                        ];
                    }
                } else {
                    $this->layout()->message = [
                        'level' => 'warn',
                        'text' => 'Fehler beim Erstellen der Payment-ID.'
                    ];
                }
            }
        }
        $xshopBasketEntity = $this->basketService->getXshopBasketEntity();
        $viewModel->setVariable('basketDiscountEnabled', $basketDiscountEnabled);
        $viewModel->setVariable('basketDiscountEnabledSys', $this->shopService->getShopConfigurationValue('basket_discount_enable'));
        $viewModel->setVariable('buyEnabled', $buyEnabled);
        $viewModel->setVariable('deleteBasketEnabled', !empty($xshopBasketEntity->getStorageItems()));
        $viewModel->setVariable('addressCount', $addressCount);
        $viewModel->setVariable('shippingPickupCustomer', !empty($xshopBasketEntity->isShippingPickupCustomer()));

        $viewModel->setVariable('shopArticleClassIdAssoc', $this->shopArticleService->getShopArticleClasssIdAssoc());

        $viewModel->setVariable('basketAddressCount', $this->basketService->countShopBasketAddresses());

        return $viewModel;
    }

    /**
     *
     * @return ViewModel|Response
     */
    public function enterBasketAddressAction()
    {
        if (empty($xshopBasketEntity = $this->basketService->checkBasket())) {
            return $this->redirect()->toRoute('shop_pos_sellforguest');
        }
        if (!$this->userService->checkUserRoleAccessMin(3) && !$this->userService->checkUserRoleAccess(101)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $viewModel = new ViewModel();
        $this->layout('layout/admin');

        $customerTypeInvoiceParam = filter_var($this->params()->fromQuery('cti'), FILTER_SANITIZE_STRING);
        if (!empty($customerTypeInvoiceParam) && $customerTypeInvoiceParam == 'private') {
            $isPrivateInvoice = true;
            setcookie('customerTypeInvoice', 'private', time() + 60 * 60 * 24, '/');
        } elseif (!empty($customerTypeInvoiceParam) && $customerTypeInvoiceParam == 'enterprise') {
            $isPrivateInvoice = false;
            setcookie('customerTypeInvoice', 'enterprise', time() + 60 * 60 * 24, '/');
        } else {
            $customerTypeInvoiceCookie = filter_input(INPUT_COOKIE, 'customerTypeInvoice', FILTER_SANITIZE_STRING);
            if (isset($customerTypeInvoiceCookie) && $customerTypeInvoiceCookie == 'private') {
                $isPrivateInvoice = true;
            } elseif (isset($customerTypeInvoiceCookie) && $customerTypeInvoiceCookie == 'enterprise') {
                $isPrivateInvoice = false;
            } else {
                $isPrivateInvoice = true;
            }
        }

        $showVariantShippingAddress = false;

        $customerTypeShipmentParam = filter_var($this->params()->fromQuery('cts'), FILTER_SANITIZE_STRING);
        if (!empty($customerTypeShipmentParam)) {
            $showVariantShippingAddress = true;
        }
        if (!empty($customerTypeShipmentParam) && $customerTypeShipmentParam == 'private') {
            $isPrivateShipment = true;
            setcookie('customerTypeShipment', 'private', time() + 60 * 60 * 24, '/');
        } elseif (!empty($customerTypeShipmentParam) && $customerTypeShipmentParam == 'enterprise') {
            $isPrivateShipment = false;
            setcookie('customerTypeShipment', 'enterprise', time() + 60 * 60 * 24, '/');
        } else {
            $customerTypeShipmentCookie = filter_input(INPUT_COOKIE, 'customerTypeShipment', FILTER_SANITIZE_STRING);
            if (isset($customerTypeShipmentCookie) && $customerTypeShipmentCookie == 'private') {
                $isPrivateShipment = true;
            } elseif (isset($customerTypeShipmentCookie) && $customerTypeShipmentCookie == 'enterprise') {
                $isPrivateShipment = false;
            } else {
                $isPrivateShipment = true;
            }
        }
        $basketAddresses = $this->basketService->getShopBasketAddressesByBasketUnique();
        $basketAddressCount = count($basketAddresses);
        if ($basketAddressCount > 0) {
            unset($basketAddresses[0]['shop_basket_address_customer_type']); // wird bei Erstellung der Form (Factory) gesetzt
            $basketAddresses[0]['shop_basket_address_birthday'] = $this->toDatestringDe($basketAddresses[0], 'shop_basket_address_birthday');
        }
        if ($basketAddressCount == 2) {
            $basketAddresses[1]['shop_basket_address_birthday'] = $this->toDatestringDe($basketAddresses[1], 'shop_basket_address_birthday');
        }
        if ($isPrivateInvoice) {
            $viewModel->setVariable('shopBasketAddressFormInvoicePrivate', $this->shopBasketAddressFormInvoicePrivate);
            if ($basketAddressCount == 0) {
                $this->shopBasketAddressFormInvoicePrivate->setShopBasketAddressIdRequired(false);
                $this->shopBasketAddressFormInvoicePrivate->init();
                $this->shopBasketAddressFormInvoicePrivate->setData(['shop_basket_unique' => $this->basketService->getBasketUnique()]);
            }
            if ($basketAddressCount >= 1) {
                $this->shopBasketAddressFormInvoicePrivate->setData($basketAddresses[0]);
            }
        } else {
            $viewModel->setVariable('shopBasketAddressFormInvoiceEnterprise', $this->shopBasketAddressFormInvoiceEnterprise);
            if ($basketAddressCount == 0) {
                $this->shopBasketAddressFormInvoiceEnterprise->setShopBasketAddressIdRequired(false);
                $this->shopBasketAddressFormInvoiceEnterprise->init();
                $this->shopBasketAddressFormInvoiceEnterprise->setData(['shop_basket_unique' => $this->basketService->getBasketUnique()]);
            }
            if ($basketAddressCount >= 1) {
                $this->shopBasketAddressFormInvoiceEnterprise->setData($basketAddresses[0]);
            }
        }
        if ($isPrivateShipment) {
            $viewModel->setVariable('shopBasketAddressFormShipmentPrivate', $this->shopBasketAddressFormShipmentPrivate);
            if ($basketAddressCount == 1) {
                $this->shopBasketAddressFormShipmentPrivate->setShopBasketAddressIdRequired(false);
                $this->shopBasketAddressFormShipmentPrivate->init();
                $this->shopBasketAddressFormShipmentPrivate->setData(['shop_basket_unique' => $this->basketService->getBasketUnique()]);
            }
            if ($basketAddressCount == 2) {
                $this->shopBasketAddressFormShipmentPrivate->setData($basketAddresses[1]);
            }
        } else {
            $viewModel->setVariable('shopBasketAddressFormShipmentEnterprise', $this->shopBasketAddressFormShipmentEnterprise);
            if ($basketAddressCount == 1) {
                $this->shopBasketAddressFormShipmentEnterprise->setShopBasketAddressIdRequired(false);
                $this->shopBasketAddressFormShipmentEnterprise->init();
                $this->shopBasketAddressFormShipmentEnterprise->setData(['shop_basket_unique' => $this->basketService->getBasketUnique()]);
            }
            if ($basketAddressCount == 2) {
                $this->shopBasketAddressFormShipmentEnterprise->setData($basketAddresses[1]);
            }
        }

        $request = $this->getRequest();
        if ($request->isPost() && $request instanceof Request) {
            $postData = $request->getPost()->toArray();
            $userRegisterFormValid = true;
            if ($postData['form_name'] == 'shop_basket_address_invoice_private') {
                $this->shopBasketAddressFormInvoicePrivate->setData($postData);
                $this->shopBasketAddressFormInvoicePrivate->switchDate('integer');
                if ($this->shopBasketAddressFormInvoicePrivate->isValid() && $userRegisterFormValid) {
                    $shopBasketAdressEntity = new ShopBasketAddressEntity();
                    $shopBasketAdressEntity->exchangeArray($this->shopBasketAddressFormInvoicePrivate->getData());
                    if ($shopBasketAdressEntity->isBasketUniqueValid($this->basketService->getBasketUnique())) {
                        if ($shopBasketAdressEntity->saveOrUpdate($this->shopBasketAddressTable, $basketAddressCount) >= 0) {
                            $basketAddresses = $this->basketService->getShopBasketAddressesByBasketUnique();
                            $this->shopBasketAddressFormInvoicePrivate->setData($basketAddresses[0]);
                            $basketAddressCount = count($basketAddresses);
                            $this->layout()->message = Messages::$messageSuccessSaveData;
                        } else {
                            $this->layout()->message = Messages::$messageErrorSaveData;
                        }
                    }
                }
                $this->shopBasketAddressFormInvoicePrivate->switchDate('string');
            } elseif ($postData['form_name'] == 'shop_basket_address_invoice_enterprise') {
                $this->shopBasketAddressFormInvoiceEnterprise->setData($postData);
                if ($this->shopBasketAddressFormInvoiceEnterprise->isValid() && $userRegisterFormValid) {
                    $shopBasketAdressEntity = new ShopBasketAddressEntity();
                    $shopBasketAdressEntity->exchangeArray($this->shopBasketAddressFormInvoiceEnterprise->getData());
                    if ($shopBasketAdressEntity->isBasketUniqueValid($this->basketService->getBasketUnique())) {
                        if ($shopBasketAdressEntity->saveOrUpdate($this->shopBasketAddressTable, $basketAddressCount) >= 0) {
                            $basketAddresses = $this->basketService->getShopBasketAddressesByBasketUnique();
                            $this->shopBasketAddressFormInvoiceEnterprise->setData($basketAddresses[0]);
                            $basketAddressCount = count($basketAddresses);
                            $this->layout()->message = Messages::$messageSuccessSaveData;
                        } else {
                            $this->layout()->message = Messages::$messageErrorSaveData;
                        }
                    }
                }
            } elseif ($postData['form_name'] == 'shop_basket_address_shipment_private') {
                if (!empty($postData['submit_delete']) && !empty($postData['shop_basket_address_id']) && !empty($postData['shop_basket_unique']) && $postData['shop_basket_unique'] == $basketUnique) {
                    if ($this->basketService->deleteShopBasketAddress([
                        'shop_basket_address_id' => (int)$postData['shop_basket_address_id'],
                        'shop_basket_unique' => $postData['shop_basket_unique']
                    ])) {
                        $this->shopBasketAddressFormShipmentPrivate->setData([]);
                        $this->shopBasketAddressFormShipmentPrivate->init();
                        return $this->redirect()->toRoute('shop_pos_enterbasketaddress');
                    }
                } elseif (!empty($postData['submit_delete'])) {
                    $showVariantShippingAddress = false;
                } elseif (empty($postData['submit_delete'])) {
                    $this->shopBasketAddressFormShipmentPrivate->setData($postData);
                    $showVariantShippingAddress = true;
                    if ($this->shopBasketAddressFormShipmentPrivate->isValid()) {
                        $shopBasketAdressEntity = new ShopBasketAddressEntity();
                        $shopBasketAdressEntity->exchangeArray($this->shopBasketAddressFormShipmentPrivate->getData());
                        if ($shopBasketAdressEntity->isBasketUniqueValid($basketUnique)) {
                            if ($shopBasketAdressEntity->saveOrUpdate($this->shopBasketAddressTable, $basketAddressCount) >= 0) {
                                $basketAddresses = $this->basketService->getShopBasketAddressesByBasketUnique();
                                $this->shopBasketAddressFormShipmentPrivate->setData($basketAddresses[1]);
                                $basketAddressCount = count($basketAddresses);
                                $this->layout()->message = Messages::$messageSuccessSaveData;
                            } else {
                                $this->layout()->message = Messages::$messageErrorSaveData;
                            }
                        }
                    }
                }
            } elseif ($postData['form_name'] == 'shop_basket_address_shipment_enterprise') {
                if (
                    !empty($postData['submit_delete'])
                    && !empty($postData['shop_basket_address_id'])
                    && !empty($postData['shop_basket_unique'])
                    && $postData['shop_basket_unique'] == $this->basketService->getBasketUnique()
                ) {
                    if ($this->basketService->deleteShopBasketAddress(['shop_basket_address_id' => (int)$postData['shop_basket_address_id'],
                            'shop_basket_unique' => $postData['shop_basket_unique']]) > 0) {
                        $this->shopBasketAddressFormShipmentEnterprise->setData([]);
                        $this->shopBasketAddressFormShipmentEnterprise->init();
                        return $this->redirect()->toRoute('shop_pos_enterbasketaddress');
                    }
                } elseif (!empty($postData['submit_delete'])) {
                    $showVariantShippingAddress = false;
                } elseif (empty($postData['submit_delete'])) {
                    $this->shopBasketAddressFormShipmentEnterprise->setData($postData);
                    $showVariantShippingAddress = true;
                    if ($this->shopBasketAddressFormShipmentEnterprise->isValid()) {
                        $shopBasketAdressEntity = new ShopBasketAddressEntity();
                        $shopBasketAdressEntity->exchangeArray($this->shopBasketAddressFormShipmentEnterprise->getData());
                        if ($shopBasketAdressEntity->isBasketUniqueValid($this->basketService->getBasketUnique())) {
                            if ($shopBasketAdressEntity->saveOrUpdate($this->shopBasketAddressTable, $basketAddressCount) >= 0) {
                                $basketAddresses = $this->shopBasketAddressTable->getShopBasketAddressesByBasketUnique($this->basketService->getBasketUnique());
                                $this->shopBasketAddressFormShipmentEnterprise->setData($basketAddresses[1]);
                                $basketAddressCount = count($basketAddresses);
                                $this->layout()->message = Messages::$messageSuccessSaveData;
                            } else {
                                $this->layout()->message = Messages::$messageErrorSaveData;
                            }
                        }
                    }
                }
            }
        }

        $viewModel->setVariable('isPrivateInvoice', $isPrivateInvoice);
        $viewModel->setVariable('isPrivateShipment', $isPrivateShipment);
        $viewModel->setVariable('basketAddressCount', $basketAddressCount);
        $viewModel->setVariable('showVariantShippingAddress', $showVariantShippingAddress);

        return $viewModel;
    }

}
