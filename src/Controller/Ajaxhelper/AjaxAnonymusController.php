<?php

namespace Bitkorn\Shop\Controller\Ajaxhelper;

use Bitkorn\Shop\Form\Basket\ShopBasketAddressForm;
use Laminas\Http\PhpEnvironment\Response;
use Laminas\View\Model\ViewModel;
use Laminas\View\Model\JsonModel;
use Bitkorn\Shop\Controller\AbstractShopController;

class AjaxAnonymusController extends AbstractShopController
{

    /**
     * @var ShopBasketAddressForm
     */
    protected $basketAddressForm;

    /**
     * @param ShopBasketAddressForm $basketAddressForm
     */
    public function setBasketAddressForm(ShopBasketAddressForm $basketAddressForm): void
    {
        $this->basketAddressForm = $basketAddressForm;
    }

    /**
     *
     * @return ViewModel
     */
    public function addressDisplayAction()
    {
        $viewModel = new ViewModel();

        $basketAddresses = $this->basketService->getShopBasketAddressesByBasketUnique();
        $viewModel->setVariable('basketAddresses', $basketAddresses);
        $viewModel->setVariable('basketAddressesCount', count($basketAddresses));
//        $existShipmentAddress = false;
//        foreach ($basketAddresses as $basketAddress) {
//            if ($basketAddress['shop_basket_address_type'] == 'shipment') {
//                $existShipmentAddress = true;
//            }
//        }
//        $viewModel->setVariable('existShipmentAddress', $existShipmentAddress);

        $this->layout('layout/clean');

        return $viewModel;
    }

    /**
     *
     * @return ViewModel
     */
    public function addressFormAction()
    {
        $addressId = (int)$this->params('address_id');
        $viewModel = new ViewModel();

        if (!empty($addressId)) {
            $basketAddress = $this->basketService->getShopBasketAddressById($addressId);
            if ($this->basketService->getBasketUnique() != $basketAddress['shop_basket_unique']) {
                $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
                return $viewModel;
            }
        }

        if (!empty($addressId)) {
//            $basketAddressForm->setShopBasketAddressIdRequired(true); // is in AJAX Form scheiss egal
        }

        if ((!empty($basketAddress) && $basketAddress['shop_basket_address_type'] == 'shipment') || (empty($basketAddress) && $this->getShopBasketAddressTable()->countShopBasketAddresses($shopBasketUnique) >= 1)) {
            $this->basketAddressForm->setTaxIdEnabled(false);
        }

        $this->basketAddressForm->init();
        if (!empty($addressId) && !empty($basketAddress)) {
            $this->basketAddressForm->setData($basketAddress);
        } else {
            $this->basketAddressForm->setData(['shop_basket_unique' => $this->basketService->getBasketUnique()]);
        }

        $viewModel->setVariable('shopBasketAddressForm', $this->basketAddressForm);

        $this->layout('layout/clean');

        return $viewModel;
    }

    /**
     *
     * @return string|JsonModel
     */
    public function addressDeleteAction()
    {
        $addressId = (int)$this->params('address_id');
        $jsonModel = new JsonModel();

        $basketAddress = $this->basketService->getShopBasketAddressById($addressId);
        if ($this->basketService->getBasketUnique() != $basketAddress['shop_basket_unique']) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }

        if ($this->basketService->deleteShopBasketAddress($basketAddress)) {
            $jsonModel->setVariable('status', 1);
        } else {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
        }

        return $jsonModel;
    }

}
