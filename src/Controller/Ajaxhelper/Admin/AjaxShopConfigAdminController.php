<?php

namespace Bitkorn\Shop\Controller\Ajaxhelper\Admin;

use Bitkorn\Shop\Service\ShopService;
use Laminas\Http\PhpEnvironment\Response;
use Laminas\View\Model\ViewModel;
use Laminas\View\Model\JsonModel;
use Bitkorn\Shop\Controller\AbstractShopController;

/**
 *
 * @author allapow
 */
class AjaxShopConfigAdminController extends AbstractShopController
{
    /**
     * @var ShopService
     */
    protected $shopService;

    /**
     * @param ShopService $shopService
     */
    public function setShopService(ShopService $shopService): void
    {
        $this->shopService = $shopService;
    }

    /**
     *
     * @return ViewModel
     */
    public function updateShopConfigValueAction()
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(2)) {
            return $jsonModel;
        }
        $shopConfigurationId = (int)$this->params('shop_configuration_id');
        $shopConfigurationValue = $this->params()->fromPost('newvalue');
        if (empty($shopConfigurationId)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
//        $jsonModel->setVariable('shopConfigurationId', $shopConfigurationId);
//        $jsonModel->setVariable('shopConfigurationValue', $shopConfigurationValue);

        if ($this->shopService->updateShopConfigurationValue($shopConfigurationId, $shopConfigurationValue) >= 0) {
            $jsonModel->setVariable('status', 1);
            $jsonModel->setVariable('newvalue', $shopConfigurationValue);
        } else {
            $jsonModel->setVariable('status', 0);
        }

        return $jsonModel;
    }

}
