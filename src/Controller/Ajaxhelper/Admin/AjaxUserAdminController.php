<?php

namespace Bitkorn\Shop\Controller\Ajaxhelper\Admin;

use Bitkorn\Shop\Entity\User\ShopUserAddressEntity;
use Bitkorn\Shop\Form\User\ShopUserAddressForm;
use Bitkorn\Shop\Service\User\ShopUserService;
use Bitkorn\Shop\Table\User\ShopUserAddressTable;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\PhpEnvironment\Response;
use Laminas\View\Model\ViewModel;
use Laminas\View\Model\JsonModel;
use Bitkorn\Shop\Controller\AbstractShopController;

/**
 *
 * @author allapow
 */
class AjaxUserAdminController extends AbstractShopController
{

    /**
     * @var ShopUserService
     */
    protected $shopUserService;

    /**
     * @var ShopUserAddressForm
     */
    protected $userAddressForm;

    /**
     * @var ShopUserAddressTable
     */
    protected $shopUserAddressTable;

    /**
     * @param ShopUserService $shopUserService
     */
    public function setShopUserService(ShopUserService $shopUserService): void
    {
        $this->shopUserService = $shopUserService;
    }

    /**
     * @param ShopUserAddressForm $userAddressForm
     */
    public function setUserAddressForm(ShopUserAddressForm $userAddressForm): void
    {
        $this->userAddressForm = $userAddressForm;
    }

    /**
     * @param ShopUserAddressTable $shopUserAddressTable
     */
    public function setShopUserAddressTable(ShopUserAddressTable $shopUserAddressTable): void
    {
        $this->shopUserAddressTable = $shopUserAddressTable;
    }

    /**
     *
     * @return ViewModel
     */
    public function addressDisplayAction()
    {
        $viewModel = new ViewModel();
        if (!$this->userService->checkUserRoleAccessMin(2) && !$this->userService->checkUserRoleAccess(101)) {
            return $viewModel;
        }
        $userUuid = $this->params('user_uuid');
        $this->layout('layout/clean');

        $userAddresses = $this->shopUserService->getShopUserAddressesByUserId($userUuid);
        $viewModel->setVariable('textarea', $userAddresses);
        $viewModel->setVariable('userAddresses', $userAddresses);
        $viewModel->setVariable('userAddressesCount', count($userAddresses));
        $existShipmentAddress = false;
        foreach ($userAddresses as $userAddress) {
            if ($userAddress['shop_user_address_type'] == 'all' || $userAddress['shop_user_address_type'] == 'shipment') {
                $existShipmentAddress = true;
            }
        }
        $viewModel->setVariable('existShipmentAddress', $existShipmentAddress);

        return $viewModel;
    }

    /**
     *
     * @return ViewModel
     */
    public function addressFormAction()
    {
        $viewModel = new ViewModel();
        $userUuid = $this->params('user_uuid');
        $addressId = (int)$this->params('address_id');
        if ((!$this->userService->checkUserRoleAccessMin(2) && !$this->userService->checkUserRoleAccess(101)) || empty($userUuid)) { // || empty($addressId)
            return $viewModel;
        }
        $this->userAddressForm->init();
        if (!empty($addressId)) {
            $userAddress = $this->shopUserService->getShopUserAddressById($addressId);
            if ($userUuid != $userAddress['user_uuid']) {
                $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
                return $viewModel;
            }
            if (!empty($userAddress['shop_user_address_birthday'])) {
                $userAddress['shop_user_address_birthday'] = strftime('%d.%m.%Y', $userAddress['shop_user_address_birthday']);
            }
            if ($userAddress['shop_user_address_customer_type'] == 'private') {
                $this->userAddressForm->setTaxIdEnabled(false);
                $this->userAddressForm->setCompanyNameEnabled(false);
                $this->userAddressForm->setCompanyDepartmentEnabled(false);
            } elseif ($userAddress['shop_user_address_customer_type'] == 'enterprise') {
                $this->userAddressForm->setTaxIdEnabled(true);
                $this->userAddressForm->setCompanyNameEnabled(TRUE);
                $this->userAddressForm->setCompanyDepartmentEnabled(true);
            }
            $userAddressCount = $this->shopUserService->countShopUserAddress($userUuid);
            if ($userAddressCount < 2 && $userAddress['shop_user_address_customer_type'] == 'private') {
                $this->userAddressForm->setBirthdayEnabled(TRUE);
            } else {
                $this->userAddressForm->setBirthdayEnabled(FALSE);
            }
        }

        $this->userAddressForm->init();
        if (!empty($addressId)) {
            $this->userAddressForm->setData($userAddress);
//            $this->userAddressForm->setShopUserAddressIdRequired(true); // is in AJAX Form scheiss egal
        } else {
            $this->userAddressForm->setData(['user_uuid' => $userUuid]);
        }

        $viewModel->setVariable('shopUserAddressForm', $this->userAddressForm);

        $this->layout('layout/clean');

        return $viewModel;
    }

    /**
     *
     * @return string|JsonModel
     */
    public function addressDeleteAction()
    {
        $addressId = (int)$this->params('address_id');
        $userUuid = $this->params('user_uuid');
        $jsonModel = new JsonModel();
        if ((!$this->userService->checkUserRoleAccessMin(3) && !$this->userService->checkUserRoleAccess(101)) || empty($userUuid) || empty($addressId)) {
            return $jsonModel;
        }
        $userAddress = $this->shopUserService->getShopUserAddressById($addressId);
        if ($userUuid != $userAddress['user_uuid']) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $userAddressEntity = new ShopUserAddressEntity();
        $userAddressEntity->flipMapping();
        $userAddressEntity->exchangeArray($userAddress);
        $deleteResult = $userAddressEntity->delete($this->shopUserAddressTable);
        if ($deleteResult > 0) {
            $jsonModel->setVariable('message', 'delete success');
        } else {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
        }

        return $jsonModel;
    }

    public function shopUserAutocompleteAction()
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(2) && !$this->userService->checkUserRoleAccess(101)) {
            return $jsonModel;
        }
        $request = $this->getRequest();
        $postData = [];
        if ($request->isPost() && $request instanceof Request) {
            $postData = $request->getPost()->toArray();
            if (isset($postData['term'])) {
//				$userResult = [['label' => 'userid1', 'value' => 'usernamexy'], ['label' => '234', 'value' => 'Peter Chris']];
//				echo json_encode($userResult);
//				exit();
                $term = filter_var($postData['term'], FILTER_SANITIZE_STRING);
                $users = $this->shopUserAddressTable->searchShopUserByName_jQueryAutocomplete($term);
                $jsonModel->setVariable('items', json_encode($users));
            }
        }

        return $jsonModel;
    }

}
