<?php

namespace Bitkorn\Shop\Controller\Ajaxhelper\Admin;

use Bitkorn\Shop\Entity\Basket\XshopBasketEntity;
use Bitkorn\Shop\Form\Basket\ShopBasketDiscountForm;
use Bitkorn\Shop\Service\Basket\BasketAdminService;
use Bitkorn\Shop\Service\User\ShopUserService;
use Bitkorn\Shop\Table\Basket\Discount\ShopBasketDiscountTable;
use Bitkorn\Shop\Tablex\Basket\ShopBasketTablex;
use Laminas\View\Model\ViewModel;
use Laminas\View\Model\JsonModel;
use Bitkorn\Shop\Controller\AbstractShopController;
use Bitkorn\Shop\Entity\Basket\ShopBasketDiscountEntity;

/**
 *
 * @author allapow
 */
class AjaxBasketAdminController extends AbstractShopController
{

    /**
     *
     * @var ShopBasketDiscountForm
     */
    protected $shopBasketDiscountForm;

    /**
     * @var BasketAdminService
     */
    protected $basketAdminService;

    /**
     * @var ShopUserService
     */
    protected $shopUserService;

    /**
     * @var ShopBasketDiscountTable
     */
    protected $shopBasketDiscountTable;

    /**
     * @var ShopBasketTablex
     */
    protected $shopBasketTablex;

    /**
     * @param ShopBasketDiscountForm $shopBasketDiscountForm
     */
    public function setShopBasketDiscountForm(ShopBasketDiscountForm $shopBasketDiscountForm): void
    {
        $this->shopBasketDiscountForm = $shopBasketDiscountForm;
    }

    /**
     * @param BasketAdminService $basketAdminService
     */
    public function setBasketAdminService(BasketAdminService $basketAdminService): void
    {
        $this->basketAdminService = $basketAdminService;
    }

    /**
     * @param ShopUserService $shopUserService
     */
    public function setShopUserService(ShopUserService $shopUserService): void
    {
        $this->shopUserService = $shopUserService;
    }

    /**
     * @param ShopBasketDiscountTable $shopBasketDiscountTable
     */
    public function setShopBasketDiscountTable(ShopBasketDiscountTable $shopBasketDiscountTable): void
    {
        $this->shopBasketDiscountTable = $shopBasketDiscountTable;
    }

    /**
     * @param ShopBasketTablex $shopBasketTablex
     */
    public function setShopBasketTablex(ShopBasketTablex $shopBasketTablex): void
    {
        $this->shopBasketTablex = $shopBasketTablex;
    }

    /**
     *
     * @return ViewModel
     */
    public function basketItemsAction()
    {
        $viewModel = new ViewModel();
        if (!$this->userService->checkUserRoleAccessMin(3)) {
            return $viewModel;
        }
        $basketUnique = $this->params('basket_unique');
        if (empty($basketUnique)) {
            return $viewModel;
        }

        $shopBasket = $this->basketAdminService->getShopBasketEntitiesByBasketUnique($basketUnique);
        $viewModel->setVariable('shopBasket', $shopBasket);

        $this->layout('layout/clean');

        return $viewModel;
    }

    /**
     *
     * @return ViewModel
     */
    public function basketDiscountFormAction()
    {
        $viewModel = new ViewModel();
        if (!$this->userService->checkUserRoleAccessMin(2)) {
            return $viewModel;
        }

        $discountHash = ShopBasketDiscountEntity::generateDiscountHash($this->shopBasketDiscountTable);

        $this->shopBasketDiscountForm->setFormName('shop_basket_discount');
        $this->shopBasketDiscountForm->setShopBasketDiscountHash($discountHash);
        $this->shopBasketDiscountForm->init();
        $viewModel->setVariable('shopBasketDiscountForm', $this->shopBasketDiscountForm);

        $this->layout('layout/clean');

        return $viewModel;
    }

    /**
     *
     * @return ViewModel
     */
    public function basketDiscountSalesstaffFormAction()
    {
        $viewModel = new ViewModel();
        if (!$this->userService->checkUserRoleAccessMin(2)) {
            return $viewModel;
        }

        $this->shopBasketDiscountForm->setFormName('shop_basket_discount_salesstaff');
        $this->shopBasketDiscountForm->setWithUserId(true);
        /*
         * die gleichen Parameter müssen in BasketDiscountAdminController()->basketDiscountsAction()
         * wenn $postData['form_name'] == 'shop_basket_discount_salesstaff'
         */
        $this->shopBasketDiscountForm->setShopUserIdAssoc($this->shopUserService->getShopUserByRoleIdAssoc(101));
        $this->shopBasketDiscountForm->setIsMassAction(true);
        $this->shopBasketDiscountForm->init();
        $viewModel->setVariable('shopBasketDiscountForm', $this->shopBasketDiscountForm);

        $this->layout('layout/clean');

        return $viewModel;
    }

    /**
     *
     * @return ViewModel
     */
    public function basketDiscountC2cFormAction()
    {
        $viewModel = new ViewModel();
        if (!$this->userService->checkUserRoleAccessMin(2)) {
            return $viewModel;
        }

        $discountHash = ShopBasketDiscountEntity::generateDiscountHash($this->shopBasketDiscountTable, 'KDNR_');

        $this->shopBasketDiscountForm->setFormName('shop_basket_discount_c2c');
        $this->shopBasketDiscountForm->setShopBasketDiscountHash($discountHash);
        $this->shopBasketDiscountForm->setWithUserId(true);
        $this->shopBasketDiscountForm->setShopUserIdAssoc($this->shopUserService->getShopUserByGroupIdAssocX(1));
        $this->shopBasketDiscountForm->init();
        $viewModel->setVariable('shopBasketDiscountForm', $this->shopBasketDiscountForm);

        $this->layout('layout/clean');

        return $viewModel;
    }

    /**
     *
     * @return JsonModel
     */
    public function basketDiscountSwitchActiveAction()
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(2)) {
            return $jsonModel;
        }
        $shopBasketDiscountId = $this->params('shop_basket_discount_id');
        $jsonModel->setVariable('discount_id', $shopBasketDiscountId);
        $shopBasketDiscount = $this->basketAdminService->getShopBasketDiscountById($shopBasketDiscountId);
        if (empty($shopBasketDiscount)) {
            return $jsonModel;
        }
        $shopBasketDiscountEntity = new ShopBasketDiscountEntity();
        $shopBasketDiscountEntity->exchangeArray($shopBasketDiscount);
        if ($shopBasketDiscount['shop_basket_discount_active'] == 0) {
            if ($shopBasketDiscountEntity->canReaktivate($this->shopBasketTablex)) {
                $jsonModel->setVariable('switchResult', $this->basketAdminService->updateShopBasketDiscountActive($shopBasketDiscountId, 1));
                $jsonModel->setVariable('discountActive', 1);
            } else {
                $jsonModel->setVariable('switchResult', 0);
                $jsonModel->setVariable('discountActive', 0);
                $jsonModel->setVariable('message', 'Rabatt-Code kann nicht aktiviert werden.');
            }
        } else {
            $jsonModel->setVariable('switchResult', $this->basketAdminService->updateShopBasketDiscountActive($shopBasketDiscountId, 0));
            $jsonModel->setVariable('discountActive', 0);
        }
        return $jsonModel;
    }

    /**
     * Aktuell nur im POS genutzt.
     * @return ViewModel
     */
    public function basketWidgetCompactAction()
    {
        $viewModel = new ViewModel();
        if (!$this->userService->checkUserRoleAccessMin(2) && !$this->userService->checkUserRoleAccess(101)) {
            return $viewModel;
        }
        if (($xshopBasketEntity = $this->basketService->checkBasket()) instanceof XshopBasketEntity) {
            $viewModel->setVariables([
                'basketData' => $xshopBasketEntity->getStorageItems(),
                'xshopBasketEntity' => $xshopBasketEntity,
                'articleAmountSum' => $xshopBasketEntity->getArticleAmountSum(),
                'articlePriceTotalSum' => $xshopBasketEntity->getArticlePriceTotalSum(),
            ]);
        }

        $this->layout('layout/clean');

        return $viewModel;
    }

}
