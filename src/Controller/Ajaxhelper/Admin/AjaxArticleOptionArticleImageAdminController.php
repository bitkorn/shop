<?php

namespace Bitkorn\Shop\Controller\Ajaxhelper\Admin;

use Bitkorn\Images\Entity\Image\ImagesEntity;
use Bitkorn\Shop\Service\Article\ShopArticleImageService;
use Bitkorn\Shop\Tablex\Article\ShopArticleImageTablex;
use Laminas\View\Model\ViewModel;
use Laminas\View\Model\JsonModel;
use Bitkorn\Shop\Controller\AbstractShopController;

/**
 *
 * @author allapow
 */
class AjaxArticleOptionArticleImageAdminController extends AbstractShopController
{

    /**
     *
     * @var ShopArticleImageService
     */
    protected $shopArticleImageService;

    /**
     * @param ShopArticleImageService $shopArticleImageService
     */
    public function setShopArticleImageService(ShopArticleImageService $shopArticleImageService): void
    {
        $this->shopArticleImageService = $shopArticleImageService;
    }

    /**
     *
     * @return ViewModel
     */
    public function formAddArticleImageRelationsAction()
    {
        $viewModel = new ViewModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            return $viewModel;
        }
        $articleId = (int)$this->params('article_id');
        $optionItemId = (int)$this->params('option_item_id');

        $viewModel->setVariable('optionItemId', $optionItemId);

        $imagesData = $this->shopArticleImageService->getImagesExcludeForShopArticleOption($articleId, $optionItemId);
        $viewModel->setVariable('images', $imagesData);

        $imageEntity = new ImagesEntity();
        $imageEntity->exchangeArrayImages($imagesData);
        $viewModel->setVariable('imageEntity', $imageEntity);

        $this->layout('layout/clean');
        return $viewModel;
    }
}
