<?php

namespace Bitkorn\Shop\Controller\Ajaxhelper\Admin;

use Bitkorn\Shop\Service\Article\ShopArticleService;
use Bitkorn\Shop\Service\Article\ShopArticleSizeService;
use Laminas\View\Model\ViewModel;
use Laminas\View\Model\JsonModel;
use Bitkorn\Shop\Controller\AbstractShopController;

/**
 *
 * @author allapow
 */
class AjaxArticleSizeAdminController extends AbstractShopController
{

    /**
     * @var ShopArticleSizeService
     */
    protected $shopArticleSizeService;

    /**
     * @param ShopArticleSizeService $shopArticleSizeService
     */
    public function setShopArticleSizeService(ShopArticleSizeService $shopArticleSizeService): void
    {
        $this->shopArticleSizeService = $shopArticleSizeService;
    }

    /**
     *
     * @return JsonModel
     */
    public function updateArticleSizeItemAction()
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            return $jsonModel;
        }
        $articleSizeItemId = (int)$this->params('shop_article_size_item_id');
        $articleSizeItemType = $this->params('shop_article_size_item_type');
        $articleSizeItemValue = $this->params('shop_article_size_item_value');
        if (empty($articleSizeItemId) || empty($articleSizeItemType) || !isset($articleSizeItemValue)) {
            $this->getResponse()->setStatusCode(\Laminas\Http\PhpEnvironment\Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setVariable('articleSizeItemId', $articleSizeItemId);
        $jsonModel->setVariable('articleSizeItemType', $articleSizeItemType);
        $jsonModel->setVariable('articleSizeItemValue', $articleSizeItemValue);

        if ($this->shopArticleSizeService->updateShopArticleSizeItem($articleSizeItemId, $articleSizeItemType, $articleSizeItemValue) >= 0) {
            $jsonModel->setVariable('status', 1);
        } else {
            $jsonModel->setVariable('status', 0);
        }

        return $jsonModel;
    }

}
