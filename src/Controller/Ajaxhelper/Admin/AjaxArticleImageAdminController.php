<?php

namespace Bitkorn\Shop\Controller\Ajaxhelper\Admin;

use Bitkorn\Images\Entity\Image\ImagesEntity;
use Bitkorn\Images\Service\Image\ImageService;
use Bitkorn\Shop\Service\Article\ShopArticleImageService;
use Laminas\View\Model\ViewModel;
use Laminas\View\Model\JsonModel;
use Bitkorn\Shop\Controller\AbstractShopController;

/**
 *
 * @author allapow
 */
class AjaxArticleImageAdminController extends AbstractShopController
{

    /**
     * @var ShopArticleImageService
     */
    protected $shopArticleImageService;

    /**
     * @param ShopArticleImageService $shopArticleImageService
     */
    public function setShopArticleImageService(ShopArticleImageService $shopArticleImageService): void
    {
        $this->shopArticleImageService = $shopArticleImageService;
    }

    /**
     *
     * @return ViewModel
     */
    public function formAddArticleImageRelationsAction()
    {
        $viewModel = new ViewModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            return $viewModel;
        }
        $articleId = (int)$this->params('article_id');

        $viewModel->setVariable('articleId', $articleId);

        $imagesData = $this->shopArticleImageService->getImagesExcludeForShopArticle($articleId);
        $viewModel->setVariable('images', $imagesData);

        $imageEntity = new ImagesEntity();
        $imageEntity->exchangeArrayImages($imagesData);
        $viewModel->setVariable('imageEntity', $imageEntity);

        $this->layout('layout/clean');
        return $viewModel;
    }

}
