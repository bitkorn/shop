<?php

namespace Bitkorn\Shop\Controller\Ajaxhelper;

use Bitkorn\Shop\Entity\User\ShopUserAddressEntity;
use Bitkorn\Shop\Form\User\ShopUserAddressForm;
use Bitkorn\Shop\Service\User\ShopUserService;
use Bitkorn\Shop\Table\User\ShopUserAddressTable;
use Laminas\Http\PhpEnvironment\Response;
use Laminas\View\Model\ViewModel;
use Laminas\View\Model\JsonModel;
use Bitkorn\Shop\Controller\AbstractShopController;

/**
 *
 * @author allapow
 */
class AjaxUserController extends AbstractShopController
{

    /**
     * @var ShopUserService
     */
    protected $shopUserService;

    /**
     * @var ShopUserAddressForm
     */
    protected $shopUserAddressForm;

    /**
     * @var ShopUserAddressTable
     */
    protected $shopUserAddressTable;

    /**
     * @param ShopUserService $shopUserService
     */
    public function setShopUserService(ShopUserService $shopUserService): void
    {
        $this->shopUserService = $shopUserService;
    }

    /**
     * @param ShopUserAddressForm $shopUserAddressForm
     */
    public function setShopUserAddressForm(ShopUserAddressForm $shopUserAddressForm): void
    {
        $this->shopUserAddressForm = $shopUserAddressForm;
    }

    /**
     * @param ShopUserAddressTable $shopUserAddressTable
     */
    public function setShopUserAddressTable(ShopUserAddressTable $shopUserAddressTable): void
    {
        $this->shopUserAddressTable = $shopUserAddressTable;
    }

    /**
     *
     * @return ViewModel
     */
    public function addressDisplayAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            return '';
        }
        $viewModel = new ViewModel();

        $userAddresses = $this->shopUserService->getShopUserAddressesByUserId($this->userService->getUserUuid());
        $viewModel->setVariable('userAddresses', $userAddresses);
        $viewModel->setVariable('userAddressesCount', count($userAddresses));
        $existShipmentAddress = false;
        foreach ($userAddresses as $userAddress) {
            if ($userAddress['shop_user_address_type'] == 'shipment') {
                $existShipmentAddress = true;
            }
        }
        $viewModel->setVariable('existShipmentAddress', $existShipmentAddress);

        $this->layout('layout/clean');

        return $viewModel;
    }

    /**
     *
     * @return ViewModel
     */
    public function addressFormAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            return '';
        }
        $addressId = $this->params('address_id');
        $viewModel = new ViewModel();

        if (!empty($addressId)) {
            $userAddress = $this->shopUserService->getShopUserAddressById($addressId);
            if ($this->userService->getUserUuid() != $userAddress['user_uuid']) {
                $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
                return $viewModel;
            }
        }

        if (!empty($addressId)) {
//            $userAddressForm->setShopUserAddressIdRequired(true); // is in AJAX Form scheiss egal
        }

        if ((!empty($userAddress) && $userAddress['shop_user_address_tax_id'] == 'shipment')
//            || (empty($userAddress) && $this->getShopUserAddressTable()->countShopUserAddresses($userAddress) >= 1)
        ) {
            $this->shopUserAddressForm->setTaxIdEnabled(false);
        }

        $this->shopUserAddressForm->init();
        if (!empty($addressId) && !empty($userAddress)) {
            $this->shopUserAddressForm->setData($userAddress);
        } else {
            $this->shopUserAddressForm->setData(['user_uuid' => $this->userService->getUserUuid()]);
        }

        $viewModel->setVariable('shopUserAddressForm', $this->shopUserAddressForm);

        $this->layout('layout/clean');

        return $viewModel;
    }

    /**
     *
     * @return string|JsonModel
     */
    public function addressDeleteAction()
    {
        $addressId = (int)$this->params('address_id');
        if (!$this->userService->checkUserRoleAccessMin(5) || empty($addressId)) {
            return '';
        }
        $jsonModel = new JsonModel();
        $userAddress = $this->shopUserService->getShopUserAddressById($addressId);
        if ($this->userService->getUserUuid() != $userAddress['user_uuid']) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $userAddressEntity = new ShopUserAddressEntity();
        $userAddressEntity->flipMapping();
        $userAddressEntity->exchangeArray($userAddress);
        $deleteResult = $userAddressEntity->delete($this->shopUserAddressTable);
        if ($deleteResult > 0) {
            $jsonModel->setVariable('message', 'delete success');
        } else {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
        }

        return $jsonModel;
    }

}
