<?php

namespace Bitkorn\Shop\Controller\Ajaxhelper;

use Bitkorn\Shop\Service\Article\ShopArticleService;
use Laminas\Http\PhpEnvironment\Response;
use Laminas\View\Model\ViewModel;
use Laminas\View\Model\JsonModel;
use Bitkorn\Shop\Controller\AbstractShopController;

/**
 *
 * @author allapow
 */
class AjaxArticleController extends AbstractShopController
{
    /**
     * @var ShopArticleService
     */
    protected $shopArticleService;

    /**
     * @param ShopArticleService $shopArticleService
     */
    public function setShopArticleService(ShopArticleService $shopArticleService): void
    {
        $this->shopArticleService = $shopArticleService;
    }

    /**
     *
     * @return ViewModel|string
     */
    public function selectGroupableArticlesFormAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            return '';
        }
        $articleId = $this->params('article_id');
        if (empty($articleId)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return '';
        }
        $viewModel = new ViewModel();

        $articles = $this->shopArticleService->getShopArticleStandardGroupableExcludeArticleId($articleId);
        $viewModel->setVariable('articles', $articles);

        $this->layout('layout/clean');

        return $viewModel;
    }

    /**
     *
     * @return ViewModel
     */
    public function selectGroupableArticlesForSizeGroupFormAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            return '';
        }
        $articleId = $this->params('article_id');
        if (empty($articleId)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return '';
        }
        $sizeGroupId = $this->params('size_group_id');
        $viewModel = new ViewModel();

        $articles = $this->shopArticleService->getShopArticleForSizeGroup($articleId, $sizeGroupId);
        $viewModel->setVariable('articles', $articles);

        $this->layout('layout/clean');

        return $viewModel;
    }

}
