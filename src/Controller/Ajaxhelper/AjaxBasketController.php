<?php

namespace Bitkorn\Shop\Controller\Ajaxhelper;

use Bitkorn\Shop\Entity\Article\ShopArticleOptionsEntity;
use Bitkorn\Shop\Entity\Article\TypeDelivery\ShopArticleLengthcutDefEntity;
use Bitkorn\Shop\Service\Article\ShopArticleParam2dService;
use Bitkorn\Shop\Service\Article\ShopArticleService;
use Bitkorn\Shop\Service\Article\ShopArticleStockService;
use Bitkorn\Shop\Service\Article\TypeDelivery\ShopArticleLengthcutDefService;
use Bitkorn\Shop\Service\Basket\BasketService;
use Bitkorn\Shop\Service\Shipping\ShippingServiceInterface;
use Bitkorn\Shop\Table\Article\Option\ShopArticleOptionArticleRelationTable;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\PhpEnvironment\Response;
use Laminas\View\Model\ViewModel;
use Laminas\View\Model\JsonModel;
use Bitkorn\Shop\Controller\AbstractShopController;

/**
 *
 * @author allapow
 */
class AjaxBasketController extends AbstractShopController
{

    /**
     * @var ShopArticleStockService
     */
    protected $shopArticleStockService;

    /**
     *
     * @var ShippingServiceInterface
     */
    protected $shippingService;

    /**
     *
     * @var ShopArticleService
     */
    protected $shopArticleService;

    /**
     * @var ShopArticleParam2dService
     */
    protected $shopArticleParam2dService;

    /**
     *
     * @var ShopArticleOptionsEntity
     */
    protected $shopArticleOptionsEntity;

    /**
     *
     * @var ShopArticleOptionArticleRelationTable
     */
    protected $shopArticleOptionArticleRelationTable;

    /**
     * @var ShopArticleLengthcutDefService
     */
    protected $shopArticleLengthcutDefService;

    /**
     * @param ShopArticleStockService $shopArticleStockService
     */
    public function setShopArticleStockService(ShopArticleStockService $shopArticleStockService): void
    {
        $this->shopArticleStockService = $shopArticleStockService;
    }

    /**
     *
     * @param ShippingServiceInterface $shippingService
     */
    public function setShippingService(ShippingServiceInterface $shippingService)
    {
        $this->shippingService = $shippingService;
    }

    /**
     * @param ShopArticleService $shopArticleService
     */
    public function setShopArticleService(ShopArticleService $shopArticleService): void
    {
        $this->shopArticleService = $shopArticleService;
    }

    /**
     * @param ShopArticleParam2dService $shopArticleParam2dService
     */
    public function setShopArticleParam2dService(ShopArticleParam2dService $shopArticleParam2dService): void
    {
        $this->shopArticleParam2dService = $shopArticleParam2dService;
    }

    /**
     * @param ShopArticleOptionsEntity $shopArticleOptionsEntity
     */
    public function setShopArticleOptionsEntity(ShopArticleOptionsEntity $shopArticleOptionsEntity): void
    {
        $this->shopArticleOptionsEntity = $shopArticleOptionsEntity;
    }

    /**
     *
     * @param ShopArticleOptionArticleRelationTable $shopArticleOptionArticleRelationTable
     */
    public function setShopArticleOptionArticleRelationTable(ShopArticleOptionArticleRelationTable $shopArticleOptionArticleRelationTable)
    {
        $this->shopArticleOptionArticleRelationTable = $shopArticleOptionArticleRelationTable;
    }

    /**
     * @param ShopArticleLengthcutDefService $shopArticleLengthcutDefService
     */
    public function setShopArticleLengthcutDefService(ShopArticleLengthcutDefService $shopArticleLengthcutDefService): void
    {
        $this->shopArticleLengthcutDefService = $shopArticleLengthcutDefService;
    }

    /**
     *
     * @return ViewModel
     */
    public function basketWidgetLineAction()
    {
        $viewModel = new ViewModel();

        if (!empty($xshopBasketEntity = $this->basketService->checkBasket())) {
            $viewModel->setVariables([
                'basketData' => $xshopBasketEntity->getStorageItems(),
                'articleAmountSum' => $xshopBasketEntity->getArticleAmountSum(),
                'articlePriceTotalSum' => $xshopBasketEntity->getArticlePriceTotalSum(),
            ]);
        }

        $this->layout('layout/clean');

        return $viewModel;
    }

    /**
     * Used in frontend AND backend (POS)
     * @return JsonModel
     */
    public function addToBasketAction()
    {
        $jsonModel = new JsonModel();
        $request = $this->getRequest();
        if (!$request->isPost() || !$request instanceof Request) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $articleId = intval($this->params('article_id', 0));
        $articleAmount = intval($this->params('article_amount', 0));
        if (empty($articleId) || $articleAmount < 0) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $postArray = $request->getPost()->toArray();

        if (!empty($articleOptions = filter_input(INPUT_POST, 'articleOptions', FILTER_SANITIZE_NUMBER_INT, FILTER_REQUIRE_ARRAY))) {
            $optionArticleRelations = $this->shopArticleOptionArticleRelationTable->getShopArticleOptionArticleRelations($articleId);
            if (count($articleOptions) != count($optionArticleRelations)) {
                $jsonModel->setVariables([
                    'message' => 'Alle Optionen müssen gewählt werden!',
                    'status' => 0
                ]);
                return $jsonModel;
            }
            $existOk = true;
            if (!empty($articleOptions)) {
                foreach ($articleOptions as $articleOption) {
                    if (!in_array($articleOption['optiondefid'], $optionArticleRelations)) {
                        $existOk = false;
                    }
                }
            } else {
                $articleOptions = [];
            }
            if (!$existOk) {
                $jsonModel->setVariables([
                    'message' => 'You are ScriptKiddy',
                    'status' => 0
                ]);
                $this->getResponse()->setStatusCode(Response::STATUS_CODE_409);
                return $jsonModel;
            }
            $this->shopArticleOptionsEntity->exchangeArrayOptions($articleOptions);
        }

        $param2dItemId = 0;
        $lengthcutDefEntity = new ShopArticleLengthcutDefEntity();
        if (isset($postArray['param2dItemId']) && isset($postArray['lengthcutLength']) && isset($postArray['lengthcutAmount'])) {
            $param2dItemId = intval($postArray['param2dItemId']);
            $lengthcutLength = intval($postArray['lengthcutLength']);
            $lengthcutAmount = intval($postArray['lengthcutAmount']);
            $lengthcutDef = $this->shopArticleLengthcutDefService->getLengthcutDefForArticleId($articleId);
            $param2dItem = $this->shopArticleParam2dService->getShopArticleParam2dItemForParam2dItemId($param2dItemId);
            if (empty($lengthcutDef) || empty($param2dItem)) {
                $this->getResponse()->setStatusCode(Response::STATUS_CODE_409);
                return $jsonModel;
            }
            $lengthcutDefEntity->setLengthPieces($lengthcutLength);
            $lengthcutDefEntity->setQuantity($lengthcutAmount);
            if (!$lengthcutDefEntity->exchangeArrayFromDatabase($lengthcutDef) || !$lengthcutDefEntity->computeQuantityCuts()) {
                $this->getResponse()->setStatusCode(Response::STATUS_CODE_409);
                return $jsonModel;
            }
        }

        /*
         * check article stock availability
         */
        if(!$this->shopArticleStockService->checkStockAvailability($articleId, $articleAmount, $this->shopArticleOptionsEntity, $param2dItemId, $lengthcutDefEntity)) {
            $jsonModel->setVariables([
                'message' => 'Die gewünschte Menge des Artikels ist nicht verfügbar.',
                'status' => 0
            ]);
            return $jsonModel;
        }

        if (!$this->basketService->existShopBasket()) {
            $this->basketService->createEmptyBasket($this->userService->getUserUuid());
        }
        if (
            $this->basketService->saveOrUpdateShopBasketItem(
                $articleId, $articleAmount, $this->shopArticleOptionsEntity->getShopArticleOptionsJson(), $param2dItemId, $lengthcutDefEntity->getDefinitionJson()
            ) <= 0
        ) {
            $jsonModel->setVariables([
                'message' => 'Es gab einen Fehler. Bitte kontaktieren Sie den Support.',
                'status' => 0
            ]);
            return $jsonModel;
        }

        $jsonModel->setVariables([
            'article_id' => $articleId,
            'article_amount' => $articleAmount,
            'status' => 1
        ]);

        $this->basketService->refreshBasket();
        $jsonModel->setVariable('articleAmountSum', $this->basketService->getXshopBasketEntity()->getArticleAmountSum());

        return $jsonModel;
    }

    public function deleteBasketItemAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->basketService->existShopBasket()) {
            return $jsonModel;
        }
        $basketItemId = intval($this->params('basket_item_id'));
        if(!$this->basketService->belongShopBasketItemToBasket($basketItemId)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if(!$this->basketService->deleteShopBasketItem($basketItemId)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
        }
        $this->basketService->refreshBasket();
        return $jsonModel;
    }

    public function deleteBasketAction()
    {
        $jsonModel = new JsonModel();
        if (!$this->basketService->existShopBasket()) {
            return $jsonModel;
        }
        $deleteCountBasket = $this->basketService->deleteShopBasket($this->basketService->getBasketUnique());
        $this->basketService->createEmptyBasket();

        return $jsonModel;
    }

    /**
     * Calls \Bitkorn\Shop\View\Helper\Article\Stock\ShopArticleStockWidget
     * @return ViewModel
     */
    public function articleStockWidgetAction()
    {
        $articleId = $this->params('article_id');
        $viewModel = new ViewModel();
        $this->layout('layout/clean');
        $articleOptions = filter_input_array(INPUT_POST);

        if (!$articleId) {
            return $viewModel;
        }
        $viewModel->setVariable('articleId', $articleId);


        $request = $this->getRequest();
        if ($request->isPost() && $request instanceof Request) {
            $postData = $request->getPost()->toArray();
            if (!empty($articleOptions['articleOptions'])) {
                $viewModel->setVariable('articleOptions', $articleOptions['articleOptions']);
            }
        }

        return $viewModel;
    }

}
