<?php

namespace Bitkorn\Shop\Controller\Ajaxhelper\Statistic;

use Bitkorn\Shop\Table\Article\Size\ShopArticleSizeArchiveTable;
use Bitkorn\Shop\Table\Article\Size\ShopArticleSizeGroupTable;
use Laminas\View\Model\JsonModel;
use Bitkorn\Shop\Controller\AbstractShopController;

/**
 *
 * @author Torsten Brieskorn
 */
class AjaxStatisticSizeArchiveController extends AbstractShopController
{

    /**
     *
     * @var ShopArticleSizeArchiveTable
     */
    private $shopArticleSizeArchiveTable;

    /**
     * @var ShopArticleSizeGroupTable
     */
    protected $shopArticleSizeGroupTable;

    /**
     *
     * @param ShopArticleSizeArchiveTable $shopArticleSizeArchiveTable
     */
    public function setShopArticleSizeArchiveTable(ShopArticleSizeArchiveTable $shopArticleSizeArchiveTable)
    {
        $this->shopArticleSizeArchiveTable = $shopArticleSizeArchiveTable;
    }

    /**
     *
     * @param ShopArticleSizeGroupTable $shopArticleSizeGroupTable
     */
    public function setShopArticleSizeGroupTable(ShopArticleSizeGroupTable $shopArticleSizeGroupTable)
    {
        $this->shopArticleSizeGroupTable = $shopArticleSizeGroupTable;
    }

    /**
     *
     * @return JsonModel
     */
    public function sizeArchiveAction()
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(3)) {
//            return $jsonModel;
        }
        $timestringFrom = $this->params('date_from', '01.01.1970');
        $timestringTo = $this->params('date_to', date('d.m.Y'));
//        $datetimeFrom = \DateTime::createFromFormat('Y-m-d', $timestringFrom);
//        $datetimeTo = \DateTime::createFromFormat('Y-m-d', $timestringTo);
        try {
            $timestampFrom = (new \DateTime($timestringFrom))->getTimestamp();
            $timestampTo = (new \DateTime($timestringTo))->getTimestamp();
        } catch (\Exception $ex) {
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() ' . $ex->getMessage());
        }
        $jsonModel->setVariables(['timestamp_from' => $timestampFrom, 'timestamp_to' => $timestampTo]);

        $sizeArchive = $this->shopArticleSizeArchiveTable->getShopArticleSizeArchiveCountFromToGrouped($timestampFrom, $timestampTo);
        $jsonModel->setVariable('sizeArchive', $sizeArchive);
        $sizeGroupIdAssoc = $this->shopArticleSizeGroupTable->getShopArticleSizeGroupIdAssoc();
        $jsonModel->setVariable('sizeGroupIdAssoc', $sizeGroupIdAssoc);
        $jsonModel->setVariable('success', 1);
        return $jsonModel;
    }
}
