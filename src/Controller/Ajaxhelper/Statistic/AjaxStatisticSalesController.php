<?php

namespace Bitkorn\Shop\Controller\Ajaxhelper\Statistic;

use Laminas\View\Model\JsonModel;
use Bitkorn\Shop\Controller\AbstractShopController;

/**
 *
 * @author Torsten Brieskorn
 */
class AjaxStatisticSalesController extends AbstractShopController
{

    /**
     *
     * @return JsonModel
     */
    public function salesAction()
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(3)) {
            return $jsonModel;
        }

        return $jsonModel;
    }

}
