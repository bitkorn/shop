<?php

namespace Bitkorn\Shop\Controller\Ajaxhelper;

use Bitkorn\Images\Entity\Image\ImagesEntity;
use Bitkorn\Shop\Service\Article\ShopArticleImageService;
use Bitkorn\Shop\Service\Article\ShopArticleOptionService;
use Laminas\Http\PhpEnvironment\Response;
use Laminas\View\Model\ViewModel;
use Bitkorn\Shop\Controller\AbstractShopController;

/**
 *
 * @author allapow
 */
class AjaxArticleOptionController extends AbstractShopController
{
    /**
     * @var ShopArticleOptionService
     */
    protected $shopArticleOptionService;

    /**
     * @var ShopArticleImageService
     */
    protected $shopArticleImageService;

    /**
     * @param ShopArticleOptionService $shopArticleOptionService
     */
    public function setShopArticleOptionService(ShopArticleOptionService $shopArticleOptionService): void
    {
        $this->shopArticleOptionService = $shopArticleOptionService;
    }

    /**
     * @param ShopArticleImageService $shopArticleImageService
     */
    public function setShopArticleImageService(ShopArticleImageService $shopArticleImageService): void
    {
        $this->shopArticleImageService = $shopArticleImageService;
    }

    /**
     * 
     * @return ViewModel
     */
    public function optionImageContainerAction()
    {
        $viewModel = new ViewModel();
        $this->layout('layout/clean');
        $articleId = $this->params('article_id');
        $optionItemId = $this->params('option_item_id');
        if (empty($articleId) || !isset($optionItemId)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $viewModel;
        }

        $shopArticleImagesData = $this->shopArticleOptionService->getShopArticleOptionItemArticleImages($optionItemId, $articleId);
        if (empty($shopArticleImagesData) && empty($optionItemId)) {
            $shopArticleImagesData = $this->shopArticleImageService->getShopArticleImages($articleId);
        } elseif (empty($shopArticleImagesData)) {
            exit();
        }
        $viewModel->setVariable('shopArticleImagesData', $shopArticleImagesData);

        $imageEntity = new ImagesEntity();
        $imageEntity->exchangeArrayImages($shopArticleImagesData);
        $viewModel->setVariable('imageEntity', $imageEntity);

        return $viewModel;
    }

}
