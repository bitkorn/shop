<?php

namespace Bitkorn\Shop\Controller\Common;

use Bitkorn\Shop\Service\Basket\BasketAdminService;
use Bitkorn\Shop\Service\Document\DocumentDeliveryService;
use Bitkorn\Shop\Service\Document\DocumentInvoiceService;
use Bitkorn\Shop\Service\Document\DocumentOrderService;
use Bitkorn\Trinket\Service\Filesystem\FolderTool;
use Laminas\View\Model\JsonModel;
use Bitkorn\Shop\Controller\AbstractShopController;

/**
 *
 * @author allapow
 */
class BasketuniquePdfController extends AbstractShopController
{

    /**
     * @var BasketAdminService
     */
    protected $basketAdminService;

    /**
     * @var DocumentInvoiceService
     */
    protected $documentInvoiceService;

    /**
     * @var DocumentDeliveryService
     */
    protected $documentDeliveryService;

    /**
     * @var DocumentOrderService
     */
    protected $documentOrderService;

    /**
     * @var FolderTool
     */
    protected $folderTool;

    /**
     * @param BasketAdminService $basketAdminService
     */
    public function setBasketAdminService(BasketAdminService $basketAdminService): void
    {
        $this->basketAdminService = $basketAdminService;
    }

    /**
     * @param DocumentInvoiceService $documentInvoiceService
     */
    public function setDocumentInvoiceService(DocumentInvoiceService $documentInvoiceService): void
    {
        $this->documentInvoiceService = $documentInvoiceService;
    }

    /**
     * @param DocumentDeliveryService $documentDeliveryService
     */
    public function setDocumentDeliveryService(DocumentDeliveryService $documentDeliveryService): void
    {
        $this->documentDeliveryService = $documentDeliveryService;
    }

    /**
     * @param DocumentOrderService $documentOrderService
     */
    public function setDocumentOrderService(DocumentOrderService $documentOrderService): void
    {
        $this->documentOrderService = $documentOrderService;
    }

    /**
     * @param FolderTool $folderTool
     */
    public function setFolderTool(FolderTool $folderTool): void
    {
        $this->folderTool = $folderTool;
    }

    /**
     * @return JsonModel|int
     */
    public function streamInvoicePdfAction()
    {
        $basketUnique = filter_var($this->params('basket_unique'), FILTER_SANITIZE_STRING);
        $documentInvoice = $this->documentInvoiceService->getShopDocumentInvoiceByBasketUnique($basketUnique);
        if (empty($documentInvoice)) {
            return -1;
        }
        $shopBasket = $this->basketAdminService->getShopBasketEntitiesByBasketUnique($basketUnique, 1)[0];
        if(empty($shopBasket) || !is_array($shopBasket)) {
            return -1;
        }

        $folder = $this->folderTool->computeBrandDateFolder($this->config['bitkorn_shop']['document_path_absolute'], 'invoice',
            date('Y', $shopBasket['shop_basket_time_create']), date('m', $shopBasket['shop_basket_time_create']));
        if (file_exists($folder . '/' . $documentInvoice['shop_document_invoice_filename'])) {
            header('Content-type: application/pdf');
            header('Content-Disposition: inline; filename="invoice.pdf"'); // inline || attachment
            header('Content-length: ' . filesize($folder . '/' . $documentInvoice['shop_document_invoice_filename']));
            header('Content-Transfer-Encoding: binary');
            header('Accept-Ranges: bytes');
            $resource = fopen('php://output', 'w');
            $rawPdf = file_get_contents($folder . '/' . $documentInvoice['shop_document_invoice_filename']);
            fwrite($resource, $rawPdf);
            exit();
        }

        $jsonModel = new JsonModel();
        $jsonModel->setVariable('documentId', $this->params('document_id'));
        return $jsonModel;
    }

    /**
     * @return JsonModel|int
     */
    public function streamOrderPdfAction()
    {
        $basketUnique = filter_var($this->params('basket_unique'), FILTER_SANITIZE_STRING);
        $documentOrder = $this->documentOrderService->getShopDocumentOrderByBasketUnique($basketUnique);
        if (empty($documentOrder)) {
            return -1;
        }
        $shopBasket = $this->basketAdminService->getShopBasketEntitiesByBasketUnique($basketUnique, 1)[0];
        if(empty($shopBasket) || !is_array($shopBasket)) {
            return -1;
        }

        $folder = $this->folderTool->computeBrandDateFolder($this->config['bitkorn_shop']['document_path_absolute'],'order',
            date('Y', $shopBasket['shop_basket_time_create']), date('m', $shopBasket['shop_basket_time_create']));
        if (file_exists($folder . '/' . $documentOrder['shop_document_order_filename'])) {
            header('Content-type: application/pdf');
            header('Content-Disposition: inline; filename="order.pdf"'); // inline || attachment
            header('Content-length: ' . filesize($folder . '/' . $documentOrder['shop_document_order_filename']));
            header('Content-Transfer-Encoding: binary');
            header('Accept-Ranges: bytes');
            $resource = fopen('php://output', 'w');
            $rawPdf = file_get_contents($folder . '/' . $documentOrder['shop_document_order_filename']);
            fwrite($resource, $rawPdf);
            exit();
        }

        $jsonModel = new JsonModel();
        $jsonModel->setVariable('documentId', $this->params('document_id'));
        return $jsonModel;
    }

    /**
     * @return JsonModel|int
     */
    public function streamDeliveryPdfAction()
    {
        $basketUnique = filter_var($this->params('basket_unique'), FILTER_SANITIZE_STRING);
        $document = $this->documentDeliveryService->getShopDocumentDeliveryByBasketUnique($basketUnique);
        if (empty($document)) {
            return -1;
        }
        $shopBasket = $this->basketAdminService->getShopBasketEntitiesByBasketUnique($basketUnique, 1)[0];
        if(empty($shopBasket) || !is_array($shopBasket)) {
            return -1;
        }

        $folder = $this->folderTool->computeBrandDateFolder($this->config['bitkorn_shop']['document_path_absolute'], 'shipment',
            date('Y', $shopBasket['shop_basket_time_create']), date('m', $shopBasket['shop_basket_time_create']));
        if (file_exists($folder . '/' . $document['shop_document_delivery_filename'])) {
            header('Content-type: application/pdf');
            header('Content-Disposition: inline; filename="delivery.pdf"'); // inline || attachment
            header('Content-length: ' . filesize($folder . '/' . $document['shop_document_delivery_filename']));
            header('Content-Transfer-Encoding: binary');
            header('Accept-Ranges: bytes');
            $resource = fopen('php://output', 'w');
            $rawPdf = file_get_contents($folder . '/' . $document['shop_document_delivery_filename']);
            fwrite($resource, $rawPdf);
            exit();
        }

        $jsonModel = new JsonModel();
        $jsonModel->setVariable('documentId', $this->params('document_id'));
        return $jsonModel;
    }

}
