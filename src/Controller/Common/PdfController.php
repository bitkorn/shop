<?php

namespace Bitkorn\Shop\Controller\Common;

use Bitkorn\Shop\Service\Basket\BasketAdminService;
use Bitkorn\Shop\Service\Document\DocumentDeliveryService;
use Bitkorn\Shop\Service\Document\DocumentInvoiceService;
use Bitkorn\Shop\Service\Document\DocumentOrderService;
use Bitkorn\Trinket\Service\Filesystem\FolderTool;
use Laminas\View\Model\JsonModel;
use Bitkorn\Shop\Controller\AbstractShopController;

/**
 *
 * @author allapow
 */
class PdfController extends AbstractShopController
{

    protected BasketAdminService $basketAdminService;
    protected DocumentInvoiceService $documentInvoiceService;
    protected DocumentDeliveryService $documentDeliveryService;
    protected DocumentOrderService $documentOrderService;
    protected FolderTool $folderTool;

    public function setBasketAdminService(BasketAdminService $basketAdminService): void
    {
        $this->basketAdminService = $basketAdminService;
    }

    public function setDocumentInvoiceService(DocumentInvoiceService $documentInvoiceService): void
    {
        $this->documentInvoiceService = $documentInvoiceService;
    }

    public function setDocumentDeliveryService(DocumentDeliveryService $documentDeliveryService): void
    {
        $this->documentDeliveryService = $documentDeliveryService;
    }

    public function setDocumentOrderService(DocumentOrderService $documentOrderService): void
    {
        $this->documentOrderService = $documentOrderService;
    }

    public function setFolderTool(FolderTool $folderTool): void
    {
        $this->folderTool = $folderTool;
    }

    /**
     * @return JsonModel|int
     */
    public function streamInvoicePdfAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            return -1;
        }
        $documentInvoice = $this->documentInvoiceService->getShopDocumentInvoiceById(intval($this->params('document_id')));
        if (empty($documentInvoice)) {
            return -1;
        }
        /**
         * check user
         */
        $shopBasket = $this->basketAdminService->getShopBasketEntitiesByBasketUnique($documentInvoice['shop_basket_unique'], 1);
        if (empty($shopBasket[0]) || ($shopBasket[0]['user_uuid'] != $this->userService->getUserUuid() && !$this->userService->checkUserRoleAccessMin(3))) {
            return -1;
        }

        $folder = $this->folderTool->getBrandDateFolder($this->config['bitkorn_shop']['document_path_absolute'], 'invoice',
            date('Y', $shopBasket[0]['shop_basket_time_create']), date('m', $shopBasket[0]['shop_basket_time_create']));
        if (file_exists($folder . '/' . $documentInvoice['shop_document_invoice_filename'])) {
            header('Content-type: application/pdf');
            header('Content-Disposition: inline; filename="invoice.pdf"'); // inline || attachment
            header('Content-length: ' . filesize($folder . '/' . $documentInvoice['shop_document_invoice_filename']));
            header('Content-Transfer-Encoding: binary');
            header('Accept-Ranges: bytes');
            $resource = fopen('php://output', 'w');
            $rawPdf = file_get_contents($folder . '/' . $documentInvoice['shop_document_invoice_filename']);
            fwrite($resource, $rawPdf);
            exit();
        }

        $jsonModel = new JsonModel();
        $jsonModel->setVariable('documentId', $this->params('document_id'));
        return $jsonModel;
    }

    /**
     * @return JsonModel|int
     */
    public function streamOrderPdfAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            return -1;
        }
        $documentOrder = $this->documentOrderService->getShopDocumentOrderById(intval($this->params('document_id')));
        if (empty($documentOrder)) {
            return -1;
        }
        /**
         * check user
         */
        $shopBasket = $this->basketAdminService->getShopBasketEntitiesByBasketUnique($documentOrder['shop_basket_unique'], 1);
        if (empty($shopBasket[0]) || ($shopBasket[0]['user_uuid'] != $this->userService->getUserUuid() && !$this->userService->checkUserRoleAccessMin(3))) {
            return -1;
        }

        $folder = $this->folderTool->getBrandDateFolder($this->config['bitkorn_shop']['document_path_absolute'], 'order',
            date('Y', $shopBasket[0]['shop_basket_time_create']), date('m', $shopBasket[0]['shop_basket_time_create']));
        if (file_exists($folder . '/' . $documentOrder['shop_document_order_filename'])) {
            header('Content-type: application/pdf');
            header('Content-Disposition: inline; filename="order.pdf"'); // inline || attachment
            header('Content-length: ' . filesize($folder . '/' . $documentOrder['shop_document_order_filename']));
            header('Content-Transfer-Encoding: binary');
            header('Accept-Ranges: bytes');
            $resource = fopen('php://output', 'w');
            $rawPdf = file_get_contents($folder . '/' . $documentOrder['shop_document_order_filename']);
            fwrite($resource, $rawPdf);
            exit();
        }

        $jsonModel = new JsonModel();
        $jsonModel->setVariable('documentId', $this->params('document_id'));
        return $jsonModel;
    }

    /**
     * @return JsonModel|int
     */
    public function streamDeliveryPdfAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            return -1;
        }
        $document = $this->documentDeliveryService->getShopDocumentDeliveryById(intval($this->params('document_id')));
        if (empty($document)) {
            return -1;
        }
        /**
         * check user
         */
        $shopBasket = $this->basketAdminService->getShopBasketEntitiesByBasketUnique($document['shop_basket_unique'], 1);
        if (empty($shopBasket[0]) || ($shopBasket[0]['user_uuid'] != $this->userService->getUserUuid() && !$this->userService->checkUserRoleAccessMin(3))) {
            return -1;
        }

        $folder = $this->folderTool->getBrandDateFolder($this->config['bitkorn_shop']['document_path_absolute'], 'delivery',
            date('Y', $shopBasket[0]['shop_basket_time_create']), date('m', $shopBasket[0]['shop_basket_time_create']));
        if (file_exists($folder . '/' . $document['shop_document_delivery_filename'])) {
            header('Content-type: application/pdf');
            header('Content-Disposition: inline; filename="delivery.pdf"'); // inline || attachment
            header('Content-length: ' . filesize($folder . '/' . $document['shop_document_delivery_filename']));
            header('Content-Transfer-Encoding: binary');
            header('Accept-Ranges: bytes');
            $resource = fopen('php://output', 'w');
            $rawPdf = file_get_contents($folder . '/' . $document['shop_document_delivery_filename']);
            fwrite($resource, $rawPdf);
            exit();
        }

        $jsonModel = new JsonModel();
        $jsonModel->setVariable('documentId', $this->params('document_id'));
        return $jsonModel;
    }

}
