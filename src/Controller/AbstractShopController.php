<?php

namespace Bitkorn\Shop\Controller;

use Bitkorn\Shop\Service\Basket\BasketService;
use Bitkorn\Trinket\Service\I18n\NumberFormatService;
use Bitkorn\Trinket\Table\ToolsTable;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Mvc\MvcEvent;

class AbstractShopController extends AbstractUserController
{

    /**
     * @var array
     */
    protected $config = [];

    /**
     * @var BasketService
     */
    protected $basketService;

    /**
     * @var NumberFormatService
     */
    protected $numberFormatService;

    /**
     *
     * @var ToolsTable
     */
    protected $toolsTable;

    /**
     * @param array $config
     */
    public function setConfig(array $config): void
    {
        $this->config = $config;
    }

    /**
     * Weils jeder brauch: hier im AbstractShopController.
     *
     * @param BasketService $basketService
     */
    public function setBasketService(BasketService $basketService): void
    {
        $this->basketService = $basketService;
    }

    /**
     * @param ToolsTable $toolsTable
     */
    public function setToolsTable(ToolsTable $toolsTable): void
    {
        $this->toolsTable = $toolsTable;
    }

    /**
     * @param NumberFormatService $numberFormatService
     */
    public function setNumberFormatService(NumberFormatService $numberFormatService): void
    {
        $this->numberFormatService = $numberFormatService;
    }

    /**
     * Controller that implements \Bitkorn\Shop\Controller\AbstractShopController get a basketunique.
     *
     * @param MvcEvent $e
     * @return mixed
     */
    public function onDispatch(MvcEvent $e)
    {
        if (isset($this->basketService) && $this->basketService instanceof BasketService) {
            $this->basketService->purgeBasketUniqueCookie();
            $e->getViewModel()->setVariable(BasketService::COOKIE_BASKET_KEY, $this->basketService->getBasketUnique());
        }
        return parent::onDispatch($e);
    }

    /**
     * @param $array
     * @param $arrayKey
     * @return false|string|null
     * @deprecated
     * @todo move in in a *Tool class
     */
    protected function toDatestringDe($array, $arrayKey)
    {
        if (!isset($array[$arrayKey])) {
            return null;
        }
        if (empty($array[$arrayKey]) || !is_numeric($array[$arrayKey])) {
            return '';
        }
        return date('d.m.Y', $array[$arrayKey]);
    }
}
