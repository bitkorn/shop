<?php

namespace Bitkorn\Shop\Form\Basket;

use Bitkorn\Trinket\Validator\FloatValidator;
use Laminas\Form\Element\Checkbox;
use Laminas\Form\Element\Number;
use Laminas\Form\Element\Select;
use Laminas\Form\Element\Submit;
use Laminas\Form\Element\Text;
use Laminas\Form\Form;
use Laminas\InputFilter\InputFilterProviderInterface;

/**
 *
 * @author allapow
 */
class ShopBasketDiscountForm extends Form implements InputFilterProviderInterface
{

    /**
     * @var boolean
     */
    private $shopBasketDiscountIdRequired = false;

    /**
     * @var string
     */
    private $shopBasketDiscountHash;

    /**
     * @var array
     */
    private $shopBasketDiscountTypeAssoc = [];

    /**
     * @var array
     */
    private $shopBasketDiscountAmountTypeAssoc = [];
    private $formName = 'shop_basket_discount';

    /**
     * @var boolean
     */
    private $withUserId = false;

    /**
     * @var boolean
     */
    private $isMassAction = false;

    /**
     * @var array
     */
    private $shopUserIdAssoc = [];

    function __construct($name = 'shop_basket_discount')
    {
        parent::__construct($name);
    }

    public function init()
    {
        $this->add([
            'name' => 'form_name',
            'attributes' => [
                'type' => 'hidden',
                'value' => $this->formName
            ],
        ]);

//        $this->add(array(
//            'name' => 'shop_basket_discount_id',
//            'attributes' => array(
//                'type' => 'hidden',
//            ),
//        ));

        if ($this->withUserId) {
            $userId = new Select('user_uuid');
            $userId->setLabel('Benutzer');
            $userIdAssocMerged = ['' => '--- Benutzer wählen --'];
            foreach ($this->shopUserIdAssoc as $key => $val) {
                $userIdAssocMerged[$key] = $val;
            }
            $userId->setValueOptions($userIdAssocMerged);
            $userId->setAttributes([
                'class' => 'w3-select',
            ]);
            $this->add($userId);
        }

        if (!$this->isMassAction) {
            $shopBasketDiscountHash = new Text('shop_basket_discount_hash');
            $shopBasketDiscountHash->setLabel('Rabatt-Code');
            $shopBasketDiscountHash->setValue($this->shopBasketDiscountHash);
            $shopBasketDiscountHash->setAttributes([
                'class' => 'w3-input',
                'readonly' => 'true'
            ]);
            $this->add($shopBasketDiscountHash);
        } else {
            $amount = new Number('amount');
            $amount->setLabel('Anzahl');
            $amount->setAttributes([
                'class' => 'w3-input',
            ]);
            $this->add($amount);
        }

        $shopBasketDiscountType = new Select('shop_basket_discount_type');
        $shopBasketDiscountType->setLabel('Typ');
        $shopBasketDiscountTypeAssocMerged = array_merge(['' => '--- bitte wählen --'], $this->shopBasketDiscountTypeAssoc);
        $shopBasketDiscountType->setValueOptions($shopBasketDiscountTypeAssocMerged);
        $shopBasketDiscountType->setAttributes([
            'class' => 'w3-select',
        ]);
        $this->add($shopBasketDiscountType);

        $shopBasketDiscountTypeValue = new Text('shop_basket_discount_typevalue');
        $shopBasketDiscountTypeValue->setLabel('Typ-Wert');
        $shopBasketDiscountTypeValue->setAttributes([
            'class' => 'w3-input',
        ]);
        $this->add($shopBasketDiscountTypeValue);

        $shopBasketDiscountAmountType = new Select('shop_basket_discount_amount_type');
        $shopBasketDiscountAmountType->setLabel('Betrag-Typ');
        $shopBasketDiscountAmountTypeAssocMerged = array_merge(['' => '--- bitte wählen --'], $this->shopBasketDiscountAmountTypeAssoc);
        $shopBasketDiscountAmountType->setValueOptions($shopBasketDiscountAmountTypeAssocMerged);
        $shopBasketDiscountAmountType->setAttributes([
            'class' => 'w3-select',
        ]);
        $this->add($shopBasketDiscountAmountType);

        $shopBasketDiscountAmountTypeValue = new Text('shop_basket_discount_amount_typevalue');
        $shopBasketDiscountAmountTypeValue->setLabel('Betrag-Typ-Wert');
        $shopBasketDiscountAmountTypeValue->setAttributes([
            'class' => 'w3-input',
        ]);
        $this->add($shopBasketDiscountAmountTypeValue);

        $shopBasketDiscountMinOrderAmount = new Text('shop_basket_discount_min_order_amount');
        $shopBasketDiscountMinOrderAmount->setLabel('Mindestbestellwert');
        $shopBasketDiscountMinOrderAmount->setAttributes([
            'class' => 'w3-input',
        ]);
        $this->add($shopBasketDiscountMinOrderAmount);

        $shopBasketDiscountActive = new Checkbox('shop_basket_discount_active');
        $shopBasketDiscountActive->setLabel('aktiv');
        $shopBasketDiscountActive->setCheckedValue(1);
        $shopBasketDiscountActive->setUncheckedValue(0);
        $shopBasketDiscountActive->setValue(1);
        $shopBasketDiscountActive->setAttributes([
            'class' => 'w3-check',
        ]);
        $this->add($shopBasketDiscountActive);

        $shopBasketDiscountDesc = new Text('shop_basket_discount_desc');
        $shopBasketDiscountDesc->setLabel('Beschreibung');
        $shopBasketDiscountDesc->setAttributes([
            'class' => 'w3-input',
        ]);
        $this->add($shopBasketDiscountDesc);

        $submit = new Submit('submit');
        $submit->setValue('speichern');
        $submit->setAttributes([
            'class' => 'w3-button w3-grey',
        ]);
        $this->add($submit);
    }

    public function getInputFilterSpecification()
    {
        $inputFilter = [];
        if ($this->withUserId) {
            $inputFilter['user_uuid'] = [
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                    ['name' => 'HtmlEntities'],
                ],
                'validators' => [
                    [
                        'name' => 'InArray',
                        'options' => [
                            'haystack' => array_keys($this->shopUserIdAssoc),
                        ],
                    ],
                ]
            ];
        }
        if (!$this->isMassAction) {
            $inputFilter['shop_basket_discount_hash'] = [
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                    ['name' => 'HtmlEntities'],
                ],
                'validators' => [
                    [
                        'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min' => 3,
                            'max' => 20,
                        ],
                    ],
                ]
            ];
        } else {
            $inputFilter['amount'] = [
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                    ['name' => 'HtmlEntities'],
                ],
                'validators' => [
                    [
                        'name' => 'Digits'
                    ]
                ]
            ];
        }
        $inputFilter = array_merge($inputFilter,
            [
                'shop_basket_discount_id' => [
                    'required' => $this->shopBasketDiscountIdRequired,
                    'filters' => [
                        ['name' => 'StripTags'],
                        ['name' => 'StringTrim'],
                        ['name' => 'HtmlEntities'],
                    ],
                    'validators' => [
                        [
                            'name' => 'Digits',
                        ],
                    ],
                ],
                'shop_basket_discount_type' => [
                    'required' => true,
                    'filters' => [
                        ['name' => 'StripTags'],
                        ['name' => 'StringTrim'],
                        ['name' => 'HtmlEntities'],
                    ],
                    'validators' => [
                        [
                            'name' => 'InArray',
                            'options' => [
                                'haystack' => array_keys($this->shopBasketDiscountTypeAssoc),
                            ],
                        ],
                    ],
                ],
                'shop_basket_discount_typevalue' => [
                    'required' => false,
                    'filters' => [
                        ['name' => 'StripTags'],
                        ['name' => 'StringTrim'],
                        ['name' => 'HtmlEntities'],
                    ],
                    'validators' => [
                        [
                            'name' => 'StringLength',
                            'options' => [
                                'encoding' => 'UTF-8',
                                'min' => 1,
                                'max' => 100,
                            ],
                        ],
                    ],
                ],
                'shop_basket_discount_amount_type' => [
                    'required' => true,
                    'filters' => [
                        ['name' => 'StripTags'],
                        ['name' => 'StringTrim'],
                        ['name' => 'HtmlEntities'],
                    ],
                    'validators' => [
                        [
                            'name' => 'InArray',
                            'options' => [
                                'haystack' => array_keys($this->shopBasketDiscountAmountTypeAssoc),
                            ],
                        ],
                    ],
                ],
                'shop_basket_discount_amount_typevalue' => [
                    'required' => true,
                    'filters' => [
                        ['name' => 'StripTags'],
                        ['name' => 'StringTrim'],
                        ['name' => 'HtmlEntities'],
                    ],
                    'validators' => [
                        new FloatValidator()
                    ],
                ],
                'shop_basket_discount_min_order_amount' => [
                    'required' => false,
                    'filters' => [
                        ['name' => 'StripTags'],
                        ['name' => 'StringTrim'],
                        ['name' => 'HtmlEntities'],
                    ],
                    'validators' => [
                        new FloatValidator()
                    ],
                ],
                'shop_basket_discount_active' => [
                    'required' => true,
                    'filters' => [
                        ['name' => 'StripTags'],
                        ['name' => 'StringTrim'],
                        ['name' => 'HtmlEntities'],
                    ],
                    'validators' => [
                        [
                            'name' => 'Digits',
                        ],
                    ],
                ],
                'shop_basket_discount_desc' => [
                    'required' => false,
                    'filters' => [
                        ['name' => 'StripTags'],
                        ['name' => 'StringTrim'],
                        ['name' => 'HtmlEntities'],
                    ],
                    'validators' => [
                        [
                            'name' => 'StringLength',
                            'options' => [
                                'encoding' => 'UTF-8',
                                'min' => 1,
                                'max' => 200,
                            ],
                        ],
                    ],
                ]
            ]
        );
        return $inputFilter;
    }

    /**
     *
     * @param boolean $shopBasketDiscountIdRequired
     */
    public function setShopBasketDiscountIdRequired($shopBasketDiscountIdRequired)
    {
        $this->shopBasketDiscountIdRequired = $shopBasketDiscountIdRequired;
    }

    public function setShopBasketDiscountHash($shopBasketDiscountHash)
    {
        $this->shopBasketDiscountHash = $shopBasketDiscountHash;
    }

    public function setShopBasketDiscountTypeAssoc(array $shopBasketDiscountTypeAssoc)
    {
        foreach ($shopBasketDiscountTypeAssoc as $shopBasketDiscountType) {
            $this->shopBasketDiscountTypeAssoc[$shopBasketDiscountType] = $shopBasketDiscountType;
        }
    }

    public function setShopBasketDiscountAmountTypeAssoc($shopBasketDiscountAmountTypeAssoc)
    {
        foreach ($shopBasketDiscountAmountTypeAssoc as $shopBasketDiscountAmountType) {
            $this->shopBasketDiscountAmountTypeAssoc[$shopBasketDiscountAmountType] = $shopBasketDiscountAmountType;
        }
    }

    /**
     *
     * @param string $formName
     */
    public function setFormName($formName)
    {
        $this->formName = $formName;
    }

    /**
     *
     * @param boolean $withUserId
     */
    public function setWithUserId($withUserId)
    {
        $this->withUserId = $withUserId;
    }

    /**
     *
     * @param array $shopUserIdAssoc
     */
    public function setShopUserIdAssoc(array $shopUserIdAssoc)
    {
        $this->shopUserIdAssoc = $shopUserIdAssoc;
    }

    /**
     *
     * @param boolean $isMassAction
     */
    public function setIsMassAction($isMassAction)
    {
        $this->isMassAction = $isMassAction;
        $this->shopBasketDiscountIdRequired = false;
    }

}
