<?php

namespace Bitkorn\Shop\Form\Basket;

use Bitkorn\Trinket\Form\Element\Text;
use Bitkorn\Trinket\Validator\IntegerValidator;
use Laminas\Filter\Digits;
use Laminas\Filter\HtmlEntities;
use Laminas\Filter\StringTrim;
use Laminas\Filter\StripTags;
use Laminas\Form\Element\Select;
use Laminas\Form\Element\Submit;
use Laminas\Form\Form;
use Bitkorn\Shop\Form\Constants;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\InArray;

/**
 *
 * @author allapow
 */
class ShopBasketAddressForm extends Form implements InputFilterProviderInterface
{

    /**
     *
     * @var string
     */
    private $formName = '';

    /**
     *
     * @var string
     */
    private $addressType = 'invoice';

    /**
     * @var boolean
     */
    private $shopBasketAddressIdRequired = false;

    /**
     *
     * @var array
     */
    private $isoCountryIdAssoc;

    /**
     * @var boolean
     */
    private $salutationEnabled = true;

    /**
     * To validate setCustomerType & form element
     * @var array
     */
    private $customerTypeIdAssoc;

    /**
     *
     * @var string
     */
    private $customerType = '';

    /**
     * @var boolean
     */
    private $birthdayEnabled = true;

    /**
     * @var boolean
     */
    private $taxIdEnabled = true;

    /**
     * @var boolean
     */
    private $companyNameEnabled = true;

    /**
     * @var boolean
     */
    private $companyDepartmentEnabled = true;

    /**
     * @var boolean
     */
    private $submitDeleteEnabled = false;

    /**
     *
     * @param string $name
     */
    function __construct($name = 'shop_basket_address')
    {
        $this->formName = $name;
        parent::__construct($name);
    }

    public function init()
    {

        $this->add([
            'name' => 'form_name',
            'attributes' => [
                'type' => 'hidden',
                'value' => $this->formName
            ],
        ]);

        $this->add([
            'name' => 'shop_basket_address_id',
            'attributes' => [
                'type' => 'hidden',
            ],
        ]);

        $this->add([
            'name' => 'shop_basket_unique',
            'attributes' => [
                'type' => 'hidden',
            ],
        ]);

        $this->add([
            'type' => Select::class,
            'name' => 'shop_basket_address_customer_type',
            'attributes' => [
                'class' => 'w3-input customer-type-select-' . $this->addressType,
                'value' => $this->customerType
            ],
            'options' => [
                'label' => '',
                'value_options' => $this->customerTypeIdAssoc
            ],
        ]);

        if ($this->salutationEnabled) {
            $this->add([
                'type' => Select::class,
                'name' => 'shop_basket_address_salut',
                'attributes' => [
//                    'class' => 'w3-input-no-width',
                    'class' => 'w3-input',
//                    'style' => 'width:24%'
                ],
                'options' => [
                    'value_options' => Constants::$salutationValueOptions,
                ],
            ]);
            $this->add([
                'type' => Text::class,
                'name' => 'shop_basket_address_degree',
                'attributes' => [
//                    'class' => 'w3-input-no-width w3-right',
                    'class' => 'w3-input w3-right',
                    'placeholder' => 'Titel',
//                    'style' => 'width:74%'
                ]
            ]);
        }

        $shopBasketAddressName1 = new Text('shop_basket_address_name1');
//        $shopBasketAddressName1->setLabel('* Vorname');
        $shopBasketAddressName1->setAttributes([
            'class' => 'w3-input',
            'placeholder' => '* Vorname'
        ]);
        $this->add($shopBasketAddressName1);

        $shopBasketAddressName2 = new Text('shop_basket_address_name2');
//        $shopBasketAddressName2->setLabel('* Nachname');
        $shopBasketAddressName2->setAttributes([
            'class' => 'w3-input',
            'placeholder' => '* Nachname'
        ]);
        $this->add($shopBasketAddressName2);

        $shopBasketAddressStreet = new Text('shop_basket_address_street');
//        $shopBasketAddressStreet->setLabel('* Straße');
        $shopBasketAddressStreet->setAttributes([
//            'class' => 'w3-input-no-width',
            'class' => 'w3-input',
            'placeholder' => '* Straße',
//            'style' => 'width: 79%'
        ]);
        $this->add($shopBasketAddressStreet);

        $shopBasketAddressStreetNo = new Text('shop_basket_address_street_no');
//        $shopBasketAddressStreetNo->setLabel('* Hausnummer');
        $shopBasketAddressStreetNo->setAttributes([
//            'class' => 'w3-input-no-width w3-right',
            'class' => 'w3-input',
            'placeholder' => '* Nr.',
//            'style' => 'width: 19%'
        ]);
        $this->add($shopBasketAddressStreetNo);

        $shopBasketAddressZip = new Text('shop_basket_address_zip');
//        $shopBasketAddressZip->setLabel('* Postleitzahl');
        $shopBasketAddressZip->setAttributes([
//            'class' => 'w3-input-no-width',
            'class' => 'w3-input',
            'placeholder' => '* PLZ',
//            'style' => 'width: 19%'
        ]);
        $this->add($shopBasketAddressZip);

        $shopBasketAddressCity = new Text('shop_basket_address_city');
//        $shopBasketAddressCity->setLabel('* Ort');
        $shopBasketAddressCity->setAttributes([
//            'class' => 'w3-input-no-width w3-right',
            'class' => 'w3-input',
            'placeholder' => '* Ort',
//            'style' => 'width: 79%'
        ]);
        $this->add($shopBasketAddressCity);

        $this->add([
            'type' => Text::class,
            'name' => 'shop_basket_address_additional',
            'attributes' => [
                'title' => '',
                'class' => 'w3-input',
                'placeholder' => 'Adresszusatz'
            ],
        ]);

        $isoCountryId = new Select('country_id');
        $isoCountryId->setAttributes([
            'class' => 'w3-input',
        ]);
        $isoCountryId->setValueOptions($this->isoCountryIdAssoc);
        $isoCountryId->setValue(48);
        $this->add($isoCountryId);

        $shopBasketAddressEmail = new Text('shop_basket_address_email');
//        $shopBasketAddressEmail->setLabel('Email');
        $shopBasketAddressEmail->setAttributes([
            'class' => 'w3-input',
            'placeholder' => '* Email Adresse'
        ]);
        $this->add($shopBasketAddressEmail);

        $shopBasketAddressTel = new Text('shop_basket_address_tel');
//        $shopBasketAddressTel->setLabel('Telefon');
        $shopBasketAddressTel->setAttributes([
            'class' => 'w3-input',
            'placeholder' => 'Telefonnummer'
        ]);
        $this->add($shopBasketAddressTel);

        if ($this->birthdayEnabled) {
            $this->add([
                'type' => 'text',
                'name' => 'shop_basket_address_birthday',
                'attributes' => [
                    'title' => '',
                    'class' => 'w3-input datepicker',
                    'placeholder' => 'Geburtstag'
                ],
//                'options' => [
//                    'label' => 'Geburtstag',
//                ],
            ]);
        }

        if ($this->taxIdEnabled) {
            $shopUserAddressTaxId = new Text('shop_basket_address_tax_id');
//            $shopUserAddressTaxId->setLabel('USt.ID');
            $shopUserAddressTaxId->setAttributes([
                'class' => 'w3-input',
                'placeholder' => '* Umsatzsteuer-ID'
            ]);
            $this->add($shopUserAddressTaxId);
        }

        if ($this->companyNameEnabled) {
            $this->add([
                'type' => Text::class,
                'name' => 'shop_basket_address_company_name',
                'attributes' => [
                    'title' => '',
                    'class' => 'w3-input',
                    'placeholder' => '* Firma'
                ],
                'options' => [
//                    'label' => 'Firma',
                ],
            ]);
        }

        if ($this->companyDepartmentEnabled) {
            $this->add([
                'type' => Text::class,
                'name' => 'shop_basket_address_company_department',
                'attributes' => [
                    'title' => '',
                    'class' => 'w3-input',
                    'placeholder' => 'Abteilung'
                ],
                'options' => [
//                    'label' => 'Abteilung',
                ],
            ]);
        }

        $submit = new Submit('submit');
        $submit->setValue('speichern');
        $submit->setAttributes([
            'class' => 'w3-button bk-success'
        ]);
        $this->add($submit);

        if ($this->submitDeleteEnabled) {
            $this->add([
                'type' => Submit::class,
                'name' => 'submit_delete',
                'attributes' => [
                    'class' => 'w3-button bk-warn',
                    'value' => 'löschen',
                ]
            ]);
        }
    }

    public function getInputFilterSpecification()
    {
        $filter = [];
        $filter['shop_basket_address_id'] = [
            'required' => $this->shopBasketAddressIdRequired,
            'filters' => [
                ['name' => StripTags::class],
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => Digits::class],
            ]
        ];
        $filter['shop_basket_unique'] = [
            'required' => true,
            'filters' => [
                ['name' => StripTags::class],
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
            ]
        ];
        $filter['shop_basket_address_customer_type'] = [
            'required' => true,
            'filters' => [
                ['name' => StripTags::class],
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
            ],
            'validators' => [
                [
                    'name' => InArray::class,
                    'options' => [
                        'haystack' => array_keys($this->customerTypeIdAssoc)
                    ]
                ]
            ]
        ];

        if ($this->salutationEnabled) {
            $filter['shop_basket_address_salut'] = [
                'required' => true,
                'filters' => [
                    ['name' => StripTags::class],
                    ['name' => StringTrim::class],
                    ['name' => HtmlEntities::class],
                ],
                'validators' => [
                    [
                        'name' => 'InArray',
                        'options' => [
                            'haystack' => array_keys(Constants::$salutationValueOptions)
                        ]
                    ]
                ]
            ];
            $filter['shop_basket_address_degree'] = [
                'required' => false,
                'filters' => [
                    ['name' => StripTags::class],
                    ['name' => StringTrim::class],
                    ['name' => HtmlEntities::class],
                ],
                'validators' => [
                    [
                        'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 14,
                        ],
                    ],
                ]
            ];
        }
        $filter['shop_basket_address_name1'] = [
            'required' => true,
            'filters' => [
                ['name' => StripTags::class],
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
            ],
            'validators' => [
                [
                    'name' => 'StringLength',
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 80,
                    ],
                ],
            ]
        ];
        $filter['shop_basket_address_name2'] = [
            'required' => true,
            'filters' => [
                ['name' => StripTags::class],
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
            ],
            'validators' => [
                [
                    'name' => 'StringLength',
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 80,
                    ],
                ],
            ]
        ];
        $filter['shop_basket_address_street'] = [
            'required' => true,
            'filters' => [
                ['name' => StripTags::class],
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
            ],
            'validators' => [
                [
                    'name' => 'StringLength',
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 80,
                    ],
                ],
            ]
        ];
        $filter['shop_basket_address_street_no'] = [
            'required' => true,
            'filters' => [
                ['name' => StripTags::class],
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
            ],
            'validators' => [
                [
                    'name' => 'StringLength',
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 10,
                    ],
                ],
            ]
        ];
        $filter['shop_basket_address_zip'] = [
            'required' => true,
            'filters' => [
                ['name' => StripTags::class],
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
            ],
            'validators' => [
                [
                    'name' => 'StringLength',
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 8,
                    ],
                ],
            ]
        ];
        $filter['shop_basket_address_city'] = [
            'required' => true,
            'filters' => [
                ['name' => StripTags::class],
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
            ],
            'validators' => [
                [
                    'name' => 'StringLength',
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 80,
                    ],
                ],
            ]
        ];
        $filter['shop_basket_address_additional'] = [
            'required' => false,
            'filters' => [
                ['name' => StripTags::class],
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
            ],
            'validators' => [
                [
                    'name' => 'StringLength',
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 80,
                    ],
                ],
            ]
        ];
        $filter['country_id'] = [
            'required' => true,
            'filters' => [
                ['name' => StripTags::class],
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
            ],
            'validators' => [
                [
                    'name' => 'InArray',
                    'options' => [
                        'haystack' => array_keys($this->isoCountryIdAssoc),
                    ],
                ],
            ]
        ];
        $filter['shop_basket_address_email'] = [
            'required' => true,
            'filters' => [
                ['name' => StripTags::class],
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
            ],
            'validators' => [
                [
                    'name' => 'EmailAddress',
                ],
            ]
        ];
        $filter['shop_basket_address_tel'] = [
            'required' => false,
            'filters' => [
                ['name' => StripTags::class],
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
            ],
            'validators' => [
                [
                    'name' => 'StringLength',
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 5,
                        'max' => 40,
                    ],
                ],
            ]
        ];
        if ($this->birthdayEnabled) {
            $filter['shop_basket_address_birthday'] = [
                'required' => false,
                'filters' => [
                    ['name' => StripTags::class],
                    ['name' => StringTrim::class],
                    ['name' => HtmlEntities::class],
                ],
                'validators' => [
                    new IntegerValidator()
                ]
            ];
        }
        if ($this->taxIdEnabled) {
            $filter['shop_basket_address_tax_id'] = [
                'required' => false,
                'filters' => [
                    ['name' => StripTags::class],
                    ['name' => StringTrim::class],
                    ['name' => HtmlEntities::class],
                ],
                'validators' => [
                    [
                        'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min' => 8,
                            'max' => 12,
                        ],
                    ],
                ]
            ];
        }
        if ($this->companyNameEnabled) {
            $filter['shop_basket_address_company_name'] = [
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                    ['name' => 'HtmlEntities'],
                ],
                'validators' => [
                    [
                        'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min' => 2,
                            'max' => 70,
                        ],
                    ],
                ]
            ];
        }
        if ($this->companyDepartmentEnabled) {
            $filter['shop_basket_address_company_department'] = [
                'required' => false,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                    ['name' => 'HtmlEntities'],
                ],
                'validators' => [
                    [
                        'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min' => 2,
                            'max' => 70,
                        ],
                    ],
                ]
            ];
        }
        return $filter;
    }

    /**
     *
     * @param string $addressType invoice | enterprise
     */
    public function setAddressType($addressType)
    {
        $this->addressType = $addressType;
    }

    /**
     *
     * @param boolean $shopBasketAddressIdRequired
     */
    public function setShopBasketAddressIdRequired($shopBasketAddressIdRequired)
    {
        $this->shopBasketAddressIdRequired = $shopBasketAddressIdRequired;
    }

    /**
     *
     * @param array $isoCountryIdAssoc
     */
    public function setIsoCountryIdAssoc(array $isoCountryIdAssoc)
    {
        $this->isoCountryIdAssoc = $isoCountryIdAssoc;
    }

    /**
     *
     * @param boolean $salutationEnabled
     */
    public function setSalutationEnabled($salutationEnabled)
    {
        $this->salutationEnabled = $salutationEnabled;
    }

    /**
     *
     * @param array $customerTypeIdAssoc
     */
    public function setCustomerTypeIdAssoc($customerTypeIdAssoc)
    {
        foreach ($customerTypeIdAssoc as $customerType) {
            $this->customerTypeIdAssoc[$customerType] = $customerType;
        }
    }

    /**
     *
     * @param string $customerType
     */
    public function setCustomerType($customerType)
    {
        $this->customerType = $customerType;
    }

    /**
     *
     * @param bool $birthdayEnabled
     */
    public function setBirthdayEnabled($birthdayEnabled)
    {
        $this->birthdayEnabled = $birthdayEnabled;
    }

    /**
     *
     * @param bool $taxIdEnabled
     */
    public function setTaxIdEnabled($taxIdEnabled)
    {
        $this->taxIdEnabled = $taxIdEnabled;
    }

    /**
     *
     * @param bool $companyNameEnabled
     */
    public function setCompanyNameEnabled($companyNameEnabled)
    {
        $this->companyNameEnabled = $companyNameEnabled;
    }

    /**
     *
     * @param bool $companyDepartmentEnabled
     */
    public function setCompanyDepartmentEnabled($companyDepartmentEnabled)
    {
        $this->companyDepartmentEnabled = $companyDepartmentEnabled;
    }

    /**
     *
     * @param bool $submitDeleteEnabled
     */
    public function setSubmitDeleteEnabled($submitDeleteEnabled)
    {
        $this->submitDeleteEnabled = $submitDeleteEnabled;
    }

    public function setName($name)
    {
        $this->formName = $name;
        return parent::setName($name);
    }

    /**
     *
     * @param string $targetMode string | integer
     * @return boolean
     */
    public function switchDate($targetMode)
    {
        $birthday = $this->get('shop_basket_address_birthday')->getValue();
        if ($targetMode == 'string') {
            if (empty($birthday)) {
                $this->get('shop_basket_address_birthday')->setValue('');
                if (isset($this->data['shop_basket_address_birthday'])) {
                    $this->data['shop_basket_address_birthday'] = '';
                }
                return true;
            }
            if (is_numeric($birthday)) {
                $this->get('shop_basket_address_birthday')->setValue(date('d.m.Y', $birthday));
                if (isset($this->data['shop_basket_address_birthday'])) {
                    $this->data['shop_basket_address_birthday'] = date('d.m.Y', $birthday);
                }
                return true;
            }
        } elseif ($targetMode == 'integer') {
            if (empty($birthday)) {
                if (isset($this->data['shop_basket_address_birthday'])) {
                    $this->data['shop_basket_address_birthday'] = 0;
                }
                return true;
            }
            $dateTime = new \DateTime($birthday);
            if ($dateTime) {
                $this->get('shop_basket_address_birthday')->setValue($dateTime->getTimestamp());
                if (isset($this->data['shop_basket_address_birthday'])) {
                    $this->data['shop_basket_address_birthday'] = $dateTime->getTimestamp();
                }
                return true;
            }
        }
        return FALSE;
    }

    public function getDataDebug()
    {
        return $this->data;
    }

}
