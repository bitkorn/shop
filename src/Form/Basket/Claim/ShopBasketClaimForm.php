<?php

namespace Bitkorn\Shop\Form\Basket\Claim;

use Laminas\Form\Element\Radio;
use Laminas\Form\Element\Submit;
use Laminas\Form\Element\Textarea;
use Laminas\Form\Form;
use Laminas\InputFilter\InputFilterProviderInterface;

/**
 *
 * @author allapow
 */
class ShopBasketClaimForm extends Form implements InputFilterProviderInterface
{

    private bool $shopBasketClaimIdRequired = false;

    public function init()
    {

        $this->add([
            'name' => 'shop_basket_claim_id',
            'attributes' => [
                'type' => 'hidden',
            ],
        ]);

        $this->add([
            'name' => 'shop_basket_unique',
            'attributes' => [
                'type' => 'hidden',
            ],
        ]);

        $this->add([
            'name' => 'shop_basket_item_id',
            'attributes' => [
                'type' => 'hidden',
            ],
        ]);

        $shopBasketClaimReason = new Textarea('shop_basket_claim_reason');
        $shopBasketClaimReason->setLabel('Retouren Grund');
        $shopBasketClaimReason->setAttributes([
            'class' => 'w3-input',
        ]);
        $this->add($shopBasketClaimReason);

        $shopBasketClaimFinished = new Radio('shop_basket_claim_finished');
        $shopBasketClaimFinished->setValueOptions([
            0 => 'nein',
            1 => 'ja'
        ]);
        $shopBasketClaimFinished->setChecked(0);
        $shopBasketClaimFinished->setLabel('erledigt');
        $shopBasketClaimFinished->setAttributes([
            'class' => 'w3-radio',
        ]);
        $this->add($shopBasketClaimFinished);

        $submit = new Submit('submit');
        $submit->setValue('speichern');
        $submit->setAttributes([
            'class' => 'w3-button w3-grey',
        ]);
        $this->add($submit);
    }

    function __construct($name = 'shop_basket_claim')
    {
        parent::__construct($name);
    }

    public function getInputFilterSpecification()
    {
        return [
            'shop_basket_claim_id' => [
                'required' => $this->shopBasketClaimIdRequired,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                    ['name' => 'HtmlEntities'],
                ],
                'validators' => [
                    [
                        'name' => 'Digits',
                    ],
                ],
            ],
            'shop_basket_unique' => [
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                    ['name' => 'HtmlEntities'],
                ],
                'validators' => [
                    [
                        'name' => 'Alnum',
                    ],
                ],
            ],
            'shop_basket_item_id' => [
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                    ['name' => 'HtmlEntities'],
                ],
                'validators' => [
                    [
                        'name' => 'Digits',
                    ],
                ],
            ],
            'shop_basket_claim_reason' => [
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                    ['name' => 'HtmlEntities'],
                ],
                'validators' => [
                    [
                        'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 50000,
                        ],
                    ],
                ],
            ],
            'shop_basket_claim_finished' => [
                'required' => false,
                'filters' => [
                    ['name' => 'Int'],
                ],
                'validators' => [
                    ['name' => 'Digits'],
                ],
            ],
        ];
    }

    public function setShopBasketClaimIdRequired($shopBasketClaimIdRequired)
    {
        $this->shopBasketClaimIdRequired = $shopBasketClaimIdRequired;
    }

}
