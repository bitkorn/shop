<?php

namespace Bitkorn\Shop\Form\Basket\Claim;

use Laminas\Form\Form;
use Laminas\InputFilter\InputFilterProviderInterface;

/**
 *
 * @author allapow
 */
class ShopBasketClaimFileForm extends Form implements InputFilterProviderInterface
{

    public function init()
    {
        $this->setAttribute('enctype', 'multipart/form-data');

        $this->add([
            'name' => 'shop_basket_claim_id',
            'attributes' => [
                'type' => 'hidden',
            ],
        ]);

        $this->add([
            'name' => 'shop_basket_unique',
            'attributes' => [
                'type' => 'hidden',
            ],
        ]);

        $this->add([
            'name' => 'shop_basket_item_id',
            'attributes' => [
                'type' => 'hidden',
            ],
        ]);

        $shopBasketClaimFilenameTokens = new \Laminas\Form\Element\Text('shop_basket_claim_filename_tokens');
        $shopBasketClaimFilenameTokens->setLabel('Text für den Dateinamen');
        $shopBasketClaimFilenameTokens->setAttributes([
            'class' => 'w3-input',
        ]);
        $this->add($shopBasketClaimFilenameTokens);

        $shopBasketClaimFile = new \Laminas\Form\Element\File('shop_basket_claim_file');
        $shopBasketClaimFile->setAttributes([
            'class' => 'w3-input w3-border',
            'accept' => 'image/jpg,image/jpeg,image/png,application/pdf,application/postscript,image/tiff,image/x-tiff'
        ]);
        $this->add($shopBasketClaimFile);

        $submit = new \Laminas\Form\Element\Submit('submit_upload');
        $submit->setValue('upload');
        $submit->setAttributes([
            'class' => 'w3-button w3-grey',
        ]);
        $this->add($submit);
    }

    function __construct($name = 'shop_basket_claim_file')
    {
        parent::__construct($name);
    }

    public function getInputFilterSpecification()
    {
        return [
            'shop_basket_claim_id' => [
                'required' => true,
                'filters' => [
                    ['name' => 'Int'],
                ],
                'validators' => [
                    [
                        'name' => 'Digits',
                    ],
                ],
            ],
            'shop_basket_unique' => [
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                    ['name' => 'HtmlEntities'],
                ],
                'validators' => [
                    [
                        'name' => 'Alnum',
                    ],
                ],
            ],
            'shop_basket_item_id' => [
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                    ['name' => 'HtmlEntities'],
                ],
                'validators' => [
                    [
                        'name' => 'Digits',
                    ],
                ],
            ],
            'shop_basket_claim_filename_tokens' => [
                'required' => true, // um einen Filename zu generieren
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 200,
                        ],
                    ],
                ],
            ],
            'shop_basket_claim_file' => [
                'required' => false, // false weil vielleicht von einem auch nur Text geaendert werden soll
                'filters' => [
//                    array('name' => 'FileInput'),
                ],
                'validators' => [
                    [
                        'name' => 'filemimetype',
                        'break_chain_on_failure' => TRUE,
                        'options' => [
                            'mimeType' => 'image/jpeg, image/png, application/pdf, image/tiff, image/x-tiff, application/postscript',
                            'magicFile' => FALSE, // wegen: exit signal Segmentation fault (11), possible coredump in /etc/apache2
                            'messages' => [
                                \Laminas\Validator\File\MimeType::FALSE_TYPE => "Falsches Dateiformat '%type%', nur Bilder und PDF",
                            ],
                        ]
                    ],
                ],
            ],
        ];
    }

}

?>
