<?php

namespace Bitkorn\Shop\Form\Article;

use Bitkorn\Trinket\Validator\FloatValidator;
use Laminas\Form\Element\Select;
use Laminas\Form\Element\Submit;
use Laminas\Form\Element\Text;
use Laminas\Form\Form;
use Laminas\InputFilter\InputFilterProviderInterface;

/**
 *
 * @author allapow
 */
class ShopArticleStockForm extends Form implements InputFilterProviderInterface
{

    /**
     * @var bool
     */
    private $shopArticleStockIdRequired = false;

    /**
     * @var array
     */
    private $shopArticleStockReasonTypesIdAssoc;

    /**
     * @var array
     */
    private $shopVendorIdAssoc;

    /**
     * @param boolean $shopArticleStockIdRequired
     */
    public function setShopArticleStockIdRequired($shopArticleStockIdRequired)
    {
        $this->shopArticleStockIdRequired = $shopArticleStockIdRequired;
    }

    /**
     *
     * @param array $shopArticleStockReasonTypesIdAssoc
     */
    public function setShopArticleStockReasonTypesIdAssoc(array $shopArticleStockReasonTypesIdAssoc)
    {
        foreach ($shopArticleStockReasonTypesIdAssoc as $shopArticleStockReasonType) {
            $this->shopArticleStockReasonTypesIdAssoc[$shopArticleStockReasonType] = $shopArticleStockReasonType;
        }
    }

    public function setShopVendorIdAssoc(array $shopVendorIdAssoc)
    {
        $this->shopVendorIdAssoc = $shopVendorIdAssoc;
    }


    public function init()
    {

        $this->add([
            'name' => 'shop_article_stock_id',
            'attributes' => [
                'type' => 'hidden',
            ],
        ]);
        $this->add([
            'name' => 'shop_article_id',
            'attributes' => [
                'type' => 'hidden',
            ],
        ]);

        $shopArticleStockAmount = new Text('shop_article_stock_amount');
        $shopArticleStockAmount->setLabel('Menge');
        $shopArticleStockAmount->setAttributes([
            'class' => 'w3-input',
        ]);
        $this->add($shopArticleStockAmount);

        $shopVendor = new Select('shop_vendor_id');
        $shopVendor->setLabel('Lieferant');
        $shopVendor->setAttributes([
            'class' => 'w3-input',
        ]);
        $shopVendor->setValueOptions($this->shopVendorIdAssoc);
        $this->add($shopVendor);

        $shopArticleStockBatch = new Text('shop_article_stock_batch');
        $shopArticleStockBatch->setLabel('Charge');
        $shopArticleStockBatch->setAttributes([
            'class' => 'w3-input',
        ]);
        $this->add($shopArticleStockBatch);

        $shopArticleStockReasonType = new Select('shop_article_stock_reason_type');
        $shopArticleStockReasonType->setLabel('Typ');
        $shopArticleStockReasonType->setAttributes([
            'class' => 'w3-input',
        ]);
        $shopArticleStockReasonType->setValueOptions($this->shopArticleStockReasonTypesIdAssoc);
        $this->add($shopArticleStockReasonType);

        $shopArticleStockReason = new Text('shop_article_stock_reason');
        $shopArticleStockReason->setLabel('Grund');
        $shopArticleStockReason->setAttributes([
            'class' => 'w3-input',
        ]);
        $this->add($shopArticleStockReason);

        $submit = new Submit('submit');
        $submit->setValue('speichern');
        $submit->setAttributes([
            'class' => 'w3-button w3-grey',
        ]);
        $this->add($submit);
    }

    function __construct($name = 'shop_article_stock')
    {
        parent::__construct($name);
    }

    public function getInputFilterSpecification()
    {
        return [
            'shop_article_stock_id' => [
                'required' => $this->shopArticleStockIdRequired,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'Digits',
                    ],
                ],
            ],
            'shop_article_id' => [
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'Digits',
                    ],
                ],
            ],
            'shop_article_stock_amount' => [
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    new FloatValidator()
                ],
            ],
            'shop_vendor_id' => [
                'required' => true,
                'filters' => [
                    ['name' => 'Digits'],
                ],
                'validators' => [
                    [
                        'name' => 'InArray',
                        'options' => [
                            'haystack' => array_keys($this->shopVendorIdAssoc),
                        ],
                    ],
                ],
            ],
            'shop_article_stock_batch' => [
                'required' => false,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 80,
                        ],
                    ],
                ],
            ],
            'shop_article_stock_reason_type' => [
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'InArray',
                        'options' => [
                            'haystack' => array_keys($this->shopArticleStockReasonTypesIdAssoc),
                        ],
                    ],
                ],
            ],
            'shop_article_stock_reason' => [
                'required' => false,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 200,
                        ],
                    ],
                ],
            ],
        ];
    }
}
