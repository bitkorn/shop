<?php

namespace Bitkorn\Shop\Form\Article;

use Laminas\Form\Element\Hidden;
use Laminas\Form\Element\Submit;
use Laminas\Form\Element\Text;
use Laminas\Form\Element\Textarea;
use Laminas\Form\Form;
use Laminas\InputFilter\InputFilterProviderInterface;

/**
 *
 * @author allapow
 */
class ShopArticleImageEditForm extends Form implements InputFilterProviderInterface
{

    private $shopArticleId;

    public function init()
    {

        $this->add([
            'name' => 'shop_article_image_id',
            'attributes' => [
                'type' => 'hidden',
            ],
        ]);

        $shopArticleId = new Hidden('shop_article_id');
        $shopArticleId->setValue($this->shopArticleId);
        $this->add($shopArticleId);

        $shopArticleImageTitle = new Text('shop_article_image_title');
        $shopArticleImageTitle->setLabel('Bild Titel');
        $shopArticleImageTitle->setAttributes([
            'class' => 'w3-input',
            'title' => 'hieraus wird der SEO-freundliche Dateiname generiert!!!'
        ]);
        $this->add($shopArticleImageTitle);

        $shopArticleImageDesc = new Textarea('shop_article_image_desc');
        $shopArticleImageDesc->setLabel('Bild Beschreibung');
        $shopArticleImageDesc->setAttributes([
            'class' => 'w3-input w3-border',
            'title' => 'dieser Text erscheint zu dem Bild im Frontend'
        ]);
        $this->add($shopArticleImageDesc);

        $submit = new Submit('submit');
        $submit->setValue('speichern');
        $submit->setAttributes([
            'class' => 'w3-button w3-grey',
        ]);
        $this->add($submit);
    }

    function __construct($name = 'shop_article_image')
    {
        parent::__construct($name);
    }

    public function getInputFilterSpecification()
    {
        return [
            'shop_article_image_id' => [
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'Digits',
                    ],
                ],
            ],
            'shop_article_id' => [
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'Digits',
                    ],
                ],
            ],
            'shop_article_image_title' => [
                'required' => true, // um einen Filename zu generieren
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 200,
                        ],
                    ],
                ],
            ],
            'shop_article_image_desc' => [
                'required' => false,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 32000,
                        ],
                    ],
                ],
            ],
        ];
    }

    public function setShopArticleId($shopArticleId)
    {
        $this->shopArticleId = $shopArticleId;
    }

}
