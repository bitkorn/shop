<?php

namespace Bitkorn\Shop\Form\Article\TypeDelivery;

use Bitkorn\Trinket\Form\AbstractForm;
use Bitkorn\Trinket\Form\Element\Text;
use Bitkorn\Trinket\Form\Element\Textarea;
use Bitkorn\Trinket\Validator\FloatValidator;
use Laminas\Filter\HtmlEntities;
use Laminas\Filter\StringTrim;
use Laminas\Filter\StripTags;
use Laminas\Form\Element\Hidden;
use Laminas\Form\Element\Number;
use Laminas\Form\Element\Select;
use Laminas\Form\Element\Submit;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\Digits;
use Laminas\Validator\InArray;
use Laminas\Validator\StringLength;

class ShopArticleLengthcutDefForm extends AbstractForm implements InputFilterProviderInterface
{

    /**
     * @var array
     */
    protected $stringNumberElementNames = ['shop_article_lengthcut_def_basequantity', 'shop_article_lengthcut_def_cutprice'];

    /**
     * @var bool
     */
    protected $isNew = false;

    /**
     * @var array
     */
    protected $quantityunitIdAssoc;

    /**
     * @param bool $isNew
     */
    public function setIsNew(bool $isNew): void
    {
        $this->isNew = $isNew;
    }

    /**
     * @param array $quantityunitIdAssoc
     */
    public function setQuantityunitIdAssoc(array $quantityunitIdAssoc): void
    {
        $this->quantityunitIdAssoc = $quantityunitIdAssoc;
    }

    public function init()
    {

        if (!$this->isNew) {
            $this->add([
                'type' => Hidden::class,
                'name' => 'shop_article_lengthcut_def_id',
            ]);
        }

        $this->add([
            'type' => Text::class,
            'name' => 'shop_article_lengthcut_def_name',
            'options' => [
                'label' => 'Name'
            ],
            'attributes' => [
                'class' => 'w3-input'
            ],
        ]);

        $this->add([
            'type' => Textarea::class,
            'name' => 'shop_article_lengthcut_def_desc',
            'options' => [
                'label' => 'Beschreibung'
            ],
            'attributes' => [
                'class' => 'w3-input'
            ],
        ]);

        $this->add([
            'type' => Select::class,
            'name' => 'quantityunit_uuid',
            'options' => [
                'label' => 'Mengeneinheit',
                'value_options' => $this->quantityunitIdAssoc
            ],
            'attributes' => [
                'class' => 'w3-select'
            ],
        ]);

        $this->add([
            'type' => Text::class,
            'name' => 'shop_article_lengthcut_def_basequantity',
            'options' => [
                'label' => 'Lagergröße'
            ],
            'attributes' => [
                'class' => 'w3-input'
            ],
        ]);

        $this->add([
            'type' => Text::class,
            'name' => 'shop_article_lengthcut_def_cutprice',
            'options' => [
                'label' => 'Preis / Schnitt'
            ],
            'attributes' => [
                'class' => 'w3-input'
            ],
        ]);

        $this->add([
            'type' => Submit::class,
            'name' => 'submit',
            'attributes' => [
                'value' => 'speichern',
                'class' => 'w3-button w3-gray'
            ],
        ]);
    }

    /**
     * Should return an array specification compatible with
     * {@link \Laminas\InputFilter\Factory::createInputFilter()}.
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $filter = [];

        if (!$this->isNew) {
            $filter['shop_article_lengthcut_def_id'] = [
                'required' => true,
                'filters' => [
                ],
                'validators' => [
                    [
                        'name' => Digits::class,
                    ]
                ]
            ];
        }

        $filter['shop_article_lengthcut_def_name'] = [
            'required' => true,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => StripTags::class]
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 3,
                        'max' => 180,
                    ]
                ]
            ]
        ];

        $filter['shop_article_lengthcut_def_desc'] = [
            'required' => false,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => StripTags::class]
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 3,
                        'max' => 64000,
                    ]
                ]
            ]
        ];

        $filter['quantityunit_uuid'] = [
            'required' => true,
            'filters' => [
                ['name' => StringTrim::class]
            ],
            'validators' => [
                [
                    'name' => InArray::class,
                    'options' => [
                        'haystack' => array_keys($this->quantityunitIdAssoc)
                    ]
                ]
            ]
        ];

        $filter['shop_article_lengthcut_def_basequantity'] = [
            'required' => true,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => StripTags::class]
            ],
            'validators' => [
                [
                    'name' => FloatValidator::class,
                ]
            ]
        ];

        $filter['shop_article_lengthcut_def_cutprice'] = [
            'required' => true,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => StripTags::class]
            ],
            'validators' => [
                [
                    'name' => FloatValidator::class,
                ]
            ]
        ];

        return $filter;
    }
}
