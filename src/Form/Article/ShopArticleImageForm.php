<?php

namespace Bitkorn\Shop\Form\Article;

use Laminas\Form\Element\File;
use Laminas\Form\Element\Hidden;
use Laminas\Form\Element\Text;
use Laminas\Form\Element\Textarea;
use Laminas\Form\Form;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\File\MimeType;

/**
 *
 * @author allapow
 */
class ShopArticleImageForm extends Form implements InputFilterProviderInterface
{

    private $shopArticleImageIdRequired = false;
    private $shopArticleId;

    public function init()
    {
        $this->setAttribute('enctype', 'multipart/form-data');

        $this->add([
            'name' => 'shop_article_image_id',
            'attributes' => [
                'type' => 'hidden',
            ],
        ]);

        $shopArticleId = new Hidden('shop_article_id');
        $shopArticleId->setValue($this->shopArticleId);
        $this->add($shopArticleId);

        $shopArticleImageTitle = new Text('shop_article_image_title');
        $shopArticleImageTitle->setLabel('Bild Titel');
        $shopArticleImageTitle->setAttributes([
            'class' => 'w3-input',
            'title' => 'hieraus wird der SEO-freundliche Dateiname generiert!!!'
        ]);
        $this->add($shopArticleImageTitle);

        $shopArticleImageDesc = new Textarea('shop_article_image_desc');
        $shopArticleImageDesc->setLabel('Bild Beschreibung');
        $shopArticleImageDesc->setAttributes([
            'class' => 'w3-input w3-border',
            'title' => 'dieser Text erscheint zu dem Bild im Frontend'
        ]);
        $this->add($shopArticleImageDesc);

        $this->add([
            'name' => 'shop_article_image_filename',
            'attributes' => [
                'type' => 'hidden',
            ],
        ]);

        $shopArticleImage = new File('shop_article_image_image');
        $shopArticleImage->setAttributes([
            'class' => 'w3-input w3-border',
            'accept' => 'image/jpg,image/jpeg,image/png'
        ]);
        $this->add($shopArticleImage);

        $submit = new \Laminas\Form\Element\Submit('submit');
        $submit->setValue('speichern');
        $submit->setAttributes([
            'class' => 'w3-button w3-grey',
        ]);
        $this->add($submit);
    }

    function __construct($name = 'shop_article_image')
    {
        parent::__construct($name);
    }

    public function getInputFilterSpecification()
    {
        return [
            'shop_article_image_id' => [
                'required' => $this->shopArticleImageIdRequired,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'Digits',
                    ],
                ],
            ],
            'shop_article_id' => [
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'Digits',
                    ],
                ],
            ],
            'shop_article_image_title' => [
                'required' => true, // um einen Filename zu generieren
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 200,
                        ],
                    ],
                ],
            ],
            'shop_article_image_desc' => [
                'required' => false,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 32000,
                        ],
                    ],
                ],
            ],
            'shop_article_image_filename' => [
                'required' => false, // generiert aus shop_article_image_title
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 200,
                        ],
                    ],
                ],
            ],
            'shop_article_image_image' => [
                'required' => false, // false weil vielleicht von einem auch nur Text geaendert werden soll
                'filters' => [
//                    array('name' => 'FileInput'),
                ],
                'validators' => [
                    [
                        'name' => 'filemimetype',
                        'break_chain_on_failure' => TRUE,
                        'options' => [
                            'mimeType' => 'image/jpeg, image/png',
                            'magicFile' => FALSE, // wegen: exit signal Segmentation fault (11), possible coredump in /etc/apache2
                            'messages' => [
                                MimeType::FALSE_TYPE => "Falsches Dateiformat '%type%', nur jpeg/jpg!",
                            ],
                        ]
                    ],
//                        [
//                        'name' => 'fileimagesize',
//                        'options' => array(
//                            'maxWidth' => 1600, // 1600, 1440, 1280, 1120, 960
//                            'maxHeight' => 1600, // 900, 810, 720, 630, 540
//                            'messages' => array(
//                                \Laminas\Validator\File\ImageSize::HEIGHT_TOO_BIG => "Das Bild ist mit %height% zu hoch, max=%maxheight%!",
//                                \Laminas\Validator\File\ImageSize::WIDTH_TOO_BIG => "Das Bild ist mit %width% zu breit, max=%maxwidth%!",
//                            ),
//                        ),
//                    ],
                ],
            ],
        ];
    }

    public function setShopArticleImageIdRequired($shopArticleImageIdRequired)
    {
        $this->shopArticleImageIdRequired = $shopArticleImageIdRequired;
    }

    public function setShopArticleId($shopArticleId)
    {
        $this->shopArticleId = $shopArticleId;
    }

}
