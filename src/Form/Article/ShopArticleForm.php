<?php

namespace Bitkorn\Shop\Form\Article;

use Bitkorn\Shop\Validator\ShopArticleSefurlValidator;
use Bitkorn\Trinket\Form\AbstractForm;
use Bitkorn\Trinket\Form\Element\Text;
use Bitkorn\Trinket\Form\Element\Textarea;
use Bitkorn\Trinket\Validator\FloatValidator;
use Bitkorn\Trinket\Validator\HexColor;
use Laminas\Form\Element\Radio;
use Laminas\Form\Element\Select;
use Laminas\Form\Element\Submit;
use Laminas\Form\Form;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\InArray;

/**
 *
 * @author allapow
 */
class ShopArticleForm extends AbstractForm implements InputFilterProviderInterface
{
    /**
     * @var array
     */
    protected $stringNumberElementNames = ['shop_article_price', 'shop_article_tax', 'shop_article_price_net', 'shop_article_weight'];

    /**
     *
     * @var boolean
     */
    protected $shopArticleIdRequired = true;

    /**
     *
     * @var array
     */
    protected $shopArticleCategoryIdAssoc;

    /**
     *
     * @var array
     */
    protected $shopArticleClassIdAssoc;

    /**
     *
     * @var array
     */
    protected $shopArticleTypeKeyValAssoc = [];

    /**
     * @var array
     */
    protected $shopArticleTypeDeliveryKeyValAssoc;

    protected $isSubmitForward = true;
    protected $isSubmitBackward = true;
    protected $defaultTax = 0;
    /**
     *
     * @var ShopArticleSefurlValidator
     */
    protected $shopArticleSefurlValidator;

    /**
     *
     * @param boolean $shopArticleIdRequired
     */
    public function setShopArticleIdRequired($shopArticleIdRequired)
    {
        $this->shopArticleIdRequired = $shopArticleIdRequired;
    }

    /**
     * To validate and for valueOptions
     * @param array $shopArticleCategoryIdAssoc
     */
    public function setShopArticleCategoryIdAssoc(array $shopArticleCategoryIdAssoc)
    {
        $this->shopArticleCategoryIdAssoc = $shopArticleCategoryIdAssoc;
    }

    /**
     *
     * @param array $shopArticleClassIdAssoc
     */
    public function setShopArticleClassIdAssoc($shopArticleClassIdAssoc)
    {
        $this->shopArticleClassIdAssoc = $shopArticleClassIdAssoc;
    }

    /**
     *
     * @param array $shopArticleTypeIdAssoc
     */
    public function setShopArticleTypeKeyValAssoc(array $shopArticleTypeIdAssoc)
    {
        foreach ($shopArticleTypeIdAssoc as $shopArticleType) {
            $this->shopArticleTypeKeyValAssoc[$shopArticleType] = $shopArticleType;
        }
    }

    /**
     * @param array $shopArticleTypeDeliveryKeyValAssoc
     */
    public function setShopArticleTypeDeliveryKeyValAssoc(array $shopArticleTypeDeliveryKeyValAssoc): void
    {
        foreach ($shopArticleTypeDeliveryKeyValAssoc as $shopArticleTypeDelivery) {
            $this->shopArticleTypeDeliveryKeyValAssoc[$shopArticleTypeDelivery] = $shopArticleTypeDelivery;
        }
    }

    public function setIsSubmitForward($isSubmitForward)
    {
        $this->isSubmitForward = $isSubmitForward;
    }

    public function setIsSubmitBackward($isSubmitBackward)
    {
        $this->isSubmitBackward = $isSubmitBackward;
    }

    /**
     * @param int $defaultTax
     */
    public function setDefaultTax(int $defaultTax): void
    {
        $this->defaultTax = $defaultTax;
    }

    /**
     *
     * @param ShopArticleSefurlValidator $shopArticleSefurlValidator
     */
    public function setShopArticleSefurlValidator(ShopArticleSefurlValidator $shopArticleSefurlValidator)
    {
        $this->shopArticleSefurlValidator = $shopArticleSefurlValidator;
    }

    public function init()
    {

        $this->add([
            'name' => 'shop_article_id',
            'attributes' => [
                'type' => 'hidden',
            ],
        ]);

        $shopArticleSku = new Text('shop_article_sku');
        $shopArticleSku->setLabel('SKU');
        $shopArticleSku->setAttributes([
            'class' => 'w3-input',
        ]);
        $this->add($shopArticleSku);

        $shopArticleCategory = new Select('shop_article_category_relation');
        $shopArticleCategory->setLabel('Kategorien' . (!$this->shopArticleIdRequired ? ' (im nächsten Schritt)' : ''));
        $shopArticleCategory->setAttributes([
            'class' => 'w3-input w3-border',
            'multiple' => 'multiple',
            'size' => 4
        ]);
        $shopArticleCategory->setValueOptions($this->shopArticleCategoryIdAssoc);
        $this->add($shopArticleCategory);

        $shopArticleClass = new Select('shop_article_class_id');
        $shopArticleClass->setLabel('Klasse');
        $shopArticleClass->setLabelAttributes(['title' => 'Warenwirtschaft']);
        $shopArticleClass->setAttributes([
            'class' => 'w3-input',
        ]);
        $shopArticleClass->setValueOptions($this->shopArticleClassIdAssoc);
        $this->add($shopArticleClass);

        $shopArticleType = new Select('shop_article_type');
        $shopArticleType->setLabel('Typ');
        $shopArticleType->setAttributes([
            'class' => 'w3-input',
        ]);
        $shopArticleType->setValueOptions($this->shopArticleTypeKeyValAssoc);
        $this->add($shopArticleType);

        $shopArticleType = new Select('shop_article_type_delivery');
        $shopArticleType->setLabel('Typ - Auslieferung');
        $shopArticleType->setAttributes([
            'class' => 'w3-input',
        ]);
        $shopArticleType->setValueOptions($this->shopArticleTypeDeliveryKeyValAssoc);
        $this->add($shopArticleType);

        $shopArticleGroupable = new Radio('shop_article_groupable');
        $shopArticleGroupable->setLabel('gruppierbar');
        $shopArticleGroupable->setLabelAttributes(['class' => 'w3-block w3-border cursor-pointer']);
        $shopArticleGroupable->setAttributes([
            'class' => 'w3-radio',
        ]);
        $shopArticleGroupable->setValueOptions([0 => 'nein', 1 => 'ja']);
        $shopArticleGroupable->setValue(0);
        $this->add($shopArticleGroupable);

        $shopArticleWeight = new Text('shop_article_weight');
        $shopArticleWeight->setLabel('Gewicht in Gramm');
        $shopArticleWeight->setAttributes([
            'class' => 'w3-input',
        ]);
        $shopArticleWeight->setValue(0);
        $this->add($shopArticleWeight);

        $shopArticleShippingCostsSingle = new Text('shop_article_shipping_costs_single');
        $shopArticleShippingCostsSingle->setLabel('Einzel-Versandkosten');
        $shopArticleShippingCostsSingle->setValue(0);
        $shopArticleShippingCostsSingle->setAttributes([
            'class' => 'w3-input',
        ]);
        $this->add($shopArticleShippingCostsSingle);

        $shopArticlePrice = new Text('shop_article_price');
        $shopArticlePrice->setLabel('Preis Brutto');
        $shopArticlePrice->setAttributes([
            'class' => 'w3-input',
        ]);
        $this->add($shopArticlePrice);

        $shopArticleTax = new Text('shop_article_tax');
        $shopArticleTax->setLabel('Ums.-St.');
        $shopArticleTax->setValue($this->defaultTax);
        $shopArticleTax->setAttributes([
            'class' => 'w3-input',
            'title' => 'Im Preis enthalten!'
        ]);
        $this->add($shopArticleTax);

        $shopArticlePriceNet = new Text('shop_article_price_net');
        $shopArticlePriceNet->setLabel('Preis Netto');
        $shopArticlePriceNet->setAttributes([
            'class' => 'w3-input',
            'readonly' => true,
            'placeholder' => 'automatisch berechnet'
        ]);
        $this->add($shopArticlePriceNet);

        $shopArticleName = new Text('shop_article_name');
        $shopArticleName->setLabel('Name');
        $shopArticleName->setAttributes([
            'class' => 'w3-input'
        ]);
        $this->add($shopArticleName);

        $shopArticleSefurl = new Text('shop_article_sefurl');
        $shopArticleSefurl->setLabel('sef URL');
        $shopArticleSefurl->setAttributes([
            'class' => 'w3-input',
            'title' => 'Search Engine Friendly URL'
        ]);
        $this->add($shopArticleSefurl);

        $shopArticleDesc = new Textarea('shop_article_desc');
        $shopArticleDesc->setLabel('Beschreibung');
        $shopArticleDesc->setLabelAttributes(['class' => 'label']);
        $shopArticleDesc->setAttributes([
            'class' => 'w3-input w3-border',
            'id' => 'wysiwyg'
        ]);
        $this->add($shopArticleDesc);

        $shopArticleHexcolor = new Text('shop_article_hexcolor');
        $shopArticleHexcolor->setLabel('RGB Hex-Color ');
        $shopArticleHexcolor->setLabelAttributes(['class' => 'label']);
        $shopArticleHexcolor->setAttributes([
            'class' => 'w3-input colorpicker'
        ]);
        $this->add($shopArticleHexcolor);

        $shopArticleMetaTitle = new Text('shop_article_meta_title');
        $shopArticleMetaTitle->setLabel('META Title');
        $shopArticleMetaTitle->setAttributes([
            'class' => 'w3-input',
        ]);
        $this->add($shopArticleMetaTitle);

        $shopArticleMetaDesc = new Textarea('shop_article_meta_desc');
        $shopArticleMetaDesc->setLabel('META description');
        $shopArticleMetaDesc->setAttributes([
            'class' => 'w3-input w3-border',
        ]);
        $this->add($shopArticleMetaDesc);

        $shopArticleMetaKeywords = new Textarea('shop_article_meta_keywords');
        $shopArticleMetaKeywords->setLabel('META Keywords');
        $shopArticleMetaKeywords->setAttributes([
            'class' => 'w3-input w3-border',
        ]);
        $this->add($shopArticleMetaKeywords);

        $shopArticleActive = new Radio('shop_article_active');
        $shopArticleActive->setLabel('aktiv');
        $shopArticleActive->setLabelAttributes(['class' => 'w3-input w3-border cursor-pointer']);
        $shopArticleActive->setAttributes([
            'class' => 'w3-radio',
        ]);
        $shopArticleActive->setValueOptions([0 => 'nicht aktiv', 1 => 'aktiv']);
        $shopArticleActive->setValue(1);
        $this->add($shopArticleActive);

        $shopArticleActiveInCategories = new Radio('shop_article_active_in_categories');
        $shopArticleActiveInCategories->setLabel('aktiv in Kategorien');
        $shopArticleActiveInCategories->setLabelAttributes(['class' => 'w3-block w3-border cursor-pointer']);
        $shopArticleActiveInCategories->setAttributes([
            'class' => 'w3-radio',
        ]);
        $shopArticleActiveInCategories->setValueOptions([0 => 'nicht aktiv', 1 => 'aktiv']);
        $shopArticleActiveInCategories->setValue(1);
        $this->add($shopArticleActiveInCategories);

        $shopArticleActiveInSearch = new Radio('shop_article_active_in_search');
        $shopArticleActiveInSearch->setLabel('aktiv in Suche');
        $shopArticleActiveInSearch->setLabelAttributes(['class' => 'w3-block w3-border cursor-pointer']);
        $shopArticleActiveInSearch->setAttributes([
            'class' => 'w3-radio',
        ]);
        $shopArticleActiveInSearch->setValueOptions([0 => 'nicht aktiv', 1 => 'aktiv']);
        $shopArticleActiveInSearch->setValue(1);
        $this->add($shopArticleActiveInSearch);

        $shopArticleStockUsing = new Radio('shop_article_stock_using');
        $shopArticleStockUsing->setLabel('Bestand verwalten');
        $shopArticleStockUsing->setLabelAttributes(['class' => 'w3-block w3-border cursor-pointer']);
        $shopArticleStockUsing->setAttributes([
            'class' => 'w3-radio',
        ]);
        $shopArticleStockUsing->setValueOptions([0 => 'nein', 1 => 'ja']);
        $shopArticleStockUsing->setValue(1);
        $this->add($shopArticleStockUsing);

        $shopArticleFeeable = new Radio('shop_article_feeable');
        $shopArticleFeeable->setLabel('Provision Festwert?');
        $shopArticleFeeable->setLabelAttributes(['class' => 'w3-block w3-border cursor-pointer']);
        $shopArticleFeeable->setAttributes([
            'class' => 'w3-radio',
        ]);
        $shopArticleFeeable->setValueOptions([0 => 'nein', 1 => 'ja']);
        $shopArticleFeeable->setValue(1);
        $this->add($shopArticleFeeable);

        $submit = new Submit('submit');
        $submit->setValue('speichern');
        $submit->setAttributes([
            'class' => 'w3-button w3-grey',
        ]);
        $this->add($submit);

        if ($this->isSubmitForward) {
            $submitForward = new Submit('submit_forward');
            $submitForward->setValue('speichern und weiter bearbeiten');
            $submitForward->setAttributes([
                'class' => 'w3-button w3-grey',
            ]);
            $this->add($submitForward);
        }

        if ($this->isSubmitBackward) {
            $submitBackward = new Submit('submit_backward');
            $submitBackward->setValue('speichern und zurück');
            $submitBackward->setAttributes([
                'class' => 'w3-button w3-grey',
            ]);
            $this->add($submitBackward);
        }
    }

    function __construct($name = 'shop_article')
    {
        parent::__construct($name);
    }

    public function getInputFilterSpecification()
    {
        $filter = [];

        $filter['shop_article_id'] = [
            'required' => $this->shopArticleIdRequired,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'Digits',
                ],
            ],
        ];

        $filter['shop_article_sku'] = [
            'required' => false,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'StringLength',
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 100,
                    ],
                ],
            ],
        ];

        $filter['shop_article_category_relation'] = [
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'Explode',
                    'options' => [
                        'validator' => new InArray([
                            'haystack' => array_keys($this->shopArticleCategoryIdAssoc),
                            'valueDelimeter' => null,
                        ])
                    ],
                ],
            ],
        ];

        $filter['shop_article_class_id'] = [
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'InArray',
                    'options' => [
                        'haystack' => array_keys($this->shopArticleClassIdAssoc),
                    ],
                ],
            ],
        ];

        $filter['shop_article_type'] = [
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'InArray',
                    'options' => [
                        'haystack' => array_keys($this->shopArticleTypeKeyValAssoc),
                    ],
                ],
            ],
        ];

        $filter['shop_article_type_delivery'] = [
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'InArray',
                    'options' => [
                        'haystack' => array_keys($this->shopArticleTypeDeliveryKeyValAssoc),
                    ],
                ],
            ],
        ];

        $filter['shop_article_groupable'] = [
            'required' => false,
            'filters' => [
                ['name' => 'Digits'],
            ],
            'validators' => [
                [
                    'name' => 'InArray',
                    'options' => [
                        'haystack' => [0, 1],
                    ],
                ],
            ],
        ];

        $filter['shop_article_weight'] = [
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                new FloatValidator()
            ],
        ];

        $filter['shop_article_shipping_costs_single'] = [
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                new FloatValidator()
            ],
        ];

        $filter['shop_article_price'] = [
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                new FloatValidator()
            ],
        ];

        $filter['shop_article_tax'] = [
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                new FloatValidator()
            ],
        ];

        $filter['shop_article_price_net'] = [
            'required' => false,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                new FloatValidator()
            ],
        ];

        $filter['shop_article_name'] = [
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'StringLength',
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 200,
                    ],
                ],
            ],
        ];

        $filter['shop_article_sefurl'] = [
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                $this->shopArticleSefurlValidator->setSelfArticleId($this->get('shop_article_id')->getValue())
            ],
        ];

        $filter['shop_article_desc'] = [
            'required' => false,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'StringLength',
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 32000,
                    ],
                ],
            ],
        ];

        $filter['shop_article_hexcolor'] = [
            'required' => false,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                new HexColor()
            ],
        ];

        $filter['shop_article_meta_title'] = [
            'required' => false,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'StringLength',
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 32000,
                    ],
                ],
            ],
        ];

        $filter['shop_article_meta_desc'] = [
            'required' => false,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'StringLength',
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 32000,
                    ],
                ],
            ],
        ];

        $filter['shop_article_meta_keywords'] = [
            'required' => false,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'StringLength',
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 32000,
                    ],
                ],
            ],
        ];

        $filter['shop_article_active'] = [
            'required' => true,
            'filters' => [
                ['name' => 'Digits'],
            ],
            'validators' => [
                [
                    'name' => 'InArray',
                    'options' => [
                        'haystack' => [0, 1],
                    ],
                ],
            ],
        ];

        $filter['shop_article_active_in_categories'] = [
            'required' => true,
            'filters' => [
                ['name' => 'Digits'],
            ],
            'validators' => [
                [
                    'name' => 'InArray',
                    'options' => [
                        'haystack' => [0, 1],
                    ],
                ],
            ],
        ];

        $filter['shop_article_active_in_search'] = [
            'required' => true,
            'filters' => [
                ['name' => 'Digits'],
            ],
            'validators' => [
                [
                    'name' => 'InArray',
                    'options' => [
                        'haystack' => [0, 1],
                    ],
                ],
            ],
        ];

        $filter['shop_article_stock_using'] = [
            'required' => true,
            'filters' => [
                ['name' => 'Digits'],
            ],
            'validators' => [
                [
                    'name' => 'InArray',
                    'options' => [
                        'haystack' => [0, 1],
                    ],
                ],
            ],
        ];

        $filter['shop_article_feeable'] = [
            'required' => true,
            'filters' => [
                ['name' => 'Digits'],
            ],
            'validators' => [
                [
                    'name' => 'InArray',
                    'options' => [
                        'haystack' => [0, 1],
                    ],
                ],
            ],
        ];

        return $filter;
    }

}
