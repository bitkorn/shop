<?php

namespace Bitkorn\Shop\Form\Article;

use Laminas\Form\Form;

/**
 *
 * @author allapow
 */
class ShopArticleSizeGroupForm extends Form implements \Laminas\InputFilter\InputFilterProviderInterface
{

    private $shopArticleSizeGroupIdRequired = true;
    private $shopArticleSizeGroupId = 0;

    public function init()
    {
        $this->setAttribute('enctype', 'multipart/form-data');

        $this->add(array(
            'name' => 'shop_article_size_group_id',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));

        $shopArticleSizeGroupName = new \Laminas\Form\Element\Text('shop_article_size_group_name');
        $shopArticleSizeGroupName->setLabel('Größen-Gruppen Name (interne Verwendung)');
        $shopArticleSizeGroupName->setAttributes([
            'class' => 'w3-input',
        ]);
        $this->add($shopArticleSizeGroupName);

        $shopArticleSizeGroupDesc = new \Laminas\Form\Element\Textarea('shop_article_size_group_desc');
        $shopArticleSizeGroupDesc->setLabel('Größen-Gruppen Beschreibung (interne Verwendung)');
        $shopArticleSizeGroupDesc->setAttributes([
            'class' => 'w3-input w3-border'
        ]);
        $this->add($shopArticleSizeGroupDesc);

        $shopArticleSizeGroupHeading = new \Laminas\Form\Element\Text('shop_article_size_group_heading');
        $shopArticleSizeGroupHeading->setLabel('Größen-Gruppen Überschrift (Anzeige im Frontend)');
        $shopArticleSizeGroupHeading->setAttributes([
            'class' => 'w3-input',
        ]);
        $this->add($shopArticleSizeGroupHeading);

        $shopArticleSizeGroupText = new \Laminas\Form\Element\Textarea('shop_article_size_group_text');
        $shopArticleSizeGroupText->setLabel('Größen-Gruppen Text (Anzeige im Frontend)');
        $shopArticleSizeGroupText->setAttributes([
            'class' => 'w3-input w3-border'
        ]);
        $this->add($shopArticleSizeGroupText);

        /*
         * Image
         */
//        $this->add(array(
//            'name' => 'shop_article_size_group_image_filename',
//            'attributes' => array(
//                'type' => 'hidden',
//            ),
//        ));

        $shopArticleSizeGroupImage = new \Laminas\Form\Element\File('shop_article_size_group_image_image');
        $shopArticleSizeGroupImage->setLabel('Ein Bild zur Größen-Gruppen Beschreibung (Anzeige im Frontend) ...vorhandenes wird überschrieben');
        $shopArticleSizeGroupImage->setAttributes([
            'class' => 'w3-input w3-border',
            'accept' => 'image/png'
        ]);
        $this->add($shopArticleSizeGroupImage);

        $submit = new \Laminas\Form\Element\Submit('submit');
        $submit->setValue('speichern');
        $submit->setAttributes([
            'class' => 'w3-button w3-grey',
        ]);
        $this->add($submit);
    }

    function __construct($name = 'shop_article_size_group')
    {
        parent::__construct($name);
    }

    public function getInputFilterSpecification()
    {
        return array(
            'shop_article_size_group_id' => array(
                'required' => $this->shopArticleSizeGroupIdRequired,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'Digits',
                    ),
                ),
            ),
            'shop_article_size_group_name' => array(
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array('name' => 'HtmlEntities'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 80,
                        ),
                    ),
                ),
            ),
            'shop_article_size_group_desc' => array(
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 32000,
                        ),
                    ),
                ),
            ),
//            'shop_article_size_group_image_filename' => array(
//                'required' => false, // @todo generieren
//                'filters' => array(
//                    array('name' => 'StripTags'),
//                    array('name' => 'StringTrim'),
//                    array('name' => 'HtmlEntities'),
//                ),
//                'validators' => array(
//                    array(
//                        'name' => 'StringLength',
//                        'options' => array(
//                            'encoding' => 'UTF-8',
//                            'min' => 1,
//                            'max' => 200,
//                        ),
//                    ),
//                ),
//            ),
            'shop_article_size_group_image_image' => array(
                'required' => false, // false weil vielleicht von einem auch nur Text geaendert werden soll
                'filters' => array(
//                    array('name' => 'FileInput'),
                ),
                'validators' => array(
                        [
                        'name' => 'filemimetype',
                        'break_chain_on_failure' => TRUE,
                        'options' => [
                            'mimeType' => 'image/png',
                            'magicFile' => FALSE, // wegen: exit signal Segmentation fault (11), possible coredump in /etc/apache2
                            'messages' => array(
                                \Laminas\Validator\File\MimeType::FALSE_TYPE => "Falsches Dateiformat '%type%', nur PNG!",
                            ),
                        ]
                    ],
//                        [
//                        'name' => 'fileimagesize',
//                        'options' => array(
//                            'maxWidth' => 1600, // 1600, 1440, 1280, 1120, 960
//                            'maxHeight' => 1600, // 900, 810, 720, 630, 540
//                            'messages' => array(
//                                \Laminas\Validator\File\ImageSize::HEIGHT_TOO_BIG => "Das Bild ist mit %height% zu hoch, max=%maxheight%!",
//                                \Laminas\Validator\File\ImageSize::WIDTH_TOO_BIG => "Das Bild ist mit %width% zu breit, max=%maxwidth%!",
//                            ),
//                        ),
//                    ],
                ),
            ),
        );
    }

    /**
     * 
     * @param boolean $shopArticleSizeGroupIdRequired
     */
    public function setShopArticleSizeGroupIdRequired($shopArticleSizeGroupIdRequired)
    {
        $this->shopArticleSizeGroupIdRequired = $shopArticleSizeGroupIdRequired;
    }

    /**
     * 
     * @param int $shopArticleSizeGroupId
     */
    public function setShopArticleSizeGroupId($shopArticleSizeGroupId)
    {
        $this->shopArticleSizeGroupId = $shopArticleSizeGroupId;
    }

    
}

?>
