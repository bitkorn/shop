<?php

namespace Bitkorn\Shop\Form\Article\Category;

use Bitkorn\Trinket\Validator\HexColor;
use Laminas\Form\Element\File;
use Laminas\Form\Element\Select;
use Laminas\Form\Element\Submit;
use Laminas\Form\Element\Text;
use Laminas\Form\Element\Textarea;
use Laminas\Form\Form;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\File\MimeType;
use Laminas\Validator\InArray;

/**
 * Description of ShopArticleCategoryForm
 *
 * @author allapow
 */
class ShopArticleCategoryForm extends Form implements InputFilterProviderInterface
{

    /**
     *
     * @var boolean
     */
    private $shopArticleCategoryIdRequired = true;

    /**
     *
     * @var array
     */
    private $shopArticleCategoryIdAssoc;

    /**
     *
     * @param boolean $shopArticleCategoryIdRequired
     */
    public function setShopArticleCategoryIdRequired($shopArticleCategoryIdRequired)
    {
        $this->shopArticleCategoryIdRequired = $shopArticleCategoryIdRequired;
    }

    /**
     * To validate and for valueOptions
     * @param array $shopArticleCategoryIdAssoc
     */
    public function setShopArticleCategoryIdAssoc(array $shopArticleCategoryIdAssoc)
    {
        $this->shopArticleCategoryIdAssoc = $shopArticleCategoryIdAssoc;
    }

    function __construct($name = 'shop_article_category')
    {
        parent::__construct($name);
    }

    public function init()
    {

        $this->add([
            'type' => 'hidden',
            'name' => 'shop_article_category_id',
            'attributes' => [
            ],
        ]);

        $this->add([
            'type' => 'text',
            'name' => 'shop_article_category_name',
            'attributes' => [
                'title' => '',
                'class' => 'w3-input'
            ],
            'options' => [
                'label' => 'Name',
            ],
        ]);

        $this->add([
            'type' => 'text',
            'name' => 'shop_article_category_alias',
            'attributes' => [
                'title' => 'wenn leer dann automatisch aus Name',
                'class' => 'w3-input'
            ],
            'options' => [
                'label' => 'sef URL',
            ],
        ]);

        $this->add([
            'type' => 'textarea',
            'name' => 'shop_article_category_desc',
            'attributes' => [
                'title' => '',
                'id' => 'wysiwyg'
            ],
            'options' => [
                'label' => 'Beschreibung',
            ],
        ]);

        $this->add([
            'type' => File::class,
            'name' => 'shop_article_category_img',
            'attributes' => [
                'class' => 'w3-input',
                'accept' => 'image/jpg,image/jpeg,image/png',
                'disabled' => true
            ],
            'options' => [
                'label' => 'Bild (coming soon)',
            ],
        ]);

        $this->add([
            'type' => Text::class,
            'name' => 'shop_article_category_hexcolor',
            'attributes' => [
                'class' => 'w3-input colorpicker'
            ],
            'options' => [
                'label' => 'Farbe',
            ],
        ]);

        $this->add([
            'type' => Select::class,
            'name' => 'shop_article_category_id_parent',
            'attributes' => [
                'class' => 'w3-input'
            ],
            'options' => [
                'label' => 'Parent',
                'value_options' => $this->shopArticleCategoryIdAssoc
            ],
        ]);

        $this->add([
            'type' => Text::class,
            'name' => 'shop_article_category_meta_title',
            'attributes' => [
                'class' => 'w3-input'
            ],
            'options' => [
                'label' => 'META title',
            ],
        ]);

        $this->add([
            'type' => Textarea::class,
            'name' => 'shop_article_category_meta_desc',
            'attributes' => [
                'class' => 'w3-input'
            ],
            'options' => [
                'label' => 'META Beschreibung',
            ],
        ]);

        $this->add([
            'type' => Textarea::class,
            'name' => 'shop_article_category_meta_keywords',
            'attributes' => [
                'class' => 'w3-input'
            ],
            'options' => [
                'label' => 'META Keywords',
            ],
        ]);

        $this->add([
            'type' => Submit::class,
            'name' => 'submit',
            'attributes' => [
                'class' => 'w3-button w3-grey',
                'value' => 'speichern'
            ]
        ]);
    }

    public function getInputFilterSpecification()
    {
        return [
            'shop_article_category_id' => [
                'required' => $this->shopArticleCategoryIdRequired,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'Digits',
                    ],
                ],
            ],
            'shop_article_category_name' => [
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 100,
                        ],
                    ],
                ],
            ],
            'shop_article_category_alias' => [
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 100,
                        ],
                    ],
                ],
            ],
            'shop_article_category_desc' => [
                'required' => false,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 10000,
                        ],
                    ],
                ],
            ],
            'shop_article_category_img' => [
                'required' => false,
                'filters' => [
                ],
                'validators' => [
                    [
                        'name' => 'filemimetype',
                        'break_chain_on_failure' => TRUE,
                        'options' => [
                            'mimeType' => 'image/jpeg, image/png',
                            'magicFile' => FALSE, // wegen: exit signal Segmentation fault (11), possible coredump in /etc/apache2
                            'messages' => [
                                MimeType::FALSE_TYPE => "Falsches Dateiformat '%type%', nur jpeg/jpg | image/png!",
                            ],
                        ]
                    ],
//                        [
//                        'name' => 'fileimagesize',
//                        'options' => array(
//                            'maxWidth' => 1600, // 1600, 1440, 1280, 1120, 960
//                            'maxHeight' => 1600, // 900, 810, 720, 630, 540
//                            'messages' => array(
//                                \Laminas\Validator\File\ImageSize::HEIGHT_TOO_BIG => "Das Bild ist mit %height% zu hoch, max=%maxheight%!",
//                                \Laminas\Validator\File\ImageSize::WIDTH_TOO_BIG => "Das Bild ist mit %width% zu breit, max=%maxwidth%!",
//                            ),
//                        ),
//                    ],
                ],
            ],
            'shop_article_category_hexcolor' => [
                'required' => false,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    new HexColor()
                ],
            ],
            'shop_article_category_id_parent' => [
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'Explode',
                        'options' => [
                            'validator' => new InArray([
                                'haystack' => array_keys($this->shopArticleCategoryIdAssoc),
                                'valueDelimeter' => null,
                            ])
                        ],
                    ],
                ],
            ],
            'shop_article_category_meta_title' => [
                'required' => false,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 200,
                        ],
                    ],
                ],
            ],
            'shop_article_category_meta_desc' => [
                'required' => false,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 32000,
                        ],
                    ],
                ],
            ],
            'shop_article_category_meta_keywords' => [
                'required' => false,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 32000,
                        ],
                    ],
                ],
            ],
        ];
    }

}
