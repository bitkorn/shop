<?php

namespace Bitkorn\Shop\Form\Filter;

use Laminas\InputFilter\InputFilter;

class PasswordFilter extends InputFilter
{
    public function __construct()
    {
        $this->add([
            'name' => 'passwd',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim']
            ],
            'validators' => [
                [
                    'name' => 'StringLength',
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 8,
                        'max' => 45,
//                        'messages' => array(
//                            \Laminas\Validator\StringLength::TOO_SHORT => '...zu kurz ...muß mindestens %min% Zeichen lang sein!',
//                            \Laminas\Validator\StringLength::TOO_LONG => '...zu lang ...darf längstens %max% Zeichen lang sein!',
//                        ),
                    ]],
            ],
        ]);
        $this->add([
            'name' => 'passwd2',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim']
            ],
            'validators' => [
                [
                    'name' => 'Identical',
                    'options' => [
                        'token' => 'passwd',
//                        'messages' => array(
//                            \Laminas\Validator\Identical::NOT_SAME => 'Das ist nicht identisch!',
//                            \Laminas\Validator\Identical::MISSING_TOKEN => 'Das erste ist leer!',
//                        ),
                    ]],
            ],
        ]);
    }

}
