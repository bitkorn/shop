<?php

namespace Bitkorn\Shop\Form\User;

use Bitkorn\Shop\Form\Fieldset\PasswordFieldset;
use Bitkorn\Shop\Form\Filter\PasswordFilter;
use Laminas\Form\Form;

/**
 *
 * @author allapow
 */
class RegisterForm extends Form
{

    public function __construct($name = 'register_form')
    {
        parent::__construct($name);

        /**
         * no filter or validation for this element
         */
        $this->add([
            'name' => 'email',
            'attributes' => [
                'type' => 'text',
            ]
        ]);

        $passwdFieldset = new PasswordFieldset('passwd_fieldset');
        $passwd1 = $passwdFieldset->get('passwd');
        $passwd1->setLabel('');
        $passwd1->setAttribute('placeholder', 'Ihr Passwort');
        $this->add($passwd1);

        $passwd2 = $passwdFieldset->get('passwd2');
        $passwd2->setLabel('');
        $passwd2->setAttribute('placeholder', 'Ihr Passwort bestätigen');
        $this->add($passwd2);

        $this->setInputFilter(new PasswordFilter());
    }

}
