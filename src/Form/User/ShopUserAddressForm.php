<?php

namespace Bitkorn\Shop\Form\User;

use Bitkorn\Trinket\Form\Element\Text;
use Bitkorn\Trinket\Validator\IntegerValidator;
use Laminas\Filter\Digits;
use Laminas\Filter\HtmlEntities;
use Laminas\Filter\StringTrim;
use Laminas\Filter\StripTags;
use Laminas\Form\Element\Select;
use Laminas\Form\Element\Submit;
use Laminas\Form\Form;
use Bitkorn\Shop\Form\Constants;
use Laminas\InputFilter\InputFilterProviderInterface;

/**
 *
 * @author allapow
 */
class ShopUserAddressForm extends Form implements InputFilterProviderInterface
{

    /**
     *
     * @var string
     */
    private $formName = '';

    /**
     *
     * @var string
     */
    private $addressType = 'invoice';

    /**
     * @var boolean
     */
    private $shopUserAddressIdRequired = false;

    /**
     *
     * @var string
     */
    private $userId;

    /**
     *
     * @var array
     */
    private $isoCountryIdAssoc;

    /**
     * @var boolean
     */
    private $salutationEnabled = true;

    /**
     * To validate setCustomerType & form element
     * @var array
     */
    private $customerTypeIdAssoc;

    /**
     * @var string
     */
    private $customerType = '';

    /**
     * @var boolean
     */
    private $birthdayEnabled = true;

    /**
     * @var boolean
     */
    private $taxIdEnabled = true;

    /**
     * @var boolean
     */
    private $companyNameEnabled = true;

    /**
     * @var boolean
     */
    private $companyDepartmentEnabled = true;

    /**
     * @var boolean
     */
    private $submitDeleteEnabled = false;

    /**
     *
     * @param string $name
     */
    function __construct($name = 'shop_user_address')
    {
        $this->formName = $name;
        parent::__construct($name);
    }

    /**
     *
     */
    public function init()
    {

        $this->add([
            'name' => 'form_name',
            'attributes' => [
                'type' => 'hidden',
                'value' => $this->formName
            ],
        ]);

        $this->add([
            'name' => 'shop_user_address_id',
            'attributes' => [
                'type' => 'hidden',
            ],
        ]);

        $this->add([
            'name' => 'user_uuid',
            'attributes' => [
                'type' => 'hidden',
                'value' => $this->userId
            ],
        ]);

        $this->add([
            'type' => Select::class,
            'name' => 'shop_user_address_customer_type',
            'attributes' => [
                'class' => 'w3-input customer-type-select-' . $this->addressType,
                'value' => $this->customerType
            ],
            'options' => [
                'label' => '',
                'value_options' => $this->customerTypeIdAssoc
            ],
        ]);

        if ($this->salutationEnabled) {
            $this->add([
                'type' => Select::class,
                'name' => 'shop_user_address_salut',
                'attributes' => [
//                    'class' => 'w3-input-no-width',
                    'class' => 'w3-input',
//                    'style' => 'width:19%'
                ],
                'options' => [
                    'value_options' => Constants::$salutationValueOptions,
                ],
            ]);
            $this->add([
                'type' => Text::class,
                'name' => 'shop_user_address_degree',
                'attributes' => [
//                    'class' => 'w3-input-no-width w3-right',
                    'class' => 'w3-input w3-right',
                    'placeholder' => 'Titel',
//                    'style' => 'width:79%'
                ]
            ]);
        }

        $shopUserAddressName1 = new Text('shop_user_address_name1');
//        $shopUserAddressName1->setLabel('* Vorname');
        $shopUserAddressName1->setAttributes([
            'class' => 'w3-input',
            'placeholder' => '* Vorname',
        ]);
        $this->add($shopUserAddressName1);

        $shopUserAddressName2 = new Text('shop_user_address_name2');
//        $shopUserAddressName2->setLabel('* Nachname');
        $shopUserAddressName2->setAttributes([
            'class' => 'w3-input',
            'placeholder' => '* Nachname',
        ]);
        $this->add($shopUserAddressName2);

        $shopUserAddressStreet = new Text('shop_user_address_street');
//        $shopUserAddressStreet->setLabel('* Straße');
        $shopUserAddressStreet->setAttributes([
//            'class' => 'w3-input-no-width',
            'class' => 'w3-input',
            'placeholder' => '* Straße',
//            'style' => 'width: 79%'
        ]);
        $this->add($shopUserAddressStreet);

        $shopUserAddressStreetNo = new Text('shop_user_address_street_no');
//        $shopUserAddressStreetNo->setLabel('* Hausnummer');
        $shopUserAddressStreetNo->setAttributes([
//            'class' => 'w3-input-no-width w3-right',
            'class' => 'w3-input',
            'placeholder' => '* Nr.',
//            'style' => 'width: 19%'
        ]);
        $this->add($shopUserAddressStreetNo);

        $shopUserAddressZip = new Text('shop_user_address_zip');
//        $shopUserAddressZip->setLabel('* Postleitzahl');
        $shopUserAddressZip->setAttributes([
//            'class' => 'w3-input-no-width',
            'class' => 'w3-input',
            'placeholder' => '* PLZ',
//            'style' => 'width: 19%'
        ]);
        $this->add($shopUserAddressZip);

        $shopUserAddressCity = new Text('shop_user_address_city');
//        $shopUserAddressCity->setLabel('* Ort');
        $shopUserAddressCity->setAttributes([
//            'class' => 'w3-input-no-width w3-right',
            'class' => 'w3-input',
            'placeholder' => '* Ort',
//            'style' => 'width: 79%'
        ]);
        $this->add($shopUserAddressCity);

        $this->add([
            'type' => Text::class,
            'name' => 'shop_user_address_additional',
            'attributes' => [
                'title' => '',
                'class' => 'w3-input',
                'placeholder' => 'Adresszusatz'
            ],
        ]);

        $isoCountryId = new Select('country_id');
//        $isoCountryId->setLabel('* Land');
        $isoCountryId->setAttributes([
            'class' => 'w3-input',
        ]);
        $isoCountryId->setValueOptions($this->isoCountryIdAssoc);
        $isoCountryId->setValue(48);
        $this->add($isoCountryId);

        $shopUserAddressTel = new Text('shop_user_address_tel');
//        $shopUserAddressTel->setLabel('Telefon');
        $shopUserAddressTel->setAttributes([
            'class' => 'w3-input',
            'placeholder' => 'Telefonnummer',
        ]);
        $this->add($shopUserAddressTel);

        if ($this->birthdayEnabled) {
            $this->add([
                'type' => 'text',
                'name' => 'shop_user_address_birthday',
                'attributes' => [
                    'title' => '',
                    'class' => 'w3-input datepicker'
                ],
                'options' => [
                    'label' => 'Geburtstag',
                ],
            ]);
        }

        if ($this->taxIdEnabled) {
            $shopUserAddressTaxId = new Text('shop_user_address_tax_id');
//            $shopUserAddressTaxId->setLabel('USt.ID');
            $shopUserAddressTaxId->setAttributes([
                'class' => 'w3-input',
                'placeholder' => '* Umsatzsteuer-ID'
            ]);
            $this->add($shopUserAddressTaxId);
        }

        if ($this->companyNameEnabled) {
            $this->add([
                'type' => Text::class,
                'name' => 'shop_user_address_company_name',
                'attributes' => [
                    'title' => '',
                    'class' => 'w3-input',
                    'placeholder' => '* Firma'
                ],
                'options' => [
//                    'label' => 'Firma',
                ],
            ]);
        }

        if ($this->companyDepartmentEnabled) {
            $this->add([
                'type' => Text::class,
                'name' => 'shop_user_address_company_department',
                'attributes' => [
                    'title' => '',
                    'class' => 'w3-input',
                    'placeholder' => 'Abteilung'
                ],
                'options' => [
//                    'label' => 'Abteilung',
                ],
            ]);
        }

        $submit = new Submit('submit');
        $submit->setValue('speichern');
        $submit->setAttributes([
            'class' => 'w3-button bk-success'
        ]);
        $this->add($submit);

        if ($this->submitDeleteEnabled) {
            $this->add([
                'type' => Submit::class,
                'name' => 'submit_delete',
                'attributes' => [
                    'class' => 'w3-button bk-warn',
                    'value' => 'löschen',
                ]
            ]);
        }
    }

    public function getInputFilterSpecification()
    {
        $specs = [];
        $specs['shop_user_address_id'] = [
            'required' => $this->shopUserAddressIdRequired,
            'filters' => [
                ['name' => StripTags::class],
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => Digits::class],
            ]
        ];
        $specs['user_uuid'] = [
            'required' => true,
            'filters' => [
                ['name' => StripTags::class],
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
            ]
        ];
        $specs['shop_user_address_customer_type'] = [
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim']
            ],
            'validators' => [
                [
                    'name' => 'InArray',
                    'options' => [
                        'haystack' => array_keys($this->customerTypeIdAssoc)
                    ]
                ]
            ]
        ];
        if ($this->salutationEnabled) {
            $specs['shop_user_address_salut'] = [
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim']
                ],
                'validators' => [
                    [
                        'name' => 'InArray',
                        'options' => [
                            'haystack' => array_keys(Constants::$salutationValueOptions)
                        ]
                    ]
                ]
            ];
            $specs['shop_user_address_degree'] = [
                'required' => false,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                    ['name' => 'HtmlEntities'],
                ],
                'validators' => [
                    [
                        'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 14,
                        ],
                    ],
                ]
            ];
        }
        $specs['shop_user_address_name1'] = [
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
                ['name' => 'HtmlEntities'],
            ],
            'validators' => [
                [
                    'name' => 'StringLength',
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 80,
                    ],
                ],
            ]
        ];
        $specs['shop_user_address_name2'] = [
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
                ['name' => 'HtmlEntities'],
            ],
            'validators' => [
                [
                    'name' => 'StringLength',
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 80,
                    ],
                ],
            ]
        ];
        $specs['shop_user_address_street'] = [
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
                ['name' => 'HtmlEntities']
            ],
            'validators' => [
                [
                    'name' => 'StringLength',
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 80,
                    ]
                ]
            ]
        ];
        $specs['shop_user_address_street_no'] = [
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
                ['name' => 'HtmlEntities'],
            ],
            'validators' => [
                [
                    'name' => 'StringLength',
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 10,
                    ],
                ],
            ]
        ];
        $specs['shop_user_address_zip'] = [
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
                ['name' => 'HtmlEntities'],
            ],
            'validators' => [
                [
                    'name' => 'StringLength',
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 8,
                    ],
                ],
            ]
        ];
        $specs['shop_user_address_city'] = [
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
                ['name' => 'HtmlEntities'],
            ],
            'validators' => [
                [
                    'name' => 'StringLength',
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 80,
                    ],
                ],
            ]
        ];
        $specs['shop_user_address_additional'] = [
            'required' => false,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
                ['name' => 'HtmlEntities'],
            ],
            'validators' => [
                [
                    'name' => 'StringLength',
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 80,
                    ],
                ],
            ]
        ];
        $specs['country_id'] = [
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'InArray',
                    'options' => [
                        'haystack' => array_keys($this->isoCountryIdAssoc),
                    ],
                ],
            ]
        ];
        $specs['shop_user_address_tel'] = [
            'required' => false,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
                ['name' => 'HtmlEntities'],
            ],
            'validators' => [
                [
                    'name' => 'StringLength',
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 5,
                        'max' => 40,
                    ],
                ],
            ]
        ];
        if ($this->birthdayEnabled) {
            $specs['shop_user_address_birthday'] = [
                'required' => false,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim']
                ],
                'validators' => [
                    new IntegerValidator()
                ]
            ];
        }
        if ($this->taxIdEnabled) {
            $specs['shop_user_address_tax_id'] = [
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                    ['name' => 'HtmlEntities'],
                ],
                'validators' => [
                    [
                        'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min' => 8,
                            'max' => 12,
                        ],
                    ],
                ]
            ];
        }
        if ($this->companyNameEnabled) {
            $specs['shop_user_address_company_name'] = [
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                    ['name' => 'HtmlEntities'],
                ],
                'validators' => [
                    [
                        'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min' => 2,
                            'max' => 70,
                        ],
                    ],
                ]
            ];
        }
        if ($this->companyDepartmentEnabled) {
            $specs['shop_user_address_company_department'] = [
                'required' => false,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                    ['name' => 'HtmlEntities'],
                ],
                'validators' => [
                    [
                        'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min' => 2,
                            'max' => 70,
                        ],
                    ],
                ]
            ];
        }
        return $specs;
    }

    /**
     *
     * @param string $addressType invoice | enterprise
     */
    public function setAddressType($addressType)
    {
        $this->addressType = $addressType;
    }

    /**
     *
     * @param boolean $shopUserAddressIdRequired
     */
    public function setShopUserAddressIdRequired($shopUserAddressIdRequired)
    {
        $this->shopUserAddressIdRequired = $shopUserAddressIdRequired;
    }

    public function setUserId($userUuid)
    {
        $this->userId = $userUuid;
    }

    /**
     *
     * @param array $isoCountryIdAssoc
     */
    public function setIsoCountryIdAssoc(array $isoCountryIdAssoc)
    {
        $this->isoCountryIdAssoc = $isoCountryIdAssoc;
    }

    /**
     *
     * @param boolean $salutationEnabled
     */
    public function setSalutationEnabled($salutationEnabled)
    {
        $this->salutationEnabled = $salutationEnabled;
    }

    /**
     *
     * @param array $customerTypeIdAssoc
     */
    public function setCustomerTypeIdAssoc($customerTypeIdAssoc)
    {
        foreach ($customerTypeIdAssoc as $customerType) {
            $this->customerTypeIdAssoc[$customerType] = $customerType;
        }
    }

    /**
     *
     * @param string $customerType
     */
    public function setCustomerType($customerType)
    {
        $this->customerType = $customerType;
    }

    /**
     *
     * @param boolean $birthdayEnabled
     */
    public function setBirthdayEnabled($birthdayEnabled)
    {
        $this->birthdayEnabled = $birthdayEnabled;
    }

    /**
     *
     * @param boolean $taxIdEnabled
     */
    public function setTaxIdEnabled($taxIdEnabled)
    {
        $this->taxIdEnabled = $taxIdEnabled;
    }

    /**
     *
     * @param boolean $companyNameEnabled
     */
    public function setCompanyNameEnabled($companyNameEnabled)
    {
        $this->companyNameEnabled = $companyNameEnabled;
    }

    /**
     *
     * @param boolean $companyDepartmentEnabled
     */
    public function setCompanyDepartmentEnabled($companyDepartmentEnabled)
    {
        $this->companyDepartmentEnabled = $companyDepartmentEnabled;
    }

    /**
     *
     * @param bool $submitDeleteEnabled
     */
    public function setSubmitDeleteEnabled($submitDeleteEnabled)
    {
        $this->submitDeleteEnabled = $submitDeleteEnabled;
    }

    public function setName($name)
    {
        $this->formName = $name;
        return parent::setName($name);
    }

    /**
     *
     * @param string $targetMode string | integer
     * @return boolean
     */
    public function switchDate($targetMode)
    {
        $birthday = $this->get('shop_user_address_birthday')->getValue();
        if ($targetMode == 'string') {
            if (empty($birthday)) {
                $this->get('shop_user_address_birthday')->setValue('');
                if (isset($this->data['shop_user_address_birthday'])) {
                    $this->data['shop_user_address_birthday'] = '';
                }
                return true;
            }
            if (is_numeric($birthday)) {
                $this->get('shop_user_address_birthday')->setValue(date('d.m.Y', $birthday));
                if (isset($this->data['shop_user_address_birthday'])) {
                    $this->data['shop_user_address_birthday'] = date('d.m.Y', $birthday);
                }
                return true;
            }
        } elseif ($targetMode == 'integer') {
            if (empty($birthday)) {
                if (isset($this->data['shop_user_address_birthday'])) {
                    $this->data['shop_user_address_birthday'] = 0;
                }
                return true;
            }
            $dateTime = new \DateTime($birthday);
            if ($dateTime) {
                $this->get('shop_user_address_birthday')->setValue($dateTime->getTimestamp());
                if (isset($this->data['shop_user_address_birthday'])) {
                    $this->data['shop_user_address_birthday'] = $dateTime->getTimestamp();
                }
                return true;
            }
        }
        return FALSE;
    }

}
