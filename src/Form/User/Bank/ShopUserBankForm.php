<?php

namespace Bitkorn\Shop\Form\User\Bank;

use Laminas\Form\Form;

/**
 *
 * @author allapow
 */
class ShopUserBankForm extends Form implements \Laminas\InputFilter\InputFilterProviderInterface
{

    /**
     *
     * @var \Laminas\Log\Logger
     */
    private $logger;
    
    public function init()
    {

        $bankOwnerName = new \Laminas\Form\Element\Text('shop_user_data_bank_owner_name');
        $bankOwnerName->setLabel('Name');
        $bankOwnerName->setAttributes([
            'class' => 'w3-input'
        ]);
        $this->add($bankOwnerName);

        $bankBankName = new \Laminas\Form\Element\Text('shop_user_data_bank_bank_name');
        $bankBankName->setLabel('Kreditinstitut');
        $bankBankName->setAttributes([
            'class' => 'w3-input',
        ]);
        $this->add($bankBankName);

        $bankIban = new \Laminas\Form\Element\Text('shop_user_data_bank_iban');
        $bankIban->setLabel('IBAN');
        $bankIban->setAttributes([
            'class' => 'w3-input',
        ]);
        $this->add($bankIban);

        $bankBic = new \Laminas\Form\Element\Text('shop_user_data_bank_bic');
        $bankBic->setLabel('BIC');
        $bankBic->setAttributes([
            'class' => 'w3-input',
        ]);
        $this->add($bankBic);


        $submit = new \Laminas\Form\Element\Submit('submit_shop_user_data_bank');
        $submit->setValue('speichern');
        $submit->setAttributes([
            'class' => 'w3-button w3-grey',
        ]);
        $this->add($submit);
    }

    function __construct($name = 'shop_user_data_bank')
    {
        parent::__construct($name);
    }

    public function getInputFilterSpecification()
    {
        return array(
            'shop_user_data_bank_owner_name' => array(
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 100,
                        ),
                    ),
                ),
            ),
            'shop_user_data_bank_bank_name' => array(
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 100,
                        ),
                    ),
                ),
            ),
            'shop_user_data_bank_iban' => array(
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'Iban'
                    ),
                ),
            ),
            'shop_user_data_bank_bic' => array(
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 100,
                        ),
                    ),
                ),
            ),
        );
    }
    
    public function setLogger(\Laminas\Log\Logger $logger)
    {
        $this->logger = $logger;
    }


}

?>
