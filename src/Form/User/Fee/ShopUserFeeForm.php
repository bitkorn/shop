<?php

namespace Bitkorn\Shop\Form\User\Fee;

use Bitkorn\Trinket\Validator\FloatValidator;
use Laminas\Form\Form;

/**
 *
 * @author allapow
 */
class ShopUserFeeForm extends Form implements \Laminas\InputFilter\InputFilterProviderInterface
{
    
    /**
     *
     * @var \Laminas\Log\Logger
     */
    private $logger;

    /**
     *
     * @var \Bitkorn\Shop\Table\User\ShopUserDataTable
     */
    private $shopUserDataTable;
    
    /**
     *
     * @var array
     */
    private $salesUserIdAssoc;
    
    /**
     *
     * @var int
     */
    private $userId;
    
    /**
     *
     * @var boolean
     */
    private $readOnly = false;

    /**
     * 
     */
    public function init()
    {

        $this->add(array(
            'name' => 'shop_user_fee_id',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));

        $this->add(array(
            'name' => 'user_uuid',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));

        $feePercent = new \Laminas\Form\Element\Text('shop_user_fee_percent');
        $feePercent->setLabel('Verkäufe');
        $feePercent->setAttributes([
            'class' => 'w3-right-align',
            'readonly' => $this->readOnly
        ]);
        $this->add($feePercent);

        $feeCouponPercent = new \Laminas\Form\Element\Text('shop_user_fee_coupon_percent');
        $feeCouponPercent->setLabel('Verkäufe mit Rabatt-Code');
        $feeCouponPercent->setAttributes([
            'class' => 'w3-right-align',
            'readonly' => $this->readOnly
        ]);
        $this->add($feeCouponPercent);

        $feeStatic = new \Laminas\Form\Element\Text('shop_user_fee_static');
        $feeStatic->setLabel('bestimmte Produkte (Festwert)');
        $feeStatic->setAttributes([
            'class' => 'w3-right-align',
            'readonly' => $this->readOnly
        ]);
        $this->add($feeStatic);

        $this->salesUserIdAssoc = $this->shopUserDataTable->getShopUserByRoleIdAssoc(101, $this->userId);
        $feeMlmParentUserId = new \Laminas\Form\Element\Select('shop_user_fee_mlm_parent_user_uuid');
        $feeMlmParentUserId->setLabel('Provisions Berechtigter (MLM parent)');
        $feeMlmParentUserId->setAttributes([
            'class' => 'w3-select'
        ]);
        $feeMlmParentUserId->setValueOptions($this->salesUserIdAssoc);
        $this->add($feeMlmParentUserId);

        $feeMlmChildFeePercent = new \Laminas\Form\Element\Text('shop_user_fee_mlm_child_fee_percent');
        $feeMlmChildFeePercent->setLabel('Käufe von MLM Kunden');
        $feeMlmChildFeePercent->setAttributes([
            'class' => 'w3-right-align',
            'readonly' => $this->readOnly
        ]);
        $this->add($feeMlmChildFeePercent);

        $feeMlmChildFeePercentFirst = new \Laminas\Form\Element\Text('shop_user_fee_mlm_child_fee_percent_first');
        $feeMlmChildFeePercentFirst->setLabel('erster Kauf von MLM Kunden');
        $feeMlmChildFeePercentFirst->setAttributes([
            'class' => 'w3-right-align',
            'readonly' => $this->readOnly
        ]);
        $this->add($feeMlmChildFeePercentFirst);

        $feeMlmChildFeeCouponPercent = new \Laminas\Form\Element\Text('shop_user_fee_mlm_child_fee_coupon_percent');
        $feeMlmChildFeeCouponPercent->setLabel('Verkäufe mit Rabatt-Code von MLM Kunden');
        $feeMlmChildFeeCouponPercent->setAttributes([
            'class' => 'w3-right-align',
            'readonly' => $this->readOnly
        ]);
        $this->add($feeMlmChildFeeCouponPercent);

        $submit = new \Laminas\Form\Element\Submit('shop_user_fee_submit');
        $submit->setValue('speichern');
        $submit->setAttributes([
            'class' => 'w3-button bk-grey-light',
        ]);
        $this->add($submit);
    }

    function __construct($name = 'shop_user_fee')
    {
        parent::__construct($name);
    }

    public function getInputFilterSpecification()
    {
        return array(
            'shop_user_fee_id' => array(
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'Digits'
                    ),
                ),
            ),
            'user_uuid' => array(
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'Digits'
                    ),
                ),
            ),
            'shop_user_fee_percent' => array(
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    new FloatValidator()
                ),
            ),
            'shop_user_fee_coupon_percent' => array(
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    new FloatValidator()
                ),
            ),
            'shop_user_fee_static' => array(
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    new FloatValidator()
                ),
            ),
            'shop_user_fee_mlm_parent_user_uuid' => array(
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'InArray',
                        'options' => array(
                            'haystack' => array_keys($this->salesUserIdAssoc),
                        ),
                    ),
                ),
            ),
            'shop_user_fee_mlm_child_fee_percent' => array(
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    new FloatValidator()
                ),
            ),
            'shop_user_fee_mlm_child_fee_percent_first' => array(
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    new FloatValidator()
                ),
            ),
            'shop_user_fee_mlm_child_fee_coupon_percent' => array(
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    new FloatValidator()
                ),
            ),
        );
    }
    
    public function setLogger(\Laminas\Log\Logger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * 
     * @param \Bitkorn\Shop\Table\User\ShopUserDataTable $shopUserDataTable
     */
    public function setShopUserDataTable(\Bitkorn\Shop\Table\User\ShopUserDataTable $shopUserDataTable)
    {
        $this->shopUserDataTable = $shopUserDataTable;
    }

    /**
     * 
     * @param int $userId User-ID for form element user_uuid & to exclude it for form select(shop_user_fee_mlm_parent_user_uuid)
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    public function setReadOnly($readOnly)
    {
        $this->readOnly = $readOnly;
    }

}

?>
