<?php

namespace Bitkorn\Shop\Form\User;

use Bitkorn\User\Validator\UsernameValidator;
use Laminas\Form\Element\Email;
use Laminas\Form\Element\Password;
use Laminas\Form\Element\Select;
use Laminas\Form\Element\Submit;
use Laminas\Form\Element\Text;
use Laminas\Form\Form;
use Laminas\Validator\Identical;

/**
 *
 * @author allapow
 */
class ShopUserCreateForm extends Form implements \Laminas\InputFilter\InputFilterProviderInterface
{

    /**
     *
     * @var array
     */
    private $shopUserGroupsIdAssoc;

    /**
     *
     * @param array $shopUserGroupsIdAssoc
     */
    public function setShopUserGroupsIdAssoc(array $shopUserGroupsIdAssoc)
    {
        $this->shopUserGroupsIdAssoc = $shopUserGroupsIdAssoc;
    }

    public function init()
    {
        $shopUserGroup = new Select('shop_user_group_id');
        $shopUserGroup->setLabel('Gruppe');
        $shopUserGroup->setAttributes([
            'class' => 'w3-select',
        ]);
        $shopUserGroup->setValueOptions($this->shopUserGroupsIdAssoc);
        $this->add($shopUserGroup);

        $login = new Text('user_login');
        $login->setLabel('Login');
        $login->setAttributes([
            'class' => 'w3-input',
        ]);
        $this->add($login);

        $email = new Email('user_email');
        $email->setLabel('Email');
        $email->setAttributes([
            'class' => 'w3-input',
        ]);
        $this->add($email);

        $passwd = new Password('user_passwd');
        $passwd->setLabel('Passwort');
        $passwd->setAttributes([
            'class' => 'w3-input',
        ]);
        $this->add($passwd);

        $passwd2 = new Password('user_passwd_confirm');
        $passwd2->setLabel('wiederholen');
        $passwd2->setAttributes([
            'class' => 'w3-input',
        ]);
        $this->add($passwd2);

        $submit = new Submit('submit');
        $submit->setValue('speichern');
        $submit->setAttributes([
            'class' => 'w3-button w3-grey',
        ]);
        $this->add($submit);
    }

    function __construct($name = 'shop_user')
    {
        parent::__construct($name);
    }

    public function getInputFilterSpecification()
    {
        return [
            'shop_user_group_id' => [
                'required' => true,
                'filters' => [
                ],
                'validators' => [
                    ['name' => 'NotEmpty'],
                    [
                        'name' => 'InArray',
                        'options' => [
                            'haystack' => array_keys($this->shopUserGroupsIdAssoc),
                        ],
                    ],
                ],
            ],
            'user_login' => [
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    new UsernameValidator()
                ],
            ],
            'user_email' => [
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
//                        'break_chain_on_failure' => true,
                        'name' => 'EmailAddress',
                    ],
                ],
            ],
            'user_passwd' => [
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min' => 8,
                            'max' => 50,
                        ],
                    ],
                ],
            ],
            'user_passwd_confirm' => [
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => Identical::class,
                        'options' => [
                            'token' => 'user_passwd',
                        ]
                    ],
                ],
            ],
        ];
    }

}
