<?php

namespace Bitkorn\Shop\Form\Fieldset;

use Laminas\Form\Fieldset;

class PasswordFieldset extends Fieldset
{

    public function __construct($name = 'passwd_fieldset')
    {
        parent::__construct($name);
        $this->add([
            'name' => 'passwd',
            'attributes' => [
                'type' => 'password',
                'class' => 'w3-input'
            ],
            'options' => [
                'label' => 'Passwort',
            ],
        ]);
        $this->add([
            'name' => 'passwd2',
            'attributes' => [
                'type' => 'password',
                'title' => 'Passwort bestätigen',
                'class' => 'w3-input'
            ],
            'options' => [
                'label' => 'wiederholen',
            ],
        ]);
    }

}
