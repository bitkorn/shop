<?php

namespace Bitkorn\Shop\Form;

/**
 * Description of Constants
 *
 * @author allapow
 */
class Constants
{
    public static $salutationValueOptions = [
        '' => '* Anrede',
        'Frau' => 'Frau',
        'Herr' => 'Herr'
    ];
}
