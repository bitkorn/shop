<?php

namespace Bitkorn\Shop\Form\Vendor;

use Laminas\Form\Element\Email;
use Laminas\Form\Element\Number;
use Laminas\Form\Element\Radio;
use Laminas\Form\Element\Select;
use Laminas\Form\Element\Submit;
use Laminas\Form\Element\Text;
use Laminas\Form\Element\Url;
use Laminas\Form\Form;
use Laminas\InputFilter\InputFilterProviderInterface;

/**
 *
 * @author allapow
 */
class ShopVendorForm extends Form implements InputFilterProviderInterface
{

    /**
     *
     * @var array
     */
    private $shopVendorGroupsIdAssoc;

    /**
     *
     * @var array
     */
    private $isoCountryIdAssoc;

    public function init()
    {

//        $shopVendorNo = new \Laminas\Form\Element\Text('shop_vendor_number_varchar');
//        $shopVendorNo->setLabel('Lieferanten Nr.');
//        $shopVendorNo->setAttributes([
//            'class' => 'w3-input readonly',
//            'readonly' => 'readonly'
//        ]);
//        $this->add($shopVendorNo);

        $shopVendorGroup = new Select('shop_vendor_group_id');
        $shopVendorGroup->setLabel('Gruppe');
        $shopVendorGroup->setAttributes([
            'class' => 'w3-select',
        ]);
        $shopVendorGroup->setValueOptions($this->shopVendorGroupsIdAssoc);
        $this->add($shopVendorGroup);

        $shopVendorName = new Text('shop_vendor_name');
        $shopVendorName->setLabel('Name');
        $shopVendorName->setAttributes([
            'class' => 'w3-input'
        ]);
        $this->add($shopVendorName);

        $shopVendorStr = new Text('shop_vendor_street');
        $shopVendorStr->setLabel('Straße');
        $shopVendorStr->setAttributes([
            'class' => 'w3-input'
        ]);
        $this->add($shopVendorStr);

        $shopVendorStrNo = new Text('shop_vendor_street_no');
        $shopVendorStrNo->setLabel('Straße Nr.');
        $shopVendorStrNo->setAttributes([
            'class' => 'w3-input'
        ]);
        $this->add($shopVendorStrNo);

        $shopVendorZip = new Text('shop_vendor_zip');
        $shopVendorZip->setLabel('PLZ');
        $shopVendorZip->setAttributes([
            'class' => 'w3-input'
        ]);
        $this->add($shopVendorZip);

        $shopVendorPobox = new Text('shop_vendor_pobox');
        $shopVendorPobox->setLabel('Postfach');
        $shopVendorPobox->setAttributes([
            'class' => 'w3-input'
        ]);
        $this->add($shopVendorPobox);

        $shopVendorCity = new Text('shop_vendor_city');
        $shopVendorCity->setLabel('Stadt');
        $shopVendorCity->setAttributes([
            'class' => 'w3-input'
        ]);
        $this->add($shopVendorCity);

        $isoCountryId = new Select('country_id');
        $isoCountryId->setLabel('Land');
        $isoCountryId->setAttributes([
            'class' => 'w3-input',
        ]);
        $isoCountryId->setValueOptions($this->isoCountryIdAssoc);
        $isoCountryId->setValue(48);
        $isoCountryId->setEmptyOption('-- Land auswählen --');
        $this->add($isoCountryId);

        $shopVendorTel = new Text('shop_vendor_tel');
        $shopVendorTel->setLabel('Telefon');
        $shopVendorTel->setAttributes([
            'class' => 'w3-input'
        ]);
        $this->add($shopVendorTel);

        $shopVendorFax = new Text('shop_vendor_fax');
        $shopVendorFax->setLabel('Fax');
        $shopVendorFax->setAttributes([
            'class' => 'w3-input'
        ]);
        $this->add($shopVendorFax);

        $shopVendorEmail = new Email('shop_vendor_email');
        $shopVendorEmail->setLabel('Email');
        $shopVendorEmail->setAttributes([
            'class' => 'w3-input'
        ]);
        $this->add($shopVendorEmail);

        $shopVendorWeb = new Url('shop_vendor_web');
        $shopVendorWeb->setLabel('Web');
        $shopVendorWeb->setAttributes([
            'class' => 'w3-input',
        ]);
        $this->add($shopVendorWeb);

        $shopVendorMinOrderValue = new Number('shop_vendor_min_order_value');
        $shopVendorMinOrderValue->setLabel('Mindestbestellwert');
        $shopVendorMinOrderValue->setAttributes([
            'class' => 'w3-input'
        ]);
        $this->add($shopVendorMinOrderValue);

        $shopVendorActive = new Radio('shop_vendor_active');
        $shopVendorActive->setLabel('aktiv');
        $shopVendorActive->setLabelAttributes(['class' => '']);
        $shopVendorActive->setAttributes([
            'class' => '',
        ]);
//        $shopVendorActive->setUncheckedValue(0);
//        $shopVendorActive->setCheckedValue(1);
        $shopVendorActive->setValueOptions([0 => 'nicht aktiv', 1 => 'aktiv']);
        $this->add($shopVendorActive);

        $submit = new Submit('submit');
        $submit->setValue('speichern');
        $submit->setAttributes([
            'class' => 'w3-button w3-grey',
        ]);
        $this->add($submit);
    }

    function __construct($name = 'shop_vendor')
    {
        parent::__construct($name);
    }

    public function getInputFilterSpecification()
    {
        return [
            'shop_vendor_group_id' => [
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    ['name' => 'NotEmpty'],
                    [
                        'name' => 'InArray',
                        'options' => [
                            'haystack' => array_keys($this->shopVendorGroupsIdAssoc),
                        ],
                    ],
                ],
            ],
            'shop_vendor_name' => [
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 80,
                        ],
                    ],
                ],
            ],
            'shop_vendor_street' => [
                'required' => false,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 80,
                        ],
                    ],
                ],
            ],
            'shop_vendor_street_no' => [
                'required' => false,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 8,
                        ],
                    ],
                ],
            ],
            'shop_vendor_zip' => [
                'required' => false,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 8,
                        ],
                    ],
                ],
            ],
            'shop_vendor_pobox' => [
                'required' => false,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 8,
                        ],
                    ],
                ],
            ],
            'shop_vendor_city' => [
                'required' => false,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 80,
                        ],
                    ],
                ],
            ],
            'country_id' => [
                'required' => false,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'InArray',
                        'options' => [
                            'haystack' => array_keys($this->isoCountryIdAssoc),
                        ],
                    ],
                ],
            ],
            'shop_vendor_tel' => [
                'required' => false,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 40,
                        ],
                    ],
                ],
            ],
            'shop_vendor_fax' => [
                'required' => false,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 40,
                        ],
                    ],
                ],
            ],
            'shop_vendor_email' => [
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'EmailAddress',
                    ],
                ],
            ],
            'shop_vendor_web' => [
                'required' => false,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'Uri',
                    ],
                ],
            ],
            'shop_vendor_min_order_value' => [
                'required' => false,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'Digits',
                    ],
                ],
            ],
            'shop_vendor_active' => [
                'required' => true,
                'filters' => [
                    ['name' => 'Digits'],
                ],
                'validators' => [
                    [
                        'name' => 'InArray',
                        'options' => [
                            'haystack' => [0, 1],
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     *
     * @param array $shopVendorGroupsIdAssoc
     */
    public function setShopVendorGroupsIdAssoc(array $shopVendorGroupsIdAssoc)
    {
        $this->shopVendorGroupsIdAssoc = $shopVendorGroupsIdAssoc;
    }

    /**
     *
     * @param array $isoCountryIdAssoc
     */
    public function setIsoCountryIdAssoc(array $isoCountryIdAssoc)
    {
        $this->isoCountryIdAssoc = $isoCountryIdAssoc;
    }

}
