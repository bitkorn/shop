<?php

namespace Bitkorn\Shop\Entity\User;

use Bitkorn\Shop\Table\User\ShopUserAddressTable;

/**
 * @author allapow
 */
class ShopUserAddressEntity extends \Bitkorn\Shop\Entity\AbstractEntity
{

    /**
     *
     * @var array Array with Key=property; value=db column
     */
    protected $mapping = [
        'shop_user_address_id' => 'shop_user_address_id',
        'user_uuid' => 'user_uuid',
        'shop_user_address_customer_type' => 'shop_user_address_customer_type',
        'shop_user_address_type' => 'shop_user_address_type',
        'shop_user_address_salut' => 'shop_user_address_salut',
        'shop_user_address_degree' => 'shop_user_address_degree',
        'shop_user_address_name1' => 'shop_user_address_name1',
        'shop_user_address_name2' => 'shop_user_address_name2',
        'shop_user_address_street' => 'shop_user_address_street',
        'shop_user_address_street_no' => 'shop_user_address_street_no',
        'shop_user_address_zip' => 'shop_user_address_zip',
        'shop_user_address_city' => 'shop_user_address_city',
        'shop_user_address_additional' => 'shop_user_address_additional',
        'country_id' => 'country_id',
        'shop_user_address_email' => 'shop_user_address_email',
        'shop_user_address_tel' => 'shop_user_address_tel',
        'shop_user_address_birthday' => 'shop_user_address_birthday',
        'shop_user_address_tax_id' => 'shop_user_address_tax_id',
        'shop_user_address_company_name' => 'shop_user_address_company_name',
        'shop_user_address_company_department' => 'shop_user_address_company_department',
    ];

    public function saveOrUpdate(ShopUserAddressTable $shopUserAddressTable, $userAddressesCount)
    {
        if (empty($this->storage['shop_user_address_birthday'])) {
            $this->storage['shop_user_address_birthday'] = 0; // MySQL 5.7 Incorrect integer value: '' for column
        }
        if (empty($this->storage['shop_user_address_id']) && $userAddressesCount < 2) {
            unset($this->storage['shop_user_address_id']); // MySQL 5.7 Incorrect integer value: '' for column
            switch ($userAddressesCount) {
                case 0:
                    $this->storage['shop_user_address_type'] = 'invoice';
                    break;
                case 1:
                    $this->storage['shop_user_address_type'] = 'shipment';
            }
            return $this->save($shopUserAddressTable);
        } elseif (!empty($this->storage['shop_user_address_id'])) {
            unset($this->storage['shop_user_address_type']); // no update
            return $this->update($shopUserAddressTable);
        }
        return -1;
    }

    public function save(ShopUserAddressTable $shopUserAddressTable)
    {

        return $shopUserAddressTable->saveNewShopUserAddress($this->storage);
    }

    public function update(ShopUserAddressTable $shopUserAddressTable)
    {
        return $shopUserAddressTable->updateShopUserAddress($this->storage);
    }

    public function delete(ShopUserAddressTable $shopUserAddressTable)
    {
        return $shopUserAddressTable->deleteShopUserAddress($this->storage);
    }

    public function isUserOwner($userId)
    {
        if (empty($this->storage['user_uuid']) || empty($userId)) {
            return false;
        }
        return $this->storage['user_uuid'] == $userId;
    }

}
