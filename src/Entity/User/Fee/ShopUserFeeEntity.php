<?php

namespace Bitkorn\Shop\Entity\User\Fee;

use Bitkorn\Shop\Table\User\ShopUserFeeTable;
use Bitkorn\Trinket\Service\I18n\NumberFormatService;

/**
 * @author allapow
 */
class ShopUserFeeEntity extends \Bitkorn\Shop\Entity\AbstractEntity
{

    /**
     *
     * @var array Array with Key=property; value=db column
     */
    protected $mapping = [
        'shop_user_fee_id' => 'shop_user_fee_id',
        'user_uuid' => 'user_uuid',
        'shop_user_fee_percent' => 'shop_user_fee_percent',
        'shop_user_fee_coupon_percent' => 'shop_user_fee_coupon_percent',
        'shop_user_fee_static' => 'shop_user_fee_static',
        'shop_user_fee_mlm_parent_user_uuid' => 'shop_user_fee_mlm_parent_user_uuid',
        'shop_user_fee_mlm_child_fee_percent' => 'shop_user_fee_mlm_child_fee_percent',
        'shop_user_fee_mlm_child_fee_percent_first' => 'shop_user_fee_mlm_child_fee_percent_first',
        'shop_user_fee_mlm_child_fee_coupon_percent' => 'shop_user_fee_mlm_child_fee_coupon_percent',
    ];

    /**
     *
     * @var array 
     */
    protected $formatServiceFields = [
        'shop_user_fee_percent',
        'shop_user_fee_coupon_percent',
        'shop_user_fee_static',
        'shop_user_fee_mlm_child_fee_percent',
        'shop_user_fee_mlm_child_fee_percent_first',
        'shop_user_fee_mlm_child_fee_coupon_percent'
    ];

    /**
     * First set $storage empty-values to have no unset key.
     * Exchange $data to an Array {and set class members (EOL)}.
     * @param array $data
     * @param NumberFormatService $numberFormatService
     * @return bool
     */
    public function exchangeArrayNumberFormat($data, NumberFormatService $numberFormatService)
    {
        $storageKeys = array_values($this->mapping);
        foreach ($storageKeys as $storageKey) {
            $this->storage[$storageKey] = '';
        }
        if (!is_array($data)) {
            return false;
        }
        foreach ($data as $key => $value) {
            if (isset($this->mapping[$key])) {
                if (in_array($key, $this->formatServiceFields) && !$this->isFlippedMapping) {
                    $this->storage[$this->mapping[$key]] = $this->$key = ($numberFormatService->parse($value) ? $numberFormatService->parse($value) : 0);
                } else {
                    $this->storage[$this->mapping[$key]] = $this->$key = $value;
                }
            }
        }
        if (empty($this->storage)) {
            return false;
        }
        return true;
    }

    public function saveOrUpdate(ShopUserFeeTable $shopUserFeeTable)
    {
        if (empty($this->storage['shop_user_fee_id'])) {
            unset($this->storage['shop_user_fee_id']);
            return $this->save($shopUserFeeTable);
        } elseif (!empty($this->storage['shop_user_fee_id'])) {
            return $this->update($shopUserFeeTable);
        }
        return -1;
    }

    public function save(ShopUserFeeTable $shopUserFeeTable)
    {
        return $shopUserFeeTable->saveNewShopUserFeeEntity($this->storage);
    }

    public function update(ShopUserFeeTable $shopUserFeeTable)
    {
        return $shopUserFeeTable->updateShopUserFeeEntity($this->storage);
    }

    public function getUserFeeId()
    {
        if(isset($this->storage['shop_user_fee_id'])) {
            return $this->storage['shop_user_fee_id'];
        }
        return 0;
    }

    public function getUserId()
    {
        if(isset($this->storage['user_uuid'])) {
            return $this->storage['user_uuid'];
        }
        return 0;
    }

    public function getUserFeePercent()
    {
        if(isset($this->storage['shop_user_fee_percent'])) {
            return $this->storage['shop_user_fee_percent'];
        }
        return 0;
    }

    public function getUserFeeCouponPercent()
    {
        if(isset($this->storage['shop_user_fee_coupon_percent'])) {
            return $this->storage['shop_user_fee_coupon_percent'];
        }
        return 0;
    }

    public function getUserFeeStatic()
    {
        if(isset($this->storage['shop_user_fee_static'])) {
            return $this->storage['shop_user_fee_static'];
        }
        return 0;
    }

    public function getUserFeeMlmParentUserId()
    {
        if(isset($this->storage['shop_user_fee_mlm_parent_user_uuid'])) {
            return $this->storage['shop_user_fee_mlm_parent_user_uuid'];
        }
        return 0;
    }

    public function getUserFeeMlmChildFeePercent()
    {
        if(isset($this->storage['shop_user_fee_mlm_child_fee_percent'])) {
            return $this->storage['shop_user_fee_mlm_child_fee_percent'];
        }
        return 0;
    }

    public function getUserFeeMlmChildFeePercentFirst()
    {
        if(isset($this->storage['shop_user_fee_mlm_child_fee_percent_first'])) {
            return $this->storage['shop_user_fee_mlm_child_fee_percent_first'];
        }
        return 0;
    }

    public function getUserFeeMlmChildFeeCouponPercent()
    {
        if(isset($this->storage['shop_user_fee_mlm_child_fee_coupon_percent'])) {
            return $this->storage['shop_user_fee_mlm_child_fee_coupon_percent'];
        }
        return 0;
    }
    
}
