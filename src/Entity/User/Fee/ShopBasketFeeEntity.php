<?php

namespace Bitkorn\Shop\Entity\User\Fee;

/**
 * Description of ShopBasketFeeEntity
 *
 * @author allapow
 */
class ShopBasketFeeEntity extends \Bitkorn\Shop\Entity\AbstractEntity implements \Iterator, \ArrayAccess
{

    protected $mapping = [
        'shop_basket_entity_id' => 'shop_basket_entity_id',
        'shop_basket_unique' => 'shop_basket_unique',
        'user_uuid' => 'user_uuid',
        'user_uuid_pos' => 'user_uuid_pos',
        'shop_basket_time_create' => 'shop_basket_time_create',
        'shop_basket_time_order' => 'shop_basket_time_order',
        'shop_basket_time_payed' => 'shop_basket_time_payed',
        'shop_article_amount' => 'shop_article_amount',
        'shop_article_price_total' => 'shop_article_price_total',
        'shop_article_tax' => 'shop_article_tax',
        'shop_basket_discount_hash' => 'shop_basket_discount_hash',
//        'computed_value_article_price_total_sum_end' => 'computed_value_article_price_total_sum_end',
//        'computed_value_article_tax_total_sum_end' => 'computed_value_article_tax_total_sum_end',
//        'computed_value_article_price_total_sum_end_net' => 'computed_value_article_price_total_sum_end_net', // UNUSED
        /**
         * UNUSED
         */
//        'shop_basket_fee_percent' => 'shop_basket_fee_percent',
//        'shop_basket_fee_static' => 'shop_basket_fee_static',
//        'shop_basket_discount_fee_percent_result' => 'shop_basket_discount_fee_percent_result',
        /**
         *
         */
        'article_fee_static' => 'article_fee_static',
        'article_price_total_net' => 'article_price_total_net',
        'article_fee_percent_result' => 'article_fee_percent_result',
        'article_fee_coupon_result' => 'article_fee_coupon_result',
        'article_fee_percent_first_result' => 'article_fee_percent_first_result',
        /**
         * fee paid
         */
        'shop_basket_fee_paid_sum' => 'shop_basket_fee_paid_sum',
        'shop_user_fee_paid_time' => 'shop_user_fee_paid_time',
        'shop_user_fee_paid_payment_id' => 'shop_user_fee_paid_payment_id',
        'shop_user_fee_paid_payment_method' => 'shop_user_fee_paid_payment_method',
        /**
         * user fee preferences
         */
        'shop_user_fee_percent' => 'shop_user_fee_percent',
        'shop_user_fee_coupon_percent' => 'shop_user_fee_coupon_percent',
        'shop_user_fee_static' => 'shop_user_fee_static',
        'shop_user_fee_mlm_parent_user_uuid' => 'shop_user_fee_mlm_parent_user_uuid',
        'shop_user_fee_mlm_child_fee_percent' => 'shop_user_fee_mlm_child_fee_percent',
        'shop_user_fee_mlm_child_fee_percent_first' => 'shop_user_fee_mlm_child_fee_percent_first',
        'shop_user_fee_mlm_child_fee_coupon_percent' => 'shop_user_fee_mlm_child_fee_coupon_percent',
        /**
         * article
         */
        'shop_article_feeable' => 'shop_article_feeable',
        /**
         * computet BasketFeeValues
         */
        'computet_basketfee_shopping_value_net' => 'computet_basketfee_shopping_value_net',
        'computet_basketfee_fee' => 'computet_basketfee_fee',
        'computet_basketfee_type' => 'computet_basketfee_type',
        'mlm_level' => 'mlm_level',
        /**
         * Invoice
         */
        'shop_document_invoice_number' => 'shop_document_invoice_number',
        /**
         * customer name
         */
        'shop_basket_address_name' => 'shop_basket_address_name',
        'shop_user_address_name' => 'shop_user_address_name',
    ];

    /**
     *
     * @var array 
     */
    public $storageItems = [];
    private $feeSum = 0, $staticFeeSum = 0, $percentFeeSum = 0, $discountFeeSum = 0, $feePaidSum = 0;

    /**
     * Use this function and NOT exchangeArray($data) from \Bitkorn\Shop\Entity\AbstractEntity()!
     * @param array $basketEntityData
     * @return boolean
     */
    public function exchangeArrayBasketFee(array $basketEntityData, $resetStorage = false)
    {
        if ($resetStorage) {
            $this->storageItems = [];
        }
        foreach ($basketEntityData as $basketEntityRow) {
            $tmp = [];
            $storageKeys = array_values($this->mapping);
            foreach ($storageKeys as $storageKey) {
                $tmp[$storageKey] = '';
            }
            if (!is_array($basketEntityRow)) {
                continue;
            }
            foreach ($basketEntityRow as $key => $value) {
                if (isset($this->mapping[$key])) {
                    $tmp[$this->mapping[$key]] = $value;
                }
            }
            $this->storageItems[] = $tmp;
        }
        if (empty($this->storageItems)) {
            return false;
        }
        return true;
    }

    public function computeFee()
    {
        if (empty($this->storageItems)) {
            return false;
        }
        foreach ($this->storageItems as &$storageItem) {
            $basketFeeTypePostString = $storageItem['mlm_level'] == 'sales' ? '' : '-MLM';
            $storageItem['computet_basketfee_shopping_value_net'] = $storageItem['article_price_total_net'];
            if (!empty($storageItem['article_fee_static'])) {
                $storageItem['computet_basketfee_fee'] = $storageItem['article_fee_static'];
                $storageItem['computet_basketfee_type'] = 'Festwert' . $basketFeeTypePostString;
                $this->staticFeeSum += $storageItem['computet_basketfee_fee'];
            } elseif (!empty($storageItem['user_uuid_pos'])) {
                if (empty($storageItem['article_fee_percent_first_result']) || floatval($storageItem['article_fee_percent_first_result']) == 0) {
                    $storageItem['computet_basketfee_fee'] = $storageItem['article_fee_percent_result'];
                    $storageItem['computet_basketfee_type'] = 'Verkauf' . $basketFeeTypePostString;
                } else {
                    $storageItem['computet_basketfee_fee'] = $storageItem['article_fee_percent_first_result'];
                    $storageItem['computet_basketfee_type'] = ' erster Verkauf' . $basketFeeTypePostString;
                }
                $this->percentFeeSum += $storageItem['computet_basketfee_fee'];
            } elseif (!empty($storageItem['article_fee_coupon_result'])) {
                $storageItem['computet_basketfee_fee'] = $storageItem['article_fee_coupon_result'];
                $storageItem['computet_basketfee_type'] = 'Rabatt-Code' . $basketFeeTypePostString;
                $this->discountFeeSum += $storageItem['computet_basketfee_fee'];
            }
            $this->feeSum += $storageItem['computet_basketfee_fee'] || 0;
            $this->feePaidSum += $storageItem['shop_basket_fee_paid_sum'] || 0;
        }
    }

    public function isBasketGroups()
    {
        return $this->basketGroups;
    }

    public function getFeeSum()
    {
        return $this->feeSum;
    }

    public function getStaticFeeSum()
    {
        return $this->staticFeeSum;
    }

    public function getPercentFeeSum()
    {
        return $this->percentFeeSum;
    }

    public function getDiscountFeeSum()
    {
        return $this->discountFeeSum;
    }

    public function getFeePaidSum()
    {
        return $this->feePaidSum;
    }

    /**
     *
     * @var int
     */
    private $iterateIndex = 0;

    public function current()
    {
        if (!isset($this->storageItems[$this->iterateIndex])) {
            return [];
        }
        return $this->storageItems[$this->iterateIndex];
    }

    public function key()
    {
        return $this->iterateIndex;
    }

    public function next()
    {
        ++$this->iterateIndex;
    }

    public function rewind()
    {
        $this->iterateIndex = 0;
    }

    public function valid()
    {
        return isset($this->storageItems[$this->iterateIndex]);
    }

    private $currentColor = '';
    private $currentColor1 = '#eee';
    private $currentColor2 = '#ccc';

    public function getCurrentColor()
    {
        if ($this->iterateIndex == 0) {
            $this->currentColor = $this->currentColor1;
        }
        if (isset($this->storageItems[$this->iterateIndex - 1]) && $this->storageItems[$this->iterateIndex - 1]['shop_basket_unique'] != $this->storageItems[$this->iterateIndex]['shop_basket_unique']) {
            $this->switchCurrentColor();
        }
        return $this->currentColor;
    }

    private function switchCurrentColor()
    {
        if ($this->currentColor == $this->currentColor2) {
            $this->currentColor = $this->currentColor1;
        } elseif ($this->currentColor == $this->currentColor1) {
            $this->currentColor = $this->currentColor2;
        }
    }

    public function offsetExists($offset)
    {
        return isset($this->storageItems[$offset]);
    }

    public function offsetGet($offset)
    {
        return $this->storageItems[$offset];
    }

    public function offsetSet($offset, $value)
    {
        $this->storageItems[$offset] = $value;
    }

    public function offsetUnset($offset)
    {
        unset($this->storageItems[$offset]);
    }

}
