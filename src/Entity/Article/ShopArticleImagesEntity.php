<?php

namespace Bitkorn\Shop\Entity\Article;

/**
 *
 * @author allapow
 */
class ShopArticleImagesEntity extends \Bitkorn\Images\Entity\Image\ImagesEntity
{

    public function __construct()
    {
        $this->mapping = array_merge($this->mapping,
                [
            // shop_article_image
            'shop_article_image_id' => 'shop_article_image_id',
            'shop_article_id' => 'shop_article_id',
            // bk_images_imagegroup
            'bk_images_imagegroup_name' => 'bk_images_imagegroup_name',
            'bk_images_imagegroup_priority' => 'bk_images_imagegroup_priority',
                ]
        );
    }

    /**
     * 
     * @param \Bitkorn\Shop\Table\Article\ShopArticleImageTable $shopArticeImageTable
     * @return int
     */
    public function save(\Bitkorn\Shop\Table\Article\ShopArticleImageTable $shopArticeImageTable)
    {
        unset($this->storage['shop_article_image_id']); // to ensure safety
        return $shopArticeImageTable->saveShopArticleImage($this->storage);
    }

    public function delete(\Bitkorn\Shop\Table\Article\ShopArticleImageTable $shopArticeImageTable)
    {
        return $shopArticeImageTable->deleteShopArticleImage($this->storage);
    }

}
