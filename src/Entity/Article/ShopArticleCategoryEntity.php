<?php

namespace Bitkorn\Shop\Entity\Article;

use Bitkorn\Shop\Entity\AbstractEntity;
use Bitkorn\Shop\Table\Article\ShopArticleCategoryTable;

/**
 *
 * @author allapow
 */
class ShopArticleCategoryEntity extends AbstractEntity
{

    /**
     *
     * @var array Array with Key=property; value=db column
     */
    protected $mapping = [
        'shop_article_category_id' => 'shop_article_category_id',
        'shop_article_category_alias' => 'shop_article_category_alias',
        'shop_article_category_name' => 'shop_article_category_name',
        'shop_article_category_desc' => 'shop_article_category_desc',
//        'shop_article_category_img' => 'shop_article_category_img',
        'shop_article_category_hexcolor' => 'shop_article_category_hexcolor',
        'shop_article_category_id_parent' => 'shop_article_category_id_parent',
        'shop_article_category_meta_title' => 'shop_article_category_meta_title',
        'shop_article_category_meta_desc' => 'shop_article_category_meta_desc',
        'shop_article_category_meta_keywords' => 'shop_article_category_meta_keywords',
    ];

    /**
     * ChildCategories of that in $this->storage
     * @var array
     */
    protected $shopArticleCategoriesWithChilds = [];

    /**
     *
     * @var array
     */
    protected $hierarchicalIdAssoc = [];

    public function save(ShopArticleCategoryTable $shopArticeCategoryTable)
    {
        unset($this->storage['shop_article_category_id']); // to ensure safety
        return $shopArticeCategoryTable->saveShopArticleCategory($this->storage);
    }

    public function update(ShopArticleCategoryTable $shopArticeCategoryTable)
    {
        return $shopArticeCategoryTable->updateShopArticleCategory($this->storage);
    }

    public function fetchShopArticleCategoryChilds(ShopArticleCategoryTable $shopArticeCategoryTable)
    {
        $shopArticleCategoryId = 0;
        if (!empty($this->storage)) {
            $shopArticleCategoryId = $this->storage['shop_article_category_id'];
        }
        $this->shopArticleCategoriesWithChilds = $shopArticeCategoryTable->getShopArticleCategoryIdAssoc();
        foreach ($this->shopArticleCategoriesWithChilds as &$shopArticleCategoriesWithChild) {
            $shopArticeCategoryTable->getShopArticleCategoryIdAssocRecursive($shopArticleCategoriesWithChild, $shopArticleCategoriesWithChild['shop_article_category_id']);
        }

    }

    public function computeHierarchicalIdAssoc(ShopArticleCategoryTable $shopArticeCategoryTable)
    {
        if (empty($this->shopArticleCategoriesWithChilds)) {
            $this->fetchShopArticleCategoryChilds($shopArticeCategoryTable);
        }
        foreach ($this->shopArticleCategoriesWithChilds as $shopArticleCategoryChild) {
            $this->hierarchicalIdAssoc[$shopArticleCategoryChild['shop_article_category_id']] = $shopArticleCategoryChild['shop_article_category_name'];
            if (isset($shopArticleCategoryChild['childs']) && !empty($shopArticleCategoryChild['childs'])) {
                $this->computeHierarchicalIdAssocRecursive($shopArticleCategoryChild['childs']);
            }
        }
    }

    private function computeHierarchicalIdAssocRecursive(array $shopArticleCategoryChilds, $prefix = '-- ')
    {
        foreach ($shopArticleCategoryChilds as $shopArticleCategoryChild) {
            $this->hierarchicalIdAssoc[$shopArticleCategoryChild['shop_article_category_id']] = $prefix . $shopArticleCategoryChild['shop_article_category_name'];
            if (isset($shopArticleCategoryChild['childs']) && !empty($shopArticleCategoryChild['childs'])) {
                $this->computeHierarchicalIdAssocRecursive($shopArticleCategoryChild['childs'], $prefix . '-- ');
            }
        }
    }

    public function getHierarchicalIdAssoc()
    {
        return $this->hierarchicalIdAssoc;
    }

}
