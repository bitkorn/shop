<?php

namespace Bitkorn\Shop\Entity\Article\TypeDelivery;

use Bitkorn\Trinket\Entity\AbstractEntity;

class ShopArticleLengthcutDefEntity extends AbstractEntity
{

    protected $primaryKey = 'shop_article_lengthcut_def_id';

    public array $mapping = [
        'shop_article_lengthcut_def_id' => 'shop_article_lengthcut_def_id',
        'shop_article_lengthcut_def_name' => 'shop_article_lengthcut_def_name',
        'shop_article_lengthcut_def_desc' => 'shop_article_lengthcut_def_desc',
        'quantityunit_uuid' => 'quantityunit_uuid',
        'shop_article_lengthcut_def_basequantity' => 'shop_article_lengthcut_def_basequantity',
        'shop_article_lengthcut_def_step' => 'shop_article_lengthcut_def_step',
        'shop_article_lengthcut_def_cutprice' => 'shop_article_lengthcut_def_cutprice',
    ];

    /**
     * @var int
     */
    protected $lengthPieces = 0;

    /**
     * @var int
     */
    protected $lengthSum = 0;

    /**
     * @var int Quantity of pieces
     */
    protected $quantity = 0;

    /**
     * @var int
     */
    protected $quantityCuts = 0;

    /**
     * @var int
     */
    protected $piecesPerBase = 0;

    /**
     * @var float Number of base pieces required
     */
    protected $quantityBase = 0;

    /**
     * @param int $lengthPieces Cutting length ... the length of each of the pieces
     */
    public function setLengthPieces(int $lengthPieces): void
    {
        $this->lengthPieces = $lengthPieces;
    }

    /**
     * @return int
     */
    public function getLengthPieces(): int
    {
        return $this->lengthPieces;
    }

    /**
     * @param int $quantity Quantity of pieces
     */
    public function setQuantity(int $quantity): void
    {
        $this->quantity = $quantity;
    }

    /**
     * @return int Quantity of pieces
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @return int
     */
    public function getLengthSum(): int
    {
        return $this->lengthSum;
    }

    /**
     * @return int
     */
    public function getQuantityCuts(): int
    {
        return $this->quantityCuts;
    }

    public function getLengthcutDefBasequantity():float
    {
        if(!isset($this->storage['shop_article_lengthcut_def_basequantity'])) {
            return 0;
        }
        return floatval($this->storage['shop_article_lengthcut_def_basequantity']);
    }

    /**
     * @return bool
     */
    public function computeQuantityCuts(): bool
    {
        if (
            empty($this->storage['shop_article_lengthcut_def_basequantity'])
            || empty($this->lengthPieces)
            || empty($this->quantity)
            || ($this->lengthPieces % $this->storage['shop_article_lengthcut_def_step']) > 0
        ) {
            return false;
        }
        $this->piecesPerBase = $this->storage['shop_article_lengthcut_def_basequantity'] / $this->lengthPieces;
        if ($this->quantity <= $this->piecesPerBase) {
            // only one base piece required
            $this->quantityBase = 1;
            if($this->quantity > 1) {
                $this->quantityCuts = $this->quantity - 1;
            } else {
                $this->quantityCuts = 1;
            }
        } else {
            // multiple base pieces required
            $this->quantityBase = floor($this->quantity / $this->piecesPerBase);
            $this->quantityCuts = $this->quantityBase * $this->piecesPerBase - $this->quantityBase;
            if (($this->quantity % $this->piecesPerBase) > 0) {
                // odd number of base pieces
                $this->quantityCuts += $this->quantity % $this->piecesPerBase;
                $this->quantityBase++;
            }
        }
        $this->lengthSum = $this->lengthPieces * $this->quantity;
        return true;
    }

    /**
     * @param float $articlePrice
     * @param string $definitionJson
     * @return float
     */
    public function computePrice(float $articlePrice, string $definitionJson): float
    {
        $def = json_decode($definitionJson, true);
        if (!is_array($def) || empty($def['length_pieces']) || empty($def['quantity']) || !isset($def['quantity_cuts']) || empty($this->piecesPerBase)) {
            return -1;
        }
        return ($articlePrice / $this->piecesPerBase) * $this->quantity;
    }

    /**
     * @return string {"lengthcut_def_id": 42,"cutprice": 4.20,"length_pieces":1500,"quantity":5,"quantity_cuts":3}
     */
    public function getDefinitionJson(): string
    {
        if (
            empty($this->storage['shop_article_lengthcut_def_id'])
            || empty($this->storage['shop_article_lengthcut_def_cutprice'])
            || empty($this->lengthPieces) || empty($this->quantity)
        ) {
            return '';
        }
        return json_encode([
            'lengthcut_def_id' => intval($this->storage['shop_article_lengthcut_def_id']),
            'cutprice' => floatval($this->storage['shop_article_lengthcut_def_cutprice']),
            'length_pieces' => floatval($this->lengthPieces),
            'quantity' => intval($this->quantity), // quantity of pieces
            'quantity_cuts' => intval($this->quantityCuts)
        ]);
    }

    /**
     * @param string $definitionJson {"lengthcut_def_id": 42,"cutprice": 4.20,"length_pieces":1500,"quantity":5,"quantity_cuts":3}
     * @return array ['lengthcut_def_id' => 42,'cutprice' => 4.20,'length_pieces' => 1500,'quantity' => 5,'quantity_cuts' => 3]
     */
    public static function decodeDefinitionJson(string $definitionJson): array
    {
        if (empty($definitionJson)) {
            return [];
        }
        return json_decode($definitionJson, true);
    }
}
