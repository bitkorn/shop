<?php

namespace Bitkorn\Shop\Entity\Article;

use Bitkorn\Shop\Table\Article\ShopArticleTable;
use Bitkorn\Shop\Table\Article\ShopArticleCategoryRelationTable;

/**
 *
 * @author allapow
 */
class ShopArticleEntity extends \Bitkorn\Shop\Entity\AbstractEntity
{

    /**
     *
     * @var array Array with Key=property; value=db column
     */
    protected $mapping = [
        'shop_article_id' => 'shop_article_id',
        'shop_article_sku' => 'shop_article_sku',
//        'shop_article_category_id' => 'shop_article_category_id',
        'shop_article_class_id' => 'shop_article_class_id',
        'shop_article_type' => 'shop_article_type',
        'shop_article_type_delivery' => 'shop_article_type_delivery',
        'shop_article_groupable' => 'shop_article_groupable',
//        'shop_article_size_def_id' => 'shop_article_size_def_id',
        'shop_article_weight' => 'shop_article_weight',
        'shop_article_shipping_costs_single' => 'shop_article_shipping_costs_single',
        'shop_article_price' => 'shop_article_price',
        'shop_article_tax' => 'shop_article_tax',
        'shop_article_price_net' => 'shop_article_price_net',
        'shop_article_name' => 'shop_article_name',
        'shop_article_sefurl' => 'shop_article_sefurl',
        'shop_article_desc' => 'shop_article_desc',
        'shop_article_hexcolor' => 'shop_article_hexcolor',
        'shop_article_meta_title' => 'shop_article_meta_title',
        'shop_article_meta_desc' => 'shop_article_meta_desc',
        'shop_article_meta_keywords' => 'shop_article_meta_keywords',
        'shop_article_active' => 'shop_article_active',
        'shop_article_active_in_categories' => 'shop_article_active_in_categories',
        'shop_article_active_in_search' => 'shop_article_active_in_search',
        'shop_article_stock_using' => 'shop_article_stock_using',
        'shop_article_feeable' => 'shop_article_feeable',
        /*
         * 
         */
        'shop_article_category_relation' => 'shop_article_category_relation'
    ];

    public function save(ShopArticleTable $shopArticeTable, ShopArticleCategoryRelationTable $shopArticeCategoryRelationTable)
    {
        if(!empty($this->storage['shop_article_category_relation']) && is_array($this->storage['shop_article_category_relation'])) {
            foreach($this->storage['shop_article_category_relation'] as $categoryId) {
                $shopArticeCategoryRelationTable->saveShopArticleCategory($this->storage['shop_article_id'], $categoryId);
            }
        }
        unset($this->storage['shop_article_category_relation']);
        unset($this->storage['shop_article_id']); // to ensure safety
        unset($this->storage['shop_article_hexcolor']);
        return $shopArticeTable->saveShopArticle($this->storage);
    }

    public function update(ShopArticleTable $shopArticeTable, ShopArticleCategoryRelationTable $shopArticeCategoryRelationTable)
    {
        if(!empty($this->storage['shop_article_category_relation']) && is_array($this->storage['shop_article_category_relation'])) {
            $shopArticeCategoryRelationTable->deleteForArticle($this->storage['shop_article_id']);
            foreach($this->storage['shop_article_category_relation'] as $categoryId) {
                $shopArticeCategoryRelationTable->saveShopArticleCategory($this->storage['shop_article_id'], $categoryId);
            }
        }
        unset($this->storage['shop_article_category_relation']);
        return $shopArticeTable->updateShopArticle($this->storage['shop_article_id'], $this->storage);
    }

    /**
     * 
     * @return boolean FALSE if it is a Group-Root-Article (group | sizegroup) AND (groupable == yes || stock using == yes)
     */
    public function checkGroupable()
    {
        if (($this->storage['shop_article_type'] == 'group' || $this->storage['shop_article_type'] == 'sizegroup')
                && ($this->storage['shop_article_groupable'] > 0 || $this->storage['shop_article_stock_using'] > 0)) {
            return false;
        }
        return true;
    }
    
    public function calculatePriceNet()
    {
        if(empty($this->storage['shop_article_price']) || empty($this->storage['shop_article_tax'])) {
            throw new \RuntimeException('Can not calculate net price if gross or tax is empty.');
        }
        $this->storage['shop_article_price_net'] = round(($this->storage['shop_article_price'] / (100 + $this->storage['shop_article_tax'])) * 100, 2);
    }
    
    public function calculatePriceGross()
    {
        if(empty($this->storage['shop_article_price_net']) || empty($this->storage['shop_article_tax'])) {
            throw new \RuntimeException('Can not calculate gross price if net or tax is empty.');
        }
        $this->storage['shop_article_price'] = round($this->storage['shop_article_price_net'] + ($this->storage['shop_article_price_net'] * $this->storage['shop_article_tax'] / 100), 2);
    }

}
