<?php

namespace Bitkorn\Shop\Entity\Article;

use Bitkorn\Shop\Entity\AbstractEntity;
use Bitkorn\Shop\Table\Article\Option\ShopArticleOptionDefTable;
use Bitkorn\Shop\Table\Article\Option\ShopArticleOptionItemArticlePricediffTable;
use Laminas\Log\Logger;

/**
 * Selected options for one shop-article.
 * @author allapow
 */
class ShopArticleOptionsEntity extends AbstractEntity
{

    /**
     *
     * @var array Array with Key=property; value=db column
     */
    protected $mapping = [
        'optiondefid' => 'optiondefid',
        'optionitemid' => 'optionitemid',
    ];

    /**
     *
     * @var array
     */
    protected $storageItems = [];

    /**
     *
     * @var Logger
     */
    private $logger;

    /**
     *
     * @var ShopArticleOptionDefTable
     */
    protected $shopArticleOptionDefTable;

    /**
     * Do not use this function. Use exchangeArrayOptions().
     * @param array $data
     */
    public function exchangeArray($data)
    {
        trigger_error(sprintf(
            'You are calling exchangeArray() instead exchangeArrayOptions() in class %s', get_class($this)
        ), E_USER_WARNING);
    }

    public function exchangeArrayOptions(array $data)
    {
        $this->storageItems = [];
        foreach ($data as $row) {
            $tmp = [];
            $storageKeys = array_values($this->mapping);
            foreach ($storageKeys as $storageKey) {
                $tmp[$storageKey] = '';
            }
            if (!is_array($row)) {
                continue;
            }
            foreach ($row as $key => $value) {
                if (isset($this->mapping[$key])) {
                    $tmp[$this->mapping[$key]] = (int)$value;
                }
            }
            $this->storageItems[] = $tmp;
        }
        if (empty($this->storageItems)) {
            return false;
        }
        return asort($this->storageItems);
    }

    /**
     *
     * @param boolean $sortTechnical TRUE to save it in database or search there. FALSE to use priority from db.shop_article_option_def.
     * @return string
     */
    public function getShopArticleOptionsJson($sortTechnical = true)
    {
        return $this->cleanOptionsJson(json_encode($this->storageItems), $sortTechnical);
    }

    /**
     *
     * @param ShopArticleOptionItemArticlePricediffTable $shopArticleOptionItemArticlePricediffTable
     * @param int $shopArticleId
     * @return float
     */
    public function computePricediff(ShopArticleOptionItemArticlePricediffTable $shopArticleOptionItemArticlePricediffTable, $shopArticleId)
    {
        $itemIds = [];
        foreach ($this->storageItems as $storageItem) {
            $itemIds[] = $storageItem['optionitemid'];
        }
        return $shopArticleOptionItemArticlePricediffTable->getShopArticleOptionItemsSummPricediff($itemIds, $shopArticleId);
    }

    /**
     *
     * @param string $optionsJson
     * @param boolean $sortTechnical TRUE to save it in database or search there. FALSE to use priority from db.shop_article_option_def.
     * @return string
     * @throws \RuntimeException
     * @todo Ist $sortTechnical == false notwendig?
     */
    public function cleanOptionsJson(string $optionsJson, $sortTechnical = true)
    {
        $array = json_decode($optionsJson, true);
        if (!is_array($array)) {
            $this->logger->err(__FILE__ . '()->' . __FUNCTION__ . '() $optionsJson are not JSON: ' . $optionsJson . '; $array: ' . print_r($array, true));
            return '[]';
        }
        $sortArray = [];
        foreach ($array as $option) {
            $option['optiondefid'] = intval($option['optiondefid']);
            $option['optionitemid'] = intval($option['optionitemid']);
            if (!$sortTechnical) {
                $index = $this->shopArticleOptionDefTable->getShopArticleOptionDefPriorityById($option['optiondefid']);
            } else {
                $index = $option['optiondefid'];
            }
            if ($index) {
                $sortArray[$index] = $option;
            } else {
                $sortArray[] = $option;
            }
        }
        if (!$sortTechnical) {
            krsort($sortArray); // largest priority first
        } else {
            ksort($sortArray);
        }
        $jsonWithoutArraKeys = '[';
        $count = 0;
        foreach ($sortArray as $storageItem) {
            if ($count > 0) {
                $jsonWithoutArraKeys .= ',';
            }
            $jsonWithoutArraKeys .= json_encode($storageItem);
            $count++;
        }
        return $jsonWithoutArraKeys . ']';
    }

    /**
     *
     * @param Logger $logger
     */
    public function setLogger(Logger $logger)
    {
        $this->logger = $logger;
    }

    /**
     *
     * @param ShopArticleOptionDefTable $shopArticleOptionDefTable
     */
    public function setShopArticleOptionDefTable(ShopArticleOptionDefTable $shopArticleOptionDefTable)
    {
        $this->shopArticleOptionDefTable = $shopArticleOptionDefTable;
    }

}
