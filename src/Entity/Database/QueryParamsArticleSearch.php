<?php


namespace Bitkorn\Shop\Entity\Database;


use Bitkorn\Trinket\Entity\Database\QueryParamsBase;

class QueryParamsArticleSearch extends QueryParamsBase
{


    protected $articleType = '';
    protected $searchtext = '';
    protected $categoryId = 0;
    protected $classId = 0;
    protected $type = '';
    protected $typeDelivery = '';

    /**
     * @return string
     */
    public function getArticleType(): string
    {
        return $this->articleType;
    }

    /**
     * @param string $articleType
     */
    public function setArticleType(string $articleType): void
    {
        $this->articleType = $articleType;
    }

    /**
     * @return string
     */
    public function getSearchtext(): string
    {
        return $this->searchtext;
    }

    /**
     * @param string $searchtext
     */
    public function setSearchtext(string $searchtext): void
    {
        $this->searchtext = $searchtext;
    }

    /**
     * @return int
     */
    public function getCategoryId(): int
    {
        return $this->categoryId;
    }

    /**
     * @param int $categoryId
     */
    public function setCategoryId(int $categoryId): void
    {
        $this->categoryId = $categoryId;
    }

    /**
     * @return int
     */
    public function getClassId(): int
    {
        return $this->classId;
    }

    /**
     * @param int $classId
     */
    public function setClassId(int $classId): void
    {
        $this->classId = $classId;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getTypeDelivery(): string
    {
        return $this->typeDelivery;
    }

    /**
     * @param string $typeDelivery
     */
    public function setTypeDelivery(string $typeDelivery): void
    {
        $this->typeDelivery = $typeDelivery;
    }

}