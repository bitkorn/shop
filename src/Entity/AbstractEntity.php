<?php

namespace Bitkorn\Shop\Entity;

/**
 * All Entities extends AbstractEntity
 * Entities manage the relation from Data-Keys in Frontend to Data-Keys in database.
 *
 * @author allapow
 */
class AbstractEntity
{

    /**
     * @var array Array with Key=property; value=db column
     */
    protected $mapping = [];
    protected $isFlippedMapping = false;
    public $storage;
    public $storageOutside;

    /**
     * Flip if data comes from DB
     */
    public function flipMapping($flipOnlyIfNotFlipped = true)
    {
        if ($flipOnlyIfNotFlipped && $this->isFlippedMapping) {
            return;
        }
        $this->mapping = array_flip($this->mapping);
        if ($this->isFlippedMapping) {
            $this->isFlippedMapping = false;
        } else {
            $this->isFlippedMapping = true;
        }
    }

    public function flipMappingBack()
    {
        if ($this->isFlippedMapping) {
            $this->mapping = array_flip($this->mapping);
            $this->isFlippedMapping = false;
        }
    }

    /**
     * First set $storage empty-values to have no unset key.
     * Exchange $data to an Array {and set class members (EOL)}.
     * @param array $data
     */
    public function exchangeArray($data)
    {
        $storageKeys = array_values($this->mapping);
        foreach ($storageKeys as $storageKey) {
            $this->storage[$storageKey] = '';
        }
        if (!is_array($data)) {
            return false;
        }
        foreach ($data as $key => $value) {
            if (isset($this->mapping[$key])) {
                $this->storage[$this->mapping[$key]] = $this->$key = $value;
            }
        }
        if (empty($this->storage)) {
            return false;
        }
        return true;
    }

    /**
     * Exchange $data to an Array {and set class members (EOL)}.
     * @param array $data
     */
    public function exchangeOutsiteArray($data)
    {
//        $storageKeys = array_values($this->mapping);
//        foreach ($storageKeys as $storageKey) {
//            $this->storageOutside[$storageKey] = '';
//        }
        if (!is_array($data)) {
            return false;
        }
        foreach ($data as $key => $value) {
            if (isset($this->mapping[$key])) {
                $this->storageOutside[$this->mapping[$key]] = $this->$key = $value;
            }
        }
        if (empty($this->storageOutside)) {
            return false;
        }
        return true;
    }

    /**
     * 
     * @return array Storage keys wich have unequal values.
     * @throws \RuntimeException
     */
    public function compareStorageValues()
    {
        if (!isset($this->storage) || !isset($this->storageOutside)) {
            throw new \RuntimeException('One of the storage is empty. ' . __CLASS__ . '->' . __FUNCTION__);
        }
        if (!is_array($this->storage) || !is_array($this->storageOutside)) {
            throw new \RuntimeException('One of the storage is not an Array. ' . __CLASS__ . '->' . __FUNCTION__);
        }
        $storageKeys = array_values($this->mapping);
        $unequals = [];
        foreach ($storageKeys as $storageKey) {
            if (isset($this->storageOutside[$storageKey]) && $this->storage[$storageKey] != $this->storageOutside[$storageKey]) {
                $unequals[] = $storageKey;
            }
        }
        return $unequals;
    }

    public function getMapping()
    {
        return $this->mapping;
    }

    public function isMappingFlipped()
    {
        return $this->isFlippedMapping;
    }

}
