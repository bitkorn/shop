<?php

namespace Bitkorn\Shop\Entity\Document;

/**
 *
 * @author allapow
 */
class ShopDocumentOrderEntity extends \Bitkorn\Shop\Entity\AbstractEntity
{

    /**
     *
     * @var array Array with Key=property; value=db column
     */
    protected $mapping = [
        'shop_document_order_id' => 'shop_document_order_id',
        'shop_document_order_number' => 'shop_document_order_number',
        'shop_document_order_filename' => 'shop_document_order_filename',
        'shop_document_order_path_relative' => 'shop_document_order_path_relative',
        'shop_document_order_path_absolute' => 'shop_document_order_path_absolute',
        'shop_document_order_time' => 'shop_document_order_time',
    ];

}
