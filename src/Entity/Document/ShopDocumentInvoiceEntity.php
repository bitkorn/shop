<?php

namespace Bitkorn\Shop\Entity\Document;

/**
 *
 * @author allapow
 */
class ShopDocumentInvoiceEntity extends \Bitkorn\Shop\Entity\AbstractEntity
{

    /**
     *
     * @var array Array with Key=property; value=db column
     */
    protected $mapping = [
        'shop_document_invoice_id' => 'shop_document_invoice_id',
        'shop_document_invoice_number' => 'shop_document_invoice_number',
        'shop_document_invoice_filename' => 'shop_document_invoice_filename',
        'shop_document_invoice_path_relative' => 'shop_document_invoice_path_relative',
        'shop_document_invoice_path_absolute' => 'shop_document_invoice_path_absolute',
        'shop_document_invoice_time' => 'shop_document_invoice_time',
    ];

}
