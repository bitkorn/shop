<?php

namespace Bitkorn\Shop\Entity\Basket;

use Bitkorn\Shop\Table\Basket\Discount\ShopBasketDiscountTable;
use Bitkorn\Shop\Tablex\Basket\ShopBasketTablex;
use Bitkorn\Shop\Tools\Basket\DiscountHash;
use Bitkorn\Shop\Tools\Basket\DiscountHashValid;
use Bitkorn\Trinket\Table\ToolsTable;

/**
 *
 * @author allapow
 */
class ShopBasketDiscountEntity extends \Bitkorn\Shop\Entity\AbstractEntity
{

    /**
     *
     * @var array
     */
    private $discountTypes = ['unique', 'quantity', 'time', 'date'];

    /**
     *
     * @var array
     */
    private $discountAmountTypes = ['static', 'percent'];

    /**
     *
     * @var array Array with Key=property; value=db column
     */
    protected $mapping = [
        'shop_basket_discount_id' => 'shop_basket_discount_id',
        'shop_basket_discount_hash' => 'shop_basket_discount_hash',
        'shop_basket_discount_type' => 'shop_basket_discount_type',
        'shop_basket_discount_typevalue' => 'shop_basket_discount_typevalue',
        'shop_basket_discount_amount_type' => 'shop_basket_discount_amount_type',
        'shop_basket_discount_amount_typevalue' => 'shop_basket_discount_amount_typevalue',
        'shop_basket_discount_min_order_amount' => 'shop_basket_discount_min_order_amount',
        'shop_basket_discount_active' => 'shop_basket_discount_active',
        'shop_basket_discount_desc' => 'shop_basket_discount_desc',
        'shop_basket_discount_time' => 'shop_basket_discount_time',
        'user_uuid' => 'user_uuid'
    ];

    /**
     *
     * @var boolean
     */
    private $hashValid = false;

    /**
     *
     * @var float Computed discount
     */
    private $computedDiscountValue;

    /**
     * @return boolean true if discount-hash is still valid
     */
    public function isHashValid()
    {
        if (empty($this->storage) || !$this->hashValid) {
            return false;
        }
        return true;
    }

    /**
     * 
     * @param ShopBasketTablex $shopBasketTablex
     * @return boolean hash valid or not
     */
    private function computeTypeValid(ShopBasketTablex $shopBasketTablex)
    {
        switch ($this->storage['shop_basket_discount_type']) {
            case 'unique':
                if ($this->storage['shop_basket_discount_active'] == 0) {
                    $this->hashValid = false;
                    break;
                }
                $hashCount = $shopBasketTablex->countShopBasketDiscount($this->storage['shop_basket_discount_hash']);
                $this->hashValid = DiscountHashValid::isDiscountHashValid('unique', '', $hashCount, 0);
//                if ($hashCount == 0) {
//                    $this->hashValid = true;
//                } else {
//                    $this->hashValid = false;
//                }
                break;
            case 'quantity':
                if ($this->storage['shop_basket_discount_active'] == 0) {
                    $this->hashValid = false;
                    break;
                }
                $hashCount = $shopBasketTablex->countShopBasketDiscount($this->storage['shop_basket_discount_hash']);
                $this->hashValid = DiscountHashValid::isDiscountHashValid('quantity', $this->storage['shop_basket_discount_typevalue'], $hashCount, 0);
//                if ($hashCount <= $this->storage['shop_basket_discount_typevalue']) {
//                    $this->hashValid = true;
//                } else {
//                    $this->hashValid = false;
//                }
                break;
            case 'time':
                if ($this->storage['shop_basket_discount_active'] == 0) {
                    $this->hashValid = false;
                    break;
                }
                $this->hashValid = DiscountHashValid::isDiscountHashValid('time', $this->storage['shop_basket_discount_typevalue'], 0,
                                $this->storage['shop_basket_discount_time']);
//                $lastValidTime = $this->storage['shop_basket_discount_time'] + $this->storage['shop_basket_discount_typevalue'] * 24 * 60 * 60;
//                if ($lastValidTime > time()) {
//                    $this->hashValid = true;
//                } else {
//                    $this->hashValid = false;
//                }
                break;
            case 'date':
                if ($this->storage['shop_basket_discount_active'] == 0) {
                    $this->hashValid = false;
                    break;
                }
                $this->hashValid = DiscountHashValid::isDiscountHashValid('date', $this->storage['shop_basket_discount_typevalue'], 0, 0);
//                $discountTypeValueTime = intval($this->storage['shop_basket_discount_typevalue']);
//                $now = time();
//                if (date('Y', $discountTypeValueTime) >= date('Y', $now) && date('m', $discountTypeValueTime) >= date('m', $now) && date('d', $discountTypeValueTime) >= date('d', $now)) {
//                    $this->hashValid = true;
//                } else {
//                    $this->hashValid = false;
//                }
                break;
        }
        return $this->hashValid;
    }

    public function loadShopBasketDiscount($discountHash, ShopBasketDiscountTable $shopBasketDiscountTable, ShopBasketTablex $hopBasketTablex)
    {
        $shopBasketDiscountData = $shopBasketDiscountTable->getShopBasketDiscount($discountHash);
        if (empty($shopBasketDiscountData)) {
            return false;
        }
        $this->exchangeArray($shopBasketDiscountData);
        if (!$this->computeTypeValid($hopBasketTablex)) {
            $shopBasketDiscountTable->deactivateShopBasketDiscount($discountHash);
        }
    }

    /**
     * 
     * @param ShopBasketTablex $shopBasketTablex
     * @return boolean hash valid or not
     */
    public function canReaktivate(ShopBasketTablex $shopBasketTablex)
    {
        switch ($this->storage['shop_basket_discount_type']) {
            case 'unique':
                $hashCount = $shopBasketTablex->countShopBasketDiscount($this->storage['shop_basket_discount_hash']);
                if ($hashCount == 0) {
                    $this->hashValid = true;
                } else {
                    $this->hashValid = false;
                }
                break;
            case 'quantity':
                $hashCount = $shopBasketTablex->countShopBasketDiscount($this->storage['shop_basket_discount_hash']);
                if ($hashCount <= $this->storage['shop_basket_discount_typevalue']) {
                    $this->hashValid = true;
                } else {
                    $this->hashValid = false;
                }
                break;
            case 'time':
                $lastValidTime = $this->storage['shop_basket_discount_time'] + $this->storage['shop_basket_discount_typevalue'] * 24 * 60 * 60;
                if ($lastValidTime > time()) {
                    $this->hashValid = true;
                } else {
                    $this->hashValid = false;
                }
                break;
            case 'date':
                if ($this->storage['shop_basket_discount_typevalue'] > time()) {
                    $this->hashValid = true;
                } else {
                    $this->hashValid = false;
                }
                break;
        }
        return $this->hashValid;
    }

    public function computeDiscountValue($basketPrice)
    {
        if (!$this->hashValid || empty($this->storage['shop_basket_discount_amount_typevalue'])) {
            return 0;
        }
        switch ($this->storage['shop_basket_discount_amount_type']) {
            case 'static':
                $this->computedDiscountValue = $this->storage['shop_basket_discount_amount_typevalue'];
                return $this->computedDiscountValue;
            case 'percent':
                $this->computedDiscountValue = $basketPrice * ($this->storage['shop_basket_discount_amount_typevalue'] / 100);
                return $this->computedDiscountValue;
            default :
                return 0;
        }
    }

    public function saveNewShopBasketDiscount(ShopBasketDiscountTable $shopBasketDiscountTable)
    {
        if (!in_array($this->storage['shop_basket_discount_type'], $this->discountTypes) || !in_array($this->storage['shop_basket_discount_amount_type'],
                        $this->discountAmountTypes)) {
            throw new \RuntimeException('generateDiscountHash() one type is unvalid. $type: ' . $this->storage['shop_basket_discount_type'] . '; $amountType: ' . $this->storage['shop_basket_discount_amount_type']);
        }
        if (empty($this->storage['user_uuid'])) {
            unset($this->storage['user_uuid']);
        }
        if ($this->storage['shop_basket_discount_type'] == 'date') {
            $shopBasketDiscountTypeValueDateTime = new \DateTime(filter_var($this->storage['shop_basket_discount_typevalue'], FILTER_SANITIZE_STRING));
            $this->storage['shop_basket_discount_typevalue'] = $shopBasketDiscountTypeValueDateTime->getTimestamp();
        }
        return $shopBasketDiscountTable->saveNewShopBasketDiscount($this->storage);
    }

    /**
     * 
     * @param ShopBasketDiscountTable $shopBasketDiscountTable
     * @param string $type Value from ENUM('unique', 'quantity', 'time', 'date')
     * @param string $typeValue Value suitable for $type
     * @param string $amountType Value from ENUM('static', 'percent')
     * @param float $amountTypeValue Value suitable for $amountType
     * @param string $desc
     * @return boolean true on success
     * @throws \RuntimeException
     */
    public function createAndLoadShopBasketDiscount(ShopBasketDiscountTable $shopBasketDiscountTable, $type, $typeValue, $amountType, $amountTypeValue,
            $desc = '')
    {
        if (!in_array($type, $this->discountTypes) || !in_array($amountType, $this->discountAmountTypes)) {
            throw new \RuntimeException('generateDiscountHash() one type is unvalid. $type: ' . $type . '; $amountType: ' . $amountType);
        }

        $discountHash = self::generateDiscountHash($shopBasketDiscountTable);

        if (!in_array($type, $this->discountTypes)) {
            return false;
        }
        switch ($type) {
            case 'unique':
                $typeValue = '';
                break;
            case 'quantity':
                $typeValue = (float) $typeValue;
                break;
            case 'time':
                $typeValue = (int) $typeValue;
                break;
            case 'date':
                $typeValue = (int) $typeValue;
                break;
        }
        if ($shopBasketDiscountTable->saveNewShopBasketDiscount([
                    'shop_basket_discount_hash' => $discountHash,
                    'shop_basket_discount_type' => $type,
                    'shop_basket_discount_typevalue' => $typeValue,
                    'shop_basket_discount_amount_type' => $amountType,
                    'shop_basket_discount_amount_typevalue' => $amountTypeValue,
                    'shop_basket_discount_desc' => $desc,
                    'shop_basket_discount_time' => time()
                ]) < 1) {
            return false;
        }
        if (!$this->exchangeArray($shopBasketDiscountTable->getShopBasketDiscount($discountHash))) {
            throw new \RuntimeException('createAndLoadShopBasketDiscount() load from table fail. $discountHash: ' . $discountHash . '; $type: ' . $type . '; $typeValue: ' . $typeValue);
        }
        return true;
    }

    /**
     * 
     * @param ShopBasketDiscountTable $shopBasketDiscountTable
     * @param string $prefix
     * @param int $length
     * @return string
     * @throws \RuntimeException
     */
    public static function generateDiscountHash(ShopBasketDiscountTable $shopBasketDiscountTable, $prefix = '', $length = 7)
    {
        $discountHash = null;
        $generateDiscountHashCount = 0;
        do {
            $discountHash = DiscountHash::generateDiscountHash($prefix, $length);
            if ($shopBasketDiscountTable->existShopBasketDiscountHash($discountHash)) {
                $discountHash = null;
                $generateDiscountHashCount++;
            }
        } while ($generateDiscountHashCount < 5 && !isset($discountHash));
        if (!isset($discountHash)) {
            throw new \RuntimeException('generateDiscountHash runs more than 5 times');
        }
        return $discountHash;
    }

    /**
     * 
     * @return int
     */
    public function getMinOrderAmount()
    {
        if (!isset($this->storage['shop_basket_discount_min_order_amount'])) {
            throw new \RuntimeException(__CLASS__ . '->' . __FUNCTION__ . '(): without storage');
        }
        return $this->storage['shop_basket_discount_min_order_amount'];
    }

}
