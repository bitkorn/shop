<?php

namespace Bitkorn\Shop\Entity\Basket;

use Bitkorn\Trinket\Entity\AbstractEntity;

/**
 * Class ShopBasketEntity
 * Entity for database table `shop_basket`.
 *
 * @package Bitkorn\Shop\Entity\Basket
 */
class ShopBasketEntity extends AbstractEntity
{
    public array $mapping = [
        'shop_basket_unique' => 'shop_basket_unique',
        'user_uuid' => 'user_uuid',
        'user_uuid_pos' => 'user_uuid_pos',
        'shop_basket_time_create' => 'shop_basket_time_create',
        'shop_basket_time_order' => 'shop_basket_time_order',
        'shop_basket_time_payed' => 'shop_basket_time_payed',
        'shop_basket_status' => 'shop_basket_status',
        'payment_method' => 'payment_method',
        'payment_number' => 'payment_number',
        'shipping_provider_unique_descriptor' => 'shipping_provider_unique_descriptor',
        'shipping_method' => 'shipping_method',
        'shop_basket_discount_hash' => 'shop_basket_discount_hash',
        'shop_basket_discount_value' => 'shop_basket_discount_value',
        'computed_value_article_id_count' => 'computed_value_article_id_count',
        'computed_value_article_amount_sum' => 'computed_value_article_amount_sum',
        'computed_value_article_weight_sum' => 'computed_value_article_weight_sum',
        'computed_value_article_shipping_costs_single_sum' => 'computed_value_article_shipping_costs_single_sum',
        'computed_value_article_price_total_sum' => 'computed_value_article_price_total_sum',
        'computed_value_article_tax_total_sum' => 'computed_value_article_tax_total_sum',
        'computed_value_article_price_total_sum_end' => 'computed_value_article_price_total_sum_end',
        'computed_value_article_tax_total_sum_end' => 'computed_value_article_tax_total_sum_end',
        'computed_value_shop_basket_discount_tax' => 'computed_value_shop_basket_discount_tax',
        'computed_value_article_shipping_costs_computed' => 'computed_value_article_shipping_costs_computed',
        'computed_value_article_shipping_costs_tax_computed' => 'computed_value_article_shipping_costs_tax_computed',
        'computed_value_shop_basket_tax_total_sum_end' => 'computed_value_shop_basket_tax_total_sum_end',
    ];
}
