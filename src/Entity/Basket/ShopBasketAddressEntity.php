<?php

namespace Bitkorn\Shop\Entity\Basket;

use Bitkorn\Shop\Table\Basket\ShopBasketAddressTable;

/**
 * @author allapow
 */
class ShopBasketAddressEntity extends \Bitkorn\Shop\Entity\AbstractEntity
{

    /**
     *
     * @var array Array with Key=property; value=db column
     */
    protected $mapping = [
        'shop_basket_address_id' => 'shop_basket_address_id',
        'shop_basket_unique' => 'shop_basket_unique',
        'shop_basket_address_customer_type' => 'shop_basket_address_customer_type',
        'shop_basket_address_type' => 'shop_basket_address_type',
        'shop_basket_address_salut' => 'shop_basket_address_salut',
        'shop_basket_address_degree' => 'shop_basket_address_degree',
        'shop_basket_address_name1' => 'shop_basket_address_name1',
        'shop_basket_address_name2' => 'shop_basket_address_name2',
        'shop_basket_address_street' => 'shop_basket_address_street',
        'shop_basket_address_street_no' => 'shop_basket_address_street_no',
        'shop_basket_address_zip' => 'shop_basket_address_zip',
        'shop_basket_address_city' => 'shop_basket_address_city',
        'shop_basket_address_additional' => 'shop_basket_address_additional',
        'country_id' => 'country_id',
        'shop_basket_address_email' => 'shop_basket_address_email',
        'shop_basket_address_tel' => 'shop_basket_address_tel',
        'shop_basket_address_birthday' => 'shop_basket_address_birthday',
        'shop_basket_address_tax_id' => 'shop_basket_address_tax_id',
        'shop_basket_address_company_name' => 'shop_basket_address_company_name',
        'shop_basket_address_company_department' => 'shop_basket_address_company_department',
    ];

    /**
     * If the data validated by a form, this is perhaps true.
     * @var boolean
     */
    private $validForm = false;

    /**
     *
     * @param boolean $validForm
     */
    public function setValidForm($validForm)
    {
        $this->validForm = $validForm;
    }

    /**
     *
     * @return boolean
     */
    public function isValidForm()
    {
        return $this->validForm;
    }

    public function saveOrUpdate(ShopBasketAddressTable $shopBasketAddressTable, $addressesCount)
    {
        if (empty($this->storage['shop_basket_address_birthday'])) {
            $this->storage['shop_basket_address_birthday'] = 0; // MySQL 5.7 Incorrect integer value: '' for column
        }
        if (empty($this->storage['shop_basket_address_id']) && $addressesCount < 2) {
            unset($this->storage['shop_basket_address_id']); // MySQL 5.7 Incorrect integer value: '' for column
            switch ($addressesCount) {
                case 0:
                    $this->storage['shop_basket_address_type'] = 'invoice';
                    break;
                case 1:
                    $this->storage['shop_basket_address_type'] = 'shipment';
            }
            return $this->save($shopBasketAddressTable);
        } elseif (!empty($this->storage['shop_basket_address_id'])) {
            unset($this->storage['shop_basket_address_type']); // no update
            return $this->update($shopBasketAddressTable);
        }
        return -1;
    }

    public function save(ShopBasketAddressTable $shopBasketAddressTable)
    {

        return $shopBasketAddressTable->saveNewShopBasketAddress($this->storage);
    }

    public function update(ShopBasketAddressTable $shopBasketAddressTable)
    {
        return $shopBasketAddressTable->updateShopBasketAddress($this->storage);
    }

    public function delete(ShopBasketAddressTable $shopBasketAddressTable)
    {
        return $shopBasketAddressTable->deleteShopBasketAddress($this->storage);
    }

    public function isBasketUniqueValid($sessionBasketUnique)
    {
        if (empty($this->storage['shop_basket_unique']) || empty($sessionBasketUnique)) {
            return false;
        }
        return $this->storage['shop_basket_unique'] == $sessionBasketUnique;
    }

}
