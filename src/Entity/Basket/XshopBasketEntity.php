<?php

namespace Bitkorn\Shop\Entity\Basket;

use Bitkorn\Mail\Mail\SimpleMailer;
use Bitkorn\Shop\Entity\Article\ShopArticleOptionsEntity;
use Bitkorn\Shop\Entity\Article\TypeDelivery\ShopArticleLengthcutDefEntity;
use Bitkorn\Shop\Service\Shipping\ShippingServiceInterface;
use Bitkorn\Shop\Table\Article\Option\ShopArticleOptionItemArticlePricediffTable;
use Bitkorn\Shop\Table\Article\Param2d\ShopArticleParam2dItemTable;
use Bitkorn\Shop\Table\Article\ShopArticleStockTable;
use Bitkorn\Shop\Table\Article\TypeDelivery\ShopArticleLengthcutDefTable;
use Bitkorn\Shop\Table\Basket\ShopBasketEntityTable;
use Bitkorn\Shop\Table\Basket\ShopBasketTable;
use Bitkorn\Shop\Tablex\Article\ShopArticleImageTablex;
use Bitkorn\Shop\Tablex\Basket\ShopBasketEntityTablex;
use Bitkorn\Trinket\Entity\AbstractEntity;

/**
 * Xtendet ShopBasket with ShopBasketItems.
 * Weil ein NUR-ShopBasket-Entity unnuetz ist, gleich eins mit den Items.
 * Also bleibt $this->storage leer (stattdessen $this->storageItems) weils so besser aussieht :)
 * @author allapow
 */
class XshopBasketEntity extends AbstractEntity
{

    /**
     * This is used if no storageItems inside, to have an empty basket.
     * @var string
     */
    private $basketUnique;

    public function __construct($basketUnique)
    {
        $this->basketUnique = $basketUnique;
    }

    /**
     * db.shop_basket_entity
     *
     * @var array Array with Key=property; value=db column
     */
    protected array $mapping = [
        // basket
        'shop_basket_unique' => 'shop_basket_unique', // Cookie
        'user_uuid' => 'user_uuid', // after login
        'user_uuid_pos' => 'user_uuid_pos', // POS? ...default 0
        'shop_basket_time_create' => 'shop_basket_time_create',
        'shop_basket_time_order' => 'shop_basket_time_order',
        'shop_basket_time_payed' => 'shop_basket_time_payed',
        'shop_basket_status' => 'shop_basket_status',
        'payment_method' => 'payment_method',
        'payment_number' => 'payment_number',
        'payment_info' => 'payment_info', // only in table shop_basket_entity
        'shipping_provider_unique_descriptor' => 'shipping_provider_unique_descriptor',
        'shipping_method' => 'shipping_method',
        'shop_basket_discount_hash' => 'shop_basket_discount_hash',
        'shop_basket_discount_value' => 'shop_basket_discount_value',
        // items
        'shop_basket_item_id' => 'shop_basket_item_id',
        'shop_article_id' => 'shop_article_id',
        'shop_article_amount' => 'shop_article_amount',
        'shop_basket_item_article_options' => 'shop_basket_item_article_options',
        'shop_article_param2d_item_id' => 'shop_article_param2d_item_id',
        'shop_basket_item_lengthcut_options' => 'shop_basket_item_lengthcut_options',
        // article
        'shop_article_sku' => 'shop_article_sku', // Artikel Nr.
        'shop_article_type' => 'shop_article_type',
        'shop_article_type_delivery' => 'shop_article_type_delivery',
        'shop_article_sefurl' => 'shop_article_sefurl',
        'shop_article_size_def_id' => 'shop_article_size_def_id',
        'shop_article_weight' => 'shop_article_weight',
        'shop_article_shipping_costs_single' => 'shop_article_shipping_costs_single',
        'shop_article_price' => 'shop_article_price',
        'shop_article_price_total' => 'shop_article_price_total', // (sbi.shop_article_amount * sa.shop_article_price) AS shop_article_price_total
        'shop_article_tax' => 'shop_article_tax',
        'shop_article_price_net' => 'shop_article_price_net',
        'shop_article_name' => 'shop_article_name',
        'shop_article_desc' => 'shop_article_desc',
        'shop_article_active' => 'shop_article_active',
        'shop_article_stock_using' => 'shop_article_stock_using',
        // computed values for all articles in the basket
        'computed_value_article_id_count' => 'computed_value_article_id_count',
        'computed_value_article_amount_sum' => 'computed_value_article_amount_sum',
        'computed_value_article_weight_sum' => 'computed_value_article_weight_sum',
        'computed_value_article_shipping_costs_single_sum' => 'computed_value_article_shipping_costs_single_sum',
        'computed_value_article_price_total_sum' => 'computed_value_article_price_total_sum',
        'computed_value_article_tax_total_sum' => 'computed_value_article_tax_total_sum',
        'computed_value_article_price_total_sum_end' => 'computed_value_article_price_total_sum_end',
        'computed_value_article_tax_total_sum_end' => 'computed_value_article_tax_total_sum_end',
        'computed_value_shop_basket_discount_tax' => 'computed_value_shop_basket_discount_tax',
        'computed_value_article_shipping_costs_computed' => 'computed_value_article_shipping_costs_computed',
        'computed_value_article_shipping_costs_tax_computed' => 'computed_value_article_shipping_costs_tax_computed',
        'computed_value_shop_basket_tax_total_sum_end' => 'computed_value_shop_basket_tax_total_sum_end',
        // computed values for one basket item
        'computed_value_article_price' => 'computed_value_article_price',
        'computed_value_article_tax' => 'computed_value_article_tax',
        'computed_value_article_amount' => 'computed_value_article_amount',
        'computed_value_article_price_total' => 'computed_value_article_price_total',
        'computed_value_article_tax_total' => 'computed_value_article_tax_total',
        'computed_value_article_weight_total' => 'computed_value_article_weight_total',
    ];

    /**
     *
     * @var array
     */
    public $storageItems = [];

    /**
     *
     * @var array
     */
    private $storageItemsItemIds = [];

    /**
     *
     * @var array
     */
    private $storageItemsArticleIds = [];

    /**
     *
     * @var array articleID => [imagesData]
     */
    private $imagesArticleIdAssoc = [];

    /**
     *
     * @var array [imagesData]
     */
    private $imagesData = [];

    /**
     *
     * @var int
     */
    private $articleIdCount = 0;

    /**
     *
     * @var float
     */
    private $articleAmountSum = 0;

    /**
     *
     * @var float
     */
    private $articleWeightSum = 0;

    /**
     *
     * @var float
     */
    private $articlePriceTotalSum = 0;

    /**
     *
     * @var float
     */
    private $articleTaxTotalSum = 0;

    /**
     *
     * @var float
     */
    private $articlePriceTotalSumEnd = 0;
    private $articleTaxTotalSumEnd = 0;
    private $basketTaxTotalSumEnd = 0;
    private $articleShippingCostsSingleSum = 0;
    private $articleShippingCostsComputed = 0;
    private $articleShippingCostsTaxComputed = 0;
    private $shippingMethod = '';

    /**
     * Compute and save in BasketController->basketAction()
     * @var string
     */
    private $basketDiscountHash = '';
    private $basketDiscountValue = 0;
    private $basketStatus = '';

    /**
     * Compute and save in BasketController->basketAction()
     * @var float
     */
    private $basketDiscountTax = 0;
    private $discountPercentage = 0;
    public $valuesComputed = false;

    /**
     * Use this function and NOT exchangeArray($data) from \Bitkorn\Shop\Entity\AbstractEntity()!
     * Allways put data from db view view_basket_items
     * @param array $basketData
     * @return boolean
     */
    public function exchangeArrayBasket(array $basketData)
    {
        foreach ($basketData as $basketRow) {
            $tmp = [];
            $storageKeys = array_values($this->mapping);
            foreach ($storageKeys as $storageKey) {
                $tmp[$storageKey] = '';
            }
            if (!is_array($basketRow)) {
                continue;
            }
            foreach ($basketRow as $key => $value) {
                if (isset($this->mapping[$key])) {
                    $tmp[$this->mapping[$key]] = $value;
                }
            }
            $this->storageItems[] = $tmp;
            $this->storageItemsItemIds[] = $tmp['shop_basket_item_id'];
            $this->storageItemsArticleIds[] = $tmp['shop_article_id'];
        }
        if (empty($this->storageItems)) {
            return false;
        }
        if (isset($this->basketUnique) && $this->basketUnique != $this->storageItems[0]['shop_basket_unique']) {
            throw new \RuntimeException('basketUniques can\'t be different in BasketEntity!');
        }
        $this->basketUnique = $this->storageItems[0]['shop_basket_unique'];
        $this->setComputedValues();
        $this->valuesComputed = false;
        return true;
    }

    /**
     *
     * @param ShippingServiceInterface $shippingService
     */
    public function computeShippingCosts(ShippingServiceInterface $shippingService)
    {
        if (!$this->valuesComputed) {
            throw new \RuntimeException(__CLASS__ . '->' . __FUNCTION__ . '() $valuesComputed = false');
        }
        if (!$this->isShippingPickupCustomer()) {
            $prov = $shippingService->getShippingProvider();
            if (!$prov) {
                return;
            }
            if (empty($this->articleShippingCostsComputed = $prov->getTotalShippingCosts())) {
                $this->articleShippingCostsComputed = $prov->getShippingCostsRating();
            }
            if ($this->articleShippingCostsComputed > 0) {
                $this->articleShippingCostsTaxComputed = round((
                        $this->articleShippingCostsComputed / (100 + $shippingService->getShippingTaxPercentage())
                    ) * $shippingService->getShippingTaxPercentage(), 2);
                $this->basketTaxTotalSumEnd = $this->articleShippingCostsTaxComputed + $this->articleTaxTotalSumEnd;
            } else {
                $this->basketTaxTotalSumEnd = $this->articleTaxTotalSumEnd;
            }
        } else {
            $this->articleShippingCostsComputed = 0;
            $this->articleShippingCostsTaxComputed = 0;
            $this->basketTaxTotalSumEnd = $this->articleTaxTotalSumEnd;
        }
    }

    public function loadImagesArticleIdAssoc(ShopArticleImageTablex $shopArticleImageTablex)
    {
        if (empty($this->storageItemsArticleIds)) {
            return [];
        }
        foreach ($this->storageItemsArticleIds as $articleId) {
            $shopArticleImagesData = $shopArticleImageTablex->getShopArticleImages($articleId);
            if (!empty($shopArticleImagesData)) {
                $this->imagesData = array_merge($shopArticleImagesData, $this->imagesData);
                foreach ($shopArticleImagesData as $shopArticleImageData) {
                    if (!isset($this->imagesArticleIdAssoc[$articleId])) {
                        $this->imagesArticleIdAssoc[$articleId] = [];
                    }
                    $this->imagesArticleIdAssoc[$articleId][] = $shopArticleImageData;
                }
            }
        }
        return $this->imagesArticleIdAssoc;
    }

    public function getStorageItems()
    {
        if (!empty($this->storageItems[0]['shop_article_id'])) {
            return $this->storageItems;
        }
        return [];
    }

    public function setBasketStatus(string $basketStatus)
    {
        $this->basketStatus = $basketStatus;
    }

    public function getBasketStatus()
    {
        return $this->basketStatus;
    }

    public function incrementArticleIdCount()
    {
        $this->articleIdCount++;
    }

    /*
     * ############################ computed values #############################
     */

    public function getArticleIdCount()
    {
        return $this->articleIdCount;
    }

    public function setArticleAmountSum(float $articleAmount)
    {
        $this->articleAmountSum = $articleAmount;
    }

    public function getArticleAmountSum()
    {
        return $this->articleAmountSum;
    }

    public function setArticleWeightSum(float $articleWeight)
    {
        $this->articleWeightSum = $articleWeight;
    }

    public function getArticleWeightSum()
    {
        return $this->articleWeightSum;
    }

    public function setArticlePriceTotalSum(float $articlePriceTotal)
    {
        $this->articlePriceTotalSum = round($articlePriceTotal, 2);
    }

    /**
     * @return float|int Sum of all articles (brutto/gross)
     */
    public function getArticlePriceTotalSum()
    {
        return $this->articlePriceTotalSum;
    }

    public function setArticleTaxTotalSum(float $articleTaxTotal)
    {
        $this->articleTaxTotalSum = $articleTaxTotal;
    }

    public function getArticleTaxTotalSum()
    {
        return $this->articleTaxTotalSum;
    }

    /**
     * @return float|int articlePriceTotalSum +/- discount
     */
    public function getArticlePriceTotalSumEnd()
    {
        return $this->articlePriceTotalSumEnd;
    }

    /**
     * @param float $articlePriceTotalSumEnd
     */
    public function setArticlePriceTotalSumEnd(float $articlePriceTotalSumEnd): void
    {
        $this->articlePriceTotalSumEnd = $articlePriceTotalSumEnd;
    }

    public function getArticleTaxTotalSumEnd()
    {
        return $this->articleTaxTotalSumEnd;
    }

    /**
     * @param int $articleTaxTotalSumEnd
     */
    public function setArticleTaxTotalSumEnd(int $articleTaxTotalSumEnd): void
    {
        $this->articleTaxTotalSumEnd = $articleTaxTotalSumEnd;
    }

    /**
     * @return int shop_basket_discount_value
     */
    public function getBasketDiscountValue()
    {
        return $this->basketDiscountValue;
    }

    /**
     * @return float|int computed_value_shop_basket_discount_tax
     */
    public function getBasketDiscountTax()
    {
        return $this->basketDiscountTax;
    }

    /**
     * @return string shop_basket_discount_hash
     */
    public function getBasketDiscountHash()
    {
        return $this->basketDiscountHash;
    }

    public function addArticleShippingCostsSingleSum(float $articleShippingCosts)
    {
        $this->articleShippingCostsSingleSum += $articleShippingCosts;
    }

    public function getArticleShippingCostsSingleSum()
    {
        return $this->articleShippingCostsSingleSum;
    }

    /**
     * @return int computed_value_article_shipping_costs_computed
     */
    public function getArticleShippingCostsComputed()
    {
        return $this->articleShippingCostsComputed;
    }

    public function getArticleShippingCostsTaxComputed()
    {
        return $this->articleShippingCostsTaxComputed;
    }

    public function getBasketTaxTotalSumEnd()
    {
        return $this->basketTaxTotalSumEnd;
    }

    /**
     * @param string $shippingMethod
     */
    public function setShippingMethod(string $shippingMethod): void
    {
        $this->shippingMethod = $shippingMethod;
    }

    /**
     * @return string
     */
    public function getShippingMethod(): string
    {
        return $this->shippingMethod;
    }

    /**
     * articlePriceTotalSumEnd + articleShippingCostsComputed
     * @return float
     */
    public function getTotalCosts()
    {
        return $this->articlePriceTotalSumEnd + $this->articleShippingCostsComputed;
    }

    /*
     * ##########################################################################
     */

    public function getShippingProviderUniqueDescriptor(): string
    {
        if (!isset($this->storageItems[0]['shipping_provider_unique_descriptor'])) {
            return '';
        }
        return $this->storageItems[0]['shipping_provider_unique_descriptor'];
    }

    public function isShippingPickupCustomer()
    {
        if (!isset($this->storageItems[0]['shipping_provider_unique_descriptor'])) {
            return false;
        }
        return $this->storageItems[0]['shipping_provider_unique_descriptor'] == 'cp';
    }

    /**
     * @return string
     */
    public function getUserUuid(): string
    {
        if (empty($this->storageItems[0]) || empty($this->storageItems[0]['user_uuid'])) {
            return '';
        }
        return $this->storageItems[0]['user_uuid'];
    }

    public function setBasketUnique($basketUnique)
    {
        $this->basketUnique = $basketUnique;
    }

    public function getBasketUnique()
    {
        if (!isset($this->basketUnique)) {
            return -1;
        }
        return $this->basketUnique;
    }

    public function getStorageItemsItemIds()
    {
        return $this->storageItemsItemIds;
    }

    public function getStorageItemsArticleIds()
    {
        return $this->storageItemsArticleIds;
    }

    public function getImagesArticleIdAssoc()
    {
        return $this->imagesArticleIdAssoc;
    }

    public function getImagesData()
    {
        return $this->imagesData;
    }

    /**
     * @return array Only the fields for db.shop_basket
     */
    public function getShopBasketValuesWithComputedValues(): array
    {
        if (count($this->storageItems) < 1) {
            return [];
        }
        $values = $this->storageItems[0];
        $values['computed_value_article_id_count'] = $this->articleIdCount;
        $values['computed_value_article_amount_sum'] = $this->articleAmountSum;
        $values['computed_value_article_weight_sum'] = $this->articleWeightSum;
        $values['computed_value_article_shipping_costs_single_sum'] = $this->articleShippingCostsSingleSum;
        $values['computed_value_article_price_total_sum'] = $this->articlePriceTotalSum;
        $values['computed_value_article_tax_total_sum'] = $this->articleTaxTotalSum;
        $values['computed_value_article_price_total_sum_end'] = $this->articlePriceTotalSumEnd;
        $values['computed_value_article_tax_total_sum_end'] = $this->articleTaxTotalSumEnd;
        $values['computed_value_shop_basket_discount_tax'] = $this->basketDiscountTax;
        $values['computed_value_article_shipping_costs_computed'] = $this->articleShippingCostsComputed;
        $values['computed_value_article_shipping_costs_tax_computed'] = $this->articleShippingCostsTaxComputed;
        $values['computed_value_shop_basket_tax_total_sum_end'] = $this->basketTaxTotalSumEnd;
        $values['shipping_method'] = $this->shippingMethod;
        $diff = array_diff_key($values, (new ShopBasketEntity())->getMapping());
        foreach ($diff as $key => $val) {
            unset($values[$key]);
        }
        return $values;
    }

    /**
     * Set local member values from storageItems
     * @return bool
     */
    protected function setComputedValues(): bool
    {
        if (count($this->storageItems) < 1) {
            return false;
        }
        $item = $this->storageItems[0];
        $this->articleIdCount = $item['computed_value_article_id_count'];
        $this->articleAmountSum = $item['computed_value_article_amount_sum'];
        $this->articleWeightSum = $item['computed_value_article_weight_sum'];
        $this->articleShippingCostsSingleSum = $item['computed_value_article_shipping_costs_single_sum'];
        $this->articlePriceTotalSum = $item['computed_value_article_price_total_sum'];
        $this->articleTaxTotalSum = $item['computed_value_article_tax_total_sum'];
        $this->articlePriceTotalSumEnd = $item['computed_value_article_price_total_sum_end'];
        $this->articleTaxTotalSumEnd = $item['computed_value_article_tax_total_sum_end'];
        $this->basketDiscountTax = $item['computed_value_shop_basket_discount_tax'];
        $this->articleShippingCostsComputed = $item['computed_value_article_shipping_costs_computed'];
        $this->articleShippingCostsTaxComputed = $item['computed_value_article_shipping_costs_tax_computed'];
        $this->basketTaxTotalSumEnd = $item['computed_value_shop_basket_tax_total_sum_end'];
        $this->shippingMethod = $item['shipping_method'];
        return true;
    }

    /**
     * @param bool $valuesComputed
     */
    public function setValuesComputed(bool $valuesComputed): void
    {
        $this->valuesComputed = $valuesComputed;
    }

    /**
     * @param string $basketDiscountHash
     */
    public function setBasketDiscountHash(string $basketDiscountHash): void
    {
        $this->basketDiscountHash = $basketDiscountHash;
    }

    /**
     * @param int $basketDiscountValue
     */
    public function setBasketDiscountValue(int $basketDiscountValue): void
    {
        $this->basketDiscountValue = $basketDiscountValue;
    }

    /**
     * @param float $basketDiscountTax
     */
    public function setBasketDiscountTax(float $basketDiscountTax): void
    {
        $this->basketDiscountTax = $basketDiscountTax;
    }

    /**
     * @return int
     */
    public function getDiscountPercentage(): int
    {
        return $this->discountPercentage;
    }

    /**
     * @param int $discountPercentage
     */
    public function setDiscountPercentage(int $discountPercentage): void
    {
        $this->discountPercentage = $discountPercentage;
    }

    /**
     * @param int $basketTaxTotalSumEnd
     */
    public function setBasketTaxTotalSumEnd(int $basketTaxTotalSumEnd): void
    {
        $this->basketTaxTotalSumEnd = round($basketTaxTotalSumEnd, 2);
    }

    public function setComputedValueShopArticlePrice(int $itemIndex, float $price)
    {
        if(!isset($this->storageItems[$itemIndex])) {
            throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() item not set');
        }
        $this->storageItems[$itemIndex]['computed_value_article_price'] = round($price, 2);
    }

    public function setComputedValueShopArticleTax(int $itemIndex, float $tax)
    {
        if(!isset($this->storageItems[$itemIndex])) {
            throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() item not set');
        }
        $this->storageItems[$itemIndex]['computed_value_article_tax'] = round($tax, 2);
    }

    public function setComputedValueShopArticleAmount(int $itemIndex, float $amount)
    {
        if(!isset($this->storageItems[$itemIndex])) {
            throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() item not set');
        }
        $this->storageItems[$itemIndex]['computed_value_article_amount'] = $amount;
    }

    public function setComputedValueShopArticlePriceTotal(int $itemIndex, float $price)
    {
        if(!isset($this->storageItems[$itemIndex])) {
            throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() item not set');
        }
        $this->storageItems[$itemIndex]['computed_value_article_price_total'] = round($price, 2);
    }

    public function setComputedValueShopArticleTaxTotal(int $itemIndex, float $tax)
    {
        if(!isset($this->storageItems[$itemIndex])) {
            throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() item not set');
        }
        $this->storageItems[$itemIndex]['computed_value_article_tax_total'] = round($tax, 2);
    }

    public function setComputedValueShopArticleWeightTotal(int $itemIndex, float $weight)
    {
        if(!isset($this->storageItems[$itemIndex])) {
            throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() item not set');
        }
        $this->storageItems[$itemIndex]['computed_value_article_weight_total'] = $weight;
    }
}
