<?php

namespace Bitkorn\Shop\Entity\Basket\Address;

/**
 * Description of Destination
 *
 * @author allapow
 */
class IsoCountry
{
    
    private $countryId;
    private $countryIso;
    private $countryName;
    private $countryEuMember;
    
    /**
     * 
     * @param int $countryId
     * @param int $countryIso
     * @param string $countryName
     * @param int $countryEuMember 1=yes; 0=no
     */
    public function __construct($countryId, $countryIso, $countryName, $countryEuMember)
    {
        $this->countryId = $countryId;
        $this->countryIso = $countryIso;
        $this->countryName = $countryName;
        $this->countryEuMember = $countryEuMember;
    }

    public function getCountryId()
    {
        return $this->countryId;
    }

    public function getCountryIso()
    {
        return $this->countryIso;
    }

    public function getCountryName()
    {
        return $this->countryName;
    }

    public function getCountryEuMember()
    {
        return $this->countryEuMember;
    }


    
}
