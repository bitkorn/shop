<?php

namespace Bitkorn\Shop\Entity\Address;

use Bitkorn\Trinket\Entity\AbstractEntity;

/**
 *
 * @author allapow
 */
class ShopAddressEntity extends AbstractEntity
{

    /**
     * @var array Array with Key=property; value=db column
     */
    protected array $mapping = [
        'address_id' => 'address_id',
        'address_customer_type' => 'address_customer_type',
        'address_type' => 'address_type',
        'address_salut' => 'address_salut',
        'address_degree' => 'address_degree',
        'address_name1' => 'address_name1',
        'address_name2' => 'address_name2',
        'address_street' => 'address_street',
        'address_street_no' => 'address_street_no',
        'address_zip' => 'address_zip',
        'address_city' => 'address_city',
        'address_additional' => 'address_additional',
        'country_id' => 'country_id',
        'address_email' => 'address_email',
        'address_tel' => 'address_tel',
        'address_birthday' => 'address_birthday',
        'address_tax_id' => 'address_tax_id',
        'address_company_name' => 'address_company_name',
        'address_company_department' => 'address_company_department',
        // join
        'country_name' => 'country_name',
        'country_iso' => 'country_iso',
        'country_member_eu' => 'country_member_eu',
        'country_currency_euro' => 'country_currency_euro',
    ];

    /**
     * @var array Array with Key = user db; value = $this->mapping key
     */
    protected array $mappingUser = [
        'shop_user_address_id' => 'address_id',
        'shop_user_address_customer_type' => 'address_customer_type',
        'shop_user_address_type' => 'address_type',
        'shop_user_address_salut' => 'address_salut',
        'shop_user_address_degree' => 'address_degree',
        'shop_user_address_name1' => 'address_name1',
        'shop_user_address_name2' => 'address_name2',
        'shop_user_address_street' => 'address_street',
        'shop_user_address_street_no' => 'address_street_no',
        'shop_user_address_zip' => 'address_zip',
        'shop_user_address_city' => 'address_city',
        'shop_user_address_additional' => 'address_additional',
        'country_id' => 'country_id',
        'shop_user_address_tel' => 'address_tel',
        'shop_user_address_birthday' => 'address_birthday',
        'shop_user_address_tax_id' => 'address_tax_id',
        'shop_user_address_company_name' => 'address_company_name',
        'shop_user_address_company_department' => 'address_company_department',
        // join
        'country_name' => 'country_name',
        'country_iso' => 'country_iso',
        'country_member_eu' => 'country_member_eu',
        'country_currency_euro' => 'country_currency_euro',
    ];

    /**
     * @var array Array with Key = user db; value = $this->mapping key
     */
    protected array $mappingBasket = [
        'shop_basket_address_id' => 'address_id',
        'shop_basket_address_customer_type' => 'address_customer_type',
        'shop_basket_address_type' => 'address_type',
        'shop_basket_address_salut' => 'address_salut',
        'shop_basket_address_degree' => 'address_degree',
        'shop_basket_address_name1' => 'address_name1',
        'shop_basket_address_name2' => 'address_name2',
        'shop_basket_address_street' => 'address_street',
        'shop_basket_address_street_no' => 'address_street_no',
        'shop_basket_address_zip' => 'address_zip',
        'shop_basket_address_city' => 'address_city',
        'shop_basket_address_additional' => 'address_additional',
        'country_id' => 'country_id',
        'shop_basket_address_email' => 'address_email',
        'shop_basket_address_tel' => 'address_tel',
        'shop_basket_address_birthday' => 'address_birthday',
        'shop_basket_address_tax_id' => 'address_tax_id',
        'shop_basket_address_company_name' => 'address_company_name',
        'shop_basket_address_company_department' => 'address_company_department',
        // join
        'country_name' => 'country_name',
        'country_iso' => 'country_iso',
        'country_member_eu' => 'country_member_eu',
        'country_currency_euro' => 'country_currency_euro',
    ];

    /**
     * @var array|string[] db.shop_basket_entity
     */
    protected array $mappingEntityUserShipment = [
        'shop_user_address_shipment_id' => 'address_id',
        'address_customer_type' => 'address_customer_type', // enum_shop_address_customer_type: private, enterprise
        'address_type' => 'address_type', // enum_shop_address_type: invoice, shipment
        'address_salut' => 'address_salut', // not by shipment
        'address_degree' => 'address_degree', // ???
        'shop_user_address_shipment_name1' => 'address_name1',
        'shop_user_address_shipment_name2' => 'address_name2',
        'shop_user_address_shipment_street' => 'address_street',
        'shop_user_address_shipment_street_no' => 'address_street_no',
        'shop_user_address_shipment_zip' => 'address_zip',
        'shop_user_address_shipment_city' => 'address_city',
        'shop_user_address_shipment_additional' => 'address_additional',
        'shop_user_address_shipment_country_id' => 'country_id',
        'address_email' => 'address_email', // not by shipment
        'shop_user_address_shipment_tel' => 'address_tel',
        'address_birthday' => 'address_birthday', // not by shipment
        'address_tax_id' => 'address_tax_id', // not by shipment
        'shop_user_address_shipment_company_name' => 'address_company_name',
        'shop_user_address_shipment_company_department' => 'address_company_department',
        // join
        'country_name' => 'country_name',
        'country_iso' => 'country_iso',
        'country_member_eu' => 'country_member_eu',
        'country_currency_euro' => 'country_currency_euro',
    ];

    /**
     * @var array|string[] db.shop_basket_entity
     */
    protected array $mappingEntityUserInvoice = [
        'shop_user_address_invoice_id' => 'address_id',
        'address_customer_type' => 'address_customer_type', // enum_shop_address_customer_type: private, enterprise
        'address_type' => 'address_type', // enum_shop_address_type: invoice, shipment
        'shop_user_address_invoice_salut' => 'address_salut',
        'shop_user_address_invoice_degree' => 'address_degree', // ???
        'shop_user_address_invoice_name1' => 'address_name1',
        'shop_user_address_invoice_name2' => 'address_name2',
        'shop_user_address_invoice_street' => 'address_street',
        'shop_user_address_invoice_street_no' => 'address_street_no',
        'shop_user_address_invoice_zip' => 'address_zip',
        'shop_user_address_invoice_city' => 'address_city',
        'shop_user_address_invoice_additional' => 'address_additional',
        'shop_user_address_invoice_country_id' => 'country_id',
        'address_email' => 'address_email', // JOIN
        'shop_user_address_invoice_tel' => 'address_tel',
        'address_birthday' => 'address_birthday', // none
        'shop_user_address_tax_id' => 'address_tax_id', // none
        'shop_user_address_invoice_company_name' => 'address_company_name',
        'shop_user_address_invoice_company_department' => 'address_company_department',
        // join
        'country_name' => 'country_name',
        'country_iso' => 'country_iso',
        'country_member_eu' => 'country_member_eu',
        'country_currency_euro' => 'country_currency_euro',
    ];

    /**
     * @var array|string[] db.shop_basket_entity
     */
    protected array $mappingEntityBasketShipment = [
        'shop_basket_address_shipment_id' => 'address_id',
        'address_customer_type' => 'address_customer_type', // enum_shop_address_customer_type: private, enterprise
        'address_type' => 'address_type', // enum_shop_address_type: invoice, shipment
        'shop_user_address_invoice_salut' => 'address_salut',
        'shop_user_address_invoice_degree' => 'address_degree', // ???
        'shop_basket_address_shipment_name1' => 'address_name1',
        'shop_basket_address_shipment_name2' => 'address_name2',
        'shop_basket_address_shipment_street' => 'address_street',
        'shop_basket_address_shipment_street_no' => 'address_street_no',
        'shop_basket_address_shipment_zip' => 'address_zip',
        'shop_basket_address_shipment_city' => 'address_city',
        'shop_basket_address_shipment_additional' => 'address_additional',
        'shop_basket_address_shipment_country_id' => 'country_id',
        'shop_basket_address_shipment_email' => 'address_email',
        'shop_basket_address_shipment_tel' => 'address_tel',
        'address_birthday' => 'address_birthday', // none
        'shop_user_address_tax_id' => 'address_tax_id', // none
        'shop_basket_address_shipment_company_name' => 'address_company_name',
        'shop_basket_address_shipment_company_department' => 'address_company_department',
        // join
        'country_name' => 'country_name',
        'country_iso' => 'country_iso',
        'country_member_eu' => 'country_member_eu',
        'country_currency_euro' => 'country_currency_euro',
    ];

    /**
     * @var array|string[] db.shop_basket_entity
     */
    protected array $mappingEntityBasketInvoice = [
        'shop_basket_address_invoice_id' => 'address_id',
        'address_customer_type' => 'address_customer_type', // enum_shop_address_customer_type: private, enterprise
        'address_type' => 'address_type', // enum_shop_address_type: invoice, shipment
        'shop_basket_address_invoice_salut' => 'address_salut',
        'shop_basket_address_invoice_degree' => 'address_degree', // ???
        'shop_basket_address_invoice_name1' => 'address_name1',
        'shop_basket_address_invoice_name2' => 'address_name2',
        'shop_basket_address_invoice_street' => 'address_street',
        'shop_basket_address_invoice_street_no' => 'address_street_no',
        'shop_basket_address_invoice_zip' => 'address_zip',
        'shop_basket_address_invoice_city' => 'address_city',
        'shop_basket_address_invoice_additional' => 'address_additional',
        'shop_basket_address_invoice_country_id' => 'country_id',
        'shop_basket_address_invoice_email' => 'address_email',
        'shop_basket_address_invoice_tel' => 'address_tel',
        'address_birthday' => 'address_birthday', // none
        'shop_basket_address_tax_id' => 'address_tax_id', // none
        'shop_basket_address_invoice_company_name' => 'address_company_name',
        'shop_basket_address_invoice_company_department' => 'address_company_department',
        // join
        'country_name' => 'country_name',
        'country_iso' => 'country_iso',
        'country_member_eu' => 'country_member_eu',
        'country_currency_euro' => 'country_currency_euro',
    ];

    /**
     * Sets the values only if there is a corresponding mapping.
     * Exchange $data to an Array.
     *
     * @param array $data
     * @return bool
     */
    private function exchangeArrayAddress(array $data): bool
    {
        $this->storage = [];
        foreach ($this->mapping as $key => $value) {
            if (isset($data[$key])) {
                $this->storage[$value] = $data[$key];
            }
        }
        if (empty($this->storage)) {
            return false;
        }
        if (!empty($this->primaryKey) && !empty($this->storage[$this->primaryKey])) {
            $this->unique = $this->storage[$this->primaryKey];
        }
        return true;
    }

    public function exchangeArrayFromDatabaseForUser(array $data): bool
    {
        $this->flipMapping();
        $this->mapping = $this->mappingUser;
        return $this->exchangeArrayAddress($data);
    }

    public function exchangeArrayFromDatabaseForBasket(array $data): bool
    {
        $this->flipMapping();
        $this->mapping = $this->mappingBasket;
        return $this->exchangeArrayAddress($data);
    }

    public function exchangeArrayForBasketEntityUserShipment(array $data): bool
    {
        $this->flipMapping();
        $this->mapping = $this->mappingEntityUserShipment;
        return $this->exchangeArrayAddress($data);
    }

    public function exchangeArrayForBasketEntityUserInvoice(array $data): bool
    {
        $this->flipMapping();
        $this->mapping = $this->mappingEntityUserInvoice;
        return $this->exchangeArrayAddress($data);
    }

    public function exchangeArrayForBasketEntityBasketShipment(array $data): bool
    {
        $this->flipMapping();
        $this->mapping = $this->mappingEntityBasketShipment;
        return $this->exchangeArrayAddress($data);
    }

    public function exchangeArrayForBasketEntityBasketInvoice(array $data): bool
    {
        $this->flipMapping();
        $this->mapping = $this->mappingEntityBasketInvoice;
        return $this->exchangeArrayAddress($data);
    }

    public function getAddressType(): string
    {
        if (!isset($this->storage['address_type'])) {
            return '';
        }
        return $this->storage['address_type'];
    }

    public function getAddressName1(): string
    {
        if (!isset($this->storage['address_name1'])) {
            return '';
        }
        return $this->storage['address_name1'];
    }

    public function getAddressName2(): string
    {
        if (!isset($this->storage['address_name2'])) {
            return '';
        }
        return $this->storage['address_name2'];
    }

    public function getAddressName1and2(): string
    {
        if (!isset($this->storage['address_name1']) || !isset($this->storage['address_name2'])) {
            return '';
        }
        return $this->storage['address_name1'] . ' ' . $this->storage['address_name2'];
    }

    public function getAddressStreet(): string
    {
        if (!isset($this->storage['address_street'])) {
            return '';
        }
        return $this->storage['address_street'];
    }

    public function getAddressStreetNo(): string
    {
        if (!isset($this->storage['address_street_no'])) {
            return '';
        }
        return $this->storage['address_street_no'];
    }

    public function getAddressStreetWithNumber(): string
    {
        if (!isset($this->storage['address_street'])) {
            return '';
        }
        return $this->storage['address_street'] . (isset($this->storage['address_street_no']) ? ' ' . $this->storage['address_street_no'] : '');
    }

    public function getAddressLine(): string
    {
        if (!isset($this->storage['address_street'])) {
            return '';
        }
        return $this->getAddressStreetWithNumber() . ', ' . $this->storage['address_zip'] . ' ' . $this->storage['address_city']
            . ', ' . strtoupper($this->storage['country_iso']) . ' - ' . $this->storage['country_name'];
    }

    public function getAddressZip(): string
    {
        if (!isset($this->storage['address_zip'])) {
            return '';
        }
        return $this->storage['address_zip'];
    }

    public function getAddressCity(): string
    {
        if (!isset($this->storage['address_city'])) {
            return '';
        }
        return $this->storage['address_city'];
    }

    public function getAddressZipAndCity(): string
    {
        if (!isset($this->storage['address_zip']) || !isset($this->storage['address_city'])) {
            return '';
        }
        return $this->storage['address_zip'] . ' ' . $this->storage['address_city'];
    }

    /**
     * @return int The country_id or -1.
     */
    public function getAddressCountryId(): int
    {
        if (!isset($this->storage['country_id'])) {
            return -1;
        }
        return intval($this->storage['country_id']);
    }

    public function getAddressEmail(): string
    {
        if (!isset($this->storage['address_email'])) {
            return '';
        }
        return $this->storage['address_email'];
    }

    public function getAddressTel(): string
    {
        if (!isset($this->storage['address_tel'])) {
            return '';
        }
        return $this->storage['address_tel'];
    }

    public function getCountryName(): string
    {
        if (!isset($this->storage['country_name'])) {
            return '';
        }
        return $this->storage['country_name'];
    }

    public function getCountryIso(): string
    {
        if (!isset($this->storage['country_iso'])) {
            return '';
        }
        return $this->storage['country_iso'];
    }

    public function getCountryIsoAndName(): string
    {
        if (!isset($this->storage['country_iso']) || !isset($this->storage['country_name'])) {
            return '';
        }
        return $this->storage['country_iso'] . '-' . $this->storage['country_name'];
    }
}
