<?php

namespace Bitkorn\Shop\Validator;

use Bitkorn\Shop\Table\Article\ShopArticleTable;

/**
 *
 * @author allapow
 */
class ShopArticleSefurlValidator extends \Laminas\Validator\AbstractValidator
{

    const SEFURL_STRINGS = 'sefurl';
    const SEFURL_EXIST = 'sefurl exist';
    const EXPRESSION = "/^[a-z0-9][a-z0-9-]*[a-z0-9]$/";

    protected $messageTemplates = [
        self::SEFURL_STRINGS => "Nur kleine Buchstaben, Zahlen oder das Minus Zeichen (Start und Ende kein Minus)!",
        self::SEFURL_EXIST => "Diese sef URL existiert bereits und darf nur einmal vorkommen."
    ];

    /**
     *
     * @var ShopArticleTable
     */
    protected $shopArticleTable;

    /**
     *
     * @var int
     */
    protected $selfArticleId;

    /**
     *
     * @param ShopArticleTable $shopArticleTable
     */
    public function setShopArticleTable(ShopArticleTable $shopArticleTable)
    {
        $this->shopArticleTable = $shopArticleTable;
    }

    public function isValid($value)
    {
        $this->setValue($value);
        if (!preg_match(self::EXPRESSION, $this->value)) {
            $this->error(self::SEFURL_STRINGS);
            return FALSE;
        }
        if ($this->shopArticleTable->existShopArticleSefurl($this->value, $this->getSelfArticleId())) {
            $this->error(self::SEFURL_EXIST);
            return FALSE;
        }
        return true;
    }

    public function getSelfArticleId()
    {
        if ($this->selfArticleId === null) {
//            throw new \RuntimeException('selfArticleId option is mandatory');
            $this->selfArticleId = 0;
        }
        return $this->selfArticleId;
    }

    public function setSelfArticleId($selfArticleId)
    {
        $this->selfArticleId = $selfArticleId;
        return $this;
    }

}
