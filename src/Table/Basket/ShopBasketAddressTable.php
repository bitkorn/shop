<?php

namespace Bitkorn\Shop\Table\Basket;

use Bitkorn\Shop\Entity\Basket\Address\IsoCountry;
use Bitkorn\Shop\Table\AbstractShopTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Select;

/**
 *
 * @author allapow
 */
class ShopBasketAddressTable extends AbstractShopTable
{

    /**
     * @var string
     */
    protected $table = 'shop_basket_address';

    const ADDRESS_TYPE_INVOICE = 'invoice';
    const ADDRESS_TYPE_SHIPPING = 'shipment';

    /**
     *
     * @param array $storage
     * @return int
     */
    public function saveNewShopBasketAddress(array $storage)
    {
        $insert = $this->sql->insert();
        try {
            $insert->values($storage);
            return $this->insertWith($insert);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function updateShopBasketAddress(array $storage)
    {
        $update = $this->sql->update();
        try {
            $update->set($storage);
            $update->where([
                'shop_basket_address_id' => $storage['shop_basket_address_id'],
                'shop_basket_unique' => $storage['shop_basket_unique'] // for security reason
            ]);
            return $this->updateWith($update);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function deleteShopBasketAddress(array $storage)
    {
        $delete = $this->sql->delete();
        try {
            $delete->where([
                'shop_basket_address_id' => $storage['shop_basket_address_id'],
                'shop_basket_unique' => $storage['shop_basket_unique'] // for security reason
            ]);
            return $this->deleteWith($delete);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function deleteShopBasketAddresses(string $basketUnique): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where([
                'shop_basket_unique' => $basketUnique
            ]);
            return $this->deleteWith($delete);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function deleteShopBasketAddressByBasketUnique($basketUnique)
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['shop_basket_unique' => $basketUnique]);
            return $this->deleteWith($delete);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param string $basketUnique
     * @return array Assoc array with shop_basket_address_type as key. First address is the invoice address.
     */
    public function getShopBasketAddressesByBasketUnique(string $basketUnique)
    {
        $select = $this->sql->select();
        $assoc = [];
        try {
            $select->join('country', 'country.country_id = shop_basket_address.country_id'
                , ['country_name', 'country_iso', 'country_member_eu', 'country_currency_euro'], Select::JOIN_LEFT);
            $select->where([
                'shop_basket_unique' => $basketUnique,
            ]);
            $select->order(['shop_basket_address_id ASC']);
            /** @var HydratingResultSet $resultset */
            $resultset = $this->selectWith($select);
            if ($resultset->valid() && $resultset->count() > 0) {
                $resultArr = $resultset->toArray();
                foreach ($resultArr as $address) {
                    $assoc[$address['shop_basket_address_type']] = $address;
                }
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return $assoc;
    }

    public function getShopBasketAddressById($addressId)
    {
        $select = $this->sql->select();
        try {
            $select->join('country', 'country.country_id = shop_basket_address.country_id'
                , ['country_name', 'country_iso', 'country_member_eu', 'country_currency_euro'], Select::JOIN_LEFT);
            $select->where([
                'shop_basket_address_id' => $addressId,
            ]);
            /** @var HydratingResultSet $resultset */
            $resultset = $this->selectWith($select);
            if ($resultset->valid() && $resultset->count() == 1) {
                $current = $resultset->current();
                return $current->getArrayCopy();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function countShopBasketAddresses($basketUnique): int
    {
        $select = $this->sql->select();
        $select->where([
            'shop_basket_unique' => $basketUnique,
        ]);
        try {
            $resultset = $this->selectWith($select);
            if ($resultset->valid()) {
                return $resultset->count();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return 0;
    }

}
