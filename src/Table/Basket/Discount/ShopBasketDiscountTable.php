<?php

namespace Bitkorn\Shop\Table\Basket\Discount;

use Bitkorn\Shop\Table\AbstractShopTable;
use Laminas\Db\Sql\Predicate\PredicateSet;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Expression;

/**
 *
 * @author allapow
 */
class ShopBasketDiscountTable extends AbstractShopTable
{

    /**
     * @var string
     */
    protected $table = 'shop_basket_discount';

    /**
     * @param array $storage
     * @return int
     */
    public function saveNewShopBasketDiscount(array $storage)
    {
        $insert = $this->sql->insert();
        try {
            unset($storage['shop_basket_discount_id']);
            $storage['shop_basket_discount_time'] = time();
            $insert->values($storage);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return $this->insertWith($insert);
    }

    /**
     * @param string $discountHash
     * @param int $onlyActive
     * @param int $fromTime
     * @param int $toTime
     * @param string $userUuid
     * @return array
     */
    public function getShopBasketDiscounts(string $discountHash = '', int $onlyActive = 1, int $fromTime = 0, int $toTime = 9999999999, string $userUuid = ''): array
    {
        $select = $this->sql->select();
        try {
            $selectBasket = new Select('shop_basket');
            $selectBasket->columns(['shop_basket_discount_hash_count' => new Expression('COUNT(shop_basket_discount_hash)')]);
            $selectBasket->where('shop_basket_discount_hash = shop_basket_discount.shop_basket_discount_hash');

            $select->columns([Select::SQL_STAR, 'shop_basket_discount_hash_count' => new Expression('?', [$selectBasket])]);

            $select->join('shop_user_data_number', 'shop_user_data_number.user_uuid = shop_basket_discount.user_uuid', Select::SQL_STAR, Select::JOIN_LEFT);
            $select->where->greaterThan('shop_basket_discount_time', $fromTime)->AND->lessThan('shop_basket_discount_time', $toTime);
            if ($onlyActive == 1) {
                $select->where([
                    'shop_basket_discount_active' => 1,
                ], PredicateSet::OP_AND);
            }
            if (!empty($discountHash)) {
                $select->where([
                    'shop_basket_discount_hash' => $discountHash,
                ], PredicateSet::OP_AND);
            }
            if (!empty($userUuid)) {
                $select->where([
                    'shop_basket_discount.user_uuid' => $userUuid,
                ], PredicateSet::OP_AND);
            }
            $resultset = $this->selectWith($select);
            if ($resultset->valid() && $resultset->count() > 0) {
                return $resultset->toArray();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $discountHash
     * @return array
     */
    public function getShopBasketDiscount($discountHash)
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'shop_basket_discount_hash' => $discountHash,
            ]);
            $resultset = $this->selectWith($select);
            if ($resultset->valid() && $resultset->count() > 0) {
                $resultArr = $resultset->toArray();
                return $resultArr[0];
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $discountHash
     * @return boolean
     */
    public function existShopBasketDiscountHash($discountHash)
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'shop_basket_discount_hash' => $discountHash,
            ]);
            $resultset = $this->selectWith($select);
            if ($resultset->valid() && $resultset->count() > 0) {
                return true;
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return false;
    }

    /**
     * @param int $shopBasketDiscountId
     * @return array
     */
    public function getShopBasketDiscountById(int $shopBasketDiscountId)
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'shop_basket_discount_id' => $shopBasketDiscountId,
            ]);
            $resultset = $this->selectWith($select);
            if ($resultset->valid() && $resultset->count() == 1) {
                $resultArr = $resultset->toArray();
                return $resultArr[0];
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $shopBasketDiscountHash
     * @return int
     */
    public function deactivateShopBasketDiscount($shopBasketDiscountHash)
    {
        $update = $this->sql->update();
        try {
            $update->set(['shop_basket_discount_active' => 0]);
            $update->where(['shop_basket_discount_hash' => $shopBasketDiscountHash]);
            return $this->updateWith($update);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param int $shopBasketDiscountId
     * @param array $storage
     * @return int
     */
    public function updateShopBasketDiscount(int $shopBasketDiscountId, array $storage): int
    {
        $update = $this->sql->update();
        if (isset($storage['shop_basket_discount_id'])) {
            unset($storage['shop_basket_discount_id']);
        }
        try {
            $update->set($storage);
            $update->where(['shop_basket_discount_id' => $shopBasketDiscountId]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

}
