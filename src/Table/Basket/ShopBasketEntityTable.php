<?php

namespace Bitkorn\Shop\Table\Basket;

use Bitkorn\Shop\Table\AbstractShopTable;
use Laminas\Db\Adapter\Driver\ResultInterface;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Predicate\PredicateSet;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Expression;

class ShopBasketEntityTable extends AbstractShopTable
{

    /**
     * @var string
     */
    protected $table = 'shop_basket_entity';

    /**
     * @param array $storageItem From \Bitkorn\Shop\Entity\Basket\XshopBasketEntity
     * @return int
     */
    public function saveShopBasketEntity(array $storageItem)
    {
        $insert = $this->sql->insert();
        try {
            $insert->values($storageItem);
            $stmt = $this->sql->prepareStatementForSqlObject($insert);
            $result = $stmt->execute(); // Laminas\Db\Adapter\Driver\Pdo\Result
            if ($result->valid() && ($result instanceof ResultInterface)) {
                return $this->getAdapter()->getDriver()->getConnection()->getLastGeneratedValue('public.shop_basket_entity_shop_basket_entity_id_seq');
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function getShopBasketEntity($shopBasketEntityId)
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'shop_basket_entity_id' => $shopBasketEntityId
            ]);
            $resultset = $this->selectWith($select);
            if ($resultset->valid() && $resultset->count() == 1) {
                return $resultset->current()->getArrayCopy();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function existShopBasket($basketUnique)
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'shop_basket_unique' => $basketUnique,
            ]);
            $resultset = $this->selectWith($select);
            if ($resultset->valid() && $resultset->count() > 0) {
                return true;
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return false;
    }

    public function getShopBasketEntitiesWithRatingsByUserId($userId)
    {
        $select = $this->sql->select();
        try {
            $select->columns([
                'shop_basket_entity_id',
                'shop_basket_unique',
                'shop_article_id',
                'shop_article_amount',
                'shop_basket_item_article_options',
                'shop_article_param2d_item_id',
                'shop_basket_item_lengthcut_options',
                'shop_article_name',
                'shop_article_desc',
                'shop_article_type',
                'shop_article_sku',
                'shop_article_price',
                'shop_article_price_total',
                'computed_value_article_shipping_costs_computed',
                'computed_value_article_price_total_sum',
                'shop_basket_time_create',
                'shop_basket_time_payed',
                'shop_basket_status'
            ]);
            $select->join('shop_article_rating', 'shop_article_rating.shop_basket_item_id = shop_basket_entity.shop_basket_item_id',
                ['shop_article_rating_value', 'shop_article_rating_text'], Select::JOIN_LEFT);
            $select->join('shop_document_order', 'shop_document_order.shop_basket_unique = shop_basket_entity.shop_basket_unique',
                ['shop_document_order_number'], Select::JOIN_LEFT);
            $select->where([
                'user_uuid' => $userId,
            ]);
            $select->order(['shop_basket_time_create DESC', 'shop_article_sku ASC']);
            $resultset = $this->selectWith($select);
            if ($resultset->valid() && $resultset->count() > 0) {
                return $resultset->toArray();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $basketUnique
     * @param int $limit
     * @return array
     */
    public function getShopBasketEntitiesByBasketUnique(string $basketUnique, int $limit = -1): array
    {
        $select = $this->sql->select();
        try {
            $select->columns(['*',
                'article_price_net' => new Expression('shop_basket_entity.shop_article_price * 100 / (100 + shop_basket_entity.shop_article_tax)'),
                'article_price_total_net' => new Expression('shop_article_price_total * 100 / (100 + shop_basket_entity.shop_article_tax)'),
            ]);
            $select->join('shop_article', 'shop_article.shop_article_id = shop_basket_entity.shop_article_id',
                'shop_article_sefurl', Select::JOIN_LEFT);
            $select->join('shop_document_order', 'shop_document_order.shop_basket_unique = shop_basket_entity.shop_basket_unique',
                'shop_document_order_number', Select::JOIN_LEFT);
            $select->where->equalTo('shop_basket_entity.shop_basket_unique', $basketUnique);
            if ($limit > 0) {
                $select->limit($limit);
            }
            $select->order(['shop_basket_entity.shop_article_sku ASC']);
            $resultset = $this->selectWith($select);
            if ($resultset->valid() && $resultset->count() > 0) {
                return $resultset->toArray();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $basketUnique
     * @return array
     */
    public function getShopBasketEntitiesByBasketUniqueWithRatings(string $basketUnique): array
    {
        $select = $this->sql->select();
        try {
            $select->columns([
                'shop_basket_entity_id',
                'shop_basket_item_id',
                'shop_article_id',
                'shop_basket_item_article_options',
                'shop_article_name',
                'shop_article_type',
                'shop_article_sku',
            ]);
            $select->join('shop_article_rating', 'shop_article_rating.shop_basket_item_id = shop_basket_entity.shop_basket_item_id',
                ['shop_article_rating_value', 'shop_article_rating_text'], Select::JOIN_LEFT);
            $select->where->equalTo('shop_basket_unique', $basketUnique);
            $select->order(['shop_article_sku ASC']);
            $resultset = $this->selectWith($select);
            if ($resultset->valid() && $resultset->count() > 0) {
                return $resultset->toArray();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * Geht davon aus: shop_basket_item_id ist unique in db.shop_basket_entity
     * @param string $shopBasketUnique
     * @param int $shopBasketItemId
     * @return array
     */
    public function getShopBasketEntityByBasketItemId($shopBasketUnique, $shopBasketItemId)
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'shop_basket_unique' => $shopBasketUnique,
                'shop_basket_item_id' => $shopBasketItemId
            ]);
            $resultset = $this->selectWith($select);
            if ($resultset->valid() && $resultset->count() == 1) {
                return $resultset->current();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     *
     * @param array $shopUserAddress
     * @param string $shopBasketUnique
     * @param string $shopUserAddressType shipping | invoice
     * @return int
     */
    public function updateShopUserAddress(array $shopUserAddress, $shopBasketUnique, $shopUserAddressType)
    {
        $updateArray = [];
        switch ($shopUserAddressType) {
            case 'invoice':
                $updateArray = [
                    'shop_user_address_invoice_id' => $shopUserAddress['shop_user_address_id'],
                    'shop_user_address_invoice_salut' => $shopUserAddress['shop_user_address_salut'],
                    'shop_user_address_invoice_degree' => $shopUserAddress['shop_user_address_degree'],
                    'shop_user_address_invoice_name1' => $shopUserAddress['shop_user_address_name1'],
                    'shop_user_address_invoice_name2' => $shopUserAddress['shop_user_address_name2'],
                    'shop_user_address_invoice_street' => $shopUserAddress['shop_user_address_street'],
                    'shop_user_address_invoice_street_no' => $shopUserAddress['shop_user_address_street_no'],
                    'shop_user_address_invoice_zip' => $shopUserAddress['shop_user_address_zip'],
                    'shop_user_address_invoice_city' => $shopUserAddress['shop_user_address_city'],
                    'shop_user_address_invoice_additional' => $shopUserAddress['shop_user_address_additional'],
                    'shop_user_address_invoice_country_id' => $shopUserAddress['country_id'],
                    'shop_user_address_invoice_tel' => $shopUserAddress['shop_user_address_tel'],
                    'shop_user_address_tax_id' => $shopUserAddress['shop_user_address_tax_id'] ?: '',
                    'shop_user_address_invoice_company_name' => $shopUserAddress['shop_user_address_company_name'],
                    'shop_user_address_invoice_company_department' => $shopUserAddress['shop_user_address_company_department'],
                ];
                break;
            case 'shipping':
                $updateArray = [
                    'shop_user_address_shipment_id' => $shopUserAddress['shop_user_address_id'],
                    'shop_user_address_shipment_name1' => $shopUserAddress['shop_user_address_name1'],
                    'shop_user_address_shipment_name2' => $shopUserAddress['shop_user_address_name2'],
                    'shop_user_address_shipment_street' => $shopUserAddress['shop_user_address_street'],
                    'shop_user_address_shipment_street_no' => $shopUserAddress['shop_user_address_street_no'],
                    'shop_user_address_shipment_zip' => $shopUserAddress['shop_user_address_zip'],
                    'shop_user_address_shipment_city' => $shopUserAddress['shop_user_address_city'],
                    'shop_user_address_shipment_additional' => $shopUserAddress['shop_user_address_additional'],
                    'shop_user_address_shipment_country_id' => $shopUserAddress['country_id'],
                    'shop_user_address_shipment_tel' => $shopUserAddress['shop_user_address_tel'],
                    'shop_user_address_shipment_company_name' => $shopUserAddress['shop_user_address_company_name'],
                    'shop_user_address_shipment_company_department' => $shopUserAddress['shop_user_address_company_department'],
                ];
                break;
        }
        $update = $this->sql->update();
        try {
            $update->set($updateArray);
            $update->where(['shop_basket_unique' => $shopBasketUnique]);
            return $this->updateWith($update);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function updateShopBasketAddress(array $shopBasketAddress, $shopBasketUnique, $shopUserAddressType)
    {
        $updateArray = [];
        switch ($shopUserAddressType) {
            case 'invoice':
                $updateArray = [
                    'shop_basket_address_invoice_id' => $shopBasketAddress['shop_basket_address_id'],
                    'shop_basket_address_invoice_salut' => $shopBasketAddress['shop_basket_address_salut'],
                    'shop_basket_address_invoice_degree' => $shopBasketAddress['shop_basket_address_degree'],
                    'shop_basket_address_invoice_name1' => $shopBasketAddress['shop_basket_address_name1'],
                    'shop_basket_address_invoice_name2' => $shopBasketAddress['shop_basket_address_name2'],
                    'shop_basket_address_invoice_street' => $shopBasketAddress['shop_basket_address_street'],
                    'shop_basket_address_invoice_street_no' => $shopBasketAddress['shop_basket_address_street_no'],
                    'shop_basket_address_invoice_zip' => $shopBasketAddress['shop_basket_address_zip'],
                    'shop_basket_address_invoice_city' => $shopBasketAddress['shop_basket_address_city'],
                    'shop_basket_address_invoice_additional' => $shopBasketAddress['shop_basket_address_additional'],
                    'shop_basket_address_invoice_country_id' => $shopBasketAddress['country_id'],
                    'shop_basket_address_invoice_email' => $shopBasketAddress['shop_basket_address_email'],
                    'shop_basket_address_invoice_tel' => $shopBasketAddress['shop_basket_address_tel'],
                    'shop_basket_address_tax_id' => $shopBasketAddress['shop_basket_address_tax_id'] ?: '',
                    'shop_basket_address_invoice_company_name' => $shopBasketAddress['shop_basket_address_company_name'],
                    'shop_basket_address_invoice_company_department' => $shopBasketAddress['shop_basket_address_company_department'],
                ];
                break;
            case 'shipping':
                $updateArray = [
                    'shop_basket_address_shipment_id' => $shopBasketAddress['shop_basket_address_id'],
                    'shop_basket_address_shipment_name1' => $shopBasketAddress['shop_basket_address_name1'],
                    'shop_basket_address_shipment_name2' => $shopBasketAddress['shop_basket_address_name2'],
                    'shop_basket_address_shipment_street' => $shopBasketAddress['shop_basket_address_street'],
                    'shop_basket_address_shipment_street_no' => $shopBasketAddress['shop_basket_address_street_no'],
                    'shop_basket_address_shipment_zip' => $shopBasketAddress['shop_basket_address_zip'],
                    'shop_basket_address_shipment_city' => $shopBasketAddress['shop_basket_address_city'],
                    'shop_basket_address_shipment_additional' => $shopBasketAddress['shop_basket_address_additional'],
                    'shop_basket_address_shipment_country_id' => $shopBasketAddress['country_id'],
                    'shop_basket_address_shipment_email' => $shopBasketAddress['shop_basket_address_email'],
                    'shop_basket_address_shipment_tel' => $shopBasketAddress['shop_basket_address_tel'],
                    'shop_basket_address_shipment_company_name' => $shopBasketAddress['shop_basket_address_company_name'],
                    'shop_basket_address_shipment_company_department' => $shopBasketAddress['shop_basket_address_company_department'],
                ];
                break;
        }
        $update = $this->sql->update();
        try {
            $update->set($updateArray);
            $update->where(['shop_basket_unique' => $shopBasketUnique]);
            return $this->updateWith($update);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param string $shopBasketUnique
     * @param string $shopBasketEntityComments
     * @return int
     */
    public function updateShopBasketEntityComments(string $shopBasketUnique, string $shopBasketEntityComments): int
    {
        $updateArray = ['shop_basket_entity_comments' => $shopBasketEntityComments];
        $update = $this->sql->update();
        try {
            $update->set($updateArray);
            $update->where(['shop_basket_unique' => $shopBasketUnique]);
            return $this->updateWith($update);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param int $daysAgoTimeOrder
     * @param int $shopBasketEntityId
     * @return array
     * If $daysAgoTimeOrder &lt; 0 and $shopBasketEntityId &gt; 0 then only one ShopBasketEntity fetched.
     * <br>
     * Else if $daysAgoTimeOrder &gt;= 0 then ShopBasketEntities with no ratingRemindSend-time and order-time greaterThanOrEqualTo $daysAgoTimeOrder are fetched.
     * <br>
     * Else an empty array.
     *
     * All grouped by BasketUnique
     */
    public function getShopBasketEntitiesWithoutRatingReminderBasketuniqueAssoc($daysAgoTimeOrder = -1, $shopBasketEntityId = -1)
    {
        $basketuniqueAssoc = [];
        if (!isset($daysAgoTimeOrder)) {
            return $basketuniqueAssoc;
        }
        $daysAgoTimeOrderUnix = $daysAgoTimeOrder * 24 * 60 * 60;
        $select = $this->sql->select();
        try {
            $select->columns([
                'shop_basket_entity_id',
                'shop_basket_unique',
                'user_uuid',
                'shop_user_address_invoice_salut',
                'shop_user_address_invoice_degree',
                'shop_user_address_invoice_name1',
                'shop_user_address_invoice_name2',
                'shop_basket_address_invoice_salut',
                'shop_basket_address_invoice_degree',
                'shop_basket_address_invoice_name1',
                'shop_basket_address_invoice_name2',
                'shop_basket_address_invoice_email',
            ]);
            $select->join('user', 'user.user_uuid = shop_basket_entity.user_uuid', ['email'], \Laminas\Db\Sql\Select::JOIN_LEFT);
            if ($daysAgoTimeOrder < 0 && $shopBasketEntityId > 0) {
                $select->where->equalTo('shop_basket_entity_id', $shopBasketEntityId);
            } elseif ($daysAgoTimeOrder >= 0) {
                $select->where->greaterThanOrEqualTo('shop_basket_time_order', $daysAgoTimeOrderUnix)->AND->isNull('shop_basket_entity_rating_reminder_send');
            } else {
                return [];
            }
            $select->group('shop_basket_unique');
            $resultset = $this->selectWith($select);
            if ($resultset->valid() && $resultset->count() > 0) {
                do {
                    $current = $resultset->current();
                    if (!isset($basketuniqueAssoc[$current['shop_basket_unique']])) {
                        $basketuniqueAssoc[$current['shop_basket_unique']] = [];
                    }
                    $basketuniqueAssoc[$current['shop_basket_unique']][] = $current;
                } while ($resultset->next());
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return $basketuniqueAssoc;
    }

    /**
     *
     * @param int $userUuidPos
     * @return array
     */
    public function getShopBasketByVendorUserId($userUuidPos)
    {
        $select = $this->sql->select();
        try {
            $select->columns(['shop_basket_entity_id', 'shop_basket_unique', 'user_uuid', 'user_uuid_pos',
                'shop_basket_time_create', 'shop_basket_time_order', 'shop_basket_time_payed',
                'shop_article_amount', 'shop_article_price_total', 'shop_article_tax',
                'shop_basket_discount_hash',
                'mlm_level' => new Expression('(SELECT \'sales\')'),
                'article_price_total_net' => new Expression('shop_article_price_total * 100 / (100 + shop_basket_entity.shop_article_tax)'),
                'article_fee_static' => new Expression('(SELECT shop_article_feeable FROM shop_article WHERE shop_article_id = shop_basket_entity.shop_article_id) * shop_article_amount * (SELECT shop_user_fee_static FROM shop_user_fee WHERE user_uuid=' . $userUuidPos . ')'),
                'article_fee_coupon_result' => new Expression('(SELECT (shop_article_price_total * 100 / (100 + shop_basket_entity.shop_article_tax)) * 0.01 * shop_user_fee_coupon_percent FROM shop_user_fee WHERE user_uuid=' . $userUuidPos . ')'),
                'shop_basket_address_name' => new Expression('(SELECT CONCAT_WS(\' \', IFNULL(shop_basket_address_name1,\'\'), IFNULL(shop_basket_address_name2,\'\')) AS a FROM shop_basket_address WHERE shop_basket_unique=shop_basket_entity.shop_basket_unique)'),
                'shop_user_address_name' => new Expression('(SELECT CONCAT_WS(\' \', IFNULL(shop_user_address_name1,\'\'), IFNULL(shop_user_address_name2,\'\')) FROM shop_user_address WHERE user_uuid=shop_basket_entity.user_uuid)'),
            ]);
            $select->join('shop_user_fee_paid'
                , 'shop_user_fee_paid.shop_basket_entity_id = shop_basket_entity.shop_basket_entity_id'
                ,
                [
                    'shop_basket_fee_paid_sum' => new Expression('IFNULL(shop_user_fee_paid.shop_user_fee_paid_sum,0)'),
                    'shop_user_fee_paid_time',
                    'shop_user_fee_paid_payment_id',
                    'shop_user_fee_paid_payment_method'
                ]
                , Select::JOIN_LEFT);

            $select->join('shop_user_fee'
                , 'shop_user_fee.user_uuid = shop_basket_entity.user_uuid_pos'
                ,
                [
                    'article_fee_percent_result' => new Expression('IFNULL(shop_user_fee.shop_user_fee_percent,0) * 0.01 * (shop_article_price_total * 100 / (100 + shop_basket_entity.shop_article_tax))'),
                    'shop_user_fee_percent',
                    'shop_user_fee_coupon_percent',
                    'shop_user_fee_static',
//                'shop_user_fee_mlm_parent_user_uuid',
//                'shop_user_fee_mlm_child_fee_percent',
//                'shop_user_fee_mlm_child_fee_percent_first',
//                'shop_user_fee_mlm_child_fee_coupon_percent'
                ]
                , Select::JOIN_LEFT);

            $select->join('shop_basket_discount'
                , 'shop_basket_discount.shop_basket_discount_hash = shop_basket_entity.shop_basket_discount_hash'
                , ['shop_basket_discount_hash']
                , Select::JOIN_LEFT);

            $select->join('shop_article'
                , 'shop_article.shop_article_id = shop_basket_entity.shop_article_id'
                , ['shop_article_feeable']
                , Select::JOIN_LEFT);

            $select->join('shop_document_invoice'
                , 'shop_document_invoice.shop_basket_unique = shop_basket_entity.shop_basket_unique'
                , ['shop_document_invoice_number']
                , Select::JOIN_LEFT);

            $selectCoupon = new Select();
            $selectCoupon->from('shop_basket_discount')->columns(['shop_basket_discount_hash'])->where(['user_uuid' => $userUuidPos]);
            $select->where(['shop_basket_entity.user_uuid_pos' => $userUuidPos])
                ->where->OR->in('shop_basket_entity.shop_basket_discount_hash', $selectCoupon);

            $select->order('shop_basket_time_order DESC, shop_basket_entity_id DESC');
            /** @var HydratingResultSet $resultset */
            $resultset = $this->selectWith($select);
            if ($resultset->valid() && $resultset->count() > 0) {
                return $resultset->toArray();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     *
     * @param int $userUuidPos
     * @return array
     */
    public function getShopBasketByVendorUserIdMlm($userUuidPos)
    {
        $select = $this->sql->select();
        try {
            $select->columns(['shop_basket_entity_id', 'shop_basket_unique', 'user_uuid', 'user_uuid_pos',
                'shop_basket_time_create', 'shop_basket_time_order', 'shop_basket_time_payed',
                'shop_article_amount', 'shop_article_price_total', 'shop_article_tax',
                'shop_basket_discount_hash',
                'mlm_level' => new Expression('(SELECT \'mlm\')'),
                'article_price_total_net' => new Expression('shop_article_price_total * 100 / (100 + shop_basket_entity.shop_article_tax)'),
                'article_fee_coupon_result' => new Expression('(SELECT (shop_article_price_total * 100 / (100 + shop_basket_entity.shop_article_tax)) * 0.01 * shop_user_fee_mlm_child_fee_coupon_percent FROM shop_user_fee WHERE user_uuid=' . $userUuidPos . ')'),
                'article_fee_percent_result' => new Expression('(SELECT (shop_article_price_total * 100 / (100 + shop_basket_entity.shop_article_tax)) * 0.01 * shop_user_fee_mlm_child_fee_percent FROM shop_user_fee WHERE user_uuid=' . $userUuidPos . ')'),
                'article_fee_percent_first_result' => new Expression('(SELECT IF((SELECT shop_basket_entity_id FROM shop_basket_entity WHERE user_uuid_pos IN(SELECT user_uuid FROM shop_user_fee WHERE shop_user_fee_mlm_parent_user_uuid=' . $userUuidPos . ') ORDER BY shop_basket_entity_id ASC LIMIT 1) = shop_basket_entity.shop_basket_entity_id, (SELECT (shop_article_price_total * 100 / (100 + shop_basket_entity.shop_article_tax)) * 0.01 * shop_user_fee_mlm_child_fee_percent_first FROM shop_user_fee WHERE user_uuid=' . $userUuidPos . '), 0))'),
                'shop_basket_address_name' => new Expression('(SELECT CONCAT_WS(\' \', IFNULL(shop_basket_address_name1,\'\'), IFNULL(shop_basket_address_name2,\'\')) AS a FROM shop_basket_address WHERE shop_basket_unique=shop_basket_entity.shop_basket_unique)'),
                'shop_user_address_name' => new Expression('(SELECT CONCAT_WS(\' \', IFNULL(shop_user_address_name1,\'\'), IFNULL(shop_user_address_name2,\'\')) FROM shop_user_address WHERE user_uuid=shop_basket_entity.user_uuid)'),
            ]);
            $select->join('shop_user_fee_paid'
                , 'shop_user_fee_paid.shop_basket_entity_id = shop_basket_entity.shop_basket_entity_id'
                ,
                [
                    'shop_basket_fee_paid_sum' => new Expression('IFNULL(shop_user_fee_paid.shop_user_fee_paid_sum,0)'),
                    'shop_user_fee_paid_time',
                    'shop_user_fee_paid_payment_id',
                    'shop_user_fee_paid_payment_method'
                ]
                , Select::JOIN_LEFT);

            $select->join('shop_basket_discount'
                , 'shop_basket_discount.shop_basket_discount_hash = shop_basket_entity.shop_basket_discount_hash'
                , ['shop_basket_discount_hash']
                , Select::JOIN_LEFT);

            $select->join('shop_document_invoice'
                , 'shop_document_invoice.shop_basket_unique = shop_basket_entity.shop_basket_unique'
                , ['shop_document_invoice_number']
                , Select::JOIN_LEFT);

            $selectMlmParent = new Select();
            $selectMlmParent->from('shop_user_fee')->columns(['user_uuid'])->where(['shop_user_fee_mlm_parent_user_uuid' => $userUuidPos]);
            $selectMlmCoupon = new Select();
            $selectMlmCoupon->from('shop_basket_discount')->columns(['shop_basket_discount_hash'])->where->in('user_uuid', $selectMlmParent);
            $select->where->in('shop_basket_entity.user_uuid_pos', $selectMlmParent)
                ->OR->in('shop_basket_entity.shop_basket_discount_hash', $selectMlmCoupon);

            $select->order('shop_basket_time_order DESC, shop_basket_entity_id DESC');
            /** @var HydratingResultSet $resultset */
            $resultset = $this->selectWith($select);
            if ($resultset->valid() && $resultset->count() > 0) {
                return $resultset->toArray();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function deleteShopBasketEntityByBasketUnique($basketUnique)
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['shop_basket_unique' => $basketUnique]);
            return $this->deleteWith($delete);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function sumOrderedAndPayedShopArticle($articleId, $articleOptions)
    {
        if (empty($articleId)) {
            return -1;
        }
        $select = $this->sql->select();
        try {
            $select->columns([
                'shop_article_amount_sum' => new Expression('SUM(shop_article_amount)')
            ]);
            $select->where(['shop_article_id' => $articleId]);
            if ($articleOptions != '[]') {
                $select->where(['shop_basket_item_article_options' => $articleOptions], PredicateSet::OP_AND);
            }
            $select->where->AND->in('shop_basket_status', ['ordered', 'payed']);
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                $current = $result->current();
                return $current['shop_article_amount_sum'];
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return 0;
    }

    /**
     * @param string $basketUnique
     * @param string $shippingMethod
     * @param string $shippingNumber
     * @return bool
     */
    public function updateShipping(string $basketUnique, string $shippingMethod, string $shippingNumber): bool
    {
        return $this->update(['shipping_number' => $shippingNumber, 'shipping_method' => $shippingMethod], ['shop_basket_unique' => $basketUnique]) >= 0;
    }

    /**
     * @param string $basketUnique
     * @return int
     */
    public function updateBasketItemRatingReminderSend(string $basketUnique): int
    {
        $update = $this->sql->update();
        try {
            $update->set(['shop_basket_entity_rating_reminder_send' => time()]);
            $update->where(['shop_basket_unique' => $basketUnique]);
            return $this->updateWith($update);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }
}
