<?php

namespace Bitkorn\Shop\Table\Basket\Claim;

use Bitkorn\Shop\Table\AbstractShopTable;

/**
 *
 * @author allapow
 */
class ShopBasketClaimTable extends AbstractShopTable
{

    /**
     *
     * @var string
     */
    protected $table = 'shop_basket_claim';

    /**
     *
     * @param array $storage
     * @param $userUuid
     * @return int
     */
    public function saveNewShopBasketClaim(array $storage, $userUuid)
    {
        $insert = $this->sql->insert();
        if(isset($storage['shop_basket_claim_id'])) {
            unset($storage['shop_basket_claim_id']);
        }
        try {

            $insert->values(array_merge($storage,
                [
                    'shop_basket_claim_user_create' => $userUuid,
                    'shop_basket_claim_time_create' => time()
                ]));
            $this->insertWith($insert);
            $contentId = $this->adapter->getDriver()->getConnection()->getLastGeneratedValue('public.shop_basket_claim_shop_basket_claim_id_seq');
            if (empty($contentId)) {
                return -1;
            }
            return $contentId;
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     *
     * @param int $shopBasketClaimId
     * @param array $storage
     * @return int
     */
    public function updateShopBasketClaim(int $shopBasketClaimId, array $storage): int
    {
        $update = $this->sql->update();
        try {
            $update->where(['shop_basket_claim_id' => $shopBasketClaimId]);
            $update->set($storage);
            return $this->updateWith($update);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     *
     * @param boolean $onlyUnfinished
     * @return array
     */
    public function getShopBasketClaims($onlyUnfinished = true)
    {
        $select = $this->sql->select();
        try {
            if ($onlyUnfinished) {
                $select->where([
                    'shop_basket_claim_finished' => 0,
                ]);
            }
            $select->order('shop_basket_claim_time_create DESC');
            $resultset = $this->selectWith($select);
            if ($resultset->valid() && $resultset->count() > 0) {
                return $resultset->toArray();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     *
     * @param string $basketUnique
     * @return array
     */
    public function getShopBasketClaimsFromBasket($basketUnique)
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'shop_basket_unique' => $basketUnique,
            ]);
            $resultset = $this->selectWith($select);
            if ($resultset->valid() && $resultset->count() > 0) {
                return $resultset->toArray();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     *
     * @param int $shopBasketClaimId
     * @return array
     */
    public function getShopBasketClaim(int $shopBasketClaimId)
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'shop_basket_claim_id' => $shopBasketClaimId,
            ]);
            $resultset = $this->selectWith($select);
            if ($resultset->valid() && $resultset->count() > 0) {
                $resultArr = $resultset->toArray();
                return $resultArr[0];
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     *
     * @param string $basketUnique
     * @return array Array with key=shop_basket_item_id; value=['shop_basket_claim_id' => 123, 'shop_basket_claim_reason' => 'blah sabbel']
     */
    public function getShopBasketClaimsFromBasketIdAssoc($basketUnique)
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'shop_basket_unique' => $basketUnique,
            ]);
            $resultset = $this->selectWith($select);
            if ($resultset->valid() && $resultset->count() > 0) {
                $idAssoc = [];
                do {
                    $current = $resultset->current();
                    $idAssoc[$current['shop_basket_item_id']]['shop_basket_claim_id'] = $current['shop_basket_claim_id'];
                    $idAssoc[$current['shop_basket_item_id']]['shop_basket_claim_reason'] = $current['shop_basket_claim_reason'];
                    $idAssoc[$current['shop_basket_item_id']]['shop_basket_claim_finished'] = $current['shop_basket_claim_finished'];
                } while ($resultset->next());
                return $idAssoc;
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function deleteShopBasketClaimByBasketUnique($basketUnique)
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['shop_basket_unique' => $basketUnique]);
            return $this->deleteWith($delete);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

}
