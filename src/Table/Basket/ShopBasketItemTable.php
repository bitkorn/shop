<?php

namespace Bitkorn\Shop\Table\Basket;

use Bitkorn\Shop\Table\AbstractShopTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\ResultSet\ResultSet;
use Laminas\Db\Sql\Predicate\PredicateSet;
use Laminas\Db\Sql\Select;

/**
 * @author allapow
 */
class ShopBasketItemTable extends AbstractShopTable
{

    /**
     * @var string
     */
    protected $table = 'shop_basket_item';

    /**
     * @param string $basketUnique
     * @return array Result from db view view_basket_items
     */
    public function getShopBasketItems(string $basketUnique): array
    {
        $select = new Select('view_basket_items');
        try {
            $select->where([
                'shop_basket_unique' => $basketUnique
            ]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     *
     * @param string $basketUnique $basketItemId wueder ausreichen, aber so noch sicherer gegen Hacker
     * @param int $basketItemId
     * @param float $newAmount
     * @return int
     */
    public function editAmount($basketUnique, $basketItemId, $newAmount): int
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'shop_article_amount' => $newAmount
            ]);
            $update->where([
                'shop_basket_unique' => $basketUnique,
                'shop_basket_item_id' => $basketItemId
            ]);
            return $this->updateWith($update);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     *
     * @param string $basketUnique $basketItemId wueder ausreichen, aber so noch sicherer gegen Hacker
     * @param int $basketItemId
     * @return int
     */
    public function deleteShopBasketItem($basketUnique, $basketItemId): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where([
                'shop_basket_unique' => $basketUnique,
                'shop_basket_item_id' => $basketItemId
            ]);
            return $this->deleteWith($delete);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     *
     * @param string $basketUnique
     * @return int
     */
    public function deleteShopBasketItemsAll($basketUnique)
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['shop_basket_unique' => $basketUnique]);
            return $this->deleteWith($delete);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function getShopBasketItem(string $basketUnique, int $articleId, string $articleOptionsJson = '', int $articleParam2dItemId = 0, string $lengthcutOptions = ''): array
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'shop_basket_unique' => $basketUnique,
                'shop_article_id' => $articleId
            ]);
            if (!empty($articleOptionsJson)) {
                $select->where([
                    'shop_basket_item_article_options' => $articleOptionsJson
                ]);
            }
            if (!empty($articleParam2dItemId)) {
                $select->where([
                    'shop_article_param2d_item_id' => $articleParam2dItemId
                ]);
            }
            if (!empty($lengthcutOptions)) {
                $select->where([
                    'shop_basket_item_lengthcut_options' => $lengthcutOptions
                ]);
            }
            /** @var ResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function existShopBasketItem(string $basketUnique, int $articleId, string $articleOptionsJson = '', int $articleParam2dItemId = 0, string $lengthcutDefJson = '')
    {
        return !empty($this->getShopBasketItem($basketUnique, $articleId, $articleOptionsJson, $articleParam2dItemId, $lengthcutDefJson));
    }

    /**
     * UNUSED, because saveOrUpdateShopBasketItem($basketUnique, $articleId, $articleAmount) in ShopBasketTablex
     * @param string $basketUnique
     * @param int $articleId
     * @param float $articleAmount
     * @param string $articleOptionsJson
     * @param int $articleParam2dItemId
     * @param string $lengthcutDefJson
     * @return int
     */
    public function saveNewShopBasketItem(string $basketUnique, int $articleId, float $articleAmount, string $articleOptionsJson = '[]', int $articleParam2dItemId = 0, string $lengthcutDefJson = '')
    {
        $insert = $this->sql->insert();
        try {
            $insert->values([
                'shop_basket_unique' => $basketUnique,
                'shop_article_id' => $articleId,
                'shop_article_amount' => $articleAmount,
                'shop_basket_item_article_options' => (empty($articleOptionsJson) ? '[]' : $articleOptionsJson),
                'shop_article_param2d_item_id' => (empty($articleParam2dItemId) ? null : $articleParam2dItemId),
                'shop_basket_item_lengthcut_options' => (empty($lengthcutDefJson) ? null : $lengthcutDefJson),
            ]);
            return $this->insertWith($insert);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * Update the amount.
     *
     * @param string $basketUnique
     * @param int $articleId
     * @param float $articleAmount
     * @param string $articleOptionsJson
     * @param int $articleParam2dItemId
     * @param string $lengthcutDefJson
     * @return int
     */
    public function updateShopBasketItem(string $basketUnique, int $articleId, float $articleAmount, string $articleOptionsJson = '[]', int $articleParam2dItemId = 0, string $lengthcutDefJson = ''): int
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'shop_article_amount' => $articleAmount
            ]);
            $update->where([
                'shop_basket_unique' => $basketUnique,
                'shop_article_id' => $articleId
            ]);
            if (!empty($articleOptionsJson) && $articleOptionsJson != '[]') {
                $update->where([
                    'shop_basket_item_article_options' => $articleOptionsJson
                ]);
            }
            if (!empty($articleParam2dItemId)) {
                $update->where([
                    'shop_article_param2d_item_id' => $articleParam2dItemId
                ]);
            }
            if (!empty($lengthcutDefJson)) {
                $update->where([
                    'shop_basket_item_lengthcut_options' => $lengthcutDefJson
                ]);
            }
            return $this->updateWith($update);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function belongShopBasketItemToBasket(string $basketUnique, int $basketItemId): bool
    {
        $select = $this->sql->select();
        try {
            $select->where(['shop_basket_item_id' => $basketItemId, 'shop_basket_unique' => $basketUnique]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return true;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return false;
    }
}
