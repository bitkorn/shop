<?php

namespace Bitkorn\Shop\Table\Basket;

use Bitkorn\Shop\Table\AbstractShopTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Expression;

/**
 * @author allapow
 */
class ShopBasketTable extends AbstractShopTable
{

    /**
     * @var string
     */
    protected $table = 'shop_basket';

    /**
     * @param string $basketUnique
     * @param string $userUuid
     * @param string $userUuidPos
     * @return int
     */
    public function saveNewShopBasket(string $basketUnique, ?string $userUuid, ?string $userUuidPos): int
    {
        if (empty($userUuid)) {
            $userUuid = null;
        }
        $insert = $this->sql->insert();
        try {
            $insert->values([
                'shop_basket_unique' => $basketUnique,
                'user_uuid' => $userUuid,
                'user_uuid_pos' => $userUuidPos,
                'shop_basket_time_create' => time()
            ]);
            return $this->insertWith($insert);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function existShopBasket($basketUnique)
    {
        $select = $this->sql->select();
        try {
            $select->where(['shop_basket_unique' => $basketUnique]);
            $resultset = $this->selectWith($select);
            if ($resultset->valid() && $resultset->count() > 0) {
                return true;
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return false;
    }

    /**
     * @param string $shopBasketUnique
     * @param string|null $userUuid
     * @return int
     */
    public function setUserUuid(string $shopBasketUnique, $userUuid = null): int
    {
        $update = $this->sql->update();
        try {
            $update->set(['user_uuid' => $userUuid]);
            $update->where(['shop_basket_unique' => $shopBasketUnique]);
            return $this->updateWith($update);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param string $shopBasketUnique
     * @param string $paymentNumber
     * @return int
     */
    public function setPaymentNumber(string $shopBasketUnique, string $paymentNumber): int
    {
        $update = $this->sql->update();
        try {
            $update->set(['payment_number' => $paymentNumber]);
            $update->where(['shop_basket_unique' => $shopBasketUnique]);
            return $this->updateWith($update);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param string $paymentNumber
     * @param string $shopbasketStatus
     * @return int
     */
    public function setShopBasketStatusByPaymentNumber(string $paymentNumber, string $shopbasketStatus): int
    {
        $update = $this->sql->update();
        try {
            $update->set(['shop_basket_status' => $shopbasketStatus]);
            $update->where(['payment_number' => $paymentNumber]);
            return $this->updateWith($update);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function setShippingProviderUniqueDescriptor(string $basketUnique, string $uniqueDescriptor)
    {
        $update = $this->sql->update();
        try {
            $update->set(['shipping_provider_unique_descriptor' => $uniqueDescriptor]);
            $update->where(['shop_basket_unique' => $basketUnique]);
            return $this->updateWith($update);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function deleteShippingProviderUniqueDescriptor(string $basketUnique): int
    {
        $update = $this->sql->update();
        try {
            $update->set(['shipping_provider_unique_descriptor' => '']);
            $update->where(['shop_basket_unique' => $basketUnique]);
            return $this->updateWith($update);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     *
     * @param string $shopBasketUnique
     * @param string $paymentMethod
     * @return int
     */
    public function setPaymentMethod($shopBasketUnique, $paymentMethod): int
    {
        $update = $this->sql->update();
        try {
            $update->set(['payment_method' => $paymentMethod]);
            $update->where(['shop_basket_unique' => $shopBasketUnique]);
            return $this->updateWith($update);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     *
     * @param string $basketUnique
     * @return array If $resultset->count() != 1 then an empty array. Else the table row.
     */
    public function getShopBasketByShopBasketUnique($basketUnique)
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'shop_basket_unique' => $basketUnique,
            ]);
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->toArray();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     *
     * @param string $paymentNumber
     * @return array An empty array if no shopBasket exist or it exists two with the same $paymentNumber. Else the shopBasket.
     */
    public function getShopBasketByPaymentNumber(string $paymentNumber): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['payment_number' => $paymentNumber]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     *
     * @param string $shopBasketUnique
     * @param array $values
     * @return int
     */
    public function updateShopBasketValues($shopBasketUnique, array $values): int
    {
        if(empty($values)) {
            return 0;
        }
        $update = $this->sql->update();
        try {
            $update->set($values);
            $update->where(['shop_basket_unique' => $shopBasketUnique]);
            return $this->updateWith($update);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @return array
     */
    public function getShopBasketsWithoutBasketEntity()
    {
        $select = new Select('view_baskets');
        try {
            $select->where->isNull('shop_basket_entity_id');
            $select->order('shop_basket_time_create DESC');
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function deleteShopBasketByBasketUnique(string $basketUnique)
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['shop_basket_unique' => $basketUnique]);
            return $this->deleteWith($delete);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param string $basketUnique
     * @param int $time
     * @return bool default = true
     */
    public function isShopBasketTooOld(string $basketUnique, int $time): bool
    {
        $select = $this->sql->select();
        try {
            $select->columns(['diff' => new Expression('shop_basket_time_create < ' . $time)]);
            $select->where([
                'shop_basket_unique' => $basketUnique,
            ]);
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return boolval($result->current()->getArrayCopy()['diff']);
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return true;
    }

}
