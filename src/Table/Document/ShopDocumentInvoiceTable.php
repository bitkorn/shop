<?php

namespace Bitkorn\Shop\Table\Document;

use Bitkorn\Shop\Table\AbstractShopTable;
use Laminas\Db\ResultSet\HydratingResultSet;

/**
 * Description of ShopDocumentInvoiceTable
 *
 * @author allapow
 */
class ShopDocumentInvoiceTable extends AbstractShopTable
{

    /**
     *
     * @var string
     */
    protected $table = 'shop_document_invoice';

    /**
     * @param int $shopDocumentInvoiceId
     * @return array
     */
    public function getShopDocumentInvoiceById(int $shopDocumentInvoiceId): array
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'shop_document_invoice_id' => $shopDocumentInvoiceId,
            ]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $basketUnique
     * @return array
     */
    public function getShopDocumentInvoiceByBasketUnique(string $basketUnique): array
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'shop_basket_unique' => $basketUnique,
            ]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function existShopDocumentInvoiceByBasketUnique($basketUnique)
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'shop_basket_unique' => $basketUnique,
            ]);
            $result = $this->selectWith($select);

            if ($result->valid() && $result->count() == 1) {
                return true;
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return false;
    }

    public function getLastShopDocumentInvoice()
    {
        $select = $this->sql->select();
        try {
            $select->order('shop_document_invoice_id DESC')->limit(1);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     *
     * @param array $shopDocumentInvoice
     * @return int
     */
    public function saveShopDocumentInvoice(array $shopDocumentInvoice)
    {
        $insert = $this->sql->insert();
        try {
            $insert->values($shopDocumentInvoice);
            $stmt = $this->sql->prepareStatementForSqlObject($insert);
            $result = $stmt->execute(); // Laminas\Db\Adapter\Driver\Pdo\Result
            if ($result->valid() && ($result instanceof \Laminas\Db\Adapter\Driver\ResultInterface)) {
                return $this->getAdapter()->getDriver()->getConnection()->getLastGeneratedValue('public.shop_document_invoice_shop_document_invoice_id_seq');
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function updateShopDocumentInvoice(array $shopDocumentInvoice, $shopDocumentInvoiceId)
    {
        $update = $this->sql->update();
        try {
            $update->set($shopDocumentInvoice);
            $update->where(['shop_document_invoice_id' => $shopDocumentInvoiceId]);
            return $this->updateWith($update);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function deleteShopDocumentInvoice($documentId)
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['shop_document_invoice_id' => $documentId]);
            return $this->deleteWith($delete);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

}
