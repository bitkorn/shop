<?php

namespace Bitkorn\Shop\Table\Document;

use Bitkorn\Shop\Table\AbstractShopTable;
use Laminas\Db\ResultSet\HydratingResultSet;

/**
 *
 * @author allapow
 */
class ShopDocumentDeliveryTable extends AbstractShopTable
{

    /**
     *
     * @var string
     */
    protected $table = 'shop_document_delivery';

    /**
     * @param int $shopDocumentDeliveryId
     * @return array
     */
    public function getShopDocumentDeliveryById(int $shopDocumentDeliveryId): array
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'shop_document_delivery_id' => $shopDocumentDeliveryId,
            ]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getShopDocumentDeliveryByBasketUnique($basketUnique)
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'shop_basket_unique' => $basketUnique,
            ]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function existShopDocumentDeliveryByBasketUnique($basketUnique)
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'shop_basket_unique' => $basketUnique,
            ]);
            $result = $this->selectWith($select);

            if ($result->valid() && $result->count() >= 1) {
                return true;
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return false;
    }

    public function getLastShopDocumentDelivery()
    {
        $select = $this->sql->select();
        try {
            $select->order('shop_document_delivery_id DESC')->limit(1);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     *
     * @param array $shopDocumentDelivery
     * @return int
     */
    public function saveShopDocumentDelivery(array $shopDocumentDelivery)
    {
        $insert = $this->sql->insert();
        try {
            $insert->values($shopDocumentDelivery);
            $stmt = $this->sql->prepareStatementForSqlObject($insert);
            $result = $stmt->execute(); // Laminas\Db\Adapter\Driver\Pdo\Result
            if ($result->valid() && ($result instanceof \Laminas\Db\Adapter\Driver\ResultInterface)) {
                return $this->getAdapter()->getDriver()->getConnection()->getLastGeneratedValue('public.shop_document_delivery_shop_document_delivery_id_seq');
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function updateShopDocumentDelivery(array $shopDocumentDelivery, $shopDocumentDeliveryId)
    {
        $update = $this->sql->update();
        try {
            $update->set($shopDocumentDelivery);
            $update->where(['shop_document_delivery_id' => $shopDocumentDeliveryId]);
            return $this->updateWith($update);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function deleteShopDocumentDelivery($documentId)
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['shop_document_delivery_id' => $documentId]);
            return $this->deleteWith($delete);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

}
