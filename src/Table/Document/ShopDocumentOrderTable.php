<?php

namespace Bitkorn\Shop\Table\Document;

use Bitkorn\Shop\Table\AbstractShopTable;
use Laminas\Db\ResultSet\HydratingResultSet;

/**
 * Description of ShopDocumentOrderTable
 *
 * @author allapow
 */
class ShopDocumentOrderTable extends AbstractShopTable
{

    /**
     *
     * @var string
     */
    protected $table = 'shop_document_order';

    /**
     * @param int $shopDocumentOrderId
     * @return array
     */
    public function getShopDocumentOrderById(int $shopDocumentOrderId): array
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'shop_document_order_id' => $shopDocumentOrderId,
            ]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function existShopDocumentOrderForBasketUnique($basketUnique)
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'shop_basket_unique' => $basketUnique,
            ]);
            $result = $this->selectWith($select);

            if ($result->valid() && $result->count() == 1) {
                return true;
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return false;
    }

    /**
     * @param string $basketUnique
     * @return array
     */
    public function getShopDocumentOrderByBasketUnique(string $basketUnique): array
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'shop_basket_unique' => $basketUnique,
            ]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     *
     * @param string $basketUnique
     * @return string
     */
    public function getShopDocumentOrderNoByBasketUnique($basketUnique)
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'shop_basket_unique' => $basketUnique,
            ]);
            $result = $this->selectWith($select);

            if ($result->valid() && $result->count() == 1) {
                $current = $result->current();
                return $current['shop_document_order_number'];
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function getLastShopDocumentOrder()
    {
        $select = $this->sql->select();
        try {
            $select->order('shop_document_order_id DESC')->limit(1);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     *
     * @param array $shopDocumentOrder
     * @return int
     */
    public function saveShopDocumentOrder(array $shopDocumentOrder): int
    {
        $insert = $this->sql->insert();
        try {
            $insert->values($shopDocumentOrder);
            $stmt = $this->sql->prepareStatementForSqlObject($insert);
            $result = $stmt->execute();
            if ($result->valid()) {
                return $this->getAdapter()->getDriver()->getConnection()->getLastGeneratedValue('public.shop_document_order_shop_document_order_id_seq');
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function updateShopDocumentOrder(array $shopDocumentOrder, $shopDocumentOrderId)
    {
        $update = $this->sql->update();
        try {
            $update->set($shopDocumentOrder);
            $update->where(['shop_document_order_id' => $shopDocumentOrderId]);
            return $this->updateWith($update);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function deleteShopDocumentOrder($documentId)
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['shop_document_order_id' => $documentId]);
            return $this->deleteWith($delete);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

}
