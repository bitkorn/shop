<?php

namespace Bitkorn\Shop\Table\Article;

use Bitkorn\Shop\Table\AbstractShopTable;

/**
 *
 * @author allapow
 */
class ShopArticleGroupRelationTable extends AbstractShopTable
{

    /**
     *
     * @var string
     */
    protected $table = 'shop_article_group_relation';

    /**
     *
     * @param int $shopArticleGroupRelationId
     * @return array
     */
    public function getShopArticleGroupRelationById($shopArticleGroupRelationId)
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'shop_article_group_relation_id' => $shopArticleGroupRelationId,
            ]);
            $resultset = $this->selectWith($select);
            if ($resultset->valid() && $resultset->count() == 1) {
                return $resultset->current();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     *
     * @param int $shopArticleGroupRelationId
     * @param float $shopArticleGroupRelationAmount The new amount
     * @return int
     */
    public function updateShopArticleGroupRelationAmount($shopArticleGroupRelationId, $shopArticleGroupRelationAmount)
    {
        $shopArticleGroupRelation = $this->getShopArticleGroupRelationById($shopArticleGroupRelationId);
        if (empty($shopArticleGroupRelation)) {
            return -1;
        }
        $update = $this->sql->update();
        try {
            $update->set(['shop_article_group_relation_amount' => $shopArticleGroupRelationAmount]);
            $update->where(['shop_article_group_relation_id' => $shopArticleGroupRelationId]);
            return $this->updateWith($update);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     *
     * @param int $shopArticleGroupRelationId
     * @return int
     */
    public function deleteShopArticleGroupRelation($shopArticleGroupRelationId)
    {
        $delete = $this->sql->delete();
        try {
            $delete->where([
                'shop_article_group_relation_id' => $shopArticleGroupRelationId,
            ]);
            return $this->deleteWith($delete);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }
}
