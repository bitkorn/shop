<?php

namespace Bitkorn\Shop\Table\Article;

use Bitkorn\Shop\Table\AbstractShopTable;

/**
 *
 * @author allapow
 */
class ShopArticleCategoryRelationTable extends AbstractShopTable
{

    /**
     *
     * @var string
     */
    protected $table = 'shop_article_category_relation';

    /**
     *
     * @param int $shopArticleId
     * @return array ShopArticleCategory-IDs
     */
    public function getShopArticleCategoriesForShopArticle($shopArticleId)
    {
        $select = $this->sql->select();
        $returnArray = [];
        try {
            $select->where([
                'shop_article_id' => $shopArticleId,
            ]);
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                $resultArr = $result->toArray();
                foreach ($resultArr as $row) {
                    $returnArray[] = $row['shop_article_category_id'];
                }
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return $returnArray;
    }

    /**
     *
     * @param int $shopArticleId
     * @param int $shopArticleCategoryId
     * @return boolean
     */
    public function existShopArticleCategoryRelation($shopArticleId, $shopArticleCategoryId)
    {
        $select = $this->sql->select();
        try {
            $select->where(['shop_article_id' => $shopArticleId, 'shop_article_category_id' => $shopArticleCategoryId]);
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return true;
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return false;
    }

    public function saveShopArticleCategory($shopArticleId, $shopArticleCategoryId)
    {
        if ($this->existShopArticleCategoryRelation($shopArticleId, $shopArticleCategoryId)) {
            return 0;
        }
        $insert = $this->sql->insert();
        try {
            $insert->values(['shop_article_id' => $shopArticleId, 'shop_article_category_id' => $shopArticleCategoryId]);
            return $this->insertWith($insert);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param int $articleId
     * @return int
     */
    public function deleteForArticle(int $articleId): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['shop_article_id' => $articleId]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
    }
}
