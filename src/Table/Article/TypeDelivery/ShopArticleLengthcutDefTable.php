<?php

namespace Bitkorn\Shop\Table\Article\TypeDelivery;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class ShopArticleLengthcutDefTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'shop_article_lengthcut_def';

    /**
     * @param int $lengthcutDefId
     * @return array
     */
    public function getLengthcutDef(int $lengthcutDefId): array
    {
        $select = $this->sql->select();
        try {
            $select->join('quantityunit', 'quantityunit.quantityunit_uuid = shop_article_lengthcut_def.quantityunit_uuid', ['quantityunit_label'], Select::JOIN_LEFT);
            $select->where(['shop_article_lengthcut_def_id' => $lengthcutDefId]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @return array
     */
    public function getLengthcutDefs(): array
    {
        $select = $this->sql->select();
        $idAssoc = [];
        try {
            $select->join('quantityunit', 'quantityunit.quantityunit_uuid = shop_article_lengthcut_def.quantityunit_uuid', ['quantityunit_label'], Select::JOIN_LEFT);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return $idAssoc;
    }

    /**
     * @return array
     */
    public function getLengthcutDefIdAssoc(): array
    {
        $idAssoc = [];
        $resultArr = $this->getLengthcutDefs();
        foreach ($resultArr as $row) {
            $idAssoc[$row['shop_article_lengthcut_def_id']] = $row['shop_article_lengthcut_def_name'];
        }
        return $idAssoc;
    }

    /**
     * @param array $storage
     * @return int
     */
    public function insertLengthcutDef(array $storage): int
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values($storage);
            if ($this->insertWith($insert) > 0) {
                return intval($this->adapter->getDriver()->getConnection()->getLastGeneratedValue('public.shop_article_lengthcut_def_shop_article_lengthcut_def_id_seq'));
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param int $articleId
     * @return array
     */
    public function getLengthcutDefForArticleId(int $articleId): array
    {
        $select = $this->sql->select();
        try {
            $selectArticleId = new Select('shop_article');
            $selectArticleId->columns(['shop_article_lengthcut_def_id']);
            $selectArticleId->where(['shop_article_id' => $articleId]);

            $select->join('quantityunit', 'quantityunit.quantityunit_uuid = shop_article_lengthcut_def.quantityunit_uuid', ['quantityunit_label'], Select::JOIN_LEFT);
            $select->where(['shop_article_lengthcut_def_id' => $selectArticleId]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}
