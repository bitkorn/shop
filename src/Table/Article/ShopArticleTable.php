<?php

namespace Bitkorn\Shop\Table\Article;

use Bitkorn\Shop\Entity\Database\QueryParamsArticleSearch;
use Bitkorn\Shop\Table\AbstractShopTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;

/**
 *
 * @author allapow
 */
class ShopArticleTable extends AbstractShopTable
{

    /**
     *
     * @var string
     */
    protected $table = 'shop_article';

    /**
     *
     * @param int $categoryId
     * @return array
     */
    public function getShopArticleByCategory($categoryId = 1)
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'shop_article_category_id' => $categoryId,
            ]);
            $resultset = $this->selectWith($select);
            if ($resultset->valid()) {
                $resArr = $resultset->toArray();
                if (isset($resArr[0])) {
                    return $resArr;
                }
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     *
     * @param int $articleId
     * @param bool $onlyActive
     * @return array
     */
    public function getShopArticleById(int $articleId, $onlyActive = false)
    {
        $select = $this->sql->select();
        try {
            if ($onlyActive) {
                $select->where(['shop_article_active' => 1]);
            }
            $select->where(['shop_article_id' => $articleId]);
            $resultset = $this->selectWith($select);
            if ($resultset->valid() && $resultset->count() == 1) {
                return $resultset->toArray()[0];
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getShopArticleCompleteById(int $articleId, $onlyActive = false)
    {
        $select = new Select('view_articles');
        try {
            if ($onlyActive) {
                $select->where(['shop_article_active' => 1]);
            }
            $select->where(['shop_article_id' => $articleId]);
            $resultset = $this->selectWith($select);
            if ($resultset->valid() && $resultset->count() == 1) {
                return $resultset->toArray()[0];
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $articleSefurl
     * @param bool $onlyActive
     * @return array
     */
    public function getShopArticleBySefurl(string $articleSefurl, bool $onlyActive = false): array
    {
        $select = new Select('view_articles');
        $whereArray = [
            'shop_article_sefurl' => $articleSefurl
        ];
        if ($onlyActive) {
            $whereArray['shop_article_active'] = 1;
        }
        try {
            $select->where($whereArray);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $searchText
     * @param int $categoryId
     * @param int $classId
     * @param string $type
     * @param string $typeDelivery
     * @return array
     * @todo feddich machen! Es soll zwei (oder mehr) Funktionen (getShopArticlesByType & getShopArticles) aus ShopArticleTablex ersetzen.
     * @todo Auch ShopArticleTablex()->getShopArticlesActive() kann ersetzt werden.
     */
    public function searchShopArticlesWithParams(string $searchText = '', int $categoryId = 0, int $classId = 0, string $type = '', string $typeDelivery = ''): array
    {
        $select = new Select('view_articles');
        try {
            if (!empty($searchText)) {
                $searchText = '%' . $searchText . '%';
                $select
                    ->where->like('shop_article_sku', $searchText)
                    ->or->like('shop_article_name', $searchText)
                    ->or->like('shop_article_sefurl', $searchText)
                    ->or->like('shop_article_desc', $searchText);
            }
            if (!empty($categoryId)) {
                $selectInCategory = new Select('shop_article_category_relation');
                $selectInCategory->columns(['shop_article_id'])->where(['shop_article_category_id' => $categoryId]);
                $select->where->in('shop_article_id', $selectInCategory);
            }
            if (!empty($classId)) {
                $select->where(['shop_article_class_id' => $classId]);
            }
            if (!empty($type)) {
                $select->where(['shop_article_type' => $type]);
            }
            if (!empty($typeDelivery)) {
                $select->where(['shop_article_type_delivery' => $typeDelivery]);
            }
            $select->where(['shop_article_active' => 1]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     *
     * @param array $storage
     * @return int
     */
    public function saveShopArticle(array $storage)
    {
        $insert = $this->sql->insert();
        try {
            $insert->values($storage);
            $this->insertWith($insert);
            $contentId = $this->adapter->getDriver()->getConnection()->getLastGeneratedValue('public.shop_article_shop_article_id_seq');
            if (!empty($contentId)) {
                return $contentId;
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     *
     * @param int $articleId
     * @param array $storage
     * @return int
     */
    public function updateShopArticle($articleId, array $storage)
    {
        $update = $this->sql->update();
        try {
            $update->where(['shop_article_id' => $articleId]);
            $update->set($storage);
            return $this->updateWith($update);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     *
     * @param int $articleId
     * @return int 1=article uses stock; 0=article do not use stock; -1=error
     */
    public function isShopArticleStockUsing($articleId)
    {
        $select = $this->sql->select();
        $select->columns(['shop_article_stock_using']);
        $select->where([
            'shop_article_id' => $articleId,
        ]);
        $resultset = $this->selectWith($select);
        if ($resultset->valid() && $resultset->count() == 1) {
            $resArr = $resultset->toArray();
            return $resArr[0]['shop_article_stock_using'];
        }
        return -1;
    }

    public function getShopArticleStandardGroupableExcludeArticleId(int $rootArticleId)
    {
        $select = $this->sql->select();
        try {
            $select->where->notEqualTo('shop_article_id', $rootArticleId)
                ->AND->equalTo('shop_article_type', 'standard')
                ->AND->equalTo('shop_article_groupable', 1);
            /** @var HydratingResultSet $resultset */
            $resultset = $this->selectWith($select);
            if ($resultset->valid() && $resultset->count() > 0) {
                return $resultset->toArray();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param int $categoryId
     * @param int $classId
     * @return array
     */
    public function getShopArticlesActiveByCategoryIdAndClassId(int $categoryId = 0, int $classId = 0): array
    {
        $select = $this->sql->select();
        try {
            if (!empty($categoryId)) {
                $selectInCategory = new Select('shop_article_category_relation');
                $selectInCategory->columns(['shop_article_id'])->where(['shop_article_category_id' => $categoryId]);
                $select->where->in('shop_article_id', $selectInCategory);
            }
            if (!empty($classId)) {
                $select->where(['shop_article_class_id' => $classId]);
            }
            $select->where(['shop_article_active' => 1]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     *
     * @param string $sefurl
     * @param int $selfArticleId
     * @return boolean
     */
    public function existShopArticleSefurl($sefurl, $selfArticleId = 0)
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'shop_article_sefurl' => $sefurl,
            ]);
            $select->where->notEqualTo('shop_article_id', $selfArticleId);
            $resultset = $this->selectWith($select);
            if ($resultset->valid() && $resultset->count() > 0) {
                return true;
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return false;
    }
}
