<?php

namespace Bitkorn\Shop\Table\Article;

use Bitkorn\Shop\Table\AbstractShopTable;
use Laminas\Db\ResultSet\HydratingResultSet;

/**
 *
 * @author allapow
 */
class ShopArticleClassTable extends AbstractShopTable
{

    /**
     *
     * @var string
     */
    protected $table = 'shop_article_class';

    public function getShopArticleClassById($id)
    {
        $select = $this->sql->select();
        $select->where([
            'shop_article_class_id' => $id,
        ]);
        /** @var HydratingResultSet $result */
        $result = $this->selectWith($select);
        if ($result->valid() && $result->count() == 1) {
            return $result->current()->getArrayCopy();
        }
        return [];
    }

    public function saveShopArticleClass(array $data)
    {
        $insert = $this->sql->insert();
        try {
            $insert->values($data);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }

        return $this->executeInsert($insert);
    }

    public function getShopArticleClasssIdAssoc()
    {
        $select = $this->sql->select();
        $idAssoc = [];
        try {
            $select->order('shop_article_class_order_priority DESC');
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                $resultArr = $result->toArray();
                foreach ($resultArr as $res) {
                    $idAssoc[$res['shop_article_class_id']] = $res['shop_article_class_id'] . ' ' . $res['shop_article_class_name'];
                }
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return $idAssoc;
    }

}
