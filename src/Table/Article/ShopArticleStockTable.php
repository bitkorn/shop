<?php

namespace Bitkorn\Shop\Table\Article;

use Bitkorn\Shop\Table\AbstractShopTable;
use Laminas\Db\Adapter\Driver\Pdo\Result;
use Laminas\Db\Adapter\Driver\ResultInterface;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;

/**
 *
 * @author allapow
 */
class ShopArticleStockTable extends AbstractShopTable
{

    /**
     *
     * @var string
     */
    protected $table = 'shop_article_stock';

    /**
     *
     * @param int $shopArticleId
     * @param float $shopArticleAmount
     * @param string $articleOptions
     * @param string $shopArticleReasonType ENUM('basket', 'retail', 'production', 'purchase')
     * @param string $shopArticleReason
     * @return int
     */
    public function saveShopArticleStock($shopArticleId, $shopArticleAmount, $articleOptions, $articleParam2DItemid = null, $shopArticleReasonType = 'basket',
                                         $shopArticleReason = '', $unixtime = 0)
    {
        if (empty($unixtime)) {
            $unixtime = time();
        }
        $insert = $this->sql->insert();
        try {
            $insert->values([
                'shop_article_id' => $shopArticleId,
                'shop_article_options' => $articleOptions,
                'shop_article_param2d_item_id' => $articleParam2DItemid,
                'shop_article_stock_amount' => $shopArticleAmount,
                'shop_article_stock_time' => $unixtime,
                'shop_article_stock_reason_type' => $shopArticleReasonType,
                'shop_article_stock_reason' => $shopArticleReason,
            ]);
            $stmt = $this->sql->prepareStatementForSqlObject($insert);
            $result = $stmt->execute();
            if ($result->valid() && ($result instanceof ResultInterface)) {
                return $this->getAdapter()->getDriver()->getConnection()->getLastGeneratedValue('public.shop_article_stock_shop_article_stock_id_seq');
            }
            return -1;
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function saveShopArticleStockData($data): int
    {
        if (empty($data['shop_article_id']) || empty($data['shop_article_options']) || empty($data['shop_article_stock_amount']) || empty($data['shop_vendor_id']) || empty($data['shop_article_stock_reason_type'])) {
            return -1;
        }
        $insert = $this->sql->insert();
        try {
            $insert->values([
                'shop_article_id' => $data['shop_article_id'],
                'shop_article_options' => $data['shop_article_options'],
                'shop_article_param2d_item_id' => (!empty($data['shop_article_param2d_item_id']) ? $data['shop_article_param2d_item_id'] : null),
                'shop_article_stock_amount' => $data['shop_article_stock_amount'],
                'shop_article_stock_time' => time(),
                'shop_vendor_id' => $data['shop_vendor_id'],
                'shop_article_stock_batch' => (!empty($data['shop_article_stock_batch']) ? $data['shop_article_stock_batch'] : ''),
                'shop_article_stock_reason_type' => $data['shop_article_stock_reason_type'],
                'shop_article_stock_reason' => (!empty($data['shop_article_stock_reason']) ? $data['shop_article_stock_reason'] : ''),
            ]);
            $stmt = $this->sql->prepareStatementForSqlObject($insert);
            /** @var Result $result */
            $result = $stmt->execute();
            if ($result->valid() && $result->count() == 1) {
                return $this->getAdapter()->getDriver()->getConnection()->getLastGeneratedValue('public.shop_article_stock_shop_article_stock_id_seq');
            }
            return -1;
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param int $articleId
     * @param string $shopArticleOptionsJson ArticleOptions JSON
     * @param int $param2dItemId
     * @return float
     */
    public function getShopArticleStockAmount(int $articleId, string $shopArticleOptionsJson = '', int $param2dItemId = 0): float
    {
        if (empty($articleId)) {
            return -1;
        }
        $select = $this->sql->select();
        try {
            $select->columns([
                'shop_article_stock_amount_sum' => new Expression('SUM(shop_article_stock_amount)')
            ]);
            if (!empty($shopArticleOptionsJson)) {
                $select->where(['shop_article_id' => $articleId, 'shop_article_options' => $shopArticleOptionsJson]);
            }
            if ($param2dItemId > 0) {
                $select->where(['shop_article_param2d_item_id' => $param2dItemId]);
            }
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                $current = $result->current()->getArrayCopy();
                return floatval($current['shop_article_stock_amount_sum']);
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return 0;
    }

    /**
     * Summarized stock for one article.
     *
     * @param int $articleId
     * @param string $articleOptionsJson
     * @param int $param2dItemId
     * @return array ['shop_article_stock_amount_sum' => 0, 'shop_article_active' => 1]
     */
    public function searchShopArticleStock(int $articleId, string $articleOptionsJson = '', int $param2dItemId = 0): array
    {
        if (empty($articleId)) {
            return [];
        }
        $select = $this->sql->select();
        try {
            $select->columns([
                'shop_article_stock_amount_sum' => new Expression('SUM(shop_article_stock_amount)')
            ]);
            $select->join('shop_article', 'shop_article.shop_article_id = shop_article_stock.shop_article_id', ['shop_article_active'], Select::JOIN_LEFT);
            $select->where(['shop_article_stock.shop_article_id' => $articleId]);
            if (!empty($articleOptionsJson)) {
                $select->where(['shop_article_options' => $articleOptionsJson]);
            }
            if ($param2dItemId > 0) {
                $select->where(['shop_article_param2d_item_id' => $param2dItemId]);
            }
            $select->group(['shop_article_stock.shop_article_id', 'shop_article.shop_article_active']);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param int $articleId
     * @param string $articleOptionsJson
     * @param int $param2dItemId
     * @return array All stock entries for one article
     */
    public function searchShopArticleStockList(int $articleId, string $articleOptionsJson = '', int $param2dItemId = 0): array
    {
        if (empty($articleId)) {
            return [];
        }
        $select = $this->sql->select();
        try {
            $select->join('shop_article', 'shop_article.shop_article_id = shop_article_stock.shop_article_id',
                ['shop_article_sku', 'shop_article_name', 'shop_article_desc'], Select::JOIN_LEFT);
            $select->where(['shop_article_stock.shop_article_id' => $articleId]);
            if (!empty($articleOptionsJson)) {
                $select->where->AND->like('shop_article_options', '%' . $articleOptionsJson . '%');
            }
            if ($param2dItemId > 0) {
                $select->where(['shop_article_param2d_item_id' => $param2dItemId]);
            }
            $select->order('shop_article_stock_time DESC');
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

}
