<?php

namespace Bitkorn\Shop\Table\Article\Size;

use Bitkorn\Shop\Table\AbstractShopTable;
use Laminas\Db\ResultSet\HydratingResultSet;

/**
 *
 * @author allapow
 */
class ShopArticleSizeGroupTable extends AbstractShopTable
{

    /**
     *
     * @var string
     */
    protected $table = 'shop_article_size_group';

    public function getShopArticleSizeGroups()
    {
        $select = $this->sql->select();
        $result = $this->selectWith($select);

        if ($result->valid() && $result->count() > 0) {
            return $result->toArray();
        }
        return [];
    }

    public function getShopArticleSizeGroupById($id)
    {
        $select = $this->sql->select();
        $select->where([
            'shop_article_size_group_id' => $id,
        ]);
        /** @var HydratingResultSet $result */
        $result = $this->selectWith($select);
        if ($result->valid() && $result->count() == 1) {
            return $result->current()->getArrayCopy();
        }
        return [];
    }

    public function saveShopArticleSizeGroup(array $data)
    {
        $insert = $this->sql->insert();
        try {
            $insert->values($data);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }

        return $this->executeInsert($insert);
    }

    public function updataShopArticleSizeGroup($articleSizeGroupId, array $data)
    {
        $update = $this->sql->update();
        try {
            $update->set($data);
            $update->where(['shop_article_size_group_id' => $articleSizeGroupId]);
            return $this->executeUpdate($update);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function getShopArticleSizeGroupIdAssoc()
    {
        $select = $this->sql->select();
        $idAssoc = [];
        try {
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                $resultArr = $result->toArray();
                foreach ($resultArr as $res) {
                    $idAssoc[$res['shop_article_size_group_id']] = $res['shop_article_size_group_name'];
                }
            }
        } catch (\Laminas\Db\Adapter\Exception\RuntimeException $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return $idAssoc;
    }

}
