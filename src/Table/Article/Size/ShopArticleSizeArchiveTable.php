<?php

namespace Bitkorn\Shop\Table\Article\Size;

use Bitkorn\Shop\Table\AbstractShopTable;
use Laminas\Db\Sql\Expression;

/**
 *
 * @author allapow
 */
class ShopArticleSizeArchiveTable extends AbstractShopTable
{

    /**
     *
     * @var string
     */
    protected $table = 'shop_article_size_archive';

    public function saveShopArticleSizeArchive(array $data)
    {
        $insert = $this->sql->insert();
        try {
            $insert->values($data);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }

        return $this->executeInsert($insert);
    }

    /**
     *
     * @param int $timestampFrom
     * @param int $timestampTo
     * @return array
     */
    public function getShopArticleSizeArchiveCountFromToGrouped(int $timestampFrom, int $timestampTo): array
    {
        $select = $this->sql->select();

        try {
            $select->columns([
//                'shop_article_size_archive_ip',
//                'shop_article_size_archive_time',
                'archive_time_sql' => new Expression('to_char(shop_article_size_archive_time, \'YYYY-MM-DD\')'),
//                'shop_article_size_group_id'
            ]);
            $select->where->greaterThanOrEqualTo('shop_article_size_archive_time', $timestampFrom)
                ->and->lessThanOrEqualTo('shop_article_size_archive_time', $timestampTo);
            $select->group(['shop_article_size_archive_uniquehash', 'shop_article_size_archive_time']); // , 'shop_article_size_archive_ip', 'shop_article_size_group_id'
            $select->order('shop_article_size_archive_time ASC');
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

}
