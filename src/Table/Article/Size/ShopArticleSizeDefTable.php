<?php

namespace Bitkorn\Shop\Table\Article\Size;

use Bitkorn\Shop\Table\AbstractShopTable;
use Laminas\Db\ResultSet\HydratingResultSet;

/**
 *
 * @author allapow
 */
class ShopArticleSizeDefTable extends AbstractShopTable
{

    /**
     *
     * @var string
     */
    protected $table = 'shop_article_size_def';

    public function countShopArticleSizeDef()
    {
        $select = $this->sql->select();
        $select->columns([
            'countC' => new \Laminas\Db\Sql\Expression("COUNT('shop_article_size_def_id')") //  as countC nicht mit Postgre kompatible
        ]);
        $results = $this->selectWith($select);
        $resultArr = $results->toArray();
        return $resultArr[0]['countC'];
    }

    public function getShopArticleSizeDefById($id)
    {
        $select = $this->sql->select();
        $select->where([
            'shop_article_size_def_id' => $id,
        ]);
        /** @var HydratingResultSet $result */
        $result = $this->selectWith($select);
        if ($result->valid() && $result->count() == 1) {
            return $result->current()->getArrayCopy();
        }
        return [];
    }

    public function saveShopArticleSizeDef(array $data)
    {
        $insert = $this->sql->insert();
        try {
            $insert->values($data);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }

        return $this->executeInsert($insert);
    }

    public function updateShopArticleSizeDef(array $data)
    {
        $update = $this->sql->update();
        $update->set($data);
        $update->where([
            'shop_article_size_def_id' => $data['shop_article_size_def_id'],
        ]);
        return $this->updateWith($update);
    }

    public function getShopArticleSizeDefIdAssoc($short = false)
    {
        $select = $this->sql->select();
        try {
            $select->order(['shop_article_size_group_id ASC', 'shop_article_size_def_priority DESC']);
            $result = $this->selectWith($select);
            if ($result->count() > 0) {
                $resultArr = $result->toArray();
                $idAssoc = [];
                foreach ($resultArr as $res) {
                    if (!$short) {
                        $idAssoc[$res['shop_article_size_def_id']] = $res['shop_article_size_def_key'] . ' - ' . $res['shop_article_size_def_name'];
                    } else {
                        $idAssoc[$res['shop_article_size_def_id']] = $res['shop_article_size_def_key'];
                    }
                }
                return $idAssoc;
            }
        } catch (\Laminas\Db\Adapter\Exception\RuntimeException $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

}
