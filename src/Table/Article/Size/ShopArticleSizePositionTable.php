<?php

namespace Bitkorn\Shop\Table\Article\Size;

use Bitkorn\Shop\Table\AbstractShopTable;
use Laminas\Db\Adapter\Exception\RuntimeException;

/**
 *
 * @author allapow
 */
class ShopArticleSizePositionTable extends AbstractShopTable
{

    /**
     *
     * @var string
     */
    protected $table = 'shop_article_size_position';

    /**
     *
     * @param int $articleSizeGroupId
     * @return array [shop_article_size_position_id => [name => '', desc => '']]
     */
    public function getShopArticleSizePositionIdAssocByArticleSizeGroupId(int $articleSizeGroupId)
    {
        if (empty($articleSizeGroupId)) {
            return [];
        }
        $idAssoc = [];
        $select = $this->sql->select();
        try {
            $select->where(['shop_article_size_group_id' => $articleSizeGroupId]);
            $select->order('shop_article_size_position_order_priority DESC');
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                $resultArr = $result->toArray();
                foreach ($resultArr as $res) {
                    $idAssoc[$res['shop_article_size_position_id']]['name'] = $res['shop_article_size_position_name'];
                    $idAssoc[$res['shop_article_size_position_id']]['desc'] = $res['shop_article_size_position_desc'];
                }
            }
        } catch (RuntimeException $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return $idAssoc;
    }

}
