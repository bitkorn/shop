<?php

namespace Bitkorn\Shop\Table\Article\Size;

use Bitkorn\Shop\Table\AbstractShopTable;
use Laminas\Db\ResultSet\HydratingResultSet;

/**
 *
 * @author allapow
 */
class ShopArticleSizeItemTable extends AbstractShopTable
{

    /**
     *
     * @var string
     */
    protected $table = 'shop_article_size_item';

    public function countShopArticleSizeItem()
    {
        $select = $this->sql->select();
        $select->columns([
            'countC' => new \Laminas\Db\Sql\Expression("COUNT('shop_article_size_item_id')") //  as countC nicht mit Postgre kompatible
        ]);
        $results = $this->selectWith($select);
        $resultArr = $results->toArray();
        return $resultArr[0]['countC'];
    }

    public function getShopArticleSizeItemById($id)
    {
        $select = $this->sql->select();
        $select->where([
            'shop_article_size_item_id' => $id,
        ]);
        /** @var HydratingResultSet $result */
        $result = $this->selectWith($select);
        if ($result->valid() && $result->count() == 1) {
            return $result->current()->getArrayCopy();
        }
        return [];
    }

    public function saveShopArticleSizeItem(array $data)
    {
        $insert = $this->sql->insert();
        try {
            $insert->values($data);
        } catch (\Exception $ex) {
            $this->logger->err($ex->getMessage());
            $this->logger->err('Code: ' . $ex->getCode());
            $this->logger->err('File: ' . $ex->getFile());
            $this->logger->err('Line: ' . $ex->getLine());
        }

        return $this->executeInsert($insert);
    }

    /**
     *
     * @param int $shopArticleSizeItemId
     * @param string $type
     * @param float $value
     * @return int
     */
    public function updateShopArticleSizeItem(int $shopArticleSizeItemId, string $type, float $value): int
    {
        if (empty($shopArticleSizeItemId) || empty($type) || ($type != 'from' && $type != 'to')) {
            return -1;
        }
        $updateColumnName = 'shop_article_size_item_' . $type;
        $update = $this->sql->update();
        try {
            $update->set([
                $updateColumnName => (float)$value
            ]);
            $update->where([
                'shop_article_size_item_id' => $shopArticleSizeItemId,
            ]);
            return $this->updateWith($update);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function getShopArticleSizeItemIdAssoc()
    {
        $select = $this->sql->select();
        try {
            $result = $this->selectWith($select);
        } catch (\Laminas\Db\Adapter\Exception\RuntimeException $ex) {
            $this->logger->err($ex->getMessage());
            $this->logger->err('Code: ' . $ex->getCode());
            $this->logger->err('File: ' . $ex->getFile());
            $this->logger->err('Line: ' . $ex->getLine());
        }
        if ($result->count() > 0) {
            $resultArr = $result->toArray();
            $idAssoc = [];
            foreach ($resultArr as $res) {
                $idAssoc[$res['shop_article_size_item_id']] = $res['shop_article_size_item_name'];
            }
            return $idAssoc;
        }
        return [];
    }

}
