<?php

namespace Bitkorn\Shop\Table\Article\Option;

use Bitkorn\Shop\Table\AbstractShopTable;

/**
 *
 * @author allapow
 */
class ShopArticleOptionItemArticleImageTable extends AbstractShopTable
{

    /**
     *
     * @var string
     */
    protected $table = 'shop_article_option_item_article_image_relation';

//    public function countShopArticleSizeDef()
//    {
//        $select = $this->sql->select();
//        $select->columns(array(
//            'countC' => new \Laminas\Db\Sql\Expression("COUNT('shop_article_size_def_id')") //  as countC nicht mit Postgre kompatible
//        ));
//        $results = $this->selectWith($select);
//        $resultArr = $results->toArray();
//        return $resultArr[0]['countC'];
//    }

    /**
     *
     * @param int $optionItemId
     * @param int $articleId
     * @return array
     */
    public function getShopArticleOptionItemArticleImageRelations($optionItemId, $articleId)
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'shop_article_option_item_id' => $optionItemId,
                'shop_article_id' => $articleId
            ]);
            $select->order(['shop_article_option_item_article_image_relation_priority DESC']);
            $result = $this->selectWith($select);

            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function addShopArticleOptionItemArticleImageRelation($optionItemId, $articleId, $bkImagesImageId)
    {
        $insert = $this->sql->insert();
        try {
            $insert->values([
                'shop_article_option_item_id' => $optionItemId,
                'shop_article_id' => $articleId,
                'bk_images_image_id' => $bkImagesImageId,
            ]);
            return $this->executeInsert($insert);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function deleteShopArticleOptionItemArticleImageRelation($optionItemId)
    {
        $delete = $this->sql->delete();
        try {
            $delete->where([
                'shop_article_option_item_article_image_relation_id' => $optionItemId,
            ]);
            return $this->deleteWith($delete);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function updateShopArticleOptionItemArticleImageRelationPriority($optionItemId, $optionItemPriority)
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'shop_article_option_item_article_image_relation_priority' => $optionItemPriority,
            ]);
            $update->where(['shop_article_option_item_article_image_relation_id' => $optionItemId]);
            return $this->updateWith($update);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

//    public function getShopArticleItemArticleImageIdAssoc()
//    {
//        $select = $this->sql->select();
//        try {
//            $select->order(['shop_article_size_group_id ASC', 'shop_article_size_def_priority DESC']);
//            $result = $this->selectWith($select);
//            if ($result->count() > 0) {
//                $resultArr = $result->toArray();
//                $idAssoc = [];
//                foreach ($resultArr as $res) {
//                    if (!$short) {
//                        $idAssoc[$res['shop_article_size_def_id']] = $res['shop_article_size_def_key'] . ' - ' . $res['shop_article_size_def_name'];
//                    } else {
//                        $idAssoc[$res['shop_article_size_def_id']] = $res['shop_article_size_def_key'];
//                    }
//                }
//                return $idAssoc;
//            }
//        } catch (\Laminas\Db\Adapter\Exception\RuntimeException $ex) {
//            $this->log($ex, __CLASS__, __FUNCTION__);
//        }
//        return [];
//    }
}
