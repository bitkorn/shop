<?php

namespace Bitkorn\Shop\Table\Article\Option;

use Bitkorn\Shop\Table\AbstractShopTable;
use Laminas\Db\ResultSet\HydratingResultSet;

/**
 *
 * @author allapow
 */
class ShopArticleOptionDefTable extends AbstractShopTable
{

    /**
     *
     * @var string
     */
    protected $table = 'shop_article_option_def';

//    public function countShopArticleSizeDef()
//    {
//        $select = $this->sql->select();
//        $select->columns(array(
//            'countC' => new \Laminas\Db\Sql\Expression("COUNT('shop_article_size_def_id')") //  as countC nicht mit Postgre kompatible
//        ));
//        $results = $this->selectWith($select);
//        $resultArr = $results->toArray();
//        return $resultArr[0]['countC'];
//    }

    public function getShopArticleOptionDefs()
    {
        $select = $this->sql->select();
        try {
            $select->order('shop_article_option_def_priority DESC');
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param int $id
     * @return array
     */
    public function getShopArticleOptionDefById(int $id): array
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'shop_article_option_def_id' => $id,
            ]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getShopArticleOptionDefPriorityById($id)
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'shop_article_option_def_id' => $id,
            ]);
            $result = $this->selectWith($select);

            if ($result->valid() && $result->count() == 1) {
                $current = $result->current()->getArrayCopy();
                return $current['shop_article_option_def_priority'];
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return 0;
    }
}
