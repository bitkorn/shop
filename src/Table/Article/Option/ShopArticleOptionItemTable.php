<?php

namespace Bitkorn\Shop\Table\Article\Option;

use Bitkorn\Shop\Table\AbstractShopTable;

/**
 *
 * @author allapow
 */
class ShopArticleOptionItemTable extends AbstractShopTable
{

    /**
     *
     * @var string
     */
    protected $table = 'shop_article_option_item';

    /**
     * @param int $defId
     * @return array
     */
    public function getShopArticleOptionItemsByDefId(int $defId): array
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'shop_article_option_def_id' => $defId,
            ]);
            $select->order('shop_article_option_item_priority DESC');
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getShopArticleOptionItemsByIds($optionIds)
    {
        $select = $this->sql->select();
        try {
            $select->where->in('shop_basket_item_id', $optionIds);
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getShopArticleOptionItemsIdAssoc($optionDefId)
    {
        $select = $this->sql->select();
        $idAssoc = [];
        try {
            $select->where(['shop_article_option_def_id' => $optionDefId]);
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                $resultArr = $result->toArray();
                foreach ($resultArr as $row) {
                    $idAssoc[$row['shop_article_option_item_id']] = $row;
                }
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return $idAssoc;
    }

    public function getShopArticleOptionItemsAllIdAssoc()
    {
        $select = $this->sql->select();
        $idAssoc = [];
        try {
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                $resultArr = $result->toArray();
                foreach ($resultArr as $row) {
                    $idAssoc[$row['shop_article_option_item_id']] = $row;
                }
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return $idAssoc;
    }

    /**
     *
     * @return array id = shop_article_option_def_id; value = $row
     */
    public function getShopArticleOptionItemsAllOptionDefIdAssoc()
    {
        $select = $this->sql->select();
        $idAssoc = [];
        try {
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                $resultArr = $result->toArray();
                foreach ($resultArr as $row) {
                    if (!isset($idAssoc[$row['shop_article_option_def_id']])) {
                        $idAssoc[$row['shop_article_option_def_id']] = [];
                    }
                    $idAssoc[$row['shop_article_option_def_id']][] = $row;
                }
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return $idAssoc;
    }

}
