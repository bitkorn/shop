<?php

namespace Bitkorn\Shop\Table\Article\Option;

use Bitkorn\Shop\Table\AbstractShopTable;
use Laminas\Db\Adapter\Exception\RuntimeException;

/**
 *
 * @author allapow
 */
class ShopArticleOptionItemArticlePricediffTable extends AbstractShopTable
{

    /**
     *
     * @var string
     */
    protected $table = 'shop_article_option_item_article_pricediff';

    /**
     *
     * @param int $articleId
     * @param int $optionItemId
     * @param float $pricediffValue
     * @return int
     */
    public function updateShopArticleOptionItemArticlePricediff($articleId, $optionItemId, $pricediffValue)
    {
        if (!$this->existShopArticleOptionItemArticlePricediff($articleId, $optionItemId)) {
            return $this->insertShopArticleOptionItemArticlePricediff($articleId, $optionItemId, $pricediffValue);
        }
        $update = $this->sql->update();
        try {
            $update->set(['shop_article_option_item_article_pricediff_value' => $pricediffValue]);
            $update->where(['shop_article_option_item_id' => $optionItemId, 'shop_article_id' => $articleId]);
            return $this->updateWith($update);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     *
     * @param int $articleId
     * @param int $optionItemId
     * @return int
     */
    public function existShopArticleOptionItemArticlePricediff($articleId, $optionItemId)
    {
        $select = $this->sql->select();
        try {
            $select->columns(['count_pricediff' => new \Laminas\Db\Sql\Predicate\Expression('COUNT(shop_article_option_item_article_pricediff_id)')]);
            $select->where([
                'shop_article_option_item_id' => $optionItemId,
                'shop_article_id' => $articleId,
            ]);
            $resultset = $this->selectWith($select);
            if ($resultset->valid() && $resultset->count() == 1) {
                $current = $resultset->current();
                return $current['count_pricediff'];
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     *
     * @param int $articleId
     * @param int $optionItemId
     * @return int
     */
    public function getShopArticleOptionItemArticlePricediff($articleId, $optionItemId)
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'shop_article_option_item_id' => $optionItemId,
                'shop_article_id' => $articleId,
            ]);
            $resultset = $this->selectWith($select);
            if ($resultset->valid() && $resultset->count() == 1) {
                $current = $resultset->current();
                return $current['shop_article_option_item_article_pricediff_value'];
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param int $articleId
     * @param int $optionItemId
     * @param float $pricediffValue
     * @return int
     */
    public function insertShopArticleOptionItemArticlePricediff(int $articleId, int $optionItemId, float $pricediffValue): int
    {
        $insert = $this->sql->insert();
        try {
            $insert->values([
                'shop_article_option_item_id' => $optionItemId,
                'shop_article_id' => $articleId,
                'shop_article_option_item_article_pricediff_value' => $pricediffValue
            ]);
            return $this->insertWith($insert);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     *
     * @param array $optionIds
     * @param int $shopArticleId
     * @return int
     */
    public function getShopArticleOptionItemsSummPricediff(array $optionIds, $shopArticleId)
    {
        if (empty($optionIds)) {
            return 0;
        }
        $select = $this->sql->select();
        try {
            $select->columns(['sum_pricediff' => new \Laminas\Db\Sql\Expression('SUM(shop_article_option_item_article_pricediff_value)')]);
            $select->where->in('shop_article_option_item_id', $optionIds)->and->equalTo('shop_article_id', $shopArticleId);
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                $current = $result->current();
                return $current['sum_pricediff'];
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return 0;
    }

    /**
     *
     * @param array $optionItemIds
     * @param int $shopArticleId
     * @return array Key = articleId_optionItemId; value = shop_article_option_item_article_pricediff_value
     */
    public function getShopArticleOptionItemArticlePricediffsIdsAssoc(array $optionItemIds, int $shopArticleId): array
    {
        $select = $this->sql->select();
        $idAssoc = [];
        try {
            $select->where->in('shop_article_option_item_id', $optionItemIds)->AND->equalTo('shop_article_id', $shopArticleId);
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                $resultArr = $result->toArray();
                foreach ($resultArr as $row) {
                    $idAssoc[$shopArticleId . '_' . $row['shop_article_option_item_id']] = $row['shop_article_option_item_article_pricediff_value'];
                }
            }
        } catch (RuntimeException $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return $idAssoc;
    }

}
