<?php

namespace Bitkorn\Shop\Table\Article\Option;

use Bitkorn\Shop\Table\AbstractShopTable;

/**
 * Description of ShopArticleOptionArticleRelationTable
 *
 * @author allapow
 */
class ShopArticleOptionArticleRelationTable extends AbstractShopTable
{

    /**
     *
     * @var string
     */
    protected $table = 'shop_article_option_article_relation';

    /**
     *
     * @param int $articleId
     * @return array
     */
    public function getShopArticleOptionArticleRelations($articleId)
    {
        $select = $this->sql->select();
        $returnArr = [];
        try {
            $select->where(['shop_article_id' => $articleId]);
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                $resultArr = $result->toArray();
                foreach ($resultArr as $row) {
                    $returnArr[] = $row['shop_article_option_def_id'];
                }
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return $returnArr;
    }

    /**
     *
     * @param int $optionDefId
     * @param int $articleId
     * @return boolean
     */
    public function existShopArticleOptionArticleRelation($optionDefId, $articleId)
    {
        $select = $this->sql->select();
        try {
            $select->where(['shop_article_option_def_id' => $optionDefId, 'shop_article_id' => $articleId]);
            $resultset = $this->selectWith($select);
            if ($resultset->valid() && $resultset->count() > 0) {
                return true;
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return false;
    }

    /**
     *
     * @param int $optionDefId
     * @param int $articleId
     * @return int
     */
    public function addShopArticleOptionArticleRelation($optionDefId, $articleId)
    {
        if ($this->existShopArticleOptionArticleRelation($optionDefId, $articleId)) {
            return 0;
        }
        $insert = $this->sql->insert();
        try {
            $insert->values(['shop_article_option_def_id' => $optionDefId, 'shop_article_id' => $articleId]);
            $this->insertWith($insert);
            $contentId = $this->adapter->getDriver()->getConnection()->getLastGeneratedValue('public.shop_article_option_article_r_shop_article_option_article_r_seq');
            if (empty($contentId)) {
                return -1;
            }
            return $contentId;
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     *
     * @param int $optionDefId
     * @param int $articleId
     * @return int
     */
    public function deleteShopArticleOptionArticleRelation($optionDefId, $articleId)
    {
        if (!$this->existShopArticleOptionArticleRelation($optionDefId, $articleId)) {
            return 0;
        }
        $delete = $this->sql->delete();
        try {
            $delete->where(['shop_article_option_def_id' => $optionDefId, 'shop_article_id' => $articleId]);
            return $this->deleteWith($delete);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return 0;
    }

    /**
     *
     * @return array id = shop_article_id; value = shop_article_option_def_id[]
     */
    public function getShopArticleOptionArticleRelationsIdAssoc()
    {
        $select = $this->sql->select();
        $idAssoc = [];
        try {
            $select->join('shop_article_option_def',
                'shop_article_option_def.shop_article_option_def_id = shop_article_option_article_relation.shop_article_option_def_id',
                'shop_article_option_def_priority', \Laminas\Db\Sql\Select::JOIN_LEFT);
            $select->order('shop_article_option_def.shop_article_option_def_priority DESC');
            $result = $this->selectWith($select);
            if ($result->count() > 0) {
                $resultArr = $result->toArray();
                foreach ($resultArr as $res) {
                    if (!isset($idAssoc[$res['shop_article_id']])) {
                        $idAssoc[$res['shop_article_id']] = [];
                    }
                    $idAssoc[$res['shop_article_id']][] = $res['shop_article_option_def_id'];
                }
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return $idAssoc;
    }

}
