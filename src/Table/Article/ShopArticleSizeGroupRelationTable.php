<?php

namespace Bitkorn\Shop\Table\Article;

use Bitkorn\Shop\Table\AbstractShopTable;

/**
 *
 * @author allapow
 */
class ShopArticleSizeGroupRelationTable extends AbstractShopTable
{

    /**
     *
     * @var string
     */
    protected $table = 'shop_article_sizegroup_ralation';

    /**
     *
     * @param int $shopArticleSizeGroupRelationId
     * @return array
     */
    public function getShopArticleSizeGroupRelationById($shopArticleSizeGroupRelationId)
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'shop_article_sizegroup_ralation_id' => $shopArticleSizeGroupRelationId,
            ]);
            $resultset = $this->selectWith($select);
            if ($resultset->valid() && $resultset->count() == 1) {
                return $resultset->current();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     *
     * @param int $shopArticleSizeGroupRelationId
     * @param float $shopArticleSizeGroupRelationAmount The new amount
     * @return int
     */
    public function updateShopArticleSizeGroupRelationAmount($shopArticleSizeGroupRelationId, $shopArticleSizeGroupRelationAmount)
    {
        $shopArticleGroupRelation = $this->getShopArticleSizeGroupRelationById($shopArticleSizeGroupRelationId);
        if (empty($shopArticleGroupRelation)) {
            return -1;
        }
        $update = $this->sql->update();
        try {
            $update->set(['shop_article_sizegroup_ralation_amount' => $shopArticleSizeGroupRelationAmount]);
            $update->where(['shop_article_sizegroup_ralation_id' => $shopArticleSizeGroupRelationId]);
            return $this->updateWith($update);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     *
     * @param int $shopArticleSizeGroupRelationId
     * @return int
     */
    public function deleteShopArticleSizeGroupRelation($shopArticleSizeGroupRelationId)
    {
        $delete = $this->sql->delete();
        try {
            $delete->where([
                'shop_article_sizegroup_ralation_id' => $shopArticleSizeGroupRelationId,
            ]);
            return $this->deleteWith($delete);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }
}
