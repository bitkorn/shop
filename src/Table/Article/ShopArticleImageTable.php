<?php

namespace Bitkorn\Shop\Table\Article;

use Bitkorn\Shop\Table\AbstractShopTable;
use Laminas\Db\ResultSet\HydratingResultSet;

/**
 *
 * @author allapow
 */
class ShopArticleImageTable extends AbstractShopTable
{

    /**
     *
     * @var string
     */
    protected $table = 'shop_article_image';

    public function countShopArticleImage($shopArticleId)
    {
        $select = $this->sql->select();
        $select->where(['shop_article_id' => $shopArticleId]);
        $select->columns([
            'countImages' => new \Laminas\Db\Sql\Expression("COUNT('shop_article_image_id')") //  as countC nicht mit Postgre kompatible
        ]);
        $results = $this->selectWith($select);
        $resultArr = $results->toArray();
        return $resultArr[0]['countImages'];
    }

    /**
     * @param int $id
     * @return array
     */
    public function getShopArticleImageById(int $id): array
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'shop_article_image_id' => $id,
            ]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getShopArticleImagesByArticleId($articleId)
    {
        $select = $this->sql->select();
        $select->where([
            'shop_article_id' => $articleId,
        ]);
        $result = $this->selectWith($select);

        if ($result->valid() && $result->count() > 0) {
            return $result->toArray();
        }
        return [];
    }

    public function getShopArticleImageXByArticleId($articleId)
    {
        $select = $this->sql->select();
        try {
            $select->join('bk_images_image', 'bk_images_image.bk_images_image_id=shop_article_image.bk_images_image_id', \Laminas\Db\Sql\Select::SQL_STAR,
                \Laminas\Db\Sql\Select::JOIN_LEFT);
            $select->where([
                'shop_article_id' => $articleId,
            ]);
            $select->order('bk_images_image.bk_images_image_priority DESC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     *
     * @param int $shopArticleId
     * @param int $imagesImageId
     * @return bool
     */
    public function existImagesImageForArticle($shopArticleId, $imagesImageId): bool
    {
        $select = $this->sql->select();
        try {
            $select->columns(['count_imageid' => new \Laminas\Db\Sql\Expression('COUNT(shop_article_image_id)')]);
            $select->where([
                'shop_article_id' => $shopArticleId,
                'bk_images_image_id' => $imagesImageId,
            ]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                $current = $result->current()->getArrayCopy();
                if ($current['count_imageid'] > 0) {
                    return true;
                }
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return false;
    }

    /**
     * @param array $storage
     * @return int
     */
    public function saveShopArticleImage(array $storage): int
    {
        $insert = $this->sql->insert();
        try {
            $insert->values($storage);
            if($this->executeInsert($insert) == 1) {
                return $this->adapter->getDriver()->getConnection()->getLastGeneratedValue('public.bk_images_image_bk_images_image_id_seq');
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function updateShopArticleImage(array $data)
    {
        $update = $this->sql->update();
        try {
            $update->set($data);
            $update->where([
                'shop_article_image_id' => $data['shop_article_image_id'],
            ]);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return $this->updateWith($update);
    }

    public function deleteShopArticleImage(array $data)
    {
        $delete = $this->sql->delete();
        $delete->where(['shop_article_image_id' => $data['shop_article_image_id']]);
        return $this->deleteWith($delete);
    }

    public function deleteShopArticleImageById(int $imageId)
    {
        $delete = $this->sql->delete();
        $delete->where(['shop_article_image_id' => $imageId]);
        return $this->deleteWith($delete);
    }

    public function deleteBitkornImages($bkImagesImageId)
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['bk_images_image_id' => $bkImagesImageId]);
            return $this->deleteWith($delete);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     *
     * @param int $articleImageId
     * @param int $articleImagePriority
     * @return int
     */
    public function updateShopArticleImagePriority(int $articleImageId, int $articleImagePriority): int
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'shop_article_image_priority' => $articleImagePriority
            ]);
            $update->where([
                'shop_article_image_id' => $articleImageId
            ]);
            return $this->updateWith($update);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

}
