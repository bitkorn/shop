<?php

namespace Bitkorn\Shop\Table\Article;

use Bitkorn\Shop\Table\AbstractShopTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Select;

/**
 *
 * @author allapow
 */
class ShopArticleRatingTable extends AbstractShopTable
{

    /**
     *
     * @var string
     */
    protected $table = 'shop_article_rating';

    /**
     *
     * @param int $articleId
     * @return array
     */
    public function getShopArticleRatings($articleId, $onlyPublished = true)
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'shop_article_id' => $articleId,
            ]);
            if ($onlyPublished) {
                $select->where([
                    'shop_article_rating_published' => 1,
                ], \Laminas\Db\Sql\Where::OP_AND);
            }
            $resultset = $this->selectWith($select);
            if ($resultset->valid() && $resultset->count() > 0) {
                return $resultset->toArray();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     *
     * @param int $basketItemId
     * @return boolean
     */
    public function existShopBasketEntityRating($basketItemId)
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'shop_basket_item_id' => $basketItemId,
            ]);
            $resultset = $this->selectWith($select);
            if ($resultset->valid() && $resultset->count() > 0) {
                return true;
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return false;
    }

    public function getShopBasketEntityRating($basketItemId)
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'shop_basket_item_id' => $basketItemId,
            ]);
            /** @var HydratingResultSet $resultset */
            $resultset = $this->selectWith($select);
            if ($resultset->valid() && $resultset->count() > 0) {
                return $resultset->current()->getArrayCopy();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     *
     * @param int $articleId
     * @param int $ratingValue
     * @param string $ratingText
     * @param int $basketEntityId
     * @return int
     */
    public function saveShopArticleRating($articleId, $ratingValue, $ratingText, $basketEntityId = null)
    {
        $insert = $this->sql->insert();
        try {
            $insert->values([
                'shop_article_id' => $articleId,
                'shop_basket_item_id' => $basketEntityId,
                'shop_article_rating_time_create' => time(),
                'shop_article_rating_value' => $ratingValue,
                'shop_article_rating_text' => $ratingText,
            ]);
            $this->insertWith($insert);
            $lastId = $this->adapter->getDriver()->getConnection()->getLastGeneratedValue('public.shop_article_rating_shop_article_rating_id_seq');
            if (empty($lastId)) {
                return -1;
            }
            return $lastId;
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function updateBasketItemRatingPublished($basketItemId, $articleRatingPublished)
    {
        $update = $this->sql->update();
        $update->where(['shop_basket_item_id' => $basketItemId]);
        try {
            $update->set([
                'shop_article_rating_published' => $articleRatingPublished
            ]);
            return $this->updateWith($update);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function searchShopArticleRatings()
    {
        $select = $this->sql->select();
        try {
            $select->join('shop_basket_entity', 'shop_basket_entity.shop_basket_item_id = shop_article_rating.shop_basket_item_id',
                'shop_basket_unique', Select::JOIN_LEFT);
            $select->join('shop_article', 'shop_article.shop_article_id = shop_article_rating.shop_article_id',
                'shop_article_name', Select::JOIN_LEFT);
//            $select->where(array(
//                'shop_basket_item_id' => $basketItemId,
//            ));
            $resultset = $this->selectWith($select);
            if ($resultset->valid() && $resultset->count() > 0) {
                return $resultset->toArray();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function updateRatingItemPublished($ratingId, $ratingPublished)
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'shop_article_rating_published' => $ratingPublished
            ]);
            $update->where(['shop_article_rating_id' => $ratingId]);
            return $this->updateWith($update);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

}
