<?php

namespace Bitkorn\Shop\Table\Article;

use Bitkorn\Shop\Table\AbstractShopTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Expression;

class ShopArticleCategoryTable extends AbstractShopTable
{

    /**
     * @var string
     */
    protected $table = 'shop_article_category';

    public function getShopArticleCategories()
    {
        $select = $this->sql->select();
        try {
            $select->order('shop_article_category_id ASC, shop_article_category_id_parent ASC');
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 1) {
                return $result->toArray();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function countShopArticleCategories()
    {
        $select = $this->sql->select();
        $select->columns([
            'countC' => new Expression("COUNT('shop_article_category_id')") //  as countC nicht mit Postgre kompatible
        ]);
        $results = $this->selectWith($select);
        $resultArr = $results->toArray();
        return $resultArr[0]['countC'];
    }

    public function getShopArticleCategoriesFromParent($idParent = 0)
    {
        $select = $this->sql->select();
        $select->where([
            'shop_article_category_id_parent' => $idParent,
        ]);
        $results = $this->selectWith($select);
        $resArr = $results->toArray();
        $returnArr = [];
        foreach ($resArr as $res) {
            $returnArr[$res['shop_article_category_id']]['shop_article_category_id'] = $res['shop_article_category_id'];
            $returnArr[$res['shop_article_category_id']]['shop_article_category_name'] = $res['shop_article_category_name'];
            $returnArr[$res['shop_article_category_id']]['shop_article_category_desc'] = $res['shop_article_category_desc'];
            $returnArr[$res['shop_article_category_id']]['shop_article_category_img'] = $res['shop_article_category_img'];
            $returnArr[$res['shop_article_category_id']]['shop_article_category_id_parent'] = $res['shop_article_category_id_parent'];
        }
        return $returnArr;
    }

    public function getShopArticleCategoryById($id)
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'shop_article_category_id' => $id,
            ]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getShopArticleCategoryByAlias(string $articleCategoryAlias): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['shop_article_category_alias' => $articleCategoryAlias,]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function saveShopArticleCategory(array $data)
    {
        $insert = $this->sql->insert();
        try {
            $insert->values($data);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }

        return $this->executeInsert($insert);
    }

    public function updateShopArticleCategory(array $data)
    {
        $update = $this->sql->update();
        try {
            $update->set($data);
            $update->where([
                'shop_article_category_id' => $data['shop_article_category_id'],
            ]);
            return $this->updateWith($update);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function getShopArticleCategoryIdAssocSimple($idParent = 0)
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'shop_article_category_id_parent' => $idParent,
            ]);
            $result = $this->selectWith($select);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        if ($result->count() > 0) {
            $resultArr = $result->toArray();
            $idAssoc = [];
            foreach ($resultArr as $res) {
                $idAssoc[$res['shop_article_category_id']] = $res['shop_article_category_name'];
            }
            return $idAssoc;
        }
        return [];
    }

    public function getShopArticleCategoryIdAssoc($idParent = 0)
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'shop_article_category_id_parent' => $idParent,
            ]);
            $result = $this->selectWith($select);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        if ($result->count() > 0) {
            $resultArr = $result->toArray();
            $idAssoc = [];
            foreach ($resultArr as $res) {
                $idAssoc[$res['shop_article_category_id']]['shop_article_category_id'] = $res['shop_article_category_id'];
                $idAssoc[$res['shop_article_category_id']]['shop_article_category_alias'] = $res['shop_article_category_alias'];
                $idAssoc[$res['shop_article_category_id']]['shop_article_category_name'] = $res['shop_article_category_name'];
            }
            return $idAssoc;
        }
        return [];
    }

    public function getShopArticleCategoryIdAssocRecursive(array &$idAssoc, $idParent = 0)
    {
        $childs = $this->getShopArticleCategoryIdAssoc($idParent);
        if (!empty($childs)) {
            foreach ($childs as &$child) {
                $this->getShopArticleCategoryIdAssocRecursive($child, $child['shop_article_category_id']);
            }
            $idAssoc['childs'] = $childs;
        }
    }

    public function getShopArticleCategoryDashedListRecursive(array &$idAssoc, $idParent = 0, $depth = '')
    {
        $childs = $this->getShopArticleCategoryIdAssoc($idParent);
        if (!empty($childs)) {
            foreach ($childs as &$child) {
                $idAssoc[$child['shop_article_category_id']]['name'] = $depth . ' ' . $child['shop_article_category_name'];
                $this->getShopArticleCategoryDashedListRecursive($idAssoc, $child['shop_article_category_id'], $depth . '-');
            }
        }
    }

}
