<?php

namespace Bitkorn\Shop\Table\Article\Param2d;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class ShopArticleParam2dItemTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'shop_article_param2d_item';

    /**
     * @param int $param2dId
     * @return array
     */
    public function getShopArticleParam2dItems(int $param2dId)
    {
        $select = new Select('view_article_param2d_items');
        try {
            $select->where(['shop_article_param2d_id' => $param2dId]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getShopArticleParam2dItemsForArticle(int $articleId): array
    {
        $select = new Select('view_article_param2d_items');
        try {
            $select->where(['shop_article_id' => $articleId]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param int $articleId
     * @return array key = shop_article_param2d_item_id
     */
    public function getShopArticleParam2dItemsForArticleIdAssoc(int $articleId): array
    {
        $items = $this->getShopArticleParam2dItemsForArticle($articleId);
        if (empty($items)) {
            return [];
        }
        $idAssoc = [];
        foreach ($items as $item) {
            $idAssoc[$item['shop_article_param2d_item_id']] = $item;
        }
        return $idAssoc;
    }

    public function getShopArticleParam2dItemForParam2dItemId(int $param2dItemId): array
    {
        $select = new Select('view_article_param2d_items');
        try {
            $select->where(['shop_article_param2d_item_id' => $param2dItemId]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}
