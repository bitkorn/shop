<?php

namespace Bitkorn\Shop\Table\Article\Param2d;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class ShopArticleParam2dTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'shop_article_param2d';

    /**
     * @param int $param2dId
     * @return array
     */
    public function getShopArticleParam2d(int $param2dId)
    {
        $select = $this->sql->select();
        try {
            $selectItemCount = new Select('shop_article_param2d_item');
            $selectItemCount->columns(['count_items' => new Expression('COUNT(\'shop_article_param2d_id\')')]);
            $selectItemCount->group('shop_article_param2d_id');
            $select->columns([
                Select::SQL_STAR,
                'count_items' => new Expression('?', [$selectItemCount])
            ]);
            $select->where(['shop_article_param2d_id' => $param2dId]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @return array
     */
    public function getShopArticleParam2dIdAssoc(): array
    {
        $select = $this->sql->select();
        $idAssoc = [];
        try {
            $selectItemCount = new Select('shop_article_param2d_item');
            $selectItemCount->columns(['count_items' => new Expression('COUNT(\'shop_article_param2d_id\')')]);
            $selectItemCount->group('shop_article_param2d_id');
            $select->columns([
                Select::SQL_STAR,
                'count_items' => new Expression('?', [$selectItemCount])
            ]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                $resultArr = $result->toArray();
                foreach ($resultArr as $row) {
                    $idAssoc[$row['shop_article_param2d_id']] = $row['shop_article_param2d_name'] . ' (' . $row['count_items'] . ')';
                }
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return $idAssoc;
    }
}
