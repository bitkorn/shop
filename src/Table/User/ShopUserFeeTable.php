<?php

namespace Bitkorn\Shop\Table\User;

use Bitkorn\Shop\Table\AbstractShopTable;
use Laminas\Db\Sql\Select;

/**
 * Description of ShopUserFeeTable
 *
 * @author allapow
 */
class ShopUserFeeTable extends AbstractShopTable
{

    /**
     *
     * @var string
     */
    protected $table = 'shop_user_fee';

    /**
     * @param string $userUuid
     * @return array
     */
    public function getShopUserFeeByUserId(string $userUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'user_uuid' => $userUuid,
            ]);
            $resultset = $this->selectWith($select);
            if ($resultset->valid() && $resultset->count() > 0) {
                $resultArray = $resultset->toArray();
                return $resultArray[0];
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function existShopUserFee($userUuid)
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'user_uuid' => $userUuid,
            ]);
            $resultset = $this->selectWith($select);
            if ($resultset->valid() && $resultset->count() > 0) {
                return true;
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return false;
    }

    public function saveOrUpdateShopUserFee($userId, $feePercent, $feeCouponPercent, $feeStatic, $feeMlmParentuserId,
                                            $feeMlmChildFeePercent, $feeMlmChildFeePercentFirst, $feeMlmChildFeeCouponPercent)
    {
        if (empty($this->getShopUserFeeByUserId($userId))) {
            return $this->saveNewShopUserFee($userId, $feePercent, $feeCouponPercent, $feeStatic, $feeMlmParentuserId,
                $feeMlmChildFeePercent, $feeMlmChildFeePercentFirst, $feeMlmChildFeeCouponPercent);
        } else {
            return $this->updateShopUserFee($userId, $feePercent, $feeCouponPercent, $feeStatic, $feeMlmParentuserId,
                $feeMlmChildFeePercent, $feeMlmChildFeePercentFirst, $feeMlmChildFeeCouponPercent);
        }
    }

    public function updateShopUserFee($userUuid, $feePercent, $feeCouponPercent, $feeStatic, $feeMlmParentuserUuid, $feeMlmChildFeePercent,
                                      $feeMlmChildFeePercentFirst, $feeMlmChildFeeCouponPercent)
    {
        $update = $this->sql->update();
        try {
            $update->set(['shop_user_fee_percent' => $feePercent,
                'shop_user_fee_coupon_percent' => $feeCouponPercent,
                'shop_user_fee_static' => $feeStatic,
                'shop_user_fee_mlm_parent_user_uuid' => $feeMlmParentuserUuid,
                'shop_user_fee_mlm_child_fee_percent' => $feeMlmChildFeePercent,
                'shop_user_fee_mlm_child_fee_percent_first' => $feeMlmChildFeePercentFirst,
                'shop_user_fee_mlm_child_fee_coupon_percent' => $feeMlmChildFeeCouponPercent]);
            $update->where(['user_uuid' => $userUuid]);
            return $this->updateWith($update);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function saveNewShopUserFee($userUuid, $feePercent, $feeCouponPercent, $feeStatic, $feeMlmParentuserUuid, $feeMlmChildFeePercent,
                                       $feeMlmChildFeePercentFirst, $feeMlmChildFeeCouponPercent)
    {
        $insert = $this->sql->insert();
        try {
            $insert->values([
                'user_uuid' => $userUuid,
                'shop_user_fee_percent' => $feePercent,
                'shop_user_fee_coupon_percent' => $feeCouponPercent,
                'shop_user_fee_static' => $feeStatic,
                'shop_user_fee_mlm_parent_user_uuid' => $feeMlmParentuserUuid,
                'shop_user_fee_mlm_child_fee_percent' => $feeMlmChildFeePercent,
                'shop_user_fee_mlm_child_fee_percent_first' => $feeMlmChildFeePercentFirst,
                'shop_user_fee_mlm_child_fee_coupon_percent' => $feeMlmChildFeeCouponPercent]);
            return $this->insertWith($insert);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function saveNewShopUserFeeMinimal($userId, $feeMlmParentuserUuid)
    {
        if ($this->existShopUserFee($userId)) {
            return 0;
        }
        $insert = $this->sql->insert();
        try {
            $insert->values([
                'user_uuid' => $userId,
                'shop_user_fee_mlm_parent_user_uuid' => $feeMlmParentuserUuid]);
            return $this->insertWith($insert);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function updateShopUserFeeEntity(array $storage)
    {
        $update = $this->sql->update();
        try {
            $update->set($storage);
            $update->where(['shop_user_fee_id' => $storage['shop_user_fee_id']]);
            return $this->updateWith($update);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function saveNewShopUserFeeEntity(array $storage)
    {
        $insert = $this->sql->insert();
        try {
            $insert->values($storage);
            return $this->insertWith($insert);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function getShopUserMlmChildsByUserId($userId)
    {
        $select = $this->sql->select();
        try {
            $select->join('shop_user_data', 'shop_user_data.user_uuid = shop_user_fee.user_uuid', ['shop_user_data_number_id']);
            $select->join('shop_user_data_number', 'shop_user_data_number.user_uuid = shop_user_fee.user_uuid', ['shop_user_data_number_int', 'shop_user_data_number_varchar']);
            $select->join('user', 'user.user_uuid = shop_user_fee.user_uuid', ['login', 'email']);
            $select->where(['shop_user_fee_mlm_parent_user_uuid' => $userId]);
            $select->order('shop_user_data_number.shop_user_data_number_int DESC');
            $resultset = $this->selectWith($select);
            if ($resultset->valid() && $resultset->count() > 0) {
                return $resultset->toArray();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $userUuid
     * @return int
     */
    public function deleteForUser(string $userUuid): int
    {
        return $this->delete(['user_uuid' => $userUuid]);
    }
}
