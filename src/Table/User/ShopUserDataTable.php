<?php

namespace Bitkorn\Shop\Table\User;

use Bitkorn\Shop\Table\AbstractShopTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Predicate\PredicateSet;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Exception\InvalidArgumentException;
use Laminas\Db\Sql\Exception\RuntimeException;

/**
 *
 * @author allapow
 */
class ShopUserDataTable extends AbstractShopTable
{

    /**
     * @var string
     */
    protected $table = 'shop_user_data';

    /**
     * @param array $storage
     * @return int
     */
    public function saveNewShopUserData(array $storage)
    {
        $insert = $this->sql->insert();
        try {
            $insert->values($storage);
            return $this->insertWith($insert);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function updateShopUserData(array $storage)
    {
        $update = $this->sql->update();
        try {
            $update->set($storage);
            $update->where(['shop_user_data_id' => $storage['shop_user_data_id']]);
            return $this->updateWith($update);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function updateShopUserDataByUserUuid(array $storage, $userUuid)
    {
        $update = $this->sql->update();
        try {
            $update->set($storage);
            $update->where(['user_uuid' => $userUuid]);
            return $this->updateWith($update);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function deleteShopUserData(array $storage)
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['shop_user_data_id' => $storage['shop_user_data_id']]);
            return $this->deleteWith($delete);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function getShopUserDataByUserUuid($userUuid)
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'user_uuid' => $userUuid,
            ]);
            $resultset = $this->selectWith($select);
            if ($resultset->valid() && $resultset->count() > 0) {
                return $resultset->current();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getShopUserDataXByUserUuid($userUuid)
    {
        $select = $this->sql->select();
        try {
            $select->join('shop_user_data_number',
                'shop_user_data_number.shop_user_data_number_id = shop_user_data.shop_user_data_number_id', ['shop_user_data_number_int', 'shop_user_data_number_varchar'],
                Select::JOIN_LEFT);
            $select->join('shop_user_group',
                'shop_user_group.shop_user_group_id = shop_user_data.shop_user_group_id', ['shop_user_group_name', 'shop_user_group_name_short'],
                Select::JOIN_LEFT);
            $select->join('user',
                'user.user_uuid = shop_user_data.user_uuid', ['user_login', 'user_active', 'user_time_create', 'user_email', 'equipment_uuid', 'user_lang_iso'],
                Select::JOIN_LEFT);
            $select->join('user_details',
                'user_details.user_uuid = shop_user_data.user_uuid', ['user_details_name_first', 'user_details_name_last', 'user_details_tel_land', 'user_details_tel_mobile'],
                Select::JOIN_LEFT);
            $select->where([
                'shop_user_data.user_uuid' => $userUuid,
            ]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getShopUserDataById($dataId)
    {
        $select = $this->sql->select();
        $select->where([
            'shop_user_data_id' => $dataId,
        ]);
        try {
            /** @var HydratingResultSet $resultset */
            $resultset = $this->selectWith($select);
            if ($resultset->valid() && $resultset->count() == 1) {
                $current = $resultset->current();
                return $current->getArrayCopy();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     *
     * @param int $userId
     * @return int 1 = true; 2 = false; 0 = no result
     */
    public function isShopUserPrivate($userId)
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'user_uuid' => $userId,
            ]);
            $resultset = $this->selectWith($select);
            if ($resultset->valid() && $resultset->count() == 1) {
                $current = $resultset->current();
                return $current['shop_user_customer_type'] == 'private' ? 1 : 2;
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return 0;
    }

    /**
     * @param array $options
     * @return array
     */
    public function searchShopUsers(array $options): array
    {
        $select = $this->sql->select();
        $returnArr = [];
        try {
            $select->join('user'
                , 'user.user_uuid = shop_user_data.user_uuid'
                , ['user_login', 'user_email', 'user_time_create', 'user_active']
                , Select::JOIN_LEFT);
            $select->join('shop_user_data_number'
                , 'shop_user_data_number.shop_user_data_number_id = shop_user_data.shop_user_data_number_id'
                , ['shop_user_data_number_int', 'shop_user_data_number_varchar']
                , Select::JOIN_LEFT);
            $select->join('shop_user_group'
                , 'shop_user_group.shop_user_group_id = shop_user_data.shop_user_group_id'
                , ['shop_user_group_name']
                , Select::JOIN_LEFT);
            $select->join('shop_user_address'
                , 'shop_user_address.user_uuid = shop_user_data.user_uuid'
                , ['count_address' => new Expression('COUNT(shop_user_address_id)')]
                , Select::JOIN_LEFT);
            $select->join('user_role_relation'
                , 'user_role_relation.user_uuid = shop_user_data.user_uuid'
                , ['user_role_id']
                , Select::JOIN_LEFT);
            if (!empty($options['only_active'])) {
                $select->where([
                    'shop_user_data.shop_user_data_active' => 1
                ], PredicateSet::OP_AND);
            }
            if (!empty($options['shop_user_group_id'])) {
                $select->where([
                    'shop_user_data.shop_user_group_id' => $options['shop_user_group_id']
                ], PredicateSet::OP_AND);
            }
            if (!empty($options['only_with_role'])) {
                $select->where->expression('shop_user_data.user_uuid IN (SELECT DISTINCT user_uuid FROM user_role_relation)');
            }
            if (!empty($options['create_by_user_uuid'])) {
                $select->where([
                    'shop_user_data.shop_user_data_user_uuid_create' => $options['create_by_user_uuid']
                ], PredicateSet::OP_AND);
            }

            $select->group('user.user_uuid')
                ->group('shop_user_data_id')
                ->group('user_role_relation.user_role_id')
                ->group('shop_user_data_number.shop_user_data_number_int')
                ->group('shop_user_data_number.shop_user_data_number_varchar')
                ->group('shop_user_group.shop_user_group_name');

            if (!empty($options['order_by']) && !empty($options['order_direc'])) {
                $select->order($options['order_by'] . ' ' . $options['order_direc']);
            }
            $resultset = $this->selectWith($select);
            if ($resultset->valid() && $resultset->count() > 0) {
                $resultArr = $resultset->toArray();
                foreach ($resultArr as $resultRow) {
                    $returnArr[] = $resultRow;
                }
            }
        } catch (InvalidArgumentException $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        } catch (RuntimeException $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return $returnArr;
    }

    /**
     *
     * @param string $userUuid
     * @param string $paypalEmail
     * @return int
     */
    public function updateShopUserFeePayPalEmail(string $userUuid, string $paypalEmail): int
    {
        $update = $this->sql->update();
        try {
            $update->set(['shop_user_data_paypal_email' => $paypalEmail]);
            $update->where(['user_uuid' => $userUuid]);
            return $this->updateWith($update);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param int $shopUserRoleId
     * @param string $excludeUserUuid
     * @param array $emptyValue
     * @return array
     */
    public function getShopUserByRoleIdAssoc(int $shopUserRoleId, string $excludeUserUuid = '', array $emptyValue = [0 => '-- bitte wählen --']): array
    {
        $select = $this->sql->select();
        $idAssoc = $emptyValue;
        try {
            $select->join('user', 'user.user_uuid = shop_user_data.user_uuid', Select::SQL_STAR, Select::JOIN_LEFT);
            $selectIn = new Select();
            $selectIn->from('user_role_relation')->columns(['user_uuid'])->where(['user_role_id' => $shopUserRoleId]);
            if (!empty($excludeUserUuid)) {
                $selectIn->where->notEqualTo('user_uuid', $excludeUserUuid);
            }
            $select->where->in('shop_user_data.user_uuid', $selectIn)->and->equalTo('shop_user_data.shop_user_data_active', 1);
            $select->order('user.user_login ASC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                $resultArray = $result->toArray();
                foreach ($resultArray as $row) {
                    $idAssoc[$row['user_uuid']] = $row['user_login'] . ' - ' . $row['user_email'];
                }
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return $idAssoc;
    }

    public function updateShopUserDataBank($userId, $bankOwnerName, $bankBankName, $bankIban, $bankBic): int
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'shop_user_data_bank_owner_name' => $bankOwnerName,
                'shop_user_data_bank_bank_name' => $bankBankName,
                'shop_user_data_bank_iban' => $bankIban,
                'shop_user_data_bank_bic' => $bankBic
            ]);
            $update->where(['user_uuid' => $userId]);
            return $this->updateWith($update);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param string $userUuid
     * @return int
     */
    public function deleteForUser(string $userUuid): int
    {
        return $this->delete(['user_uuid' => $userUuid]);
    }
}
