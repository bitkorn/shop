<?php

namespace Bitkorn\Shop\Table\User;

use Bitkorn\Shop\Table\AbstractShopTable;
use Laminas\Db\Adapter\Driver\Pdo\Result;
use Laminas\Db\Sql\Expression;
use Bitkorn\Shop\Service\User\ShopUserNumberServiceInterface;

/**
 *
 * @author allapow
 */
class ShopUserDataNumberTable extends AbstractShopTable
{

    /**
     *
     * @var string
     */
    protected $table = 'shop_user_data_number';

    /**
     * Generiert mit shop_user_data_number_int + 1 und einem ShopUserNumberService eine neue Kunden-Nr. und speichert diese.
     *
     * @param string $userUuid
     * @param int $shopUserGroupId
     * @param ShopUserNumberServiceInterface $numberService
     * @return int Last insert ID or -1.
     */
    public function createNewShopUserDataNumber($userUuid, $shopUserGroupId, ShopUserNumberServiceInterface $numberService)
    {
        $this->adapter->query('LOCK TABLE ' . $this->table);
        $select = $this->sql->select();
        $select->columns([new Expression('MAX(shop_user_data_number_int) as max_int')]);

        $nextInt = 0;
        try {
            $select->where(['shop_user_group_id' => $shopUserGroupId]);
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                $current = $result->current();
                $nextInt = $current['max_int'] + 1;
            } else {
                $nextInt++;
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        $newShopUserNumber = $numberService->generateNewShopUserNumberFromInt($shopUserGroupId, $nextInt);

        $insert = $this->sql->insert();
        try {
            $insert->values([
                'user_uuid' => $userUuid,
                'shop_user_group_id' => $shopUserGroupId,
                'shop_user_data_number_int' => $nextInt,
                'shop_user_data_number_varchar' => $newShopUserNumber
            ]);
            $stmt = $this->sql->prepareStatementForSqlObject($insert);
            $resultInsert = $stmt->execute();
            if ($resultInsert->valid()) {
                $this->adapter->query('UNLOCK TABLES');
                return $this->getAdapter()->getDriver()->getConnection()->getLastGeneratedValue('public.shop_user_data_number_shop_user_data_number_id_seq');
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        $this->adapter->query('UNLOCK TABLES');
        return -1;
    }

    /**
     * @param string $userUuid
     * @return int
     */
    public function deleteForUser(string $userUuid): int
    {
        return $this->delete(['user_uuid' => $userUuid]);
    }
}
