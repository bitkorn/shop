<?php

namespace Bitkorn\Shop\Table\User;

use Bitkorn\Shop\Table\AbstractShopTable;
use Laminas\Db\Sql\Select;

/**
 * Description of ShopUserFeePaidTable
 *
 * @author allapow
 */
class ShopUserFeePaidTable extends AbstractShopTable
{

    /**
     *
     * @var string
     */
    protected $table = 'shop_user_fee_paid';

    /**
     * @param int $basketEntityId
     * @return bool
     */
    public function existShopUserFeePaidByBasketEntityId(int $basketEntityId): bool
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'shop_basket_entity_id' => $basketEntityId,
            ]);
            $resultset = $this->selectWith($select);
            if ($resultset->valid() && $resultset->count() > 0) {
                return true;
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return false;
    }

    /**
     * @param $userUuid
     * @param $basketEntityId
     * @param $paidSum
     * @param $timePaid
     * @param string $paymentId
     * @param string $paymentMethod
     * @return int
     */
    public function saveNewShopUserFeePaid($userUuid, $basketEntityId, $paidSum, $timePaid, $paymentId = '', $paymentMethod = 'other'): int
    {
        $insert = $this->sql->insert();
        try {
            $insert->values([
                'user_uuid' => $userUuid,
                'shop_basket_entity_id' => $basketEntityId,
                'shop_user_fee_paid_sum' => $paidSum,
                'shop_user_fee_paid_time' => $timePaid,
                'shop_user_fee_paid_payment_id' => $paymentId,
                'shop_user_fee_paid_payment_method' => $paymentMethod
            ]);
            return $this->insertWith($insert);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function deleteShopUserFeePaidByBasketEntityId($basketEntityId)
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['shop_basket_entity_id' => $basketEntityId]);
            return $this->deleteWith($delete);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param string $userUuid
     * @return int
     */
    public function deleteForUser(string $userUuid): int
    {
        return $this->delete(['user_uuid' => $userUuid]);
    }
}
