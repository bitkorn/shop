<?php

namespace Bitkorn\Shop\Table\User;

use Bitkorn\Shop\Entity\Basket\Address\IsoCountry;
use Bitkorn\Shop\Table\AbstractShopTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Select;

/**
 *
 * @author allapow
 */
class ShopUserAddressTable extends AbstractShopTable
{

    /**
     *
     * @var string
     */
    protected $table = 'shop_user_address';

    const ADDRESS_TYPE_INVOICE = 'invoice';
    const ADDRESS_TYPE_SHIPPING = 'shipment';

    /**
     *
     * @param array $storage
     * @return int
     */
    public function saveNewShopUserAddress(array $storage)
    {
        $insert = $this->sql->insert();
        try {
            $insert->values($storage);
            return $this->insertWith($insert);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function updateShopUserAddress(array $storage)
    {
        $update = $this->sql->update();
        try {
            $update->set($storage);
            $update->where(['shop_user_address_id' => $storage['shop_user_address_id']]);
            return $this->updateWith($update);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param array $storage
     * @return int
     */
    public function deleteShopUserAddress(array $storage): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['shop_user_address_id' => $storage['shop_user_address_id']]);
            return $this->deleteWith($delete);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     *
     * @param string $userUuid
     * @param bool $addressTypeAssoc If true then return as an associative array that has the address type (invoice & shipment) as key.
     * @return array The shopUser addresses (invoice & shipment). First address is the shipping address.
     * Result is joined with db.user & db.shop_user_data_number.
     */
    public function getShopUserAddressesByUserId(string $userUuid, bool $addressTypeAssoc = false)
    {
        $select = $this->sql->select();
        $assoc = [];
        try {
            $select->where([
                'shop_user_address.user_uuid' => $userUuid,
            ]);
            $select->join('user', 'user.user_uuid = shop_user_address.user_uuid', Select::SQL_STAR, Select::JOIN_LEFT);
            $select->join('shop_user_data_number', 'shop_user_data_number.user_uuid = shop_user_address.user_uuid', Select::SQL_STAR, Select::JOIN_LEFT);
            $select->join('country', 'country.country_id = shop_user_address.country_id', ['country_name', 'country_iso', 'country_member_eu', 'country_currency_euro'], Select::JOIN_LEFT);
            $select->order(['shop_user_address_id ASC']);
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                $resultArr = $result->toArray();
                if (!$addressTypeAssoc) {
                    return $resultArr;
                }
                foreach ($resultArr as $address) {
                    $assoc[$address['shop_user_address_type']] = $address;
                }
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return $assoc;
    }

    /**
     * @param int $addressId
     * @return array
     */
    public function getShopUserAddressById(int $addressId): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['shop_user_address_id' => $addressId]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     *
     * @param string $userUuid
     * @param string $addressType
     * @return IsoCountry|null
     */
    public function computeIsoCountryByUserId(string $userUuid, string $addressType)
    {
        $select = $this->sql->select();
        try {
            $select->join('country', 'country.country_id = shop_user_address.country_id', Select::SQL_STAR, Select::JOIN_LEFT);
            $select->where([
                'user_uuid' => $userUuid,
                'shop_user_address_type' => $addressType
            ]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                $current = $result->current();
                return new IsoCountry($current['country_id'], $current['country_iso'], $current['country_name'], $current['country_member_eu']);
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return null;
    }

    /**
     * @param string $userUuid
     * @return int
     */
    public function countShopUserAddress(string $userUuid): int
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'user_uuid' => $userUuid,
            ]);
            $result = $this->selectWith($select);
            if ($result->valid()) {
                return $result->count();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return 0;
    }

    public function searchShopUserByName_jQueryAutocomplete($shopUserName)
    {
        $select = $this->sql->select();
        $labelValueAssoc = [];
        try {
            $select->where->like('shop_user_address_name1', "%$shopUserName%")->OR->like('shop_user_address_name2', "%$shopUserName%");
//			$select->group('user_uuid');
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                $resultArr = $result->toArray();
                foreach ($resultArr as $resultRow) {
                    $labelValueAssoc[] = [
                        'value' => $resultRow['user_uuid'],
                        'label' => $resultRow['shop_user_address_name1'] . ' ' . $resultRow['shop_user_address_name2'] . ' (' . $resultRow['shop_user_address_type'] . ')'
                    ];
                }
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return $labelValueAssoc;
    }

    /**
     * @param string $userUuid
     * @return int
     */
    public function deleteForUser(string $userUuid): int
    {
        return $this->delete(['user_uuid' => $userUuid]);
    }
}
