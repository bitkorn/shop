<?php

namespace Bitkorn\Shop\Table\User;

use Bitkorn\Shop\Table\AbstractShopTable;
use Laminas\Db\Adapter\Exception\RuntimeException;
use Laminas\Db\Sql\Select;

/**
 *
 * @author allapow
 */
class ShopUserGroupTable extends AbstractShopTable
{

    /**
     *
     * @var string
     */
    protected $table = 'shop_user_group';

    /**
     *
     * @param array $emptyValue
     * @return array
     */
    public function getShopUserGroupIdAssoc($emptyValue = ['' => '-- bitte wählen --'])
    {
        $select = $this->sql->select();
        if($emptyValue) {
            $idAssoc = $emptyValue;
        }
        try {
            $select->order('shop_user_group_id ASC');
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                $resultArray = $result->toArray();
                foreach ($resultArray as $row) {
                    $idAssoc[$row['shop_user_group_id']] = $row['shop_user_group_id'] . ' - ' . $row['shop_user_group_name'];
                }
            }
        } catch (RuntimeException $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return $idAssoc;
    }

    /**
     * @param int $shopUserGroupId
     * @return array
     */
    public function getShopUserByGroupIdAssocX(int $shopUserGroupId): array
    {
        $select = $this->sql->select();
        $idAssoc = [];
        try {
            $select->join('shop_user_data', 'shop_user_data.shop_user_group_id = shop_user_group.shop_user_group_id', Select::SQL_STAR, Select::JOIN_LEFT);
            $select->join('user', 'user.user_uuid = shop_user_data.user_uuid', Select::SQL_STAR, Select::JOIN_LEFT);
            $select->where(['shop_user_group.shop_user_group_id' => $shopUserGroupId, 'shop_user_data.shop_user_data_active = 1']);
            $select->order('user.login ASC');
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                $resultArray = $result->toArray();
                foreach ($resultArray as $row) {
                    $idAssoc[$row['user_uuid']] = $row['login'] . ' - ' . $row['email'];
                }
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return $idAssoc;
    }

    /**
     *
     * @return array
     */
    public function getShopUserByGroupsIdAssocX(array $shopUserGroupIds)
    {
        $select = $this->sql->select();
        $idAssoc = [];
        try {
            $select->join('shop_user_data', 'shop_user_data.shop_user_group_id = shop_user_group.shop_user_group_id', Select::SQL_STAR, Select::JOIN_LEFT);
            $select->join('user', 'user.user_uuid = shop_user_data.user_uuid', Select::SQL_STAR, Select::JOIN_LEFT);
            $select->where->in('shop_user_group.shop_user_group_id', $shopUserGroupIds)->and->equalTo('shop_user_data.shop_user_data_active', 1);
            $select->order('user.login ASC');
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                $resultArray = $result->toArray();
                foreach ($resultArray as $row) {
                    $idAssoc[$row['user_uuid']] = $row['login'] . ' - ' . $row['email'] . ' (' . $row['shop_user_group_name_short'] . ')';
                }
            }
        } catch (RuntimeException $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return $idAssoc;
    }

}
