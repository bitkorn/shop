<?php

namespace Bitkorn\Shop\Table\Vendor;

use Bitkorn\Shop\Table\AbstractShopTable;

/**
 *
 * @author allapow
 */
class ShopVendorTable extends AbstractShopTable
{

    /**
     *
     * @var string
     */
    protected $table = 'shop_vendor';

    /**
     *
     * @param int $groupId
     * @return array
     */
    public function getShopVendorsByGroup($groupId = 1)
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'shop_vendor_group_id' => $groupId,
            ]);
            $resultset = $this->selectWith($select);
            if ($resultset->valid() && $resultset->count() > 0) {
                return $resultset->toArray();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     *
     * @param int $vendorId
     * @return array
     */
    public function getShopVendorById($vendorId)
    {
        $select = $this->sql->select();
        try {
            $select->where(['shop_vendor_id' => $vendorId]);
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                $resArr = $result->toArray();
                if (isset($resArr[0])) {
                    return $resArr[0];
                }
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     *
     * @param array $storage
     * @return int
     */
    public function saveShopVendor(array $storage)
    {
        $insert = $this->sql->insert();
        try {
            $insert->values($storage);
            $this->insertWith($insert);
            $lastId = $this->getAdapter()->getDriver()->getConnection()->getLastGeneratedValue('public.shop_vendor_shop_vendor_id_seq');
            if (empty($lastId)) {
                return -1;
            }
            return $lastId;
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     *
     * @param int $vendorId
     * @param array $storage
     * @return int
     */
    public function updateShopVendor($vendorId, array $storage)
    {
        $update = $this->sql->update();
        try {
            $update->where(['shop_vendor_id' => $vendorId]);
            $update->set($storage);
            return $this->updateWith($update);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function getShopVendorNumberVarcharIdAssoc()
    {
        $select = $this->sql->select();
        $idAssoc = [];
        try {
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                $resultArr = $result->toArray();
                foreach ($resultArr as $res) {
                    $idAssoc[$res['shop_vendor_id']] = $res['shop_vendor_number_varchar'];
                }
            }
        } catch (\Laminas\Db\Adapter\Exception\RuntimeException $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return $idAssoc;
    }

    public function getShopVendorIdAssoc()
    {
        $select = $this->sql->select();
        $idAssoc = [];
        try {
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                $resultArr = $result->toArray();
                foreach ($resultArr as $res) {
                    $idAssoc[$res['shop_vendor_id']] = $res['shop_vendor_number_varchar'] . ' | ' . $res['shop_vendor_name'];
                }
            }
        } catch (\Laminas\Db\Adapter\Exception\RuntimeException $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return $idAssoc;
    }

    /**
     *
     * @return int MAX(shop_vendor_number_int)
     */
    public function getShopVendorNoMax()
    {
        $select = $this->sql->select();
        try {
            $select->columns(['shop_vendor_no_max' => new \Laminas\Db\Sql\Expression('MAX(shop_vendor_number_int)')]);
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                $current = $result->current();
                if (isset($current['shop_vendor_no_max'])) {
                    return $current['shop_vendor_no_max'];
                }
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return 0;
    }
}
