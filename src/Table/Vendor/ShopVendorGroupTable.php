<?php

namespace Bitkorn\Shop\Table\Vendor;

use Bitkorn\Shop\Table\AbstractShopTable;
use Laminas\Db\ResultSet\HydratingResultSet;

/**
 *
 * @author allapow
 */
class ShopVendorGroupTable extends AbstractShopTable
{

    /**
     *
     * @var string
     */
    protected $table = 'shop_vendor_group';

    /**
     *
     * @param int $id
     * @return array
     */
    public function getShopVendorGroupById($id)
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'shop_vendor_group_id' => $id,
            ]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     *
     * @param int $no
     * @return array
     */
    public function getShopVendorGroupByNo($no)
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'shop_vendor_group_no' => $no,
            ]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function saveShopVendorGroup(array $data)
    {
        $insert = $this->sql->insert();
        try {
            $insert->values($data);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }

        return $this->executeInsert($insert);
    }

    public function getShopVendorGroupNoAssoc()
    {
        $select = $this->sql->select();
        $idAssoc = [];
        try {
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                $resultArr = $result->toArray();
                foreach ($resultArr as $res) {
                    $idAssoc[$res['shop_vendor_group_no']] = $res['shop_vendor_group_no'] . ' ' . $res['shop_vendor_group_name'];
                }
            }
        } catch (\Laminas\Db\Adapter\Exception\RuntimeException $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return $idAssoc;
    }

    public function getShopVendorGroupIdAssoc($emptyValue = [0 => '-- bitte wählen --'])
    {
        $select = $this->sql->select();
        $idAssoc = $emptyValue;
        try {
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                $resultArr = $result->toArray();
                foreach ($resultArr as $res) {
                    $idAssoc[$res['shop_vendor_group_id']] = $res['shop_vendor_group_no'] . ' ' . $res['shop_vendor_group_name'];
                }
            }
        } catch (\Laminas\Db\Adapter\Exception\RuntimeException $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return $idAssoc;
    }

}
