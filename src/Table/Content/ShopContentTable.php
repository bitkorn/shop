<?php

namespace Bitkorn\Shop\Table\Content;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class ShopContentTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'shop_content';

    /**
     * @param string $contentAlias
     * @return array
     */
    public function getContentByAlias(string $contentAlias): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['shop_content_alias' => $contentAlias]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}
