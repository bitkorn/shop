<?php

namespace Bitkorn\Shop\Table;

use Bitkorn\Trinket\Table\AbstractLibTable;

/**
 * Description of AbstractShopTable
 *
 * @author allapow
 */
class AbstractShopTable extends AbstractLibTable
{
    
    /**
     * 
     * @param string $idColumn
     * @param string $valueColumn
     * @return array
     */
    public function getIdAssoc($idColumn, $valueColumn)
    {
        $select = $this->sql->select();
        $idAssoc = [];
        try {
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                $resultArr = $result->toArray();
                foreach ($resultArr as $res) {
                    $idAssoc[$res[$idColumn]] = $res[$valueColumn];
                }
            }
        } catch (\RuntimeException $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return $idAssoc;
    }
    
    /**
     * 
     * @param string $column
     * @param mixed $value
     * @return bool
     */
    public function exist(string $column, $value) : bool
    {
        $select = $this->sql->select();
        try {
            $select->where([$column => $value]);
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return true;
            }
        } catch (\RuntimeException $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return false;
    }

}
