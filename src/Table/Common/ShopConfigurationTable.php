<?php

namespace Bitkorn\Shop\Table\Common;

use Bitkorn\Shop\Table\AbstractShopTable;

/**
 *
 * @author allapow
 */
class ShopConfigurationTable extends AbstractShopTable
{

    /**
     *
     * @var string
     */
    protected $table = 'shop_configuration';

    /**
     *
     * @param string $shopConfigurationName
     * @return string
     */
    public function getShopConfigurationValue($shopConfigurationName)
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'shop_configuration_key' => $shopConfigurationName,
            ]);
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                $current = $result->current();
                return $current['shop_configuration_value'];
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function getShopConfigurations()
    {
        $select = $this->sql->select();
        try {
            $select->order(['shop_configuration_group ASC', 'shop_configuration_orderweight DESC']);
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getShopConfigurationsGrouped()
    {
        $select = $this->sql->select();
        $groupedAssoc = [];
        try {
            $select->order(['shop_configuration_group ASC', 'shop_configuration_orderweight DESC']);
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                $resultArr = $result->toArray();
                foreach ($resultArr as $row) {
                    if (!isset($groupedAssoc[$row['shop_configuration_group']])) {
                        $groupedAssoc[$row['shop_configuration_group']] = [];
                    }
                    $groupedAssoc[$row['shop_configuration_group']][$row['shop_configuration_id']] = $row;
                }
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return $groupedAssoc;
    }

    /**
     * @param int $shopConfigurationId
     * @param string $shopConfigurationValue
     * @return int
     */
    public function updateShopConfigurationValue(int $shopConfigurationId, string $shopConfigurationValue): int
    {
        if (empty($shopConfigurationId)) {
            return -1;
        }
        $update = $this->sql->update();
        try {
            $update->set(['shop_configuration_value' => $shopConfigurationValue]);
            $update->where(['shop_configuration_id' => $shopConfigurationId]);
            return $this->updateWith($update);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }
}
