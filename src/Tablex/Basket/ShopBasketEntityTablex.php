<?php

namespace Bitkorn\Shop\Tablex\Basket;

use Bitkorn\Shop\Table\Basket\ShopBasketEntityTable;
use Bitkorn\Shop\Tablex\AbstractTablex;
use Laminas\Db\Adapter\ParameterContainer;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Sql;

/**
 *
 * @author allapow
 */
class ShopBasketEntityTablex extends AbstractTablex
{

    /**
     *
     * @param int $userUuid
     * @param string $shopBasketUnique
     * @param ShopBasketEntityTable $shopBasketEntityTable
     * @return int 1=only shipment is updated; 2=both addresses are updated, 0=some error
     */
    public function copyShopUserAddressesToBasketEntity($userUuid, $shopBasketUnique, ShopBasketEntityTable $shopBasketEntityTable)
    {
        $parameter = new ParameterContainer([$userUuid]);
        $stmtShipping = $this->adapter->createStatement('SELECT * FROM shop_user_address WHERE user_uuid=? AND shop_user_address_type=\'shipment\'', $parameter);
        $stmtShipping->prepare();
        $resultShipping = $stmtShipping->execute();
        $updateShipping = 0;
        if ($resultShipping->valid() && $resultShipping->count() > 0) {
            $updateShipping = $shopBasketEntityTable->updateShopUserAddress($resultShipping->current(), $shopBasketUnique, 'shipping');
        }
        $stmtInvoice = $this->adapter->createStatement('SELECT * FROM shop_user_address WHERE user_uuid=? AND shop_user_address_type=\'invoice\'', $parameter);
        $stmtInvoice->prepare();
        $resultInvoice = $stmtInvoice->execute();
        $updateInvoice = 0;
        if ($resultInvoice->valid() && $resultInvoice->count() > 0) {
            $updateInvoice = $shopBasketEntityTable->updateShopUserAddress($resultInvoice->current(), $shopBasketUnique, 'invoice');
        }
        return $updateShipping + $updateInvoice;
    }

    /**
     *
     * @param string $shopBasketUnique
     * @param ShopBasketEntityTable $shopBasketEntityTable
     * @return int 1=only shipment is updated; 2=both addresses are updated, 0=some error
     */
    public function copyShopBasketAddressesToBasketEntity($shopBasketUnique, ShopBasketEntityTable $shopBasketEntityTable)
    {
        $parameter = new ParameterContainer([$shopBasketUnique]);
        $stmtShipping = $this->adapter->createStatement('SELECT * FROM shop_basket_address WHERE shop_basket_unique=? AND shop_basket_address_type=\'shipment\'',
            $parameter);
        $stmtShipping->prepare();
        $resultShipping = $stmtShipping->execute();
        $updateShipping = 0;
        if ($resultShipping->valid() && $resultShipping->count() > 0) {
            $updateShipping = $shopBasketEntityTable->updateShopBasketAddress($resultShipping->current(), $shopBasketUnique, 'shipping');
        }
        $stmtInvoice = $this->adapter->createStatement('SELECT * FROM shop_basket_address WHERE shop_basket_unique=? AND shop_basket_address_type=\'invoice\'',
            $parameter);
        $stmtInvoice->prepare();
        $resultInvoice = $stmtInvoice->execute();
        $updateInvoice = 0;
        if ($resultInvoice->valid() && $resultInvoice->count() > 0) {
            $updateInvoice = $shopBasketEntityTable->updateShopBasketAddress($resultInvoice->current(), $shopBasketUnique, 'invoice');
        }
        return $updateShipping + $updateInvoice;
    }

    /**
     * @param string $paymentNumber
     * @param string $shopDocumentOrderNumber
     * @param int $timeCreateFrom
     * @param int $timeCreateTo
     * @param string $basketStatus
     * @param string $userUuid
     * @param string $orderBy
     * @param string $orderByDirection
     * @return array
     */
    public function searchShopBasketItems(string $paymentNumber, string $shopDocumentOrderNumber, int $timeCreateFrom = 0, int $timeCreateTo = 9999999999,
                                          string $basketStatus = '', string $userUuid = '', string $orderBy = 'shop_basket_time_order', string $orderByDirection = 'DESC'): array
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select('view_basket_entities_admin');
        if (!empty($paymentNumber)) {
            $select->where->like('payment_number', "$paymentNumber");
        }
        if (!empty($shopDocumentOrderNumber)) {
            $select->where->like('shop_document_order_number', $shopDocumentOrderNumber);
        }
        if (!empty($basketStatus)) {
            $select->where(['shop_basket_status' => $basketStatus]);
        }
        if (!empty($userUuid)) {
            $select->where(['user_uuid' => $userUuid]);
        }
        $select->where->greaterThanOrEqualTo('shop_basket_time_order', $timeCreateFrom);
        $select->where->lessThanOrEqualTo('shop_basket_time_order', $timeCreateTo);

        $select->order("$orderBy $orderByDirection");

        $stmt = $sql->prepareStatementForSqlObject($select);

        $result = $stmt->execute();
        $returnArray = [];
        if ($result->valid() && $result->count() > 0) {
            do {
                $returnArray[] = $result->current();
                $result->next();
            } while ($result->valid());
        }
        return $returnArray;
    }

    const MOVE_BASKETADDRESS_TO_USERADDRESS = 'INSERT
                                                INTO
                                                        shop_user_address(
                                                                user_uuid,
                                                                shop_user_address_customer_type,
                                                                shop_user_address_type,
                                                                shop_user_address_salut,
                                                                shop_user_address_degree,
                                                                shop_user_address_name1,
                                                                shop_user_address_name2,
                                                                shop_user_address_street,
                                                                shop_user_address_street_no,
                                                                shop_user_address_zip,
                                                                shop_user_address_city,
                                                                shop_user_address_additional,
                                                                country_id,
                                                                shop_user_address_tel,
                                                                shop_user_address_birthday,
                                                                shop_user_address_tax_id,
                                                                shop_user_address_company_name,
                                                                shop_user_address_company_department
                                                        ) SELECT
                                                                ?,
                                                                shop_basket_address_customer_type,
                                                                shop_basket_address_type,
                                                                shop_basket_address_salut,
                                                                shop_basket_address_degree,
                                                                shop_basket_address_name1,
                                                                shop_basket_address_name2,
                                                                shop_basket_address_street,
                                                                shop_basket_address_street_no,
                                                                shop_basket_address_zip,
                                                                shop_basket_address_city,
                                                                shop_basket_address_additional,
                                                                country_id,
                                                                shop_basket_address_tel,
                                                                shop_basket_address_birthday,
                                                                shop_basket_address_tax_id,
                                                                shop_basket_address_company_name,
                                                                shop_basket_address_company_department
                                                        FROM
                                                                shop_basket_address
                                                        WHERE
                                                                shop_basket_address.shop_basket_unique = ?';

    /**
     *
     * @param string $basketUnique
     * @param string $userUuid
     * @return int
     */
    public function moveShopBasketAddressesToShopUserAddresses($basketUnique, $userUuid): int
    {
        $parameterBasket = new ParameterContainer([$basketUnique]);
        $parameter = new ParameterContainer([$userUuid, $basketUnique]);
        $stmtCopy = $this->adapter->createStatement(self::MOVE_BASKETADDRESS_TO_USERADDRESS, $parameter);
        $stmtCopy->prepare();
        $resultSelect = $stmtCopy->execute();
        if (!$resultSelect->valid()) {
            return -1;
        }
        $countCopy = $resultSelect->count();
        if ($countCopy < 1) {
            return 0;
        }
        $stmtDelete = $this->adapter->createStatement('DELETE FROM shop_basket_address WHERE shop_basket_unique=?', $parameterBasket);
        $stmtDelete->prepare();
        $resultDelete = $stmtDelete->execute();
        if (!$resultDelete->valid()) {
            return -1;
        }
        $countDelete = $resultDelete->count();
        if ($countDelete != $countCopy) {
            return -1;
        }
        return 1;
    }

}
