<?php

namespace Bitkorn\Shop\Tablex\Basket;

use Bitkorn\Shop\Tablex\AbstractTablex;
use Laminas\Db\Adapter\ParameterContainer;

/**
 *
 * @author allapow
 */
class ShopBasketTablex extends AbstractTablex
{

    const SELECT_SHOP_BASKET = 'SELECT 
                                    sbi.shop_basket_item_id,
                                    sbi.shop_article_id,
                                    sbi.shop_article_amount,
                                    sbi.shop_basket_item_article_options,
                                    sbi.shop_article_param2d_item_id,
                                    sbi.shop_basket_item_lengthcut_options,
                                    sb.*,
                                    sa.shop_article_sku,
                                    sa.shop_article_type,
                                    sa.shop_article_size_def_id,
                                    sa.shop_article_weight,
                                    sa.shop_article_shipping_costs_single,
                                    sa.shop_article_price,
                                    sa.shop_article_tax,
                                    sa.shop_article_price_net,
                                    sbi.shop_article_amount * sa.shop_article_price AS shop_article_price_total,
                                    sbi.shop_article_amount * sa.shop_article_price_net AS shop_article_price_net_total,
                                    sa.shop_article_name,
                                    sa.shop_article_sefurl,
                                    sa.shop_article_desc,
                                    sa.shop_article_active,
                                    sa.shop_article_stock_using
                                FROM
                                    shop_basket_item sbi
                                        LEFT JOIN
                                    shop_basket sb USING (shop_basket_unique)
                                        LEFT JOIN
                                    shop_article sa USING (shop_article_id)
                                WHERE
                                    sb.shop_basket_unique = ?';

    /**
     * @param string $shopBasketUnique
     * @return array ShopBasketItems
     */
    public function getShopBasket(string $shopBasketUnique)
    {
        $parameter = new ParameterContainer([$shopBasketUnique]);
        $stmt = $this->adapter->createStatement(self::SELECT_SHOP_BASKET, $parameter);
        $stmt->prepare();
        $result = $stmt->execute();
        $resultArr = [];
        if ($result->valid() && $result->count() > 0) {
            do {
                $current = $result->current();
                $resultArr[] = $current;
                $result->next();
            } while ($result->valid());
        }
        return $resultArr;
    }

    /**
     * @param string $shopBasketUnique
     * @param string $shopBasketStatus
     * @return array List of shop_basket_item FROM view_basket_items
     */
    public function getShopBasketByStatus($shopBasketUnique, $shopBasketStatus = 'basket')
    {
        $parameter = new ParameterContainer([$shopBasketUnique, $shopBasketStatus]);
        $stmt = $this->adapter->createStatement('SELECT * FROM view_basket_items WHERE shop_basket_unique = ? AND shop_basket_status = ?', $parameter);
        $stmt->prepare();
        $result = $stmt->execute();
        $resultArr = [];
        if ($result->valid() && $result->count() > 0) {
            do {
                $current = $result->current();
                $resultArr[] = $current;
                $result->next();
            } while ($result->valid());
        }
        return $resultArr;
    }

    public function isShopBasketItemsEditable($basketUnique)
    {
        $parameter = new ParameterContainer([$basketUnique]);
        $stmt = $this->adapter->createStatement('SELECT shop_basket_status FROM shop_basket WHERE shop_basket_unique=?', $parameter);
        $stmt->prepare();
        $result = $stmt->execute();
        if ($result->valid() && $result->count() == 1) {
            $current = $result->current();
            if ($current['shop_basket_status'] == 'basket') {
                return true;
            }
        }
        return false;
    }

    public function countShopBasketDiscount($shopBasketDiscountHash)
    {
        $parameter = new ParameterContainer([$shopBasketDiscountHash]);
        $stmt = $this->adapter->createStatement('SELECT COUNT(shop_basket_discount_hash) AS hash_count FROM shop_basket WHERE shop_basket_discount_hash=?', $parameter);
        $stmt->prepare();
        $result = $stmt->execute();
        if ($result->valid() && $result->count() == 1) {
            $current = $result->current();
            return $current['hash_count'];
        }
        return 0;
    }

    /**
     * Delete ShopBasketItems, ShopBasketAddresses, ShopBasket
     * @param string $basketUnique
     * @return int
     * @todo Delete ArticleStock with ArticleOptions
     * @todo Sicher loeschen mit Transaktionen
     */
    public function deleteShopBasket(string $basketUnique): int
    {
        $parameter = new ParameterContainer([$basketUnique]);
        $stmt = $this->adapter->createStatement('DELETE FROM shop_basket_item WHERE shop_basket_unique=?', $parameter);
        $stmt->prepare();
        $result = $stmt->execute();
        $success = 0;
        if (!$result->valid()) {
            $success = -1;
        } else {
            $success += $result->getAffectedRows();
        }

        $stmt2 = $this->adapter->createStatement('DELETE FROM shop_basket_address WHERE shop_basket_unique=?', $parameter);
        $stmt2->prepare();
        $result2 = $stmt2->execute();
        if (!$result2->valid()) {
            $success = -1;
        } else {
            $success += $result2->getAffectedRows();
        }

        if ($success >= 0) {
            $stmt3 = $this->adapter->createStatement('DELETE FROM shop_basket WHERE shop_basket_unique=?', $parameter);
            $stmt3->prepare();
            $result3 = $stmt3->execute();
            if (!$result3->valid()) {
                $success = -1;
            } else {
                $success += $result3->getAffectedRows();
            }
        }
        return $success;
    }

}
