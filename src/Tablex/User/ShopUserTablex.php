<?php

namespace Bitkorn\Shop\Tablex\User;

use Bitkorn\Shop\Tablex\AbstractTablex;
use Laminas\Db\Adapter\Driver\Pdo\Result;
use Laminas\Db\Adapter\ParameterContainer;

/**
 *
 * @author allapow
 */
class ShopUserTablex extends AbstractTablex
{

    /**
     *
     */
    const SELECT_COUNT_SHIPMENT_ADDRESS = 'SELECT COUNT(shop_user_address_id) AS count_shipment_address 
                                            FROM shop_user_address 
                                            WHERE shop_user_address_type= \'shipment\'
                                            AND user_uuid=?';

    /**
     *
     * @return int
     */
    public function countShipmentAddress($userId)
    {
        $parameter = new ParameterContainer([$userId]);
        $stmt = $this->adapter->createStatement(self::SELECT_COUNT_SHIPMENT_ADDRESS, $parameter);
        $stmt->prepare();
        $result = $stmt->execute();
        if ($result->valid() && $result->count() > 0) {
            $current = $result->current();
            return $current['count_shipment_address'];
        }
        return -1;
    }

    /**
     *
     */
    const SELECT_COUNT_INVOICE_ADDRESS = 'SELECT COUNT(shop_user_address_id) AS count_invoice_address FROM shop_user_address
                                            WHERE shop_user_address_type=\'invoice\'
                                            AND user_uuid=?';

    /**
     *
     * @return int
     */
    public function countInvoiceAddress($userId)
    {
        $parameter = new ParameterContainer([$userId]);
        $stmt = $this->adapter->createStatement(self::SELECT_COUNT_INVOICE_ADDRESS, $parameter);
        $stmt->prepare();
        $result = $stmt->execute();
        if ($result->valid() && $result->count() > 0) {
            $current = $result->current();
            return $current['count_invoice_address'];
        }
        return -1;
    }

    /**
     *
     */
    const SELECT_COUNT_ADDRESS = 'SELECT COUNT(shop_user_address_id) AS count_address FROM shop_user_address WHERE user_uuid=?';

    /**
     * @param $userUuid
     * @return int
     * @deprecated use ShopUserAddressTable()->countShopUserAddress()
     */
    public function countAddress($userUuid): int
    {
        $parameter = new ParameterContainer([$userUuid]);
        $stmt = $this->adapter->createStatement(self::SELECT_COUNT_ADDRESS, $parameter);
        $stmt->prepare();
        $result = $stmt->execute();
        if ($result->valid() && $result->count() > 0) {
            $current = $result->current();
            return intval($current['count_address']);
        }
        return -1;
    }

    /**
     *
     */
    const SELECT_SHOP_USER_WITH_ADDRESSES = 'SELECT 
                                                    sua.*, u.*, sudn.*
                                                FROM
                                                    shop_user_address sua
                                                        LEFT JOIN
                                                    "user" u ON u.user_uuid = sua.user_uuid
                                                        LEFT JOIN
                                                    shop_user_data_number sudn ON sudn.user_uuid = sua.user_uuid
                                                WHERE
                                                    sua.user_uuid = ?';

    /**
     * @param string $userUuid
     * @return array The shopUser addresses (invoice & shipment) in an associative array that has the address type (invoice & shipment) as key.
     */
    public function getShopUserWithAddresses(string $userUuid): array
    {
        $parameter = new ParameterContainer([$userUuid]);
        $stmt = $this->adapter->createStatement(self::SELECT_SHOP_USER_WITH_ADDRESSES, $parameter);
        $stmt->prepare();
        $result = $stmt->execute();
        $resultArray = [];
        if ($result->valid() && $result->count() > 0) {
            do {
                $current = $result->current();
                $resultArray[$current['shop_user_address_type']] = $current;
                $result->next();
            } while ($result->valid());
        }
        return $resultArray;
    }

    /**
     *
     */
    const SELECT_JOIN_BASKETENTITY_DOCUMENT_INVOICES_BYUSERID = 'SELECT 
                                                                        sdi.*
                                                                    FROM
                                                                        shop_document_invoice sdi
                                                                            LEFT JOIN
                                                                        shop_basket_entity sbe USING (shop_basket_unique)
                                                                    WHERE
                                                                        sbe.user_uuid = ?';

    /**
     *
     * @param string $userUuid
     * @return array
     */
    public function getShopDocumentInvoicesBasketuniqueAssocByUserid($userUuid)
    {
        $parameter = new ParameterContainer([$userUuid]);
        $stmt = $this->adapter->createStatement(self::SELECT_JOIN_BASKETENTITY_DOCUMENT_INVOICES_BYUSERID, $parameter);
        $stmt->prepare();
        $result = $stmt->execute();
        $basketuniqueAssoc = [];
        if ($result->valid() && $result->count() > 0) {
            do {
                $current = $result->current();
                $basketuniqueAssoc[$current['shop_basket_unique']] = $current;
                $result->next();
            } while ($result->valid());
        }
        return $basketuniqueAssoc;
    }

    /**
     *
     */
    const SELECT_JOIN_BASKETENTITY_DOCUMENT_ORDERS_BYUSERID = 'SELECT 
                                                                        sdo.*
                                                                    FROM
                                                                        shop_document_order sdo
                                                                            LEFT JOIN
                                                                        shop_basket_entity sbe USING (shop_basket_unique)
                                                                    WHERE
                                                                        sbe.user_uuid = ?';

    /**
     *
     * @param int $userId
     * @return array
     */
    public function getShopDocumentOrdersBasketuniqueAssocByUserid($userId)
    {
        $parameter = new ParameterContainer([$userId]);
        $stmt = $this->adapter->createStatement(self::SELECT_JOIN_BASKETENTITY_DOCUMENT_ORDERS_BYUSERID, $parameter);
        $stmt->prepare();
        $result = $stmt->execute();
        $basketuniqueAssoc = [];
        if ($result->valid() && $result->count() > 0) {
            do {
                $current = $result->current();
                $basketuniqueAssoc[$current['shop_basket_unique']] = $current;
                $result->next();
            } while ($result->valid());
        }
        return $basketuniqueAssoc;
    }

    /**
     *
     */
    const SELECT_JOIN_REGISTERED_SHOPUSERS = 'SELECT sud.*, u.*, sudn.*, sug.*, COUNT(sua.shop_user_address_id) as count_address FROM shop_user_data sud
                                    LEFT JOIN "user" u ON u.user_uuid = sud.user_uuid
                                    LEFT JOIN shop_user_data_number sudn ON sudn.user_uuid = sud.user_uuid
                                    LEFT JOIN shop_user_group sug ON sug.shop_user_group_id = sud.shop_user_group_id
                                    LEFT JOIN shop_user_address sua ON sua.user_uuid = sud.user_uuid
                                    WHERE sud.shop_user_data_active=1
                                    GROUP BY sud.user_uuid
                                    ORDER BY ? ?';

    public function getRegisteredShopUsers($orderColumn, $orderDirection)
    {
        $parameter = new ParameterContainer([$orderColumn, $orderDirection]);
        $stmt = $this->adapter->createStatement(self::SELECT_JOIN_REGISTERED_SHOPUSERS, $parameter);
        $stmt->prepare();
        $result = $stmt->execute();
        $returnArray = [];
        if ($result->valid() && $result->count() > 0) {
            do {
                $current = $result->current();
                $returnArray[$current['user_uuid']] = $result->current();
                $result->next();
            } while ($result->valid());
        }
        return $returnArray;
    }

    /**
     *
     */
    const SELECT_JOIN_REGISTERED_SHOPUSERS_BY_GROUP = 'SELECT sud.*, u.*, sudn.*, sug.*, COUNT(sua.shop_user_address_id) as count_address FROM shop_user_data sud
                                    LEFT JOIN "user" u ON u.user_uuid = sud.user_uuid
                                    LEFT JOIN shop_user_data_number sudn ON sudn.user_uuid = sud.user_uuid
                                    LEFT JOIN shop_user_group sug ON sug.shop_user_group_id = sud.shop_user_group_id
                                    LEFT JOIN shop_user_address sua ON sua.user_uuid = sud.user_uuid
                                    WHERE sud.shop_user_data_active=1
                                    AND sug.shop_user_group_id = ?
                                    GROUP BY sud.user_uuid
                                    ORDER BY ? ?';

    public function getRegisteredShopUsersByGroup($shopUserGroupId, $orderColumn, $orderDirection)
    {
        $parameter = new ParameterContainer([$shopUserGroupId, $orderColumn, $orderDirection]);
        $stmt = $this->adapter->createStatement(self::SELECT_JOIN_REGISTERED_SHOPUSERS_BY_GROUP, $parameter);
        $stmt->prepare();
        $result = $stmt->execute();
        $returnArray = [];
        if ($result->valid() && $result->count() > 0) {
            do {
                $current = $result->current();
                $returnArray[$current['user_uuid']] = $result->current();
                $result->next();
            } while ($result->valid());
        }
        return $returnArray;
    }

    /**
     *
     */
    const SELECT_SHOP_USER_DATA = 'SELECT 
                                        sud.*, u.*, sudn.*, sug.*, COUNT(sua.shop_user_address_id) AS shop_user_address_count,
                                        suf.shop_user_fee_mlm_parent_user_uuid
                                    FROM
                                        shop_user_data sud
                                        LEFT JOIN
                                        "user" u ON u.user_uuid = sud.user_uuid
                                        LEFT JOIN
                                        shop_user_data_number sudn ON sudn.user_uuid = sud.user_uuid
                                        LEFT JOIN
                                        shop_user_group sug ON sug.shop_user_group_id = sud.shop_user_group_id
                                        LEFT JOIN
                                        shop_user_address sua ON sua.user_uuid = sud.user_uuid
                                        LEFT JOIN
                                        shop_user_fee suf ON suf.user_uuid = sud.user_uuid
                                    WHERE
                                        sud.user_uuid = ?
                                    GROUP BY sud.shop_user_data_id, sudn.shop_user_data_number_id, u.user_uuid, sug.shop_user_group_id, suf.shop_user_fee_mlm_parent_user_uuid';

    public function getShopUserData($userId)
    {
        $parameter = new ParameterContainer([$userId]);
        $stmt = $this->adapter->createStatement(self::SELECT_SHOP_USER_DATA, $parameter);
        $stmt->prepare();
        /** @var Result $result */
        $result = $stmt->execute();
        if ($result->valid() && $result->count() == 1) {
            return $result->current();
        }
        return [];
    }
}
