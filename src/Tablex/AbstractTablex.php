<?php

namespace Bitkorn\Shop\Tablex;
use Laminas\Db\Adapter\Adapter;
use Laminas\Log\Logger;

/**
 * Description of AbstractTablex
 *
 * @author allapow
 */
class AbstractTablex
{

    /**
     *
     * @var \Laminas\Db\Adapter\Adapter
     */
    protected $adapter;

    /**
     *
     * @var \Laminas\Log\Logger
     */
    protected $logger;

    /**
     * 
     * @param Adapter $adapter
     */
    public function setDbAdapter(Adapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function setLogger(Logger $logger)
    {
        $this->logger = $logger;
    }

}
