<?php

namespace Bitkorn\Shop\Tablex\Vendor;

use Bitkorn\Shop\Tablex\AbstractTablex;
use Laminas\Db\Adapter\ParameterContainer;

/**
 *
 * @author allapow
 */
class ShopVendorTablex extends AbstractTablex
{

    /**
     *
     */
    const SELECT_JOIN_SHOP_VENDORS = 'SELECT
                                                sv.*,
                                                svg.*
                                        FROM
                                                shop_vendor sv
                                        LEFT JOIN shop_vendor_group svg
                                                        USING(shop_vendor_group_id)
                                        ORDER BY
                                                svg.shop_vendor_group_no ASC';

    /**
     *
     */
    const SELECT_JOIN_SHOP_VENDORS_BY_GROUP = 'SELECT
                                                        sv.*,
                                                        svg.*
                                                FROM
                                                        shop_vendor sv
                                                LEFT JOIN shop_vendor_group svg
                                                                USING(shop_vendor_group_id)
                                                WHERE
                                                        sv.shop_vendor_group_id =?
                                                ORDER BY
                                                        svg.shop_vendor_group_no ASC';

    /**
     *
     * @param int $shopVendorGroupId
     * @param boolean $idAssoc
     * @return array
     */
    public function getShopVendors($shopVendorGroupId = 0, $idAssoc = false)
    {
        if (!empty($shopVendorGroupId)) {
            $parameter = new ParameterContainer([$shopVendorGroupId]);
            $stmt = $this->adapter->createStatement(self::SELECT_JOIN_SHOP_VENDORS_BY_GROUP, $parameter);
        } else {
            $stmt = $this->adapter->createStatement(self::SELECT_JOIN_SHOP_VENDORS);
        }
        $result = $stmt->execute();
        $resultArr = [];
        if ($result->valid() && $result->count() > 0) {
            do {
                $current = $result->current();
                if (!$idAssoc) {
                    $resultArr[] = $current;
                } else {
                    $resultArr[$current['shop_vendor_id']] = $current;
                }
                $result->next();
            } while ($result->valid());
        }
        return $resultArr;
    }

    const SELECT_JOIN_IN_SELECT_ARTICLEID = 'SELECT
                                                        DISTINCT sv.*,
                                                        svg.*
                                                FROM
                                                        shop_vendor sv
                                                LEFT JOIN shop_vendor_group svg
                                                                USING(shop_vendor_group_id)
                                                WHERE
                                                        sv.shop_vendor_id IN(
                                                                SELECT
                                                                        shop_vendor_id
                                                                FROM
                                                                        shop_article_stock
                                                                WHERE
                                                                        shop_article_id = 1
                                                        )
                                                ORDER BY
                                                        svg.shop_vendor_group_no ASC';

    public function getShopVendorsByArticleId($shopArticleId, $idAssoc = false, $idAssocAll = false)
    {
        $parameter = new ParameterContainer([$shopArticleId]);
        $stmt = $this->adapter->createStatement(self::SELECT_JOIN_IN_SELECT_ARTICLEID, $parameter);
        $result = $stmt->execute();
        $resultArr = [];
        if ($result->valid() && $result->count() > 0) {
            do {
                $current = $result->current();
                if (!$idAssoc) {
                    $resultArr[] = $current;
                } else {
                    if ($idAssocAll) {
                        $resultArr[$current['shop_vendor_id']] = $current;
                    } else {
                        $resultArr[$current['shop_vendor_id']] = $current['shop_vendor_number_varchar'] . ' | ' . $current['shop_vendor_name'];
                    }
                }
                $result->next();
            } while ($result->valid());
        }
        return $resultArr;
    }

}
