<?php

namespace Bitkorn\Shop\Tablex\Article;

use Bitkorn\Shop\Tablex\AbstractTablex;
use Laminas\Db\Adapter\ParameterContainer;

/**
 *
 * @author allapow
 */
class ShopArticleStockTablex extends AbstractTablex
{

    const SELECT_SHOP_ARTICLE_STOCK = 'SELECT 
                                            sa.shop_article_sku, sa.shop_article_name, sa.shop_article_desc,
                                            SUM(sas.shop_article_stock_amount) AS shop_article_stock_amount_sum
                                        FROM
                                            shop_article_stock sas
                                                LEFT JOIN
                                            shop_article sa USING (shop_article_id)
                                        WHERE
                                            sa.shop_article_stock_using = 1
                                                AND sas.shop_article_id = ?
                                        GROUP BY sas.shop_article_id';

    const SELECT_SHOP_ARTICLE_STOCK_ARTICLE_ACTIVE = 'SELECT 
                                            sa.shop_article_sku, sa.shop_article_name, sa.shop_article_desc,
                                            SUM(sas.shop_article_stock_amount) AS shop_article_stock_amount_sum
                                        FROM
                                            shop_article_stock sas
                                                LEFT JOIN
                                            shop_article sa USING (shop_article_id)
                                        WHERE
                                            sa.shop_article_stock_using = 1
                                                AND sa.shop_article_active = 1
                                                AND sas.shop_article_id = ?
                                        GROUP BY sas.shop_article_id';

    /**
     * Gibt Artikel-Stammdaten und seinen aktuellen Lagerbestand.
     * @param int $shopArticleId
     * @param boolean $onlyActive Default TRUE
     * @return array
     */
    public function getShopArticleStock($shopArticleId, $onlyActive = true)
    {
        $parameter = new ParameterContainer([$shopArticleId]);
        if ($onlyActive) {
            $stmt = $this->adapter->createStatement(self::SELECT_SHOP_ARTICLE_STOCK_ARTICLE_ACTIVE, $parameter);
        } else {
            $stmt = $this->adapter->createStatement(self::SELECT_SHOP_ARTICLE_STOCK, $parameter);
        }
        $result = $stmt->execute();
        if ($result->valid() && $result->count() == 1) {
            $current = $result->current();
            return $current;
        }
        return [];
    }


    const SELECT_SHOP_ARTICLE_STOCK_LIST = 'SELECT 
                                            sas.*,
                                            sa.shop_article_sku, sa.shop_article_name, sa.shop_article_desc
                                        FROM
                                            shop_article_stock sas
                                                LEFT JOIN
                                            shop_article sa USING (shop_article_id)
                                        WHERE
                                            sa.shop_article_stock_using = 1
                                                AND sas.shop_article_id = ?
                                        ORDER BY sas.shop_article_stock_time DESC';

    /**
     * Gibt zu einem Artikel jede Lagerbewegung.
     * @param int $shopArticleId
     * @return array
     */
    public function getShopArticleStockList($shopArticleId)
    {
        $parameter = new ParameterContainer([$shopArticleId]);
        $stmt = $this->adapter->createStatement(self::SELECT_SHOP_ARTICLE_STOCK_LIST, $parameter);
        $result = $stmt->execute();
        $returnArray = [];
        if ($result->valid() && $result->count() > 0) {
            do {
                $returnArray[] = $result->current();
                $result->next();
            } while ($result->valid());
        }
        return $returnArray;
    }
}
