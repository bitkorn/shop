<?php

namespace Bitkorn\Shop\Tablex\Article;

use Bitkorn\Shop\Tablex\AbstractTablex;

/**
 *
 * @author allapow
 */
class ShopArticleImageTablex extends AbstractTablex
{
    const BK_IMAGES_IMAGEGROUP_ID_SHOPARTICLE = 2;

    /**
     *
     */
    const SELECT_JOIN_SHOP_ARTICLEIMAGES_FOR_ARTICLE = 'SELECT 
                                                            sai.*, bkii.*, bkiig.*
                                                        FROM
                                                            shop_article_image sai
														LEFT JOIN
                                                            bk_images_image bkii USING (bk_images_image_id)
														LEFT JOIN
                                                            bk_images_imagegroup bkiig ON bkiig.bk_images_imagegroup_id = bkii.bk_images_imagegroup_id
                                                        WHERE sai.shop_article_id = ?
                                                        ORDER BY sai.shop_article_image_priority DESC, bkii.bk_images_image_priority DESC';

    /**
     *
     * @return array
     */
    public function getShopArticleImages($shopArticleId)
    {
        $parameter = new \Laminas\Db\Adapter\ParameterContainer([$shopArticleId]);
        $stmt = $this->adapter->createStatement(self::SELECT_JOIN_SHOP_ARTICLEIMAGES_FOR_ARTICLE, $parameter);
        $result = $stmt->execute();
        $resultArr = [];
        if ($result->valid() && $result->count() > 0) {
//            return $result->getResource()->fetchAll(); // ONLY PDO!!!
            do {
                $current = $result->current();
                $resultArr[] = $current;
                $result->next();
            } while ($result->valid());
        }
        return $resultArr;
    }

    /**
     *
     */
    const SELECT_JOIN_IMAGES_EXCLUDE_ARTICLE = 'SELECT 
                                                    *
                                                FROM
                                                    bk_images_image
                                                WHERE bk_images_image_id NOT IN (
                                                    SELECT bk_images_image_id FROM shop_article_image WHERE shop_article_id = ?                                                          
                                                ) AND bk_images_imagegroup_id = ' . self::BK_IMAGES_IMAGEGROUP_ID_SHOPARTICLE;

    /**
     *
     * @return array
     */
    public function getImagesExcludeForShopArticle($shopArticleId)
    {
        $parameter = new \Laminas\Db\Adapter\ParameterContainer([$shopArticleId]);
        $stmt = $this->adapter->createStatement(self::SELECT_JOIN_IMAGES_EXCLUDE_ARTICLE, $parameter);
        $result = $stmt->execute();
        $resultArr = [];
        if ($result->valid() && $result->count() > 0) {
//            return $result->getResource()->fetchAll(); // ONLY PDO!!!
            do {
                $current = $result->current();
                $resultArr[] = $current;
                $result->next();
            } while ($result->valid());
        }
        return $resultArr;
    }

    /**
     *
     */
    const SELECT_JOIN_IMAGES_EXCLUDE_ARTICLE_OPTION = 'SELECT 
                                                            *
                                                        FROM
                                                            bk_images_image
                                                        WHERE bk_images_image_id NOT IN (
                                                            SELECT bk_images_image_id FROM shop_article_option_item_article_image_relation 
                                                                WHERE shop_article_id = ? 
                                                                AND shop_article_option_item_id = ?
                                                        ) AND bk_images_imagegroup_id = ' . self::BK_IMAGES_IMAGEGROUP_ID_SHOPARTICLE;

    /**
     *
     * @return array
     */
    public function getImagesExcludeForShopArticleOption($shopArticleId, $optionItemId)
    {
        $parameter = new \Laminas\Db\Adapter\ParameterContainer([$shopArticleId, $optionItemId]);
        $stmt = $this->adapter->createStatement(self::SELECT_JOIN_IMAGES_EXCLUDE_ARTICLE_OPTION, $parameter);
        $result = $stmt->execute();
        $resultArr = [];
        if ($result->valid() && $result->count() > 0) {
//            return $result->getResource()->fetchAll(); // ONLY PDO!!!
            do {
                $current = $result->current();
                $resultArr[] = $current;
                $result->next();
            } while ($result->valid());
        }
        return $resultArr;
    }

}
