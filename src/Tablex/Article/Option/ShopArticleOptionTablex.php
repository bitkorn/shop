<?php

namespace Bitkorn\Shop\Tablex\Article\Option;

use Bitkorn\Shop\Tablex\AbstractTablex;
use Laminas\Db\Adapter\ParameterContainer;

/**
 *
 * @author allapow
 */
class ShopArticleOptionTablex extends AbstractTablex
{

    const SELECT_SHOP_ARTICLE_OPTION_DEFS = 'SELECT
                                                        DISTINCT saod.*,
                                                        COUNT( DISTINCT saoi.shop_article_option_item_id ) AS count_saoi,
                                                        COUNT( DISTINCT saoar.shop_article_option_article_relation_id ) AS count_saoar
                                                FROM
                                                        shop_article_option_def saod
                                                LEFT JOIN shop_article_option_item saoi
                                                                USING(shop_article_option_def_id)
                                                LEFT JOIN shop_article_option_article_relation saoar
                                                                USING(shop_article_option_def_id)
                                                GROUP BY
                                                        saod.shop_article_option_def_id
                                                ORDER BY saod.shop_article_option_def_priority DESC';

    /**
     *
     * @return array
     */
    public function getShopArticleOptionDefs()
    {
        $stmt = $this->adapter->createStatement(self::SELECT_SHOP_ARTICLE_OPTION_DEFS);
        $result = $stmt->execute();
        $returnArray = [];
        if ($result->valid() && $result->count() > 0) {
            return $result->getResource()->fetchAll(\PDO::FETCH_ASSOC); // ONLY PDO!!!
//            do {
//                $current = $result->current();
//                $returnArray[] = $current;
//            } while ($result->next());
        }
        return $returnArray;
    }

    const SELECT_SHOP_ARTICLE_OPTION_DEF_ARTICLE_IMAGES = 'SELECT
                                                                    saoiair.*,
                                                                    saoi.*,
                                                                    bkii.*
                                                            FROM
                                                                    shop_article_option_item_article_image_relation saoiair
                                                            LEFT JOIN shop_article_option_item saoi
                                                                            USING(shop_article_option_item_id)
                                                            LEFT JOIN bk_images_image bkii
                                                                            USING(bk_images_image_id)
                                                            WHERE
                                                                    saoi.shop_article_option_def_id = ?
                                                                    AND saoiair.shop_article_id = ?
                                                            ORDER BY saoiair.shop_article_option_item_article_image_relation_priority DESC';

    /**
     *
     * @param int $optionDefId
     * @param int $articleId
     * @return array IdAssoc: id=shop_article_option_item_id . '_' . $counter; value=db row
     */
    public function getShopArticleOptionDefArticleImagesIdAssoc($optionDefId, $articleId)
    {
        $parameter = new ParameterContainer([$optionDefId, $articleId]);
        $stmt = $this->adapter->createStatement(self::SELECT_SHOP_ARTICLE_OPTION_DEF_ARTICLE_IMAGES, $parameter);
        $result = $stmt->execute();
        $returnArray = [];
        if ($result->valid() && $result->count() > 0) {
            do {
                $current = $result->current();
                if (!isset($returnArray[$current['shop_article_option_item_id']])) {
                    $returnArray[$current['shop_article_option_item_id']] = [];
                }
                $returnArray[$current['shop_article_option_item_id']][] = $current;
                $result->next();
            } while ($result->valid());
        }
        return $returnArray;
    }

    /**
     *
     * @param int $optionDefId
     * @param int $articleId
     * @return array
     */
    public function getShopArticleOptionDefArticleImages($optionDefId, $articleId)
    {
        $parameter = new ParameterContainer([$optionDefId, $articleId]);
        $stmt = $this->adapter->createStatement(self::SELECT_SHOP_ARTICLE_OPTION_DEF_ARTICLE_IMAGES, $parameter);
        $result = $stmt->execute();
        $returnArray = [];
        if ($result->valid() && $result->count() > 0) {
            do {
                $current = $result->current();
                $returnArray[] = $current;
                $result->next();
            } while ($result->valid());
        }
        return $returnArray;
    }

    public function getShopArticleOptionDefArticleImagesBoth($optionDefId, $articleId)
    {
        $parameter = new ParameterContainer([$optionDefId, $articleId]);
        $stmt = $this->adapter->createStatement(self::SELECT_SHOP_ARTICLE_OPTION_DEF_ARTICLE_IMAGES, $parameter);
        $result = $stmt->execute();
        $idAssoc = [];
        $stock = [];
        if ($result->valid() && $result->count() > 0) {
            do {
                $current = $result->current();
                $stock[] = $current;
                if (!isset($idAssoc[$current['shop_article_option_item_id']])) {
                    $idAssoc[$current['shop_article_option_item_id']] = [];
                }
                $idAssoc[$current['shop_article_option_item_id']][] = $current;
                $result->next();
            } while ($result->valid());
        }
        return ['stock' => $stock, 'idAssoc' => $idAssoc];
    }

    /**
     * @param int $articleId
     * @param bool $askPricediff
     * @return array
     */
    public function getShopArticleOptionItemArticleRelationsIdAssoc(int $articleId, bool $askPricediff = false): array
    {
        $parameter = new ParameterContainer([$articleId]);
        $stmt = $this->adapter->createStatement('SELECT * FROM view_article_option_items WHERE shop_article_id = ?', $parameter);
        $result = $stmt->execute();
        $returnArray = [];
        if ($result->valid() && $result->count() > 0) {
            if ($askPricediff) {
                $pricediff = 0;
            }
            do {
                $current = $result->current();
                if (!isset($returnArray[$current['shop_article_option_def_id']])) {
                    $returnArray[$current['shop_article_option_def_id']] = [];
                }
                $returnArray[$current['shop_article_option_def_id']][] = $current;
                if ($askPricediff) {
                    if ($current['pricediff'] != 0) {
                        $pricediff = 1;
                    }
                }
                $result->next();
            } while ($result->valid());
            if ($askPricediff) {
                $returnArray['pricediff'] = $pricediff;
            }
        }
        return $returnArray;
    }


    const SELECT_SHOP_ARTICLE_OPTION_ITEM_ARTICLE_IMAGES = 'SELECT
                                                                    DISTINCT saoiair.*,
                                                                    saoi.shop_article_option_item_priority,
                                                                    bkii.*
                                                            FROM
                                                                    shop_article_option_item_article_image_relation saoiair
                                                            LEFT JOIN shop_article_option_item saoi
                                                                            USING(shop_article_option_item_id)
                                                            LEFT JOIN shop_article_option_article_relation saoar ON
                                                                    saoar.shop_article_option_def_id = saoi.shop_article_option_def_id
                                                            LEFT JOIN bk_images_image bkii
                                                                            USING(bk_images_image_id)
                                                            WHERE
                                                                    saoiair.shop_article_option_item_id = ?
                                                                    AND saoiair.shop_article_id = ?
                                                            ORDER BY
                                                                    saoi.shop_article_option_item_priority DESC,
                                                                    saoiair.shop_article_option_item_article_image_relation_priority DESC';

    /**
     *
     * @param int $articleId
     * @return array
     */
    public function getShopArticleOptionItemArticleImages($optionItemId, $articleId)
    {
        $parameter = new ParameterContainer([$optionItemId, $articleId]);
        $stmt = $this->adapter->createStatement(self::SELECT_SHOP_ARTICLE_OPTION_ITEM_ARTICLE_IMAGES, $parameter);
        $result = $stmt->execute();
        $returnArray = [];
        if ($result->valid() && $result->count() > 0) {
            do {
                $current = $result->current();
                $returnArray[] = $current;
                $result->next();
            } while ($result->valid());
        }
        return $returnArray;
    }

    const SELECT_SHOP_ARTICLE_OPTION_ITEMS = 'SELECT
                                                        saoi.*,
                                                        saod.*
                                                FROM
                                                        shop_article_option_item saoi
                                                LEFT JOIN shop_article_option_def saod
                                                                USING(shop_article_option_def_id)
                                                ORDER by
                                                        saod.shop_article_option_def_priority DESC, saoi.shop_article_option_item_priority DESC';


    /**
     *
     * @param int $articleId
     * @return array
     */
    public function getShopArticleOptionItemsIdAssoc()
    {
        $stmt = $this->adapter->createStatement(self::SELECT_SHOP_ARTICLE_OPTION_ITEMS);
        $result = $stmt->execute();
        $returnArray = [];
        if ($result->valid() && $result->count() > 0) {
            do {
                $current = $result->current();
                if (!isset($returnArray[$current['shop_article_option_def_id']])) {
                    $returnArray[$current['shop_article_option_def_id']] = [];
                }
                $returnArray[$current['shop_article_option_def_id']][] = $current;
                $result->next();
            } while ($result->valid());
        }
        return $returnArray;
    }

    /**
     *
     */
    const SELECT_SHOP_ARTICLE_BY_ITEMVALUE = 'SELECT
                                                        sa.*,
                                                        saod.*,
                                                        saoi.*
                                                from
                                                        shop_article sa
                                                LEFT JOIN shop_article_option_article_relation saoar
                                                                USING(shop_article_id)
                                                LEFT JOIN shop_article_option_def saod ON
                                                        saod.shop_article_option_def_id = saoar.shop_article_option_def_id
                                                LEFT JOIN shop_article_option_item saoi ON
                                                        saoi.shop_article_option_def_id = saoar.shop_article_option_def_id
                                                WHERE
                                                        saod.shop_article_option_def_type = ?
                                                        AND saoi.shop_article_option_item_value = ?';

    /**
     *
     * @param string $shopArticleOptionDefType
     * @param string $shopArticleOptionItemValue Im Fall von 'sizedef' ists die shop_article_size_def_id
     * @return array
     */
    public function getShopArticleByItemValue($shopArticleOptionDefType, $shopArticleOptionItemValue)
    {
        $parameter = new ParameterContainer([$shopArticleOptionDefType, $shopArticleOptionItemValue]);
        $stmt = $this->adapter->createStatement(self::SELECT_SHOP_ARTICLE_BY_ITEMVALUE, $parameter);
        $result = $stmt->execute();
        if ($result->valid() && $result->count() > 0) {
            return $result->getResource()->fetchAll(\PDO::FETCH_ASSOC); // ONLY PDO!!!
        }
        return [];
    }

    /**
     *
     */
    const SELECT_OPTIONITEM_ARTICLEPRICEDIFFS = 'SELECT saoi.*,
       (
           SELECT CASE
                      WHEN
                          shop_article_option_item_article_pricediff_value IS NULL THEN 0
                      ELSE shop_article_option_item_article_pricediff_value
                      END
           FROM shop_article_option_item_article_pricediff
           WHERE shop_article_option_item_id = saoi.shop_article_option_item_id
             AND shop_article_id = ?
       ) AS pricediff,
       saod.shop_article_option_def_priority
FROM shop_article_option_item saoi
         LEFT JOIN shop_article_option_def saod USING (shop_article_option_def_id)
ORDER BY saod.shop_article_option_def_priority DESC';

    /**
     *
     * @param int $shopArticleId
     * @return array key = shopArticleId_shopArticleOptionItemId; value = db row
     */
    public function getShopArticleOptionItemsAllWithArticlePricediffIdAssoc($shopArticleId)
    {
        $parameter = new ParameterContainer([$shopArticleId]);
        $stmt = $this->adapter->createStatement(self::SELECT_OPTIONITEM_ARTICLEPRICEDIFFS, $parameter);
        $result = $stmt->execute();
        $idAssoc = [];
        if ($result->valid() && $result->count() > 0) {
            do {
                $current = $result->current();
                $idAssoc[$shopArticleId . '_' . $current['shop_article_option_item_id']] = $current;
                $result->next();
            } while ($result->valid());
        }
        return $idAssoc;
    }
}
