<?php

namespace Bitkorn\Shop\Tablex\Article;

use Bitkorn\Shop\Tablex\AbstractTablex;
use Laminas\Db\Adapter\Driver\Pdo\Result;
use Laminas\Db\Adapter\ParameterContainer;

/**
 *
 * @author allapow
 */
class ShopArticleTablex extends AbstractTablex
{

    /**
     *
     */
    const SELECT_SHOP_ARTICLES_ALL = 'SELECT 
                                        sa.*,
                                        COUNT(DISTINCT sagr.shop_article_group_relation_id) AS count_group_relation,
                                        COUNT(DISTINCT sasgr.shop_article_sizegroup_ralation_id) AS count_sizegroup_relation,
                                        COUNT(DISTINCT sai.shop_article_image_id) AS count_image,
                                        COUNT(DISTINCT sar.shop_article_relation_id) AS count_relation,
                                        COUNT(DISTINCT sasi.shop_article_size_item_id) AS count_size_item,
                                        COUNT(DISTINCT sacomm.shop_article_comment_id) AS count_comment,
                                        (SELECT 
                                            SUM(shop_article_stock_amount) 
                                            FROM shop_article_stock 
                                            WHERE shop_article_id = sa.shop_article_id 
                                            GROUP BY shop_article_id) 
                                        AS sum_shop_article_stock_amount,
                                        COUNT(DISTINCT saoar.shop_article_option_article_relation_id) AS count_optiondefs
                                    FROM
                                        shop_article sa
                                            LEFT JOIN
                                        shop_article_group_relation sagr ON sagr.shop_article_id_root = sa.shop_article_id
                                            LEFT JOIN
                                        shop_article_sizegroup_ralation sasgr ON sasgr.shop_article_id_root = sa.shop_article_id
                                            LEFT JOIN
                                        shop_article_image sai USING (shop_article_id)
                                            LEFT JOIN
                                        shop_article_relation sar ON sar.shop_article_id_root = sa.shop_article_id
                                            LEFT JOIN
                                        shop_article_size_item sasi USING (shop_article_size_def_id)
                                            LEFT JOIN
                                        shop_article_comment sacomm USING (shop_article_id)
                                            LEFT JOIN
                                        shop_article_stock sas USING (shop_article_id)
                                            LEFT JOIN
                                        shop_article_option_article_relation saoar USING (shop_article_id)
                                    GROUP BY sa.shop_article_id, sas.shop_article_id';

    /**
     *
     * @return array
     */
    public function getShopArticles()
    {
        $stmt = $this->adapter->createStatement(self::SELECT_SHOP_ARTICLES_ALL);
        /** @var Result $result */
        $result = $stmt->execute();
        $resultArr = [];
        if ($result->valid() && $result->count() > 0) {
            return $result->getResource()->fetchAll(); // ONLY PDO!!!
//            do {
//                $current = $result->current();
//                $resultArr[] = $current;
//            } while ($result->next());
        }
        return $resultArr;
    }

    /**
     *
     */
    const SELECT_SHOP_ARTICLES_BY_TYPE = 'SELECT 
                                        sa.*,
                                        sasg.*,
                                        COUNT(DISTINCT sagr.shop_article_group_relation_id) AS count_group_relation,
                                        COUNT(DISTINCT sasgr.shop_article_sizegroup_ralation_id) AS count_sizegroup_relation,
                                        COUNT(DISTINCT sai.shop_article_image_id) AS count_image,
                                        COUNT(DISTINCT sar.shop_article_relation_id) AS count_relation,
                                        sasd.*,
                                        COUNT(DISTINCT sasi.shop_article_size_item_id) AS count_size_item,
                                        COUNT(DISTINCT sacomm.shop_article_comment_id) AS count_comment,
                                        SUM(DISTINCT sas.shop_article_stock_amount) AS sum_shop_article_stock_amount,
                                        COUNT(DISTINCT saoar.shop_article_option_article_relation_id) AS count_optiondefs
                                    FROM
                                        shop_article sa
                                            LEFT JOIN
                                        shop_article_group_relation sagr ON sagr.shop_article_id_root = sa.shop_article_id
                                            LEFT JOIN
                                        shop_article_sizegroup_ralation sasgr ON sasgr.shop_article_id_root = sa.shop_article_id
                                            LEFT JOIN
                                        shop_article_image sai USING (shop_article_id)
                                            LEFT JOIN
                                        shop_article_relation sar ON sar.shop_article_id_root = sa.shop_article_id
                                            LEFT JOIN
                                        shop_article_size_def sasd USING (shop_article_size_def_id)
                                            LEFT JOIN
                                        shop_article_size_group sasg ON sasg.shop_article_size_group_id = sasd.shop_article_size_group_id
                                            LEFT JOIN
                                        shop_article_size_item sasi USING (shop_article_size_def_id)
                                            LEFT JOIN
                                        shop_article_comment sacomm USING (shop_article_id)
                                            LEFT JOIN
                                        shop_article_stock sas USING (shop_article_id)
                                            LEFT JOIN
                                        shop_article_option_article_relation saoar USING (shop_article_id)
                                    WHERE sa.shop_article_type = ?
                                    GROUP BY sa.shop_article_id, sasg.shop_article_size_group_id, sasd.shop_article_size_def_id';

    /**
     *
     * @return array
     */
    public function getShopArticlesByType($articleType)
    {
        $parameter = new ParameterContainer([$articleType]);
        $stmt = $this->adapter->createStatement(self::SELECT_SHOP_ARTICLES_BY_TYPE, $parameter);
        $result = $stmt->execute();
        $resultArr = [];
        if ($result->valid() && $result->count() > 0) {
            return $result->getResource()->fetchAll(); // ONLY PDO!!!
//            do {
//                $current = $result->current();
//                $resultArr[] = $current;
//            } while ($result->next());
        }
        return $resultArr;
    }

    /**
     *
     */
    const SELECT_SHOP_ARTICLES_ACTIVE = 'SELECT 
                                        sa.*,
                                        COUNT(DISTINCT sagr.shop_article_group_relation_id) AS count_group_relation,
                                        COUNT(DISTINCT sai.shop_article_image_id) AS count_image,
                                        COUNT(DISTINCT sar.shop_article_relation_id) AS count_relation,
                                        sasd.*,
                                        COUNT(DISTINCT sasi.shop_article_size_item_id) AS count_size_item,
                                        COUNT(DISTINCT sacomm.shop_article_comment_id) AS count_comment,
                                        SUM(DISTINCT sas.shop_article_stock_amount) AS sum_shop_article_stock_amount
                                    FROM
                                        shop_article sa
                                            LEFT JOIN
                                        shop_article_group_relation sagr ON sagr.shop_article_id_root = sa.shop_article_id
                                            LEFT JOIN
                                        shop_article_image sai USING (shop_article_id)
                                            LEFT JOIN
                                        shop_article_relation sar ON sar.shop_article_id_root = sa.shop_article_id
                                            LEFT JOIN
                                        shop_article_size_def sasd USING (shop_article_size_def_id)
                                            LEFT JOIN
                                        shop_article_size_item sasi USING (shop_article_size_def_id)
                                            LEFT JOIN
                                        shop_article_comment sacomm USING (shop_article_id)
                                            LEFT JOIN
                                        shop_article_stock sas USING (shop_article_id)
                                        WHERE sa.shop_article_active = 1
                                    GROUP BY sa.shop_article_id, sasd.shop_article_size_def_id';

    /**
     * Only active articles ...for Shop-Frontend
     * @return array
     */
    public function getShopArticlesActive()
    {
        $stmt = $this->adapter->createStatement(self::SELECT_SHOP_ARTICLES_ACTIVE);
        $result = $stmt->execute();
        $resultArr = [];
        if ($result->valid() && $result->count() > 0) {
            do {
                $resultArr[] = $result->current();
                $result->next();
            } while ($result->valid());
        }
        return $resultArr;
    }

    /**
     *
     */
    const SELECT_SHOP_ARTICLES_ACTIVE_BY_CATEGORYALIAS = 'SELECT 
                                        sa.*,
                                        sac.*,
                                        COUNT(DISTINCT sagr.shop_article_group_relation_id) AS count_group_relation,
                                        COUNT(DISTINCT sai.shop_article_image_id) AS count_image,
                                        COUNT(DISTINCT sar.shop_article_relation_id) AS count_relation,
                                        sasd.*,
                                        COUNT(DISTINCT sasi.shop_article_size_item_id) AS count_size_item,
                                        COUNT(DISTINCT sacomm.shop_article_comment_id) AS count_comment,
                                        SUM(DISTINCT sas.shop_article_stock_amount) AS sum_shop_article_stock_amount
                                    FROM
                                        shop_article sa
                                            LEFT JOIN
                                        shop_article_category_relation sacr USING (shop_article_id)
                                            LEFT JOIN
                                        shop_article_category sac ON sac.shop_article_category_id = sacr.shop_article_category_id
                                            LEFT JOIN
                                        shop_article_group_relation sagr ON sagr.shop_article_id_root = sa.shop_article_id
                                            LEFT JOIN
                                        shop_article_image sai USING (shop_article_id)
                                            LEFT JOIN
                                        shop_article_relation sar ON sar.shop_article_id_root = sa.shop_article_id
                                            LEFT JOIN
                                        shop_article_size_def sasd USING (shop_article_size_def_id)
                                            LEFT JOIN
                                        shop_article_size_item sasi USING (shop_article_size_def_id)
                                            LEFT JOIN
                                        shop_article_comment sacomm USING (shop_article_id)
                                            LEFT JOIN
                                        shop_article_stock sas USING (shop_article_id)
                                        WHERE sa.shop_article_active = 1
                                        AND sac.shop_article_category_alias = ?
                                    GROUP BY sa.shop_article_id, sac.shop_article_category_id, sasd.shop_article_size_def_id';

    /**
     * Only active articles ...for Shop-Frontend
     * @return array
     */
    public function getShopArticlesActiveByCategoryAlias($categoryAlias)
    {
        $parameter = new ParameterContainer([$categoryAlias]);
        $stmt = $this->adapter->createStatement(self::SELECT_SHOP_ARTICLES_ACTIVE_BY_CATEGORYALIAS, $parameter);
        $result = $stmt->execute();
        $resultArr = [];
        if ($result->valid() && $result->count() > 0) {
            do {
                $current = $result->current();
                $resultArr[] = $current;
                $result->next();
            } while ($result->valid());
        }
        return $resultArr;
    }

    /**
     *
     */
    const SELECT_SHOP_ARTICLE_GROUP_RELATION_ARTICLES = 'SELECT 
                                                            sa.*, sagr.*
                                                        FROM
                                                            shop_article_group_relation sagr
                                                        LEFT JOIN shop_article sa
                                                        ON sa.shop_article_id = sagr.shop_article_id_slave
                                                        WHERE
                                                        sagr.shop_article_id_root = ?';

    /**
     * Get all slave-article to a root-article
     * @param int $articleId The root-article ID
     * @return array
     */
    public function getShopArticleGroupRelationArticles($articleId)
    {
        $parameter = new ParameterContainer([$articleId]);
        $stmt = $this->adapter->createStatement(self::SELECT_SHOP_ARTICLE_GROUP_RELATION_ARTICLES, $parameter);
        $result = $stmt->execute();
        $resultArr = [];
        if ($result->valid() && $result->count() > 0) {
            do {
                $current = $result->current();
                $resultArr[] = $current;
                $result->next();
            } while ($result->valid());
        }
        return $resultArr;
    }

    /**
     *
     */
    const SELECT_SHOP_ARTICLE_GROUP_RELATION_BY_ROOT_AND_SLAVE = 'SELECT * FROM shop_article_group_relation WHERE shop_article_id_root=? AND shop_article_id_slave=?';

    /**
     * Ist der Artikel noch nicht in der Liste, kommt er hinzu.
     * Ist er in der Liste wird einer hinzu addiert.
     * @param int $articleIdRoot
     * @param int $articleIdSlave
     * @return boolean
     */
    public function addOrUpdateShopArticleGroupRelation($articleIdRoot, $articleIdSlave)
    {
        $parameter = new ParameterContainer([$articleIdRoot, $articleIdSlave]);
        $stmt = $this->adapter->createStatement(self::SELECT_SHOP_ARTICLE_GROUP_RELATION_BY_ROOT_AND_SLAVE, $parameter);
        $result = $stmt->execute();
        if ($result->valid() && $result->count() == 1) {
            $current = $result->current();
            $paramUpdate = new ParameterContainer([$current['shop_article_group_relation_amount'] + 1, $current['shop_article_group_relation_id']]);
            $stmtUpdate = $this->adapter->createStatement('UPDATE shop_article_group_relation SET shop_article_group_relation_amount=? WHERE shop_article_group_relation_id=?', $paramUpdate);
            $result = $stmtUpdate->execute();
            return $result->valid();
        } elseif ($result->valid() && $result->count() == 0) {
            $stmtInsert = $this->adapter->createStatement('INSERT INTO shop_article_group_relation (shop_article_id_root,shop_article_id_slave,shop_article_group_relation_amount) VALUES (?,?,1)', $parameter);
            $result = $stmtInsert->execute();
            return $result->valid();
        }
    }


    /**
     *
     */
    const SELECT_SHOP_ARTICLE_SIZEGROUP_RELATION_ARTICLES = 'SELECT 
                                                                    sasgr.*, sa.*
                                                                FROM
                                                                    shop_article_sizegroup_ralation sasgr
                                                                LEFT JOIN  shop_article sa
                                                                ON sa.shop_article_id = sasgr.shop_article_id_slave
                                                                WHERE
                                                                sasgr.shop_article_id_root = ?';

    /**
     * Get all slave-article to a root-article
     * @param int $articleId The root-article ID
     * @return array
     */
    public function getShopArticleSizeGroupRelationArticles($articleId)
    {
        $parameter = new ParameterContainer([$articleId]);
        $stmt = $this->adapter->createStatement(self::SELECT_SHOP_ARTICLE_SIZEGROUP_RELATION_ARTICLES, $parameter);
        $result = $stmt->execute();
        $resultArr = [];
        if ($result->valid() && $result->count() > 0) {
            do {
                $current = $result->current();
                $resultArr[] = $current;
                $result->next();
            } while ($result->valid());
        }
        return $resultArr;
    }

    /**
     *
     */
    const SELECT_JOIN_SHOP_ARTICLE_FOR_SIZE_GROUP = 'SELECT sa.*, sasd.*, sasg.*
                                                        FROM shop_article sa
                                                        LEFT JOIN shop_article_size_def sasd 
                                                            USING(shop_article_size_def_id)
                                                        LEFT JOIN shop_article_size_group sasg 
                                                            ON sasg.shop_article_size_group_id = sasd.shop_article_size_group_id
                                                        WHERE NOT sa.shop_article_id = ?
                                                        AND sa.shop_article_groupable = 1
                                                        %s';

    /**
     *
     * @param int $rootArticleId
     * @param int $sizeGroupId
     * @return array
     */
    public function getShopArticleForSizeGroup(int $rootArticleId, int $sizeGroupId = 0)
    {
        if (!empty($sizeGroupId)) {
            $sqlString = sprintf(self::SELECT_JOIN_SHOP_ARTICLE_FOR_SIZE_GROUP, ' AND sasd.shop_article_size_group_id = ?');
            $paramArray = [$rootArticleId, $sizeGroupId];
        } else {
            $sqlString = sprintf(self::SELECT_JOIN_SHOP_ARTICLE_FOR_SIZE_GROUP, ' AND sasd.shop_article_size_group_id IS NOT NULL');
            $paramArray = [$rootArticleId];
        }
        $parameter = new ParameterContainer($paramArray);
        $stmt = $this->adapter->createStatement($sqlString, $parameter);
        $result = $stmt->execute();
        if ($result->valid() && $result->count() > 0) {
            return $result->getResource()->fetchAll(); // ONLY PDO!!!
        }
        return [];
    }

    /**
     *
     */
    const SELECT_SHOP_ARTICLE_SIZEGROUP_RELATION_BY_ROOT_AND_SLAVE = 'SELECT * FROM shop_article_sizegroup_ralation WHERE shop_article_id_root=? AND shop_article_id_slave=?';

    /**
     * Ist der Artikel noch nicht in der Liste, kommt er hinzu.
     * Ist er in der Liste wird einer hinzu addiert.
     * @param int $articleIdRoot
     * @param int $articleIdSlave
     * @return bool
     */
    public function addOrUpdateShopArticleSizeGroupRelation($articleIdRoot, $articleIdSlave): bool
    {
        $parameter = new ParameterContainer([$articleIdRoot, $articleIdSlave]);
        $stmt = $this->adapter->createStatement(self::SELECT_SHOP_ARTICLE_SIZEGROUP_RELATION_BY_ROOT_AND_SLAVE, $parameter);
        $result = $stmt->execute();
        if ($result->valid() && $result->count() == 1) {
            $current = $result->current();
            $paramUpdate = new ParameterContainer([$current['shop_article_sizegroup_ralation_amount'] + 1, $current['shop_article_sizegroup_ralation_id']]);
            $stmtUpdate = $this->adapter->createStatement('UPDATE shop_article_sizegroup_ralation'
                . ' SET shop_article_sizegroup_ralation_amount=? WHERE shop_article_sizegroup_ralation_id=?', $paramUpdate);
            $result = $stmtUpdate->execute();
            return $result->valid();
        } elseif ($result->valid() && $result->count() == 0) {
            $stmtInsert = $this->adapter->createStatement('INSERT INTO
                                                shop_article_sizegroup_ralation (shop_article_id_root,shop_article_id_slave,shop_article_sizegroup_ralation_amount)
                                                VALUES (?,?,1)', $parameter);
            $result = $stmtInsert->execute();
            return $result->valid();
        }
    }
}
