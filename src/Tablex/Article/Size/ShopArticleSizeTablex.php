<?php

namespace Bitkorn\Shop\Tablex\Article\Size;

use Bitkorn\Shop\Tablex\AbstractTablex;
use Laminas\Db\Adapter\ParameterContainer;

/**
 *
 * @author allapow
 */
class ShopArticleSizeTablex extends AbstractTablex
{

    const SELECT_SHOP_ARTICLE_SIZE_DEFS = 'SELECT 
                                                sasd.*,
                                                COUNT(sasi.shop_article_size_item_id) AS shop_article_size_item_count,
                                                sasg.*
                                            FROM
                                                shop_article_size_def sasd
                                            LEFT JOIN shop_article_size_item sasi 
                                                USING(shop_article_size_def_id)
                                            LEFT JOIN shop_article_size_group sasg
                                                USING(shop_article_size_group_id)
                                            GROUP BY sasd.shop_article_size_def_id
                                            ORDER BY sasd.shop_article_size_group_id ASC,
                                                        sasd.shop_article_size_def_order_priority DESC';

    /**
     *
     * @return array
     */
    public function getShopArticleSizeDefs()
    {
        $stmt = $this->adapter->createStatement(self::SELECT_SHOP_ARTICLE_SIZE_DEFS);
        $result = $stmt->execute();
        $returnArray = [];
        if ($result->valid() && $result->count() > 0) {
            do {
                $current = $result->current();
                if ($current['shop_article_size_def_id'] == 1) {
                    continue;
                }
                $returnArray[$current['shop_article_size_def_id']] = $current;
                $result->next();
            } while ($result->valid());
        }
        return $returnArray;
    }

    const SELECT_SHOP_ARTICLE_SIZE_ITEMS = 'SELECT 
                                                sasi.*, sasp.*, sasd.*, sasg.*
                                            FROM
                                                shop_article_size_item sasi
                                            LEFT JOIN shop_article_size_position sasp
                                                USING(shop_article_size_position_id)
                                            LEFT JOIN shop_article_size_def sasd 
                                                USING(shop_article_size_def_id)
                                            LEFT JOIN shop_article_size_group sasg
                                                ON sasd.shop_article_size_group_id = sasg.shop_article_size_group_id
                                            ORDER BY sasp.shop_article_size_position_order_priority DESC';

    public function getShopArticleSizeItems()
    {
        $stmt = $this->adapter->createStatement(self::SELECT_SHOP_ARTICLE_SIZE_ITEMS);
        $result = $stmt->execute();
        $returnArray = [];
        if ($result->valid() && $result->count() > 0) {
            do {
                $returnArray[] = $result->current();
                $result->next();
            } while ($result->valid());
        }
        return $returnArray;
    }

    public function getShopArticleSizeItemsGroupedAssoc()
    {
        $stmt = $this->adapter->createStatement(self::SELECT_SHOP_ARTICLE_SIZE_ITEMS);
        $result = $stmt->execute();
        $returnArray = [];
        if ($result->valid() && $result->count() > 0) {
            do {
                $current = $result->current();
                $returnArray[$current['shop_article_size_def_id']][$current['shop_article_size_item_id']] = $current;
                $result->next();
            } while ($result->valid());
        }
        return $returnArray;
    }

    const SELECT_SIZEDEF_BY_POS_AND_FROM_TO = 'SELECT
                                                    *
                                                FROM
                                                    shop_article_size_def
                                                WHERE
                                                    shop_article_size_def_id IN(
                                                        SELECT
                                                            shop_article_size_def_id
                                                        FROM
                                                            shop_article_size_item
                                                        WHERE
                                                            shop_article_size_position_id =?
                                                            AND(
                                                                shop_article_size_item_from <=?
                                                                AND shop_article_size_item_to >=?
                                                            )
                                                    )
                                                AND
                                                    shop_article_size_group_id=?
                                                ORDER BY shop_article_size_def_order_priority DESC';

    /**
     *
     * @param int $sizePositionId
     * @param float $sizeItemFrom
     * @param float $sizeItemTo
     * @param int $sizeGroupId
     * @return array Rows from db.shop_article_size_def merged with [shop_article_size_item_from => $sizeItemFrom, shop_article_size_item_to => $sizeItemTo]
     */
    public function getShopArticleSizeDefBySizePositionIdAndSizeItemFromTo(int $sizePositionId, float $sizeItemFrom, float $sizeItemTo, int $sizeGroupId = 0)
    {
        $parameter = new ParameterContainer([$sizePositionId, $sizeItemFrom, $sizeItemTo, $sizeGroupId]);
        $stmt = $this->adapter->createStatement(self::SELECT_SIZEDEF_BY_POS_AND_FROM_TO, $parameter);
        $result = $stmt->execute();
        $resultArr = [];
        if ($result->valid() && $result->count() > 0) {
            do {
                $current = $result->current();
                $resultArr[] = array_merge($current, ['shop_article_size_item_from' => $sizeItemFrom, 'shop_article_size_item_to' => $sizeItemTo]);
                $result->next();
            } while ($result->valid());
        }
        return $resultArr;
    }

    const SELECT_SIZEITEMS_BY_SIZEDEFIDS = 'SELECT
                                                sasi.*,
                                                sasd.shop_article_size_def_key
                                            FROM
                                                shop_article_size_item sasi
                                            LEFT JOIN shop_article_size_def sasd
                                                USING(shop_article_size_def_id)
                                            LEFT JOIN shop_article_size_position sasp
                                                USING(shop_article_size_position_id)
                                            WHERE
                                                sasi.shop_article_size_def_id IN(%s)
                                            AND
                                                sasd.shop_article_size_group_id=?
                                            ORDER BY sasp.shop_article_size_position_order_priority DESC, sasd.shop_article_size_def_order_priority DESC';

    /**
     *
     * @param array $sizeDefIds
     * @return array
     */
    public function getShopArticleSizeItemsBySizeDefIdsIdsExtAssoc(array $sizeDefIds, $sizeGroupId)
    {
        $parameter = new ParameterContainer([$sizeGroupId]);
        $stmt = $this->adapter->createStatement(sprintf(self::SELECT_SIZEITEMS_BY_SIZEDEFIDS, implode(',', $sizeDefIds)), $parameter);
        $result = $stmt->execute();
        $resultArr = [];
        if ($result->valid() && $result->count() > 0) {
            do {
                $current = $result->current();

                /**
                 * funzr auch
                 */
//                if (!isset($resultArr[$current['shop_article_size_position_id']])) {
//                    $resultArr[$current['shop_article_size_position_id']] = [];
//                }
//                if (!isset($resultArr[$current['shop_article_size_position_id']][$current['shop_article_size_def_id']])) {
//                    $resultArr[$current['shop_article_size_position_id']][$current['shop_article_size_def_id']] = [];
//                }
//                $resultArr[$current['shop_article_size_position_id']][$current['shop_article_size_def_id']] = [
//                    'from' => $current['shop_article_size_item_from'],
//                    'to' => $current['shop_article_size_item_to']
//                ];

                /**
                 * funzr auch
                 */
//                if (!isset($resultArr[$current['shop_article_size_position_id']])) {
//                    $resultArr[$current['shop_article_size_position_id']] = [
//                        $current['shop_article_size_def_id'] => [
//                            'from' => $current['shop_article_size_item_from'],
//                            'to' => $current['shop_article_size_item_to'],
//                            'key' => $current['shop_article_size_def_key']
//                        ]
//                    ];
//                } else {
//                    $resultArr[$current['shop_article_size_position_id']][$current['shop_article_size_def_id']] = [
//                        'from' => $current['shop_article_size_item_from'],
//                        'to' => $current['shop_article_size_item_to'],
//                        'key' => $current['shop_article_size_def_key']
//                    ];
//                }

                if (!isset($resultArr[$current['shop_article_size_position_id']])) {
                    $resultArr[$current['shop_article_size_position_id']] = [];
                }
                $resultArr[$current['shop_article_size_position_id']][] = [
                    'from' => $current['shop_article_size_item_from'],
                    'to' => $current['shop_article_size_item_to'],
                    'key' => $current['shop_article_size_def_key']
                ];
                $result->next();
            } while ($result->valid());
        }
        return $resultArr;
    }

}
