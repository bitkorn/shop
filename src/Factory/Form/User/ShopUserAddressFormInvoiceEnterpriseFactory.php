<?php

namespace Bitkorn\Shop\Factory\Form\User;

use Bitkorn\Shop\Factory\Form\User\ShopUserAddressFormFactory;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;

/**
 * Description of ShopUserAddressFormShortFactory
 *
 * @author allapow
 */
class ShopUserAddressFormInvoiceEnterpriseFactory extends ShopUserAddressFormFactory
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $form = parent::__invoke($container, $requestedName, $options);
        $form->setName('shop_user_address_invoice_enterprise');
        $form->setCustomerType('enterprise');
        $form->setBirthdayEnabled(false);
        $form->init();
        return $form;
    }

}
