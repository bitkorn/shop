<?php

namespace Bitkorn\Shop\Factory\Form\User;

use Bitkorn\Shop\Form\User\ShopUserForm;
use Bitkorn\Shop\Table\User\ShopUserGroupTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ShopUserFormFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $form = new ShopUserForm();
        $form->setShopUserGroupsIdAssoc($container->get(ShopUserGroupTable::class)->getShopUserGroupIdAssoc());
        return $form;
    }
}
