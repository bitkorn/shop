<?php

namespace Bitkorn\Shop\Factory\Form\User\Bank;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Bitkorn\Shop\Form\User\Bank\ShopUserBankForm;

/**
 * Description of ShopUserBankFormFactory
 *
 * @author allapow
 */
class ShopUserBankFormFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $form = new ShopUserBankForm();
        $form->setLogger($container->get('logger'));
        return $form;
    }

}
