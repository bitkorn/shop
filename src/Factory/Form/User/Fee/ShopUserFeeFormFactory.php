<?php

namespace Bitkorn\Shop\Factory\Form\User\Fee;

use Bitkorn\Shop\Table\User\ShopUserDataTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Bitkorn\Shop\Form\User\Fee\ShopUserFeeForm;

/**
 * Description of ShopUserFeeFormFactory
 *
 * @author allapow
 */
class ShopUserFeeFormFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $form = new ShopUserFeeForm();
        $form->setLogger($container->get('logger'));
        $form->setShopUserDataTable($container->get(ShopUserDataTable::class));
        return $form;
    }

}
