<?php

namespace Bitkorn\Shop\Factory\Form\Basket;

use Bitkorn\Shop\Form\User\ShopUserAddressForm;
use Bitkorn\Trinket\Table\IsoCountryTable;
use Bitkorn\Trinket\Table\ToolsTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Bitkorn\Shop\Form\Basket\ShopBasketAddressForm;

/**
 * Description of ShopBasketAddressFormFactory
 *
 * @author allapow
 */
class ShopBasketAddressFormFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $form = new ShopBasketAddressForm();
        /** @var IsoCountryTable $isoCountryTable */
        $isoCountryTable = $container->get(IsoCountryTable::class);
        $form->setIsoCountryIdAssoc($isoCountryTable->getIsoCountryIdAssoc());
        /** @var ToolsTable $toolsTable */
        $toolsTable = $container->get(ToolsTable::class);
        $form->setCustomerTypeIdAssoc($toolsTable->getEnumValuesPostgreSQL('enum_shop_address_customer_type'));
        return $form;
    }

}
