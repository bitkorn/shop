<?php

namespace Bitkorn\Shop\Factory\Form\Basket;

use Bitkorn\Shop\Factory\Form\Basket\ShopBasketAddressFormFactory;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;

/**
 * Description of ShopBasketAddressFormShipmentPrivateFactory
 *
 * @author allapow
 */
class ShopBasketAddressFormShipmentPrivateFactory extends ShopBasketAddressFormFactory
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $form = parent::__invoke($container, $requestedName, $options);
        $form->setName('shop_basket_address_shipment_private');
        $form->setAddressType('shipment');
        $form->setCustomerType('private');
        $form->setSalutationEnabled(false);
        $form->setBirthdayEnabled(false);
        $form->setTaxIdEnabled(false);
        $form->setCompanyNameEnabled(false);
        $form->setCompanyDepartmentEnabled(false);
        $form->setSubmitDeleteEnabled(true);
        $form->init();
        return $form;
    }

}
