<?php

namespace Bitkorn\Shop\Factory\Form\Basket;

use Bitkorn\Shop\Factory\Form\Basket\ShopBasketAddressFormFactory;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;

/**
 * Description of ShopBasketAddressFormShipmentEnterpriseFactory
 *
 * @author allapow
 */
class ShopBasketAddressFormShipmentEnterpriseFactory extends ShopBasketAddressFormFactory
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $form = parent::__invoke($container, $requestedName, $options);
        $form->setName('shop_basket_address_shipment_enterprise');
        $form->setAddressType('shipment');
        $form->setCustomerType('enterprise');
        $form->setSalutationEnabled(false);
        $form->setBirthdayEnabled(false);
        $form->setTaxIdEnabled(false);
        $form->setSubmitDeleteEnabled(true);
        $form->init();
        return $form;
    }

}
