<?php

namespace Bitkorn\Shop\Factory\Form\Article\TypeDelivery;

use Bitkorn\Shop\Form\Article\TypeDelivery\ShopArticleLengthcutDefForm;
use Bitkorn\Trinket\Table\QuantityunitTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ShopArticleLengthcutDefFormFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $form = new ShopArticleLengthcutDefForm();
        /** @var QuantityunitTable $quantityunitTable */
        $quantityunitTable = $container->get(QuantityunitTable::class);
        $form->setQuantityunitIdAssoc($quantityunitTable->getQuantityunitUuidLabelAssoc());
        return $form;
    }
}
