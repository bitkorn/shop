<?php

namespace Bitkorn\Shop\Factory\Form\Article\Category;

use Bitkorn\Shop\Form\Article\Category\ShopArticleCategoryForm;
use Bitkorn\Shop\Table\Article\ShopArticleCategoryTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ShopArticleCategoryFormFactory implements FactoryInterface
{
	/**
	 * Create an object
	 *
	 * @param ContainerInterface $container
	 * @param string $requestedName
	 * @param null|array $options
	 * @return object
	 * @throws ServiceNotFoundException if unable to resolve the service
	 * @throws ServiceNotCreatedException if an exception is raised when creating a service
	 */
	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
		$form = new ShopArticleCategoryForm();
		/** @var ShopArticleCategoryTable $shopArticleCategoryTable */
		$shopArticleCategoryTable = $container->get(ShopArticleCategoryTable::class);
        $form->setShopArticleCategoryIdAssoc($shopArticleCategoryTable->getShopArticleCategoryIdAssocSimple());
		return $form;
	}
}
