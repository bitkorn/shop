<?php

namespace Bitkorn\Shop\Factory\Form\Article;

use Bitkorn\Shop\Form\Article\ShopArticleStockForm;
use Bitkorn\Shop\Table\Vendor\ShopVendorTable;
use Bitkorn\Trinket\Table\ToolsTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ShopArticleStockFormFactory implements FactoryInterface
{
	/**
	 * Create an object
	 *
	 * @param ContainerInterface $container
	 * @param string $requestedName
	 * @param null|array $options
	 * @return object
	 * @throws ServiceNotFoundException if unable to resolve the service
	 * @throws ServiceNotCreatedException if an exception is raised when creating a service
	 */
	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
		$form = new ShopArticleStockForm();
        /** @var ShopVendorTable $shopVendorTable */
		$shopVendorTable = $container->get(ShopVendorTable::class);
        $form->setShopVendorIdAssoc($shopVendorTable->getShopVendorIdAssoc());
        /** @var ToolsTable $toolsTable */
        $toolsTable = $container->get(ToolsTable::class);
        $form->setShopArticleStockReasonTypesIdAssoc($toolsTable->getEnumValuesPostgreSQL('enum_shop_article_stock_reason_type'));
		return $form;
	}
}
