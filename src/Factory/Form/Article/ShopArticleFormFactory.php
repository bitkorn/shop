<?php

namespace Bitkorn\Shop\Factory\Form\Article;

use Bitkorn\Shop\Entity\Article\ShopArticleCategoryEntity;
use Bitkorn\Shop\Form\Article\ShopArticleForm;
use Bitkorn\Shop\Table\Article\ShopArticleCategoryTable;
use Bitkorn\Shop\Table\Article\ShopArticleClassTable;
use Bitkorn\Shop\Table\Article\ShopArticleTable;
use Bitkorn\Shop\Validator\ShopArticleSefurlValidator;
use Bitkorn\Trinket\Table\ToolsTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ShopArticleFormFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $form = new ShopArticleForm();
        /** @var ToolsTable $toolsTable */
        $toolsTable = $container->get(ToolsTable::class);
        $form->setShopArticleTypeKeyValAssoc($toolsTable->getEnumValuesPostgreSQL('enum_shop_article_type'));
        $form->setShopArticleTypeDeliveryKeyValAssoc($toolsTable->getEnumValuesPostgreSQL('enum_shop_article_type_delivery'));
        $shopArticleCategoryEntity = new ShopArticleCategoryEntity();
        $shopArticleCategoryEntity->computeHierarchicalIdAssoc($container->get(ShopArticleCategoryTable::class));
        $form->setShopArticleCategoryIdAssoc($shopArticleCategoryEntity->getHierarchicalIdAssoc());
        /** @var ShopArticleClassTable $shopArticleClassTable */
        $shopArticleClassTable = $container->get(ShopArticleClassTable::class);
        $form->setShopArticleClassIdAssoc($shopArticleClassTable->getShopArticleClasssIdAssoc());
        $shopArticleSefurlValidator = new ShopArticleSefurlValidator();
        $shopArticleSefurlValidator->setShopArticleTable($container->get(ShopArticleTable::class));
        $form->setDefaultTax($container->get('config')['bitkorn_shop']['tax_default']);
        $form->setShopArticleSefurlValidator($shopArticleSefurlValidator);
        return $form;
    }
}
