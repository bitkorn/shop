<?php

namespace Bitkorn\Shop\Factory\Form\Vendor;

use Bitkorn\Shop\Form\Vendor\ShopVendorForm;
use Bitkorn\Shop\Table\Vendor\ShopVendorGroupTable;
use Bitkorn\Trinket\Table\IsoCountryTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ShopVendorFormFactory implements FactoryInterface
{
	/**
	 * Create an object
	 *
	 * @param ContainerInterface $container
	 * @param string $requestedName
	 * @param null|array $options
	 * @return object
	 * @throws ServiceNotFoundException if unable to resolve the service
	 * @throws ServiceNotCreatedException if an exception is raised when creating a service
	 */
	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
		$form = new ShopVendorForm();
		$shopVendorGroupTable = $container->get(ShopVendorGroupTable::class);
        $form->setShopVendorGroupsIdAssoc($shopVendorGroupTable->getShopVendorGroupIdAssoc());
        /** @var IsoCountryTable $isoCountryTable */
        $isoCountryTable = $container->get(IsoCountryTable::class);
        $form->setIsoCountryIdAssoc($isoCountryTable->getIsoCountryIdAssoc());
		return $form;
	}
}
