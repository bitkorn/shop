<?php

namespace Bitkorn\Shop\Factory\Controller\Ajaxhelper;

use Bitkorn\Shop\Service\Article\ShopArticleImageService;
use Bitkorn\Shop\Service\Article\ShopArticleOptionService;
use Bitkorn\Shop\Service\Basket\BasketService;
use Bitkorn\Shop\Table\Article\Option\ShopArticleOptionItemArticleImageTable;
use Bitkorn\Shop\Tablex\Article\Option\ShopArticleOptionTablex;
use Bitkorn\Shop\Tablex\Article\ShopArticleImageTablex;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Bitkorn\Shop\Controller\Ajaxhelper\AjaxArticleOptionController;

/**
 * Description of AjaxArticleOptionControllerFactory
 *
 * @author allapow
 */
class AjaxArticleOptionControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new AjaxArticleOptionController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setBasketService($container->get(BasketService::class));
        $controller->setShopArticleOptionService($container->get(ShopArticleOptionService::class));
        $controller->setShopArticleImageService($container->get(ShopArticleImageService::class));
        return $controller;
    }
}
