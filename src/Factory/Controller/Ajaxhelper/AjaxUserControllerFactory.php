<?php

namespace Bitkorn\Shop\Factory\Controller\Ajaxhelper;

use Bitkorn\Shop\Controller\Ajaxhelper\AjaxUserController;
use Bitkorn\Shop\Form\User\ShopUserAddressForm;
use Bitkorn\Shop\Service\Basket\BasketService;
use Bitkorn\Shop\Service\User\ShopUserService;
use Bitkorn\Shop\Table\User\ShopUserAddressTable;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class AjaxUserControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new AjaxUserController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setBasketService($container->get(BasketService::class));
        $controller->setShopUserService($container->get(ShopUserService::class));
        $controller->setShopUserAddressForm($container->get(ShopUserAddressForm::class));
        $controller->setShopUserAddressTable($container->get(ShopUserAddressTable::class));
        return $controller;
    }
}
