<?php

namespace Bitkorn\Shop\Factory\Controller\Ajaxhelper;

use Bitkorn\Shop\Controller\Ajaxhelper\AjaxBasketController;
use Bitkorn\Shop\Entity\Article\ShopArticleOptionsEntity;
use Bitkorn\Shop\Service\Article\ShopArticleParam2dService;
use Bitkorn\Shop\Service\Article\ShopArticleService;
use Bitkorn\Shop\Service\Article\ShopArticleStockService;
use Bitkorn\Shop\Service\Article\TypeDelivery\ShopArticleLengthcutDefService;
use Bitkorn\Shop\Service\Basket\BasketService;
use Bitkorn\Shop\Service\Shipping\ShippingService\ShippingService;
use Bitkorn\Shop\Table\Article\Option\ShopArticleOptionArticleRelationTable;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class AjaxBasketControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new AjaxBasketController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setBasketService($container->get(BasketService::class));
        $controller->setShopArticleStockService($container->get(ShopArticleStockService::class));
        $controller->setShippingService($container->get(ShippingService::class));
        $controller->setShopArticleService($container->get(ShopArticleService::class));
        $controller->setShopArticleParam2dService($container->get(ShopArticleParam2dService::class));
        $controller->setShopArticleOptionsEntity($container->get(ShopArticleOptionsEntity::class));
        $controller->setShopArticleOptionArticleRelationTable($container->get(ShopArticleOptionArticleRelationTable::class));
        $controller->setShopArticleLengthcutDefService($container->get(ShopArticleLengthcutDefService::class));
        return $controller;
    }
}
