<?php

namespace Bitkorn\Shop\Factory\Controller\Ajaxhelper;

use Bitkorn\Shop\Controller\Ajaxhelper\AjaxAnonymusController;
use Bitkorn\Shop\Form\Basket\ShopBasketAddressForm;
use Bitkorn\Shop\Service\Basket\BasketService;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class AjaxAnonymusControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new AjaxAnonymusController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setBasketService($container->get(BasketService::class));
        $controller->setBasketAddressForm($container->get(ShopBasketAddressForm::class));
        return $controller;
    }
}
