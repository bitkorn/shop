<?php

namespace Bitkorn\Shop\Factory\Controller\Ajaxhelper\Admin;

use Bitkorn\Shop\Controller\Ajaxhelper\Admin\AjaxBasketAdminController;
use Bitkorn\Shop\Form\Basket\ShopBasketDiscountForm;
use Bitkorn\Shop\Service\Basket\BasketAdminService;
use Bitkorn\Shop\Service\Basket\BasketService;
use Bitkorn\Shop\Service\User\ShopUserService;
use Bitkorn\Shop\Table\Basket\Discount\ShopBasketDiscountTable;
use Bitkorn\Shop\Tablex\Basket\ShopBasketTablex;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class AjaxBasketAdminControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new AjaxBasketAdminController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setShopBasketDiscountForm($container->get(ShopBasketDiscountForm::class));
        $controller->setBasketAdminService($container->get(BasketAdminService::class));
        $controller->setBasketService($container->get(BasketService::class));
        $controller->setShopUserService($container->get(ShopUserService::class));
        $controller->setShopBasketDiscountTable($container->get(ShopBasketDiscountTable::class));
        $controller->setShopBasketTablex($container->get(ShopBasketTablex::class));
        return $controller;
    }
}
