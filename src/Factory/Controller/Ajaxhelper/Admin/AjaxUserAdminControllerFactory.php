<?php

namespace Bitkorn\Shop\Factory\Controller\Ajaxhelper\Admin;

use Bitkorn\Shop\Controller\Ajaxhelper\Admin\AjaxUserAdminController;
use Bitkorn\Shop\Form\User\ShopUserAddressForm;
use Bitkorn\Shop\Service\User\ShopUserService;
use Bitkorn\Shop\Table\User\ShopUserAddressTable;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class AjaxUserAdminControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new AjaxUserAdminController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setShopUserService($container->get(ShopUserService::class));
        $controller->setUserAddressForm($container->get(ShopUserAddressForm::class));
        $controller->setShopUserAddressTable($container->get(ShopUserAddressTable::class));
        return $controller;
    }
}
