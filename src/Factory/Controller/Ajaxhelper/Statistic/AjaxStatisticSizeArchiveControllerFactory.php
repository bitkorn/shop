<?php

namespace Bitkorn\Shop\Factory\Controller\Ajaxhelper\Statistic;

use Bitkorn\Shop\Table\Article\Size\ShopArticleSizeArchiveTable;
use Bitkorn\Shop\Table\Article\Size\ShopArticleSizeGroupTable;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Bitkorn\Shop\Controller\Ajaxhelper\Statistic\AjaxStatisticSizeArchiveController;

/**
 * Description of AjaxStatisticSalesFactory
 *
 * @author Torsten Brieskorn
 */
class AjaxStatisticSizeArchiveControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new AjaxStatisticSizeArchiveController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setShopArticleSizeArchiveTable($container->get(ShopArticleSizeArchiveTable::class));
        $controller->setShopArticleSizeGroupTable($container->get(ShopArticleSizeGroupTable::class));
        return $controller;
    }

}
