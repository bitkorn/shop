<?php

namespace Bitkorn\Shop\Factory\Controller\Ajaxhelper;

use Bitkorn\Shop\Controller\Ajaxhelper\AjaxArticleController;
use Bitkorn\Shop\Service\Article\ShopArticleService;
use Bitkorn\Shop\Service\Basket\BasketService;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class AjaxArticleControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new AjaxArticleController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setBasketService($container->get(BasketService::class));
        $controller->setShopArticleService($container->get(ShopArticleService::class));
        return $controller;
    }
}
