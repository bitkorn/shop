<?php

namespace Bitkorn\Shop\Factory\Controller\Admin\Basket\Claim;

use Bitkorn\Shop\Controller\Admin\Basket\Claim\ClaimAdminController;
use Bitkorn\Shop\Form\Basket\Claim\ShopBasketClaimFileForm;
use Bitkorn\Shop\Form\Basket\Claim\ShopBasketClaimForm;
use Bitkorn\Shop\Service\Basket\BasketService;
use Bitkorn\Shop\Service\Claim\ShopClaimService;
use Bitkorn\Trinket\Service\Filesystem\FolderTool;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ClaimAdminControllerFactory implements FactoryInterface
{
	/**
	 * Create an object
	 *
	 * @param ContainerInterface $container
	 * @param string $requestedName
	 * @param null|array $options
	 * @return object
	 * @throws ServiceNotFoundException if unable to resolve the service
	 * @throws ServiceNotCreatedException if an exception is raised when creating a service
	 */
	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
		$controller = new ClaimAdminController();
		$controller->setLogger($container->get('logger'));
		$controller->setUserService($container->get(UserService::class));
        $controller->setConfig($container->get('config'));
        $controller->setShopClaimService($container->get(ShopClaimService::class));
        $controller->setBasketService($container->get(BasketService::class));
        $controller->setShopBasketClaimForm($container->get(ShopBasketClaimForm::class));
        $controller->setShopBasketClaimFileForm($container->get(ShopBasketClaimFileForm::class));
        $controller->setFolderTool($container->get(FolderTool::class));
		return $controller;
	}
}
