<?php

namespace Bitkorn\Shop\Factory\Controller\Admin\Basket;

use Bitkorn\Shop\Service\Basket\BasketAdminService;
use Bitkorn\Shop\Service\Basket\BasketFinalizeService;
use Bitkorn\Shop\Service\Basket\BasketService;
use Bitkorn\Shop\Service\Document\DocumentDeliveryService;
use Bitkorn\Shop\Service\Document\DocumentInvoiceService;
use Bitkorn\Shop\Service\User\ShopUserService;
use Bitkorn\Shop\Table\Article\Option\ShopArticleOptionItemArticlePricediffTable;
use Bitkorn\Shop\Table\Article\Param2d\ShopArticleParam2dItemTable;
use Bitkorn\Shop\Table\Article\ShopArticleRatingTable;
use Bitkorn\Shop\Table\Article\TypeDelivery\ShopArticleLengthcutDefTable;
use Bitkorn\Trinket\Service\Filesystem\FolderTool;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Bitkorn\Shop\Controller\Admin\Basket\BasketAdminController;

/**
 * Description of BasketAdminControllerFactory
 *
 * @author allapow
 */
class BasketAdminControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new BasketAdminController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setShopConfig($container->get('config')['bitkorn_shop']);
        $controller->setDocumentDeliveryService($container->get(DocumentDeliveryService::class));
        $controller->setDocumentInvoiceService($container->get(DocumentInvoiceService::class));
        $controller->setBasketFinalizeService($container->get(BasketFinalizeService::class));
        $controller->setBasketAdminService($container->get(BasketAdminService::class));
        $controller->setShopUserService($container->get(ShopUserService::class));
        $controller->setBasketService($container->get(BasketService::class));
        $controller->setShopArticleOptionItemArticlePricediffTable($container->get(ShopArticleOptionItemArticlePricediffTable::class));
        $controller->setShopArticleParam2dItemTable($container->get(ShopArticleParam2dItemTable::class));
        $controller->setShopArticleLengthcutDefTable($container->get(ShopArticleLengthcutDefTable::class));
        $controller->setFolderTool($container->get(FolderTool::class));
        return $controller;
    }

}
