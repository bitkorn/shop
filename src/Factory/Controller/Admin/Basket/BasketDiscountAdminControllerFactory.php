<?php

namespace Bitkorn\Shop\Factory\Controller\Admin\Basket;

use Bitkorn\Shop\Controller\Admin\Basket\BasketDiscountAdminController;
use Bitkorn\Shop\Form\Basket\ShopBasketDiscountForm;
use Bitkorn\Shop\Service\Basket\BasketAdminService;
use Bitkorn\Shop\Service\ShopService;
use Bitkorn\Shop\Service\User\ShopUserService;
use Bitkorn\Shop\Table\Basket\Discount\ShopBasketDiscountTable;
use Bitkorn\Shop\Table\User\ShopUserDataTable;
use Bitkorn\Trinket\Service\LangService;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class BasketDiscountAdminControllerFactory implements FactoryInterface
{
	/**
	 * Create an object
	 *
	 * @param ContainerInterface $container
	 * @param string $requestedName
	 * @param null|array $options
	 * @return object
	 * @throws ServiceNotFoundException if unable to resolve the service
	 * @throws ServiceNotCreatedException if an exception is raised when creating a service
	 */
	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
		$controller = new BasketDiscountAdminController();
		$controller->setLogger($container->get('logger'));
		$controller->setUserService($container->get(UserService::class));
        $controller->setShopService($container->get(ShopService::class));
        $controller->setLangService($container->get(LangService::class));
        $controller->setShopBasketDiscountForm($container->get(ShopBasketDiscountForm::class));
        $controller->setShopBasketDiscountSalesstaffForm($container->get(ShopBasketDiscountForm::class));
        $controller->setShopBasketDiscountC2cForm($container->get(ShopBasketDiscountForm::class));
        $controller->setBasketAdminService($container->get(BasketAdminService::class));
        $controller->setShopUserService($container->get(ShopUserService::class));
        $controller->setShopBasketDiscountTable($container->get(ShopBasketDiscountTable::class));
        $controller->setShopUserDataTable($container->get(ShopUserDataTable::class));
		return $controller;
	}
}
