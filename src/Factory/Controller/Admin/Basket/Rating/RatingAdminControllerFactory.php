<?php

namespace Bitkorn\Shop\Factory\Controller\Admin\Basket\Rating;

use Bitkorn\Shop\Table\Article\ShopArticleRatingTable;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Bitkorn\Shop\Controller\Admin\Basket\Rating\RatingAdminController;

/**
 *
 * @author allapow
 */
class RatingAdminControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new RatingAdminController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setShopArticleRatingTable($container->get(ShopArticleRatingTable::class));
        return $controller;
    }

}
