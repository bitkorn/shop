<?php
/**
 * User: Torsten Brieskorn
 * Date: 13.12.17
 */

namespace Bitkorn\Shop\Factory\Controller\Admin\Backup;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Bitkorn\Shop\Controller\Admin\Backup\BackupController;

class BackupControllerFactory implements FactoryInterface
{

    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new BackupController();
        $controller->setConfig($container->get('config'));
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setAdapter($container->get('dbDefault'));
        return $controller;
    }
}
