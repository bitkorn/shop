<?php

namespace Bitkorn\Shop\Factory\Controller\Admin\Vendor;

use Bitkorn\Shop\Controller\Admin\Vendor\VendorAdminController;
use Bitkorn\Shop\Form\Vendor\ShopVendorForm;
use Bitkorn\Shop\Service\Vendor\ShopVendorService;
use Bitkorn\Shop\Service\Vendor\VendorNumberService;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class VendorAdminControllerFactory implements FactoryInterface
{
	/**
	 * Create an object
	 *
	 * @param ContainerInterface $container
	 * @param string $requestedName
	 * @param null|array $options
	 * @return object
	 * @throws ServiceNotFoundException if unable to resolve the service
	 * @throws ServiceNotCreatedException if an exception is raised when creating a service
	 */
	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
		$controller = new VendorAdminController();
		$controller->setLogger($container->get('logger'));
		$controller->setUserService($container->get(UserService::class));
        $controller->setShopVendorForm($container->get(ShopVendorForm::class));
        $controller->setShopVendorService($container->get(ShopVendorService::class));
        $controller->setVendorNumberService($container->get(VendorNumberService::class));
		return $controller;
	}
}
