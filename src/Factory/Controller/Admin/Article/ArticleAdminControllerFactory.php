<?php
namespace Bitkorn\Shop\Factory\Controller\Admin\Article;

use Bitkorn\Shop\Entity\Article\ShopArticleOptionsEntity;
use Bitkorn\Shop\Form\Article\ShopArticleForm;
use Bitkorn\Shop\Form\Article\ShopArticleStockForm;
use Bitkorn\Shop\Service\Article\ShopArticleParam2dService;
use Bitkorn\Shop\Service\Article\ShopArticleService;
use Bitkorn\Shop\Service\Article\ShopArticleStockService;
use Bitkorn\Shop\Service\Article\ShopArticleTypeDeliveryService;
use Bitkorn\Shop\Service\Basket\BasketService;
use Bitkorn\Shop\Service\Vendor\ShopVendorService;
use Bitkorn\Shop\Table\Article\ShopArticleCategoryRelationTable;
use Bitkorn\Shop\Table\Article\ShopArticleTable;
use Bitkorn\Shop\Tablex\Article\Option\ShopArticleOptionTablex;
use Bitkorn\Trinket\Service\I18n\NumberFormatService;
use Bitkorn\Trinket\Table\ToolsTable;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Bitkorn\Shop\Controller\Admin\Article\ArticleAdminController;

/**
 * Description of ArticleAdminControllerFactory
 *
 * @author allapow
 */
class ArticleAdminControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new ArticleAdminController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setNumberFormatService($container->get(NumberFormatService::class));
        $controller->setBasketService($container->get(BasketService::class));
        $controller->setShopArticleService($container->get(ShopArticleService::class));
        $controller->setShopVendorService($container->get(ShopVendorService::class));
        $controller->setShopArticleStockService($container->get(ShopArticleStockService::class));
        $controller->setShopArticleParam2dService($container->get(ShopArticleParam2dService::class));
        $controller->setShopArticleTypeDeliveryService($container->get(ShopArticleTypeDeliveryService::class));
        $controller->setShopArticleTable($container->get(ShopArticleTable::class));
        $controller->setShopArticleForm($container->get(ShopArticleForm::class));
        $controller->setShopArticleStockForm($container->get(ShopArticleStockForm::class));
        $controller->setShopArticleCategoryRelationTable($container->get(ShopArticleCategoryRelationTable::class));
        $controller->setShopArticleOptionTablex($container->get(ShopArticleOptionTablex::class));
        $controller->setShopArticleOptionsEntity($container->get(ShopArticleOptionsEntity::class));
        $controller->setToolsTable($container->get(ToolsTable::class));
        return $controller;
    }
}
