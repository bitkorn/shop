<?php

namespace Bitkorn\Shop\Factory\Controller\Admin\Article\TypeDelivery;

use Bitkorn\Shop\Controller\Admin\Article\TypeDelivery\ArticleLengthcutController;
use Bitkorn\Shop\Form\Article\TypeDelivery\ShopArticleLengthcutDefForm;
use Bitkorn\Shop\Service\Article\ShopArticleTypeDeliveryService;
use Bitkorn\Trinket\Service\I18n\NumberFormatService;
use Bitkorn\Trinket\Service\LangService;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ArticleLengthcutControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new ArticleLengthcutController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setLangService($container->get(LangService::class));
        $controller->setShopArticleTypeDeliveryService($container->get(ShopArticleTypeDeliveryService::class));
        $controller->setShopArticleLengthcutDefForm($container->get(ShopArticleLengthcutDefForm::class));
        $controller->setNumberFormatService($container->get(NumberFormatService::class));
        return $controller;
    }
}
