<?php

namespace Bitkorn\Shop\Factory\Controller\Admin\Article\Size;

use Bitkorn\Shop\Controller\Admin\Article\Size\ArticleSizeAdminController;
use Bitkorn\Shop\Form\Article\ShopArticleSizeGroupForm;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ArticleSizeAdminControllerFactory implements FactoryInterface
{
	/**
	 * Create an object
	 *
	 * @param ContainerInterface $container
	 * @param string $requestedName
	 * @param null|array $options
	 * @return object
	 * @throws ServiceNotFoundException if unable to resolve the service
	 * @throws ServiceNotCreatedException if an exception is raised when creating a service
	 */
	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
		$controller = new ArticleSizeAdminController();
		$controller->setLogger($container->get('logger'));
        $controller->setConfig($container->get('config'));
		$controller->setUserService($container->get(UserService::class));
        $controller->setShopArticleSizeGroupForm($container->get(ShopArticleSizeGroupForm::class));
		return $controller;
	}
}
