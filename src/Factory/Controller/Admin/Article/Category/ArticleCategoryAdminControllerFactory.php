<?php

namespace Bitkorn\Shop\Factory\Controller\Admin\Article\Category;

use Bitkorn\Shop\Controller\Admin\Article\Category\ArticleCategoryAdminController;
use Bitkorn\Shop\Form\Article\Category\ShopArticleCategoryForm;
use Bitkorn\Shop\Service\Article\ShopArticleService;
use Bitkorn\Shop\Table\Article\ShopArticleCategoryTable;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ArticleCategoryAdminControllerFactory implements FactoryInterface
{
	/**
	 * Create an object
	 *
	 * @param ContainerInterface $container
	 * @param string $requestedName
	 * @param null|array $options
	 * @return object
	 * @throws ServiceNotFoundException if unable to resolve the service
	 * @throws ServiceNotCreatedException if an exception is raised when creating a service
	 */
	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
		$controller = new ArticleCategoryAdminController();
		$controller->setLogger($container->get('logger'));
		$controller->setUserService($container->get(UserService::class));
        $controller->setShopArticleService($container->get(ShopArticleService::class));
        $controller->setShopArticleCategoryForm($container->get(ShopArticleCategoryForm::class));
        $controller->setShopArticleCategoryTable($container->get(ShopArticleCategoryTable::class));
		return $controller;
	}
}
