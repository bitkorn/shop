<?php

namespace Bitkorn\Shop\Factory\Controller\Admin\Article\Option;

use Bitkorn\Shop\Service\Article\ShopArticleOptionService;
use Bitkorn\Shop\Service\Article\ShopArticleService;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Bitkorn\Shop\Controller\Admin\Article\Option\ArticleOptionAdminController;

/**
 * Description of ArticleOptionAdminControllerFactory
 *
 * @author allapow
 */
class ArticleOptionAdminControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new ArticleOptionAdminController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setShopArticleService($container->get(ShopArticleService::class));
        $controller->setShopArticleOptionService($container->get(ShopArticleOptionService::class));
        return $controller;
    }

}
