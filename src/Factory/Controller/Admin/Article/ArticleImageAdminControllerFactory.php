<?php

namespace Bitkorn\Shop\Factory\Controller\Admin\Article;

use Bitkorn\Images\Form\Image\ImageUploadForm;
use Bitkorn\Images\Service\Image\ImageService;
use Bitkorn\Shop\Controller\Admin\Article\ArticleImageAdminController;
use Bitkorn\Shop\Service\Article\ShopArticleImageService;
use Bitkorn\Shop\Service\Article\ShopArticleService;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ArticleImageAdminControllerFactory implements FactoryInterface
{
	/**
	 * Create an object
	 *
	 * @param ContainerInterface $container
	 * @param string $requestedName
	 * @param null|array $options
	 * @return object
	 * @throws ServiceNotFoundException if unable to resolve the service
	 * @throws ServiceNotCreatedException if an exception is raised when creating a service
	 */
	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
		$controller = new ArticleImageAdminController();
		$controller->setLogger($container->get('logger'));
		$controller->setUserService($container->get(UserService::class));
        $controller->setImagesConfig($container->get('config')['bitkorn_images']);
        $controller->setImageUploadForm($container->get(ImageUploadForm::class));
        $controller->setImageService($container->get(ImageService::class));
        $controller->setShopArticleImageService($container->get(ShopArticleImageService::class));
        $controller->setShopArticleService($container->get(ShopArticleService::class));
		return $controller;
	}
}
