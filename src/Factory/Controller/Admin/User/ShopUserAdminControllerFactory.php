<?php

namespace Bitkorn\Shop\Factory\Controller\Admin\User;

use Bitkorn\Shop\Form\User\ShopUserAddressForm;
use Bitkorn\Shop\Form\User\ShopUserCreateForm;
use Bitkorn\Shop\Form\User\ShopUserForm;
use Bitkorn\Shop\Service\User\ShopUserNumberService;
use Bitkorn\Shop\Service\User\ShopUserService;
use Bitkorn\Shop\Table\User\ShopUserAddressTable;
use Bitkorn\Shop\Table\User\ShopUserDataNumberTable;
use Bitkorn\Shop\Table\User\ShopUserFeeTable;
use Bitkorn\Shop\Table\User\ShopUserGroupTable;
use Bitkorn\Trinket\Service\I18n\NumberFormatService;
use Bitkorn\User\Service\Registration\UserRegistrationService;
use Bitkorn\User\Service\UserService;
use Bitkorn\User\Table\User\Role\UserRoleRelationTable;
use Bitkorn\User\Table\User\Role\UserRoleTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Bitkorn\Shop\Controller\Admin\User\ShopUserAdminController;

/**
 * Description of ShopUserAdminControllerFactory
 *
 * @author allapow
 */
class ShopUserAdminControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new ShopUserAdminController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setShopUserCreateForm($container->get(ShopUserCreateForm::class));
        $controller->setShopUserForm($container->get(ShopUserForm::class));
        $controller->setShopUserAddressForm($container->get(ShopUserAddressForm::class));
        $controller->setShopUserAddressTable($container->get(ShopUserAddressTable::class));
        $controller->setShopUserService($container->get(ShopUserService::class));
        $controller->setUserRegistrationService($container->get(UserRegistrationService::class));
        $controller->setShopUserNumberService($container->get(ShopUserNumberService::class));
        $controller->setShopUserDataNumberTable($container->get(ShopUserDataNumberTable::class));
        $controller->setShopUserGroupTable($container->get(ShopUserGroupTable::class));
        $controller->setUserRoleRelationTable($container->get(UserRoleRelationTable::class));
        $controller->setUserRoleTable($container->get(UserRoleTable::class));
        $controller->setShopUserFeeTable($container->get(ShopUserFeeTable::class));
        $controller->setNumberFormatService($container->get(NumberFormatService::class));
        return $controller;
    }
}
