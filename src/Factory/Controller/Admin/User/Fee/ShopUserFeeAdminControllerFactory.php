<?php

namespace Bitkorn\Shop\Factory\Controller\Admin\User\Fee;

use Bitkorn\Shop\Form\User\Bank\ShopUserBankForm;
use Bitkorn\Shop\Form\User\Fee\ShopUserFeeForm;
use Bitkorn\Shop\Service\Basket\BasketAdminService;
use Bitkorn\Shop\Service\User\ShopUserFeeService;
use Bitkorn\Shop\Service\User\ShopUserService;
use Bitkorn\Shop\Table\User\ShopUserFeePaidTable;
use Bitkorn\Shop\Table\User\ShopUserFeeTable;
use Bitkorn\Trinket\Service\I18n\NumberFormatService;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Bitkorn\Shop\Controller\Admin\User\Fee\ShopUserFeeAdminController;

/**
 * Description of ShopUserFeeAdminControllerFactory
 *
 * @author allapow
 */
class ShopUserFeeAdminControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new ShopUserFeeAdminController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setShopUserFeeService($container->get(ShopUserFeeService::class));
        $controller->setBasketAdminService($container->get(BasketAdminService::class));
        $controller->setNumberFormatService($container->get(NumberFormatService::class));
        $controller->setShopUserFeeForm($container->get(ShopUserFeeForm::class));
        $controller->setShopUserBankForm($container->get(ShopUserBankForm::class));
        $controller->setShopUserService($container->get(ShopUserService::class));
        $controller->setShopPaypalConfig($container->get('config')['bitkorn_shop_paypal']);
        return $controller;
    }

}
