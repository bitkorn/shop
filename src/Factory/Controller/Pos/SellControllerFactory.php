<?php

namespace Bitkorn\Shop\Factory\Controller\Pos;

use Bitkorn\Shop\Controller\Pos\SellController;
use Bitkorn\Shop\Service\Article\ShopArticleService;
use Bitkorn\Shop\Service\Basket\BasketFinalizeService;
use Bitkorn\Shop\Service\Basket\BasketService;
use Bitkorn\Shop\Service\ShopService;
use Bitkorn\Shop\Service\User\ShopUserService;
use Bitkorn\Shop\Table\Basket\Discount\ShopBasketDiscountTable;
use Bitkorn\Shop\Table\Basket\ShopBasketAddressTable;
use Bitkorn\Shop\Tablex\Basket\ShopBasketTablex;
use Bitkorn\ShopPrepay\Service\PaymentNumberService;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class SellControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new SellController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setBasketService($container->get(BasketService::class));
        $controller->setShopService($container->get(ShopService::class));
        $controller->setShopUserService($container->get(ShopUserService::class));
        $controller->setPaymentNumberService($container->get(PaymentNumberService::class));
        $controller->setBasketFinalizeService($container->get(BasketFinalizeService::class));
        $controller->setShopArticleService($container->get(ShopArticleService::class));
        $controller->setShopBasketAddressFormInvoicePrivate($container->get('Bitkorn\Shop\Form\Basket\ShopBasketAddressFormInvoicePrivate'));
        $controller->setShopBasketAddressFormInvoiceEnterprise($container->get('Bitkorn\Shop\Form\Basket\ShopBasketAddressFormInvoiceEnterprise'));
        $controller->setShopBasketAddressFormShipmentPrivate($container->get('Bitkorn\Shop\Form\Basket\ShopBasketAddressFormShipmentPrivate'));
        $controller->setShopBasketAddressFormShipmentEnterprise($container->get('Bitkorn\Shop\Form\Basket\ShopBasketAddressFormShipmentEnterprise'));
        $controller->setShopBasketDiscountTable($container->get(ShopBasketDiscountTable::class));
        $controller->setShopBasketTablex($container->get(ShopBasketTablex::class));
        $controller->setShopBasketAddressTable($container->get(ShopBasketAddressTable::class));
        return $controller;
    }
}
