<?php

namespace Bitkorn\Shop\Factory\Controller\Frontend\Basket;

use Bitkorn\Mail\Mail\SimpleMailer;
use Bitkorn\Shop\Controller\Frontend\Basket\BasketController;
use Bitkorn\Shop\Form\User\RegisterForm;
use Bitkorn\Shop\Service\Article\ShopArticleService;
use Bitkorn\Shop\Service\Basket\BasketService;
use Bitkorn\Shop\Service\Document\DocumentDeliveryService;
use Bitkorn\Shop\Service\Document\DocumentInvoiceService;
use Bitkorn\Shop\Service\Document\DocumentOrderService;
use Bitkorn\Shop\Service\Shipping\ShippingService\ShippingService;
use Bitkorn\Shop\Service\ShopService;
use Bitkorn\Shop\Service\User\ShopUserService;
use Bitkorn\Shop\Table\Article\ShopArticleRatingTable;
use Bitkorn\Shop\Table\Basket\ShopBasketAddressTable;
use Bitkorn\Shop\Table\User\ShopUserAddressTable;
use Bitkorn\Trinket\Service\I18n\NumberFormatService;
use Bitkorn\User\Service\Registration\UserRegistrationService;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class BasketControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new BasketController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setBasketService($container->get(BasketService::class));
        $controller->setUserRegisterForm($container->get(RegisterForm::class));
        $controller->setUserRegistrationService($container->get(UserRegistrationService::class));
        $controller->setSimpleMailer($container->get(SimpleMailer::class));
        $controller->setShopService($container->get(ShopService::class));
        $controller->setShopUserService($container->get(ShopUserService::class));
        $controller->setShippingService($container->get(ShippingService::class));
        $controller->setDocumentInvoiceService($container->get(DocumentInvoiceService::class));
        $controller->setDocumentDeliveryService($container->get(DocumentDeliveryService::class));
        $controller->setDocumentOrderService($container->get(DocumentOrderService::class));
        $controller->setShopArticleService($container->get(ShopArticleService::class));

        $controller->setShopUserAddressTable($container->get(ShopUserAddressTable::class));
        $controller->setShopBasketAddressTable($container->get(ShopBasketAddressTable::class));

        $controller->setShopBasketAddressFormInvoicePrivate($container->get('Bitkorn\Shop\Form\Basket\ShopBasketAddressFormInvoicePrivate'));
        $controller->setShopBasketAddressFormInvoiceEnterprise($container->get('Bitkorn\Shop\Form\Basket\ShopBasketAddressFormInvoiceEnterprise'));
        $controller->setShopBasketAddressFormShipmentPrivate($container->get('Bitkorn\Shop\Form\Basket\ShopBasketAddressFormShipmentPrivate'));
        $controller->setShopBasketAddressFormShipmentEnterprise($container->get('Bitkorn\Shop\Form\Basket\ShopBasketAddressFormShipmentEnterprise'));

        $controller->setShopUserAddressFormInvoicePrivate($container->get('Bitkorn\Shop\Form\User\ShopUserAddressFormInvoicePrivate'));
        $controller->setShopUserAddressFormInvoiceEnterprise($container->get('Bitkorn\Shop\Form\User\ShopUserAddressFormInvoiceEnterprise'));
        $controller->setShopUserAddressFormShipmentPrivate($container->get('Bitkorn\Shop\Form\User\ShopUserAddressFormShipmentPrivate'));
        $controller->setShopUserAddressFormShipmentEnterprise($container->get('Bitkorn\Shop\Form\User\ShopUserAddressFormShipmentEnterprise'));

        $controller->setShopArticleRatingTable($container->get(ShopArticleRatingTable::class));
        $controller->setNumberFormatService($container->get(NumberFormatService::class));
        return $controller;
    }
}
