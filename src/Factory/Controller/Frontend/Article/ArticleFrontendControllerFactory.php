<?php

namespace Bitkorn\Shop\Factory\Controller\Frontend\Article;

use Bitkorn\Shop\Service\Article\ShopArticleImageService;
use Bitkorn\Shop\Service\Article\ShopArticleOptionService;
use Bitkorn\Shop\Service\Article\ShopArticleParam2dService;
use Bitkorn\Shop\Service\Article\ShopArticleService;
use Bitkorn\Shop\Service\Article\ShopArticleStockService;
use Bitkorn\Shop\Service\ShopService;
use Bitkorn\Shop\Table\Article\ShopArticleRatingTable;
use Bitkorn\Shop\Tablex\Article\Option\ShopArticleOptionTablex;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Bitkorn\Shop\Controller\Frontend\Article\ArticleFrontendController;

/**
 * Description of ArticleFrontendControllerFactory
 *
 * @author allapow
 */
class ArticleFrontendControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new ArticleFrontendController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setShopArticleService($container->get(ShopArticleService::class));
        $controller->setShopArticleImageService($container->get(ShopArticleImageService::class));
        $controller->setShopService($container->get(ShopService::class));
        $controller->setShopArticleOptionService($container->get(ShopArticleOptionService::class));
        $controller->setShopArticleParam2dService($container->get(ShopArticleParam2dService::class));
        $controller->setShopArticleStockService($container->get(ShopArticleStockService::class));
        $controller->setViewRender($container->get('ViewRenderer'));
        return $controller;
    }
}
