<?php

namespace Bitkorn\Shop\Factory\Controller\Frontend\Article\Size;

use Bitkorn\Shop\Controller\Frontend\Article\Size\ArticleSizeFrontendController;
use Bitkorn\Shop\Service\Article\ShopArticleSizeService;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ArticleSizeFrontendControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new ArticleSizeFrontendController();
        $controller->setLogger($container->get('logger'));
        $controller->setConfig($container->get('config'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setShopArticleSizeService($container->get(ShopArticleSizeService::class));
        return $controller;
    }
}
