<?php

namespace Bitkorn\Shop\Factory\Controller\Frontend\User;

use Bitkorn\Shop\Controller\Frontend\User\UserBackendController;
use Bitkorn\Shop\Service\Basket\BasketService;
use Bitkorn\Shop\Service\User\ShopUserService;
use Bitkorn\Shop\Table\Basket\ShopBasketEntityTable;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class UserBackendControllerFactory implements FactoryInterface
{
	/**
	 * Create an object
	 *
	 * @param ContainerInterface $container
	 * @param string $requestedName
	 * @param null|array $options
	 * @return object
	 * @throws ServiceNotFoundException if unable to resolve the service
	 * @throws ServiceNotCreatedException if an exception is raised when creating a service
	 */
	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
		$controller = new UserBackendController();
		$controller->setLogger($container->get('logger'));
		$controller->setUserService($container->get(UserService::class));
        $controller->setBasketService($container->get(BasketService::class));
        $controller->setShopUserService($container->get(ShopUserService::class));
		return $controller;
	}
}
