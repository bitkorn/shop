<?php

namespace Bitkorn\Shop\Factory\Controller\Console;

use Bitkorn\Mail\Mail\MailWrapper;
use Bitkorn\Shop\Table\Basket\ShopBasketEntityTable;
use Bitkorn\Shop\Table\Common\ShopConfigurationTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Bitkorn\Shop\Controller\Console\CronController;
use Laminas\View\HelperPluginManager;

/**
 * Description of AjaxArticleOptionControllerFactory
 *
 * @author allapow
 */
class CronControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new CronController();
        $controller->setLogger($container->get('logger'));
        $controller->setShopConfigurationTable($container->get(ShopConfigurationTable::class));
        $controller->setShopBasketEntityTable($container->get(ShopBasketEntityTable::class));
        /** @var HelperPluginManager $hpm */
        $hpm = $container->get('ViewHelperManager');
        $controller->setViewHelperManager($hpm);
        $controller->setMailWrapper($container->get(MailWrapper::class));
        return $controller;
    }
}
