<?php

namespace Bitkorn\Shop\Factory\Controller\Ajax\Article;

use Bitkorn\Shop\Controller\Ajax\Article\TypeDeliveryAjaxController;
use Bitkorn\Shop\Service\Article\ShopArticleTypeDeliveryService;
use Bitkorn\Trinket\Service\I18n\NumberFormatService;
use Bitkorn\Trinket\Service\LangService;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class TypeDeliveryAjaxControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new TypeDeliveryAjaxController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setLangService($container->get(LangService::class));
        $controller->setNumberFormatService($container->get(NumberFormatService::class));
        $controller->setShopArticleTypeDeliveryService($container->get(ShopArticleTypeDeliveryService::class));
        return $controller;
    }
}
