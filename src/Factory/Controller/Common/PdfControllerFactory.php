<?php

namespace Bitkorn\Shop\Factory\Controller\Common;

use Bitkorn\Shop\Controller\Common\PdfController;
use Bitkorn\Shop\Service\Basket\BasketAdminService;
use Bitkorn\Shop\Service\Document\DocumentDeliveryService;
use Bitkorn\Shop\Service\Document\DocumentInvoiceService;
use Bitkorn\Shop\Service\Document\DocumentOrderService;
use Bitkorn\Trinket\Service\Filesystem\FolderTool;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class PdfControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new PdfController();
        $controller->setLogger($container->get('logger'));
        $controller->setConfig($container->get('config'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setBasketAdminService($container->get(BasketAdminService::class));
        $controller->setDocumentInvoiceService($container->get(DocumentInvoiceService::class));
        $controller->setDocumentDeliveryService($container->get(DocumentDeliveryService::class));
        $controller->setDocumentOrderService($container->get(DocumentOrderService::class));
        $controller->setFolderTool($container->get(FolderTool::class));
        return $controller;
    }
}
