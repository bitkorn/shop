<?php


namespace Bitkorn\Shop\Factory\Listener;


use Bitkorn\Mail\Mail\SimpleMailer;
use Bitkorn\Shop\Listener\UserDeleteListener;
use Bitkorn\Shop\Service\User\ShopUserService;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class UserDeleteListenerFactory implements FactoryInterface
{

    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $listener = new UserDeleteListener();
        $listener->setLogger($container->get('logger'));
        /** @var UserService $userService */
        $userService = $container->get(UserService::class);
        $eventManager = $userService->getEvents();
        $listener->attach($eventManager);
        $listener->setShopUserService($container->get(ShopUserService::class));
        $listener->setSimpleMailer($container->get(SimpleMailer::class));
        return $listener;
    }
}