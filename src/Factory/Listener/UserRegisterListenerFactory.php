<?php

namespace Bitkorn\Shop\Factory\Listener;

use Bitkorn\Shop\Listener\UserRegisterListener;
use Bitkorn\Shop\Service\User\ShopUserNumberService;
use Bitkorn\Shop\Table\User\ShopUserDataNumberTable;
use Bitkorn\Shop\Table\User\ShopUserDataTable;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class UserRegisterListenerFactory implements FactoryInterface
{

    /**
     * @inheritDoc
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $listener = new UserRegisterListener();
        $listener->setLogger($container->get('logger'));
        /** @var UserService $userService */
        $userService = $container->get(UserService::class);
//        $eventManager = $userService->getEvents();
        $listener->attach($userService->getEvents());
        $listener->setShopUserNumberService($container->get(ShopUserNumberService::class));
        $listener->setShopUserDataTable($container->get(ShopUserDataTable::class));
        $listener->setShopUserDataNumberTable($container->get(ShopUserDataNumberTable::class));
        return $listener;
    }
}
