<?php

namespace Bitkorn\Shop\Factory\Table\User;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Bitkorn\Shop\Table\User\ShopUserFeePaidTable;

/**
 * Description of ShopUserFeePaidTableFactory
 *
 * @author allapow
 */
class ShopUserFeePaidTableFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $table = new ShopUserFeePaidTable();
        $table->setDbAdapter($container->get('dbDefault'));
        $table->setLogger($container->get('logger'));
        return $table;
    }
}
