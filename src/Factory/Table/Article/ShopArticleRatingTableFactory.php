<?php

namespace Bitkorn\Shop\Factory\Table\Article;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Bitkorn\Shop\Table\Article\ShopArticleRatingTable;

/**
 * Description of ShopArticleRatingTableFactory
 *
 * @author allapow
 */
class ShopArticleRatingTableFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $table = new ShopArticleRatingTable();
        $table->setDbAdapter($container->get('dbDefault'));
        $table->setLogger($container->get('logger'));
        return $table;
    }

}
