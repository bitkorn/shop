<?php

namespace Bitkorn\Shop\Factory\Entity\Article;

use Bitkorn\Shop\Table\Article\Option\ShopArticleOptionDefTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Bitkorn\Shop\Entity\Article\ShopArticleOptionsEntity;

/**
 *
 * @author allapow
 */
class ShopArticleOptionsEntityFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entity = new ShopArticleOptionsEntity();
        $entity->setLogger($container->get('logger'));
        $entity->setShopArticleOptionDefTable($container->get(ShopArticleOptionDefTable::class));
        return $entity;
    }

}
