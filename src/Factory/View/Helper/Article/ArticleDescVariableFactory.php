<?php

namespace Bitkorn\Shop\Factory\View\Helper\Article;

use Bitkorn\Shop\Table\Article\TypeDelivery\ShopArticleLengthcutDefTable;
use Bitkorn\Shop\View\Helper\Article\ArticleDescVariable;
use Bitkorn\Shop\View\Helper\Article\Option\ShopArticleOptionsFromJson;
use Bitkorn\Shop\View\Helper\Article\Param2d\ArticleParam2dItemDesc;
use Bitkorn\Shop\View\Helper\Article\TypeDelivery\ArticleLengthcutOptionsDesc;
use Bitkorn\Trinket\Service\I18n\NumberFormatService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Laminas\View\HelperPluginManager;

class ArticleDescVariableFactory implements FactoryInterface
{
	/**
	 * Create an object
	 *
	 * @param ContainerInterface $container
	 * @param string $requestedName
	 * @param null|array $options
	 * @return object
	 * @throws ServiceNotFoundException if unable to resolve the service
	 * @throws ServiceNotCreatedException if an exception is raised when creating a service
	 */
	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
		$viewHelper = new ArticleDescVariable();
		$viewHelper->setLogger($container->get('logger'));
        /** @var HelperPluginManager $vhm */
        $vhm = $container->get('ViewHelperManager');
        $viewHelper->setShopArticleOptionsFromJson($vhm->get(ShopArticleOptionsFromJson::class));
        $viewHelper->setArticleParam2dItemDesc($vhm->get(ArticleParam2dItemDesc::class));
        $viewHelper->setArticleLengthcutOptionsDesc($vhm->get(ArticleLengthcutOptionsDesc::class));
        $viewHelper->setNumberFormatService($container->get(NumberFormatService::class));
        $viewHelper->setShopArticleLengthcutDefTable($container->get(ShopArticleLengthcutDefTable::class));
		return $viewHelper;
	}
}
