<?php

namespace Bitkorn\Shop\Factory\View\Helper\Article;

use Bitkorn\Shop\Table\Article\ShopArticleTable;
use Bitkorn\Shop\View\Helper\Article\ShopArticleSefurl;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ShopArticleSefurlFactory implements FactoryInterface
{
	/**
	 * Create an object
	 *
	 * @param ContainerInterface $container
	 * @param string $requestedName
	 * @param null|array $options
	 * @return object
	 * @throws ServiceNotFoundException if unable to resolve the service
	 * @throws ServiceNotCreatedException if an exception is raised when creating a service
	 */
	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
		$viewHelper = new ShopArticleSefurl();
		$viewHelper->setShopArticleTable($container->get(ShopArticleTable::class));
		return $viewHelper;
	}
}
