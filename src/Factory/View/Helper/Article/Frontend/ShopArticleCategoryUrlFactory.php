<?php

namespace Bitkorn\Shop\Factory\View\Helper\Article\Frontend;

use Bitkorn\Shop\Table\Article\ShopArticleCategoryTable;
use Bitkorn\Shop\View\Helper\Article\Frontend\ShopArticleCategoryUrl;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Laminas\View\HelperPluginManager;

class ShopArticleCategoryUrlFactory implements FactoryInterface
{
	/**
	 * Create an object
	 *
	 * @param ContainerInterface $container
	 * @param string $requestedName
	 * @param null|array $options
	 * @return object
	 * @throws ServiceNotFoundException if unable to resolve the service
	 * @throws ServiceNotCreatedException if an exception is raised when creating a service
	 */
	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
		$viewHelper = new ShopArticleCategoryUrl();
		$viewHelper->setShopArticleCategoryTable($container->get(ShopArticleCategoryTable::class));
        /** @var HelperPluginManager $hpm */
		$hpm = $container->get('ViewHelperManager');
        $viewHelper->setUrlHelper($hpm->get('url'));
		return $viewHelper;
	}
}
