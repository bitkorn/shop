<?php

namespace Bitkorn\Shop\Factory\View\Helper\Article\Frontend;

use Bitkorn\Shop\Table\Article\ShopArticleImageTable;
use Bitkorn\Shop\View\Helper\Article\Frontend\ShopArticlePreviewImage;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ShopArticlePreviewImageFactory implements FactoryInterface
{
	/**
	 * Create an object
	 *
	 * @param ContainerInterface $container
	 * @param string $requestedName
	 * @param null|array $options
	 * @return object
	 * @throws ServiceNotFoundException if unable to resolve the service
	 * @throws ServiceNotCreatedException if an exception is raised when creating a service
	 */
	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
		$viewHelper = new ShopArticlePreviewImage();
		$viewHelper->setShopArticleImageTable($container->get(ShopArticleImageTable::class));
        $viewHelper->setBitkornImagesConfig($container->get('config')['bitkorn_images']);
		return $viewHelper;
	}
}
