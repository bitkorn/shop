<?php

namespace Bitkorn\Shop\Factory\View\Helper\Article\Frontend;

use Bitkorn\Shop\Tablex\Article\ShopArticleImageTablex;
use Bitkorn\Shop\View\Helper\Article\Frontend\ShopArticlePreview;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ShopArticlePreviewFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $viewHelper = new ShopArticlePreview();
        $viewHelper->setShopArticleImageTablex($container->get(ShopArticleImageTablex::class));
        return $viewHelper;
    }
}
