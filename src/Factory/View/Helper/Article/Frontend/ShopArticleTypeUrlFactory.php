<?php

namespace Bitkorn\Shop\Factory\View\Helper\Article\Frontend;

use Bitkorn\Shop\View\Helper\Article\Frontend\ShopArticleTypeUrl;
use Bitkorn\Trinket\Table\ToolsTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Laminas\View\HelperPluginManager;

class ShopArticleTypeUrlFactory implements FactoryInterface
{
	/**
	 * Create an object
	 *
	 * @param ContainerInterface $container
	 * @param string $requestedName
	 * @param null|array $options
	 * @return object
	 * @throws ServiceNotFoundException if unable to resolve the service
	 * @throws ServiceNotCreatedException if an exception is raised when creating a service
	 */
	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
		$viewHelper = new ShopArticleTypeUrl();
        /** @var ToolsTable $toolsTable */
        $toolsTable = $container->get(ToolsTable::class);
		$viewHelper->setShopArticleTypes($toolsTable->getEnumValuesPostgreSQL('enum_shop_article_type'));
        $viewHelper->setShopArticleDeliveryTypes($toolsTable->getEnumValuesPostgreSQL('enum_shop_article_type_delivery'));
		/** @var HelperPluginManager $vhm */
        $vhm =  $container->get('ViewHelperManager');
        $viewHelper->setUrlHelper($vhm->get('url'));
		return $viewHelper;
	}
}
