<?php

namespace Bitkorn\Shop\Factory\View\Helper\Article\Stock;

use Bitkorn\Shop\Entity\Article\ShopArticleOptionsEntity;
use Bitkorn\Shop\Table\Article\ShopArticleStockTable;
use Bitkorn\Shop\View\Helper\Article\Stock\ShopArticleStockWidget;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ShopArticleStockWidgetFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $viewHelper = new ShopArticleStockWidget();
        $viewHelper->setLogger($container->get('logger'));
        $viewHelper->setShopArticleOptionsEntity($container->get(ShopArticleOptionsEntity::class));
        $viewHelper->setShopArticleStockTable($container->get(ShopArticleStockTable::class));
        return $viewHelper;
    }
}
