<?php

namespace Bitkorn\Shop\Factory\View\Helper\Article;

use Bitkorn\Shop\View\Helper\Article\ShopArticleImageRelativeFilename;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ShopArticleImageRelativeFilenameFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $viewHelper = new ShopArticleImageRelativeFilename();
        $viewHelper->setBitkornShopConfig($container->get('config')['bitkorn_shop']);
        return $viewHelper;
    }
}
