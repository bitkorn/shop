<?php

namespace Bitkorn\Shop\Factory\View\Helper\Article\Option;

use Bitkorn\Shop\Entity\Article\ShopArticleOptionsEntity;
use Bitkorn\Shop\Tablex\Article\Option\ShopArticleOptionTablex;
use Bitkorn\Shop\View\Helper\Article\Option\ShopArticleOptionsFromJson;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ShopArticleOptionsFromJsonFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $viewHelper = new ShopArticleOptionsFromJson();
        $viewHelper->setShopArticleOptionTablex($container->get(ShopArticleOptionTablex::class));
        $viewHelper->setShopArticleOptionsEntity($container->get(ShopArticleOptionsEntity::class));
        return $viewHelper;
    }
}
