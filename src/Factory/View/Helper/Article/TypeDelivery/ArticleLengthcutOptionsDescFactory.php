<?php

namespace Bitkorn\Shop\Factory\View\Helper\Article\TypeDelivery;

use Bitkorn\Shop\Service\Article\ShopArticleParam2dService;
use Bitkorn\Shop\Service\Article\ShopArticleService;
use Bitkorn\Shop\Service\Article\TypeDelivery\ShopArticleLengthcutDefService;
use Bitkorn\Shop\View\Helper\Article\TypeDelivery\ArticleLengthcutOptionsDesc;
use Bitkorn\Trinket\Service\I18n\NumberFormatService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ArticleLengthcutOptionsDescFactory implements FactoryInterface
{
	/**
	 * Create an object
	 *
	 * @param ContainerInterface $container
	 * @param string $requestedName
	 * @param null|array $options
	 * @return object
	 * @throws ServiceNotFoundException if unable to resolve the service
	 * @throws ServiceNotCreatedException if an exception is raised when creating a service
	 */
	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
		$viewHelper = new ArticleLengthcutOptionsDesc();
		$viewHelper->setLogger($container->get('logger'));
        $viewHelper->setShopArticleLengthcutDefService($container->get(ShopArticleLengthcutDefService::class));
        $viewHelper->setNumberFormatService($container->get(NumberFormatService::class));
        $viewHelper->setShopArticleService($container->get(ShopArticleService::class));
        $viewHelper->setShopArticleParam2dService($container->get(ShopArticleParam2dService::class));
		return $viewHelper;
	}
}
