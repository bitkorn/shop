<?php

namespace Bitkorn\Shop\Factory\View\Helper\Document;

use Bitkorn\Shop\Table\Document\ShopDocumentDeliveryTable;
use Bitkorn\Shop\Table\Document\ShopDocumentInvoiceTable;
use Bitkorn\Shop\Table\Document\ShopDocumentOrderTable;
use Bitkorn\Shop\View\Helper\Document\ShopDocumentIdByBasketUnique;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ShopDocumentIdByBasketUniqueFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $viewHelper = new ShopDocumentIdByBasketUnique();
        $viewHelper->setShopDocumentInvoiceTable($container->get(ShopDocumentInvoiceTable::class));
        $viewHelper->setShopDocumentOrderTable($container->get(ShopDocumentOrderTable::class));
        $viewHelper->setShopDocumentDeliveryTable($container->get(ShopDocumentDeliveryTable::class));
        return $viewHelper;
    }
}
