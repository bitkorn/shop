<?php

namespace Bitkorn\Shop\Factory\View\Helper\Address;

use Bitkorn\Shop\Table\Basket\ShopBasketAddressTable;
use Bitkorn\Shop\Table\User\ShopUserAddressTable;
use Bitkorn\Shop\View\Helper\Address\AddressDisplay;
use Bitkorn\Trinket\Table\IsoCountryTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class AddressDisplayFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $viewHelper = new AddressDisplay();
        $viewHelper->setShopBasketAddressTable($container->get(ShopBasketAddressTable::class));
        $viewHelper->setShopUserAddressTable($container->get(ShopUserAddressTable::class));
        /** @var IsoCountryTable $isoCountryTable */
        $isoCountryTable = $container->get(IsoCountryTable::class);
        $viewHelper->setIsoCountryIdAssoc($isoCountryTable->getIsoCountryIdAssoc());
        return $viewHelper;
    }
}
