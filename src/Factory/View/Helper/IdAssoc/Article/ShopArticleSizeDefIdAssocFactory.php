<?php

namespace Bitkorn\Shop\Factory\View\Helper\IdAssoc\Article;

use Bitkorn\Shop\Table\Article\Size\ShopArticleSizeDefTable;
use Bitkorn\Shop\View\Helper\IdAssoc\Article\ShopArticleSizeDefIdAssoc;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ShopArticleSizeDefIdAssocFactory implements FactoryInterface
{
	/**
	 * Create an object
	 *
	 * @param ContainerInterface $container
	 * @param string $requestedName
	 * @param null|array $options
	 * @return object
	 * @throws ServiceNotFoundException if unable to resolve the service
	 * @throws ServiceNotCreatedException if an exception is raised when creating a service
	 */
	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
		$viewHelper = new ShopArticleSizeDefIdAssoc();
		$viewHelper->setShopArticleSizeDefTable($container->get(ShopArticleSizeDefTable::class));
		return $viewHelper;
	}
}
