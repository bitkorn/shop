<?php

namespace Bitkorn\Shop\Factory\View\Helper\Basket\Email;

use Bitkorn\Shop\Table\Common\ShopConfigurationTable;
use Bitkorn\Shop\Tablex\User\ShopUserTablex;
use Bitkorn\Shop\View\Helper\Basket\Email\ShopBasketSummary;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ShopBasketSummaryFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $viewHelper = new ShopBasketSummary();
        $viewHelper->setShopUserTablex($container->get(ShopUserTablex::class));
        $viewHelper->setShopConfigurationTable($container->get(ShopConfigurationTable::class));
        return $viewHelper;
    }
}
