<?php

namespace Bitkorn\Shop\Factory\View\Helper\Basket\Email\Confirmation;

use Bitkorn\Shop\Table\Basket\ShopBasketAddressTable;
use Bitkorn\Shop\Table\Common\ShopConfigurationTable;
use Bitkorn\Shop\Table\Document\ShopDocumentOrderTable;
use Bitkorn\Shop\Tablex\User\ShopUserTablex;
use Bitkorn\Shop\View\Helper\Basket\Email\Confirmation\ShopBasketOrdered;
use Bitkorn\Trinket\Table\IsoCountryTable;
use Bitkorn\User\Table\User\UserTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ShopBasketOrderedFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $viewHelper = new ShopBasketOrdered();
        $viewHelper->setUserTable($container->get(UserTable::class));
        $viewHelper->setShopUserTablex($container->get(ShopUserTablex::class));
        $viewHelper->setShopConfigurationTable($container->get(ShopConfigurationTable::class));
        $viewHelper->setShopBasketAddressTable($container->get(ShopBasketAddressTable::class));
        $viewHelper->setShopDocumentOrderTable($container->get(ShopDocumentOrderTable::class));
        /** @var IsoCountryTable $isoCountryTable */
        $isoCountryTable = $container->get(IsoCountryTable::class);
        $viewHelper->setIsoCountryIdAssoc($isoCountryTable->getIsoCountryIdAssoc());
        return $viewHelper;
    }
}
