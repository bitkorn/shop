<?php

namespace Bitkorn\Shop\Factory\View\Helper\Basket\Frontend;

use Bitkorn\Shop\Service\Basket\BasketService;
use Bitkorn\Shop\Service\Shipping\ShippingService\ShippingService;
use Bitkorn\Shop\Table\Article\Option\ShopArticleOptionItemArticlePricediffTable;
use Bitkorn\Shop\Table\Basket\ShopBasketEntityTable;
use Bitkorn\Shop\Table\Common\ShopConfigurationTable;
use Bitkorn\Shop\Tablex\Article\ShopArticleImageTablex;
use Bitkorn\Shop\Tablex\Basket\ShopBasketTablex;
use Bitkorn\Shop\View\Helper\Basket\Frontend\ShopBasketOverview;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ShopBasketOverviewFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $viewHelper = new ShopBasketOverview();
        $viewHelper->setLogger($container->get('logger'));
        $viewHelper->setBasketService($container->get(BasketService::class));
        $viewHelper->setShopBasketTablex($container->get(ShopBasketTablex::class));
        $viewHelper->setShopBasketEntityTable($container->get(ShopBasketEntityTable::class));
        $viewHelper->setShopArticleImageTablex($container->get(ShopArticleImageTablex::class));
        $viewHelper->setShopArticleOptionItemArticlePricediffTable($container->get(ShopArticleOptionItemArticlePricediffTable::class));
        $viewHelper->setShopConfigurationTable($container->get(ShopConfigurationTable::class));
        $viewHelper->setShippingService($container->get(ShippingService::class));
        return $viewHelper;
    }
}
