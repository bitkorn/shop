<?php

namespace Bitkorn\Shop\Factory\View\Helper\Quantityunit;

use Bitkorn\Shop\Service\Quantityunit\QuantityunitService;
use Bitkorn\Shop\View\Helper\Quantityunit\QuantityunitToOrigin;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class QuantityunitToOriginFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $viewHelper = new QuantityunitToOrigin();
        $viewHelper->setLogger($container->get('logger'));
        $viewHelper->setQuantityunitService($container->get(QuantityunitService::class));
        return $viewHelper;
    }
}
