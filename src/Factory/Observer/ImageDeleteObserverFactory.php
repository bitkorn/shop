<?php


namespace Bitkorn\Shop\Factory\Observer;


use Bitkorn\Shop\Observer\ImageDeleteObserver;
use Bitkorn\Shop\Table\Article\ShopArticleImageTable;
use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ImageDeleteObserverFactory implements FactoryInterface
{

    /**
     * @inheritDoc
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $obs = new ImageDeleteObserver();
        $obs->setLogger($container->get('logger'));
        $obs->setShopArticleImageTable($container->get(ShopArticleImageTable::class));
        return $obs;
    }
}
