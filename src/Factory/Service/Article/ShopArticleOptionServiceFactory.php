<?php

namespace Bitkorn\Shop\Factory\Service\Article;

use Bitkorn\Shop\Service\Article\ShopArticleOptionService;
use Bitkorn\Shop\Table\Article\Option\ShopArticleOptionArticleRelationTable;
use Bitkorn\Shop\Table\Article\Option\ShopArticleOptionDefTable;
use Bitkorn\Shop\Table\Article\Option\ShopArticleOptionItemArticleImageTable;
use Bitkorn\Shop\Table\Article\Option\ShopArticleOptionItemArticlePricediffTable;
use Bitkorn\Shop\Table\Article\Option\ShopArticleOptionItemTable;
use Bitkorn\Shop\Table\Article\ShopArticleTable;
use Bitkorn\Shop\Tablex\Article\Option\ShopArticleOptionTablex;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ShopArticleOptionServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new ShopArticleOptionService();
        $service->setLogger($container->get('logger'));
        $service->setShopArticleTable($container->get(ShopArticleTable::class));
        $service->setShopArticleOptionTablex($container->get(ShopArticleOptionTablex::class));
        $service->setShopArticleOptionDefTable($container->get(ShopArticleOptionDefTable::class));
        $service->setShopArticleOptionItemTable($container->get(ShopArticleOptionItemTable::class));
        $service->setShopArticleOptionItemArticleImageTable($container->get(ShopArticleOptionItemArticleImageTable::class));
        $service->setShopArticleOptionItemArticlePricediffTable($container->get(ShopArticleOptionItemArticlePricediffTable::class));
        $service->setShopArticleOptionArticleRelationTable($container->get(ShopArticleOptionArticleRelationTable::class));
        return $service;
    }
}
