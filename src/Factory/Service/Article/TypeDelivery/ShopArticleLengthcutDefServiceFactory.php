<?php

namespace Bitkorn\Shop\Factory\Service\Article\TypeDelivery;

use Bitkorn\Shop\Service\Article\TypeDelivery\ShopArticleLengthcutDefService;
use Bitkorn\Shop\Table\Article\TypeDelivery\ShopArticleLengthcutDefTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ShopArticleLengthcutDefServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new ShopArticleLengthcutDefService();
        $service->setLogger($container->get('logger'));
        $service->setShopArticleLengthcutDefTable($container->get(ShopArticleLengthcutDefTable::class));
        return $service;
    }
}
