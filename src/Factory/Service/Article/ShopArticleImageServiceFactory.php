<?php

namespace Bitkorn\Shop\Factory\Service\Article;

use Bitkorn\Images\Table\Image\ImageTable;
use Bitkorn\Shop\Service\Article\ShopArticleImageService;
use Bitkorn\Shop\Table\Article\ShopArticleImageTable;
use Bitkorn\Shop\Tablex\Article\ShopArticleImageTablex;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ShopArticleImageServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new ShopArticleImageService();
        $service->setLogger($container->get('logger'));
        $service->setImageTable($container->get(ImageTable::class));
        $service->setShopArticleImageTable($container->get(ShopArticleImageTable::class));
        $service->setShopArticleImageTablex($container->get(ShopArticleImageTablex::class));
        return $service;
    }
}
