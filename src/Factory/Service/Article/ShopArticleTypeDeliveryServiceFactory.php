<?php

namespace Bitkorn\Shop\Factory\Service\Article;

use Bitkorn\Shop\Service\Article\ShopArticleTypeDeliveryService;
use Bitkorn\Shop\Table\Article\ShopArticleTable;
use Bitkorn\Shop\Table\Article\TypeDelivery\ShopArticleLengthcutDefTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ShopArticleTypeDeliveryServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new ShopArticleTypeDeliveryService();
        $service->setLogger($container->get('logger'));
        $service->setShopArticleLengthcutDefTable($container->get(ShopArticleLengthcutDefTable::class));
        $service->setShopArticleTable($container->get(ShopArticleTable::class));
        return $service;
    }
}
