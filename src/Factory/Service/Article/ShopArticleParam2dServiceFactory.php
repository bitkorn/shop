<?php

namespace Bitkorn\Shop\Factory\Service\Article;

use Bitkorn\Shop\Service\Article\ShopArticleParam2dService;
use Bitkorn\Shop\Table\Article\Param2d\ShopArticleParam2dItemTable;
use Bitkorn\Shop\Table\Article\Param2d\ShopArticleParam2dTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ShopArticleParam2dServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new ShopArticleParam2dService();
        $service->setLogger($container->get('logger'));
        $service->setShopArticleParam2dTable($container->get(ShopArticleParam2dTable::class));
        $service->setShopArticleParam2dItemTable($container->get(ShopArticleParam2dItemTable::class));
        return $service;
    }
}
