<?php

namespace Bitkorn\Shop\Factory\Service\Article;

use Bitkorn\Shop\Service\Article\ShopArticleService;
use Bitkorn\Shop\Table\Article\ShopArticleCategoryRelationTable;
use Bitkorn\Shop\Table\Article\ShopArticleCategoryTable;
use Bitkorn\Shop\Table\Article\ShopArticleClassTable;
use Bitkorn\Shop\Table\Article\ShopArticleGroupRelationTable;
use Bitkorn\Shop\Table\Article\ShopArticleRatingTable;
use Bitkorn\Shop\Table\Article\ShopArticleSizeGroupRelationTable;
use Bitkorn\Shop\Table\Article\ShopArticleStockTable;
use Bitkorn\Shop\Table\Article\ShopArticleTable;
use Bitkorn\Shop\Table\Article\Size\ShopArticleSizeDefTable;
use Bitkorn\Shop\Tablex\Article\ShopArticleTablex;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ShopArticleServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new ShopArticleService();
        $service->setLogger($container->get('logger'));
        $service->setShopArticleTable($container->get(ShopArticleTable::class));
        $service->setShopArticleTablex($container->get(ShopArticleTablex::class));
        $service->setShopArticleCategoryRelationTable($container->get(ShopArticleCategoryRelationTable::class));
        $service->setShopArticleGroupRelationTable($container->get(ShopArticleGroupRelationTable::class));
        $service->setShopArticleSizeGroupRelationTable($container->get(ShopArticleSizeGroupRelationTable::class));
        $service->setShopArticleSizeDefTable($container->get(ShopArticleSizeDefTable::class));
        $service->setShopArticleCategoryTable($container->get(ShopArticleCategoryTable::class));
        $service->setShopArticleClassTable($container->get(ShopArticleClassTable::class));
        $service->setShopArticleStockTable($container->get(ShopArticleStockTable::class));
        $service->setShopArticleRatingTable($container->get(ShopArticleRatingTable::class));
        return $service;
    }
}
