<?php

namespace Bitkorn\Shop\Factory\Service\Article;

use Bitkorn\Shop\Service\Article\ShopArticleStockService;
use Bitkorn\Shop\Service\Article\TypeDelivery\ShopArticleLengthcutDefService;
use Bitkorn\Shop\Table\Article\ShopArticleStockTable;
use Bitkorn\Shop\Table\Article\ShopArticleTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ShopArticleStockServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new ShopArticleStockService();
        $service->setLogger($container->get('logger'));
        $service->setShopArticleStockTable($container->get(ShopArticleStockTable::class));
        $service->setShopArticleTable($container->get(ShopArticleTable::class));
        $service->setShopArticleLengthcutDefService($container->get(ShopArticleLengthcutDefService::class));
        return $service;
    }
}
