<?php

namespace Bitkorn\Shop\Factory\Service\Article;

use Bitkorn\Shop\Service\Article\ShopArticleSizeService;
use Bitkorn\Shop\Table\Article\ShopArticleSizeGroupRelationTable;
use Bitkorn\Shop\Table\Article\Size\ShopArticleSizeDefTable;
use Bitkorn\Shop\Table\Article\Size\ShopArticleSizeGroupTable;
use Bitkorn\Shop\Table\Article\Size\ShopArticleSizeItemTable;
use Bitkorn\Shop\Table\Article\Size\ShopArticleSizePositionTable;
use Bitkorn\Shop\Tablex\Article\Size\ShopArticleSizeTablex;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ShopArticleSizeServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new ShopArticleSizeService();
        $service->setLogger($container->get('logger'));
        $service->setShopArticleSizeTablex($container->get(ShopArticleSizeTablex::class));
        $service->setShopArticleSizeDefTable($container->get(ShopArticleSizeDefTable::class));
        $service->setShopArticleSizeItemTable($container->get(ShopArticleSizeItemTable::class));
        $service->setShopArticleSizeGroupTable($container->get(ShopArticleSizeGroupTable::class));
        $service->setShopArticleSizeGroupRelationTable($container->get(ShopArticleSizeGroupRelationTable::class));
        $service->setShopArticleSizePositionTable($container->get(ShopArticleSizePositionTable::class));
        return $service;
    }
}
