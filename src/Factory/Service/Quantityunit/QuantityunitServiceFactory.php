<?php

namespace Bitkorn\Shop\Factory\Service\Quantityunit;

use Bitkorn\Shop\Service\Quantityunit\QuantityunitService;
use Bitkorn\Trinket\Table\QuantityunitTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class QuantityunitServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new QuantityunitService();
        $service->setLogger($container->get('logger'));
        $service->setQuantityunitTable($container->get(QuantityunitTable::class));
        return $service;
    }
}
