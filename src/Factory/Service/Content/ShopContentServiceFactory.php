<?php

namespace Bitkorn\Shop\Factory\Service\Content;

use Bitkorn\Shop\Service\Content\ShopContentService;
use Bitkorn\Shop\Table\Content\ShopContentTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ShopContentServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new ShopContentService();
        $service->setLogger($container->get('logger'));
        $service->setShopContentTable($container->get(ShopContentTable::class));
        return $service;
    }
}
