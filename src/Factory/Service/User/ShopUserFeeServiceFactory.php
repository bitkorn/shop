<?php

namespace Bitkorn\Shop\Factory\Service\User;

use Bitkorn\Shop\Service\User\ShopUserFeeService;
use Bitkorn\Shop\Table\User\ShopUserDataTable;
use Bitkorn\Shop\Table\User\ShopUserFeePaidTable;
use Bitkorn\Shop\Table\User\ShopUserFeeTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ShopUserFeeServiceFactory implements FactoryInterface
{
	/**
	 * Create an object
	 *
	 * @param ContainerInterface $container
	 * @param string $requestedName
	 * @param null|array $options
	 * @return object
	 * @throws ServiceNotFoundException if unable to resolve the service
	 * @throws ServiceNotCreatedException if an exception is raised when creating a service
	 */
	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
		$service = new ShopUserFeeService();
		$service->setLogger($container->get('logger'));
        $service->setShopUserFeeTable($container->get(ShopUserFeeTable::class));
        $service->setShopUserFeePaidTable($container->get(ShopUserFeePaidTable::class));
        $service->setShopUserDataTable($container->get(ShopUserDataTable::class));
		return $service;
	}
}
