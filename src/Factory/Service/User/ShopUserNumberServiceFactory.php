<?php

namespace Bitkorn\Shop\Factory\Service\User;

use Bitkorn\Shop\Service\User\ShopUserNumberService;
use Bitkorn\Shop\Table\User\ShopUserDataTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ShopUserNumberServiceFactory implements FactoryInterface
{
	/**
	 * Create an object
	 *
	 * @param ContainerInterface $container
	 * @param string $requestedName
	 * @param null|array $options
	 * @return object
	 * @throws ServiceNotFoundException if unable to resolve the service
	 * @throws ServiceNotCreatedException if an exception is raised when creating a service
	 */
	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
		$service = new ShopUserNumberService();
		$service->setLogger($container->get('logger'));
        $service->setShopUserDataTable($container->get(ShopUserDataTable::class));
		return $service;
	}
}
