<?php

namespace Bitkorn\Shop\Factory\Service\User;

use Bitkorn\Shop\Service\User\ShopUserNumberService;
use Bitkorn\Shop\Service\User\ShopUserService;
use Bitkorn\Shop\Table\User\ShopUserAddressTable;
use Bitkorn\Shop\Table\User\ShopUserDataNumberTable;
use Bitkorn\Shop\Table\User\ShopUserDataTable;
use Bitkorn\Shop\Table\User\ShopUserFeePaidTable;
use Bitkorn\Shop\Table\User\ShopUserFeeTable;
use Bitkorn\Shop\Table\User\ShopUserGroupTable;
use Bitkorn\Shop\Tablex\User\ShopUserTablex;
use Bitkorn\User\Table\User\Role\UserRoleRelationTable;
use Bitkorn\User\Table\User\Role\UserRoleTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ShopUserServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new ShopUserService();
        $service->setLogger($container->get('logger'));
        $service->setShopUserNumberService($container->get(ShopUserNumberService::class));
        $service->setShopUserDataNumberTable($container->get(ShopUserDataNumberTable::class));
        $service->setShopUserDataTable($container->get(ShopUserDataTable::class));
        $service->setUserRoleTable($container->get(UserRoleTable::class));
        $service->setShopUserFeeTable($container->get(ShopUserFeeTable::class));
        $service->setShopUserFeePaidTable($container->get(ShopUserFeePaidTable::class));
        $service->setUserRoleRelationTable($container->get(UserRoleRelationTable::class));
        $service->setShopUserGroupTable($container->get(ShopUserGroupTable::class));
        $service->setShopUserTablex($container->get(ShopUserTablex::class));
        $service->setShopUserAddressTable($container->get(ShopUserAddressTable::class));
        return $service;
    }
}
