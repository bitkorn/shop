<?php

namespace Bitkorn\Shop\Factory\Service\Document;

use Bitkorn\Shop\Service\Address\ShopAddressService;
use Bitkorn\Shop\Service\Document\DocumentOrderService;
use Bitkorn\Shop\Table\Document\ShopDocumentOrderTable;
use Bitkorn\ShopTcpdf\Pdf\Order\PdfOrder;
use Bitkorn\Trinket\Service\Filesystem\FolderTool;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class DocumentOrderServiceFactory implements FactoryInterface
{
	/**
	 * Create an object
	 *
	 * @param ContainerInterface $container
	 * @param string $requestedName
	 * @param null|array $options
	 * @return object
	 * @throws ServiceNotFoundException if unable to resolve the service
	 * @throws ServiceNotCreatedException if an exception is raised when creating a service
	 */
	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
		$service = new DocumentOrderService();
		$service->setLogger($container->get('logger'));
        $config = $container->get('config');
        $service->setShopConfig($config['bitkorn_shop']);
        $service->setShopDocumentOrderTable($container->get(ShopDocumentOrderTable::class));
        $service->setAdapter($container->get('dbDefault'));
        $service->setFolderTool($container->get(FolderTool::class));
        $service->setPdfOrder($container->get(PdfOrder::class));
        $service->setShopAddressService($container->get(ShopAddressService::class));
		return $service;
	}
}
