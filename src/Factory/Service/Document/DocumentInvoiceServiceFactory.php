<?php

namespace Bitkorn\Shop\Factory\Service\Document;

use Bitkorn\Shop\Service\Address\ShopAddressService;
use Bitkorn\Shop\Service\Document\DocumentInvoiceService;
use Bitkorn\Shop\Table\Document\ShopDocumentInvoiceTable;
use Bitkorn\ShopTcpdf\Pdf\Invoice\PdfInvoice;
use Bitkorn\Trinket\Service\Filesystem\FolderTool;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class DocumentInvoiceServiceFactory implements FactoryInterface
{
	/**
	 * Create an object
	 *
	 * @param ContainerInterface $container
	 * @param string $requestedName
	 * @param null|array $options
	 * @return object
	 * @throws ServiceNotFoundException if unable to resolve the service
	 * @throws ServiceNotCreatedException if an exception is raised when creating a service
	 */
	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
		$service = new DocumentInvoiceService();
		$service->setLogger($container->get('logger'));
        $config = $container->get('config');
        $service->setShopConfig($config['bitkorn_shop']);
        $service->setShopDocumentInvoiceTable($container->get(ShopDocumentInvoiceTable::class));
        $service->setAdapter($container->get('dbDefault'));
        $service->setFolderTool($container->get(FolderTool::class));
        $service->setPdfInvoice($container->get(PdfInvoice::class));
        $service->setShopAddressService($container->get(ShopAddressService::class));
		return $service;
	}
}
