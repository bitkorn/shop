<?php

namespace Bitkorn\Shop\Factory\Service\Routes;

use Bitkorn\Shop\Service\Routes\ConfigRoutesService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Laminas\View\HelperPluginManager;

class ConfigRoutesServiceFactory implements FactoryInterface
{
	/**
	 * Create an object
	 *
	 * @param ContainerInterface $container
	 * @param string $requestedName
	 * @param null|array $options
	 * @return object
	 * @throws ServiceNotFoundException if unable to resolve the service
	 * @throws ServiceNotCreatedException if an exception is raised when creating a service
	 */
	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
		$service = new ConfigRoutesService();
		$service->setLogger($container->get('logger'));
        $service->setAdapter($container->get('dbDefault'));
        /** @var HelperPluginManager $vhm */
        $vhm = $container->get('ViewHelperManager');
        $service->setUrlHelper($vhm->get('url'));
        $config = $container->get('config');
        $configRoutes = $config['router']['routes'];
        $service->initService($configRoutes);
		return $service;
	}
}
