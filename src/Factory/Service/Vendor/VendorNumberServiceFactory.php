<?php

namespace Bitkorn\Shop\Factory\Service\Vendor;

use Bitkorn\Shop\Service\Vendor\VendorNumberService;
use Bitkorn\Shop\Table\Vendor\ShopVendorTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class VendorNumberServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new VendorNumberService();
        $service->setLogger($container->get('logger'));
        $service->setShopVendorTable($container->get(ShopVendorTable::class));
        return $service;
    }
}
