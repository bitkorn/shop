<?php

namespace Bitkorn\Shop\Factory\Service\Shipping;

use Bitkorn\Shop\Tablex\Basket\ShopBasketTablex;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Bitkorn\Shop\Service\Shipping\ShippingService\ShippingService;

/**
 * Description of ShippingServiceFactory
 *
 * @author allapow
 */
class ShippingServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new ShippingService();
        $service->setContainer($container);
        $config = $container->get('config');
        $service->registerShippingProviders($config['bitkorn_shop']['shipping_service_provider']);
        return $service;
    }

}
