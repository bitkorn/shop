<?php

namespace Bitkorn\Shop\Factory\Service;

use Bitkorn\Shop\Service\ShopService;
use Bitkorn\Shop\Table\Common\ShopConfigurationTable;
use Bitkorn\Trinket\Table\QuantityunitTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ShopServiceFactory implements FactoryInterface
{
	/**
	 * Create an object
	 *
	 * @param ContainerInterface $container
	 * @param string $requestedName
	 * @param null|array $options
	 * @return object
	 * @throws ServiceNotFoundException if unable to resolve the service
	 * @throws ServiceNotCreatedException if an exception is raised when creating a service
	 */
	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
		$service = new ShopService();
		$service->setLogger($container->get('logger'));
        $service->setShopConfigurationTable($container->get(ShopConfigurationTable::class));
        $service->setQuantityunitTable($container->get(QuantityunitTable::class));
		return $service;
	}
}
