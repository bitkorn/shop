<?php

namespace Bitkorn\Shop\Factory\Service\Claim;

use Bitkorn\Shop\Service\Claim\ShopClaimService;
use Bitkorn\Shop\Table\Basket\Claim\ShopBasketClaimTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ShopClaimServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new ShopClaimService();
        $service->setLogger($container->get('logger'));
        $service->setShopBasketClaimTable($container->get(ShopBasketClaimTable::class));
        return $service;
    }
}
