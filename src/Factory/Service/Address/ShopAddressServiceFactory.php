<?php

namespace Bitkorn\Shop\Factory\Service\Address;

use Bitkorn\Shop\Service\Address\ShopAddressService;
use Bitkorn\Shop\Table\Basket\ShopBasketAddressTable;
use Bitkorn\Shop\Table\Basket\ShopBasketEntityTable;
use Bitkorn\Shop\Table\User\ShopUserAddressTable;
use Bitkorn\Shop\Table\User\ShopUserDataTable;
use Bitkorn\Trinket\Table\IsoCountryTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ShopAddressServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new ShopAddressService();
        $service->setLogger($container->get('logger'));
        $service->setShopBasketAddressTable($container->get(ShopBasketAddressTable::class));
        $service->setShopUserAddressTable($container->get(ShopUserAddressTable::class));
        $service->setShopBasketEntityTable($container->get(ShopBasketEntityTable::class));
        $service->setShopUserDataTable($container->get(ShopUserDataTable::class));
        $service->setIsoCountryTable($container->get(IsoCountryTable::class));
        return $service;
    }
}
