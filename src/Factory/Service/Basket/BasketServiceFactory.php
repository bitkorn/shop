<?php

namespace Bitkorn\Shop\Factory\Service\Basket;

use Bitkorn\Shop\Service\Basket\BasketService;
use Bitkorn\Shop\Service\Shipping\ShippingService\ShippingService;
use Bitkorn\Shop\Table\Article\Option\ShopArticleOptionItemArticlePricediffTable;
use Bitkorn\Shop\Table\Article\Param2d\ShopArticleParam2dItemTable;
use Bitkorn\Shop\Table\Article\TypeDelivery\ShopArticleLengthcutDefTable;
use Bitkorn\Shop\Table\Basket\Discount\ShopBasketDiscountTable;
use Bitkorn\Shop\Table\Basket\ShopBasketAddressTable;
use Bitkorn\Shop\Table\Basket\ShopBasketEntityTable;
use Bitkorn\Shop\Table\Basket\ShopBasketItemTable;
use Bitkorn\Shop\Table\Basket\ShopBasketTable;
use Bitkorn\Shop\Table\Common\ShopConfigurationTable;
use Bitkorn\Shop\Table\User\ShopUserAddressTable;
use Bitkorn\Shop\Tablex\Article\ShopArticleImageTablex;
use Bitkorn\Shop\Tablex\Basket\ShopBasketEntityTablex;
use Bitkorn\Shop\Tablex\Basket\ShopBasketTablex;
use Bitkorn\Trinket\Service\Session\RedisService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class BasketServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new BasketService();
        $service->setLogger($container->get('logger'));
        $config = $container->get('config');
        $service->setSessionLifetime($config['bitkorn_user']['session_lifetime']);
        /** @var RedisService $rs */
        $rs = $container->get(RedisService::class);
        $service->setRedis($rs->getRedis());
        $service->setShopConfigurationTable($container->get(ShopConfigurationTable::class));
        $service->setShopBasketItemTable($container->get(ShopBasketItemTable::class));
        $service->setShopBasketTablex($container->get(ShopBasketTablex::class));
        $service->setShopArticleOptionItemArticlePricediffTable($container->get(ShopArticleOptionItemArticlePricediffTable::class));
        $service->setShippingService($container->get(ShippingService::class));
        $service->setShopArticleImageTablex($container->get(ShopArticleImageTablex::class));
        $service->setShopBasketDiscountTable($container->get(ShopBasketDiscountTable::class));
        $service->setShopBasketTable($container->get(ShopBasketTable::class));
        $service->setShopBasketEntityTable($container->get(ShopBasketEntityTable::class));
        $service->setShopBasketAddressTable($container->get(ShopBasketAddressTable::class));
        $service->setShopUserAddressTable($container->get(ShopUserAddressTable::class));
        $service->setShopBasketEntityTablex($container->get(ShopBasketEntityTablex::class));
        $service->setShopArticleParam2dItemTable($container->get(ShopArticleParam2dItemTable::class));
        $service->setShopArticleLengthcutDefTable($container->get(ShopArticleLengthcutDefTable::class));
        return $service;
    }
}
