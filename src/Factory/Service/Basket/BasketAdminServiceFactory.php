<?php

namespace Bitkorn\Shop\Factory\Service\Basket;

use Bitkorn\Mail\Mail\MailWrapper;
use Bitkorn\Mail\Mail\SimpleMailer;
use Bitkorn\Shop\Service\Basket\BasketAdminService;
use Bitkorn\Shop\Service\Document\DocumentDeliveryService;
use Bitkorn\Shop\Service\Document\DocumentInvoiceService;
use Bitkorn\Shop\Service\Document\DocumentOrderService;
use Bitkorn\Shop\Table\Article\ShopArticleRatingTable;
use Bitkorn\Shop\Table\Basket\Claim\ShopBasketClaimTable;
use Bitkorn\Shop\Table\Basket\Discount\ShopBasketDiscountTable;
use Bitkorn\Shop\Table\Basket\ShopBasketAddressTable;
use Bitkorn\Shop\Table\Basket\ShopBasketEntityTable;
use Bitkorn\Shop\Table\Basket\ShopBasketItemTable;
use Bitkorn\Shop\Table\Basket\ShopBasketTable;
use Bitkorn\Shop\Table\Common\ShopConfigurationTable;
use Bitkorn\Shop\Table\Document\ShopDocumentDeliveryTable;
use Bitkorn\Shop\Table\Document\ShopDocumentInvoiceTable;
use Bitkorn\Shop\Table\Document\ShopDocumentOrderTable;
use Bitkorn\Shop\Table\User\ShopUserFeePaidTable;
use Bitkorn\Shop\Tablex\Basket\ShopBasketEntityTablex;
use Bitkorn\ShopPrepay\Table\ShopPrepayNumberTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class BasketAdminServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new BasketAdminService();
        $service->setLogger($container->get('logger'));
        $config = $container->get('config');
        $service->setShopConfig($config['bitkorn_shop']);
        $service->setShopConfigurationTable($container->get(ShopConfigurationTable::class));
        $service->setShopBasketTable($container->get(ShopBasketTable::class));
        $service->setShopBasketItemTable($container->get(ShopBasketItemTable::class));
        $service->setShopBasketAddressTable($container->get(ShopBasketAddressTable::class));
        $service->setShopBasketClaimTable($container->get(ShopBasketClaimTable::class));
        $service->setShopBasketEntityTable($container->get(ShopBasketEntityTable::class));
        $service->setShopBasketEntityTablex($container->get(ShopBasketEntityTablex::class));
        $service->setMailWrapper($container->get(MailWrapper::class));
        $service->setSimpleMailer($container->get(SimpleMailer::class));
        $service->setDocumentOrderService($container->get(DocumentOrderService::class));
        $service->setDocumentInvoiceService($container->get(DocumentInvoiceService::class));
        $service->setDocumentDeliveryService($container->get(DocumentDeliveryService::class));
        $service->setShopUserFeePaidTable($container->get(ShopUserFeePaidTable::class));
        $service->setShopPrepayNumberTable($container->get(ShopPrepayNumberTable::class));
        $service->setShopArticleRatingTable($container->get(ShopArticleRatingTable::class));
        $service->setShopDocumentInvoiceTable($container->get(ShopDocumentInvoiceTable::class));
        $service->setShopDocumentOrderTable($container->get(ShopDocumentOrderTable::class));
        $service->setShopDocumentDeliveryTable($container->get(ShopDocumentDeliveryTable::class));
        $service->setShopBasketDiscountTable($container->get(ShopBasketDiscountTable::class));
        return $service;
    }
}
