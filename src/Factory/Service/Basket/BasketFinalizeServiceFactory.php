<?php

namespace Bitkorn\Shop\Factory\Service\Basket;

use Bitkorn\Mail\Mail\MailWrapper;
use Bitkorn\Mail\Mail\SimpleMailer;
use Bitkorn\Shop\Entity\Article\ShopArticleOptionsEntity;
use Bitkorn\Shop\Service\Address\ShopAddressService;
use Bitkorn\Shop\Service\Article\ShopArticleStockService;
use Bitkorn\Shop\Service\Basket\BasketFinalizeService;
use Bitkorn\Shop\Service\Basket\BasketService;
use Bitkorn\Shop\Service\Document\DocumentInvoiceService;
use Bitkorn\Shop\Service\Document\DocumentOrderService;
use Bitkorn\Shop\Service\Shipping\ShippingService\ShippingService;
use Bitkorn\Shop\Table\Article\Option\ShopArticleOptionItemArticlePricediffTable;
use Bitkorn\Shop\Table\Article\Param2d\ShopArticleParam2dItemTable;
use Bitkorn\Shop\Table\Article\TypeDelivery\ShopArticleLengthcutDefTable;
use Bitkorn\Shop\Table\Basket\ShopBasketAddressTable;
use Bitkorn\Shop\Table\Basket\ShopBasketEntityTable;
use Bitkorn\Shop\Table\Basket\ShopBasketItemTable;
use Bitkorn\Shop\Table\Basket\ShopBasketTable;
use Bitkorn\Shop\Table\Common\ShopConfigurationTable;
use Bitkorn\Shop\Table\Document\ShopDocumentInvoiceTable;
use Bitkorn\Shop\Table\User\ShopUserAddressTable;
use Bitkorn\Shop\Tablex\Basket\ShopBasketEntityTablex;
use Bitkorn\Shop\Tablex\Basket\ShopBasketTablex;
use Bitkorn\Shop\Tablex\User\ShopUserTablex;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Laminas\View\HelperPluginManager;

class BasketFinalizeServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new BasketFinalizeService();
        $service->setLogger($container->get('logger'));
        $config = $container->get('config');
        $service->setShopConfig($config['bitkorn_shop']);
        $service->setShopBasketTablex($container->get(ShopBasketTablex::class));
        $service->setShopBasketTable($container->get(ShopBasketTable::class));
        $service->setShopBasketItemTable($container->get(ShopBasketItemTable::class));
        $service->setShopBasketAddressTable($container->get(ShopBasketAddressTable::class));
        $service->setShopUserAddressTable($container->get(ShopUserAddressTable::class));
        $service->setShopBasketEntityTable($container->get(ShopBasketEntityTable::class));
        $service->setShopAddressService($container->get(ShopAddressService::class));
        $service->setShopBasketEntityTablex($container->get(ShopBasketEntityTablex::class));
        $service->setShopArticleStockService($container->get(ShopArticleStockService::class));
        $service->setShopArticleOptionItemArticlePricediffTable($container->get(ShopArticleOptionItemArticlePricediffTable::class));
        $service->setShopUserTablex($container->get(ShopUserTablex::class));
        $service->setShopConfigurationTable($container->get(ShopConfigurationTable::class));
        $service->setDocumentOrderService($container->get(DocumentOrderService::class));
        $service->setDocumentInvoiceService($container->get(DocumentInvoiceService::class));
        $service->setShopDocumentInvoiceTable($container->get(ShopDocumentInvoiceTable::class));
        $service->setMailWrapper($container->get(MailWrapper::class));
        $service->setSimpleMailer($container->get(SimpleMailer::class));
        $service->setShopArticleOptionsEntity($container->get(ShopArticleOptionsEntity::class));
        $service->setShippingService($container->get(ShippingService::class));
        $service->setBasketService($container->get(BasketService::class));
        /** @var HelperPluginManager $hpm */
        $hpm = $container->get('ViewHelperManager');
        $service->setViewHelperManager($hpm);
        $service->setShopArticleParam2dItemTable($container->get(ShopArticleParam2dItemTable::class));
        $service->setShopArticleLengthcutDefTable($container->get(ShopArticleLengthcutDefTable::class));
        return $service;
    }
}
