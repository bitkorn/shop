<?php

namespace Bitkorn\Shop\Observer;

use Bitkorn\Shop\Table\Article\ShopArticleImageTable;
use Bitkorn\Trinket\Observer\Observer\ObserverInterface;
use Laminas\Log\Logger;

/**
 * Delete all references to one image.
 *
 * @author allapow
 */
class ImageDeleteObserver implements ObserverInterface
{

    /**
     *
     * @var Logger
     */
    private $logger;

    /**
     *
     * @var ShopArticleImageTable
     */
    private $shopArticleImageTable;

    /**
     * @param array $data
     */
    public function triggered(array $data)
    {
        if (empty($data['imageId'])) {
            return;
        }
        $this->shopArticleImageTable->deleteBitkornImages(intval($data['imageId']));
    }

    /**
     *
     * @param Logger $logger
     */
    public function setLogger(Logger $logger)
    {
        $this->logger = $logger;
    }

    /**
     *
     * @param ShopArticleImageTable $shopArticleImageTable
     */
    public function setShopArticleImageTable(ShopArticleImageTable $shopArticleImageTable)
    {
        $this->shopArticleImageTable = $shopArticleImageTable;
    }

}
