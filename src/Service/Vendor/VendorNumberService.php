<?php

namespace Bitkorn\Shop\Service\Vendor;

use Bitkorn\Shop\Table\Vendor\ShopVendorTable;
use Bitkorn\Trinket\Service\AbstractService;

/**
 * Overwrite it if you want.
 *
 * @author allapow
 */
class VendorNumberService extends AbstractService
{

    const VENDOR_NO_LENGTH = 5;

    /**
     *
     * @var ShopVendorTable
     */
    protected $shopVendorTable;

    /**
     * @param ShopVendorTable $shopVendorTable
     */
    public function setShopVendorTable(ShopVendorTable $shopVendorTable): void
    {
        $this->shopVendorTable = $shopVendorTable;
    }

    /**
     * Overwrite it if you want.
     *
     * @param int $shopVendorGroupNo
     * @return array
     */
    public function getNewVendorNumber($shopVendorGroupNo)
    {
        $maxShopVendorNo = $this->shopVendorTable->getShopVendorNoMax();
        $returnArray = [];
        if (!empty($maxShopVendorNo)) {
            $returnArray['int'] = $maxShopVendorNo + 1;
            $returnArray['string'] = $shopVendorGroupNo . str_pad($maxShopVendorNo + 1, self::VENDOR_NO_LENGTH - 1, '0', STR_PAD_LEFT);
        } else {
            $returnArray['int'] = 1;
            $returnArray['string'] = $shopVendorGroupNo . str_pad(1, self::VENDOR_NO_LENGTH - 1, '0', STR_PAD_LEFT);
        }
        return $returnArray;
    }

}
