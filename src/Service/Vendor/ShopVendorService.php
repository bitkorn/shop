<?php

namespace Bitkorn\Shop\Service\Vendor;

use Bitkorn\Shop\Table\Vendor\ShopVendorGroupTable;
use Bitkorn\Shop\Table\Vendor\ShopVendorTable;
use Bitkorn\Shop\Tablex\Vendor\ShopVendorTablex;
use Bitkorn\Trinket\Service\AbstractService;

class ShopVendorService extends AbstractService
{
    /**
     * @var ShopVendorTable
     */
    protected $shopVendorTable;

    /**
     * @var ShopVendorTablex
     */
    protected $shopVendorTablex;

    /**
     * @var ShopVendorGroupTable
     */
    protected $shopVendorGroupTable;

    /**
     * @param ShopVendorTable $shopVendorTable
     */
    public function setShopVendorTable(ShopVendorTable $shopVendorTable): void
    {
        $this->shopVendorTable = $shopVendorTable;
    }

    /**
     * @param ShopVendorTablex $shopVendorTablex
     */
    public function setShopVendorTablex(ShopVendorTablex $shopVendorTablex): void
    {
        $this->shopVendorTablex = $shopVendorTablex;
    }

    /**
     * @param ShopVendorGroupTable $shopVendorGroupTable
     */
    public function setShopVendorGroupTable(ShopVendorGroupTable $shopVendorGroupTable): void
    {
        $this->shopVendorGroupTable = $shopVendorGroupTable;
    }

    public function getShopVendorNumberVarcharIdAssoc()
    {
        return $this->shopVendorTable->getShopVendorNumberVarcharIdAssoc();
    }

    public function getShopVendorIdAssoc()
    {
        return $this->shopVendorTable->getShopVendorIdAssoc();
    }

    public function getShopVendors()
    {
        return $this->shopVendorTablex->getShopVendors();
    }

    public function getShopVendorById($vendorId): array
    {
        return $this->shopVendorTable->getShopVendorById($vendorId);
    }

    public function updateShopVendor($vendorId, $formData)
    {
        return $this->shopVendorTable->updateShopVendor($vendorId, $formData);
    }

    public function getShopVendorGroupById(int $vendorGroupId)
    {
        return $this->shopVendorGroupTable->getShopVendorGroupById($vendorGroupId);
    }

    public function saveShopVendor(array $storage)
    {
        return $this->shopVendorTable->saveShopVendor($storage);
    }
}
