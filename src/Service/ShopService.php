<?php

namespace Bitkorn\Shop\Service;

use Bitkorn\Shop\Table\Common\ShopConfigurationTable;
use Bitkorn\Trinket\Service\AbstractService;
use Bitkorn\Trinket\Table\QuantityunitTable;

class ShopService extends AbstractService
{

    /**
     * @var ShopConfigurationTable
     */
    protected $shopConfigurationTable;

    /**
     * @var QuantityunitTable
     */
    protected $quantityunitTable;

    /**
     * @param ShopConfigurationTable $shopConfigurationTable
     */
    public function setShopConfigurationTable(ShopConfigurationTable $shopConfigurationTable): void
    {
        $this->shopConfigurationTable = $shopConfigurationTable;
    }

    /**
     * @param QuantityunitTable $quantityunitTable
     */
    public function setQuantityunitTable(QuantityunitTable $quantityunitTable): void
    {
        $this->quantityunitTable = $quantityunitTable;
    }

    /**
     * @param string $key
     * @return string
     */
    public function getShopConfigurationValue(string $key): string
    {
        return $this->shopConfigurationTable->getShopConfigurationValue($key);
    }

    /**
     * @return array
     */
    public function getShopConfigurationsGrouped(): array
    {
        return $this->shopConfigurationTable->getShopConfigurationsGrouped();
    }

    /**
     * @param int $shopConfigurationId
     * @param string $shopConfigurationValue
     * @return bool
     */
    public function updateShopConfigurationValue(int $shopConfigurationId, string $shopConfigurationValue): bool
    {
        return $this->shopConfigurationTable->updateShopConfigurationValue($shopConfigurationId, $shopConfigurationValue) >= 0;
    }

    /**
     * @return array
     */
    public function getQuantityunitUuidAssoc(): array
    {
        return $this->quantityunitTable->getQuantityunitUuidLabelAssoc();
    }
}
