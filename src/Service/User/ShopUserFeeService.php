<?php

namespace Bitkorn\Shop\Service\User;

use Bitkorn\Shop\Table\User\ShopUserDataTable;
use Bitkorn\Shop\Table\User\ShopUserFeePaidTable;
use Bitkorn\Shop\Table\User\ShopUserFeeTable;
use Bitkorn\Trinket\Service\AbstractService;

class ShopUserFeeService extends AbstractService
{

    /**
     *
     * @var ShopUserFeeTable
     */
    protected $shopUserFeeTable;

    /**
     *
     * @var ShopUserFeePaidTable
     */
    protected $shopUserFeePaidTable;

    /**
     * @var ShopUserDataTable
     */
    protected $shopUserDataTable;

    /**
     *
     * @param ShopUserFeeTable $shopUserFeeTable
     */
    public function setShopUserFeeTable(ShopUserFeeTable $shopUserFeeTable)
    {
        $this->shopUserFeeTable = $shopUserFeeTable;
    }

    /**
     * @return ShopUserFeeTable
     */
    public function getShopUserFeeTable(): ShopUserFeeTable
    {
        return $this->shopUserFeeTable;
    }

    /**
     * @return ShopUserFeePaidTable
     */
    public function getShopUserFeePaidTable(): ShopUserFeePaidTable
    {
        return $this->shopUserFeePaidTable;
    }

    public function setShopUserFeePaidTable(ShopUserFeePaidTable $shopUserFeePaidTable)
    {
        $this->shopUserFeePaidTable = $shopUserFeePaidTable;
    }

    /**
     * @param ShopUserDataTable $shopUserDataTable
     */
    public function setShopUserDataTable(ShopUserDataTable $shopUserDataTable): void
    {
        $this->shopUserDataTable = $shopUserDataTable;
    }

    /**
     * @param string $userUuid
     * @return array
     */
    public function getShopUserFeeByUserId(string $userUuid): array
    {
        return $this->shopUserFeeTable->getShopUserFeeByUserId($userUuid);
    }

    public function updateShopUserFeePayPalEmail($userUuid, $payPalEmail)
    {
        return $this->shopUserDataTable->updateShopUserFeePayPalEmail($userUuid, $payPalEmail);
    }

    public function existShopUserFeePaidByBasketEntityId(int $shopBasketEntityId): bool
    {
        return $this->shopUserFeePaidTable->existShopUserFeePaidByBasketEntityId($shopBasketEntityId);
    }

    public function saveNewShopUserFeePaid($userUuid, $basketEntityId, $paidSum, $timePaid, $paymentId = '', $paymentMethod = 'other'): int
    {
        return $this->shopUserFeePaidTable->saveNewShopUserFeePaid($userUuid, $basketEntityId, $paidSum, $timePaid, $paymentId, $paymentMethod);
    }

    public function getShopUserMlmChildsByUserId(string $userUuid)
    {
        return $this->shopUserFeeTable->getShopUserMlmChildsByUserId($userUuid);
    }
}
