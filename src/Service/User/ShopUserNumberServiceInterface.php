<?php

namespace Bitkorn\Shop\Service\User;

/**
 *
 * @author allapow
 */
interface ShopUserNumberServiceInterface
{

    public function generateNewShopUserNumberFromInt($shopUserGroupId, $someOngoingInt);
}
