<?php

namespace Bitkorn\Shop\Service\User;

use Bitkorn\Shop\Table\User\ShopUserDataTable;
use Bitkorn\Trinket\Service\AbstractService;
use Bitkorn\User\Entity\User\UserEntity;

/**
 * Generiert Kundennummer nach dem Schema db.shop_user_group_id und dann 4-stellig fortlaufend.
 * 4-stellig fortlaufend: am besten die ID aus Tabelle user.
 * @author allapow
 */
class ShopUserNumberService extends AbstractService implements ShopUserNumberServiceInterface
{

    /**
     *
     * @var ShopUserDataTable
     */
    protected $shopUserDataTable;

    /**
     * @param ShopUserDataTable $shopUserDataTable
     */
    public function setShopUserDataTable(ShopUserDataTable $shopUserDataTable): void
    {
        $this->shopUserDataTable = $shopUserDataTable;
    }

    public function generateNewShopUserNumberFromInt($shopUserGroupId, $someOngoingInt)
    {
        return '' . $shopUserGroupId . '-' . str_pad($someOngoingInt, 5, '0', STR_PAD_LEFT);
    }

}
