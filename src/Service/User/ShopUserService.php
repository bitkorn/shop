<?php

namespace Bitkorn\Shop\Service\User;

use Bitkorn\Shop\Entity\Basket\Address\IsoCountry;
use Bitkorn\Shop\Table\User\ShopUserAddressTable;
use Bitkorn\Shop\Table\User\ShopUserDataNumberTable;
use Bitkorn\Shop\Table\User\ShopUserDataTable;
use Bitkorn\Shop\Table\User\ShopUserFeePaidTable;
use Bitkorn\Shop\Table\User\ShopUserFeeTable;
use Bitkorn\Shop\Table\User\ShopUserGroupTable;
use Bitkorn\Shop\Tablex\User\ShopUserTablex;
use Bitkorn\Trinket\Service\AbstractService;
use Bitkorn\User\Table\User\Role\UserRoleRelationTable;
use Bitkorn\User\Table\User\Role\UserRoleTable;

class ShopUserService extends AbstractService
{

    /**
     * @var ShopUserNumberService
     */
    protected $shopUserNumberService;

    /**
     *
     * @var ShopUserDataNumberTable
     */
    protected $shopUserDataNumberTable;

    /**
     *
     * @var ShopUserDataTable
     */
    protected $shopUserDataTable;

    /**
     *
     * @var ShopUserGroupTable
     */
    protected $shopUserGroupTable;

    /**
     *
     * @var UserRoleRelationTable
     */
    protected $userRoleRelationTable;

    /**
     *
     * @var UserRoleTable
     */
    protected $userRoleTable;

    /**
     *
     * @var ShopUserFeeTable
     */
    protected $shopUserFeeTable;

    /**
     *
     * @var ShopUserFeePaidTable
     */
    protected $shopUserFeePaidTable;

    /**
     *
     * @var ShopUserTablex
     */
    protected $shopUserTablex;

    /**
     * @var ShopUserAddressTable
     */
    protected $shopUserAddressTable;

    /**
     * @param ShopUserNumberService $shopUserNumberService
     */
    public function setShopUserNumberService(ShopUserNumberService $shopUserNumberService): void
    {
        $this->shopUserNumberService = $shopUserNumberService;
    }

    /**
     * @param ShopUserDataNumberTable $shopUserDataNumberTable
     */
    public function setShopUserDataNumberTable(ShopUserDataNumberTable $shopUserDataNumberTable): void
    {
        $this->shopUserDataNumberTable = $shopUserDataNumberTable;
    }

    /**
     * @param ShopUserDataTable $shopUserDataTable
     */
    public function setShopUserDataTable(ShopUserDataTable $shopUserDataTable): void
    {
        $this->shopUserDataTable = $shopUserDataTable;
    }

    /**
     * @param ShopUserGroupTable $shopUserGroupTable
     */
    public function setShopUserGroupTable(ShopUserGroupTable $shopUserGroupTable): void
    {
        $this->shopUserGroupTable = $shopUserGroupTable;
    }

    /**
     * @param UserRoleRelationTable $userRoleRelationTable
     */
    public function setUserRoleRelationTable(UserRoleRelationTable $userRoleRelationTable): void
    {
        $this->userRoleRelationTable = $userRoleRelationTable;
    }

    /**
     * @param UserRoleTable $userRoleTable
     */
    public function setUserRoleTable(UserRoleTable $userRoleTable): void
    {
        $this->userRoleTable = $userRoleTable;
    }

    /**
     * @param ShopUserFeeTable $shopUserFeeTable
     */
    public function setShopUserFeeTable(ShopUserFeeTable $shopUserFeeTable): void
    {
        $this->shopUserFeeTable = $shopUserFeeTable;
    }

    /**
     * @param ShopUserFeePaidTable $shopUserFeePaidTable
     */
    public function setShopUserFeePaidTable(ShopUserFeePaidTable $shopUserFeePaidTable): void
    {
        $this->shopUserFeePaidTable = $shopUserFeePaidTable;
    }

    /**
     * @param ShopUserTablex $shopUserTablex
     */
    public function setShopUserTablex(ShopUserTablex $shopUserTablex): void
    {
        $this->shopUserTablex = $shopUserTablex;
    }

    /**
     * @param ShopUserAddressTable $shopUserAddressTable
     */
    public function setShopUserAddressTable(ShopUserAddressTable $shopUserAddressTable): void
    {
        $this->shopUserAddressTable = $shopUserAddressTable;
    }

    /**
     * @param array $options
     * @return array
     */
    public function searchShopUsers(array $options): array
    {
        return $this->shopUserDataTable->searchShopUsers($options);
    }

    /**
     * @return array
     */
    public function getShopUserGroupIdAssoc(): array
    {
        return $this->shopUserGroupTable->getShopUserGroupIdAssoc();
    }

    /**
     * @param string $userUuid
     * @param int $shopUserGroupId
     * @return int Last insert ID or -1.
     */
    public function createNewShopUserDataNumber(string $userUuid, int $shopUserGroupId): int
    {
        return $this->shopUserDataNumberTable->createNewShopUserDataNumber($userUuid, $shopUserGroupId, $this->shopUserNumberService);
    }

    /**
     * @param array $storage
     * @return int
     */
    public function saveNewShopUserData(array $storage): int
    {
        return $this->shopUserDataTable->saveNewShopUserData($storage);
    }

    /**
     * @param array $storage
     * @param string $userUuid
     * @return bool
     */
    public function updateShopUserDataByUserUuid(array $storage, string $userUuid): bool
    {
        return $this->shopUserDataTable->updateShopUserDataByUserUuid($storage, $userUuid) == 1;
    }

    /**
     * @param string $userUuid
     * @return array
     */
    public function getShopUserData(string $userUuid): array
    {
        return $this->shopUserTablex->getShopUserData($userUuid);
    }

    /**
     * @param string $userUuid
     * @return array
     */
    public function getUserRoleRelationsRoleIds(string $userUuid): array
    {
        return $this->userRoleRelationTable->getUserRoleRelationsRoleIds($userUuid);
    }

    public function getShopUserByRoleIdAssoc(int $shopUserRoleId, string $excludeUserUuid = ''): array
    {
        return $this->shopUserDataTable->getShopUserByRoleIdAssoc($shopUserRoleId, $excludeUserUuid);
    }

    public function getShopUserDataXByUserId(string $userUuid): array
    {
        return $this->shopUserDataTable->getShopUserDataXByUserUuid($userUuid);
    }

    /**
     * @param int $shopUserGroupId
     * @return array
     */
    public function getShopUserByGroupIdAssocX(int $shopUserGroupId): array
    {
        return $this->shopUserGroupTable->getShopUserByGroupIdAssocX($shopUserGroupId);
    }

    public function updateShopUserDataBank($userId, $bankOwnerName, $bankBankName, $bankIban, $bankBic)
    {
        return $this->shopUserDataTable->updateShopUserDataBank($userId, $bankOwnerName, $bankBankName, $bankIban, $bankBic);
    }

    /**
     * @param string $userUuid
     * @param bool $addressTypeAssoc
     * @return array address_type assoc
     */
    public function getShopUserAddressesByUserId(string $userUuid, bool $addressTypeAssoc = false): array
    {
        return $this->shopUserAddressTable->getShopUserAddressesByUserId($userUuid, $addressTypeAssoc);
    }

    /**
     * @param int $addressId
     * @return array
     */
    public function getShopUserAddressById(int $addressId): array
    {
        return $this->shopUserAddressTable->getShopUserAddressById($addressId);
    }

    /**
     * @param string $userUuid
     * @return int
     */
    public function countShopUserAddress(string $userUuid): int
    {
        return $this->shopUserAddressTable->countShopUserAddress($userUuid);
    }

    /**
     * @param array $storage
     * @return bool
     */
    public function deleteShopUserAddress(array $storage): bool
    {
        if (empty($storage['shop_user_address_id']) || empty($storage['shop_user_address_id'])) {
            return false;
        }
        return $this->shopUserAddressTable->deleteShopUserAddress($storage) >= 0;
    }

    /**
     * @param string $userUuid
     * @return bool
     * @todo delete complete:
     * - shop_basket_entity
     * - shop_basket_discount
     * - shop_basket
     * - shop_article_size_archive
     * Nicht jeden Datensatz kann man loeschen: default userUuid verwenden/setzen
     */
    public function deleteShopUserComplete(string $userUuid): bool
    {
        $this->shopUserFeePaidTable->deleteForUser($userUuid);
        $this->shopUserFeeTable->deleteForUser($userUuid);
        $this->shopUserDataNumberTable->deleteForUser($userUuid);
        $this->shopUserDataTable->deleteForUser($userUuid);
        $this->shopUserAddressTable->deleteForUser($userUuid);
        return true;
    }

    /**
     *
     * @param string $userUuid
     * @param string $addressType
     * @return IsoCountry|null
     */
    public function computeIsoCountryByUserId(string $userUuid, string $addressType)
    {
        return $this->shopUserAddressTable->computeIsoCountryByUserId($userUuid, $addressType);
    }

    /**
     * @param string $userUuid
     * @return array
     */
    public function getShopDocumentInvoicesBasketuniqueAssocByUserid(string $userUuid): array
    {
        return $this->shopUserTablex->getShopDocumentInvoicesBasketuniqueAssocByUserid($userUuid);
    }

    /**
     * @param string $userUuid
     * @return array
     */
    public function getShopDocumentOrdersBasketuniqueAssocByUserid(string $userUuid): array
    {
        return $this->shopUserTablex->getShopDocumentOrdersBasketuniqueAssocByUserid($userUuid);
    }
}
