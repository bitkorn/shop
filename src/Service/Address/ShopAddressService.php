<?php

namespace Bitkorn\Shop\Service\Address;

use Bitkorn\Shop\Entity\Address\ShopAddressEntity;
use Bitkorn\Shop\Entity\Basket\XshopBasketEntity;
use Bitkorn\Shop\Table\Basket\ShopBasketAddressTable;
use Bitkorn\Shop\Table\Basket\ShopBasketEntityTable;
use Bitkorn\Shop\Table\User\ShopUserAddressTable;
use Bitkorn\Shop\Table\User\ShopUserDataTable;
use Bitkorn\Trinket\Service\AbstractService;
use Bitkorn\Trinket\Table\IsoCountryTable;

class ShopAddressService extends AbstractService
{
    const ADDRESS_ACCESS_TYPE_BASKET = 'basket';
    const ADDRESS_ACCESS_TYPE_USER = 'user';

    protected ShopBasketAddressTable $shopBasketAddressTable;
    protected ShopUserAddressTable $shopUserAddressTable;
    protected ShopBasketEntityTable $shopBasketEntityTable;
    protected ShopUserDataTable $shopUserDataTable;
    protected IsoCountryTable $isoCountryTable;

    public function setShopBasketAddressTable(ShopBasketAddressTable $shopBasketAddressTable): void
    {
        $this->shopBasketAddressTable = $shopBasketAddressTable;
    }

    public function setShopUserAddressTable(ShopUserAddressTable $shopUserAddressTable): void
    {
        $this->shopUserAddressTable = $shopUserAddressTable;
    }

    public function setShopBasketEntityTable(ShopBasketEntityTable $shopBasketEntityTable): void
    {
        $this->shopBasketEntityTable = $shopBasketEntityTable;
    }

    public function setShopUserDataTable(ShopUserDataTable $shopUserDataTable): void
    {
        $this->shopUserDataTable = $shopUserDataTable;
    }

    public function setIsoCountryTable(IsoCountryTable $isoCountryTable): void
    {
        $this->isoCountryTable = $isoCountryTable;
    }

    public function getAddressById(string $addressAccessType, int $addressId): ?ShopAddressEntity
    {
        $entity = new ShopAddressEntity();
        switch ($addressAccessType) {
            case self::ADDRESS_ACCESS_TYPE_BASKET:
                $success = $entity->exchangeArrayFromDatabaseForBasket($this->shopBasketAddressTable->getShopBasketAddressById($addressId));
                break;
            case self::ADDRESS_ACCESS_TYPE_USER:
                $success = $entity->exchangeArrayFromDatabaseForUser($this->shopUserAddressTable->getShopUserAddressById($addressId));
                break;
            default:
                $success = false;
        }
        if ($success) {
            return $entity;
        }
        return null;
    }

    public function getShopAddressEntity(XshopBasketEntity $basketEntity, string $preferredShopAddressType = 'shipment'): ShopAddressEntity
    {
        $shopAddress = new ShopAddressEntity();
        if (!empty($userUuid = $basketEntity->getUserUuid())) {
            $shopUserData = $this->shopUserAddressTable->getShopUserAddressesByUserId($userUuid, true);
            $shopAddress->exchangeArrayFromDatabaseForUser($shopUserData[$preferredShopAddressType] ?? $shopUserData['invoice']);
        } else if (!empty($basketAddresses = $this->shopBasketAddressTable->getShopBasketAddressesByBasketUnique($basketEntity->getBasketUnique()))) {
            $shopAddress->exchangeArrayFromDatabaseForBasket($basketAddresses[$preferredShopAddressType] ?? $basketAddresses['invoice']);
        }
        return $shopAddress;
    }

    /**
     * After a basket is ordered, there are only db.shop_basket_entity an none XshopBasketEntity is created.
     *
     * @param string $basketUnique
     * @param string $preferredShopAddressType
     * @return ShopAddressEntity
     */
    public function getShopAddressEntityOrdered(string $basketUnique, string $preferredShopAddressType = 'shipment'): ShopAddressEntity
    {
        if (empty($basketEntity = $this->shopBasketEntityTable->getShopBasketEntitiesByBasketUnique($basketUnique, 1)[0])) {
            throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() No shop_basket_entity for basketUnique: ' . $basketUnique);
        }
        $shopAddress = new ShopAddressEntity();
        $isoCountryIdAssoc = $this->isoCountryTable->getIsoCountryIdAssocRow();
        if (!empty($basketEntity['user_uuid']) && !empty($user = $this->shopUserDataTable->getShopUserDataXByUserUuid($basketEntity['user_uuid']))) {
            $basketEntity['address_email'] = $user['user_email'];
            if($preferredShopAddressType == 'shipment' && !empty($basketEntity['shop_user_address_shipment_id'])) {
                $basketEntity['country_name'] = $isoCountryIdAssoc[$basketEntity['shop_user_address_shipment_country_id']]['country_name'];
                $basketEntity['country_iso'] = $isoCountryIdAssoc[$basketEntity['shop_user_address_shipment_country_id']]['country_iso'];
                $shopAddress->exchangeArrayForBasketEntityUserShipment($basketEntity);
            } else {
                $basketEntity['country_name'] = $isoCountryIdAssoc[$basketEntity['shop_user_address_invoice_country_id']]['country_name'];
                $basketEntity['country_iso'] = $isoCountryIdAssoc[$basketEntity['shop_user_address_invoice_country_id']]['country_iso'];
                $shopAddress->exchangeArrayForBasketEntityUserInvoice($basketEntity);
            }
        } else if (!empty($basketEntity['user_uuid'])) {
            throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() Not empty user_uuid, but no UserData: ' . $basketUnique);
        } else {
            if($preferredShopAddressType == 'shipment' && !empty($basketEntity['shop_basket_address_shipment_id'])) {
                $basketEntity['country_name'] = $isoCountryIdAssoc[$basketEntity['shop_basket_address_shipment_country_id']]['country_name'];
                $basketEntity['country_iso'] = $isoCountryIdAssoc[$basketEntity['shop_basket_address_shipment_country_id']]['country_iso'];
                $shopAddress->exchangeArrayForBasketEntityBasketShipment($basketEntity);
            } else {
                $basketEntity['country_name'] = $isoCountryIdAssoc[$basketEntity['shop_basket_address_invoice_country_id']]['country_name'];
                $basketEntity['country_iso'] = $isoCountryIdAssoc[$basketEntity['shop_basket_address_invoice_country_id']]['country_iso'];
                $shopAddress->exchangeArrayForBasketEntityBasketInvoice($basketEntity);
            }
        }
        return $shopAddress;
    }
}
