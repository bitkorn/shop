<?php

namespace Bitkorn\Shop\Service\Basket;

use Bitkorn\Mail\Mail\MailWrapper;
use Bitkorn\Mail\Mail\SimpleMailer;
use Bitkorn\Shop\Entity\Address\ShopAddressEntity;
use Bitkorn\Shop\Entity\Article\ShopArticleOptionsEntity;
use Bitkorn\Shop\Service\Address\ShopAddressService;
use Bitkorn\Shop\Service\Article\ShopArticleStockService;
use Bitkorn\Shop\Service\Document\DocumentInvoiceService;
use Bitkorn\Shop\Service\Document\DocumentOrderService;
use Bitkorn\Shop\Service\Shipping\ShippingServiceInterface;
use Bitkorn\Shop\Table\Article\Option\ShopArticleOptionItemArticlePricediffTable;
use Bitkorn\Shop\Table\Article\Param2d\ShopArticleParam2dItemTable;
use Bitkorn\Shop\Table\Article\ShopArticleStockTable;
use Bitkorn\Shop\Table\Article\TypeDelivery\ShopArticleLengthcutDefTable;
use Bitkorn\Shop\Table\Basket\ShopBasketAddressTable;
use Bitkorn\Shop\Table\Basket\ShopBasketEntityTable;
use Bitkorn\Shop\Table\Basket\ShopBasketItemTable;
use Bitkorn\Shop\Table\Basket\ShopBasketTable;
use Bitkorn\Shop\Table\Common\ShopConfigurationTable;
use Bitkorn\Shop\Table\Document\ShopDocumentInvoiceTable;
use Bitkorn\Shop\Table\User\ShopUserAddressTable;
use Bitkorn\Shop\Tablex\Basket\ShopBasketEntityTablex;
use Bitkorn\Shop\Tablex\Basket\ShopBasketTablex;
use Bitkorn\Shop\Tablex\User\ShopUserTablex;
use Bitkorn\Shop\Tools\Basket\BasketUnique;
use Bitkorn\Shop\View\Helper\Basket\Email\Rating\RatingReminder;
use Bitkorn\Trinket\Service\AbstractService;
use Bitkorn\Shop\Entity\Basket\XshopBasketEntity;
use Laminas\View\HelperPluginManager;

/**
 * Description of BasketPayedService
 *
 * @author allapow
 */
class BasketFinalizeService extends AbstractService
{
    protected XshopBasketEntity $xshopBasketEntity;
    protected HelperPluginManager $viewHelperManager;
    protected array $shopConfig;
    protected ShopBasketTablex $shopBasketTablex;
    protected ShopBasketTable $shopBasketTable;
    protected ShopBasketItemTable $shopBasketItemTable;
    protected ShopBasketEntityTable $shopBasketEntityTable;
    protected ShopAddressService $shopAddressService;
    protected ShopBasketAddressTable $shopBasketAddressTable;
    protected ShopUserAddressTable $shopUserAddressTable;
    protected ShopBasketEntityTablex $shopBasketEntityTablex;
    protected ShopArticleStockService $shopArticleStockService;
    protected ShopArticleOptionItemArticlePricediffTable $shopArticleOptionItemArticlePricediffTable;
    protected ShopUserTablex $shopUserTablex;
    protected ShopConfigurationTable $shopConfigurationTable;
    protected BasketService $basketService;
    protected ShippingServiceInterface $shippingService;
    protected DocumentOrderService $documentOrderService;
    protected DocumentInvoiceService $documentInvoiceService;
    protected ShopDocumentInvoiceTable $shopDocumentInvoiceTable;
    protected MailWrapper $mailWrapper;
    protected SimpleMailer $simpleMailer;
    protected ShopArticleOptionsEntity $shopArticleOptionsEntity;
    protected ShopArticleParam2dItemTable $shopArticleParam2dItemTable;
    protected ShopArticleLengthcutDefTable $shopArticleLengthcutDefTable;

    public function setViewHelperManager(HelperPluginManager $viewHelperManager): void
    {
        $this->viewHelperManager = $viewHelperManager;
    }

    public function setShopConfig(array $shopConfig): void
    {
        $this->shopConfig = $shopConfig;
    }

    public function setShopBasketTablex(ShopBasketTablex $shopBasketTablex): void
    {
        $this->shopBasketTablex = $shopBasketTablex;
    }

    public function setShopBasketTable(ShopBasketTable $shopBasketTable): void
    {
        $this->shopBasketTable = $shopBasketTable;
    }

    public function setShopBasketItemTable(ShopBasketItemTable $shopBasketItemTable): void
    {
        $this->shopBasketItemTable = $shopBasketItemTable;
    }

    public function setShopBasketEntityTable(ShopBasketEntityTable $shopBasketEntityTable): void
    {
        $this->shopBasketEntityTable = $shopBasketEntityTable;
    }

    public function setShopAddressService(ShopAddressService $shopAddressService): void
    {
        $this->shopAddressService = $shopAddressService;
    }

    public function setShopBasketAddressTable(ShopBasketAddressTable $shopBasketAddressTable): void
    {
        $this->shopBasketAddressTable = $shopBasketAddressTable;
    }

    public function setShopUserAddressTable(ShopUserAddressTable $shopUserAddressTable): void
    {
        $this->shopUserAddressTable = $shopUserAddressTable;
    }

    public function setShopBasketEntityTablex(ShopBasketEntityTablex $shopBasketEntityTablex): void
    {
        $this->shopBasketEntityTablex = $shopBasketEntityTablex;
    }

    public function setShopArticleStockService(ShopArticleStockService $shopArticleStockService): void
    {
        $this->shopArticleStockService = $shopArticleStockService;
    }

    public function setShopArticleOptionItemArticlePricediffTable(ShopArticleOptionItemArticlePricediffTable $shopArticleOptionItemArticlePricediffTable): void
    {
        $this->shopArticleOptionItemArticlePricediffTable = $shopArticleOptionItemArticlePricediffTable;
    }

    public function setShopUserTablex(ShopUserTablex $shopUserTablex): void
    {
        $this->shopUserTablex = $shopUserTablex;
    }

    public function setShopConfigurationTable(ShopConfigurationTable $shopConfigurationTable): void
    {
        $this->shopConfigurationTable = $shopConfigurationTable;
    }

    public function setBasketService(BasketService $basketService): void
    {
        $this->basketService = $basketService;
    }

    public function setShippingService(ShippingServiceInterface $shippingService): void
    {
        $this->shippingService = $shippingService;
    }

    public function setDocumentOrderService(DocumentOrderService $documentOrderService): void
    {
        $this->documentOrderService = $documentOrderService;
    }

    public function setDocumentInvoiceService(DocumentInvoiceService $documentInvoiceService): void
    {
        $this->documentInvoiceService = $documentInvoiceService;
    }

    public function setShopDocumentInvoiceTable(ShopDocumentInvoiceTable $shopDocumentInvoiceTable): void
    {
        $this->shopDocumentInvoiceTable = $shopDocumentInvoiceTable;
    }

    public function setMailWrapper(MailWrapper $mailWrapper): void
    {
        $this->mailWrapper = $mailWrapper;
    }

    public function setSimpleMailer(SimpleMailer $simpleMailer): void
    {
        $this->simpleMailer = $simpleMailer;
    }

    public function setShopArticleOptionsEntity(ShopArticleOptionsEntity $shopArticleOptionsEntity): void
    {
        $this->shopArticleOptionsEntity = $shopArticleOptionsEntity;
    }

    public function setShopArticleParam2dItemTable(ShopArticleParam2dItemTable $shopArticleParam2dItemTable): void
    {
        $this->shopArticleParam2dItemTable = $shopArticleParam2dItemTable;
    }

    public function setShopArticleLengthcutDefTable(ShopArticleLengthcutDefTable $shopArticleLengthcutDefTable): void
    {
        $this->shopArticleLengthcutDefTable = $shopArticleLengthcutDefTable;
    }

    /**
     * Creates the locale \Bitkorn\Shop\Entity\Basket\XshopBasketEntity $xshopBasketEntity.
     *
     * @param string $basketUnique
     * @param array $shopbasket All basket items from db.shop_basket_item with JOIN db.shop_basket & db.shop_article
     * @return boolean
     */
    private function createXshopBasketEntityWithBasketData($basketUnique, array $shopbasket)
    {
        if (!isset($this->xshopBasketEntity)) {
            $this->xshopBasketEntity = new XshopBasketEntity($basketUnique);
            $this->xshopBasketEntity->flipMappingBack();
            $this->xshopBasketEntity->exchangeArrayBasket($shopbasket);
            $this->xshopBasketEntity = $this->basketService->computeValuesXshopBasketEntity($this->xshopBasketEntity);
            $this->shippingService->setXshopBasketEntity($this->xshopBasketEntity);
            if (!empty($shopbasket[0]) && !empty($shopbasket[0]['shipping_provider_unique_descriptor'])) {
                try {
                    if ($this->shippingService->setShippingProviderUniqueDescriptor($shopbasket[0]['shipping_provider_unique_descriptor'])) {
                        $addressEntity = $this->shopAddressService->getShopAddressEntity($this->xshopBasketEntity);
                        $this->shippingService->setShopAddressEntity($addressEntity);
                        if (!empty($shippingProvider = $this->shippingService->getShippingProvider())) {
                            $shippingProvider->setXshopBasketEntity($this->xshopBasketEntity);
                            $shippingProvider->setShopAddressEntity($addressEntity);
                        }
                        $this->xshopBasketEntity->computeShippingCosts($this->shippingService);
                    }
                } catch (\Exception $exception) {
                    $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() Error while setShippingProviderUniqueDescriptor()');
                }
            }
        }
        if (!$this->xshopBasketEntity instanceof XshopBasketEntity) {
            return false;
        }
        return true;
    }

    /**
     * Wird IMMER beim Kauf aufgerufen
     * set basket status (status muss == 'basket')
     *
     * XshopBasketEntity()->updateComputedValues()
     *
     * manage stock
     *
     * create order-document (Set order No.)
     * send email with document to customer
     * archive ShopBasket (db.shop_basket_entity)
     *
     * @param string $basketUnique
     * @return int
     */
    public function basketOrdered($basketUnique)
    {
        if (!$this->shopBasketTable->existShopBasket($basketUnique)) {
            $this->logger->warn(__CLASS__ . '()->' . __FUNCTION__ . '(): shopBasket with ' . $basketUnique . ' does not exist');
            return -1;
        }
        $timeOrdered = time();
        $this->shopBasketTable->updateShopBasketValues($basketUnique, ['shop_basket_status' => 'ordered', 'shop_basket_time_order' => $timeOrdered]);
        $shopbasket = $this->shopBasketTablex->getShopBasket($basketUnique);

        setcookie('basketunique', BasketUnique::computeBasketUnique(), $timeOrdered + 60 * 60 * 24 * 4, '/'); // 10 Tage

        if (!$this->createXshopBasketEntityWithBasketData($basketUnique, $shopbasket)) {
            return -1;
        }

        if (!$this->shopArticleStockService->manageStockOut($this->xshopBasketEntity, $timeOrdered)) {
            $this->simpleMailer->sendAdminEmail(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__ . ': $xshopBasketEntity->manageStock() failed; $shopBasketUnique: ' . $basketUnique);
        }
        $this->shippingService->setShippingProviderUniqueDescriptor($this->xshopBasketEntity->getShippingProviderUniqueDescriptor());
        $this->xshopBasketEntity->setShippingMethod($this->shippingService->getShippingProvider()->getBrandText());

        if (!$this->archiveShopBasket()) {
            $this->simpleMailer->sendAdminEmail('Error while archiveShopBasket() in ' . __CLASS__ . '()->' . __FUNCTION__ . '; $shopBasketUnique: ' . $basketUnique);
            return -1;
        }

        $shopDocumentOrder = $this->documentOrderService->createDocument($this->xshopBasketEntity, $timeOrdered);
        $fqfnOrder = $shopDocumentOrder['shop_document_order_path_absolute'] . '/' . $shopDocumentOrder['shop_document_order_filename'];
        return $this->sendEmailOrder($basketUnique, $shopbasket, $fqfnOrder);
    }

    /**
     *
     * @param string $basketUnique
     * @param array $shopbasket
     * @param string $fqfnOrder
     * @return int
     */
    public function sendEmailOrder($basketUnique, array $shopbasket, $fqfnOrder): int
    {
        if (!$this->createXshopBasketEntityWithBasketData($basketUnique, $shopbasket)) {
            return -1;
        }
        $fqfnAgb = $this->shopConfig['document_path_absolute'] . '/agb.pdf';

        $emailShopConfirmationOrdered = $this->viewHelperManager->get('emailShopConfirmationOrdered');
        $this->mailWrapper->setBccEmails([$this->shopConfigurationTable->getShopConfigurationValue('email_basket_bcc')]);
        $this->mailWrapper->setFromEmail($this->shopConfigurationTable->getShopConfigurationValue('email_basket_from_email'));
        $this->mailWrapper->setFromName($this->shopConfigurationTable->getShopConfigurationValue('email_basket_from_name'));
        $this->mailWrapper->setSubject($this->shopConfigurationTable->getShopConfigurationValue('email_basketordered_subject'));
        $this->mailWrapper->setTextHtml(call_user_func($emailShopConfirmationOrdered, $this->xshopBasketEntity));

        $addressEntity = $this->shopAddressService->getShopAddressEntityOrdered($basketUnique);
        $this->mailWrapper->setToName($addressEntity->getAddressName1() . ' ' . $addressEntity->getAddressName2());
        $this->mailWrapper->setToEmail($addressEntity->getAddressEmail());

        if ($this->mailWrapper->sendMailWithAttachment([$fqfnOrder => 'application/pdf', $fqfnAgb => 'application/pdf']) != 1) {
            $this->simpleMailer->sendAdminEmail('Error while mailWrapper->sendMailWithAttachment() in ' . __CLASS__ . '()->' . __FUNCTION__ . '; $shopBasketUnique: ' . $basketUnique);
            return -1;
        }
        return 1;
    }

    /**
     * NUR EIN MAL FUNKTIONIERT HIER
     * ...DARF NICHT ZWEI MAL HIER LANDEN!!!
     *
     * set basket status (status muss == 'ordered')
     *
     * Setzt die Bezahlzeit im ShopBasket
     *
     * Generiert und versendet eine Email an den Kaeufer.
     *
     * @param string $basketUnique
     * @param int|null $timePayed
     * @param string $paymentInfo
     * @return int
     */
    public function basketPayed(string $basketUnique, int $timePayed = null, string $paymentInfo = ''):int
    {
        $shopbasketEntities = $this->shopBasketEntityTable->getShopBasketEntitiesByBasketUnique($basketUnique);
        if (empty($shopbasketEntities)) {
            $this->logger->warn(__CLASS__ . '->' . __FUNCTION__ . '(): shopBasketEntity with ' . $basketUnique . ' are empty');
            return -1;
        }
        if (empty($shopbasketEntities[0]['payment_number']) || empty($shopbasketEntities[0]['payment_method'])) {
            $this->logger->warn(__CLASS__ . '->' . __FUNCTION__ . '(): shopBasketEntity with ' . $basketUnique . ' payment_method | payment_number is empty');
            return -1;
        }
        if ($shopbasketEntities[0]['shop_basket_status'] != 'ordered') {
            $this->logger->warn(__CLASS__ . '->' . __FUNCTION__ . '(): shopBasketEntity with ' . $basketUnique . ' shop_basket_status != ordered');
            return -1;
        }
        if (!empty($shopbasketEntities[0]['shop_basket_time_payed'])) {
            $this->logger->warn(__CLASS__ . '->' . __FUNCTION__ . '(): shopBasketEntity with ' . $basketUnique . ' is already payed ...pay two times are impossible');
            return -1;
        }
        if (!$timePayed) {
            $timePayed = time();
        }
        $updateResult = $this->shopBasketEntityTable->update(
            [
                'shop_basket_status' => 'payed',
                'shop_basket_time_payed' => $timePayed,
                'payment_info' => $paymentInfo
            ], ['shop_basket_unique' => $basketUnique]);
        if ($updateResult < 1) {
            $this->simpleMailer->sendAdminEmail(__CLASS__ . '()->' . __FUNCTION__ . '(' . __LINE__ . '): $setShopBasketTimePayedResult; $shopBasketUnique: ' . $basketUnique);
            return -1;
        }
        return $this->sendEmailPayed($basketUnique, $shopbasketEntities);
    }

    /**
     *
     * @param string $basketUnique
     * @param array $shopbasketEntities
     * @return int
     */
    public function sendEmailPayed($basketUnique, array $shopbasketEntities)
    {
        $emailShopConfirmationPayed = $this->viewHelperManager->get('emailShopConfirmationPayed');
        $this->mailWrapper->setFromEmail($this->shopConfigurationTable->getShopConfigurationValue('email_basket_from_email'));
        $this->mailWrapper->setFromName($this->shopConfigurationTable->getShopConfigurationValue('email_basket_from_name'));
        $this->mailWrapper->setSubject($this->shopConfigurationTable->getShopConfigurationValue('email_basketpayed_subject'));
        $this->mailWrapper->setTextHtml(call_user_func($emailShopConfirmationPayed, $shopbasketEntities));

        $addressEntity = $this->shopAddressService->getShopAddressEntityOrdered($basketUnique);
        $this->mailWrapper->setToName($addressEntity->getAddressName1() . ' ' . $addressEntity->getAddressName2());
        $this->mailWrapper->setToEmail($addressEntity->getAddressEmail());
        if ($this->mailWrapper->sendMail() != 1) {
            $this->simpleMailer->sendAdminEmail('Error while mailWrapper->sendMail() in ' . __CLASS__ . '; $shopBasketUnique: ' . $basketUnique);
            return -1;
        }
        return 1;
    }

    /**
     * sets basket status (status muss == 'payed')
     * shipping number must exist
     *
     * @param string $shopBasketUnique
     * @return int
     */
    public function basketShipped($shopBasketUnique)
    {
        $shopbasketEntities = $this->shopBasketEntityTable->getShopBasketEntitiesByBasketUnique($shopBasketUnique);
        if (empty($shopbasketEntities)) {
            $this->logger->warn(__CLASS__ . '->' . __FUNCTION__ . '(): shopBasketEntity with ' . $shopBasketUnique . ' are empty');
            return -1;
        }
        if ($shopbasketEntities[0]['shop_basket_status'] != 'payed') {
            $this->logger->warn(__CLASS__ . '->' . __FUNCTION__ . '(): shopBasketEntity with ' . $shopBasketUnique . ' shop_basket_status != payed');
            return -1;
        }
        $this->shopBasketEntityTable->update(['shop_basket_status' => 'shipped', 'shop_basket_entity_time_shipped' => time()],
            ['shop_basket_unique' => $shopBasketUnique]);


        if (!$this->shopDocumentInvoiceTable->existShopDocumentInvoiceByBasketUnique($shopBasketUnique)) {
            $xshopBasketEntity = new XshopBasketEntity($shopBasketUnique);
            $xshopBasketEntity->exchangeArrayBasket($shopbasketEntities);
            $xshopBasketEntity = $this->basketService->computeValuesXshopBasketEntity($xshopBasketEntity);
            $documentInvoice = $this->documentInvoiceService->createDocument($xshopBasketEntity, time());
        } else {
            $documentInvoice = $this->shopDocumentInvoiceTable->getShopDocumentInvoiceByBasketUnique($shopBasketUnique);
        }
        $fqfnInvoice = $documentInvoice['shop_document_invoice_path_absolute'] . '/' . $documentInvoice['shop_document_invoice_filename'];

        return $this->sendEmailShipped($shopBasketUnique, $shopbasketEntities, $fqfnInvoice);
    }

    /**
     *
     * @param string $basketUnique
     * @param array $shopbasketEntities
     * @param string $fqfnInvoice
     * @return int
     */
    public function sendEmailShipped($basketUnique, $shopbasketEntities, $fqfnInvoice)
    {
        $fqfnAgb = $this->shopConfig['document_path_absolute'] . '/agb.pdf';

        $emailShopConfirmationShipped = $this->viewHelperManager->get('emailShopConfirmationShipped');
        $this->mailWrapper->setFromEmail($this->shopConfigurationTable->getShopConfigurationValue('email_basket_from_email'));
        $this->mailWrapper->setFromName($this->shopConfigurationTable->getShopConfigurationValue('email_basket_from_name'));
        $this->mailWrapper->setSubject($this->shopConfigurationTable->getShopConfigurationValue('email_basketshipped_subject'));
        $this->mailWrapper->setTextHtml(call_user_func($emailShopConfirmationShipped, $shopbasketEntities));

        $addressEntity = $this->shopAddressService->getShopAddressEntityOrdered($basketUnique);
        $this->mailWrapper->setToName($addressEntity->getAddressName1() . ' ' . $addressEntity->getAddressName2());
        $this->mailWrapper->setToEmail($addressEntity->getAddressEmail());

        $addressEntity = $this->shopAddressService->getShopAddressEntityOrdered($basketUnique);
        $this->mailWrapper->setToName($addressEntity->getAddressName1() . ' ' . $addressEntity->getAddressName2());
        $this->mailWrapper->setToEmail($addressEntity->getAddressEmail());

//        $basketAddressCount = $this->shopBasketAddressTable->countShopBasketAddresses($basketUnique);
//        if ($basketAddressCount <= 0) {
//            $userWithAddresses = $this->shopUserTablex->getShopUserWithAddresses($shopbasketEntities[0]['user_uuid']);
//            $this->mailWrapper->setToName($userWithAddresses['invoice']['shop_user_address_name1'] . ' ' . $userWithAddresses['invoice']['shop_user_address_name2']);
//            $this->mailWrapper->setToEmail($userWithAddresses['invoice']['email']);
//        } else {
//            $shopBasketAdresses = $this->shopBasketAddressTable->getShopBasketAddressesByBasketUnique($basketUnique);
//            $this->mailWrapper->setToName($shopBasketAdresses['invoice']['shop_basket_address_name1'] . ' ' . $shopBasketAdresses['invoice']['shop_basket_address_name2']);
//            $this->mailWrapper->setToEmail($shopBasketAdresses['invoice']['shop_basket_address_email']);
//        }

        return $this->mailWrapper->sendMailWithAttachment([$fqfnInvoice => 'application/pdf', $fqfnAgb => 'application/pdf']);
    }

    public function sendRatingReminder($basketEntities)
    {
        $emailRatingReminder = $this->viewHelperManager->get(RatingReminder::class);
        $this->mailWrapper->setFromEmail($this->shopConfigurationTable->getShopConfigurationValue('email_basket_from_email'));
        $this->mailWrapper->setFromName($this->shopConfigurationTable->getShopConfigurationValue('email_basket_from_name'));
        $this->mailWrapper->setSubject('Bitte bewerten Sie Ihren Einkauf bei Systemgurt');
        $this->mailWrapper->setTextHtml(call_user_func($emailRatingReminder, $basketEntities));

        $basketAddressCount = $this->shopBasketAddressTable->countShopBasketAddresses($basketEntities[0]['shop_basket_unique']);
        if ($basketAddressCount <= 0) {
            $userWithAddresses = $this->shopUserTablex->getShopUserWithAddresses($basketEntities[0]['user_uuid']);
            $this->mailWrapper->setToName($userWithAddresses['invoice']['shop_user_address_name1'] . ' ' . $userWithAddresses['invoice']['shop_user_address_name2']);
            $this->mailWrapper->setToEmail($userWithAddresses['invoice']['email']);
        } else {
            $shopBasketAdresses = $this->shopBasketAddressTable->getShopBasketAddressesByBasketUnique($basketEntities[0]['shop_basket_unique']);
            $this->mailWrapper->setToName($shopBasketAdresses['invoice']['shop_basket_address_name1'] . ' ' . $shopBasketAdresses['invoice']['shop_basket_address_name2']);
            $this->mailWrapper->setToEmail($shopBasketAdresses['invoice']['shop_basket_address_email']);
        }

        return $this->mailWrapper->sendMail();
    }

    public function archiveShopBasket(): bool
    {
        $success = true;
        $storageItems = $this->xshopBasketEntity->getStorageItems();
        foreach ($storageItems as $storageItem) {
            $storageItem['computed_value_article_id_count'] = $this->xshopBasketEntity->getArticleIdCount();
            $storageItem['computed_value_article_amount_sum'] = $this->xshopBasketEntity->getArticleAmountSum();
            $storageItem['computed_value_article_weight_sum'] = $this->xshopBasketEntity->getArticleWeightSum();
            $storageItem['computed_value_article_shipping_costs_single_sum'] = $this->xshopBasketEntity->getArticleShippingCostsSingleSum();
            $storageItem['computed_value_article_price_total_sum'] = $this->xshopBasketEntity->getArticlePriceTotalSum();
            $storageItem['computed_value_article_tax_total_sum'] = $this->xshopBasketEntity->getArticleTaxTotalSum();
            $storageItem['computed_value_article_price_total_sum_end'] = $this->xshopBasketEntity->getArticlePriceTotalSumEnd();
            $storageItem['computed_value_article_tax_total_sum_end'] = $this->xshopBasketEntity->getArticleTaxTotalSumEnd();
            $storageItem['computed_value_shop_basket_discount_tax'] = $this->xshopBasketEntity->getBasketDiscountTax();
            $storageItem['computed_value_article_shipping_costs_computed'] = $this->xshopBasketEntity->getArticleShippingCostsComputed();
            $storageItem['computed_value_article_shipping_costs_tax_computed'] = $this->xshopBasketEntity->getArticleShippingCostsTaxComputed();
            $storageItem['computed_value_shop_basket_tax_total_sum_end'] = $this->xshopBasketEntity->getBasketTaxTotalSumEnd();
            unset($storageItem['shop_article_sefurl']);
            $storageItem['shipping_method'] = $this->xshopBasketEntity->getShippingMethod();
            $lastInsertId = $this->shopBasketEntityTable->saveShopBasketEntity($storageItem);
            if (empty($lastInsertId)) {
                $this->simpleMailer->sendAdminEmail(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__ . '; shop_basket_unique: ' . $storageItem['shop_basket_unique']);
                $success = false;
            }
        }
        if (!empty($userUuid = $this->xshopBasketEntity->getUserUuid())) {
            if ($this->shopBasketEntityTablex->copyShopUserAddressesToBasketEntity($userUuid, $this->xshopBasketEntity->getBasketUnique(),
                    $this->shopBasketEntityTable) < 1) {
                $this->simpleMailer->sendAdminEmail(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__ . '; shop_basket_unique: ' . $storageItem['shop_basket_unique']);
                $success = false;
            }
        } else {
            if ($this->shopBasketEntityTablex->copyShopBasketAddressesToBasketEntity($this->xshopBasketEntity->getBasketUnique(), $this->shopBasketEntityTable) < 1) {
                $this->simpleMailer->sendAdminEmail(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__ . '; shop_basket_unique: ' . $storageItem['shop_basket_unique']);
                $success = false;
            }
        }
        return $success;
    }
}
