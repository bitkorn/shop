<?php

namespace Bitkorn\Shop\Service\Basket;

use Bitkorn\Shop\Entity\Address\ShopAddressEntity;
use Bitkorn\Shop\Entity\Article\ShopArticleOptionsEntity;
use Bitkorn\Shop\Entity\Article\TypeDelivery\ShopArticleLengthcutDefEntity;
use Bitkorn\Shop\Entity\Basket\Address\IsoCountry;
use Bitkorn\Shop\Entity\Basket\ShopBasketDiscountEntity;
use Bitkorn\Shop\Entity\Basket\XshopBasketEntity;
use Bitkorn\Shop\Service\Shipping\ShippingService\ShippingService;
use Bitkorn\Shop\Table\Article\Option\ShopArticleOptionItemArticlePricediffTable;
use Bitkorn\Shop\Table\Article\Param2d\ShopArticleParam2dItemTable;
use Bitkorn\Shop\Table\Article\TypeDelivery\ShopArticleLengthcutDefTable;
use Bitkorn\Shop\Table\Basket\Discount\ShopBasketDiscountTable;
use Bitkorn\Shop\Table\Basket\ShopBasketAddressTable;
use Bitkorn\Shop\Table\Basket\ShopBasketEntityTable;
use Bitkorn\Shop\Table\Basket\ShopBasketItemTable;
use Bitkorn\Shop\Table\Basket\ShopBasketTable;
use Bitkorn\Shop\Table\Common\ShopConfigurationTable;
use Bitkorn\Shop\Table\User\ShopUserAddressTable;
use Bitkorn\Shop\Tablex\Article\ShopArticleImageTablex;
use Bitkorn\Shop\Tablex\Basket\ShopBasketEntityTablex;
use Bitkorn\Shop\Tablex\Basket\ShopBasketTablex;
use Bitkorn\Shop\Tools\Basket\BasketUnique;
use Bitkorn\Trinket\Service\AbstractService;
use Laminas\Stdlib\ArrayUtils;
use Redis;

/**
 * Class BasketService
 * @package Bitkorn\Shop\Service\Basket
 *
 * Manage the basket session object with redis.
 */
class BasketService extends AbstractService
{
    const COOKIE_BASKET_KEY = 'basketunique';
    const SESSION_BASKETUNIQUE_KEY = 'BASKET_UNIQUE';
    const SESSION_BASKETENTITY_KEY = 'BASKET_ENTITY';
    const SESSION_TIME_TOUCH_KEY = 'BASKET_TIME_TOUCH';
    const GUEST_PASSWD_KEY = 'GUEST_PASSWD';

    /**
     * @var int
     */
    protected $sessionLifetime;

    /**
     * @var Redis
     */
    protected $redis;

    /**
     * @var string
     */
    protected $basketUnique;

    /**
     * @var XshopBasketEntity
     */
    protected $xshopBasketEntity;

    /**
     * @var ShopConfigurationTable
     */
    protected $shopConfigurationTable;

    /**
     * @var ShopBasketItemTable
     */
    protected $shopBasketItemTable;

    /**
     * @var ShopBasketTablex
     */
    protected $shopBasketTablex;

    /**
     * @var ShopArticleOptionItemArticlePricediffTable
     */
    protected $shopArticleOptionItemArticlePricediffTable;

    /**
     * @var ShippingService
     */
    protected $shippingService;

    /**
     * @var ShopArticleImageTablex
     */
    protected $shopArticleImageTablex;

    /**
     * @var ShopBasketDiscountTable
     */
    protected $shopBasketDiscountTable;

    /**
     * @var ShopBasketTable
     */
    protected $shopBasketTable;

    /**
     * @var ShopBasketEntityTable
     */
    protected $shopBasketEntityTable;

    /**
     * @var ShopBasketAddressTable
     */
    protected $shopBasketAddressTable;

    /**
     * @var ShopUserAddressTable
     */
    protected $shopUserAddressTable;

    /**
     * @var ShopBasketEntityTablex
     */
    protected $shopBasketEntityTablex;

    /**
     * @var ShopArticleParam2dItemTable
     */
    protected $shopArticleParam2dItemTable;

    /**
     * @var ShopArticleLengthcutDefTable
     */
    protected $shopArticleLengthcutDefTable;

    /**
     * @param int $sessionLifetime
     */
    public function setSessionLifetime(int $sessionLifetime): void
    {
        $this->sessionLifetime = $sessionLifetime;
    }

    /**
     * @param Redis $redis
     */
    public function setRedis(Redis $redis): void
    {
        $this->redis = $redis;
    }

    /**
     * @param ShopConfigurationTable $shopConfigurationTable
     */
    public function setShopConfigurationTable(ShopConfigurationTable $shopConfigurationTable): void
    {
        $this->shopConfigurationTable = $shopConfigurationTable;
    }

    /**
     * @param ShopBasketItemTable $shopBasketItemTable
     */
    public function setShopBasketItemTable(ShopBasketItemTable $shopBasketItemTable): void
    {
        $this->shopBasketItemTable = $shopBasketItemTable;
    }

    /**
     * @param ShopBasketTablex $shopBasketTablex
     */
    public function setShopBasketTablex(ShopBasketTablex $shopBasketTablex): void
    {
        $this->shopBasketTablex = $shopBasketTablex;
    }

    /**
     * @param ShopArticleOptionItemArticlePricediffTable $shopArticleOptionItemArticlePricediffTable
     */
    public function setShopArticleOptionItemArticlePricediffTable(ShopArticleOptionItemArticlePricediffTable $shopArticleOptionItemArticlePricediffTable): void
    {
        $this->shopArticleOptionItemArticlePricediffTable = $shopArticleOptionItemArticlePricediffTable;
    }

    /**
     * @param ShippingService $shippingService
     */
    public function setShippingService(ShippingService $shippingService): void
    {
        $this->shippingService = $shippingService;
    }

    /**
     * @param ShopArticleImageTablex $shopArticleImageTablex
     */
    public function setShopArticleImageTablex(ShopArticleImageTablex $shopArticleImageTablex): void
    {
        $this->shopArticleImageTablex = $shopArticleImageTablex;
    }

    /**
     * @param ShopBasketDiscountTable $shopBasketDiscountTable
     */
    public function setShopBasketDiscountTable(ShopBasketDiscountTable $shopBasketDiscountTable): void
    {
        $this->shopBasketDiscountTable = $shopBasketDiscountTable;
    }

    /**
     * @param ShopBasketTable $shopBasketTable
     */
    public function setShopBasketTable(ShopBasketTable $shopBasketTable): void
    {
        $this->shopBasketTable = $shopBasketTable;
    }

    /**
     * @param ShopBasketEntityTable $shopBasketEntityTable
     */
    public function setShopBasketEntityTable(ShopBasketEntityTable $shopBasketEntityTable): void
    {
        $this->shopBasketEntityTable = $shopBasketEntityTable;
    }

    /**
     * @param ShopBasketAddressTable $shopBasketAddressTable
     */
    public function setShopBasketAddressTable(ShopBasketAddressTable $shopBasketAddressTable): void
    {
        $this->shopBasketAddressTable = $shopBasketAddressTable;
    }

    /**
     * @param ShopUserAddressTable $shopUserAddressTable
     */
    public function setShopUserAddressTable(ShopUserAddressTable $shopUserAddressTable): void
    {
        $this->shopUserAddressTable = $shopUserAddressTable;
    }

    /**
     * @param ShopBasketEntityTablex $shopBasketEntityTablex
     */
    public function setShopBasketEntityTablex(ShopBasketEntityTablex $shopBasketEntityTablex): void
    {
        $this->shopBasketEntityTablex = $shopBasketEntityTablex;
    }

    /**
     * @param ShopArticleParam2dItemTable $shopArticleParam2dItemTable
     */
    public function setShopArticleParam2dItemTable(ShopArticleParam2dItemTable $shopArticleParam2dItemTable): void
    {
        $this->shopArticleParam2dItemTable = $shopArticleParam2dItemTable;
    }

    /**
     * @param ShopArticleLengthcutDefTable $shopArticleLengthcutDefTable
     */
    public function setShopArticleLengthcutDefTable(ShopArticleLengthcutDefTable $shopArticleLengthcutDefTable): void
    {
        $this->shopArticleLengthcutDefTable = $shopArticleLengthcutDefTable;
    }

    /**
     * @return string
     */
    public function getBasketUnique(): string
    {
        return $this->basketUnique;
    }

    /**
     * E.g. for base_url/einkauf-status/[basketUnique]
     * to set basketUnique manually and create basket from it ($this->>checkBasket()).
     * @param string $basketUnique
     */
    public function setBasketUnique(string $basketUnique): void
    {
        $this->basketUnique = $basketUnique;
    }

    /**
     * Diese Funktion in ControllerActions aufrufen um das BasketCookie zu haben.
     */
    public function purgeBasketUniqueCookie(): void
    {
        $basketUnique = filter_input(INPUT_COOKIE, self::COOKIE_BASKET_KEY, FILTER_SANITIZE_STRING);
        if (false === $basketUnique) {
            // filter fails
            $a = $basketUnique;
        }
        if (null === $basketUnique) {
            // none cookie set
            $basketUnique = '';
        }
        $this->purgeBasketUniqueSession($basketUnique);
    }

    /**
     * Sets or creates a hash that does not exist yet.
     *
     * @param string $basketUnique
     * @return string
     */
    private function purgeBasketUniqueSession(string $basketUnique): string
    {
        if (empty($basketUnique) || strlen($basketUnique) < 60) {
            $basketUnique = $this->purgeBasketUniqueSession(BasketUnique::computeBasketUnique());
        }
        if (!$this->redis->hExists(self::SESSION_BASKETUNIQUE_KEY, $basketUnique)) {
            $this->redis->hSet($basketUnique, self::SESSION_BASKETUNIQUE_KEY, $basketUnique);
            setcookie(self::COOKIE_BASKET_KEY, $basketUnique, time() + $this->sessionLifetime, '/'); // 10 Tage
        }
        $this->basketUnique = $basketUnique;
        return $this->basketUnique;
    }

    /**
     * A guest password can only exist if there is a basketunique.
     *
     * @param string $passwd
     */
    public function setGuestPasswd(string $passwd)
    {
        if (empty($this->basketUnique)) {
            throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() called without basketUnique');
        }
        $this->redis->hSet($this->basketUnique, self::GUEST_PASSWD_KEY, $passwd);
    }

    /**
     * A guest password can only exist if there is a basketunique.
     *
     * @return string
     */
    public function getGuestPasswd()
    {
        if (empty($this->basketUnique)) {
            throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() called without basketUnique');
        }
        return $this->redis->hGet($this->basketUnique, self::GUEST_PASSWD_KEY);
    }

    public function deleteGuestPasswd()
    {
        if (empty($this->basketUnique)) {
            throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() called without basketUnique');
        }
        return $this->redis->hDel($this->basketUnique, self::GUEST_PASSWD_KEY);
    }

    /**
     * - creates an empty basket
     * - save it to session
     * - save it to database
     * @param string $userUuid
     * @param string $userUuidPos
     */
    public function createEmptyBasket(?string $userUuid = null, ?string $userUuidPos = null)
    {
        if ($userUuid && empty($userUuidPos)) {
            $userUuidPos = $userUuid;
        }
        $storage = [
            'shop_basket_unique' => $this->basketUnique,
            'user_uuid' => $userUuid,
            'user_uuid_pos' => $userUuidPos,
        ];
        $this->xshopBasketEntity = new XshopBasketEntity($this->basketUnique);
        $this->xshopBasketEntity->exchangeArrayFromDatabase($storage);
        if ($this->shopBasketTable->saveNewShopBasket($this->basketUnique, $userUuid, $userUuidPos) == 1) {
            $this->sessionSetBasketEntity();
        }
    }

    /**
     * @param string|null $basketUnique
     * @return bool
     */
    public function existShopBasket(?string $basketUnique = null): bool
    {
        if (isset($basketUnique)) {
            return $this->shopBasketTable->existShopBasket($basketUnique);
        } else {
            return $this->shopBasketTable->existShopBasket($this->basketUnique);
        }
    }

    /**
     * @param string $basketUnique
     * @return int Amount of deleted shopBasketItems
     */
    public function deleteShopBasket(string $basketUnique): int
    {
        return $this->shopBasketTablex->deleteShopBasket($basketUnique);
    }

    /**
     * $this->purgeBasketUniqueSession() must called before.
     *
     * @param bool $ignoreSessionLifetime
     * @return XshopBasketEntity|null
     */
    public function checkBasket(bool $ignoreSessionLifetime = false)
    {
        if (empty($this->basketUnique)) {
            return null;
        }
        $xshopBasketEntitySerialized = $this->redis->hGet($this->basketUnique, self::SESSION_BASKETENTITY_KEY);
        if (empty($xshopBasketEntitySerialized)) {
            return null;
        }
        if (!$ignoreSessionLifetime) {
            $sessionTimeTouch = intval($this->redis->hGet($this->basketUnique, self::SESSION_TIME_TOUCH_KEY));
            if ($sessionTimeTouch > 0 && $sessionTimeTouch + $this->sessionLifetime < time()) {
                return null;
            }
        }
        /** @var XshopBasketEntity xshopBasketEntity */
        $this->xshopBasketEntity = unserialize($xshopBasketEntitySerialized);
        if (!$this->isBasketEntity()) {
            $this->deleteShopBasket($this->basketUnique);
            return null;
        }
        $this->redis->hSet($this->basketUnique, self::SESSION_TIME_TOUCH_KEY, time());
        return $this->xshopBasketEntity;
    }

    /**
     * 1. fetch basket data from db
     * 2. make entity
     * 3. compute values and shipping costs
     * 4. init registered shipping provider
     * 5. load images
     * 6. push the new entity into the session
     */
    public function refreshBasket()
    {
        $basketData = $this->shopBasketTablex->getShopBasketByStatus($this->basketUnique);
        $this->xshopBasketEntity = new XshopBasketEntity($this->basketUnique);
        $this->xshopBasketEntity->flipMappingBack();
        $this->xshopBasketEntity->exchangeArrayBasket($basketData);
        $this->xshopBasketEntity = $this->computeValuesXshopBasketEntity($this->xshopBasketEntity);

        $this->shippingService->setXshopBasketEntity($this->xshopBasketEntity);
        if (isset($basketData[0]) && !empty($basketData[0]['shipping_provider_unique_descriptor'])) {
            try {
                if ($this->shippingService->setShippingProviderUniqueDescriptor($basketData[0]['shipping_provider_unique_descriptor'])) {
                    $addressEntity = $this->getShopAddressEntity();
                    $this->shippingService->setShopAddressEntity($addressEntity);
                    if (!empty($shippingProvider = $this->shippingService->getShippingProvider())) {
                        $shippingProvider->setXshopBasketEntity($this->xshopBasketEntity);
                        $shippingProvider->setShopAddressEntity($addressEntity);
                    }
                    $this->xshopBasketEntity->computeShippingCosts($this->shippingService);
                }
            } catch (\Exception $exception) {
                $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() Error while setShippingProviderUniqueDescriptor()');
            }
        }
        if (!empty($shippingProviders = $this->shippingService->getShippingProvidersInstances())) {
            foreach ($shippingProviders as $shippingProvider) {
                $shippingProvider->setXshopBasketEntity($this->xshopBasketEntity);
                $shippingProvider->setShopAddressEntity($this->getShopAddressEntity());
            }
        }
        $this->xshopBasketEntity->loadImagesArticleIdAssoc($this->shopArticleImageTablex);
        $this->updateShopBasket();
        $this->sessionSetBasketEntity();
    }

    public function computeValuesXshopBasketEntity(XshopBasketEntity $xshopBasketEntity): XshopBasketEntity
    {
        $storageItems = $xshopBasketEntity->getStorageItems();
        $articleAmountSum = 0;
        $articlePriceTotalSum = 0;
        $articleTaxTotalSum = 0;
        $articleWeightSum = 0;
        foreach ($storageItems as $index => $storageItem) {
            $xshopBasketEntity->incrementArticleIdCount();
            $xshopBasketEntity->addArticleShippingCostsSingleSum($storageItem['shop_article_shipping_costs_single']);
            $articlePrice = $storageItem['shop_article_price']; // base price
            $articleAmount = $storageItem['shop_article_amount'];
            $articleAmountSum += $articleAmount;
            $articleWeight = $storageItem['shop_article_weight'];
            if (!empty($storageItem['shop_basket_item_article_options']) && $storageItem['shop_basket_item_article_options'] != '[]') {
                $optionsEntity = new ShopArticleOptionsEntity();
                $optionsEntity->exchangeArrayOptions(json_decode($storageItem['shop_basket_item_article_options'], true));
                $priceDiff = $optionsEntity->computePricediff($this->shopArticleOptionItemArticlePricediffTable, $storageItem['shop_article_id']);
                $articlePrice += $priceDiff;
                $articlePriceTotal = $articlePrice * $articleAmount;
                $articleWeightSum += $articleWeight * $articleAmount;
            } else if (!empty($storageItem['shop_article_param2d_item_id']) && !empty($storageItem['shop_basket_item_lengthcut_options'])) {
                /**
                 * lengthcut article ever has param2d
                 */
                $param2dItem = $this->shopArticleParam2dItemTable->getShopArticleParam2dItemForParam2dItemId($storageItem['shop_article_param2d_item_id']);
                $lengthcutDef = $this->shopArticleLengthcutDefTable->getLengthcutDefForArticleId($storageItem['shop_article_id']);
                $lengthcutDefArr = ShopArticleLengthcutDefEntity::decodeDefinitionJson($storageItem['shop_basket_item_lengthcut_options']);

                // compute base article amount
                $articleAmountLengthcut = round(($lengthcutDefArr['length_pieces'] * $lengthcutDefArr['quantity']) / $lengthcutDef['shop_article_lengthcut_def_basequantity'], 2);
                // + priceDiff param 2D
                $articlePrice += $param2dItem['shop_article_param2d_item_pricediff'];
                // * base article amount
                $articlePrice *= $articleAmountLengthcut;
                // + cutprice
                $articlePrice += $lengthcutDef['shop_article_lengthcut_def_cutprice'] * $lengthcutDefArr['quantity_cuts'];
                $articlePriceTotal = $articlePrice;

                // shop_article_param2d_item_weightdiff
                $articleWeight += $param2dItem['shop_article_param2d_item_weightdiff'];
                $articleWeight *= $articleAmountLengthcut;
                $articleWeightSum += $articleWeight;
            } else {
                $articlePriceTotal = $articlePrice * $articleAmount;
                $articleWeightSum += $articleWeight * $articleAmount;
            }
            $xshopBasketEntity->setComputedValueShopArticlePrice($index, $articlePrice);
            $xshopBasketEntity->setComputedValueShopArticleTax($index, $articlePrice / (100 + $storageItem['shop_article_tax']) * $storageItem['shop_article_tax']);
            $xshopBasketEntity->setComputedValueShopArticleAmount($index, $articleAmount);
            $xshopBasketEntity->setComputedValueShopArticlePriceTotal($index, $articlePriceTotal);
            $xshopBasketEntity->setComputedValueShopArticleTaxTotal($index, $articlePriceTotal / (100 + $storageItem['shop_article_tax']) * $storageItem['shop_article_tax']);
            $xshopBasketEntity->setComputedValueShopArticleWeightTotal($index, $articleWeight);
            $xshopBasketEntity->setArticleWeightSum($articleWeightSum);
            $articlePriceTotalSum += $articlePriceTotal;
            $articleTaxTotalSum += round(($articlePriceTotal / (100 + $storageItem['shop_article_tax'])) * $storageItem['shop_article_tax'], 2);
        } // end foreach
        $xshopBasketEntity->setArticleAmountSum($articleAmountSum);
        $xshopBasketEntity->setArticlePriceTotalSum($articlePriceTotalSum);
        $xshopBasketEntity->setArticleTaxTotalSum($articleTaxTotalSum);

        if (!empty($storageItems[0]['shop_article_id'])) {
            $xshopBasketEntity->setBasketStatus($storageItem['shop_basket_status']);
        }
        if (!empty($storageItems[0]['shop_basket_discount_value'])) {
            $xshopBasketEntity->setBasketDiscountHash($storageItems['shop_basket_discount_hash']);
            $xshopBasketEntity->setBasketDiscountValue($storageItems['shop_basket_discount_value']);
            $xshopBasketEntity->setArticlePriceTotalSumEnd($xshopBasketEntity->getArticlePriceTotalSum() - $xshopBasketEntity->getBasketDiscountValue());
            /**
             * tax
             * Weil Artikel unterschiedlichen Tax haben koennen wird hier mit dem prozentualem Discount gerechnet.
             */
            $xshopBasketEntity->setDiscountPercentage($xshopBasketEntity->getBasketDiscountValue() / ($xshopBasketEntity->getArticlePriceTotalSum() / 100));
            $xshopBasketEntity->setBasketDiscountTax(round($xshopBasketEntity->getArticleTaxTotalSum() * ($xshopBasketEntity->getDiscountPercentage() / 100), 2));
            $xshopBasketEntity->setArticleTaxTotalSumEnd($xshopBasketEntity->getArticleTaxTotalSum() - $xshopBasketEntity->getBasketDiscountTax());
        } else {
            $xshopBasketEntity->setArticlePriceTotalSumEnd($xshopBasketEntity->getArticlePriceTotalSum());
            $xshopBasketEntity->setBasketTaxTotalSumEnd($xshopBasketEntity->getArticleTaxTotalSum());
            $xshopBasketEntity->setArticleTaxTotalSumEnd($xshopBasketEntity->getArticleTaxTotalSum());
        }
        $xshopBasketEntity->setValuesComputed(true);
        return $xshopBasketEntity;
    }

    public function updateShopBasket()
    {
        return $this->shopBasketTable->updateShopBasketValues($this->getBasketUnique(), $this->xshopBasketEntity->getShopBasketValuesWithComputedValues());
    }

    /**
     * Save xshopBasketEntity with basketUnique in session.
     * Both, basketUnique and xshopBasketEntity can not be empty!
     */
    protected function sessionSetBasketEntity(): void
    {
        if (!BasketUnique::validUnique($this->basketUnique) || empty($this->xshopBasketEntity) || !$this->xshopBasketEntity instanceof XshopBasketEntity) {
            throw new \RuntimeException('You can not call ' . __CLASS__ . '()->' . __FUNCTION__ . '() without basketUnique nor xshopBasketEntity');
        }
        $xshopBasketEntitySerialized = serialize($this->xshopBasketEntity);
        $this->redis->hSet($this->basketUnique, self::SESSION_BASKETENTITY_KEY, $xshopBasketEntitySerialized);
    }

    /**
     * @param string|null $basketUnique
     * @return array
     */
    public function getShopBasket(?string $basketUnique = null): array
    {
        if (isset($basketUnique)) {
            return $this->shopBasketTablex->getShopBasket($basketUnique);
        } else {
            return $this->shopBasketTablex->getShopBasket($this->basketUnique);
        }
    }

    /**
     * @return XshopBasketEntity
     */
    public function getXshopBasketEntity(): XshopBasketEntity
    {
        return $this->xshopBasketEntity;
    }

    /**
     * @return bool
     */
    public function isBasketDiscountEnabled(): bool
    {
        if (!$this->isBasketEntity()) {
            return false;
        }
        return empty($this->xshopBasketEntity->getBasketDiscountHash()) && $this->shopConfigurationTable->getShopConfigurationValue('basket_discount_enable') == 1;
    }

    private function isBasketEntity(): bool
    {
        return isset($this->xshopBasketEntity) && $this->xshopBasketEntity instanceof XshopBasketEntity;
    }

    /**
     * @param int $basketItemId
     * @param float $basketItemAmount
     * @return int
     */
    public function updateShopBasketItemAmount(int $basketItemId, float $basketItemAmount): int
    {
        if ($basketItemAmount > 0) {
            return $this->shopBasketItemTable->editAmount($this->basketUnique, $basketItemId, $basketItemAmount);
        } else if ($basketItemAmount == 0) {
            return $this->shopBasketItemTable->deleteShopBasketItem($this->basketUnique, $basketItemId);
        }
    }

    /**
     * @param string $basketDiscountHash
     * @return bool
     */
    public function computeBasketDiscount(string $basketDiscountHash): bool
    {
        if (!$this->isBasketDiscountEnabled() && $this->xshopBasketEntity->getArticlePriceTotalSum() > 0) {
            $this->message = 'Rabatt ist zur Zeit nicht möglich.';
            return false;
        }
        $shopBasketDiscountEntity = new ShopBasketDiscountEntity();
        $shopBasketDiscountEntity->loadShopBasketDiscount($basketDiscountHash, $this->shopBasketDiscountTable, $this->shopBasketTablex);
        if ($shopBasketDiscountEntity->isHashValid()) {
            if ($shopBasketDiscountEntity->getMinOrderAmount() <= $this->xshopBasketEntity->getArticlePriceTotalSum()) {
                if ($this->updateShopBasketValues([
                        'shop_basket_discount_hash' => $basketDiscountHash,
                        'shop_basket_discount_value' => $shopBasketDiscountEntity->computeDiscountValue($this->xshopBasketEntity->getArticlePriceTotalSum())]) > 0) {
                    $shopBasketDiscountEntity->loadShopBasketDiscount($basketDiscountHash, $this->shopBasketDiscountTable,
                        $this->shopBasketTablex);
                    $this->refreshBasket();
                }
            } else {
                $this->message = 'Der Mindestbestellwert für diesen Rabatt-Code ist nicht erreicht.';
                return false;
            }
        } else {
            $this->message = 'Der Rabatt-Code ist nicht gültig.';
            return false;
        }
        return true;
    }

    public function updateShopBasketValues(array $storage): bool
    {
        return $this->shopBasketTable->updateShopBasketValues($this->basketUnique, $storage) >= 0;
    }

    /**
     * @param $articleId
     * @param $articleOptionsJson
     * @return int
     */
    public function sumOrderedAndPayedShopArticle($articleId, $articleOptionsJson)
    {
        return $this->shopBasketEntityTable->sumOrderedAndPayedShopArticle($articleId, $articleOptionsJson);
    }

    /**
     * @param string $shopBasketUnique
     * @param int $shopBasketItemId
     * @return array
     */
    public function getShopBasketEntityByBasketItemId(string $shopBasketUnique, int $shopBasketItemId)
    {
        return $this->shopBasketEntityTable->getShopBasketEntityByBasketItemId($shopBasketUnique, $shopBasketItemId);
    }

    /**
     * @param string $paymentNumber
     * @return int
     */
    public function setPaymentNumber(string $paymentNumber): int
    {
        return $this->shopBasketTable->setPaymentNumber($this->basketUnique, $paymentNumber);
    }

    /**
     * @param string $method
     * @return int
     */
    public function setPaymentMethod(string $method): int
    {
        return $this->shopBasketTable->setPaymentMethod($this->basketUnique, $method);
    }

    /**
     * @param string $basketStatus
     * @return array
     */
    public function getShopBasketByStatus($basketStatus = 'basket')
    {
        return $this->shopBasketTablex->getShopBasketByStatus($this->basketUnique, $basketStatus);
    }

    /**
     * @return array Assoc array with shop_basket_address_type as key. First address is the invoice address.
     */
    public function getShopBasketAddressesByBasketUnique(): array
    {
        return $this->shopBasketAddressTable->getShopBasketAddressesByBasketUnique($this->basketUnique);
    }

    /**
     * @param int $addressId
     * @return array
     */
    public function getShopBasketAddressById(int $addressId): array
    {
        return $this->shopBasketAddressTable->getShopBasketAddressById($addressId);
    }

    /**
     * @param array $storage Must contain not empty values for fields:
     * - shop_basket_unique**
     * - shop_basket_address_id
     * @return bool
     */
    public function deleteShopBasketAddress(array $storage): bool
    {
        if (empty($storage['shop_basket_address_id']) || empty($storage['shop_basket_unique'])) {
            return false;
        }
        return $this->shopBasketAddressTable->deleteShopBasketAddress($storage) >= 0;
    }

    public function deleteShopBasketAddresses()
    {
        return $this->shopBasketAddressTable->deleteShopBasketAddresses($this->basketUnique);
    }

    /**
     * @param int $articleId
     * @param float $articleAmount
     * @param string $articleOptionsJson
     * @param int $articleParam2dItemId
     * @param string $lengthcutDefJson
     * @return int
     * @todo update also for lengthcutDef ...check $lengthcutDefJson for lengthcut_def_id & length_pieces THEN update quantity & quantity_cuts
     */
    public function saveOrUpdateShopBasketItem(int $articleId, float $articleAmount, string $articleOptionsJson, int $articleParam2dItemId = 0, string $lengthcutDefJson = '')
    {
        if (!$this->shopBasketTablex->isShopBasketItemsEditable($this->basketUnique)) {
            return -1;
        }
        if (empty($lengthcutDefJson) && $this->shopBasketItemTable->existShopBasketItem($this->basketUnique, $articleId, $articleOptionsJson, $articleParam2dItemId)) {
            return $this->shopBasketItemTable->updateShopBasketItem($this->basketUnique, $articleId, $articleAmount, $articleOptionsJson, $articleParam2dItemId, $lengthcutDefJson);
        } else {
            return $this->shopBasketItemTable->saveNewShopBasketItem($this->basketUnique, $articleId, $articleAmount, $articleOptionsJson, $articleParam2dItemId, $lengthcutDefJson);
        }
    }

    public function setBasketUser($userUuid = null)
    {
        return $this->shopBasketTable->setUserUuid($this->basketUnique, $userUuid);
    }

    public function countShopBasketAddresses(): int
    {
        return $this->shopBasketAddressTable->countShopBasketAddresses($this->basketUnique);
    }

    public function moveShopBasketAddressesToShopUserAddresses(string $userUuid): bool
    {
        return $this->shopBasketEntityTablex->moveShopBasketAddressesToShopUserAddresses($this->basketUnique, $userUuid) == 1;
    }

    /**
     * Save to database.
     *
     * @param string $shippingProviderUniqueDescriptor
     * @return bool
     */
    public function setShippingProviderUniqueDescriptor(string $shippingProviderUniqueDescriptor): bool
    {
        if (!$this->shippingService->validShippingProviderUniqueDescriptor($shippingProviderUniqueDescriptor)) {
            throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() Not a valid ShippingProviderUniqueDescriptor: ' . $shippingProviderUniqueDescriptor);
        }
        return $this->shopBasketTable->setShippingProviderUniqueDescriptor($this->basketUnique, $shippingProviderUniqueDescriptor) === 1;
    }

    public function deleteShippingProviderUniqueDescriptor(): bool
    {
        return $this->shopBasketTable->deleteShippingProviderUniqueDescriptor($this->basketUnique) >= 0;
    }

    /**
     * @param string $paymentNumber
     * @param string $shopbasketStatus
     * @return bool
     */
    public function setShopBasketStatusByPaymentNumber(string $paymentNumber, string $shopbasketStatus): bool
    {
        return $this->shopBasketTable->setShopBasketStatusByPaymentNumber($paymentNumber, $shopbasketStatus) >= 0;
    }

    /**
     * @param string $basketUnique
     * @return array
     */
    public function getShopBasketEntitiesByBasketUniqueWithRatings(string $basketUnique): array
    {
        return $this->shopBasketEntityTable->getShopBasketEntitiesByBasketUniqueWithRatings($basketUnique);
    }

    public function getShopBasketEntitiesWithRatings(): array
    {
        return $this->getShopBasketEntitiesByBasketUniqueWithRatings($this->basketUnique);
    }

    public function getShopBasketEntitiesWithRatingsByUserId(string $userUuid): array
    {
        return $this->shopBasketEntityTable->getShopBasketEntitiesWithRatingsByUserId($userUuid);
    }

    /**
     * @param string $preferredShopAddressType
     * @return ShopAddressEntity With shipment address: from user if exist else from basket & address_type == shipment else address_type = invoice.
     */
    protected function getShopAddressEntity(string $preferredShopAddressType = 'shipment'): ShopAddressEntity
    {
        $shopAddress = new ShopAddressEntity();
        if (!empty($userUuid = $this->xshopBasketEntity->getUserUuid())) {
            $shopUserData = $this->shopUserAddressTable->getShopUserAddressesByUserId($userUuid, true);
            $shopAddress->exchangeArrayFromDatabaseForUser($shopUserData[$preferredShopAddressType] ?? $shopUserData['invoice']);
        } else if (!empty($basketAddresses = $this->shopBasketAddressTable->getShopBasketAddressesByBasketUnique($this->xshopBasketEntity->getBasketUnique()))) {
            $shopAddress->exchangeArrayFromDatabaseForBasket($basketAddresses[$preferredShopAddressType] ?? $basketAddresses['invoice']);
        }
        return $shopAddress;
    }

    public function belongShopBasketItemToBasket(int $basketItemId): bool
    {
        return $this->shopBasketItemTable->belongShopBasketItemToBasket($this->basketUnique, $basketItemId);
    }

    public function deleteShopBasketItem(int $basketItemId): bool
    {
        return $this->shopBasketItemTable->deleteShopBasketItem($this->basketUnique, $basketItemId) > 0;
    }

    public function getShopBasketByPaymentNumber(string $paymentId)
    {
        return $this->shopBasketTable->getShopBasketByPaymentNumber($paymentId);
    }

    /**
     * check if too old
     * 2592000 = 30 days
     * @param string $basketUnique
     * @return bool default = true
     * @todo time for diff from config
     *
     */
    public function isBasketTooOld(string $basketUnique): bool
    {
        $time = time() - 2592000;
        return $this->shopBasketTable->isShopBasketTooOld($basketUnique, $time);
    }
}
