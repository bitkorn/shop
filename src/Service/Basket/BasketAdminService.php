<?php

namespace Bitkorn\Shop\Service\Basket;

use Bitkorn\Mail\Mail\MailWrapper;
use Bitkorn\Mail\Mail\SimpleMailer;
use Bitkorn\Shop\Service\Document\DocumentDeliveryService;
use Bitkorn\Shop\Service\Document\DocumentInvoiceService;
use Bitkorn\Shop\Service\Document\DocumentOrderService;
use Bitkorn\Shop\Table\Article\ShopArticleRatingTable;
use Bitkorn\Shop\Table\Basket\Claim\ShopBasketClaimTable;
use Bitkorn\Shop\Table\Basket\Discount\ShopBasketDiscountTable;
use Bitkorn\Shop\Table\Basket\ShopBasketAddressTable;
use Bitkorn\Shop\Table\Basket\ShopBasketEntityTable;
use Bitkorn\Shop\Table\Basket\ShopBasketItemTable;
use Bitkorn\Shop\Table\Basket\ShopBasketTable;
use Bitkorn\Shop\Table\Common\ShopConfigurationTable;
use Bitkorn\Shop\Table\Document\ShopDocumentDeliveryTable;
use Bitkorn\Shop\Table\Document\ShopDocumentInvoiceTable;
use Bitkorn\Shop\Table\Document\ShopDocumentOrderTable;
use Bitkorn\Shop\Table\User\ShopUserFeePaidTable;
use Bitkorn\Shop\Tablex\Basket\ShopBasketEntityTablex;
use Bitkorn\ShopPrepay\Table\ShopPrepayNumberTable;
use Bitkorn\Trinket\Service\AbstractService;

/**
 *
 * @author allapow
 */
class BasketAdminService extends AbstractService
{

    /**
     *
     * @var array
     */
    protected $shopConfig;

    /**
     *
     * @var MailWrapper
     */
    protected $mailWrapper;

    /**
     *
     * @var SimpleMailer
     */
    protected $simpleMailer;

    /**
     *
     * @var ShopConfigurationTable
     */
    protected $shopConfigurationTable;

    /**
     *
     * @var ShopBasketEntityTable
     */
    protected $shopBasketEntityTable;

    /**
     * @var ShopBasketEntityTablex
     */
    protected $shopBasketEntityTablex;

    /**
     *
     * @var ShopBasketTable
     */
    protected $shopBasketTable;

    /**
     *
     * @var ShopBasketItemTable
     */
    protected $shopBasketItemTable;

    /**
     *
     * @var ShopBasketAddressTable
     */
    protected $shopBasketAddressTable;

    /**
     *
     * @var ShopBasketClaimTable
     */
    protected $shopBasketClaimTable;

    /**
     *
     * @var ShopArticleRatingTable
     */
    protected $shopArticleRatingTable;

    /**
     *
     * @var DocumentOrderService
     */
    protected $documentOrderService;

    /**
     *
     * @var DocumentInvoiceService
     */
    protected $documentInvoiceService;

    /**
     *
     * @var DocumentDeliveryService
     */
    protected $documentDeliveryService;

    /**
     *
     * @var ShopUserFeePaidTable
     */
    protected $shopUserFeePaidTable;

    /**
     *
     * @var ShopPrepayNumberTable
     */
    protected $shopPrepayNumberTable;

    /**
     * @var ShopDocumentInvoiceTable
     */
    protected $shopDocumentInvoiceTable;

    /**
     * @var ShopDocumentDeliveryTable
     */
    protected $shopDocumentDeliveryTable;

    /**
     * @var ShopDocumentOrderTable
     */
    protected $shopDocumentOrderTable;

    /**
     * @var ShopBasketDiscountTable
     */
    protected $shopBasketDiscountTable;

    /**
     * @param array $shopConfig
     */
    public function setShopConfig(array $shopConfig): void
    {
        $this->shopConfig = $shopConfig;
    }

    /**
     * @param MailWrapper $mailWrapper
     */
    public function setMailWrapper(MailWrapper $mailWrapper): void
    {
        $this->mailWrapper = $mailWrapper;
    }

    /**
     * @param SimpleMailer $simpleMailer
     */
    public function setSimpleMailer(SimpleMailer $simpleMailer): void
    {
        $this->simpleMailer = $simpleMailer;
    }

    /**
     * @param ShopConfigurationTable $shopConfigurationTable
     */
    public function setShopConfigurationTable(ShopConfigurationTable $shopConfigurationTable): void
    {
        $this->shopConfigurationTable = $shopConfigurationTable;
    }

    /**
     * @param ShopBasketEntityTable $shopBasketEntityTable
     */
    public function setShopBasketEntityTable(ShopBasketEntityTable $shopBasketEntityTable): void
    {
        $this->shopBasketEntityTable = $shopBasketEntityTable;
    }

    /**
     * @param ShopBasketEntityTablex $shopBasketEntityTablex
     */
    public function setShopBasketEntityTablex(ShopBasketEntityTablex $shopBasketEntityTablex): void
    {
        $this->shopBasketEntityTablex = $shopBasketEntityTablex;
    }

    /**
     * @param ShopBasketTable $shopBasketTable
     */
    public function setShopBasketTable(ShopBasketTable $shopBasketTable): void
    {
        $this->shopBasketTable = $shopBasketTable;
    }

    /**
     * @param ShopBasketItemTable $shopBasketItemTable
     */
    public function setShopBasketItemTable(ShopBasketItemTable $shopBasketItemTable): void
    {
        $this->shopBasketItemTable = $shopBasketItemTable;
    }

    /**
     * @param ShopBasketAddressTable $shopBasketAddressTable
     */
    public function setShopBasketAddressTable(ShopBasketAddressTable $shopBasketAddressTable): void
    {
        $this->shopBasketAddressTable = $shopBasketAddressTable;
    }

    /**
     * @param ShopBasketClaimTable $shopBasketClaimTable
     */
    public function setShopBasketClaimTable(ShopBasketClaimTable $shopBasketClaimTable): void
    {
        $this->shopBasketClaimTable = $shopBasketClaimTable;
    }

    /**
     * @param ShopArticleRatingTable $shopArticleRatingTable
     */
    public function setShopArticleRatingTable(ShopArticleRatingTable $shopArticleRatingTable): void
    {
        $this->shopArticleRatingTable = $shopArticleRatingTable;
    }

    /**
     * @param DocumentOrderService $documentOrderService
     */
    public function setDocumentOrderService(DocumentOrderService $documentOrderService): void
    {
        $this->documentOrderService = $documentOrderService;
    }

    /**
     * @param DocumentInvoiceService $documentInvoiceService
     */
    public function setDocumentInvoiceService(DocumentInvoiceService $documentInvoiceService): void
    {
        $this->documentInvoiceService = $documentInvoiceService;
    }

    /**
     * @param DocumentDeliveryService $documentDeliveryService
     */
    public function setDocumentDeliveryService(DocumentDeliveryService $documentDeliveryService): void
    {
        $this->documentDeliveryService = $documentDeliveryService;
    }

    /**
     * @param ShopUserFeePaidTable $shopUserFeePaidTable
     */
    public function setShopUserFeePaidTable(ShopUserFeePaidTable $shopUserFeePaidTable): void
    {
        $this->shopUserFeePaidTable = $shopUserFeePaidTable;
    }

    /**
     * @param ShopPrepayNumberTable $shopPrepayNumberTable
     */
    public function setShopPrepayNumberTable(ShopPrepayNumberTable $shopPrepayNumberTable): void
    {
        $this->shopPrepayNumberTable = $shopPrepayNumberTable;
    }

    /**
     * @param ShopDocumentInvoiceTable $shopDocumentInvoiceTable
     */
    public function setShopDocumentInvoiceTable(ShopDocumentInvoiceTable $shopDocumentInvoiceTable): void
    {
        $this->shopDocumentInvoiceTable = $shopDocumentInvoiceTable;
    }

    /**
     * @param ShopDocumentDeliveryTable $shopDocumentDeliveryTable
     */
    public function setShopDocumentDeliveryTable(ShopDocumentDeliveryTable $shopDocumentDeliveryTable): void
    {
        $this->shopDocumentDeliveryTable = $shopDocumentDeliveryTable;
    }

    /**
     * @param ShopDocumentOrderTable $shopDocumentOrderTable
     */
    public function setShopDocumentOrderTable(ShopDocumentOrderTable $shopDocumentOrderTable): void
    {
        $this->shopDocumentOrderTable = $shopDocumentOrderTable;
    }

    /**
     * @param ShopBasketDiscountTable $shopBasketDiscountTable
     */
    public function setShopBasketDiscountTable(ShopBasketDiscountTable $shopBasketDiscountTable): void
    {
        $this->shopBasketDiscountTable = $shopBasketDiscountTable;
    }

    /**
     * Löscht alle Datensätze und Dateien eines BasketEntity (Alle Produkteinträge eines Baskets).
     * Dazu gehören (außer allen BasketEntities mit gleichem basketUnique):
     * - basket-entity-rating
     * - basket, basket-item, basket-address, basket-claim
     * - doc-order, doc-invoice, doc-delivery
     * - prepay-number
     * - user-fee-paid
     *
     * @param string $basketUnique
     * @return array
     */
    public function deleteCompleteBasketEntityBasketGroup($basketUnique)
    {
        if (empty($basketUnique)) {
            return [];
        }
        $shopBasketEntityData = $this->shopBasketEntityTable->getShopBasketEntitiesByBasketUnique($basketUnique);
        $dataCount = 0;
        $successList = [];
        $errorList = [];
        /*
         * Fee paid
         */
        foreach ($shopBasketEntityData as $shopBasketEntity) {
            $dataCountFeePaid = $this->shopUserFeePaidTable->deleteShopUserFeePaidByBasketEntityId($shopBasketEntity['shop_basket_entity_id']);
            if ($dataCountFeePaid >= 0) {
                $dataCount += $dataCountFeePaid;
                $successList[] = 'ShopUserFeePaid';
            } else {
                $errorList[] = 'ShopUserFeePaid';
            }
        }
        /*
         * PrePay number
         */
        if (!empty($shopBasketEntityData[0]) && !empty($shopBasketEntityData[0]['payment_number'])) {
            $dataCountPrepayNumber = $this->shopPrepayNumberTable->deleteShopPrepayNumberByNumber($shopBasketEntityData[0]['payment_number']);
            if ($dataCountPrepayNumber >= 0) {
                $dataCount += $dataCountPrepayNumber;
                $successList[] = 'ShopPrepayNumber';
            } else {
                $errorList[] = 'ShopPrepayNumber';
            }
        }
        /*
         * documents
         */
        if (!empty($shopBasketEntityData[0]) && isset($shopBasketEntityData[0]['user_uuid'])) {
            $dataCountDocDelivery = $this->documentDeliveryService->deleteDocument($basketUnique, $shopBasketEntityData[0]['user_uuid']);
            if ($dataCountDocDelivery >= 0) {
                $dataCount += $dataCountDocDelivery;
                $successList[] = 'DocumentDelivery';
            } else {
                $errorList[] = 'DocumentDelivery';
            }
            $dataCountDocInvoice = $this->documentInvoiceService->deleteDocument($basketUnique, $shopBasketEntityData[0]['user_uuid']);
            if ($dataCountDocInvoice >= 0) {
                $dataCount += $dataCountDocInvoice;
                $successList[] = 'DocumentInvoice';
            } else {
                $errorList[] = 'DocumentInvoice';
            }
            $dataCountDocOrder = $this->documentOrderService->deleteDocument($basketUnique, $shopBasketEntityData[0]['user_uuid']);
            if ($dataCountDocOrder >= 0) {
                $dataCount += $dataCountDocOrder;
                $successList[] = 'DocumentOrder';
            } else {
                $errorList[] = 'DocumentOrder';
            }
        }

        $dataCountBasketClaim = $this->shopBasketClaimTable->deleteShopBasketClaimByBasketUnique($basketUnique);
        if ($dataCountBasketClaim >= 0) {
            $dataCount += $dataCountBasketClaim;
            $successList[] = 'ShopBasketClaim';
        } else {
            $errorList[] = 'ShopBasketClaim';
        }

        $dataCountBasketAddress = $this->shopBasketAddressTable->deleteShopBasketAddressByBasketUnique($basketUnique);
        if ($dataCountBasketAddress >= 0) {
            $dataCount += $dataCountBasketAddress;
            $successList[] = 'ShopBasketAddress';
        } else {
            $errorList[] = 'ShopBasketAddress';
        }

        $dataCountBasketItem = $this->shopBasketItemTable->deleteShopBasketItemsAll($basketUnique);
        if ($dataCountBasketItem >= 0) {
            $dataCount += $dataCountBasketItem;
            $successList[] = 'ShopBasketItem';
        } else {
            $errorList[] = 'ShopBasketItem';
        }

        $dataCountBasket = $this->shopBasketTable->deleteShopBasketByBasketUnique($basketUnique);
        if ($dataCountBasket >= 0) {
            $dataCount += $dataCountBasket;
            $successList[] = 'ShopBasket';
        } else {
            $errorList[] = 'ShopBasket';
        }

        if (!empty($shopBasketEntityData[0])) {
            foreach ($shopBasketEntityData as $shopBasketEntity) {
                $dataCountBasketEntityRating = $this->shopArticleRatingTable->delete(['shop_basket_item_id' => $shopBasketEntity['shop_basket_item_id']]);
                if ($dataCountBasketEntityRating >= 0) {
                    $dataCount += $dataCountBasketEntityRating;
                    $successList[] = 'BasketEntityRating_' . $shopBasketEntity['shop_basket_entity_id'];
                } else {
                    $errorList[] = 'BasketEntityRating_' . $shopBasketEntity['shop_basket_entity_id'];
                }
            }
            $dataCountBasketEntity = $this->shopBasketEntityTable->deleteShopBasketEntityByBasketUnique($basketUnique);
            if ($dataCountBasketEntity >= 0) {
                $dataCount += $dataCountBasketEntity;
                $successList[] = 'ShopBasketEntity';
            } else {
                $errorList[] = 'ShopBasketEntity';
            }
        }
        $status = empty($errorList) ? 1 : -1;
        return ['status' => $status, 'count' => $dataCount, 'success' => $successList, 'errors' => $errorList, 'basketUnique' => $basketUnique];
    }

    /**
     * @param string $paymentNumber
     * @param string $shopDocumentOrderNumber
     * @param int $timeCreateFrom
     * @param int $timeCreateTo
     * @param string $basketStatus
     * @param string $userId
     * @param string $orderBy
     * @param string $orderByDirection
     * @return array
     */
    public function getShopBasketEntitiesGrouped(string $paymentNumber, string $shopDocumentOrderNumber, int $timeCreateFrom = 0, int $timeCreateTo = 9999999999,
                                                 string $basketStatus = '', string $userId = '', string $orderBy = 'shop_basket_time_order', string $orderByDirection = 'DESC'): array
    {
        return $this->shopBasketEntityTablex->searchShopBasketItems($paymentNumber, $shopDocumentOrderNumber, $timeCreateFrom, $timeCreateTo, $basketStatus, $userId, $orderBy, $orderByDirection);
    }

    /**
     * @param string $basketUnique
     * @param int $limit
     * @return array
     */
    public function getShopBasketEntitiesByBasketUnique(string $basketUnique, int $limit = -1): array
    {
        return $this->shopBasketEntityTable->getShopBasketEntitiesByBasketUnique($basketUnique, $limit);
    }

    public function existShopDocumentInvoiceByBasketUnique($basketUnique)
    {
        return $this->shopDocumentInvoiceTable->existShopDocumentInvoiceByBasketUnique($basketUnique);
    }

    public function existShopDocumentDeliveryByBasketUnique($basketUnique)
    {
        return $this->shopDocumentDeliveryTable->existShopDocumentDeliveryByBasketUnique($basketUnique);
    }

    /**
     * @param string $basketUnique
     * @param string $shippingMethod
     * @param string $shippingNumber
     * @return bool
     */
    public function updateShipping(string $basketUnique, string $shippingMethod, string $shippingNumber): bool
    {
        return $this->shopBasketEntityTable->updateShipping($basketUnique, $shippingMethod, $shippingNumber);
    }

    /**
     * @param string $shopBasketUnique
     * @param string $shopBasketEntityComments
     * @return int
     */
    public function updateShopBasketEntityComments(string $shopBasketUnique, string $shopBasketEntityComments): int
    {
        return $this->shopBasketEntityTable->updateShopBasketEntityComments($shopBasketUnique, $shopBasketEntityComments);
    }

    /**
     * @param string $basketUnique
     * @return array
     */
    public function getShopDocumentOrderByBasketUnique(string $basketUnique): array
    {
        return $this->shopDocumentOrderTable->getShopDocumentOrderByBasketUnique($basketUnique);
    }

    /**
     * @param string $basketUnique
     * @return array
     */
    public function getShopDocumentDeliveryByBasketUnique(string $basketUnique): array
    {
        return $this->shopDocumentDeliveryTable->getShopDocumentDeliveryByBasketUnique($basketUnique);
    }

    /**
     * @param string $basketUnique
     * @return array
     */
    public function getShopDocumentInvoiceByBasketUnique(string $basketUnique): array
    {
        return $this->shopDocumentInvoiceTable->getShopDocumentInvoiceByBasketUnique($basketUnique);
    }

    public function getShopBasketClaimsFromBasketIdAssoc(string $basketUnique)
    {
        return $this->shopBasketClaimTable->getShopBasketClaimsFromBasketIdAssoc($basketUnique);
    }

    public function getShopBasketsWithoutBasketEntity()
    {
        return $this->shopBasketTable->getShopBasketsWithoutBasketEntity();
    }

    public function getShopBasketEntityRating(int $shopBasketItemId): array
    {
        return $this->shopArticleRatingTable->getShopBasketEntityRating($shopBasketItemId);
    }

    public function getShopBasketEntity(int $basketEntityId): array
    {
        return $this->shopBasketEntityTable->getShopBasketEntity($basketEntityId);
    }

    public function updateBasketItemRatingPublished(int $shopBasketItemId, int $published): bool
    {
        return $this->shopArticleRatingTable->updateBasketItemRatingPublished($shopBasketItemId, $published) >= 0;
    }

    public function getShopBasketDiscounts($shopBasketDiscountHash, $shopBasketDiscountOnlyActive, $shopBasketDiscountTimeFrom, $shopBasketDiscountTimeTo, $postuserId)
    {
        return $this->shopBasketDiscountTable->getShopBasketDiscounts($shopBasketDiscountHash, $shopBasketDiscountOnlyActive, $shopBasketDiscountTimeFrom, $shopBasketDiscountTimeTo, $postuserId);
    }

    public function getShopBasketByVendorUserId(string $userUuid)
    {
        return $this->shopBasketEntityTable->getShopBasketByVendorUserId($userUuid);
    }

    public function getShopBasketByVendorUserIdMlm(string $userUuid)
    {
        return $this->shopBasketEntityTable->getShopBasketByVendorUserIdMlm($userUuid);
    }

    public function getShopBasketDiscountById(int $shopBasketDiscountId): array
    {
        return $this->shopBasketDiscountTable->getShopBasketDiscountById($shopBasketDiscountId);
    }

    public function updateShopBasketDiscountActive(int $shopBasketDiscountId, int $active): bool
    {
        return $this->shopBasketDiscountTable->updateShopBasketDiscount($shopBasketDiscountId, ['shop_basket_discount_active' => $active]) >= 0;
    }

    public function updateBasketItemRatingReminderSend(string $basketUnique): bool
    {
        return $this->shopBasketEntityTable->updateBasketItemRatingReminderSend($basketUnique) >= 0;
    }
}
