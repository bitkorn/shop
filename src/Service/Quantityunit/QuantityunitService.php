<?php

namespace Bitkorn\Shop\Service\Quantityunit;

use Bitkorn\Trinket\Service\AbstractService;
use Bitkorn\Trinket\Table\QuantityunitTable;
use Laminas\Log\Logger;

class QuantityunitService extends AbstractService
{
    /**
     * @var QuantityunitTable
     */
    protected $quantityunitTable;

    /**
     * @var array
     */
    protected $quantityunitUuidAssoc = [];

    /**
     * @param QuantityunitTable $quantityunitTable
     */
    public function setQuantityunitTable(QuantityunitTable $quantityunitTable): void
    {
        $this->quantityunitTable = $quantityunitTable;
    }

    /**
     * @param string $quantityunitUuid
     * @return array
     */
    protected function getQuantityunit(string $quantityunitUuid): array
    {
        if(empty($this->quantityunitUuidAssoc)) {
            $this->quantityunitUuidAssoc = $this->quantityunitTable->getQuantityunitUuidAssoc();
        }
        if (!isset($this->quantityunitUuidAssoc[$quantityunitUuid])) {
            throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() No quantityunit fount for uuid ' . $quantityunitUuid);
        }
        return $this->quantityunitUuidAssoc[$quantityunitUuid];
    }

    /**
     * @param string $quantityunitUuid
     * @param float $value
     * @return float
     */
    public function convertQuantityunitToOrigin(string $quantityunitUuid, float $value): float
    {
        $quantityunit = $this->getQuantityunit($quantityunitUuid);
        return $value * $quantityunit['quantityunit_resolution'];
        if ($quantityunit['quantityunit_resolution'] == 1) {
            return $value;
        }
        $quantityunitOrigin = $this->getQuantityunit($quantityunit['quantityunit_resolution_origin']);
        return $value * $quantityunitOrigin['quantityunit_resolution'];
    }
}
