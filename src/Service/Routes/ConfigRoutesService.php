<?php

namespace Bitkorn\Shop\Service\Routes;

use Bitkorn\Trinket\Service\AbstractService;
use Bitkorn\Trinket\Service\Routes\ModuleConfigRoutesInterface;
use Laminas\Db\Adapter\Adapter;
use Laminas\Router\Http\Segment;
use Laminas\View\Helper\Url;

/**
 *
 * @author allapow
 */
class ConfigRoutesService extends AbstractService implements ModuleConfigRoutesInterface
{

    /**
     *
     * @var Adapter
     */
    protected $adapter;

    /**
     *
     * @var Url
     */
    protected $urlHelper;

    /**
     *
     * @var array [0 => ['display' => valueDisplay, 'url' => valueUrl],]
     */
    protected $routes = [];

    /**
     * @param Adapter $adapter
     */
    public function setAdapter(Adapter $adapter): void
    {
        $this->adapter = $adapter;
    }

    /**
     * @param Url $urlHelper
     */
    public function setUrlHelper(Url $urlHelper): void
    {
        $this->urlHelper = $urlHelper;
    }

    /**
     * @param array $configRoutes
     */
    public function initService(array $configRoutes): void
    {
        foreach ($configRoutes as $routeName => $route) {
            if (!isset($route['route_def'])) {
                unset($configRoutes[$routeName]);
                continue;
            }
            switch ($route['type']) {
                case 'Literal':
                case \Laminas\Router\Http\Literal::class:
                    $this->routes[] = ['display' => $route['route_def']['name'], 'url' => call_user_func($this->urlHelper, $routeName)];
                    break;
                case 'Segment':
                case Segment::class:
                    $constraintsCount = count($route['route_def']['constraints']);
                    if ($constraintsCount == 0) {
                        $this->logger->warn('route_def constraints are empty for Segment route');
                        continue;
                    }
                    switch ($constraintsCount) {
                        case 1:
                            $constraintNames = array_keys($route['route_def']['constraints']);
                            $constraint = $route['route_def']['constraints'][$constraintNames[0]];
                            try {
                                $stmt = $this->adapter->createStatement('SELECT ' . $constraint['db_table_value'] . ',' . $constraint['db_table_display']
                                    . ' FROM ' . $constraint['db_table'] . ' WHERE ' . $constraint['db_where']);
                                $stmt->prepare();
                                $result = $stmt->execute();
                                if ($result->valid() && $result->count() > 0) {
                                    do {
                                        $current = $result->current();
                                        $this->routes[] = ['display' => $route['route_def']['name'] . $current[$constraint['db_table_display']]
                                            , 'url' => call_user_func($this->urlHelper, $routeName,
                                                [$constraintNames[0] => $current[$constraint['db_table_value']]])];
                                        $result->next();
                                    } while ($result->valid());
                                }
                            } catch (\Exception $ex) {
                                $this->logger->err($ex->getMessage());
                            }
                            break;
                        case 2:
                        default :
                            $this->logger->err('More than one constraint is not yet supported. Class: ' . __CLASS__);
                    }
                    break;
                default :
                    continue;
            }
        }
    }

    /**
     *
     * @return array
     */
    public function getRoutes()
    {
        return $this->routes;
    }

}
