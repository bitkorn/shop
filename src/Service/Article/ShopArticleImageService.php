<?php

namespace Bitkorn\Shop\Service\Article;

use Bitkorn\Images\Table\Image\ImageTable;
use Bitkorn\Shop\Table\Article\ShopArticleImageTable;
use Bitkorn\Shop\Tablex\Article\ShopArticleImageTablex;
use Bitkorn\Trinket\Service\AbstractService;

class ShopArticleImageService extends AbstractService
{

    /**
     * @var ImageTable
     */
    protected $imageTable;

    /**
     * @var ShopArticleImageTable
     */
    protected $shopArticleImageTable;

    /**
     * @var ShopArticleImageTablex
     */
    protected $shopArticleImageTablex;

    /**
     * @param ImageTable $imageTable
     */
    public function setImageTable(ImageTable $imageTable): void
    {
        $this->imageTable = $imageTable;
    }

    /**
     * @param ShopArticleImageTable $shopArticleImageTable
     */
    public function setShopArticleImageTable(ShopArticleImageTable $shopArticleImageTable): void
    {
        $this->shopArticleImageTable = $shopArticleImageTable;
    }

    /**
     * @param ShopArticleImageTablex $shopArticleImageTablex
     */
    public function setShopArticleImageTablex(ShopArticleImageTablex $shopArticleImageTablex): void
    {
        $this->shopArticleImageTablex = $shopArticleImageTablex;
    }

    /**
     * @param int $articleId
     * @return array
     */
    public function getShopArticleImages(int $articleId): array
    {
        return $this->shopArticleImageTablex->getShopArticleImages($articleId);
    }

    /**
     * @param int $articleImageId
     * @return array
     */
    public function getShopArticleImageById(int $articleImageId): array
    {
        return $this->shopArticleImageTable->getShopArticleImageById($articleImageId);
    }

    public function deleteShopArticleImageById($articleImageId)
    {
        return $this->shopArticleImageTable->deleteShopArticleImageById($articleImageId);
    }

    /**
     * @param int $articleId
     * @param int $ralationImageId
     * @return bool
     */
    public function existImagesImageForArticle(int $articleId, int $ralationImageId): bool
    {
        return $this->shopArticleImageTable->existImagesImageForArticle($articleId, $ralationImageId);
    }

    /**
     * @param array $storage
     * @return int
     */
    public function saveShopArticleImage(array $storage): int
    {
        return $this->shopArticleImageTable->saveShopArticleImage($storage);
    }

    /**
     * @param int $articleImageId
     * @param int $articleImagePriority
     * @return bool
     */
    public function updateShopArticleImagePriority(int $articleImageId, int $articleImagePriority): bool
    {
        return $this->shopArticleImageTable->updateShopArticleImagePriority($articleImageId, $articleImagePriority) >= 0;
    }

    /**
     * @param int $articleId
     * @return array
     */
    public function getImagesExcludeForShopArticle(int $articleId)
    {
        return $this->shopArticleImageTablex->getImagesExcludeForShopArticle($articleId);
    }

    public function getImagesExcludeForShopArticleOption($articleId, $optionItemId)
    {
        return $this->shopArticleImageTablex->getImagesExcludeForShopArticleOption($articleId, $optionItemId);
    }
}
