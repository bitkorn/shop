<?php

namespace Bitkorn\Shop\Service\Article;

use Bitkorn\Shop\Table\Article\ShopArticleTable;
use Bitkorn\Shop\Table\Article\TypeDelivery\ShopArticleLengthcutDefTable;
use Bitkorn\Trinket\Service\AbstractService;

class ShopArticleTypeDeliveryService extends AbstractService
{
    /**
     * @var ShopArticleLengthcutDefTable
     */
    protected $shopArticleLengthcutDefTable;

    /**
     * @var ShopArticleTable
     */
    protected $shopArticleTable;

    /**
     * @param ShopArticleLengthcutDefTable $shopArticleLengthcutDefTable
     */
    public function setShopArticleLengthcutDefTable(ShopArticleLengthcutDefTable $shopArticleLengthcutDefTable): void
    {
        $this->shopArticleLengthcutDefTable = $shopArticleLengthcutDefTable;
    }

    /**
     * @param ShopArticleTable $shopArticleTable
     */
    public function setShopArticleTable(ShopArticleTable $shopArticleTable): void
    {
        $this->shopArticleTable = $shopArticleTable;
    }

    /**
     * @return array
     */
    public function getLengthcutDefs(): array
    {
        return $this->shopArticleLengthcutDefTable->getLengthcutDefs();
    }

    /**
     * @return array
     */
    public function getLengthcutDefIdAssoc(): array
    {
        return $this->shopArticleLengthcutDefTable->getLengthcutDefIdAssoc();
    }

    /**
     * @param array $storage
     * @return int The new generated shop_article_lengthcut_def_id
     */
    public function insertLengthcutDef(array $storage): int
    {
        return $this->shopArticleLengthcutDefTable->insertLengthcutDef($storage);
    }

    /**
     * @param int $articleId
     * @param int $lengthCutDefId
     * @return bool
     */
    public function isArticleLengthCutDefIdSwitchValid(int $articleId, int $lengthCutDefId): bool
    {
        $article = $this->shopArticleTable->getShopArticleById($articleId);
        $lengthcutDef = $this->shopArticleLengthcutDefTable->getLengthcutDef($lengthCutDefId);
        if(empty($article) || empty($lengthcutDef)) {
            return false;
        }
        return $article['shop_article_type_delivery'] == 'lengthcut';
    }

    /**
     * @param int $articleId
     * @param int $lengthCutDefId
     * @return bool
     */
    public function updateArticleLengthCutDefId(int $articleId, int $lengthCutDefId): bool
    {
        return $this->shopArticleTable->updateShopArticle($articleId, ['shop_article_lengthcut_def_id' => $lengthCutDefId]) >= 0;
    }
}
