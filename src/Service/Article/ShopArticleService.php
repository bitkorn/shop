<?php

namespace Bitkorn\Shop\Service\Article;

use Bitkorn\Shop\Entity\Database\QueryParamsArticleSearch;
use Bitkorn\Shop\Table\Article\ShopArticleCategoryRelationTable;
use Bitkorn\Shop\Table\Article\ShopArticleCategoryTable;
use Bitkorn\Shop\Table\Article\ShopArticleClassTable;
use Bitkorn\Shop\Table\Article\ShopArticleGroupRelationTable;
use Bitkorn\Shop\Table\Article\ShopArticleRatingTable;
use Bitkorn\Shop\Table\Article\ShopArticleSizeGroupRelationTable;
use Bitkorn\Shop\Table\Article\ShopArticleStockTable;
use Bitkorn\Shop\Table\Article\ShopArticleTable;
use Bitkorn\Shop\Table\Article\Size\ShopArticleSizeDefTable;
use Bitkorn\Shop\Tablex\Article\ShopArticleTablex;
use Bitkorn\Trinket\Service\AbstractService;

class ShopArticleService extends AbstractService
{
    /**
     * @var ShopArticleTable
     */
    protected $shopArticleTable;

    /**
     * @var ShopArticleTablex
     */
    protected $shopArticleTablex;

    /**
     * @var ShopArticleCategoryRelationTable
     */
    protected $shopArticleCategoryRelationTable;

    /**
     * @var ShopArticleGroupRelationTable
     */
    protected $shopArticleGroupRelationTable;

    /**
     * @var ShopArticleSizeGroupRelationTable
     */
    protected $shopArticleSizeGroupRelationTable;

    /**
     * @var ShopArticleSizeDefTable
     */
    protected $shopArticleSizeDefTable;

    /**
     * @var ShopArticleCategoryTable
     */
    protected $shopArticleCategoryTable;

    /**
     * @var ShopArticleClassTable
     */
    protected $shopArticleClassTable;

    /**
     * @var ShopArticleStockTable
     */
    protected $shopArticleStockTable;

    /**
     * @var ShopArticleRatingTable
     */
    protected $shopArticleRatingTable;

    /**
     * @param ShopArticleTable $shopArticleTable
     */
    public function setShopArticleTable(ShopArticleTable $shopArticleTable): void
    {
        $this->shopArticleTable = $shopArticleTable;
    }

    /**
     * @param ShopArticleTablex $shopArticleTablex
     */
    public function setShopArticleTablex(ShopArticleTablex $shopArticleTablex): void
    {
        $this->shopArticleTablex = $shopArticleTablex;
    }

    /**
     * @param ShopArticleCategoryRelationTable $shopArticleCategoryRelationTable
     */
    public function setShopArticleCategoryRelationTable(ShopArticleCategoryRelationTable $shopArticleCategoryRelationTable): void
    {
        $this->shopArticleCategoryRelationTable = $shopArticleCategoryRelationTable;
    }

    /**
     * @param ShopArticleGroupRelationTable $shopArticleGroupRelationTable
     */
    public function setShopArticleGroupRelationTable(ShopArticleGroupRelationTable $shopArticleGroupRelationTable): void
    {
        $this->shopArticleGroupRelationTable = $shopArticleGroupRelationTable;
    }

    /**
     * @param ShopArticleSizeGroupRelationTable $shopArticleSizeGroupRelationTable
     */
    public function setShopArticleSizeGroupRelationTable(ShopArticleSizeGroupRelationTable $shopArticleSizeGroupRelationTable): void
    {
        $this->shopArticleSizeGroupRelationTable = $shopArticleSizeGroupRelationTable;
    }

    /**
     * @param ShopArticleSizeDefTable $shopArticleSizeDefTable
     */
    public function setShopArticleSizeDefTable(ShopArticleSizeDefTable $shopArticleSizeDefTable): void
    {
        $this->shopArticleSizeDefTable = $shopArticleSizeDefTable;
    }

    /**
     * @param ShopArticleCategoryTable $shopArticleCategoryTable
     */
    public function setShopArticleCategoryTable(ShopArticleCategoryTable $shopArticleCategoryTable): void
    {
        $this->shopArticleCategoryTable = $shopArticleCategoryTable;
    }

    /**
     * @param ShopArticleClassTable $shopArticleClassTable
     */
    public function setShopArticleClassTable(ShopArticleClassTable $shopArticleClassTable): void
    {
        $this->shopArticleClassTable = $shopArticleClassTable;
    }

    /**
     * @param ShopArticleStockTable $shopArticleStockTable
     */
    public function setShopArticleStockTable(ShopArticleStockTable $shopArticleStockTable): void
    {
        $this->shopArticleStockTable = $shopArticleStockTable;
    }

    /**
     * @param ShopArticleRatingTable $shopArticleRatingTable
     */
    public function setShopArticleRatingTable(ShopArticleRatingTable $shopArticleRatingTable): void
    {
        $this->shopArticleRatingTable = $shopArticleRatingTable;
    }

    /**
     * @param int $articleId
     * @param bool $onlyActive
     * @return array
     */
    public function getShopArticleById(int $articleId, $onlyActive = false): array
    {
        return $this->shopArticleTable->getShopArticleById($articleId, $onlyActive);
    }

    /**
     * @param int $articleId
     * @param bool $onlyActive
     * @return array
     */
    public function getShopArticleCompleteById(int $articleId, $onlyActive = false): array
    {
        return $this->shopArticleTable->getShopArticleCompleteById($articleId, $onlyActive);
    }

    /**
     *
     * @return array
     */
    public function getShopArticles()
    {
        return $this->shopArticleTable->searchShopArticlesWithParams();
    }

    /**
     *
     * @param $articleType
     * @return array
     */
    public function getShopArticlesByType($articleType)
    {
        return $this->shopArticleTablex->getShopArticlesByType($articleType);
    }

    /**
     * @param int $articleId
     * @return array
     */
    public function getShopArticleCategoriesForShopArticle(int $articleId): array
    {
        return $this->shopArticleCategoryRelationTable->getShopArticleCategoriesForShopArticle($articleId);
    }

    /**
     * @param $articleData
     * @return int The new article ID.
     */
    public function saveShopArticle($articleData): int
    {
        return $this->shopArticleTable->saveShopArticle($articleData);
    }

    /**
     * @param int $articleId
     * @return array
     */
    public function getShopArticleGroupRelationArticles(int $articleId)
    {
        return $this->shopArticleTablex->getShopArticleGroupRelationArticles($articleId);
    }

    /**
     * @param int $articleId
     * @param int $relationArticleId
     * @return bool
     */
    public function addOrUpdateShopArticleGroupRelation(int $articleId, int $relationArticleId)
    {
        return $this->shopArticleTablex->addOrUpdateShopArticleGroupRelation($articleId, $relationArticleId);
    }

    /**
     * @param int $shopArticleGroupRelationId
     * @param $shopArticleGroupRelationAmount
     * @return int
     */
    public function updateShopArticleGroupRelationAmount(int $shopArticleGroupRelationId, $shopArticleGroupRelationAmount)
    {
        return $this->shopArticleGroupRelationTable->updateShopArticleGroupRelationAmount($shopArticleGroupRelationId, $shopArticleGroupRelationAmount);
    }

    /**
     * @param int $shopArticleGroupRelationId
     * @return int
     */
    public function deleteShopArticleGroupRelation(int $shopArticleGroupRelationId)
    {
        return $this->shopArticleGroupRelationTable->deleteShopArticleGroupRelation($shopArticleGroupRelationId);
    }

    /**
     * @param int $articleId
     * @return array
     */
    public function getShopArticleSizeGroupRelationArticles(int $articleId): array
    {
        return $this->shopArticleTablex->getShopArticleSizeGroupRelationArticles($articleId);
    }

    public function addOrUpdateShopArticleSizeGroupRelation($articleId, $relationArticleId)
    {
        return $this->shopArticleTablex->addOrUpdateShopArticleSizeGroupRelation($articleId, $relationArticleId);
    }

    public function updateShopArticleSizeGroupRelationAmount($shopArticleSizeGroupRelationId, $shopArticleGroupRelationAmount)
    {
        return $this->shopArticleSizeGroupRelationTable->updateShopArticleSizeGroupRelationAmount($shopArticleSizeGroupRelationId, $shopArticleGroupRelationAmount);
    }

    public function deleteShopArticleSizeGroupRelation($shopArticleSizeGroupRelationId)
    {
        return $this->shopArticleSizeGroupRelationTable->deleteShopArticleSizeGroupRelation($shopArticleSizeGroupRelationId);
    }

    public function getShopArticleSizeDefById($shopArticleSizeDefId)
    {
        return $this->shopArticleSizeDefTable->getShopArticleSizeDefById($shopArticleSizeDefId);
    }

    public function getShopArticleCategories(): array
    {
        return $this->shopArticleCategoryTable->getShopArticleCategories();
    }

    public function getShopArticleCategoryIdAssoc($idParent = 0): array
    {
        return $this->shopArticleCategoryTable->getShopArticleCategoryIdAssoc($idParent);
    }

    public function getShopArticleClasssIdAssoc(): array
    {
        return $this->shopArticleClassTable->getShopArticleClasssIdAssoc();
    }

    public function getShopArticleCategoryById($categoryId): array
    {
        return $this->shopArticleCategoryTable->getShopArticleCategoryById($categoryId);
    }

    public function getShopArticleStandardGroupableExcludeArticleId(int $articleId)
    {
        return $this->shopArticleTable->getShopArticleStandardGroupableExcludeArticleId($articleId);
    }

    public function getShopArticleForSizeGroup($articleId, $sizeGroupId)
    {
        return $this->shopArticleTablex->getShopArticleForSizeGroup($articleId, $sizeGroupId);
    }

    public function getShopArticleCategoryByAlias(string $articleCategoryAlias): array
    {
        return $this->shopArticleCategoryTable->getShopArticleCategoryByAlias($articleCategoryAlias);
    }

    public function getShopArticlesActiveByCategoryAlias(string $articleCategoryAlias): array
    {
        return $this->shopArticleTablex->getShopArticlesActiveByCategoryAlias($articleCategoryAlias);
    }

    public function getShopArticlesActiveByCategoryIdAndClassId(int $shopArticleCategoryId, int $shopArticleClassId): array
    {
        return $this->shopArticleTable->getShopArticlesActiveByCategoryIdAndClassId($shopArticleCategoryId, $shopArticleClassId);
    }

    public function getShopArticlesActive(): array
    {
        return $this->shopArticleTablex->getShopArticlesActive();
    }

    public function saveShopArticleRating(int $articleId, int $ratingValue, string $ratingText): bool
    {
        return $this->shopArticleRatingTable->saveShopArticleRating($articleId, $ratingValue, $ratingText) > 0;
    }

    public function getShopArticleBySefurl(string $articleSefurl, $onlyActive = false): array
    {
        return $this->shopArticleTable->getShopArticleBySefurl($articleSefurl, $onlyActive);
    }

    public function existShopBasketEntityRating(int $basketItemId): bool
    {
        return $this->shopArticleRatingTable->existShopBasketEntityRating($basketItemId);
    }

    public function searchShopArticles(string $searchText, string $categoryAlias, int $classId, string $type, string $typeDelivery): array
    {
        $category = $this->shopArticleCategoryTable->getShopArticleCategoryByAlias($categoryAlias);
        if (!empty($category)) {
            $categoryId = intval($category['shop_article_category_id']);
        } else {
            $categoryId = 0;
        }
        return $this->shopArticleTable->searchShopArticlesWithParams($searchText, $categoryId, $classId, $type, $typeDelivery);
    }
}
