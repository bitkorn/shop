<?php

namespace Bitkorn\Shop\Service\Article;

use Bitkorn\Shop\Entity\Article\ShopArticleOptionsEntity;
use Bitkorn\Shop\Entity\Article\TypeDelivery\ShopArticleLengthcutDefEntity;
use Bitkorn\Shop\Entity\Basket\XshopBasketEntity;
use Bitkorn\Shop\Service\Article\TypeDelivery\ShopArticleLengthcutDefService;
use Bitkorn\Shop\Table\Article\ShopArticleStockTable;
use Bitkorn\Shop\Table\Article\ShopArticleTable;
use Bitkorn\Shop\View\Helper\Article\Option\ShopArticleOption;
use Bitkorn\Trinket\Service\AbstractService;

class ShopArticleStockService extends AbstractService
{
    /**
     * @var ShopArticleStockTable
     */
    protected $shopArticleStockTable;

    /**
     * @var ShopArticleTable
     */
    protected $shopArticleTable;

    /**
     * @var ShopArticleLengthcutDefService
     */
    protected $shopArticleLengthcutDefService;

    /**
     * @param ShopArticleStockTable $shopArticleStockTable
     */
    public function setShopArticleStockTable(ShopArticleStockTable $shopArticleStockTable): void
    {
        $this->shopArticleStockTable = $shopArticleStockTable;
    }

    /**
     * @param ShopArticleTable $shopArticleTable
     */
    public function setShopArticleTable(ShopArticleTable $shopArticleTable): void
    {
        $this->shopArticleTable = $shopArticleTable;
    }

    /**
     * @param ShopArticleLengthcutDefService $shopArticleLengthcutDefService
     */
    public function setShopArticleLengthcutDefService(ShopArticleLengthcutDefService $shopArticleLengthcutDefService): void
    {
        $this->shopArticleLengthcutDefService = $shopArticleLengthcutDefService;
    }

    /**
     * @param array $data
     * @return int
     */
    public function saveShopArticleStockData(array $data): int
    {
        return $this->shopArticleStockTable->saveShopArticleStockData($data);
    }

    /**
     * Summarized stock for one article.
     *
     * @param int $articleId
     * @param string $articleOptionsJson
     * @param int $param2dItemId
     * @return array ['shop_article_stock_amount_sum' => 0, 'shop_article_active' => 1]
     */
    public function searchShopArticleStock(int $articleId, string $articleOptionsJson = '', int $param2dItemId = 0): array
    {
        return $this->shopArticleStockTable->searchShopArticleStock($articleId, $articleOptionsJson, $param2dItemId);
    }

    /**
     * @param int $articleId
     * @param string $articleOptionsJson
     * @param int $param2dItemId
     * @return array All stock entries for one article
     */
    public function searchShopArticleStockList(int $articleId, string $articleOptionsJson = '', int $param2dItemId = 0): array
    {
        return $this->shopArticleStockTable->searchShopArticleStockList($articleId, $articleOptionsJson, $param2dItemId);
    }

    public function isShopArticleStockUsing(int $articleId): bool
    {
        return $this->shopArticleTable->isShopArticleStockUsing($articleId) == 1;
    }

    /**
     * @param int $articleId
     * @param string $shopArticleOptionsJson
     * @param int $param2dItemId
     * @return float
     */
    public function getShopArticleStockAmount(int $articleId, string $shopArticleOptionsJson = '[]', int $param2dItemId = 0): float
    {
        return $this->shopArticleStockTable->getShopArticleStockAmount($articleId, $shopArticleOptionsJson, $param2dItemId);
    }

    public function checkStockAvailability(int $articleId, float $quantity, ShopArticleOptionsEntity $optionsEntity, int $param2dItemId, ShopArticleLengthcutDefEntity $lengthcutDefEntity): bool
    {
        if(!$this->isShopArticleStockUsing($articleId)) {
            return true;
        }
        $stockAvailable = $this->getShopArticleStockAmount($articleId, $optionsEntity->getShopArticleOptionsJson(), $param2dItemId);
        if($lengthcutDefEntity && $lengthcutDefEntity->getLengthPieces() > 0) {
            $quantity = $this->computeQuantityStockForLengthcut($lengthcutDefEntity->getLengthPieces(), $lengthcutDefEntity->getQuantity(), $lengthcutDefEntity->getLengthcutDefBasequantity());
        }
        return $quantity <= $stockAvailable;
    }

    /**
     * @param XshopBasketEntity $xshopBasketEntity
     * @param int $time
     * @return bool
     */
    public function manageStockOut(XshopBasketEntity $xshopBasketEntity, int $time): bool
    {
        if (empty($time)) {
            $time = time();
        }
        $returnVal = true;
        $storageItems = $xshopBasketEntity->getStorageItems();
        $optionsEntity = new ShopArticleOptionsEntity();
        foreach ($storageItems as $storageItem) {
            if (!empty($storageItem['shop_article_stock_using'])) {
                if (!empty($storageItem['shop_basket_item_article_options']) && $storageItem['shop_basket_item_article_options'] != '[]') {
                    $optionsEntity->exchangeArrayOptions(json_decode($storageItem['shop_basket_item_article_options'], true));
                    if ($this->shopArticleStockTable->saveShopArticleStock($storageItem['shop_article_id'],
                            $storageItem['shop_article_amount'] * -1,
                            $optionsEntity->getShopArticleOptionsJson(true),
                            null,
                            'basket',
                            'shop_basket_item_id ' . $storageItem['shop_basket_item_id'], $time) < 1) {
                        $returnVal = false;
                    }
                }
                /**
                 * WITHOUT lengthcut options
                 */
                if (!empty($storageItem['shop_article_param2d_item_id']) && empty($storageItem['shop_basket_item_lengthcut_options'])) {
                    if ($this->shopArticleStockTable->saveShopArticleStock($storageItem['shop_article_id'],
                            $storageItem['shop_article_amount'] * -1,
                            '[]',
                            $storageItem['shop_article_param2d_item_id'],
                            'basket',
                            'shop_basket_item_id ' . $storageItem['shop_basket_item_id'], $time) < 1) {
                        $returnVal = false;
                    }
                }
                /**
                 * WITH lengthcut options
                 * {"lengthcut_def_id":2,"cutprice":14.8,"length_pieces":1000,"quantity":7,"quantity_cuts":5}
                 */
                if (!empty($storageItem['shop_article_param2d_item_id']) && !empty($storageItem['shop_basket_item_lengthcut_options'])) {
                    $lengthcutOptions = json_decode($storageItem['shop_basket_item_lengthcut_options'], true);
                    if (empty($lengthcutOptions) || !is_array($lengthcutOptions) || !isset($lengthcutOptions['lengthcut_def_id'])) {
                        $this->logger->emerg(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__
                            . ' error while json_decode(shop_basket_item_lengthcut_options) - basket_unique: ' . $storageItem['shop_basket_unique']);
                        continue;
                    }
                    $lengthcutDef = $this->shopArticleLengthcutDefService->getLengthcutDef($lengthcutOptions['lengthcut_def_id']);
                    if(empty($lengthcutDef)) {
                        $this->logger->emerg(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__
                            . ' can not find lengthcutDef for lengthcut_def_id ' . $lengthcutOptions['lengthcut_def_id']);
                        continue;
                    }
                    $quantityStock = $this->computeQuantityStockForLengthcut($lengthcutOptions['length_pieces'], $lengthcutOptions['quantity'], $lengthcutDef['shop_article_lengthcut_def_basequantity']);
                    if ($this->shopArticleStockTable->saveShopArticleStock($storageItem['shop_article_id'],
                            $quantityStock * -1,
                            '[]',
                            $storageItem['shop_article_param2d_item_id'],
                            'basket',
                            'shop_basket_item_id ' . $storageItem['shop_basket_item_id'], $time) < 1) {
                        $returnVal = false;
                    }
                }
            }
        }
        return $returnVal;
    }

    /**
     * @param float $lengtPieces
     * @param int $quantityPieces
     * @param float $lengtcutDefBasequantity
     * @return float
     */
    protected function computeQuantityStockForLengthcut(float $lengtPieces, int $quantityPieces, float $lengtcutDefBasequantity): float
    {
        return round(($lengtPieces * $quantityPieces) / $lengtcutDefBasequantity, 2);
    }
}
