<?php

namespace Bitkorn\Shop\Service\Article;

use Bitkorn\Shop\Table\Article\Option\ShopArticleOptionArticleRelationTable;
use Bitkorn\Shop\Table\Article\Option\ShopArticleOptionDefTable;
use Bitkorn\Shop\Table\Article\Option\ShopArticleOptionItemArticleImageTable;
use Bitkorn\Shop\Table\Article\Option\ShopArticleOptionItemArticlePricediffTable;
use Bitkorn\Shop\Table\Article\Option\ShopArticleOptionItemTable;
use Bitkorn\Shop\Table\Article\ShopArticleTable;
use Bitkorn\Shop\Tablex\Article\Option\ShopArticleOptionTablex;
use Bitkorn\Trinket\Service\AbstractService;

class ShopArticleOptionService extends AbstractService
{

    /**
     *
     * @var ShopArticleTable
     */
    protected $shopArticleTable;

    /**
     *
     * @var ShopArticleOptionTablex
     */
    protected $shopArticleOptionTablex;

    /**
     *
     * @var ShopArticleOptionDefTable
     */
    protected $shopArticleOptionDefTable;

    /**
     *
     * @var ShopArticleOptionItemTable
     */
    protected $shopArticleOptionItemTable;

    /**
     *
     * @var ShopArticleOptionItemArticleImageTable
     */
    protected $shopArticleOptionItemArticleImageTable;

    /**
     *
     * @var ShopArticleOptionItemArticlePricediffTable
     */
    protected $shopArticleOptionItemArticlePricediffTable;

    /**
     *
     * @var ShopArticleOptionArticleRelationTable
     */
    protected $shopArticleOptionArticleRelationTable;

    /**
     * @param ShopArticleTable $shopArticleTable
     */
    public function setShopArticleTable(ShopArticleTable $shopArticleTable): void
    {
        $this->shopArticleTable = $shopArticleTable;
    }

    /**
     * @param ShopArticleOptionTablex $shopArticleOptionTablex
     */
    public function setShopArticleOptionTablex(ShopArticleOptionTablex $shopArticleOptionTablex): void
    {
        $this->shopArticleOptionTablex = $shopArticleOptionTablex;
    }

    /**
     * @param ShopArticleOptionDefTable $shopArticleOptionDefTable
     */
    public function setShopArticleOptionDefTable(ShopArticleOptionDefTable $shopArticleOptionDefTable): void
    {
        $this->shopArticleOptionDefTable = $shopArticleOptionDefTable;
    }

    /**
     * @param ShopArticleOptionItemTable $shopArticleOptionItemTable
     */
    public function setShopArticleOptionItemTable(ShopArticleOptionItemTable $shopArticleOptionItemTable): void
    {
        $this->shopArticleOptionItemTable = $shopArticleOptionItemTable;
    }

    /**
     * @param ShopArticleOptionItemArticleImageTable $shopArticleOptionItemArticleImageTable
     */
    public function setShopArticleOptionItemArticleImageTable(ShopArticleOptionItemArticleImageTable $shopArticleOptionItemArticleImageTable): void
    {
        $this->shopArticleOptionItemArticleImageTable = $shopArticleOptionItemArticleImageTable;
    }

    /**
     * @param ShopArticleOptionItemArticlePricediffTable $shopArticleOptionItemArticlePricediffTable
     */
    public function setShopArticleOptionItemArticlePricediffTable(ShopArticleOptionItemArticlePricediffTable $shopArticleOptionItemArticlePricediffTable): void
    {
        $this->shopArticleOptionItemArticlePricediffTable = $shopArticleOptionItemArticlePricediffTable;
    }

    /**
     * @param ShopArticleOptionArticleRelationTable $shopArticleOptionArticleRelationTable
     */
    public function setShopArticleOptionArticleRelationTable(ShopArticleOptionArticleRelationTable $shopArticleOptionArticleRelationTable): void
    {
        $this->shopArticleOptionArticleRelationTable = $shopArticleOptionArticleRelationTable;
    }

    /**
     * @param int $articleOptionDefId
     * @param int $articleId
     * @return int
     */
    public function addShopArticleOptionArticleRelation(int $articleOptionDefId, int $articleId)
    {
        return $this->shopArticleOptionArticleRelationTable->addShopArticleOptionArticleRelation($articleOptionDefId, $articleId);
    }

    /**
     * @param int $articleOptionDefId
     * @param int $articleId
     * @return int
     */
    public function deleteShopArticleOptionArticleRelation(int $articleOptionDefId, int $articleId)
    {
        return $this->shopArticleOptionArticleRelationTable->deleteShopArticleOptionArticleRelation($articleOptionDefId, $articleId);
    }

    /**
     * @return array
     */
    public function getShopArticleOptionDefs(): array
    {
        return $this->shopArticleOptionTablex->getShopArticleOptionDefs();
    }

    /**
     * @param int $articleId
     * @return array
     */
    public function getShopArticleOptionArticleRelations(int $articleId)
    {
        return $this->shopArticleOptionArticleRelationTable->getShopArticleOptionArticleRelations($articleId);;
    }

    /**
     * @param int $optionItemId
     * @param int $articleId
     * @param int $bkImagesImageId
     * @return int
     */
    public function addShopArticleOptionItemArticleImageRelation(int $optionItemId, int $articleId, int $bkImagesImageId): int
    {
        return $this->shopArticleOptionItemArticleImageTable->addShopArticleOptionItemArticleImageRelation($optionItemId, $articleId, $bkImagesImageId);
    }

    /**
     * @param int $optionItemArticleImageRelationId
     * @return bool
     */
    public function deleteShopArticleOptionItemArticleImageRelation(int $optionItemArticleImageRelationId): bool
    {
        return $this->shopArticleOptionItemArticleImageTable->deleteShopArticleOptionItemArticleImageRelation($optionItemArticleImageRelationId) > 0;
    }

    /**
     * @param int $optionItemArticleImageRelationId
     * @param int $optionItemArticleImageRelationPriority
     * @return bool
     */
    public function updateShopArticleOptionItemArticleImageRelationPriority(int $optionItemArticleImageRelationId, int $optionItemArticleImageRelationPriority): bool
    {
        return $this->shopArticleOptionItemArticleImageTable->updateShopArticleOptionItemArticleImageRelationPriority($optionItemArticleImageRelationId, $optionItemArticleImageRelationPriority) >= 0;
    }

    /**
     * @param int $defId
     * @return array
     */
    public function getShopArticleOptionDefById(int $defId): array
    {
        return $this->shopArticleOptionDefTable->getShopArticleOptionDefById($defId);
    }

    public function getShopArticleOptionItemsIdAssoc(int $defId): array
    {
        return $this->shopArticleOptionItemTable->getShopArticleOptionItemsIdAssoc($defId);
    }

    public function getShopArticleOptionDefArticleImagesBoth(int $defId, int $articleId): array
    {
        return $this->shopArticleOptionTablex->getShopArticleOptionDefArticleImagesBoth($defId, $articleId);
    }

    public function updateShopArticleOptionItemArticlePricediff(int $articleId, int $itemId, $newVal): int
    {
        return $this->shopArticleOptionItemArticlePricediffTable->updateShopArticleOptionItemArticlePricediff($articleId, $itemId, $newVal);
    }

    public function getShopArticleOptionItemArticlePricediffsIdsAssoc(array $shopArticleOptionItemIds, int $articleId): array
    {
        return $this->shopArticleOptionItemArticlePricediffTable->getShopArticleOptionItemArticlePricediffsIdsAssoc($shopArticleOptionItemIds, $articleId);
    }

    public function getShopArticleOptionItemsByDefId(int $defId): array
    {
        return $this->shopArticleOptionItemTable->getShopArticleOptionItemsByDefId($defId);
    }

    public function getShopArticleOptionItemArticleImages(int $optionItemId, int $articleId): array
    {
        return $this->shopArticleOptionTablex->getShopArticleOptionItemArticleImages($optionItemId, $articleId);
    }

    public function getShopArticleOptionItemArticleRelationsIdAssoc(int $articleId, bool $askPricediff = false)
    {
        return $this->shopArticleOptionTablex->getShopArticleOptionItemArticleRelationsIdAssoc($articleId, true);
    }
}
