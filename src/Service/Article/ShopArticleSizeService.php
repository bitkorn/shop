<?php

namespace Bitkorn\Shop\Service\Article;

use Bitkorn\Shop\Table\Article\ShopArticleSizeGroupRelationTable;
use Bitkorn\Shop\Table\Article\Size\ShopArticleSizeDefTable;
use Bitkorn\Shop\Table\Article\Size\ShopArticleSizeGroupTable;
use Bitkorn\Shop\Table\Article\Size\ShopArticleSizeItemTable;
use Bitkorn\Shop\Table\Article\Size\ShopArticleSizePositionTable;
use Bitkorn\Shop\Tablex\Article\Size\ShopArticleSizeTablex;
use Bitkorn\Trinket\Service\AbstractService;

class ShopArticleSizeService extends AbstractService
{
    /**
     *
     * @var ShopArticleSizeTablex
     */
    protected $shopArticleSizeTablex;

    /**
     *
     * @var ShopArticleSizeDefTable
     */
    protected $shopArticleSizeDefTable;

    /**
     *
     * @var ShopArticleSizeItemTable
     */
    protected $shopArticleSizeItemTable;

    /**
     *
     * @var ShopArticleSizeGroupTable
     */
    protected $shopArticleSizeGroupTable;

    /**
     *
     * @var ShopArticleSizeGroupRelationTable
     */
    protected $shopArticleSizeGroupRelationTable;

    /**
     *
     * @var ShopArticleSizePositionTable
     */
    protected $shopArticleSizePositionTable;

    /**
     * @param ShopArticleSizeTablex $shopArticleSizeTablex
     */
    public function setShopArticleSizeTablex(ShopArticleSizeTablex $shopArticleSizeTablex): void
    {
        $this->shopArticleSizeTablex = $shopArticleSizeTablex;
    }

    /**
     * @param ShopArticleSizeDefTable $shopArticleSizeDefTable
     */
    public function setShopArticleSizeDefTable(ShopArticleSizeDefTable $shopArticleSizeDefTable): void
    {
        $this->shopArticleSizeDefTable = $shopArticleSizeDefTable;
    }

    /**
     * @param ShopArticleSizeItemTable $shopArticleSizeItemTable
     */
    public function setShopArticleSizeItemTable(ShopArticleSizeItemTable $shopArticleSizeItemTable): void
    {
        $this->shopArticleSizeItemTable = $shopArticleSizeItemTable;
    }

    /**
     * @param ShopArticleSizeGroupTable $shopArticleSizeGroupTable
     */
    public function setShopArticleSizeGroupTable(ShopArticleSizeGroupTable $shopArticleSizeGroupTable): void
    {
        $this->shopArticleSizeGroupTable = $shopArticleSizeGroupTable;
    }

    /**
     * @param ShopArticleSizeGroupRelationTable $shopArticleSizeGroupRelationTable
     */
    public function setShopArticleSizeGroupRelationTable(ShopArticleSizeGroupRelationTable $shopArticleSizeGroupRelationTable): void
    {
        $this->shopArticleSizeGroupRelationTable = $shopArticleSizeGroupRelationTable;
    }

    /**
     * @param ShopArticleSizePositionTable $shopArticleSizePositionTable
     */
    public function setShopArticleSizePositionTable(ShopArticleSizePositionTable $shopArticleSizePositionTable): void
    {
        $this->shopArticleSizePositionTable = $shopArticleSizePositionTable;
    }

    /**
     * @param int $articleSizeItemId
     * @param string $articleSizeItemType
     * @param float $articleSizeItemValue
     * @return bool
     */
    public function updateShopArticleSizeItem(int $articleSizeItemId, string $articleSizeItemType, float $articleSizeItemValue): bool
    {
        return $this->shopArticleSizeItemTable->updateShopArticleSizeItem($articleSizeItemId, $articleSizeItemType, $articleSizeItemValue) >= 0;
    }

    public function getShopArticleSizeGroupById(int $sizeGroupId)
    {
        return $this->shopArticleSizeGroupTable->getShopArticleSizeGroupById($sizeGroupId);
    }

    /**
     *
     * @param int $articleSizeGroupId
     * @return array [shop_article_size_position_id => [name => '', desc => '']]
     */
    public function getShopArticleSizePositionIdAssocByArticleSizeGroupId(int $articleSizeGroupId): array
    {
        return $this->shopArticleSizePositionTable->getShopArticleSizePositionIdAssocByArticleSizeGroupId($articleSizeGroupId);
    }

    /**
     *
     * @param int $sizePositionId
     * @param float $sizeItemFrom
     * @param float $sizeItemTo
     * @param int $sizeGroupId
     * @return array Rows from db.shop_article_size_def merged with [shop_article_size_item_from => $sizeItemFrom, shop_article_size_item_to => $sizeItemTo]
     */
    public function getShopArticleSizeDefBySizePositionIdAndSizeItemFromTo(int $sizePositionId, float $sizeItemFrom, float $sizeItemTo, int $sizeGroupId = 0)
    {
        return $this->shopArticleSizeTablex->getShopArticleSizeDefBySizePositionIdAndSizeItemFromTo($sizePositionId, $sizeItemFrom, $sizeItemTo,  $sizeGroupId);
    }
}
