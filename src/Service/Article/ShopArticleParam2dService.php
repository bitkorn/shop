<?php

namespace Bitkorn\Shop\Service\Article;

use Bitkorn\Shop\Table\Article\Param2d\ShopArticleParam2dItemTable;
use Bitkorn\Shop\Table\Article\Param2d\ShopArticleParam2dTable;
use Bitkorn\Trinket\Service\AbstractService;
use Laminas\Log\Logger;

class ShopArticleParam2dService extends AbstractService
{
    /**
     * @var ShopArticleParam2dTable
     */
    protected $shopArticleParam2dTable;

    /**
     * @var ShopArticleParam2dItemTable
     */
    protected $shopArticleParam2dItemTable;

    /**
     * @param ShopArticleParam2dTable $shopArticleParam2dTable
     */
    public function setShopArticleParam2dTable(ShopArticleParam2dTable $shopArticleParam2dTable): void
    {
        $this->shopArticleParam2dTable = $shopArticleParam2dTable;
    }

    /**
     * @param ShopArticleParam2dItemTable $shopArticleParam2dItemTable
     */
    public function setShopArticleParam2dItemTable(ShopArticleParam2dItemTable $shopArticleParam2dItemTable): void
    {
        $this->shopArticleParam2dItemTable = $shopArticleParam2dItemTable;
    }

    /**
     * @return array
     */
    public function getShopArticleParam2dIdAssoc(): array
    {
        return $this->shopArticleParam2dTable->getShopArticleParam2dIdAssoc();
    }

    /**
     * @param int $articleId
     * @return array
     */
    public function getShopArticleParam2dItemsForArticle(int $articleId): array
    {
        return $this->shopArticleParam2dItemTable->getShopArticleParam2dItemsForArticle($articleId);
    }

    /**
     * @param int $articleId
     * @return array key = shop_article_param2d_item_id
     */
    public function getShopArticleParam2dItemsForArticleIdAssoc(int $articleId): array
    {
        return $this->shopArticleParam2dItemTable->getShopArticleParam2dItemsForArticleIdAssoc($articleId);
    }

    public function getShopArticleParam2dItemForParam2dItemId(int $param2dItemId): array
    {
        return $this->shopArticleParam2dItemTable->getShopArticleParam2dItemForParam2dItemId($param2dItemId);
    }
}
