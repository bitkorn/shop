<?php

namespace Bitkorn\Shop\Service\Article\TypeDelivery;

use Bitkorn\Shop\Table\Article\TypeDelivery\ShopArticleLengthcutDefTable;
use Bitkorn\Trinket\Service\AbstractService;
use Laminas\Log\Logger;

class ShopArticleLengthcutDefService extends AbstractService
{
    /**
     * @var ShopArticleLengthcutDefTable
     */
    protected $shopArticleLengthcutDefTable;

    /**
     * @param ShopArticleLengthcutDefTable $shopArticleLengthcutDefTable
     */
    public function setShopArticleLengthcutDefTable(ShopArticleLengthcutDefTable $shopArticleLengthcutDefTable): void
    {
        $this->shopArticleLengthcutDefTable = $shopArticleLengthcutDefTable;
    }

    /**
     * @param int $articleId
     * @return array
     */
    public function getLengthcutDefForArticleId(int $articleId): array
    {
        return $this->shopArticleLengthcutDefTable->getLengthcutDefForArticleId($articleId);
    }

    public function getLengthcutDef(int $lengthcutDefId): array
    {
        return $this->shopArticleLengthcutDefTable->getLengthcutDef($lengthcutDefId);
    }
}
