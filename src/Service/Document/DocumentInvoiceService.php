<?php

namespace Bitkorn\Shop\Service\Document;

use Bitkorn\Shop\Entity\Basket\XshopBasketEntity;
use Bitkorn\Shop\Service\Address\ShopAddressService;
use Bitkorn\Shop\Table\Document\ShopDocumentInvoiceTable;
use Bitkorn\ShopTcpdf\Pdf\Invoice\PdfInvoice;
use Bitkorn\Trinket\Service\AbstractService;
use Bitkorn\Trinket\Service\Filesystem\FolderTool;
use Laminas\Db\Adapter\Adapter;
use Laminas\Db\Adapter\Driver\AbstractConnection;

/**
 *
 * @author allapow
 */
class DocumentInvoiceService extends AbstractService
{

    const INVOICE_NO_LENGTH = 6;

    protected array $shopConfig;
    protected ShopDocumentInvoiceTable $shopDocumentInvoiceTable;
    protected FolderTool $folderTool;
    protected Adapter $adapter;
    protected PdfInvoice $pdfInvoice;
    protected ShopAddressService $shopAddressService;

    public function setShopConfig(array $shopConfig): void
    {
        $this->shopConfig = $shopConfig;
    }

    public function setShopDocumentInvoiceTable(ShopDocumentInvoiceTable $shopDocumentInvoiceTable): void
    {
        $this->shopDocumentInvoiceTable = $shopDocumentInvoiceTable;
    }

    public function setFolderTool(FolderTool $folderTool): void
    {
        $this->folderTool = $folderTool;
    }

    public function setAdapter(Adapter $adapter): void
    {
        $this->adapter = $adapter;
    }

    public function setPdfInvoice(PdfInvoice $pdfInvoice): void
    {
        $this->pdfInvoice = $pdfInvoice;
    }

    public function setShopAddressService(ShopAddressService $shopAddressService): void
    {
        $this->shopAddressService = $shopAddressService;
    }

    /**
     * @param string $basketUnique
     * @return array
     */
    public function getShopDocumentInvoiceByBasketUnique(string $basketUnique): array
    {
        return $this->shopDocumentInvoiceTable->getShopDocumentInvoiceByBasketUnique($basketUnique);
    }

    /**
     * @param int $shopDocumentInvoiceId
     * @return array
     */
    public function getShopDocumentInvoiceById(int $shopDocumentInvoiceId): array
    {
        return $this->shopDocumentInvoiceTable->getShopDocumentInvoiceById($shopDocumentInvoiceId);
    }

    /**
     *
     * @return string
     */
    public function getNewInvoiceNumber()
    {
        $lastShopDocumentInvoice = $this->shopDocumentInvoiceTable->getLastShopDocumentInvoice();
        $newInvoiceNumber = date('Y') . '-3';
        if (!empty($lastShopDocumentInvoice['shop_document_invoice_number'])) {
            if (substr($lastShopDocumentInvoice['shop_document_invoice_number'], 0, 4) == date('Y')) {
                $nextNumber = intval(substr($lastShopDocumentInvoice['shop_document_invoice_number'], 6));
                $newInvoiceNumber .= str_pad($nextNumber + 1, self::INVOICE_NO_LENGTH, '0', STR_PAD_LEFT);
            } else {
                $newInvoiceNumber .= str_pad(1, self::INVOICE_NO_LENGTH, '0', STR_PAD_LEFT);
            }
        } else {
            $newInvoiceNumber .= str_pad(1, self::INVOICE_NO_LENGTH, '0', STR_PAD_LEFT);
        }
        return $newInvoiceNumber;
    }

    /**
     * @param XshopBasketEntity $xshopBasketEntity
     * @param int $unixtime
     * @return array
     */
    public function createDocument(XshopBasketEntity $xshopBasketEntity, $unixtime)
    {
        /** @var AbstractConnection $connection */
        $connection = $this->adapter->getDriver()->getConnection();
        $connection->beginTransaction();

        $newNumber = $this->getNewInvoiceNumber();
        $shopDocumentInvoiceId = $this->shopDocumentInvoiceTable->saveShopDocumentInvoice([
            'shop_document_invoice_number' => $newNumber,
            'shop_basket_unique' => $xshopBasketEntity->getBasketUnique(),
            'shop_document_invoice_time' => $unixtime]);

        if($shopDocumentInvoiceId < 0) {
            $connection->rollback();
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() can not save new db.document');
            return [];
        }
        $year = date('Y', $unixtime);
        $month = date('m', $unixtime);
        $documentFolder = $this->folderTool->computeBrandDateFolder(realpath($this->shopConfig['document_path_absolute']), 'invoice', $year, $month);

        $filename = 'invoice_' . $newNumber . '.pdf';
        $fqfn = $documentFolder . '/' . $filename;

        $addressEntity = $this->shopAddressService->getShopAddressEntityOrdered($xshopBasketEntity->getBasketUnique());
        $this->pdfInvoice->setLang(strtolower($addressEntity->getCountryIso()));
        $this->pdfInvoice->setUserdataAddressRecipient([$addressEntity->getAddressName1and2(), $addressEntity->getAddressStreetWithNumber(), $addressEntity->getAddressZipAndCity(), $addressEntity->getCountryIsoAndName()]);
        $this->pdfInvoice->makeBasics();
        $this->pdfInvoice->setXBasketEntity($xshopBasketEntity);
        if(!$this->pdfInvoice->makeDocument(['invoice_number' => $newNumber, 'invoice_filename' => $filename, 'invoice_path_absolute' => $documentFolder])) {
            $connection->rollback();
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() bitkorn/shop-tcpdf can not create document for invoice ' . $newNumber);
            return [];
        }

        if (file_exists($fqfn)) {
            $updateCount = $this->shopDocumentInvoiceTable->updateShopDocumentInvoice([
                'shop_document_invoice_filename' => $filename,
                'shop_document_invoice_path_absolute' => realpath($documentFolder)
            ], $shopDocumentInvoiceId);
            if ($updateCount == 1) {
                $connection->commit();
                return $this->shopDocumentInvoiceTable->getShopDocumentInvoiceById($shopDocumentInvoiceId);
            }
        } else {
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() Can not create file ' . $fqfn);
        }
        $connection->rollback();
        $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() Can not create document for invoice ' . $newNumber);
        return [];
    }

    public function deleteDocument($basketUnique, $userId)
    {
        if (empty($basketUnique) || !isset($userId)) {
            return -1;
        }
        $documentData = $this->shopDocumentInvoiceTable->getShopDocumentInvoiceByBasketUnique($basketUnique);
        if (empty($documentData)) {
            return 0;
        }
        $dateIdFolder = $this->folderTool->getBrandDateFolder(realpath($this->shopConfig['document_path_absolute']), 'invoice', date('Y', $documentData['shop_document_invoice_time']), date('m', $documentData['shop_document_invoice_time']));
        if (!empty($dateIdFolder) && file_exists($dateIdFolder . '/' . $documentData['shop_document_invoice_filename'])) {
            unlink($dateIdFolder . '/' . $documentData['shop_document_invoice_filename']);
        }
        return $this->shopDocumentInvoiceTable->deleteShopDocumentInvoice($documentData['shop_document_invoice_id']);
    }
}
