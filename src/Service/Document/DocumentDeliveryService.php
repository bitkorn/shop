<?php

namespace Bitkorn\Shop\Service\Document;

use Bitkorn\Shop\Entity\Basket\XshopBasketEntity;
use Bitkorn\Shop\Service\Address\ShopAddressService;
use Bitkorn\Shop\Table\Document\ShopDocumentDeliveryTable;
use Bitkorn\ShopTcpdf\Pdf\Delivery\PdfDelivery;
use Bitkorn\Trinket\Service\AbstractService;
use Bitkorn\Trinket\Service\Filesystem\FolderTool;
use Laminas\Db\Adapter\Adapter;
use Laminas\Db\Adapter\Driver\AbstractConnection;

class DocumentDeliveryService extends AbstractService
{
    const DELIVERY_NO_LENGTH = 6;

    protected array $shopConfig;
    protected ShopDocumentDeliveryTable $shopDocumentDeliveryTable;
    protected FolderTool $folderTool;
    protected Adapter $adapter;
    protected PdfDelivery $pdfDelivery;
    protected ShopAddressService $shopAddressService;

    public function setShopConfig(array $shopConfig): void
    {
        $this->shopConfig = $shopConfig;
    }

    public function setShopDocumentDeliveryTable(ShopDocumentDeliveryTable $shopDocumentDeliveryTable): void
    {
        $this->shopDocumentDeliveryTable = $shopDocumentDeliveryTable;
    }

    public function setFolderTool(FolderTool $folderTool): void
    {
        $this->folderTool = $folderTool;
    }

    public function setAdapter(Adapter $adapter): void
    {
        $this->adapter = $adapter;
    }

    public function setPdfDelivery(PdfDelivery $pdfDelivery): void
    {
        $this->pdfDelivery = $pdfDelivery;
    }

    public function setShopAddressService(ShopAddressService $shopAddressService): void
    {
        $this->shopAddressService = $shopAddressService;
    }

    /**
     * @return string
     */
    public function getNewDeliveryNumber(): string
    {
        $lastShopDocumentDelivery = $this->shopDocumentDeliveryTable->getLastShopDocumentDelivery();
        $newDeliveryNumber = date('Y') . '-2';
        if (!empty($lastShopDocumentDelivery['shop_document_delivery_number'])) {
            if (substr($lastShopDocumentDelivery['shop_document_delivery_number'], 0, 4) == date('Y')) {
                $nextNumber = intval(substr($lastShopDocumentDelivery['shop_document_delivery_number'], 6));
                $newDeliveryNumber .= str_pad($nextNumber + 1, self::DELIVERY_NO_LENGTH, '0', STR_PAD_LEFT);
            } else {
                $newDeliveryNumber .= str_pad(1, self::DELIVERY_NO_LENGTH, '0', STR_PAD_LEFT);
            }
        } else {
            $newDeliveryNumber .= str_pad(1, self::DELIVERY_NO_LENGTH, '0', STR_PAD_LEFT);
        }
        return $newDeliveryNumber;
    }

    /**
     * @param XshopBasketEntity $xshopBasketEntity
     * @param int $unixtime
     * @return array
     */
    public function createDocument(XshopBasketEntity $xshopBasketEntity, $unixtime): array
    {
        /** @var AbstractConnection $connection */
        $connection = $this->adapter->getDriver()->getConnection();
        $connection->beginTransaction();

        $newNumber = $this->getNewDeliveryNumber();
        $shopDocumentDeliveryId = $this->shopDocumentDeliveryTable->saveShopDocumentDelivery([
            'shop_document_delivery_number' => $newNumber,
            'shop_basket_unique' => $xshopBasketEntity->getBasketUnique(),
            'shop_document_delivery_time' => $unixtime]);
        if($shopDocumentDeliveryId < 0) {
            $connection->rollback();
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() can not save new db.document');
            return [];
        }
        $year = date('Y', $unixtime);
        $month = date('m', $unixtime);
        $documentFolder = $this->folderTool->computeBrandDateFolder(realpath($this->shopConfig['document_path_absolute']), 'delivery', $year, $month);

        $filename = 'delivery_' . $newNumber . '.pdf';
        $fqfn = $documentFolder . '/' . $filename;

        $addressEntity = $this->shopAddressService->getShopAddressEntityOrdered($xshopBasketEntity->getBasketUnique());
        $this->pdfDelivery->setLang(strtolower($addressEntity->getCountryIso()));
        $this->pdfDelivery->setUserdataAddressRecipient([$addressEntity->getAddressName1and2(), $addressEntity->getAddressStreetWithNumber(), $addressEntity->getAddressZipAndCity(), $addressEntity->getCountryIsoAndName()]);
        $this->pdfDelivery->makeBasics();
        $this->pdfDelivery->setXBasketEntity($xshopBasketEntity);
        if(!$this->pdfDelivery->makeDocument(['delivery_number' => $newNumber, 'delivery_filename' => $filename, 'delivery_path_absolute' => $documentFolder])) {
            $connection->rollback();
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() bitkorn/shop-tcpdf can not create document for invoice ' . $newNumber);
            return [];
        }

        if (file_exists($fqfn)) {
            $updateCount = $this->shopDocumentDeliveryTable->updateShopDocumentDelivery([
                'shop_document_delivery_filename' => $filename,
                'shop_document_delivery_path_absolute' => realpath($documentFolder)
            ], $shopDocumentDeliveryId);
            if ($updateCount == 1) {
                $connection->commit();
                return $this->shopDocumentDeliveryTable->getShopDocumentDeliveryById($shopDocumentDeliveryId);
            }
        } else {
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() Can not create file ' . $fqfn);
        }
        $connection->rollback();
        $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() Can not create document for invoice ' . $newNumber);
        return [];
    }

    public function deleteDocument(string $basketUnique, $userUuid)
    {
        if (empty($basketUnique) || !isset($userUuid)) {
            return -1;
        }
        $documentData = $this->shopDocumentDeliveryTable->getShopDocumentDeliveryByBasketUnique($basketUnique);
        if (empty($documentData)) {
            return 0;
        }
        $dateIdFolder = $this->folderTool->getDateIdFolder(realpath($this->shopConfig['document_path_absolute']), date('Y', $documentData['shop_document_delivery_time']), date('m', $documentData['shop_document_delivery_time']), $userUuid);
        if (!empty($dateIdFolder) && file_exists($dateIdFolder . '/' . $documentData['shop_document_delivery_filename'])) {
            unlink($dateIdFolder . '/' . $documentData['shop_document_delivery_filename']);
        }
        return $this->shopDocumentDeliveryTable->deleteShopDocumentDelivery($documentData['shop_document_delivery_id']);
    }

    /**
     * @param string $basketUnique
     * @return array
     */
    public function getShopDocumentDeliveryByBasketUnique(string $basketUnique): array
    {
        return $this->shopDocumentDeliveryTable->getShopDocumentDeliveryByBasketUnique($basketUnique);
    }

    /**
     * @param int $shopDocumentDeliveryId
     * @return array
     */
    public function getShopDocumentDeliveryById(int $shopDocumentDeliveryId): array
    {
        return $this->shopDocumentDeliveryTable->getShopDocumentDeliveryById($shopDocumentDeliveryId);
    }

}
