<?php

namespace Bitkorn\Shop\Service\Document;

use Bitkorn\Shop\Entity\Basket\XshopBasketEntity;
use Bitkorn\Shop\Service\Address\ShopAddressService;
use Bitkorn\Shop\Table\Document\ShopDocumentOrderTable;
use Bitkorn\ShopTcpdf\Pdf\Order\PdfOrder;
use Bitkorn\Trinket\Service\AbstractService;
use Bitkorn\Trinket\Service\Filesystem\FolderTool;
use Laminas\Db\Adapter\Adapter;
use Laminas\Db\Adapter\Driver\AbstractConnection;

class DocumentOrderService extends AbstractService
{
    const ORDER_NO_LENGTH = 6;

    protected array $shopConfig;
    protected ShopDocumentOrderTable $shopDocumentOrderTable;
    protected FolderTool $folderTool;
    protected Adapter $adapter;
    protected PdfOrder $pdfOrder;
    protected ShopAddressService $shopAddressService;

    public function setShopConfig(array $shopConfig): void
    {
        $this->shopConfig = $shopConfig;
    }

    public function setShopDocumentOrderTable(ShopDocumentOrderTable $shopDocumentOrderTable): void
    {
        $this->shopDocumentOrderTable = $shopDocumentOrderTable;
    }

    public function setFolderTool(FolderTool $folderTool): void
    {
        $this->folderTool = $folderTool;
    }

    public function setAdapter(Adapter $adapter): void
    {
        $this->adapter = $adapter;
    }

    public function setPdfOrder(PdfOrder $pdfOrder): void
    {
        $this->pdfOrder = $pdfOrder;
    }

    public function setShopAddressService(ShopAddressService $shopAddressService): void
    {
        $this->shopAddressService = $shopAddressService;
    }

    /**
     * @param string $basketUnique
     * @return array
     */
    public function getShopDocumentOrderByBasketUnique(string $basketUnique): array
    {
        return $this->shopDocumentOrderTable->getShopDocumentOrderByBasketUnique($basketUnique);
    }

    /**
     * @param int $shopDocumentOrderId
     * @return array
     */
    public function getShopDocumentOrderById(int $shopDocumentOrderId): array
    {
        return $this->shopDocumentOrderTable->getShopDocumentOrderById($shopDocumentOrderId);
    }

    /**
     * @return string
     */
    public function getNewOrderNumber()
    {
        $lastShopDocumentOrder = $this->shopDocumentOrderTable->getLastShopDocumentOrder();
        $newOrderNumber = date('Y') . '-4';
        if (!empty($lastShopDocumentOrder['shop_document_order_number'])) {

            if (substr($lastShopDocumentOrder['shop_document_order_number'], 0, 4) == date('Y')) {
                $nextNumber = intval(substr($lastShopDocumentOrder['shop_document_order_number'], 6));
                $newOrderNumber .= str_pad($nextNumber + 1, self::ORDER_NO_LENGTH, '0', STR_PAD_LEFT);
            } else {
                $newOrderNumber .= str_pad(1, self::ORDER_NO_LENGTH, '0', STR_PAD_LEFT);
            }
        } else {
            $newOrderNumber .= str_pad(1, self::ORDER_NO_LENGTH, '0', STR_PAD_LEFT);
        }
        return $newOrderNumber;
    }

    public function existDocumentOrder($basketUnique)
    {
        return $this->shopDocumentOrderTable->existShopDocumentOrderForBasketUnique($basketUnique);
    }

    /**
     *
     * @param XshopBasketEntity $xshopBasketEntity
     * @param int $unixtime
     * @return array
     */
    public function createDocument(XshopBasketEntity $xshopBasketEntity, $unixtime)
    {

        /** @var AbstractConnection $connection */
        $connection = $this->adapter->getDriver()->getConnection();
        $connection->beginTransaction();

        $newNumber = $this->getNewOrderNumber();
        $shopDocumentOrderId = $this->shopDocumentOrderTable->saveShopDocumentOrder([
            'shop_document_order_number' => $newNumber,
            'shop_basket_unique' => $xshopBasketEntity->getBasketUnique(),
            'shop_document_order_time' => $unixtime]);
        if ($shopDocumentOrderId < 0) {
            $connection->rollback();
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() can not save new db.document');
            return [];
        }
        $year = date('Y', $unixtime);
        $month = date('m', $unixtime);
        $documentFolder = $this->folderTool->computeBrandDateFolder($this->shopConfig['document_path_absolute'], 'order', $year, $month);

        $filename = 'order_' . $newNumber . '.pdf';
        $fqfn = $documentFolder . '/' . $filename;

        $addressEntity = $this->shopAddressService->getShopAddressEntityOrdered($xshopBasketEntity->getBasketUnique());
        $this->pdfOrder->setLang(strtolower($addressEntity->getCountryIso()));
        $this->pdfOrder->setUserdataAddressRecipient([$addressEntity->getAddressName1and2(), $addressEntity->getAddressStreetWithNumber(), $addressEntity->getAddressZipAndCity(), $addressEntity->getCountryIsoAndName()]);
        $this->pdfOrder->makeBasics();
        $this->pdfOrder->setXBasketEntity($xshopBasketEntity);
        if (!$this->pdfOrder->makeDocument(['order_number' => $newNumber, 'order_filename' => $filename, 'order_path_absolute' => $documentFolder])) {
            $connection->rollback();
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() bitkorn/shop-tcpdf can not create document for order ' . $newNumber);
            return [];
        }

        if (file_exists($fqfn)) {
            $updateCount = $this->shopDocumentOrderTable->updateShopDocumentOrder([
                'shop_document_order_filename' => $filename,
                'shop_document_order_path_absolute' => realpath($documentFolder)
            ], $shopDocumentOrderId);
            if ($updateCount == 1) {
                $connection->commit();
                return $this->shopDocumentOrderTable->getShopDocumentOrderById($shopDocumentOrderId);
            }
        } else {
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() Can not create file ' . $fqfn);
        }
        $connection->rollback();
        $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() Can not create document for order ' . $newNumber);
        return [];
    }

    public function deleteDocument($basketUnique, $userUuid)
    {
        if (empty($basketUnique) || !isset($userUuid)) {
            return -1;
        }
        $documentData = $this->shopDocumentOrderTable->getShopDocumentOrderByBasketUnique($basketUnique);
        if (empty($documentData)) {
            return 0;
        }
        $brandDateFolder = $this->folderTool->getBrandDateFolder(
            realpath($this->shopConfig['document_path_absolute']),
            'order',
            date('Y', $documentData['shop_document_order_time']),
            date('m', $documentData['shop_document_order_time'])
        );
        if (!empty($brandDateFolder) && file_exists($brandDateFolder . '/' . $documentData['shop_document_order_filename'])) {
            unlink($brandDateFolder . '/' . $documentData['shop_document_order_filename']);
        }
        return $this->shopDocumentOrderTable->deleteShopDocumentOrder($documentData['shop_document_order_id']);
    }
}
