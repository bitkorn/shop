<?php

namespace Bitkorn\Shop\Service\Content;

use Bitkorn\Shop\Table\Content\ShopContentTable;
use Bitkorn\Trinket\Service\AbstractService;
use Laminas\Log\Logger;

class ShopContentService extends AbstractService
{
    /**
     * @var ShopContentTable
     */
    protected $shopContentTable;

    /**
     * @param ShopContentTable $shopContentTable
     */
    public function setShopContentTable(ShopContentTable $shopContentTable): void
    {
        $this->shopContentTable = $shopContentTable;
    }

    /**
     * @param string $contentAlias
     * @return array
     */
    public function getContentByAlias(string $contentAlias): array
    {
        return $this->shopContentTable->getContentByAlias($contentAlias);
    }
}
