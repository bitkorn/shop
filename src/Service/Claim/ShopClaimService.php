<?php

namespace Bitkorn\Shop\Service\Claim;

use Bitkorn\Shop\Table\Basket\Claim\ShopBasketClaimTable;
use Bitkorn\Trinket\Service\AbstractService;

class ShopClaimService extends AbstractService
{
    protected ShopBasketClaimTable $shopBasketClaimTable;

    public function setShopBasketClaimTable(ShopBasketClaimTable $shopBasketClaimTable): void
    {
        $this->shopBasketClaimTable = $shopBasketClaimTable;
    }

    public function getShopBasketClaims($onlyUnfinished)
    {
        return $this->shopBasketClaimTable->getShopBasketClaims($onlyUnfinished);
    }

    public function getShopBasketClaim(int $claimId): array
    {
        return $this->shopBasketClaimTable->getShopBasketClaim($claimId);
    }

    public function saveNewShopBasketClaim(array $storage, string $userUuid)
    {
        return $this->shopBasketClaimTable->saveNewShopBasketClaim($storage, $userUuid);
    }

    public function updateShopBasketClaim(int $shopBasketClaimId, array $storage): bool
    {
        return $this->shopBasketClaimTable->updateShopBasketClaim($shopBasketClaimId, $storage) >= 0;
    }
}
