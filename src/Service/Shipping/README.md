
Register new ShippingProvide in module.config.php:
```php
return [
    'bitkorn_shop' => [
        // ...
        'shipping_service_provider' => [
            'dhl' => Service\Shipping\ShippingProvider\ShippingProviderDhl::class,
            'cp' => Service\Shipping\ShippingProvider\ShippingProviderCustomerPickup::class,
        ],
    ]
];
```