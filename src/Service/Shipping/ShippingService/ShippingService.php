<?php

namespace Bitkorn\Shop\Service\Shipping\ShippingService;

use Bitkorn\Shop\Entity\Address\ShopAddressEntity;
use Bitkorn\Shop\Entity\Basket\Address\IsoCountry;
use Bitkorn\Shop\Entity\Basket\XshopBasketEntity;
use Bitkorn\Shop\Service\Shipping\ShippingProvider\AbstractShippingProvider;
use Bitkorn\Shop\Service\Shipping\ShippingProvider\ShippingProviderInterface;
use Bitkorn\Shop\Service\Shipping\ShippingServiceInterface;
use Bitkorn\Shop\Tablex\Basket\ShopBasketTablex;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

/**
 * Description of ShippingService
 *
 * @author allapow
 */
class ShippingService extends ShippingServiceInterface
{

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     *
     * @var [] [uniqueDescriptor => classname]
     */
    private $shippingProviders = [];

    /**
     * @var AbstractShippingProvider[] Instances from $this->shippingProviders
     */
    protected $shippingProvidersInstances;

    /**
     *
     * @var string Choosed shippingProviders UniqueDescriptor
     */
    private $shippingProviderUniqueDescriptor;

    /**
     *
     * @var AbstractShippingProvider Choosed ShippingProvider
     */
    private $shippingProvider;

    /**
     * @var ShopAddressEntity
     */
    protected $shopAddressEntity;

    /**
     * @var XshopBasketEntity
     */
    protected $xshopBasketEntity;

    /**
     * @var IsoCountry
     */
    protected $isoCountry;

    /**
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container): void
    {
        $this->container = $container;
    }

    /**
     * Register all shippingProvider from module.config.php.
     * This function has to be call at create time (factory).
     * @param array $shippingProviders Key = uniqueDescriptor; value = \Bitkorn\Shop\Service\Shipping\ShippingProviderInterface | FactoryInterface
     */
    public function registerShippingProviders(array $shippingProviders)
    {
        foreach ($shippingProviders as $uniqueDescriptor => $classNameString) {
            if (!class_exists($classNameString)) {
                continue;
            }
            $this->shippingProviders[$uniqueDescriptor] = $classNameString;
        }
    }

    /**
     * Sets the shippingProviderUniqueDescriptor and creates the shippingProvider object.
     *
     * @param string $shippingProviderUniqueDescriptor
     * @return bool
     * @throws \Exception
     */
    public function setShippingProviderUniqueDescriptor(string $shippingProviderUniqueDescriptor): bool
    {
        if (!$this->validShippingProviderUniqueDescriptor($shippingProviderUniqueDescriptor)) {
            throw new \InvalidArgumentException(__CLASS__ . '()->' . __FUNCTION__ . '() called with wrong shippingProviderUniqueDescriptor');
        }
        $className = $this->shippingProviders[$shippingProviderUniqueDescriptor];
        if (!class_exists($className)) {
            throw new \InvalidArgumentException(__CLASS__ . '()->' . __FUNCTION__ . '() shippingProvider class does not exist: ' . $className);
        }
        $this->shippingProvider = $this->container->get($className);
        if (!$this->shippingProvider instanceof AbstractShippingProvider) {
            throw new \Exception(__CLASS__ . '()->' . __FUNCTION__ . '() Can not instanciate the shippingProvider with classname: ' . $className);
        }
        $this->shippingProviderUniqueDescriptor = $shippingProviderUniqueDescriptor;
        return true;
    }

    /**
     * @return AbstractShippingProvider[] Instances from $this->shippingProviders.
     */
    public function getShippingProvidersInstances(): array
    {
        if (isset($this->shippingProvidersInstances)) {
            return $this->shippingProvidersInstances;
        }
        foreach ($this->shippingProviders as $key => $className) {
            $shippingProvider = $this->container->get($className);
            if (!isset($shippingProvider) || !$shippingProvider instanceof ShippingProviderInterface) {
                continue;
            }
            if (isset($this->shopAddressEntity)) {
                $shippingProvider->setShopAddressEntity($this->shopAddressEntity);
            }
            if (isset($this->xshopBasketEntity)) {
                $shippingProvider->setXshopBasketEntity($this->xshopBasketEntity);
            }
            $this->shippingProvidersInstances[$key] = $shippingProvider;
        }
        return $this->shippingProvidersInstances;
    }

    public function getShippingProviderInstance(string $shippingProviderUniqueDescriptor): AbstractShippingProvider
    {
        if (!$this->validShippingProviderUniqueDescriptor($shippingProviderUniqueDescriptor)) {
            throw new \InvalidArgumentException(__CLASS__ . '()->' . __FUNCTION__ . '() called with wrong shippingProviderUniqueDescriptor');
        }
        return $this->container->get($this->shippingProviders[$shippingProviderUniqueDescriptor]);
    }

    /**
     *
     * @return AbstractShippingProvider
     */
    public function getShippingProvider()
    {
        return $this->shippingProvider;
    }

    /**
     * @return float
     */
    public function getTotalShippingCosts()
    {
        return $this->shippingProvider->getTotalShippingCosts();
    }

    public function getShippingTaxPercentage()
    {
        return $this->shippingProvider->getShippingTaxPercentage();
    }

    public function setTotalShippingCosts(float $totalShippingCosts)
    {
        $this->shippingProvider->setShippingCostsShipConfirm($totalShippingCosts);
    }

    /**
     * @param float $totalWeight In grams
     */
    public function setTotalWeight(float $totalWeight)
    {
        $this->shippingProvider->setTotalWeight($totalWeight);
    }

    /**
     *
     * @param IsoCountry $isoCountry
     */
    public function setIsoCountry(IsoCountry $isoCountry = null)
    {
        $this->isoCountry = $isoCountry;
        if ($this->issetShippingProvider()) {
            $this->shippingProvider->setIsoCountry($isoCountry);
        }
    }

    /**
     * @param string $shippingProviderUniqueDescriptor
     * @return bool
     */
    public function validShippingProviderUniqueDescriptor(string $shippingProviderUniqueDescriptor): bool
    {
        return isset($this->shippingProviders[$shippingProviderUniqueDescriptor]);
    }

    /**
     * @param XshopBasketEntity $xshopBasketEntity
     */
    public function setXshopBasketEntity(XshopBasketEntity $xshopBasketEntity): void
    {
        $this->xshopBasketEntity = $xshopBasketEntity;
        if ($this->issetShippingProvider()) {
            $this->shippingProvider->setXshopBasketEntity($this->xshopBasketEntity);
        }
    }

    /**
     * @param ShopAddressEntity $shopAddressEntity
     */
    public function setShopAddressEntity(ShopAddressEntity $shopAddressEntity): void
    {
        $this->shopAddressEntity = $shopAddressEntity;
        if ($this->issetShippingProvider()) {
            $this->shippingProvider->setShopAddressEntity($this->shopAddressEntity);
        }
    }

    protected function issetShippingProvider(): bool
    {
        if (empty($this->shippingProviderUniqueDescriptor) || !isset($this->shippingProvider) || !$this->shippingProvider instanceof ShippingProviderInterface) {
            return false;
        }
        return true;
    }
}
