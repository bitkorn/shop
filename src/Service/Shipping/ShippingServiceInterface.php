<?php

namespace Bitkorn\Shop\Service\Shipping;

use Bitkorn\Shop\Entity\Address\ShopAddressEntity;
use Bitkorn\Shop\Entity\Basket\Address\IsoCountry;
use Bitkorn\Shop\Service\Shipping\ShippingProvider\ShippingProviderInterface;

/**
 *
 * @author allapow
 */
abstract class ShippingServiceInterface
{

    /**
     *
     * @param array $shippingProvider Key = uniqueDescriptor; value = \Bitkorn\Shop\Service\Shipping\ShippingProviderInterface
     */
    public abstract function registerShippingProviders(array $shippingProvider);

    /**
     * @param string $shippingProviderUniqueDescriptor
     * @return bool
     */
    public abstract function validShippingProviderUniqueDescriptor(string $shippingProviderUniqueDescriptor): bool;

    /**
     *
     * @param string $shippingProviderUniqueDescriptor The unique descriptor from choosed ShippingProvider
     * @return bool
     */
    public abstract function setShippingProviderUniqueDescriptor(string $shippingProviderUniqueDescriptor): bool;

    /**
     * @return ShippingProviderInterface The ShippingProvider
     */
    public abstract function getShippingProvider();

    public abstract function setTotalWeight(float $totalWeight);

    public abstract function setTotalShippingCosts(float $totalShippingCosts);

    /**
     * @return float The net (netto) shipping costs.
     */
    public abstract function getTotalShippingCosts();

    public abstract function getShippingTaxPercentage();

    public abstract function setIsoCountry(IsoCountry $isoCountry = null);

    public abstract function setShopAddressEntity(ShopAddressEntity $shopAddressEntity): void;
}
