<?php

namespace Bitkorn\Shop\Service\Shipping\ShippingProvider;

use Bitkorn\Shop\Entity\Address\ShopAddressEntity;
use Bitkorn\Shop\Entity\Basket\Address\IsoCountry;

/**
 *
 * @author allapow
 */
interface ShippingProviderInterface
{

    public function getUniqueDescriptor(): string;

    public function getBrand(): string;

    public function getBrandText(): string;

    public function setTotalWeight(float $totalWeight);

    public function setShippingCostsShipConfirm(float $totalShippingCosts);

    public function getShippingCostsRating(): float;

    public function getShippingCostsShipConfirm(): float;

    public function getShippingTaxPercentage(): float;

    public function setIsoCountry(IsoCountry $isoCountry = null);

    public function setShopAddressEntity(ShopAddressEntity $shopAddressEntity): void;
}
