<?php

namespace Bitkorn\Shop\Service\Shipping\ShippingProvider;

use Bitkorn\Shop\Entity\Address\ShopAddressEntity;
use Bitkorn\Shop\Entity\Basket\Address\IsoCountry;
use Bitkorn\Shop\Entity\Basket\XshopBasketEntity;

class AbstractShippingProvider implements ShippingProviderInterface
{

    /**
     * @var float Rated shipping costs for the whole basket
     */
    protected $shippingCostsRating = 0;

    /**
     * @var float Shipping costs for the whole basket
     */
    protected $shippingCostsShipConfirm = 0;

    /**
     * @var float
     */
    protected $totalWeight = 0;

    /**
     *
     * @var IsoCountry
     */
    protected $isoCountry;

    /**
     * @var ShopAddressEntity
     */
    protected $shopAddressEntity;

    /**
     * @var XshopBasketEntity
     */
    protected $xshopBasketEntity;

    /**
     * @param ShopAddressEntity $shopAddressEntity
     */
    public function setShopAddressEntity(ShopAddressEntity $shopAddressEntity): void
    {
        $this->shopAddressEntity = $shopAddressEntity;
    }

    /**
     * Sets the $xshopBasketEntity and some values: totalWeight,
     * @param XshopBasketEntity $xshopBasketEntity
     */
    public function setXshopBasketEntity(XshopBasketEntity $xshopBasketEntity): void
    {
        $this->xshopBasketEntity = $xshopBasketEntity;
        $this->totalWeight = $this->xshopBasketEntity->getArticleWeightSum();
    }

    /**
     * @return string
     */
    public function getUniqueDescriptor(): string
    {
        return '';
    }

    /**
     * @return string
     */
    public function getBrand(): string
    {
        return 'abstract';
    }

    /**
     * @return string
     */
    public function getBrandText(): string
    {
        return 'abstract';
    }

    /**
     * @return float Rated shipping costs for the whole basket
     */
    public function getShippingCostsRating(): float
    {
        return $this->shippingCostsRating;
    }

    /**
     * @return float Shipping costs for the whole basket
     */
    public function getShippingCostsShipConfirm(): float
    {
        return $this->shippingCostsShipConfirm;
    }

    /**
     * @return float If not empty THEN shippingCostsShipConfirm ELSE shippingCostsRating
     */
    public function getTotalShippingCosts(): float
    {
        if($this->shippingCostsShipConfirm > 0) {
            return $this->shippingCostsShipConfirm;
        }
        return $this->shippingCostsRating;
    }

    /**
     * @return float
     */
    public function getShippingTaxPercentage(): float
    {
        return 19;
    }

    /**
     * @param float $shippingCostsShipConfirm
     */
    public function setShippingCostsShipConfirm(float $shippingCostsShipConfirm)
    {
        $this->shippingCostsShipConfirm = $shippingCostsShipConfirm;
    }

    /**
     * @return float
     */
    public function getTotalWeight(): float
    {
        return $this->totalWeight;
    }

    /**
     * @param float $totalWeight In gram
     */
    public function setTotalWeight(float $totalWeight)
    {
        $this->totalWeight = $totalWeight;
    }

    /**
     * @param IsoCountry $isoCountry
     */
    public function setIsoCountry(IsoCountry $isoCountry = null)
    {
        $this->isoCountry = $isoCountry;
    }
}
