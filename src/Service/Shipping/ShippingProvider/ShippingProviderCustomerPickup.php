<?php

namespace Bitkorn\Shop\Service\Shipping\ShippingProvider;

/**
 * Description of ShippingProviderCustomerPickup
 *
 * @author allapow
 */
class ShippingProviderCustomerPickup extends AbstractShippingProvider
{
    const UNIQUE_DESCRIPTOR = 'cp';

    public function getUniqueDescriptor(): string
    {
        return self::UNIQUE_DESCRIPTOR;
    }

    public function getBrand(): string
    {
        return '<img src="/img/module/customer-pickup/selbstabholung_66x40.png" alt="Selbstabholung">';
    }

    /**
     *
     * @return string
     */
    public function getBrandText(): string
    {
        return 'Selbstabholung';
    }

}
