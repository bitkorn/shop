<?php

namespace Bitkorn\Shop\Listener;

use Bitkorn\Mail\Mail\SimpleMailer;
use Bitkorn\Shop\Service\User\ShopUserService;
use Bitkorn\User\Service\UserService;
use Laminas\EventManager\AbstractListenerAggregate;
use Laminas\EventManager\EventInterface;
use Laminas\EventManager\EventManagerInterface;
use Laminas\Log\Logger;

class UserDeleteListener extends AbstractListenerAggregate
{
    protected $listeners = [];

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var ShopUserService
     */
    protected $shopUserService;

    /**
     * @var SimpleMailer
     */
    protected $simpleMailer;

    /**
     * @param Logger $logger
     */
    public function setLogger(Logger $logger): void
    {
        $this->logger = $logger;
    }

    /**
     * @param ShopUserService $shopUserService
     */
    public function setShopUserService(ShopUserService $shopUserService): void
    {
        $this->shopUserService = $shopUserService;
    }

    /**
     * @param SimpleMailer $simpleMailer
     */
    public function setSimpleMailer(SimpleMailer $simpleMailer): void
    {
        $this->simpleMailer = $simpleMailer;
    }

    /**
     * Attach one or more listeners
     *
     * Implementors may add an optional $priority argument; the EventManager
     * implementation will pass this to the aggregate.
     *
     * @param EventManagerInterface $events
     * @param int $priority
     * @return void
     */
    public function attach(EventManagerInterface $events, $priority = 1)
    {
        $this->listeners[] = $events->attach(UserService::EVENT_BEFORE_DELETE_USER, [$this, 'beforeDeleteUser']);
    }

    /**
     * @param EventInterface $event
     * @return string
     */
    public function beforeDeleteUser(EventInterface $event): string
    {
        $params = $event->getParams();
        if (empty($params['user_uuid'])) {
            return '';
        }
        $this->simpleMailer->sendAdminEmail('User deleted with uuid: ' . $params['user_uuid']);
        return $this->shopUserService->deleteShopUserComplete($params['user_uuid']);
    }
}
