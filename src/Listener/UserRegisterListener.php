<?php

namespace Bitkorn\Shop\Listener;

use Bitkorn\Shop\Service\User\ShopUserNumberService;
use Bitkorn\Shop\Table\User\ShopUserDataNumberTable;
use Bitkorn\Shop\Table\User\ShopUserDataTable;
use Bitkorn\Trinket\Controller\Plugin\ValidUuid;
use Bitkorn\User\Service\UserService;
use Laminas\EventManager\AbstractListenerAggregate;
use Laminas\EventManager\EventInterface;
use Laminas\EventManager\EventManagerInterface;
use Laminas\Log\Logger;
use Laminas\Validator\Uuid;

/**
 * Generate a new ShopUserNumber in table db.shop_user_data_number.
 * User is in group db.shop_user_group
 * Save new data in table db.shop_user_data.
 *
 * @author allapow
 */
class UserRegisterListener extends AbstractListenerAggregate
{
    protected $listeners = [];

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var Uuid
     */
    private $uuidValidator;

    /**
     * @var ShopUserNumberService
     */
    private $shopUserNumberService;

    /**
     * @var ShopUserDataTable
     */
    private $shopUserDataTable;

    /**
     * @var ShopUserDataNumberTable
     */
    private $shopUserDataNumberTable;

    /**
     * UserRegisterListener constructor.
     */
    public function __construct()
    {
        $this->uuidValidator = new Uuid();
    }

    /**
     * @param Logger $logger
     */
    public function setLogger(Logger $logger): void
    {
        $this->logger = $logger;
    }

    /**
     * @param ShopUserNumberService $shopUserNumberService
     */
    public function setShopUserNumberService(ShopUserNumberService $shopUserNumberService)
    {
        $this->shopUserNumberService = $shopUserNumberService;
    }

    /**
     * @param ShopUserDataTable $shopUserDataTable
     */
    public function setShopUserDataTable(ShopUserDataTable $shopUserDataTable)
    {
        $this->shopUserDataTable = $shopUserDataTable;
    }

    /**
     * @param ShopUserDataNumberTable $shopUserDataNumberTable
     */
    public function setShopUserDataNumberTable(ShopUserDataNumberTable $shopUserDataNumberTable)
    {
        $this->shopUserDataNumberTable = $shopUserDataNumberTable;
    }

    public function attach(EventManagerInterface $events, $priority = 1)
    {
        $this->listeners[] = $events->attach(UserService::EVENT_AFTER_CREATE_USER, [$this, 'afterCreateUser']);
    }

    /**
     * Generate ShopUserNo if not exist.
     *
     * NUR if !exist weil:
     * Registriert sich ein User, klickt aber noch nicht den Link in der VerifyEmail
     * und kauft dann MIT Registrierung (das hier triggert)
     * und klickt dann den Link in der Verify (das hier triggert wieder)
     * gibt es zwei ShopUserNo für einen User.
     *
     * @param EventInterface $event
     */
    public function afterCreateUser(EventInterface $event)
    {
        $userUuid = $event->getParam('user_uuid');
        if (empty($userUuid) || !$this->uuidValidator->isValid($userUuid)) {
            $this->logger->warn(__CLASS__ . '()->' . __FUNCTION__ . '() called with none valid user_uuid: ' . $userUuid);
            return '';
        }
        if (!$this->shopUserDataNumberTable->exist('user_uuid', $userUuid)) {
            $lastInsertId = $this->shopUserDataNumberTable->createNewShopUserDataNumber($userUuid, 1, $this->shopUserNumberService);
            if ($lastInsertId < 0) {
                throw new \RuntimeException('shopUserDataNumberTable->createNewShopUserDataNumber returns lastInsertId < 0');
            }
            $this->shopUserDataTable->saveNewShopUserData([
                'user_uuid' => $userUuid,
                'shop_user_group_id' => 1,
                'shop_user_data_number_id' => $lastInsertId
            ]);
        }
    }
}
