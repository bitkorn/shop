<?php

namespace Bitkorn\Shop;

use Bitkorn\Shop\Observer\ImageDeleteObserver;
use Bitkorn\Shop\Observer\UserRegisterListener;
use Bitkorn\Shop\Tools\Basket\BasketUnique;
use Bitkorn\Trinket\Observer\ObserverManager;
use Bitkorn\User\Service\UserService;
use Laminas\Log\Logger;
use Psr\Container\ContainerInterface;
use Laminas\EventManager\LazyListener;
use Laminas\ModuleManager\Feature\ValidatorProviderInterface;
use Laminas\ModuleManager\Feature\ConsoleUsageProviderInterface;
use Laminas\Mvc\MvcEvent;

class Module implements ValidatorProviderInterface, ConsoleUsageProviderInterface
{

    public function onBootstrap(MvcEvent $e)
    {
//        $app = $e->getApplication();
//        $eventManager = $app->getEventManager();
//        $sm = $app->getServiceManager();
//        /** @var UserService $userService */
//        $userService = $sm->get(UserService::class);
//        /** @var Logger $logger */
//        $logger = $sm->get('logger');
//        $userService->getEvents()->attach(UserService::EVENT_AFTER_CREATE_USER, [UserRegisterListener::class, 'triggered']);
    }

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    public function getValidatorConfig()
    {
        return [
            'factories' => [
            ]
        ];
    }

    public function getConsoleUsage(\Laminas\Console\Adapter\AdapterInterface $console)
    {
        return [
            // Describe available commands
            'sendratingreminder (all|one) [<basket_entity_id>]' => 'Sending email(s) for basket-entities they do not have receive it yet or only one basket-entity',
            // Describe expected parameters
            ['all', 'Email for all they do not have receive it yet'],
            ['one', 'Email for one basket-entity with ID basket_entity_id'],
            ['basket_entity_id', 'The basket-entity-id only used with parameter one'],
        ];
    }

}
