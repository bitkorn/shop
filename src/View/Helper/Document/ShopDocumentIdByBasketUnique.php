<?php

namespace Bitkorn\Shop\View\Helper\Document;

use Bitkorn\Shop\Table\Document\ShopDocumentDeliveryTable;
use Bitkorn\Shop\Table\Document\ShopDocumentInvoiceTable;
use Bitkorn\Shop\Table\Document\ShopDocumentOrderTable;
use Laminas\View\Helper\AbstractHelper;

/**
 *
 * @author allapow
 */
class ShopDocumentIdByBasketUnique extends AbstractHelper
{

    /**
     *
     * @var ShopDocumentInvoiceTable
     */
    private $shopDocumentInvoiceTable;

    /**
     *
     * @var ShopDocumentOrderTable
     */
    private $shopDocumentOrderTable;

    /**
     *
     * @var ShopDocumentDeliveryTable
     */
    private $shopDocumentDeliveryTable;

    /**
     *
     * @param string $basketUnique
     * @return int
     */
    public function __invoke($basketUnique, $documentType)
    {

        if ($documentType == 'invoice') {
            $doc = $this->shopDocumentInvoiceTable->getShopDocumentInvoiceByBasketUnique($basketUnique);
            return $doc['shop_document_invoice_id'];
        } elseif ($documentType == 'order') {
            $doc = $this->shopDocumentOrderTable->getShopDocumentOrderByBasketUnique($basketUnique);
            return $doc['shop_document_order_id'];
        } elseif ($documentType == 'delivery') {
            $doc = $this->shopDocumentDeliveryTable->getShopDocumentDeliveryByBasketUnique($basketUnique);
            return $doc['shop_document_delivery_id'];
        }

        return -1;
    }

    /**
     *
     * @param ShopDocumentInvoiceTable $shopDocumentInvoiceTable
     */
    public function setShopDocumentInvoiceTable(ShopDocumentInvoiceTable $shopDocumentInvoiceTable)
    {
        $this->shopDocumentInvoiceTable = $shopDocumentInvoiceTable;
    }

    /**
     *
     * @param ShopDocumentOrderTable $shopDocumentOrderTable
     */
    public function setShopDocumentOrderTable(ShopDocumentOrderTable $shopDocumentOrderTable)
    {
        $this->shopDocumentOrderTable = $shopDocumentOrderTable;
    }

    /**
     *
     * @param ShopDocumentDeliveryTable $shopDocumentDeliveryTable
     */
    public function setShopDocumentDeliveryTable(ShopDocumentDeliveryTable $shopDocumentDeliveryTable)
    {
        $this->shopDocumentDeliveryTable = $shopDocumentDeliveryTable;
    }

}
