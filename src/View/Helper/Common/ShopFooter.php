<?php

namespace Bitkorn\Shop\View\Helper\Common;

use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Model\ViewModel;

/**
 *
 * @author allapow
 */
class ShopFooter extends AbstractHelper
{

    const TEMPLATE = 'template/common/shop-footer';

    /**
     *
     * @return string
     */
    public function __invoke()
    {
        $viewModel = new ViewModel();
        $viewModel->setTemplate(self::TEMPLATE);

        return $this->getView()->render($viewModel);
    }

}
