<?php

namespace Bitkorn\Shop\View\Helper\Basket\Admin;

use Bitkorn\Trinket\Table\ToolsTable;
use Laminas\View\Helper\AbstractHelper;

/**
 *
 * @author allapow
 */
class BasketStatusSelect extends AbstractHelper
{

    const TEMPLATE = 'template/basket/admin/basketStatusSelect';

    /**
     *
     * @var ToolsTable
     */
    private $toolsTable;

    /**
     * @param $selectName
     * @param string $current
     * @param string $cssClass
     * @return string
     * @todo $this->toolsTable->getEnumValues call getEnumValuesPostgreSQL()
     */
    public function __invoke($selectName, $current = '', $cssClass = '')
    {
        $viewModel = new \Laminas\View\Model\ViewModel();
        $viewModel->setTemplate(self::TEMPLATE);

        $viewModel->setVariable('selectName', $selectName);
        $viewModel->setVariable('current', $current);
        $viewModel->setVariable('cssClass', $cssClass);

        $basketStatuss = $this->toolsTable->getEnumValuesPostgreSQL('enum_shop_basket_status');
        $viewModel->setVariable('basketStatuss', $basketStatuss);
//        $keyAssoc = [];
//        foreach ($basketStatuss as $basketStatus) {
//            $keyAssoc[$basketStatus] = $basketStatus;
//        }
//        $viewModel->setVariable('keyAssoc', $keyAssoc);

        return $this->getView()->render($viewModel);
    }

    /**
     *
     * @param ToolsTable $toolsTable
     */
    public function setToolsTable(ToolsTable $toolsTable)
    {
        $this->toolsTable = $toolsTable;
    }


}
