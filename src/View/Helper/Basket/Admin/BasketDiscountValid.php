<?php

namespace Bitkorn\Shop\View\Helper\Basket\Admin;

use Laminas\View\Helper\AbstractHelper;
use Bitkorn\Shop\Tools\Basket\DiscountHashValid;

/**
 * Called in view template file with $this->isBasketDiscountValid('', '', 0, 1486892155)
 * @author allapow
 */
class BasketDiscountValid extends AbstractHelper
{

    public function __invoke(string $discountType, string $discountTypeValue, int $discounthashCount, int $discountTime): bool
    {
        return DiscountHashValid::isDiscountHashValid($discountType, $discountTypeValue, $discounthashCount, $discountTime);
    }

}
