<?php

namespace Bitkorn\Shop\View\Helper\Basket\Frontend;

use Bitkorn\Images\Entity\Image\ImagesEntity;
use Bitkorn\Shop\Service\Basket\BasketService;
use Bitkorn\Shop\Service\Shipping\ShippingServiceInterface;
use Bitkorn\Shop\Table\Article\Option\ShopArticleOptionItemArticlePricediffTable;
use Bitkorn\Shop\Table\Basket\ShopBasketEntityTable;
use Bitkorn\Shop\Table\Common\ShopConfigurationTable;
use Bitkorn\Shop\Tablex\Article\ShopArticleImageTablex;
use Bitkorn\Shop\Tablex\Basket\ShopBasketTablex;
use Bitkorn\Trinket\View\Helper\AbstractViewHelper;
use Bitkorn\Shop\Entity\Basket\XshopBasketEntity;
use Laminas\View\Model\ViewModel;

/**
 *
 * @author allapow
 */
class ShopBasketOverview extends AbstractViewHelper
{

    const TEMPLATE = 'template/basket/frontend/shopBasketOverview';

    /**
     * @var BasketService
     */
    protected $basketService;

    /**
     *
     * @var XshopBasketEntity
     */
    protected $xshopBasketEntity;

    /**
     *
     * @var ShopBasketTablex
     */
    private $shopBasketTablex;

    /**
     *
     * @var ShopBasketEntityTable
     */
    private $shopBasketEntityTable;

    /**
     *
     * @var ShopArticleImageTablex
     */
    private $shopArticleImageTablex;

    /**
     *
     * @var ShopArticleOptionItemArticlePricediffTable
     */
    private $shopArticleOptionItemArticlePricediffTable;

    /**
     *
     * @var ShopConfigurationTable
     */
    private $shopConfigurationTable;

    /**
     *
     * @var ShippingServiceInterface
     */
    private $shippingService;

    /**
     * @param BasketService $basketService
     */
    public function setBasketService(BasketService $basketService): void
    {
        $this->basketService = $basketService;
    }

    /**
     * @param ShopBasketTablex $shopBasketTablex
     */
    public function setShopBasketTablex(ShopBasketTablex $shopBasketTablex)
    {
        $this->shopBasketTablex = $shopBasketTablex;
    }

    /**
     *
     * @param ShopBasketEntityTable $shopBasketEntityTable
     */
    public function setShopBasketEntityTable(ShopBasketEntityTable $shopBasketEntityTable)
    {
        $this->shopBasketEntityTable = $shopBasketEntityTable;
    }

    public function setShopArticleImageTablex(ShopArticleImageTablex $shopArticleImageTablex)
    {
        $this->shopArticleImageTablex = $shopArticleImageTablex;
    }

    /**
     *
     * @param ShopArticleOptionItemArticlePricediffTable $shopArticleOptionItemArticlePricediffTable
     */
    public function setShopArticleOptionItemArticlePricediffTable(ShopArticleOptionItemArticlePricediffTable $shopArticleOptionItemArticlePricediffTable)
    {
        $this->shopArticleOptionItemArticlePricediffTable = $shopArticleOptionItemArticlePricediffTable;
    }

    public function setShopConfigurationTable(ShopConfigurationTable $shopConfigurationTable)
    {
        $this->shopConfigurationTable = $shopConfigurationTable;
    }

    /**
     *
     * @param ShippingServiceInterface $shippingService
     */
    public function setShippingService(ShippingServiceInterface $shippingService)
    {
        $this->shippingService = $shippingService;
    }

    /**
     *
     * @param string $basketUnique
     * @param bool $withSession
     * @return string
     */
    public function __invoke($basketUnique, $withSession = true)
    {
        if (
            empty($basketUnique)
            || (empty($this->xshopBasketEntity = $this->basketService->getXshopBasketEntity()) && $withSession)
            || (!$this->xshopBasketEntity instanceof XshopBasketEntity && $withSession)
        ) {
            return '';
        }
        $viewModel = new ViewModel();
        $viewModel->setTemplate(self::TEMPLATE);

        if ($withSession) {
            if ($this->xshopBasketEntity->getBasketUnique() != $basketUnique) {
                $this->xshopBasketEntity = null;
                return $viewModel;
            }
            if (empty($this->xshopBasketEntity->getStorageItems())) {
                return $viewModel;
            }

            $imageEntity = new ImagesEntity();
            $imageEntity->exchangeArrayImages($this->xshopBasketEntity->getImagesData());
            $viewModel->setVariables([
                'shopBasketStatus' => $this->xshopBasketEntity->getBasketStatus(),
                'shopBasketData' => $this->xshopBasketEntity->getStorageItems(),
                'imagesArticleIdAssoc' => $this->xshopBasketEntity->getImagesArticleIdAssoc(),
                'imageEntity' => $imageEntity,
                'articleAmountSum' => $this->xshopBasketEntity->getArticleAmountSum(),
                'articlePriceTotalSum' => $this->xshopBasketEntity->getArticlePriceTotalSum(),
                'articlePriceTotalSumEnd' => $this->xshopBasketEntity->getArticlePriceTotalSumEnd(),
                'basketDiscountHash' => $this->xshopBasketEntity->getBasketDiscountHash(),
                'basketDiscountValue' => $this->xshopBasketEntity->getBasketDiscountValue(),
                'articleShippingCostsComputed' => $this->xshopBasketEntity->getArticleShippingCostsComputed(),
                'basketTaxTotalSumEnd' => $this->xshopBasketEntity->getBasketTaxTotalSumEnd(),
            ]);
        } else {
            $shopBasketEntityData = $this->shopBasketEntityTable->getShopBasketEntitiesByBasketUnique($basketUnique);
            if (!empty($shopBasketEntityData)) {
                $this->xshopBasketEntity = new XshopBasketEntity($basketUnique);
                $this->xshopBasketEntity->flipMapping();
                $this->xshopBasketEntity->exchangeArrayBasket($shopBasketEntityData);
                $viewModel->setVariables([
                    'shopBasketStatus' => $shopBasketEntityData[0]['shop_basket_status'],
                    'shopBasketData' => $this->xshopBasketEntity->getStorageItems(),
                    'articleAmountSum' => $shopBasketEntityData[0]['computed_value_article_amount_sum'],
                    'articlePriceTotalSum' => $shopBasketEntityData[0]['computed_value_article_price_total_sum'],
                    'articlePriceTotalSumEnd' => $shopBasketEntityData[0]['computed_value_article_price_total_sum_end'],
                    'basketDiscountHash' => $shopBasketEntityData[0]['shop_basket_discount_hash'],
                    'basketDiscountValue' => $shopBasketEntityData[0]['shop_basket_discount_value'],
                    'articleShippingCostsComputed' => $shopBasketEntityData[0]['computed_value_article_shipping_costs_computed'],
                    'basketTaxTotalSumEnd' => $shopBasketEntityData[0]['computed_value_shop_basket_tax_total_sum_end'],
                ]);
            }
        }
        $viewModel->setVariable('editable', $this->xshopBasketEntity->getBasketStatus() == 'basket');
        return $this->getView()->render($viewModel);
    }

}
