<?php

namespace Bitkorn\Shop\View\Helper\Basket\Email;

use Bitkorn\Shop\Table\Basket\ShopBasketEntityTable;
use Bitkorn\Shop\Table\Common\ShopConfigurationTable;
use Bitkorn\Shop\Tablex\User\ShopUserTablex;
use Bitkorn\Trinket\View\Helper\AbstractViewHelper;
use Bitkorn\User\Table\User\UserTable;
use Laminas\View\Model\ViewModel;

/**
 *
 * @author allapow
 */
class ShopBasketShipped extends AbstractViewHelper
{

    const TEMPLATE = 'template/basket/email/shopBasketShipped';

    /**
     *
     * @var UserTable
     */
    private $userTable;

    /**
     *
     * @var ShopUserTablex
     */
    private ShopUserTablex $shopUserTablex;

    /**
     *
     * @var ShopConfigurationTable
     */
    private ShopConfigurationTable $shopConfigurationTable;

    /**
     *
     * @var ShopBasketEntityTable
     */
    private ShopBasketEntityTable $shopBasketEntityTable;

    /**
     *
     * @param string $shopBasketUnique
     * @return string
     * @throws \RuntimeException
     */
    public function __invoke($shopBasketUnique)
    {
        $viewModel = new ViewModel();
        $viewModel->setTemplate(self::TEMPLATE);

        $shopBasketEntityData = $this->shopBasketEntityTable->getShopBasketEntitiesByBasketUnique($shopBasketUnique, 1);
        if (empty($shopBasketEntityData)) {
            throw new \RuntimeException(__CLASS__ . '->' . __FUNCTION__ . '(): empty($shopBasketEntityData); $shopBasketUnique: ' . $shopBasketUnique);
        }
        $shopBasketEntity = $shopBasketEntityData[0];
        $viewModel->setVariable('shopBasketEntity', $shopBasketEntity);

        $userWithAddresses = $this->shopUserTablex->getShopUserWithAddresses($shopBasketEntity['user_uuid']);
        if (empty($userWithAddresses['shipment'])) {
            throw new \RuntimeException(__CLASS__ . '->' . __FUNCTION__ . '(): no UserShipmentAddress; $shopBasketUnique: ' . $shopBasketUnique);
        }
        $viewModel->setVariable('userAddressShipment', $userWithAddresses['shipment']);

        $viewModel->setVariable('impressumHtml', $this->shopConfigurationTable->getShopConfigurationValue('impressum_html'));

        return $this->getView()->render($viewModel);
    }

    /**
     *
     * @param UserTable $userTable
     */
    public function setUserTable(UserTable $userTable)
    {
        $this->userTable = $userTable;
    }

    /**
     *
     * @param ShopUserTablex $shopUserTablex
     */
    public function setShopUserTablex(ShopUserTablex $shopUserTablex)
    {
        $this->shopUserTablex = $shopUserTablex;
    }

    /**
     *
     * @param ShopConfigurationTable $shopConfigurationTable
     */
    public function setShopConfigurationTable(ShopConfigurationTable $shopConfigurationTable)
    {
        $this->shopConfigurationTable = $shopConfigurationTable;
    }

    public function setShopBasketEntityTable(ShopBasketEntityTable $shopBasketEntityTable)
    {
        $this->shopBasketEntityTable = $shopBasketEntityTable;
    }

}
