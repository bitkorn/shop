<?php

namespace Bitkorn\Shop\View\Helper\Basket\Email\Confirmation;

use Bitkorn\Shop\Table\Basket\ShopBasketAddressTable;
use Bitkorn\Shop\Table\Common\ShopConfigurationTable;
use Bitkorn\Shop\Table\Document\ShopDocumentOrderTable;
use Bitkorn\Shop\Tablex\User\ShopUserTablex;
use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Model\ViewModel;

/**
 *
 * @author allapow
 */
class ShopBasketPayed extends AbstractHelper
{

    const TEMPLATE = 'template/basket/email/confirmation/shopBasketPayed';

    /**
     *
     * @var ShopDocumentOrderTable
     */
    private $shopDocumentOrderTable;

    /**
     *
     * @var ShopUserTablex
     */
    private $shopUserTablex;

    /**
     *
     * @var ShopBasketAddressTable
     */
    private $shopBasketAddressTable;

    /**
     *
     * @var ShopConfigurationTable
     */
    private $shopConfigurationTable;

    public function __invoke(array $shopbasketEntities)
    {
        if (empty($shopbasketEntities)) {
            return '';
        }
        $viewModel = new ViewModel();
        $viewModel->setTemplate(self::TEMPLATE);

        $viewModel->setVariable('orderNo', $this->shopDocumentOrderTable->getShopDocumentOrderNoByBasketUnique($shopbasketEntities[0]['shop_basket_unique']));

        if ($shopbasketEntities[0]['user_uuid'] > 0) {
            $userWithAddresses = $this->shopUserTablex->getShopUserWithAddresses($shopbasketEntities[0]['user_uuid']);
            if (empty($userWithAddresses['invoice'])) {
                throw new \RuntimeException(__CLASS__ . '->' . __FUNCTION__ . '(): $shopbasketEntities[0] without UserInvoiceAddress; UserId: ' . $shopbasketEntities[0]['user_uuid']);
            }
            $viewModel->setVariable('customerName',
                $userWithAddresses['invoice']['shop_user_address_name1'] . ' ' . $userWithAddresses['invoice']['shop_user_address_name2']);
        } else {
            $shopBasketAdresses = $this->shopBasketAddressTable->getShopBasketAddressesByBasketUnique($shopbasketEntities[0]['shop_basket_unique']);
            $viewModel->setVariable('customerName',
                $shopBasketAdresses['invoice']['shop_basket_address_name1'] . ' ' . $shopBasketAdresses['invoice']['shop_basket_address_name2']);
        }

        $viewModel->setVariable('impressumHtml', $this->shopConfigurationTable->getShopConfigurationValue('impressum_html'));

        return $this->getView()->render($viewModel);
    }

    /**
     *
     * @param ShopDocumentOrderTable $shopDocumentOrderTable
     */
    public function setShopDocumentOrderTable(ShopDocumentOrderTable $shopDocumentOrderTable)
    {
        $this->shopDocumentOrderTable = $shopDocumentOrderTable;
    }

    /**
     *
     * @param ShopUserTablex $shopUserTablex
     */
    public function setShopUserTablex(ShopUserTablex $shopUserTablex)
    {
        $this->shopUserTablex = $shopUserTablex;
    }

    /**
     *
     * @param ShopBasketAddressTable $shopBasketAddressTable
     */
    public function setShopBasketAddressTable(ShopBasketAddressTable $shopBasketAddressTable)
    {
        $this->shopBasketAddressTable = $shopBasketAddressTable;
    }

    /**
     *
     * @param ShopConfigurationTable $shopConfigurationTable
     */
    public function setShopConfigurationTable(ShopConfigurationTable $shopConfigurationTable)
    {
        $this->shopConfigurationTable = $shopConfigurationTable;
    }

}
