<?php

namespace Bitkorn\Shop\View\Helper\Basket\Email\Confirmation;

use Bitkorn\Shop\Table\Basket\ShopBasketAddressTable;
use Bitkorn\Shop\Table\Common\ShopConfigurationTable;
use Bitkorn\Shop\Table\Document\ShopDocumentOrderTable;
use Bitkorn\Shop\Tablex\User\ShopUserTablex;
use Bitkorn\User\Table\User\UserTable;
use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Model\ViewModel;

/**
 *
 * @author allapow
 */
class ShopBasketShipped extends AbstractHelper
{

    const TEMPLATE = 'template/basket/email/confirmation/shopBasketShipped';

    /**
     *
     * @var UserTable
     */
    private $userTable;

    /**
     *
     * @var ShopUserTablex
     */
    private $shopUserTablex;

    /**
     *
     * @var ShopConfigurationTable
     */
    private $shopConfigurationTable;

    /**
     *
     * @var ShopBasketAddressTable
     */
    private $shopBasketAddressTable;

    /**
     *
     * @var ShopDocumentOrderTable
     */
    private $shopDocumentOrderTable;

    /**
     *
     * @var array
     */
    private $isoCountryIdAssoc;

    public function __invoke($shopbasketEntities)
    {
        if (empty($shopbasketEntities)) {
            return '';
        }
        $viewModel = new ViewModel();
        $viewModel->setTemplate(self::TEMPLATE);

        $viewModel->setVariable('basketItems', $shopbasketEntities);

        if ($shopbasketEntities[0]['user_uuid'] > 0) {
            $userWithAddresses = $this->shopUserTablex->getShopUserWithAddresses($shopbasketEntities[0]['user_uuid']);
            if (empty($userWithAddresses['invoice'])) {
                throw new \RuntimeException(__CLASS__ . '->' . __FUNCTION__ . '(): $shopbasketEntities[0] without UserInvoiceAddress; UserId: ' . $shopbasketEntities[0]['user_uuid']);
            }
            $viewModel->setVariable('customerNo', $userWithAddresses['invoice']['shop_user_data_number_varchar']);
            if (!empty($userWithAddresses['shipment'])) {
                $viewModel->setVariable('customerName',
                    $userWithAddresses['shipment']['shop_user_address_name1'] . ' ' . $userWithAddresses['shipment']['shop_user_address_name2']);
                $viewModel->setVariable('addressShipping',
                    $userWithAddresses['shipment']['shop_user_address_name1'] . ' ' . $userWithAddresses['shipment']['shop_user_address_name2']
                    . '<br>' . $userWithAddresses['shipment']['shop_user_address_street'] . ' ' . $userWithAddresses['shipment']['shop_user_address_street_no']
                    . '<br>' . $userWithAddresses['shipment']['shop_user_address_zip'] . ' ' . $userWithAddresses['shipment']['shop_user_address_city']
                    . '<br>' . $this->isoCountryIdAssoc[$userWithAddresses['shipment']['country_id']]
                );
            } else {
                $viewModel->setVariable('customerName',
                    $userWithAddresses['invoice']['shop_user_address_name1'] . ' ' . $userWithAddresses['invoice']['shop_user_address_name2']);
                $viewModel->setVariable('addressShipping',
                    $userWithAddresses['invoice']['shop_user_address_name1'] . ' ' . $userWithAddresses['invoice']['shop_user_address_name2']
                    . '<br>' . $userWithAddresses['invoice']['shop_user_address_street'] . ' ' . $userWithAddresses['invoice']['shop_user_address_street_no']
                    . '<br>' . $userWithAddresses['invoice']['shop_user_address_zip'] . ' ' . $userWithAddresses['invoice']['shop_user_address_city']
                    . '<br>' . $this->isoCountryIdAssoc[$userWithAddresses['invoice']['country_id']]
                );
            }
        } else {
            $shopBasketAdresses = $this->shopBasketAddressTable->getShopBasketAddressesByBasketUnique($shopbasketEntities[0]['shop_basket_unique']);
            if (!empty($shopBasketAdresses['shipment'])) {
                $viewModel->setVariable('customerName',
                    $shopBasketAdresses['shipment']['shop_basket_address_name1'] . ' ' . $shopBasketAdresses['shipment']['shop_basket_address_name2']);
                $viewModel->setVariable('addressShipping',
                    $shopBasketAdresses['shipment']['shop_basket_address_name1'] . ' ' . $shopBasketAdresses['shipment']['shop_basket_address_name2']
                    . '<br>' . $shopBasketAdresses['shipment']['shop_basket_address_street'] . ' ' . $shopBasketAdresses['shipment']['shop_basket_address_street_no']
                    . '<br>' . $shopBasketAdresses['shipment']['shop_basket_address_zip'] . ' ' . $shopBasketAdresses['shipment']['shop_basket_address_city']
                    . '<br>' . $this->isoCountryIdAssoc[$shopBasketAdresses['shipment']['country_id']]
                );
            } else {
                $viewModel->setVariable('customerName',
                    $shopBasketAdresses['invoice']['shop_basket_address_name1'] . ' ' . $shopBasketAdresses['invoice']['shop_basket_address_name2']);
                $viewModel->setVariable('addressShipping',
                    $shopBasketAdresses['invoice']['shop_basket_address_name1'] . ' ' . $shopBasketAdresses['invoice']['shop_basket_address_name2']
                    . '<br>' . $shopBasketAdresses['invoice']['shop_basket_address_street'] . ' ' . $shopBasketAdresses['invoice']['shop_basket_address_street_no']
                    . '<br>' . $shopBasketAdresses['invoice']['shop_basket_address_zip'] . ' ' . $shopBasketAdresses['invoice']['shop_basket_address_city']
                    . '<br>' . $this->isoCountryIdAssoc[$shopBasketAdresses['invoice']['country_id']]
                );
            }
        }

        $viewModel->setVariable('orderDate', date('d.m.Y', $shopbasketEntities[0]['shop_basket_time_create']));
        $viewModel->setVariable('orderNo', $this->shopDocumentOrderTable->getShopDocumentOrderNoByBasketUnique($shopbasketEntities[0]['shop_basket_unique']));
        $viewModel->setVariable('shipmentNo', $shopbasketEntities[0]['shipping_number']);
        $viewModel->setVariable('impressumHtml', $this->shopConfigurationTable->getShopConfigurationValue('impressum_html'));
        $viewModel->setVariable('cancellationRight', $this->shopConfigurationTable->getShopConfigurationValue('cancellation_right_html'));

        return $this->getView()->render($viewModel);
    }

    /**
     *
     * @param UserTable $userTable
     */
    public function setUserTable(UserTable $userTable)
    {
        $this->userTable = $userTable;
    }

    /**
     *
     * @param ShopUserTablex $shopUserTablex
     */
    public function setShopUserTablex(ShopUserTablex $shopUserTablex)
    {
        $this->shopUserTablex = $shopUserTablex;
    }

    /**
     *
     * @param ShopConfigurationTable $shopConfigurationTable
     */
    public function setShopConfigurationTable(ShopConfigurationTable $shopConfigurationTable)
    {
        $this->shopConfigurationTable = $shopConfigurationTable;
    }

    /**
     *
     * @param ShopBasketAddressTable $shopBasketAddressTable
     */
    public function setShopBasketAddressTable(ShopBasketAddressTable $shopBasketAddressTable)
    {
        $this->shopBasketAddressTable = $shopBasketAddressTable;
    }

    /**
     *
     * @param ShopDocumentOrderTable $shopDocumentOrderTable
     */
    public function setShopDocumentOrderTable(ShopDocumentOrderTable $shopDocumentOrderTable)
    {
        $this->shopDocumentOrderTable = $shopDocumentOrderTable;
    }

    public function setIsoCountryIdAssoc($isoCountryIdAssoc)
    {
        $this->isoCountryIdAssoc = $isoCountryIdAssoc;
    }

}
