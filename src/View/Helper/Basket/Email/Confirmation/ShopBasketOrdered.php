<?php

namespace Bitkorn\Shop\View\Helper\Basket\Email\Confirmation;

use Bitkorn\Shop\Entity\Basket\XshopBasketEntity;
use Bitkorn\Shop\Table\Basket\ShopBasketAddressTable;
use Bitkorn\Shop\Table\Common\ShopConfigurationTable;
use Bitkorn\Shop\Table\Document\ShopDocumentOrderTable;
use Bitkorn\Shop\Tablex\User\ShopUserTablex;
use Bitkorn\User\Table\User\UserTable;
use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Model\ViewModel;

/**
 *
 * @author allapow
 */
class ShopBasketOrdered extends AbstractHelper
{

    const TEMPLATE = 'template/basket/email/confirmation/shopBasketOrdered';

    /**
     *
     * @var UserTable
     */
    private $userTable;

    /**
     *
     * @var ShopUserTablex
     */
    private $shopUserTablex;

    /**
     *
     * @var ShopConfigurationTable
     */
    private $shopConfigurationTable;

    /**
     *
     * @var ShopBasketAddressTable
     */
    private $shopBasketAddressTable;

    /**
     *
     * @var ShopDocumentOrderTable
     */
    private $shopDocumentOrderTable;

    /**
     *
     * @var array
     */
    private $isoCountryIdAssoc;

    public function __invoke(XshopBasketEntity $xshopBasketEntity)
    {
        $basketItems = $xshopBasketEntity->getStorageItems();
        if (empty($basketItems)) {
            return '';
        }
        $viewModel = new ViewModel();
        $viewModel->setTemplate(self::TEMPLATE);
        $viewModel->setVariable('xshopBasketEntity', $xshopBasketEntity);
        $viewModel->setVariable('basketItems', $basketItems);
        $viewModel->setVariable('host', $_SERVER['SERVER_NAME']);
        $viewModel->setVariable('url',
            strtolower(substr($_SERVER['SERVER_PROTOCOL'], 0, strpos($_SERVER['SERVER_PROTOCOL'], '/'))) . '://' . $_SERVER['SERVER_NAME']);

        if (!empty($userUuid = $xshopBasketEntity->getUserUuid())) {
            $userWithAddresses = $this->shopUserTablex->getShopUserWithAddresses($userUuid);
            $viewModel->setVariable('customerName',
                $userWithAddresses['invoice']['shop_user_address_name1'] . ' ' . $userWithAddresses['invoice']['shop_user_address_name2']);
            $viewModel->setVariable('customerNo', $userWithAddresses['invoice']['shop_user_data_number_varchar']);
            $viewModel->setVariable('addressInvoice',
                $userWithAddresses['invoice']['shop_user_address_name1'] . ' ' . $userWithAddresses['invoice']['shop_user_address_name2']
                . '<br>' . $userWithAddresses['invoice']['shop_user_address_street'] . ' ' . $userWithAddresses['invoice']['shop_user_address_street_no']
                . '<br>' . $userWithAddresses['invoice']['shop_user_address_zip'] . ' ' . $userWithAddresses['invoice']['shop_user_address_city']
                . '<br>' . $this->isoCountryIdAssoc[$userWithAddresses['invoice']['country_id']]
            );
            if (!empty($userWithAddresses['shipment'])) {
                $viewModel->setVariable('addressShipping',
                    $userWithAddresses['shipment']['shop_user_address_name1'] . ' ' . $userWithAddresses['shipment']['shop_user_address_name2']
                    . '<br>' . $userWithAddresses['shipment']['shop_user_address_street'] . ' ' . $userWithAddresses['shipment']['shop_user_address_street_no']
                    . '<br>' . $userWithAddresses['shipment']['shop_user_address_zip'] . ' ' . $userWithAddresses['shipment']['shop_user_address_city']
                    . '<br>' . $this->isoCountryIdAssoc[$userWithAddresses['shipment']['country_id']]
                );
            }
        } else {
            $shopBasketAdresses = $this->shopBasketAddressTable->getShopBasketAddressesByBasketUnique($xshopBasketEntity->getBasketUnique());
            $viewModel->setVariable('customerName',
                $shopBasketAdresses['invoice']['shop_basket_address_name1'] . ' ' . $shopBasketAdresses['invoice']['shop_basket_address_name2']);
            $viewModel->setVariable('addressInvoice',
                $shopBasketAdresses['invoice']['shop_basket_address_name1'] . ' ' . $shopBasketAdresses['invoice']['shop_basket_address_name2']
                . '<br>' . $shopBasketAdresses['invoice']['shop_basket_address_street'] . ' ' . $shopBasketAdresses['invoice']['shop_basket_address_street_no']
                . '<br>' . $shopBasketAdresses['invoice']['shop_basket_address_zip'] . ' ' . $shopBasketAdresses['invoice']['shop_basket_address_city']
                . '<br>' . $this->isoCountryIdAssoc[$shopBasketAdresses['invoice']['country_id']]
            );
            if (!empty($shopBasketAdresses['shipment'])) {
                $viewModel->setVariable('addressShipping',
                    $shopBasketAdresses['shipment']['shop_basket_address_name1'] . ' ' . $shopBasketAdresses['shipment']['shop_basket_address_name2']
                    . '<br>' . $shopBasketAdresses['shipment']['shop_basket_address_street'] . ' ' . $shopBasketAdresses['shipment']['shop_basket_address_street_no']
                    . '<br>' . $shopBasketAdresses['shipment']['shop_basket_address_zip'] . ' ' . $shopBasketAdresses['shipment']['shop_basket_address_city']
                    . '<br>' . $this->isoCountryIdAssoc[$shopBasketAdresses['shipment']['country_id']]
                );
            }
        }

        $viewModel->setVariable('orderDate', date('d.m.Y', $basketItems[0]['shop_basket_time_create']));
        $viewModel->setVariable('orderNo', $this->shopDocumentOrderTable->getShopDocumentOrderNoByBasketUnique($xshopBasketEntity->getBasketUnique()));
        $viewModel->setVariable('impressumHtml', $this->shopConfigurationTable->getShopConfigurationValue('impressum_html'));
        $viewModel->setVariable('cancellationRight', $this->shopConfigurationTable->getShopConfigurationValue('cancellation_right_html'));

        $viewModel->setVariable('bank_data_owner', $this->shopConfigurationTable->getShopConfigurationValue('bank_data_owner'));
        $viewModel->setVariable('bank_data_iban', $this->shopConfigurationTable->getShopConfigurationValue('bank_data_iban'));
        $viewModel->setVariable('bank_data_bic', $this->shopConfigurationTable->getShopConfigurationValue('bank_data_bic'));

        return $this->getView()->render($viewModel);
    }

    /**
     *
     * @param UserTable $userTable
     */
    public function setUserTable(UserTable $userTable)
    {
        $this->userTable = $userTable;
    }

    /**
     *
     * @param ShopUserTablex $shopUserTablex
     */
    public function setShopUserTablex(ShopUserTablex $shopUserTablex)
    {
        $this->shopUserTablex = $shopUserTablex;
    }

    /**
     *
     * @param ShopConfigurationTable $shopConfigurationTable
     */
    public function setShopConfigurationTable(ShopConfigurationTable $shopConfigurationTable)
    {
        $this->shopConfigurationTable = $shopConfigurationTable;
    }

    /**
     *
     * @param ShopBasketAddressTable $shopBasketAddressTable
     */
    public function setShopBasketAddressTable(ShopBasketAddressTable $shopBasketAddressTable)
    {
        $this->shopBasketAddressTable = $shopBasketAddressTable;
    }

    public function setShopDocumentOrderTable(ShopDocumentOrderTable $shopDocumentOrderTable)
    {
        $this->shopDocumentOrderTable = $shopDocumentOrderTable;
    }

    public function setIsoCountryIdAssoc($isoCountryIdAssoc)
    {
        $this->isoCountryIdAssoc = $isoCountryIdAssoc;
    }

}
