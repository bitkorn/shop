<?php

namespace Bitkorn\Shop\View\Helper\Basket\Email;

use Bitkorn\Shop\Entity\Basket\XshopBasketEntity;
use Bitkorn\Shop\Table\Common\ShopConfigurationTable;
use Bitkorn\Shop\Tablex\User\ShopUserTablex;
use Bitkorn\User\Table\User\UserTable;
use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Model\ViewModel;

/**
 *
 * @author allapow
 */
class ShopBasketSummary extends AbstractHelper
{

    const TEMPLATE = 'template/basket/email/shopBasketSummary';

    /**
     * @var ShopUserTablex
     */
    private $shopUserTablex;

    /**
     *
     * @var ShopConfigurationTable
     */
    private $shopConfigurationTable;

    public function __invoke(XshopBasketEntity $xshopBasketEntity)
    {
        $viewModel = new ViewModel();
        $viewModel->setTemplate(self::TEMPLATE);

        $viewModel->setVariables([
            'xshopBasketEntity' => $xshopBasketEntity,
            'storageItems' => $xshopBasketEntity->getStorageItems(),
            'articleIdCount' => $xshopBasketEntity->getArticleIdCount(),
            'articleAmountSum' => $xshopBasketEntity->getArticleAmountSum(),
            'articlePriceTotalSum' => $xshopBasketEntity->getArticlePriceTotalSum(),
            'articleTaxTotalSum' => $xshopBasketEntity->getArticleTaxTotalSum(),
            'articlePriceTotalSumEnd' => $xshopBasketEntity->getArticlePriceTotalSumEnd(),
            'articleTaxTotalSumEnd' => $xshopBasketEntity->getArticleTaxTotalSumEnd(),
            'basketDiscountHash' => $xshopBasketEntity->getBasketDiscountHash(),
            'basketDiscountValue' => $xshopBasketEntity->getBasketDiscountValue(),
            'basketDiscountTax' => $xshopBasketEntity->getBasketDiscountTax(),
            'articleShippingCostsComputed' => $xshopBasketEntity->getArticleShippingCostsComputed(),
        ]);

        $userWithAddresses = $this->shopUserTablex->getShopUserWithAddresses($xshopBasketEntity->getUserUuid());
        if (empty($userWithAddresses['shipment'])) {
            throw new \RuntimeException(__CLASS__ . '->' . __FUNCTION__ . '(): XshopBasketEntity without UserShipmentAddress; UserUuid: ' . $xshopBasketEntity->getUserUuid());
        }
        $viewModel->setVariable('userAddressShipment', $userWithAddresses['shipment']);

        $viewModel->setVariable('impressumHtml', $this->shopConfigurationTable->getShopConfigurationValue('impressum_html'));

        return $this->getView()->render($viewModel);
    }

    /**
     *
     * @param ShopUserTablex $shopUserTablex
     */
    public function setShopUserTablex(ShopUserTablex $shopUserTablex)
    {
        $this->shopUserTablex = $shopUserTablex;
    }

    /**
     *
     * @param ShopConfigurationTable $shopConfigurationTable
     */
    public function setShopConfigurationTable(ShopConfigurationTable $shopConfigurationTable)
    {
        $this->shopConfigurationTable = $shopConfigurationTable;
    }

}
