<?php

namespace Bitkorn\Shop\View\Helper\Basket\Email\Rating;

use Bitkorn\Shop\Table\Common\ShopConfigurationTable;
use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Model\ViewModel;

/**
 *
 * @author allapow
 */
class RatingReminder extends AbstractHelper
{

    const TEMPLATE = 'template/basket/email/rating/ratingReminder';

    /**
     *
     * @var ShopConfigurationTable
     */
    private $shopConfigurationTable;

    public function __invoke(array $shopBasketEntitiesData)
    {
        if (empty($shopBasketEntitiesData[0])) {
            return '';
        }
        $viewModel = new ViewModel();
        $viewModel->setTemplate(self::TEMPLATE);
        $viewModel->setVariable('shopName', $this->shopConfigurationTable->getShopConfigurationValue('shop_name'));
        $viewModel->setVariable('shopUrl', strtolower(substr($_SERVER['SERVER_PROTOCOL'], 0, strpos($_SERVER['SERVER_PROTOCOL'], '/'))) . '://' . $_SERVER['SERVER_NAME']);

        if (!empty($shopBasketEntitiesData[0]['user_uuid'])) {
            $viewModel->setVariable('customerName',
                $shopBasketEntitiesData[0]['shop_user_address_invoice_salut']
                . ' ' . $shopBasketEntitiesData[0]['shop_user_address_invoice_name1']
                . ' ' . $shopBasketEntitiesData[0]['shop_user_address_invoice_name2']);
        } else {
            $viewModel->setVariable('customerName',
                $shopBasketEntitiesData[0]['shop_basket_address_invoice_salut']
                . ' ' . $shopBasketEntitiesData[0]['shop_basket_address_invoice_name1']
                . ' ' . $shopBasketEntitiesData[0]['shop_basket_address_invoice_name2']);
        }
        $viewModel->setVariable('shopBasketUnique', $shopBasketEntitiesData[0]['shop_basket_unique']);

        $viewModel->setVariable('impressumHtml', $this->shopConfigurationTable->getShopConfigurationValue('impressum_html'));

        return $this->getView()->render($viewModel);
    }

    /**
     *
     * @param ShopConfigurationTable $shopConfigurationTable
     */
    public function setShopConfigurationTable(ShopConfigurationTable $shopConfigurationTable)
    {
        $this->shopConfigurationTable = $shopConfigurationTable;
    }

}
