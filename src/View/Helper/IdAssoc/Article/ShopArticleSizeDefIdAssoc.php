<?php

namespace Bitkorn\Shop\View\Helper\IdAssoc\Article;

use Laminas\View\Helper\AbstractHelper;

/**
 *
 * @author allapow
 */
class ShopArticleSizeDefIdAssoc extends AbstractHelper
{
    
    /**
     *
     * @var \Bitkorn\Shop\Table\Article\Size\ShopArticleSizeDefTable
     */
    private $shopArticleSizeDefTable;
    private $shopArticleSizeDefIdAssoc;

    public function __invoke($shopArticleSizeDefId, $short = false)
    {
        if(!isset($this->shopArticleSizeDefIdAssoc)) {
            $this->shopArticleSizeDefIdAssoc = $this->shopArticleSizeDefTable->getShopArticleSizeDefIdAssoc($short);
        }
        return $this->shopArticleSizeDefIdAssoc[$shopArticleSizeDefId];
    }

    public function setShopArticleSizeDefTable(\Bitkorn\Shop\Table\Article\Size\ShopArticleSizeDefTable $shopArticleSizeDefTable)
    {
        $this->shopArticleSizeDefTable = $shopArticleSizeDefTable;
    }


}
