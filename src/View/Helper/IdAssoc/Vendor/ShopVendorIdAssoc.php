<?php

namespace Bitkorn\Shop\View\Helper\IdAssoc\Vendor;

use Bitkorn\Shop\Tablex\Vendor\ShopVendorTablex;
use Laminas\View\Helper\AbstractHelper;

/**
 *
 * @author allapow
 */
class ShopVendorIdAssoc extends AbstractHelper
{

    /**
     *
     * @var ShopVendorTablex
     */
    private ShopVendorTablex $shopVendorTablex;

    /**
     * @var array
     */
    private array $shopVendorIdAssoc;

    public function __invoke($shopVendorId)
    {
        if (!isset($this->shopVendorIdAssoc)) {
            $this->shopVendorIdAssoc = $this->shopVendorTablex->getShopVendors(0, true);
        }
        return $this->shopVendorIdAssoc[$shopVendorId];
    }

    /**
     *
     * @param ShopVendorTablex $shopVendorTablex
     */
    public function setShopVendorTablex(ShopVendorTablex $shopVendorTablex)
    {
        $this->shopVendorTablex = $shopVendorTablex;
    }


}
