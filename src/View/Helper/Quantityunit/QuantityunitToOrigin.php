<?php

namespace Bitkorn\Shop\View\Helper\Quantityunit;

use Bitkorn\Shop\Service\Quantityunit\QuantityunitService;
use Bitkorn\Trinket\View\Helper\AbstractViewHelper;
use Laminas\Log\Logger;
use Laminas\View\Model\ViewModel;

class QuantityunitToOrigin extends AbstractViewHelper
{
    /**
     * @var QuantityunitService
     */
    protected $quantityunitService;

    /**
     * @param QuantityunitService $quantityunitService
     */
    public function setQuantityunitService(QuantityunitService $quantityunitService): void
    {
        $this->quantityunitService = $quantityunitService;
    }

    /**
     * @param string $quantityunitUuid
     * @param float $value
     * @return float
     */
    public function __invoke(string $quantityunitUuid, float $value)
    {
        if (empty($quantityunitUuid)) {
            return 0;
        }
        return $this->quantityunitService->convertQuantityunitToOrigin($quantityunitUuid, $value);
    }
}
