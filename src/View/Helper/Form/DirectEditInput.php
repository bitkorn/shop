<?php

namespace Bitkorn\Shop\View\Helper\Form;


use Bitkorn\Trinket\View\Helper\AbstractViewHelper;

/**
 * Description of DirectEditInput
 *
 * @author allapow
 */
class DirectEditInput extends AbstractViewHelper
{
    const TEMPLATE_TEXT = 'template/form/directEditInput/text';
    const TEMPLATE_TEXTAREA = 'template/form/directEditInput/textarea';
    const TEMPLATE_RADIO = 'template/form/directEditInput/radio';

    public function __invoke($inputType, $uniqueId, $value, $shopConfigurationValueOptions = null)
    {
        $viewModel = new \Laminas\View\Model\ViewModel();

        switch ($inputType) {
            case 'text':
                $viewModel->setTemplate(self::TEMPLATE_TEXT);
                break;
            case 'textarea':
                $viewModel->setTemplate(self::TEMPLATE_TEXTAREA);
                break;
            case 'radio':
                if (empty($shopConfigurationValueOptions)) {
                    throw new \RuntimeException('Call ' . __CLASS__ . ' without ShopConfigurationValueOptions');
                }
                $viewModel->setTemplate(self::TEMPLATE_RADIO);
                $viewModel->setVariable('jsonValueOptions', htmlentities($shopConfigurationValueOptions));
                $shopConfigurationValueOptions = json_decode($shopConfigurationValueOptions, true);
                $viewModel->setVariable('shopConfigurationValueOptions', $shopConfigurationValueOptions);
                break;
            default :
                return '';
        }
        $viewModel->setVariable('uniqueId', $uniqueId);
        $viewModel->setVariable('value', $value);

        return $this->getView()->render($viewModel);
    }
}
