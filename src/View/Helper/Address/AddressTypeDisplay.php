<?php

namespace Bitkorn\Shop\View\Helper\Address;

use Laminas\View\Helper\AbstractHelper;

/**
 *
 * @author allapow
 */
class AddressTypeDisplay extends AbstractHelper
{

    public function __invoke($addressType)
    {
        switch ($addressType) {
            case 'shipment':
                return 'Versand-Adresse';
            case 'invoice':
                return 'Rechnungs-Adresse';
        }
    }

}
