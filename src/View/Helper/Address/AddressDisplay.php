<?php

namespace Bitkorn\Shop\View\Helper\Address;

use Bitkorn\Shop\Table\Basket\ShopBasketAddressTable;
use Bitkorn\Shop\Table\User\ShopUserAddressTable;
use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Model\ViewModel;

/**
 *
 * @author allapow
 */
class AddressDisplay extends AbstractHelper
{

    const TEMPLATE = 'template/address/address-display';

    /**
     *
     * @var ShopBasketAddressTable
     */
    private $shopBasketAddressTable;

    /**
     *
     * @var ShopUserAddressTable
     */
    private $shopUserAddressTable;

    /**
     *
     * @var array
     */
    private array $isoCountryIdAssoc;

    /**
     *
     * @param array $zeug either key 'userUuid' or/and 'basketunique'
     * @param bool $simple
     * @return string
     */
    public function __invoke($zeug = [], $simple = false)
    {
        if (empty($zeug)) {
            return '';
        }
        $viewModel = new ViewModel();
        $viewModel->setTemplate(self::TEMPLATE);
        $viewModel->setVariable('simple', $simple);
        if (isset($zeug['userUuid']) && !empty($zeug['userUuid'])) {
            $addresses = $this->shopUserAddressTable->getShopUserAddressesByUserId($zeug['userUuid']);
            if (empty($addresses[0])) {
                return 'Es sind keine Adressen vorhanden.';
            }
            $viewModel->setVariable('type', 'user');
            $viewModel->setVariable('invoiceCompanyName', !empty($addresses[0]['shop_user_address_company_name']) ? $addresses[0]['shop_user_address_company_name'] : '');
            $viewModel->setVariable('invoiceName', $addresses[0]['shop_user_address_salut'] . ' ' . $addresses[0]['shop_user_address_name1'] . ' ' . $addresses[0]['shop_user_address_name2']);
            $viewModel->setVariable('invoiceStreet', $addresses[0]['shop_user_address_street'] . ' ' . $addresses[0]['shop_user_address_street_no']);
            $viewModel->setVariable('invoiceCity', $addresses[0]['shop_user_address_zip'] . ' ' . $addresses[0]['shop_user_address_city']);
            $viewModel->setVariable('invoiceCountry', $this->isoCountryIdAssoc[$addresses[0]['country_id']]);
            $viewModel->setVariable('invoiceTel', isset($addresses[0]['shop_user_address_tel']) ? $addresses[0]['shop_user_address_tel'] : '');
            if (!empty($addresses[1])) {
                $viewModel->setVariable('shipping', 1);
                $viewModel->setVariable('shippingCompanyName', !empty($addresses[1]['shop_user_address_company_name']) ? $addresses[1]['shop_user_address_company_name'] : '');
                $viewModel->setVariable('shippingCompanyDepartment', !empty($addresses[1]['shop_user_address_company_department']) ? $addresses[1]['shop_user_address_company_department'] : '');
                $viewModel->setVariable('shippingName', $addresses[1]['shop_user_address_name1'] . ' ' . $addresses[1]['shop_user_address_name2']);
                $viewModel->setVariable('shippingStreet', $addresses[1]['shop_user_address_street'] . ' ' . $addresses[1]['shop_user_address_street_no']);
                $viewModel->setVariable('shippingCity', $addresses[1]['shop_user_address_zip'] . ' ' . $addresses[1]['shop_user_address_city']);
                $viewModel->setVariable('shippingCountry', $this->isoCountryIdAssoc[$addresses[1]['country_id']]);
                $viewModel->setVariable('shippingTel', isset($addresses[1]['shop_user_address_tel']) ? $addresses[1]['shop_user_address_tel'] : '');
            }
        } elseif (isset($zeug['basketunique']) && !empty($zeug['basketunique'])) {
            $addresses = $this->shopBasketAddressTable->getShopBasketAddressesByBasketUnique($zeug['basketunique']);
            if (empty($addresses['invoice'])) {
                return 'Es sind keine Adressen vorhanden.';
            }
            $viewModel->setVariable('type', 'basket');
            $viewModel->setVariable('invoiceCompanyName', !empty($addresses['invoice']['shop_basket_address_company_name']) ? $addresses['invoice']['shop_basket_address_company_name'] : '');
            $viewModel->setVariable('invoiceName', $addresses['invoice']['shop_basket_address_salut'] . ' ' . $addresses['invoice']['shop_basket_address_name1'] . ' ' . $addresses['invoice']['shop_basket_address_name2']);
            $viewModel->setVariable('invoiceStreet', $addresses['invoice']['shop_basket_address_street'] . ' ' . $addresses['invoice']['shop_basket_address_street_no']);
            $viewModel->setVariable('invoiceCity', $addresses['invoice']['shop_basket_address_zip'] . ' ' . $addresses['invoice']['shop_basket_address_city']);
            $viewModel->setVariable('invoiceCountry', $this->isoCountryIdAssoc[$addresses['invoice']['country_id']]);
            $viewModel->setVariable('invoiceEmail', isset($addresses['invoice']['shop_basket_address_email']) ? $addresses['invoice']['shop_basket_address_email'] : '');
            $viewModel->setVariable('invoiceTel', isset($addresses['invoice']['shop_basket_address_tel']) ? $addresses['invoice']['shop_basket_address_tel'] : '');
            if (!empty($addresses['shipment'])) {
                $viewModel->setVariable('shipping', 1);
                $viewModel->setVariable('shippingCompanyName', !empty($addresses['shipment']['shop_basket_address_company_name']) ? $addresses['shipment']['shop_basket_address_company_name'] : '');
                $viewModel->setVariable('shippingCompanyDepartment', !empty($addresses['shipment']['shop_basket_address_company_department']) ? $addresses['shipment']['shop_basket_address_company_department'] : '');
                $viewModel->setVariable('shippingName', $addresses['shipment']['shop_basket_address_name1'] . ' ' . $addresses['shipment']['shop_basket_address_name2']);
                $viewModel->setVariable('shippingStreet', $addresses['shipment']['shop_basket_address_street'] . ' ' . $addresses['shipment']['shop_basket_address_street_no']);
                $viewModel->setVariable('shippingCity', $addresses['shipment']['shop_basket_address_zip'] . ' ' . $addresses['shipment']['shop_basket_address_city']);
                $viewModel->setVariable('shippingCountry', $this->isoCountryIdAssoc[$addresses['shipment']['country_id']]);
                $viewModel->setVariable('shippingEmail', isset($addresses['shipment']['shop_basket_address_email']) ? $addresses['shipment']['shop_basket_address_email'] : '');
                $viewModel->setVariable('shippingTel', isset($addresses['shipment']['shop_basket_address_tel']) ? $addresses['shipment']['shop_basket_address_tel'] : '');
            }
        } else {
            return '';
        }
        return $this->getView()->render($viewModel);
    }

    /**
     *
     * @param ShopBasketAddressTable $shopBasketAddressTable
     */
    public function setShopBasketAddressTable(ShopBasketAddressTable $shopBasketAddressTable)
    {
        $this->shopBasketAddressTable = $shopBasketAddressTable;
    }

    /**
     *
     * @param ShopUserAddressTable $shopUserAddressTable
     */
    public function setShopUserAddressTable(ShopUserAddressTable $shopUserAddressTable)
    {
        $this->shopUserAddressTable = $shopUserAddressTable;
    }

    public function setIsoCountryIdAssoc(array $isoCountryIdAssoc)
    {
        $this->isoCountryIdAssoc = $isoCountryIdAssoc;
    }

}
