<?php

namespace Bitkorn\Shop\View\Helper\Article\Category;

use Bitkorn\Shop\Table\Article\ShopArticleCategoryTable;
use Laminas\View\Helper\AbstractHelper;

/**
 * @todo Implementieren falls man z.B. ein accordion machen möchte.
 * @author allapow
 */
class ShopArticleCategoryUnorderedList extends AbstractHelper
{
    const TEMPLATE = 'template/article/category/shopArticleCategoryUnorderedList';

    /**
     *
     * @var ShopArticleCategoryTable
     */
    private $shopArticleCategoryTable;

    public function __invoke()
    {
        $viewModel = new \Laminas\View\Model\ViewModel();
        $viewModel->setTemplate(self::TEMPLATE);

        $viewModel->setVariable('', 0);

        return $this->getView()->render($viewModel);
    }

    public function setShopArticleCategoryTable(ShopArticleCategoryTable $shopArticleCategoryTable)
    {
        $this->shopArticleCategoryTable = $shopArticleCategoryTable;
    }

}
