<?php

namespace Bitkorn\Shop\View\Helper\Article\Category;

use Bitkorn\Shop\Table\Article\ShopArticleCategoryTable;
use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Model\ViewModel;

/**
 * \Bitkorn\Trinket\View\Helper\Form\SimpleSelect hätts wohl auch getan.
 * @author allapow
 */
class ShopArticleCategorySelect extends AbstractHelper
{
    const TEMPLATE = 'template/article/category/shopArticleCategorySelect';

    /**
     *
     * @var ShopArticleCategoryTable
     */
    private ShopArticleCategoryTable $shopArticleCategoryTable;

    /**
     * @param int $categoryIdCurrent
     * @param string $selectName
     * @param bool $withEmptyValue
     * @param string $selectClass
     * @return string
     */
    public function __invoke($categoryIdCurrent = 0, $selectName = 'shop_article_category_id', $withEmptyValue = true, $selectClass = 'w3-select')
    {
        $viewModel = new ViewModel();
        $viewModel->setTemplate(self::TEMPLATE);
        $viewModel->setVariable('categoryIdCurrent', $categoryIdCurrent);
        $viewModel->setVariable('selectName', $selectName);
        $viewModel->setVariable('withEmptyValue', $withEmptyValue);
        $viewModel->setVariable('selectClass', $selectClass);
        $shopArticleCategoryDashedList = [];
        $this->shopArticleCategoryTable->getShopArticleCategoryDashedListRecursive($shopArticleCategoryDashedList);
        $viewModel->setVariable('shopArticleCategoryDashedList', $shopArticleCategoryDashedList);

        return $this->getView()->render($viewModel);
    }

    /**
     * @param ShopArticleCategoryTable $shopArticleCategoryTable
     */
    public function setShopArticleCategoryTable(ShopArticleCategoryTable $shopArticleCategoryTable)
    {
        $this->shopArticleCategoryTable = $shopArticleCategoryTable;
    }

}
