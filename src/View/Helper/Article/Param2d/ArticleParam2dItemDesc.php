<?php

namespace Bitkorn\Shop\View\Helper\Article\Param2d;

use Bitkorn\Shop\Service\Article\ShopArticleParam2dService;
use Bitkorn\Shop\Table\Article\Param2d\ShopArticleParam2dItemTable;
use Bitkorn\Trinket\View\Helper\AbstractViewHelper;
use Laminas\Log\Logger;
use Laminas\View\Model\ViewModel;

class ArticleParam2dItemDesc extends AbstractViewHelper
{
    const TEMPLATE = 'template/someTamplate';

    /**
     * @var ShopArticleParam2dItemTable
     */
    protected $shopArticleParam2dItemTable;

    /**
     * @param ShopArticleParam2dItemTable $shopArticleParam2dItemTable
     */
    public function setShopArticleParam2dItemTable(ShopArticleParam2dItemTable $shopArticleParam2dItemTable): void
    {
        $this->shopArticleParam2dItemTable = $shopArticleParam2dItemTable;
    }

    /**
     * @param int $param2dItemId
     * @param bool $isHtml
     * @return string
     */
    public function __invoke(?int $param2dItemId, bool $isHtml = true)
    {
        if (!$param2dItemId) {
            return '';
        }
        $param2dItem = $this->shopArticleParam2dItemTable->getShopArticleParam2dItemForParam2dItemId($param2dItemId);
        if (empty($param2dItem)) {
            return '';
        }
        if(!$isHtml) {
            return html_entity_decode($param2dItem['shop_article_param2d_name_1'] . ': '
                . $param2dItem['shop_article_param2d_item_value_1'] . ' ' . $param2dItem['quantity1_label'] . "\r\n"
                . $param2dItem['shop_article_param2d_name_2'] . ': '
                . $param2dItem['shop_article_param2d_item_value_2'] . ' ' . $param2dItem['quantity2_label']);
        } else {
            return $param2dItem['shop_article_param2d_name_1'] . ': '
                . $param2dItem['shop_article_param2d_item_value_1'] . ' ' . $param2dItem['quantity1_label'] . '<br>'
                . $param2dItem['shop_article_param2d_name_2'] . ': '
                . $param2dItem['shop_article_param2d_item_value_2'] . ' ' . $param2dItem['quantity2_label'];
        }
    }
}
