<?php

namespace Bitkorn\Shop\View\Helper\Article\Param2d;

use Bitkorn\Shop\Table\Article\Param2d\ShopArticleParam2dItemTable;
use Bitkorn\Trinket\View\Helper\AbstractViewHelper;
use Laminas\View\Model\ViewModel;

class ArticleParam2dsSelect extends AbstractViewHelper
{
    const TEMPLATE = 'template/article/param2d/param2dSelect';

    /**
     * @var ShopArticleParam2dItemTable
     */
    protected $shopArticleParam2dItemTable;

    /**
     * @param ShopArticleParam2dItemTable $shopArticleParam2dItemTable
     */
    public function setShopArticleParam2dItemTable(ShopArticleParam2dItemTable $shopArticleParam2dItemTable): void
    {
        $this->shopArticleParam2dItemTable = $shopArticleParam2dItemTable;
    }

    /**
     * @param array $shopArticleParam2dItemsIdAssoc
     * @param int $param2dItemIdSelected
     * @return string
     */
    public function __invoke(array $shopArticleParam2dItemsIdAssoc, int $param2dItemIdSelected = 0)
    {
        if (empty($shopArticleParam2dItemsIdAssoc)) {
            return '';
        }
        $viewModel = new ViewModel();
        $viewModel->setTemplate(self::TEMPLATE);
        $viewModel->setVariable('selectLabel', $shopArticleParam2dItemsIdAssoc[array_key_first($shopArticleParam2dItemsIdAssoc)]['shop_article_param2d_name']);
        $viewModel->setVariable('param2dItems', $shopArticleParam2dItemsIdAssoc);
        $viewModel->setVariable('selectedId', $param2dItemIdSelected);
        return $this->getView()->render($viewModel);
    }
}
