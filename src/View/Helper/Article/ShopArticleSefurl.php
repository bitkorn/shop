<?php

namespace Bitkorn\Shop\View\Helper\Article;

use Bitkorn\Shop\Table\Article\ShopArticleTable;
use Laminas\View\Helper\AbstractHelper;

/**
 * Description of ShopArticleSefurl
 *
 * @author allapow
 */
class ShopArticleSefurl extends AbstractHelper
{
    /**
     *
     * @var ShopArticleTable
     */
    private $shopArticleTable;

    /**
     *
     * @param ShopArticleTable $shopArticleTable
     */
    public function setShopArticleTable(ShopArticleTable $shopArticleTable)
    {
        $this->shopArticleTable = $shopArticleTable;
    }

    public function __invoke(int $shopArticleId)
    {
        $articleData = $this->shopArticleTable->getShopArticleById($shopArticleId);
        if (!empty($articleData)) {
            return $articleData['shop_article_sefurl'];
        }
        return '';
    }

}
