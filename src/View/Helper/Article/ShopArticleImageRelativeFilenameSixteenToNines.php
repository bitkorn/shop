<?php

namespace Bitkorn\Shop\View\Helper\Article;

/**
 * UNUSED since BitkornImages
 *
 * @author allapow
 */
class ShopArticleImageRelativeFilenameSixteenToNines extends \Laminas\View\Helper\AbstractHelper
{
    
    private $bitkornShopConfig;
    
    public function __invoke($shopArticleImageData)
    {
        if(empty($shopArticleImageData) || empty($shopArticleImageData['shop_article_image_sixteen_to_nines'])) {
            return [];
        }
        $sixteenToNines = json_decode($shopArticleImageData['shop_article_image_sixteen_to_nines'], true);
        if(empty($sixteenToNines) || !is_array($sixteenToNines)) {
            return [];
        }
        $imagePaths = [];
        foreach($sixteenToNines as $sixteenToNineMultiplicator => $sixteenToNine) {
            $imagePaths[$sixteenToNineMultiplicator] = $this->bitkornShopConfig['image_path_relative'] . '/' . $shopArticleImageData['shop_article_id'] . '/' . $sixteenToNine['filename'];
        }
        return $imagePaths;
    }

    public function setBitkornShopConfig($bitkornShopConfig)
    {
        $this->bitkornShopConfig = $bitkornShopConfig;
    }


}
