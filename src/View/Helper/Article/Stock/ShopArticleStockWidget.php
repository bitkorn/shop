<?php

namespace Bitkorn\Shop\View\Helper\Article\Stock;

use Bitkorn\Shop\Entity\Article\ShopArticleOptionsEntity;
use Bitkorn\Shop\Table\Article\ShopArticleStockTable;
use Bitkorn\Trinket\View\Helper\AbstractViewHelper;
use Laminas\View\Model\ViewModel;

/**
 * Description of ShopArticle
 *
 * @author allapow
 */
class ShopArticleStockWidget extends AbstractViewHelper
{

    const TEMPLATE = 'template/article/stock/articleStockWidget';

    /**
     *
     * @var ShopArticleOptionsEntity
     */
    private $shopArticleOptionsEntity;

    /**
     *
     * @var ShopArticleStockTable
     */
    private $shopArticleStockTable;

    /**
     *
     * @param ShopArticleOptionsEntity $shopArticleOptionsEntity
     */
    public function setShopArticleOptionsEntity(ShopArticleOptionsEntity $shopArticleOptionsEntity)
    {
        $this->shopArticleOptionsEntity = $shopArticleOptionsEntity;
    }

    /**
     *
     * @param ShopArticleStockTable $shopArticleStockTable
     */
    public function setShopArticleStockTable(ShopArticleStockTable $shopArticleStockTable)
    {
        $this->shopArticleStockTable = $shopArticleStockTable;
    }

    /**
     *
     * @param int $articleId
     * @param string $articleOptions
     * @return string
     */
    public function __invoke(int $articleId, string $articleOptions)
    {
        if (empty($articleId)) {
            return '';
        }
        $viewModel = new ViewModel();
        $viewModel->setTemplate(self::TEMPLATE);

        $viewModel->setVariable('articleId', $articleId);

        $array = json_decode($articleOptions);
        if (!$array) {
            $articleOptions = '[]';
        } else {
            $articleOptions = $this->shopArticleOptionsEntity->cleanOptionsJson($articleOptions);
        }
        $articleStock = $this->shopArticleStockTable->searchShopArticleStock($articleId, $articleOptions);
        $viewModel->setVariable('articleStock', (!empty($articleStock) ? $articleStock['shop_article_stock_amount_sum'] : 0));

        return $this->getView()->render($viewModel);
    }

}
