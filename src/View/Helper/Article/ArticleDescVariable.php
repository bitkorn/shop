<?php

namespace Bitkorn\Shop\View\Helper\Article;

use Bitkorn\Shop\Entity\Article\TypeDelivery\ShopArticleLengthcutDefEntity;
use Bitkorn\Shop\Table\Article\TypeDelivery\ShopArticleLengthcutDefTable;
use Bitkorn\Shop\View\Helper\Article\Option\ShopArticleOptionsFromJson;
use Bitkorn\Shop\View\Helper\Article\Param2d\ArticleParam2dItemDesc;
use Bitkorn\Shop\View\Helper\Article\TypeDelivery\ArticleLengthcutOptionsDesc;
use Bitkorn\Trinket\Service\I18n\NumberFormatService;
use Bitkorn\Trinket\View\Helper\AbstractViewHelper;
use Bitkorn\Trinket\View\Helper\I18n\NumberFormat;
use Laminas\Log\Logger;
use Laminas\View\Model\ViewModel;

class ArticleDescVariable extends AbstractViewHelper
{
    const TEMPLATE = 'template/someTamplate';

    /**
     * @var ShopArticleOptionsFromJson ViewHelper
     */
    protected $shopArticleOptionsFromJson;

    /**
     * @var ArticleParam2dItemDesc ViewHelper
     */
    protected $articleParam2dItemDesc;

    /**
     * @var ArticleLengthcutOptionsDesc ViewHelper
     */
    protected $articleLengthcutOptionsDesc;

    /**
     * @var NumberFormatService
     */
    protected $numberFormatService;

    /**
     * @var ShopArticleLengthcutDefTable
     */
    protected $shopArticleLengthcutDefTable;

    /**
     * @param ShopArticleOptionsFromJson $shopArticleOptionsFromJson
     */
    public function setShopArticleOptionsFromJson(ShopArticleOptionsFromJson $shopArticleOptionsFromJson): void
    {
        $this->shopArticleOptionsFromJson = $shopArticleOptionsFromJson;
    }

    /**
     * @param ArticleParam2dItemDesc $articleParam2dItemDesc
     */
    public function setArticleParam2dItemDesc(ArticleParam2dItemDesc $articleParam2dItemDesc): void
    {
        $this->articleParam2dItemDesc = $articleParam2dItemDesc;
    }

    /**
     * @param ArticleLengthcutOptionsDesc $articleLengthcutOptionsDesc
     */
    public function setArticleLengthcutOptionsDesc(ArticleLengthcutOptionsDesc $articleLengthcutOptionsDesc): void
    {
        $this->articleLengthcutOptionsDesc = $articleLengthcutOptionsDesc;
    }

    /**
     * @param NumberFormatService $numberFormatService
     */
    public function setNumberFormatService(NumberFormatService $numberFormatService): void
    {
        $this->numberFormatService = $numberFormatService;
    }

    /**
     * @param ShopArticleLengthcutDefTable $shopArticleLengthcutDefTable
     */
    public function setShopArticleLengthcutDefTable(ShopArticleLengthcutDefTable $shopArticleLengthcutDefTable): void
    {
        $this->shopArticleLengthcutDefTable = $shopArticleLengthcutDefTable;
    }

    /**
     * @param array $basketItem
     * @param bool $isHtml
     * @return string
     */
    public function __invoke(array $basketItem, bool $isHtml = true): string
    {
        $desc = '';
        if (empty($basketItem) || !is_array($basketItem)) {
            return $desc;
        }
        if (!empty($basketItem['shop_basket_item_article_options']) && $basketItem['shop_basket_item_article_options'] != '[]') {
            $desc .= call_user_func($this->shopArticleOptionsFromJson, $basketItem['shop_basket_item_article_options'], $basketItem['shop_article_id'], $isHtml);
        } else if (!empty($basketItem['shop_article_param2d_item_id'])) {
            $desc .= call_user_func($this->articleParam2dItemDesc, $basketItem['shop_article_param2d_item_id'], $isHtml);
        }
        if (!empty($basketItem['shop_basket_item_lengthcut_options'])) {
            $lengthcutDef = $this->shopArticleLengthcutDefTable->getLengthcutDefForArticleId($basketItem['shop_article_id']);
            $lengthcutDefArr = ShopArticleLengthcutDefEntity::decodeDefinitionJson($basketItem['shop_basket_item_lengthcut_options']);
            $articleAmountLengthcut = round(($lengthcutDefArr['length_pieces'] * $lengthcutDefArr['quantity']) / $lengthcutDef['shop_article_lengthcut_def_basequantity'], 2);

            // compute base article amount
            $articleAmountLengthcut = round(($lengthcutDefArr['length_pieces'] * $lengthcutDefArr['quantity']) / $lengthcutDef['shop_article_lengthcut_def_basequantity'], 2);
            if($isHtml) {
                $desc = 'Anzahl Basislängen: ' . $this->numberFormatService->format($articleAmountLengthcut) . '<br>' . $desc;
                $desc .= '<br>';
            } else {
                $desc = 'Anzahl Basislängen: ' . $this->numberFormatService->format($articleAmountLengthcut) . PHP_EOL . $desc;
                $desc .= PHP_EOL;
            }
            $desc .= call_user_func($this->articleLengthcutOptionsDesc, $basketItem, $isHtml);
        }
        return $desc;
    }
}
