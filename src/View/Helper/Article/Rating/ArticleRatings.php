<?php

namespace Bitkorn\Shop\View\Helper\Article\Rating;

use Bitkorn\Shop\Table\Article\ShopArticleRatingTable;
use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Model\ViewModel;

/**
 *
 * @author allapow
 */
class ArticleRatings extends AbstractHelper
{

    const TEMPLATE = 'template/article/rating/article-ratings';

    /**
     *
     * @var ShopArticleRatingTable
     */
    private $shopArticleRatingTable;

    /**
     *
     * @param string $articleId
     * @return string
     */
    public function __invoke($articleId)
    {
        if (empty($articleId)) {
            return '';
        }
        $viewModel = new ViewModel();
        $viewModel->setTemplate(self::TEMPLATE);

        $viewModel->setVariable('articleId', $articleId);

        $ratings = $this->shopArticleRatingTable->getShopArticleRatings($articleId);
        $viewModel->setVariable('ratings', $ratings);

        return $this->getView()->render($viewModel);
    }

    public function setShopArticleRatingTable(ShopArticleRatingTable $shopArticleRatingTable)
    {
        $this->shopArticleRatingTable = $shopArticleRatingTable;
    }

}
