<?php

namespace Bitkorn\Shop\View\Helper\Article\Rating;

use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Model\ViewModel;

/**
 * http://antenna.io/demo/jquery-bar-rating/examples/
 * @author allapow
 */
class BarRating extends AbstractHelper
{

    const TEMPLATE = 'template/article/rating/bar-rating';

    /**
     * 
     * @param string $currentValue
     * @return string
     */
    public function __invoke($currentValue, $readOnly = true, $selectName = '')
    {
        $viewModel = new ViewModel();
        $viewModel->setTemplate(self::TEMPLATE);
        
        $viewModel->setVariable('currentValue', $currentValue);
        $viewModel->setVariable('readOnly', $readOnly);
        $viewModel->setVariable('selectName', $selectName);
        
        return $this->getView()->render($viewModel);
    }
    
}
