<?php

namespace Bitkorn\Shop\View\Helper\Article\Frontend;

use Laminas\View\Model\ViewModel;

/**
 * Description of ShopArticles
 *
 * @author allapow
 */
class ShopArticlesPreview extends \Laminas\View\Helper\AbstractHelper
{
    const ARTICLES_TEMPLATE = 'template/article/frontend/articlesPreview';

    public function __invoke(array $shopArticlesData)
    {
        if (empty($shopArticlesData)) {
            return '';
        }
        $viewModel = new ViewModel();
        $viewModel->setTemplate(self::ARTICLES_TEMPLATE);

        $viewModel->setVariable('shopArticlesData', $shopArticlesData);

        return $this->getView()->render($viewModel);
    }

}
