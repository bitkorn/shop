<?php

namespace Bitkorn\Shop\View\Helper\Article\Frontend;

use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Helper\Url;

/**
 * Gibt zu einem Article-Type plus Article-ID die passende URL aus.
 *
 * @author allapow
 */
class ShopArticleTypeUrl extends AbstractHelper
{

    /**
     *
     * @var array
     */
    private $shopArticleTypes;

    /**
     *
     * @var array
     */
    private $shopArticleDeliveryTypes;

    /**
     *
     * @var Url
     */
    private $urlHelper;

    public function setShopArticleTypes($shopArticleTypes)
    {
        $this->shopArticleTypes = $shopArticleTypes;
    }

    /**
     * @param array $shopArticleDeliveryTypes
     */
    public function setShopArticleDeliveryTypes(array $shopArticleDeliveryTypes): void
    {
        $this->shopArticleDeliveryTypes = $shopArticleDeliveryTypes;
    }

    /**
     *
     * @param Url $urlHelper
     */
    public function setUrlHelper(Url $urlHelper)
    {
        $this->urlHelper = $urlHelper;
    }

    public function __invoke(string $shopArticleType, string $shopArticleTypeDelivery, string $shopArticleSefurl)
    {
        if (
            empty($shopArticleType) || !in_array($shopArticleType, $this->shopArticleTypes)
            || empty($shopArticleTypeDelivery) || !in_array($shopArticleTypeDelivery, $this->shopArticleDeliveryTypes)
            || empty($shopArticleSefurl)) {
            return 'JavaScript:void(0);';
        }
        $urlHelper = $this->urlHelper;
        switch ($shopArticleType) {
            case 'config':
                return $urlHelper('shop_frontend_article_configarticle', ['article_sefurl' => $shopArticleSefurl]);
                break;
            case 'param2d':
                switch ($shopArticleTypeDelivery) {
                    case 'lengthcut':
                        return $urlHelper('shop_frontend_article_param2dlengthcutarticle', ['article_sefurl' => $shopArticleSefurl]);
                    case 'download':
                    case 'standard':
                }
                break;
            case 'standard':
            default:
                return $urlHelper('shop_frontend_article_article', ['article_sefurl' => $shopArticleSefurl]);
        }
    }

}