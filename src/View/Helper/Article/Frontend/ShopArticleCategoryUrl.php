<?php

namespace Bitkorn\Shop\View\Helper\Article\Frontend;

use Bitkorn\Shop\Table\Article\ShopArticleCategoryTable;
use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Helper\Url;

/**
 * Falls sich der Alias einer Kategorie mal aendert, kann das hier besser mit der Kategorie ID benutzt werden.
 * @author allapow
 */
class ShopArticleCategoryUrl extends AbstractHelper
{
    /**
     *
     * @var ShopArticleCategoryTable
     */
    private ShopArticleCategoryTable $shopArticleCategoryTable;

    /**
     *
     * @var Url
     */
    private Url $urlHelper;

    public function __invoke($shopArticleCategoryId)
    {
        $articleCategoryData = $this->shopArticleCategoryTable->getShopArticleCategoryById($shopArticleCategoryId);
        if (!empty($articleCategoryData) && isset($articleCategoryData['shop_article_category_alias'])) {
            return call_user_func($this->urlHelper, 'shop_frontend_article_articles', ['article_category_alias' => $articleCategoryData['shop_article_category_alias']]);
        }
        return '';
    }

    /**
     *
     * @param ShopArticleCategoryTable $shopArticleCategoryTable
     */
    public function setShopArticleCategoryTable(ShopArticleCategoryTable $shopArticleCategoryTable)
    {
        $this->shopArticleCategoryTable = $shopArticleCategoryTable;
    }

    /**
     *
     * @param Url $urlHelper
     */
    public function setUrlHelper(Url $urlHelper)
    {
        $this->urlHelper = $urlHelper;
    }

}
