<?php

namespace Bitkorn\Shop\View\Helper\Article\Frontend;

use Bitkorn\Images\Entity\Image\ImagesEntity;
use Bitkorn\Shop\Tablex\Article\ShopArticleImageTablex;
use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Model\ViewModel;

/**
 * Description of ShopArticle
 *
 * @author allapow
 */
class ShopArticlePreview extends AbstractHelper
{
    const ARTICLE_TEMPLATE = 'template/article/frontend/articlePreview';

    /**
     *
     * @var ShopArticleImageTablex
     */
    private $shopArticleImageTablex;

    /**
     *
     * @param ShopArticleImageTablex $shopArticleImageTablex
     */
    public function setShopArticleImageTablex(ShopArticleImageTablex $shopArticleImageTablex)
    {
        $this->shopArticleImageTablex = $shopArticleImageTablex;
    }

    public function __invoke(array $shopArticleData)
    {
        if(empty($shopArticleData)) {
            return '';
        }
        $viewModel = new ViewModel();
        $viewModel->setTemplate(self::ARTICLE_TEMPLATE);

        $viewModel->setVariable('shopArticleData', $shopArticleData);
        $shopArticleImageData = $this->shopArticleImageTablex->getShopArticleImages($shopArticleData['shop_article_id']);
        if (!empty($shopArticleImageData)) {
            $viewModel->setVariable('shopArticleImageData', $shopArticleImageData[0]);
            $imageEntity = new ImagesEntity();
            $imageEntity->exchangeArrayImages($shopArticleImageData);
            $viewModel->setVariable('imageEntity', $imageEntity);
        }

        if(!empty($shopArticleData['shop_article_option_item_id'])) {
            $viewModel->setVariable('shopArticleUrlAppend', '?optionitemid=' . $shopArticleData['shop_article_option_item_id']);
        }

        return $this->getView()->render($viewModel);
    }

}
