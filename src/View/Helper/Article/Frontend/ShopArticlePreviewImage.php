<?php

namespace Bitkorn\Shop\View\Helper\Article\Frontend;

use Bitkorn\Shop\Table\Article\ShopArticleImageTable;
use Laminas\View\Helper\AbstractHelper;

/**
 * Gibt zu einem Article-Type plus Article-ID die passende URL aus.
 *
 * @author allapow
 */
class ShopArticlePreviewImage extends AbstractHelper
{

    /**
     *
     * @var array
     */
    private $bitkornImagesConfig;

    /**
     *
     * @var ShopArticleImageTable
     */
    private $shopArticleImageTable;

    public function __invoke($shopArticleId)
    {
        if (empty($shopArticleId)) {
            return '';
        }
        $imageSrcRelative = '';
        $shopArticleImage = $this->shopArticleImageTable->getShopArticleImageXByArticleId($shopArticleId);
        if (!empty($shopArticleImage)) {
            $imageSrcRelative = $this->bitkornImagesConfig['image_path_relative']
                . '/' . date('Y', $shopArticleImage['bk_images_image_time_create'])
                . '/' . date('m', $shopArticleImage['bk_images_image_time_create'])
                . '/' . $shopArticleImage['bk_images_image_id']
                . '/' . $shopArticleImage['bk_images_image_filename']
                . '_mts_64.' . $shopArticleImage['bk_images_image_extension'];
        }
        return $imageSrcRelative;
    }

    /**
     *
     * @param ShopArticleImageTable $shopArticleImageTable
     */
    public function setShopArticleImageTable(ShopArticleImageTable $shopArticleImageTable)
    {
        $this->shopArticleImageTable = $shopArticleImageTable;
    }

    public function setBitkornImagesConfig(array $bitkornImagesConfig)
    {
        $this->bitkornImagesConfig = $bitkornImagesConfig;
    }

}
