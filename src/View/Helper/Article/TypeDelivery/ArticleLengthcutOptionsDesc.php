<?php

namespace Bitkorn\Shop\View\Helper\Article\TypeDelivery;

use Bitkorn\Shop\Entity\Article\TypeDelivery\ShopArticleLengthcutDefEntity;
use Bitkorn\Shop\Service\Article\ShopArticleParam2dService;
use Bitkorn\Shop\Service\Article\ShopArticleService;
use Bitkorn\Shop\Service\Article\TypeDelivery\ShopArticleLengthcutDefService;
use Bitkorn\Trinket\Service\I18n\NumberFormatService;
use Bitkorn\Trinket\View\Helper\AbstractViewHelper;
use Laminas\Log\Logger;
use Laminas\View\Model\ViewModel;

class ArticleLengthcutOptionsDesc extends AbstractViewHelper
{

    /**
     * @var ShopArticleLengthcutDefService
     */
    protected $shopArticleLengthcutDefService;
    /**
     * @var NumberFormatService
     */
    protected $numberFormatService;

    /**
     * @var ShopArticleService
     */
    protected $shopArticleService;

    /**
     * @var ShopArticleParam2dService
     */
    protected $shopArticleParam2dService;

    /**
     * @param ShopArticleLengthcutDefService $shopArticleLengthcutDefService
     */
    public function setShopArticleLengthcutDefService(ShopArticleLengthcutDefService $shopArticleLengthcutDefService): void
    {
        $this->shopArticleLengthcutDefService = $shopArticleLengthcutDefService;
    }

    /**
     * @param NumberFormatService $numberFormatService
     */
    public function setNumberFormatService(NumberFormatService $numberFormatService): void
    {
        $this->numberFormatService = $numberFormatService;
    }

    /**
     * @param ShopArticleService $shopArticleService
     */
    public function setShopArticleService(ShopArticleService $shopArticleService): void
    {
        $this->shopArticleService = $shopArticleService;
    }

    /**
     * @param ShopArticleParam2dService $shopArticleParam2dService
     */
    public function setShopArticleParam2dService(ShopArticleParam2dService $shopArticleParam2dService): void
    {
        $this->shopArticleParam2dService = $shopArticleParam2dService;
    }

    /**
     * lengthcut options {"lengthcut_def_id":2,"cutprice":"14.8","length_pieces":500,"quantity":3,"quantity_cuts":2}
     *
     * @param array $xBasketItem
     * @param bool $isHtml
     * @return string
     */
    public function __invoke(array $xBasketItem, bool $isHtml = true)
    {
        if (
            empty($xBasketItem)
            || empty($xBasketItem['shop_basket_item_lengthcut_options'])
            || empty($arr = ShopArticleLengthcutDefEntity::decodeDefinitionJson($xBasketItem['shop_basket_item_lengthcut_options']))
            || empty($arr['lengthcut_def_id'])
        ) {
            return '';
        }
        $lnb = $isHtml ? '<br>' : "\r\n";
        $article = $this->shopArticleService->getShopArticleById($xBasketItem['shop_article_id']);
        $lengthCutDef = $this->shopArticleLengthcutDefService->getLengthcutDef($arr['lengthcut_def_id']);
        $param2dItem = $this->shopArticleParam2dService->getShopArticleParam2dItemForParam2dItemId($xBasketItem['shop_article_param2d_item_id']);
        $lengthcutDecimal = round(($arr['length_pieces'] * $arr['quantity']) / $lengthCutDef['shop_article_lengthcut_def_basequantity'], 2);
        $price = ($article['shop_article_price'] + $param2dItem['shop_article_param2d_item_pricediff']) * $lengthcutDecimal;
        $stringPieces = $arr['quantity'] . ' Stck. ' . $arr['length_pieces'] . ' ' . $lengthCutDef['quantityunit_label'] . ' = ' . $this->numberFormatService->format($price) . ' €';
        $stringCuts = $arr['quantity_cuts'] . ' Schnitte zu ' . $this->numberFormatService->format($arr['cutprice'])
            . ' € = ' . $this->numberFormatService->format($arr['quantity_cuts'] * $arr['cutprice']) . ' €';
        return $stringPieces . $lnb . $stringCuts;
    }
}
