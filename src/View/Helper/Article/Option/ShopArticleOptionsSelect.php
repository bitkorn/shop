<?php

namespace Bitkorn\Shop\View\Helper\Article\Option;

use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Model\ViewModel;

/**
 * Description of ShopArticleOption
 *
 * @author allapow
 */
class ShopArticleOptionsSelect extends AbstractHelper
{
    const TEMPLATE = 'template/article/option/options';
    
    public function __invoke(array $shopArticleOptions, $small = false, $optionItemIdsSelected = [], $additionalCssClassOuter = '', $additionalCssClassInner = '')
    {
        if(empty($shopArticleOptions)) {
            return '';
        }
        $viewModel = new ViewModel();
        $viewModel->setTemplate(self::TEMPLATE);
        
        $viewModel->setVariable('shopArticleOptions', $shopArticleOptions);
        $viewModel->setVariable('smallCssClass', ($small ? 'w3-small' : ''));
        $viewModel->setVariable('optionItemIdsSelected', $optionItemIdsSelected);
        $viewModel->setVariable('additionalCssClassOuter', $additionalCssClassOuter);
        $viewModel->setVariable('additionalCssClassInner', $additionalCssClassInner);
        
        return $this->getView()->render($viewModel);
    }

}
