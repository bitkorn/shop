<?php

namespace Bitkorn\Shop\View\Helper\Article\Option;

use Bitkorn\Shop\Entity\Article\ShopArticleOptionsEntity;
use Bitkorn\Shop\Tablex\Article\Option\ShopArticleOptionTablex;
use Html2Text\Html2Text;
use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Model\ViewModel;

/**
 * Description of ShopArticleOption
 *
 * @author allapow
 */
class ShopArticleOptionsFromJson extends AbstractHelper
{
    const TEMPLATE = 'template/article/option/shopArticleOptionsFromJson';

    /**
     *
     * @var ShopArticleOptionTablex
     */
    private $shopArticleOptionTablex;

    /**
     *
     * @var array
     */
    private array $shopArticleOptionsIdAssocs = [];

    /**
     *
     * @var ShopArticleOptionsEntity
     */
    private $shopArticleOptionsEntity;

    /**
     *
     * @param ShopArticleOptionTablex $shopArticleOptionTablex
     */
    public function setShopArticleOptionTablex(ShopArticleOptionTablex $shopArticleOptionTablex)
    {
        $this->shopArticleOptionTablex = $shopArticleOptionTablex;
    }

    /**
     *
     * @param ShopArticleOptionsEntity $shopArticleOptionsEntity
     */
    public function setShopArticleOptionsEntity(ShopArticleOptionsEntity $shopArticleOptionsEntity)
    {
        $this->shopArticleOptionsEntity = $shopArticleOptionsEntity;
    }

    public function __invoke($shopArticleOptionsJson, $shopArticleId, bool $isHtml = true)
    {
        $viewModel = new ViewModel();
        $viewModel->setTemplate(self::TEMPLATE);
        if (empty($shopArticleOptionsJson) || empty($shopArticleId)) {
            return $this->getView()->render($viewModel);
        }
        $shopArticleOptions = json_decode($this->shopArticleOptionsEntity->cleanOptionsJson($shopArticleOptionsJson, false), true);
        $viewModel->setVariable('shopArticleOptions', $shopArticleOptions);
        $viewModel->setVariable('shopArticleId', $shopArticleId);

        if (!isset($this->shopArticleOptionsIdAssocs[$shopArticleId])) {
            $this->shopArticleOptionsIdAssocs[$shopArticleId] = $this->shopArticleOptionTablex->getShopArticleOptionItemsAllWithArticlePricediffIdAssoc($shopArticleId);
        }
        $viewModel->setVariable('shopArticleOptionPricediffsIdAssoc', $this->shopArticleOptionsIdAssocs[$shopArticleId]);

        if($isHtml) {
            return $this->getView()->render($viewModel);
        }
        $html = new Html2Text($this->getView()->render($viewModel));
        return $html->getText();
    }

}
