<?php

namespace Bitkorn\Shop\View\Helper\Article\Option;

use Bitkorn\Shop\Tablex\Article\Option\ShopArticleOptionTablex;
use Laminas\View\Model\ViewModel;

/**
 * Description of ShopArticleOption
 *
 * @author allapow
 */
class ShopArticleOption extends \Laminas\View\Helper\AbstractHelper
{

    const SELECT_TEMPLATE = 'template/article/option/optionSelect';
    const SIZEDEF_TEMPLATE = 'template/article/option/optionSizedef';

    /**
     *
     * @var ShopArticleOptionTablex
     */
    private $shopArticleOptionTablex;

    /**
     *
     * @param ShopArticleOptionTablex $shopArticleOptionTablex
     */
    public function setShopArticleOptionTablex(ShopArticleOptionTablex $shopArticleOptionTablex)
    {
        $this->shopArticleOptionTablex = $shopArticleOptionTablex;
    }

    public function __invoke(array $shopArticleOptionItems, $optionItemIdsSelected = [], $additionalCssClassInner = '')
    {
        if (empty($shopArticleOptionItems[0])) {
            return '';
        }
        $viewModel = new ViewModel();
        $shopArticleOptionType = $shopArticleOptionItems[0]['shop_article_option_def_type'];
        switch ($shopArticleOptionType) {
            case 'input':
                break;
            case 'select':
                $viewModel->setTemplate(self::SELECT_TEMPLATE);
                break;
            case 'sizedef':
                $viewModel->setTemplate(self::SIZEDEF_TEMPLATE);
                break;
            case 'multiselect':
            case 'checkbox':
            case 'radio':
            case 'file':
        }

        $viewModel->setVariable('shopArticleOptionItems', $shopArticleOptionItems);
        $viewModel->setVariable('optionItemIdsSelected', $optionItemIdsSelected);

        $isPricediff = 0;
        $optionItemPreselection = [];
        foreach ($shopArticleOptionItems as $shopArticleOptionItem) {
            if (in_array($shopArticleOptionItem['shop_article_option_item_id'], $optionItemIdsSelected)) {
                $optionItemPreselection = $shopArticleOptionItem;
            }
            if (!empty($shopArticleOptionItem['pricediff'])) {
                $isPricediff = 1;
            }
        }
        $viewModel->setVariable('optionItemPreselection', $optionItemPreselection);
        $viewModel->setVariable('isPricediff', $isPricediff);
        $viewModel->setVariable('additionalCssClassInner', $additionalCssClassInner);

        return $this->getView()->render($viewModel);
    }

}
