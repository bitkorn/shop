<?php

namespace Bitkorn\Shop\View\Helper\Article\Option;

use Bitkorn\Shop\Table\Article\Option\ShopArticleOptionArticleRelationTable;
use Bitkorn\Shop\Table\Article\Option\ShopArticleOptionItemArticlePricediffTable;
use Bitkorn\Shop\Tablex\Article\Option\ShopArticleOptionTablex;
use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Model\ViewModel;

/**
 * Benutzung in Artikel-Listen wie z.B. im POS.
 * Laed alle ArtikelRelations und ArtikelOptions:
 * Bei Erstellung (construct) werden zwei grosse Arrays erstellt ($shopArticleOptionArticleRelationsIdAssoc & $shopArticleOptionItemIdAssoc)
 *
 * @author allapow
 */
class ShopArticleOptionsByArticleId extends AbstractHelper
{
    const TEMPLATE = 'template/article/option/shopArticleOptionsByArticleId';

    /**
     *
     * @var ShopArticleOptionArticleRelationTable
     */
    private ShopArticleOptionArticleRelationTable $shopArticleOptionArticleRelationTable;

    /**
     *
     * @var ShopArticleOptionTablex
     */
    private ShopArticleOptionTablex $shopArticleOptionTablex;

    /**
     *
     * @var ShopArticleOptionItemArticlePricediffTable
     */
    private ShopArticleOptionItemArticlePricediffTable $shopArticleOptionItemArticlePricediffTable;

    /**
     *
     * @var array key = shop_article_id; value = shop_article_option_def_id
     */
    private array $shopArticleOptionArticleRelationsIdAssoc;

    /**
     *
     * @var array key = shop_article_option_def_id; value = $shopArticleOptionItems[]
     */
    private array $shopArticleOptionItemIdAssoc;

    public function __invoke($shopArticleId, $additionalCssClassOuter = '', $additionalCssClassInner = '')
    {
        if (empty($shopArticleId)) {
            return '';
        }
        $viewModel = new ViewModel();
        $viewModel->setTemplate(self::TEMPLATE);

        if (empty($this->shopArticleOptionArticleRelationsIdAssoc)) {
            $this->shopArticleOptionArticleRelationsIdAssoc = $this->shopArticleOptionArticleRelationTable->getShopArticleOptionArticleRelationsIdAssoc();
        }
        if (!isset($this->shopArticleOptionArticleRelationsIdAssoc[$shopArticleId])) {
            return '';
        }
        if (empty($this->shopArticleOptionItemIdAssoc)) {
            $this->shopArticleOptionItemIdAssoc = $this->shopArticleOptionTablex->getShopArticleOptionItemsIdAssoc();
        }
        if (!isset($this->shopArticleOptionItemIdAssoc[$this->shopArticleOptionArticleRelationsIdAssoc[$shopArticleId][0]])) {
            return '';
        }
        $shopArticleOptions = [];
        foreach ($this->shopArticleOptionArticleRelationsIdAssoc[$shopArticleId] as $optionDefId) {
            foreach ($this->shopArticleOptionItemIdAssoc[$optionDefId] as &$shopArticleOptionItem) {
                if ($this->shopArticleOptionItemArticlePricediffTable->existShopArticleOptionItemArticlePricediff($shopArticleId, $shopArticleOptionItem['shop_article_option_item_id'])) {
                    $shopArticleOptionItem['pricediff'] = $this->shopArticleOptionItemArticlePricediffTable->getShopArticleOptionItemArticlePricediff($shopArticleId, $shopArticleOptionItem['shop_article_option_item_id']);
                }
                $shopArticleOptionItem = array_merge($shopArticleOptionItem, ['shop_article_id' => $shopArticleId]);
            }
            $shopArticleOptions[] = $this->shopArticleOptionItemIdAssoc[$optionDefId];
        }

        $viewModel->setVariable('articleId', $shopArticleId);
        $viewModel->setVariable('shopArticleOptions', $shopArticleOptions);
        $viewModel->setVariable('additionalCssClassOuter', $additionalCssClassOuter);
        $viewModel->setVariable('additionalCssClassInner', $additionalCssClassInner);

        return $this->getView()->render($viewModel);
    }

    /**
     *
     * @param ShopArticleOptionArticleRelationTable $shopArticleOptionArticleRelationTable
     */
    public function setShopArticleOptionArticleRelationTable(ShopArticleOptionArticleRelationTable $shopArticleOptionArticleRelationTable)
    {
        $this->shopArticleOptionArticleRelationTable = $shopArticleOptionArticleRelationTable;
    }

    /**
     *
     * @param ShopArticleOptionTablex $shopArticleOptionTablex
     */
    public function setShopArticleOptionTablex(ShopArticleOptionTablex $shopArticleOptionTablex)
    {
        $this->shopArticleOptionTablex = $shopArticleOptionTablex;
    }

    /**
     *
     * @param ShopArticleOptionItemArticlePricediffTable $shopArticleOptionItemArticlePricediffTable
     */
    public function setShopArticleOptionItemArticlePricediffTable(ShopArticleOptionItemArticlePricediffTable $shopArticleOptionItemArticlePricediffTable)
    {
        $this->shopArticleOptionItemArticlePricediffTable = $shopArticleOptionItemArticlePricediffTable;
    }

}
