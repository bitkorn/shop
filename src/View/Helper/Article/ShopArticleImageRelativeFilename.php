<?php

namespace Bitkorn\Shop\View\Helper\Article;

use Laminas\View\Helper\AbstractHelper;

/**
 * UNUSED since BitkornImages
 *
 * @author allapow
 */
class ShopArticleImageRelativeFilename extends AbstractHelper
{

    private $bitkornShopConfig;

    public function __invoke(array $shopArticleImageData)
    {
        if (empty($shopArticleImageData)) {
            return '';
        }
        $imagePaths = [
            0 => $this->bitkornShopConfig['image_path_relative'] . '/' . $shopArticleImageData['shop_article_id'] . '/' . $shopArticleImageData['shop_article_image_filename'],
            1 => $this->bitkornShopConfig['image_path_relative'] . '/' . $shopArticleImageData['shop_article_id'] . '/' . $shopArticleImageData['shop_article_image_filename_thumb'],
            2 => $this->bitkornShopConfig['image_path_relative'] . '/' . $shopArticleImageData['shop_article_id'] . '/' . $shopArticleImageData['shop_article_image_filename_thumb_small'],
            3 => $this->bitkornShopConfig['image_path_relative'] . '/' . $shopArticleImageData['shop_article_id'] . '/' . $shopArticleImageData['shop_article_image_filename_thumb_medium']
        ];
        return $imagePaths;
    }

    public function setBitkornShopConfig($bitkornShopConfig)
    {
        $this->bitkornShopConfig = $bitkornShopConfig;
    }

}
