<?php

namespace Bitkorn\Shop\Tools\Article;

/**
 * Description of ShopArticleSefurl
 *
 * @author allapow
 */
class ShopArticleSefurl
{

    const PREG_SEFURL_CHARS = "/[a-z0-9]+/";

    public static function computeSefurl($inputString)
    {
        if(empty($inputString)) {
            return '';
        }
        $inputString = strtolower($inputString);
        $inputString = preg_replace(['/ä/','/ö/','/ü/','/ß/'], ['ae','oe','ue','ss'], $inputString);
        $matches = array();
        $kp = preg_match_all(self::PREG_SEFURL_CHARS, $inputString, $matches);
        if ($kp) {
            return implode('-', $matches[0]);
        }
        return 'foo';
    }

}
