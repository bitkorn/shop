<?php

namespace Bitkorn\Shop\Tools\Basket;

/**
 * Description of DiscountHashValid
 *
 * @author Torsten Brieskorn
 */
class DiscountHashValid
{

    /**
     * 
     * @param string $discountType required for all discountTypes
     * @param string $discountTypeValue required for discountType: quantity, time, date
     * @param int $discounthashCount required for discountType: unique, quantity
     * @param int $discountTime required for discountType: time
     * @return bool
     */
    public static function isDiscountHashValid(string $discountType, string $discountTypeValue, int $discounthashCount, int $discountTime): bool
    {
        switch ($discountType) {
            case 'unique':
                if ($discounthashCount == 0) {
                    return true;
                } else {
                    return false;
                }
                break;
            case 'quantity':
                if ($discounthashCount < intval($discountTypeValue)) {
                    return true;
                } else {
                    return false;
                }
                break;
            case 'time':
                if (($discountTime + intval($discountTypeValue) * 86400) > time()) {
                    return true;
                } else {
                    return false;
                }
                break;
            case 'date':
                $discountTypeValueTime = intval($discountTypeValue);
                $now = time();
                if (date('Y', $discountTypeValueTime) >= date('Y', $now) && date('m', $discountTypeValueTime) >= date('m', $now) && date('d', $discountTypeValueTime) >= date('d', $now)) {
                    return true;
                } else {
                    return false;
                }
                break;
        }
        return false;
    }

}
