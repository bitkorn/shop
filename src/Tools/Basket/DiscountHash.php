<?php

namespace Bitkorn\Shop\Tools\Basket;

/**
 *
 * @author allapow
 */
class DiscountHash
{

    /**
     * $prefix length plus $length must not longer than db field (20).
     * @param int $prefix
     * @param int $length
     * @param boolean $uppercase
     * @return string
     */
    public static function generateDiscountHash($prefix = '', $length = 7, $uppercase = true)
    {
        $sha256 = hash('sha256', 'discounthash ak85$/w_ 2' . time() . rand(0, 2000));
        if(!$uppercase) {
            return $prefix . substr($sha256, 0, $length);
        }
        return $prefix . strtoupper(substr($sha256, 0, $length));
    }

}
