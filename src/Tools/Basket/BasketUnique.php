<?php

namespace Bitkorn\Shop\Tools\Basket;


/**
 * Description of BasketUnique
 *
 * @author allapow
 */
class BasketUnique
{

    public static function computeBasketUnique()
    {
        return hash('sha256', 'salt like city :)' . time() . rand(10000, 999999999));
    }

    public static function validUnique(string $unique): bool
    {
        return strlen($unique) > 60;
    }
}
